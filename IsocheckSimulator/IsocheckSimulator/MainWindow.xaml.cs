﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO;
using System.IO.Ports;

namespace IsocheckSimulator
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const String terminatorString = "$\x0d\x0a";
        static SerialPort _serialPort = null;

        static Thread readThread = null;
        const string defaultComFileName = "defaultComPort.txt";

        public MainWindow()
        {
            InitializeComponent();
            manageComPort();
            initInterfaceField();
        }

        private void manageComPort()
        {
            String lastComPort = "";
            if (File.Exists(defaultComFileName) != false)
            {
                string[] lines = System.IO.File.ReadAllLines(defaultComFileName);
                if (lines.Length > 0)
                {
                    lastComPort = lines[0];
                }
            }

            int defIndex = 0;

            string[] ports = SerialPort.GetPortNames();
            for (int i = 0; i < ports.Length; i++)
            {
                if (lastComPort.Equals(ports[i]) != false)
                {
                    defIndex = i;
                }
                serialPortComboBox.Items.Add(ports[i]);
            }
            if (serialPortComboBox.Items.Count > 0)
            {
                if (defIndex < serialPortComboBox.Items.Count)
                {
                    serialPortComboBox.SelectedIndex = defIndex;
                }
                else
                {
                    serialPortComboBox.SelectedIndex = 0;
                }
            }
        }

        private void connectDisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                if (serialPortComboBox.SelectedIndex >= 0)
                {
                    _serialPort = new SerialPort(serialPortComboBox.Items[serialPortComboBox.SelectedIndex].ToString(), 9600, Parity.None, 8, StopBits.One);

                    readThread = new Thread(Read);

                    _serialPort.Open();

                    readThread.Start();

                    connectDisconnectButton.Content = "Disconnect";

                    using (StreamWriter outputFile = new StreamWriter(defaultComFileName))
                    {
                        outputFile.WriteLine(serialPortComboBox.Items[serialPortComboBox.SelectedIndex].ToString());
                    }
                }
                else
                {

                    MessageBox.Show("No com selected");
                }
            }
            else
            {
                readThread.Abort();

                _serialPort.Close();

                readThread = null;

                _serialPort = null;

                connectDisconnectButton.Content = "Connect";
            }

        }

        public void Read()
        {
            byte[] rxBuffer = Enumerable.Repeat((byte)0x00, 1024).ToArray();
            byte[] byteArrived = new byte[1];
            while (true)
            {
                Thread.Sleep(1);

                if(_serialPort.BytesToRead > 0)
                {
                    _serialPort.Read(byteArrived, 0, 1);

                    String timeStamp = DateTime.Now.ToString("HH:mm:ss:fff");
                    try
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            rxTextBox.Text = timeStamp + " - " + byteArrived.Length.ToString().PadLeft(3, '0') +  " - " + (char)(byteArrived[0]);
                        });
                    }
                    catch (Exception)
                    {

                    }
                    
                    String toSend = null;
                    switch (byteArrived[0])
                    {
                        case (byte)'#':
                            /* dati continui: ductTemperature - differentialPressure - staticPressure */
                            toSend = "#";

                            if (ductTemperature < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(Math.Abs(ductTemperature))).ToString().PadLeft(4, '0');

                            toSend += ",";
                             
                            if (differentialPressure < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(Math.Abs(differentialPressure * 10))).ToString().PadLeft(4, '0');

                            toSend += ",";

                            if (staticPressure < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(Math.Abs(staticPressure))).ToString().PadLeft(4, '0');

                            toSend += terminatorString;
                            break;
                        case (byte)'&':
                            /* init 2: fwa */
                            toSend = "&";

                            if (fwa < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(Math.Abs(fwa * 1000))).ToString().PadLeft(4, '0');

                            toSend += terminatorString;
                            break;
                        case (byte)'S':
                            /* init 1: ductShapeComboBox - diameter - longSide - shortSide - kappa - density - moisture - barometricPressure */
                            int ductShape = 0;
                            try
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    ductShape = ductShapeComboBox.SelectedIndex;
                                });
                            }
                            catch (Exception){}
                            toSend = "S"
                                + ductShape.ToString().PadLeft(4, '0');

                            toSend += ",";

                            if (diameter < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(diameter)).ToString().PadLeft(4, '0');

                            toSend += ",";

                            if (longSide < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(longSide)).ToString().PadLeft(4, '0');

                            toSend += ",";

                            if (shortSide < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(shortSide)).ToString().PadLeft(4, '0');

                            toSend += ",";

                            if (kappa < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(kappa * 100)).ToString().PadLeft(4, '0');

                            toSend += ",";

                            if (density < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(density * 1000)).ToString().PadLeft(4, '0');

                            toSend += ",";

                            if (moisture < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(moisture * 100)).ToString().PadLeft(4, '0');

                            toSend += ",";

                            if (barometricPressure < 0)
                            {
                                toSend += "-";
                            }
                            toSend += ((int)(barometricPressure)).ToString().PadLeft(4, '0');

                            toSend += terminatorString;
                            break;
                    }
                    if(toSend != null)
                    {
                        timeStamp = DateTime.Now.ToString("HH:mm:ss:fff");
                        
                        try
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                txTextBox.Text = timeStamp + " - " + toSend.Length.ToString().PadLeft(3, '0') + " - " + toSend;
                            });
                        }
                        catch (Exception)
                        {

                        }
                        byte[] bytes = Encoding.ASCII.GetBytes(toSend);
                        _serialPort.Write(bytes, 0, bytes.Length);
                    }
                }

            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                if(readThread != null)
                {
                    readThread.Abort();
                }

                readThread = null;
            }
            catch (Exception)
            {

            }

            try
            {
                if (_serialPort != null)
                {
                    _serialPort.Close();
                }

                _serialPort = null;
            }
            catch (Exception)
            {

            }
        }

        double diameter;
        double shortSide;
        double longSide;
        double kappa;
        double density;
        double moisture;
        double barometricPressure;
        double fwa;
        double differentialPressure;
        double staticPressure;
        double ductTemperature;

        private void initInterfaceField()
        {
            ductShapeComboBox.Items.Add("Circle");
            ductShapeComboBox.Items.Add("Rectangular");
            ductShapeComboBox.SelectedIndex = 0;

            diameter = 100;
            shortSide = 100;
            longSide = 100;
            kappa = 0.831;
            density = 1.293;
            moisture = 10.0;
            barometricPressure = 1013;
            fwa = 0.995;
            differentialPressure = 30;
            staticPressure = 10;
            ductTemperature = 100;

            diameterTextBox.Text      = diameter.ToString();
            longSideTextBox.Text = longSide.ToString();
            shortSideTextBox.Text = shortSide.ToString();
            kappaTextBox.Text = kappa.ToString("F2");
            densityTextBox.Text = density.ToString("F3");
            moistureTextBox.Text = moisture.ToString("F2");
            barometricPressureTextBox.Text = barometricPressure.ToString();
            fwaTextBox.Text = fwa.ToString("F3");
            differentialPressureTextBox.Text = differentialPressure.ToString("F1");
            staticPressureTextBox.Text = staticPressure.ToString("F0");
            ductTemperatureTextBox.Text = ductTemperature.ToString();

            diameterTextBox.Background = Brushes.White;
            longSideTextBox.Background = Brushes.White;
            shortSideTextBox.Background = Brushes.White;
            kappaTextBox.Background = Brushes.White;
            densityTextBox.Background = Brushes.White;
            moistureTextBox.Background = Brushes.White;
            barometricPressureTextBox.Background = Brushes.White;
            fwaTextBox.Background = Brushes.White;
            differentialPressureTextBox.Background = Brushes.White;
            staticPressureTextBox.Background = Brushes.White;
            ductTemperatureTextBox.Background = Brushes.White;
        }

        private void diameterTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    diameter = (float)Math.Round(double.Parse(diameterTextBox.Text), 0);
                    diameterTextBox.Background = Brushes.White;
                    if (diameter.ToString().Equals(diameterTextBox.Text) == false)
                    {
                        diameterTextBox.Text = diameter.ToString();
                        diameterTextBox.SelectionStart = diameterTextBox.Text.Length;
                        diameterTextBox.SelectionLength = 0;
                    }
                    diameterTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    diameterTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void diameterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            diameterTextBox.Background = Brushes.Yellow;
        }

        private void shortSideTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    shortSide = (float)Math.Round(double.Parse(shortSideTextBox.Text), 0);
                    shortSideTextBox.Background = Brushes.White;
                    if (shortSide.ToString().Equals(shortSideTextBox.Text) == false)
                    {
                        shortSideTextBox.Text = shortSide.ToString();
                        shortSideTextBox.SelectionStart = shortSideTextBox.Text.Length;
                        shortSideTextBox.SelectionLength = 0;
                    }
                    shortSideTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    shortSideTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void shortSideTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            shortSideTextBox.Background = Brushes.Yellow;
        }

        private void longSideTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    longSide = (float)Math.Round(double.Parse(longSideTextBox.Text), 0);
                    longSideTextBox.Background = Brushes.White;
                    if (longSide.ToString().Equals(longSideTextBox.Text) == false)
                    {
                        longSideTextBox.Text = longSide.ToString();
                        longSideTextBox.SelectionStart = longSideTextBox.Text.Length;
                        longSideTextBox.SelectionLength = 0;
                    }
                    longSideTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    longSideTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void longSideTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            longSideTextBox.Background = Brushes.Yellow;
        }

        private void kappaTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    kappa = (float)Math.Round(double.Parse(kappaTextBox.Text), 2);
                    kappaTextBox.Background = Brushes.White;
                    if (kappa.ToString().Equals(kappaTextBox.Text) == false)
                    {
                        kappaTextBox.Text = kappa.ToString("F2");
                        kappaTextBox.SelectionStart = kappaTextBox.Text.Length;
                        kappaTextBox.SelectionLength = 0;
                    }
                    kappaTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    kappaTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void kappaTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            kappaTextBox.Background = Brushes.Yellow;
        }

        private void densityTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    density = (float)Math.Round(double.Parse(densityTextBox.Text), 3);
                    densityTextBox.Background = Brushes.White;
                    if (density.ToString().Equals(densityTextBox.Text) == false)
                    {
                        densityTextBox.Text = density.ToString("F3");
                        densityTextBox.SelectionStart = densityTextBox.Text.Length;
                        densityTextBox.SelectionLength = 0;
                    }
                    densityTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    densityTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void densityTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            densityTextBox.Background = Brushes.Yellow;
        }

        private void moistureTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    moisture = (float)Math.Round(double.Parse(moistureTextBox.Text), 2);
                    moistureTextBox.Background = Brushes.White;
                    if (moisture.ToString().Equals(moistureTextBox.Text) == false)
                    {
                        moistureTextBox.Text = moisture.ToString("F2");
                        moistureTextBox.SelectionStart = moistureTextBox.Text.Length;
                        moistureTextBox.SelectionLength = 0;
                    }
                    moistureTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    moistureTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void moistureTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            moistureTextBox.Background = Brushes.Yellow;
        }

        private void barometricPressureTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    barometricPressure = (float)Math.Round(double.Parse(barometricPressureTextBox.Text), 0);
                    barometricPressureTextBox.Background = Brushes.White;
                    if (barometricPressure.ToString().Equals(barometricPressureTextBox.Text) == false)
                    {
                        barometricPressureTextBox.Text = barometricPressure.ToString();
                        barometricPressureTextBox.SelectionStart = barometricPressureTextBox.Text.Length;
                        barometricPressureTextBox.SelectionLength = 0;
                    }
                    barometricPressureTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    barometricPressureTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void barometricPressureTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            barometricPressureTextBox.Background = Brushes.Yellow;
        }

        private void fwaTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    fwa = (float)Math.Round(double.Parse(fwaTextBox.Text), 3);
                    fwaTextBox.Background = Brushes.White;
                    if (fwa.ToString().Equals(fwaTextBox.Text) == false)
                    {
                        fwaTextBox.Text = fwa.ToString("F3");
                        fwaTextBox.SelectionStart = fwaTextBox.Text.Length;
                        fwaTextBox.SelectionLength = 0;
                    }
                    fwaTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    fwaTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void fwaTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            fwaTextBox.Background = Brushes.Yellow;
        }

        private void differentialPressureTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    differentialPressure = (float)Math.Round(double.Parse(differentialPressureTextBox.Text), 1);
                    differentialPressureTextBox.Background = Brushes.White;
                    if (differentialPressure.ToString().Equals(differentialPressureTextBox.Text) == false)
                    {
                        differentialPressureTextBox.Text = differentialPressure.ToString("F1");
                        differentialPressureTextBox.SelectionStart = differentialPressureTextBox.Text.Length;
                        differentialPressureTextBox.SelectionLength = 0;
                    }
                    differentialPressureTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    differentialPressureTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void differentialPressureTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            differentialPressureTextBox.Background = Brushes.Yellow;
        }

        private void staticPressureTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    staticPressure = (float)Math.Round(double.Parse(staticPressureTextBox.Text), 0);
                    staticPressureTextBox.Background = Brushes.White;
                    if (staticPressure.ToString().Equals(staticPressureTextBox.Text) == false)
                    {
                        staticPressureTextBox.Text = staticPressure.ToString();
                        staticPressureTextBox.SelectionStart = staticPressureTextBox.Text.Length;
                        staticPressureTextBox.SelectionLength = 0;
                    }
                    staticPressureTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    staticPressureTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void staticPressureTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            staticPressureTextBox.Background = Brushes.Yellow;
        }

        private void ductTemperatureTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    ductTemperature = (float)Math.Round(double.Parse(ductTemperatureTextBox.Text), 0);
                    ductTemperatureTextBox.Background = Brushes.White;
                    if (ductTemperature.ToString().Equals(ductTemperatureTextBox.Text) == false)
                    {
                        ductTemperatureTextBox.Text = ductTemperature.ToString();
                        ductTemperatureTextBox.SelectionStart = ductTemperatureTextBox.Text.Length;
                        ductTemperatureTextBox.SelectionLength = 0;
                    }
                    ductTemperatureTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    ductTemperatureTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void ductTemperatureTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ductTemperatureTextBox.Background = Brushes.Yellow;
        }
    }
}
