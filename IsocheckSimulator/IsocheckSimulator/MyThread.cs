﻿using System;
using System.Threading;

namespace IsocheckSimulator
{
    public abstract class MyThread
    {
        private String _id;
        private volatile bool _shouldStop;
        private ThreadPriority _tp;
        private Thread mThread;
        private int _creatorLevel;

        public String Id
        {
            get { return _id; }
        }

        public int CreatorLevel
        {
            get { return _creatorLevel; }
        }

        public bool ShouldStop
        {
            get { return _shouldStop; }
        }



        /**
            * Costruttore
            * @param id - Identificativo del thread
            * @param tp - priorità del thread
            * */
        public MyThread(String id, int creatorLevel, ThreadPriority tp)
        {
            this._id = id;
            this._tp = tp;
            this._creatorLevel = creatorLevel;
            init();
        }

        /**
            * Costruttore
            * @param id - Identificativo del thread
            * */
        public MyThread(String id, int creatorLevel)
            : this(id, creatorLevel, ThreadPriority.Normal)
        {
        }

        /**
        * Run : metodo astratto che dovrà essere implementato da ogni classe che estenderà MyThread
            */
        public abstract void run();

        private void init()
        {
            mThread = new Thread(this.run);
            mThread.Name = this._id;
            this._shouldStop = false;
            this.mThread.Priority = this._tp;
        }

        /**
        * Start : metodo per far partire il thread
        * */
        public void Start()
        {
            if (mThread.ThreadState == ThreadState.Unstarted)
            {
                mThread.Start();
            }
            else if (mThread.ThreadState == ThreadState.Stopped)
            {
                init();
                mThread.Start();
            }
        }

        /**
        * Stop : metodo per far terminare forzatamente il thread
        * */
        public void Stop()
        {
            if (mThread.IsAlive)
            {
                mThread.Abort();
            }
        }

        public void RequestStop()
        {
            _shouldStop = true;
        }

        public bool isRunning()
        {
            return mThread.IsAlive;
        }

        public ThreadState GetThreadState()
        {
            return mThread.ThreadState;
        }
    }
}
