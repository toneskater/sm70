﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IsocheckSimulator
{
    class MyQueue
    {
        Queue<byte[]> messageTxQueue;
        object messageTxQueueLock;
        public MyQueue()
        {
            messageTxQueue = new Queue<byte[]>();
            messageTxQueueLock = new object();
        }

        public byte[] dequeueTxMessage()
        {
            byte[] returnValue = null;
            lock (messageTxQueueLock)
            {
                returnValue = messageTxQueue.Dequeue();
            }
            return returnValue;
        }

        public void clearQueueTx()
        {
            lock (messageTxQueueLock)
            {
                messageTxQueue.Clear();
            }
        }

        public void enqueueTxMessage(byte[] message, bool checkIsEqual)
        {
            lock (messageTxQueueLock)
            {
                bool found = false;
                if (checkIsEqual != false)
                {
                    for (int i = 0; (i < messageTxQueue.Count) && (found == false); i++)
                    {
                        byte[] array = messageTxQueue.ElementAt(i);
                        if (array.Length == message.Length)
                        {
                            bool equal = true;
                            for (int j = 0; j < (message.Length) && (equal == true); j++)
                            {
                                if (array[j] != message[j])
                                {
                                    equal = false;
                                }
                            }
                            if (equal != false)
                            {
                                found = true;
                            }
                        }
                    }
                }

                if (found == false)
                {
                    messageTxQueue.Enqueue(message);
                }
            }
        }

        public int sizeQueueTxMessage()
        {
            int returnValue;
            lock (messageTxQueueLock)
            {
                returnValue = messageTxQueue.Count;
            }
            return returnValue;
        }
    }
}
