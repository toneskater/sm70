/*
 * client_file.h
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */
  
#ifndef CLIENT_FILE_H
#define CLIENT_FILE_H

/* Includes ------------------------------------------------------------------*/
#include "typedef.h"

/* Public define ------------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Public function prototypes -----------------------------------------------*/
STD_RETURN_t ClientFile_OpenAndReadOrCreateFile(sbyte* fileName, _FILE_PATH destPath, char* headerToWrite, ulong clientMaxNumber);
STD_RETURN_t ClientFile_SaveFile(sbyte* fileName, _FILE_PATH destFileSystem, ulong clientMaxNumber);

#endif // CLIENT_FILE_H 
