/*
 * info.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define INFO_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"
#include "eepm_data.h"

/* Private define ------------------------------------------------------------*/
#define HMI_MY_ID HMI_ID_INFO

/* #define INFO_ENABLE_DEBUG_COUNTERS */
#define INFO_NORMAL_ENABLE
/* #define INFO_FAST_ENABLE */
/* #define INFO_LOW_PRIO_ENABLE */
/* #define INFO_THREAD_ENABLE */

#define INFO_THREAD_SLEEP_MS 50U

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Info_logFlag(FLAG_t flag);
static STD_RETURN_t Info_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Info_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Info_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Info_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t Info_initVariable(void);
static STD_RETURN_t Info_manageButtons(ulong execTimeMs);
static STD_RETURN_t Info_manageScreen(ulong execTimeMs);
#ifdef INFO_THREAD_ENABLE
static STD_RETURN_t Info_modelInit(void);
static STD_RETURN_t Info_model(ulong execTimeMs);
#endif

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Info_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Info_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Info_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Info_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t Info_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t Info_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Info_initVariable %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Info_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(infoScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        infoScreen_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Info_logFlag(FLAG_INFO_BUTTON_BACK);
    }
    if(info_ambienteTestButton_released != false) /* These flag is reset by GUI */
    {
        info_ambienteTestButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Info_logFlag(FLAG_INFO_AMBIENTE_TEST);
    }
    if(info_ambienteDownloadButton_released != false) /* These flag is reset by GUI */
    {
        info_ambienteDownloadButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Info_logFlag(FLAG_INFO_AMBIENTE_DOWNLOAD);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Info_manageButtons %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Info_showCalibrationData(void)
{
    ubyte calibrationDateTimeString[17U] = {0U};
    ubyte calibrationDateTimeEeprom[6U] = {0U};
    if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_SYST_DATETIME_OF_LAST_CALIBRATION, &calibrationDateTimeEeprom[0], EEPM_VALUE_TYPE_UBYTE, sizeof(calibrationDateTimeEeprom)))
    {
        if(UTIL_MemoryEqualTo(&calibrationDateTimeEeprom[0], sizeof(calibrationDateTimeEeprom), 0x00U) != false)
        {
            /* field is empty all to 0s */
            string_snprintf(&calibrationDateTimeString[0], sizeof(calibrationDateTimeString), "----/--/-- --:--");
        }
        else
        {
            string_snprintf(
                    &calibrationDateTimeString[0], sizeof(calibrationDateTimeString), "%c%c%c%c/%c%c/%c%c %c%c:%c%c", 
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[0]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[0]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[1]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[1]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[2]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[2]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[3]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[3]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[4]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[4]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[5]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[5])
            );
        }
        UTIL_BufferFromSbyteToUShort(&calibrationDateTimeString[0], sizeof(calibrationDateTimeString), &info_lastCalibration_1, 17U);
    } 

    if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_SYST_DATETIME_OF_SECOND_LAST_CALIBRATION, &calibrationDateTimeEeprom[0], EEPM_VALUE_TYPE_UBYTE, sizeof(calibrationDateTimeEeprom)))
    {
        if(UTIL_MemoryEqualTo(&calibrationDateTimeEeprom[0], sizeof(calibrationDateTimeEeprom), 0x00U) != false)
        {
            /* field is empty all to 0s */
            string_snprintf(&calibrationDateTimeString[0], sizeof(calibrationDateTimeString), "----/--/-- --:--");
        }
        else
        {
            string_snprintf(
                    &calibrationDateTimeString[0], sizeof(calibrationDateTimeString), "%c%c%c%c/%c%c/%c%c %c%c:%c%c", 
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[0]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[0]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[1]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[1]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[2]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[2]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[3]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[3]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[4]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[4]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[5]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[5])
            );
        }
        UTIL_BufferFromSbyteToUShort(&calibrationDateTimeString[0], sizeof(calibrationDateTimeString), &info_secondLastCalibration_1, 17U);
    }

    if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_SYST_DATETIME_OF_FIRST_CALIBRATION, &calibrationDateTimeEeprom[0], EEPM_VALUE_TYPE_UBYTE, sizeof(calibrationDateTimeEeprom)))
    {
        if(UTIL_MemoryEqualTo(&calibrationDateTimeEeprom[0], sizeof(calibrationDateTimeEeprom), 0x00U) != false)
        {
            /* field is empty all to 0s */
            string_snprintf(&calibrationDateTimeString[0], sizeof(calibrationDateTimeString), "----/--/-- --:--");
        }
        else
        {
            string_snprintf(
                    &calibrationDateTimeString[0], sizeof(calibrationDateTimeString), "%c%c%c%c/%c%c/%c%c %c%c:%c%c", 
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[0]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[0]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[1]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[1]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[2]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[2]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[3]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[3]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[4]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[4]),
                    BCD_HI_TO_CHAR(calibrationDateTimeEeprom[5]), BCD_LO_TO_CHAR(calibrationDateTimeEeprom[5])
            );
        }
        UTIL_BufferFromSbyteToUShort(&calibrationDateTimeString[0], sizeof(calibrationDateTimeString), &info_firstCalibration_1, 17U);
    } 
}

static STD_RETURN_t Info_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(MessageScreen_isEnteredHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            Info_showCalibrationData();
        }
    } 
    
    if(FLAG_GetAndReset(FLAG_INFO_AMBIENTE_TEST) != false)
    {
        Ambiente_Test = true;
    }
    if(FLAG_GetAndReset(FLAG_INFO_AMBIENTE_DOWNLOAD) != false)
    {
        Ambiente_Download = true;
    }
    
    if(MessageScreen_isExitedHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Info_manageScreen %d", returnValue);
    }
    return returnValue;
}

#ifdef INFO_THREAD_ENABLE
static STD_RETURN_t Info_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Info_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef INFO_THREAD_ENABLE */

#ifdef INFO_THREAD_ENABLE
static STD_RETURN_t Info_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Info_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef INFO_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void info_init()
{
    (void)Info_initVariable();
}

#ifdef INFO_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */
void info()
{
#ifdef INFO_ENABLE_DEBUG_COUNTERS
    Info_NormalRoutine++;
#endif
    (void)Info_manageButtons(Execution_Normal);
    (void)Info_manageScreen(Execution_Normal);
}
#endif /* #ifdef INFO_NORMAL_ENABLE */

#ifdef INFO_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void info_fast()
{
#ifdef INFO_ENABLE_DEBUG_COUNTERS
    Info_FastRoutine++;
#endif
}
#endif /* #ifdef INFO_FAST_ENABLE */

#ifdef INFO_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void info_low_priority()
{
#ifdef INFO_ENABLE_DEBUG_COUNTERS
    Info_LowPrioRoutine++;
#endif
}
#endif /* #ifdef INFO_LOW_PRIO_ENABLE */

#ifdef INFO_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void info_thread()
{
    ulong execTime = INFO_THREAD_SLEEP_MS;
    
    (void)Info_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef INFO_ENABLE_DEBUG_COUNTERS
        Info_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)Info_model(execTime);
    }
}
#endif /* #ifdef INFO_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void info_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void info_shutdown()
//{
//}
