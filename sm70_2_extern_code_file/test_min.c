/*
 * test_min.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define TEST_MIN_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"
#include "eepm_data.h"

/* Private define ------------------------------------------------------------*/
/* #define TEST_MIN_ENABLE_DEBUG_COUNTERS */
#define TEST_MIN_NORMAL_ENABLE
/* #define TEST_MIN_FAST_ENABLE */
/* #define TEST_MIN_LOW_PRIO_ENABLE */
#define TEST_MIN_THREAD_ENABLE

#define TEST_MIN_THREAD_SLEEP_MS 50U

/* Private typedef -----------------------------------------------------------*/
typedef enum TEST_MIN_GUI_STATE_e
{
    TEST_MIN_GUI_STATE_INIT = 0,
    TEST_MIN_GUI_STATE_READY,
    TEST_MIN_GUI_STATE_RUNNING,
    /* TEST_MIN_GUI_STATE_PAUSED, */
    TEST_MIN_GUI_STATE_PUMP_STOPPING,
    TEST_MIN_GUI_STATE_STOPPED,
    TEST_MIN_GUI_STATE_MAX_NUMBER
}TEST_MIN_GUI_STATE_t;

typedef enum TEST_MIN_MODEL_STATE_e
{
    TEST_MIN_MODEL_STATE_INIT = 0,
    TEST_MIN_MODEL_STATE_READY,
    /* TEST_MIN_MODEL_STATE_WAIT_START, */
    /* TEST_MIN_MODEL_STATE_PUMP_START, */
    TEST_MIN_MODEL_STATE_RUNNING,
    /* TEST_MIN_MODEL_STATE_PAUSED, */
    TEST_MIN_MODEL_STATE_PUMP_STOPPING,
    TEST_MIN_MODEL_STATE_STOPPED,
    TEST_MIN_MODEL_STATE_ERROR,
    TEST_MIN_MODEL_STATE_MAX_NUMBER
}TEST_MIN_MODEL_STATE_t;

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static TEST_MIN_GUI_STATE_t TestMin_GuiState;
static TEST_MIN_MODEL_STATE_t TestMin_ModelState;
static float TestMin_actualPercentage;

static const sbyte* TestMin_guiStateNames[TEST_MIN_GUI_STATE_MAX_NUMBER] =
{
	/* TEST_MIN_GUI_STATE_INIT,              */ "Init         ",
	/* TEST_MIN_GUI_STATE_READY,             */ "Ready        ",
	/* TEST_MIN_GUI_STATE_RUNNING,           */ "Running      ",
	/* TEST_MIN_GUI_STATE_PUMP_STOPPING,     */ "Pump Stopping",
	/* TEST_MIN_GUI_STATE_STOPPED,           */ "Stopped      ",
};

static const sbyte* TestMin_modelStateNames[TEST_MIN_MODEL_STATE_MAX_NUMBER] =
{
	/* TEST_MIN_MODEL_STATE_INIT,            */ "Init         ",
	/* TEST_MIN_MODEL_STATE_READY,           */ "Ready        ",
	///* TEST_MIN_MODEL_STATE_PUMP_START,      */ "Pump Starting",
	/* TEST_MIN_MODEL_STATE_RUNNING,         */ "Running      ",
	/* TEST_MIN_MODEL_STATE_PUMP_STOP,       */ "Pump Stopping",
	/* TEST_MIN_MODEL_STATE_STOPPED,         */ "Stopped      ",
	/* TEST_MIN_MODEL_STATE_ERROR,           */ "Error        ",
};

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t TestMin_logFlag(FLAG_t flag);
static STD_RETURN_t TestMin_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t TestMin_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t TestMin_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t TestMin_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t TestMin_initVariable(void);
#ifdef TEST_MIN_NORMAL_ENABLE
static STD_RETURN_t TestMin_manageButtons(ulong execTimeMs);
static STD_RETURN_t TestMin_manageScreen(ulong execTimeMs);
#endif /* #ifdef TEST_MIN_NORMAL_ENABLE */
#ifdef TEST_MIN_THREAD_ENABLE
static STD_RETURN_t TestMin_modelInit(void);
static STD_RETURN_t TestMin_model(ulong execTimeMs);
#endif /* #ifdef TEST_MIN_THREAD_ENABLE */

static STD_RETURN_t TestMin_CheckAndManageAutoTerminationCondition(void);
static STD_RETURN_t TestMin_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError);
static STD_RETURN_t TestMin_ErrorDetected(SAMPLING_ERROR_t* samplingError);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t TestMin_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t TestMin_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t TestMin_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t TestMin_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t TestMin_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t TestMin_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    TestMin_GuiState = TEST_MIN_GUI_STATE_INIT;
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef TEST_MIN_NORMAL_ENABLE
static STD_RETURN_t TestMin_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(test_min_backButton_released != false) /* These flag is reset by GUI */
    {
        test_min_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestMin_logFlag(FLAG_TEST_MIN_BUTTON_BACK);
    }
    if(test_min_startButton_released != false) /* These flag is reset by GUI */
    {
        test_min_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestMin_logFlag(FLAG_TEST_MIN_BUTTON_START);
    }
    if(test_min_stopButton_released != false) /* These flag is reset by GUI */
    {
        test_min_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestMin_logFlag(FLAG_TEST_MIN_BUTTON_STOP);
    }
    if(test_min_desiredFlow_eventAccepted != false) /* user must set the value to false */
    {
        test_min_desiredFlow_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)TestMin_logFloatValue("Desired flow", test_min_desiredFlow_value);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_MIN_NORMAL_ENABLE */

#ifdef TEST_MIN_NORMAL_ENABLE
static void TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_t stateOfArrival)
{
    debug_print("TestMin_manageScreenFieldsEnable %d", stateOfArrival);
    switch(stateOfArrival)
    {
        case TEST_MIN_GUI_STATE_INIT:
            test_min_backButton_enabled = true;
            test_min_startButton_enabled = true;
            test_min_stopButton_enabled = false;
            test_min_dataFields_enable = true;
            test_min_setPointPumpPercentage_backgroundColor = WHITE;
            break;
        case TEST_MIN_GUI_STATE_READY:
            test_min_backButton_enabled = true;
            test_min_startButton_enabled = true;
            test_min_stopButton_enabled = false;
            test_min_dataFields_enable = true;
            test_min_setPointPumpPercentage_backgroundColor = WHITE;
            break;
        case TEST_MIN_GUI_STATE_RUNNING:        
            test_min_backButton_enabled = false;
            test_min_startButton_enabled = false;
            test_min_stopButton_enabled = true;
            test_min_dataFields_enable = false;
            test_min_setPointPumpPercentage_backgroundColor = DARKGRAY;
            break;
        case TEST_MIN_GUI_STATE_PUMP_STOPPING:
            test_min_backButton_enabled = false;
            test_min_startButton_enabled = false;
            test_min_stopButton_enabled = false;
            test_min_dataFields_enable = false;
            test_min_setPointPumpPercentage_backgroundColor = DARKGRAY;
            break;
        case TEST_MIN_GUI_STATE_STOPPED:
            test_min_backButton_enabled = true;
            test_min_startButton_enabled = true;
            test_min_stopButton_enabled = false;
            test_min_dataFields_enable = true;
            test_min_setPointPumpPercentage_backgroundColor = WHITE;
            break;
    }
}

static float TestMin_FlowFound;
static ulong TestMin_RemainingMsToNextStep;

static STD_RETURN_t TestMin_showDataOnCharts(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    float flowActual = 0.0f;
    
    test_min_flowFound_value = FLOAT_TO_WORD(TestMin_FlowFound); 
    
    test_min_percentage = FLOAT_TO_WORD(TestMin_actualPercentage);                
    
    test_min_secondsToNextStep_value = (TestMin_RemainingMsToNextStep / 1000U);                        
    
    if(NETWORK_GetLitersMinuteRead(&flowActual) == STD_RETURN_OK)
    {
        common_samplingFlowActual = FLOAT_TO_WORD(flowActual);
        test_min_flow_color = WHITE;
    }
    else
    {
        common_samplingFlowActual = FLOAT_TO_WORD(0.0f);
        test_min_flow_color = RED;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_showDataOnCharts %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestMin_manageScreen(ulong execTimeMs)
{
    const HMI_ID_t localHmiId = HMI_ID_TEST_MIN;
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ushort ushortValue;
    /* START CODE */
    static TEST_MIN_GUI_STATE_t TestMin_LastGuiState = TEST_MIN_GUI_STATE_INIT;
    
    if(TestMin_LastGuiState != TestMin_GuiState)
    {
        debug_print("TMI GUI STATE from %d %s to %d %s", TestMin_LastGuiState, TestMin_guiStateNames[TestMin_LastGuiState], TestMin_GuiState, TestMin_guiStateNames[TestMin_GuiState]);
        TestMin_LastGuiState = TestMin_GuiState;
    }
    
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
        
        }
        else
        {
            switch(TestMin_GuiState)
            {
                case TEST_MIN_GUI_STATE_INIT:
                    TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_INIT);
                    common_samplingFlowActual = FLOAT_TO_WORD(0.0f);
                    test_min_flow_color = WHITE;
                    test_min_percentage  = FLOAT_TO_WORD(0.0f);                    
                    test_min_flowFound_value = FLOAT_TO_WORD(0.0f); 
                    test_min_secondsToNextStep_value = 0;
                    
                    returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MIN, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1);
                    if(STD_RETURN_OK == returnValue)
                    {
                        test_min_desiredFlow_rangeMin = FLOAT_TO_WORD(((float)ushortValue) / 100);
                    }
                    if(STD_RETURN_OK == returnValue)
                    {
                        returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MAX, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1);
                    }
                    if(STD_RETURN_OK == returnValue)
                    {
                        test_min_desiredFlow_rangeMax = FLOAT_TO_WORD(((float)ushortValue) / 100);
                    }
                    if(FLOAT_TO_WORD(1.0f) < test_min_desiredFlow_rangeMin)
                    {
                        test_min_desiredFlow_value = test_min_desiredFlow_rangeMin; /* For inverter pump */
                    }
                    else
                    {
                        test_min_desiredFlow_value = FLOAT_TO_WORD(1.0);
                    }
                    
                    TestMin_FlowFound = 0;
                    TestMin_actualPercentage = 0;
                    TestMin_RemainingMsToNextStep = 0;
                    
                    TestMin_GuiState = TEST_MIN_GUI_STATE_READY;
                    TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_READY);
                    FLAG_Set(FLAG_TEST_MIN_SCREEN_COMMAND_GO_TO_READY); 
                    break;
                case TEST_MIN_GUI_STATE_READY:
                    break;
                case TEST_MIN_GUI_STATE_RUNNING:     
                    break;
                case TEST_MIN_GUI_STATE_PUMP_STOPPING:
                    break;
                case TEST_MIN_GUI_STATE_STOPPED:
                    break;
                default:
                    break;
            }
        }
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_MIN_BUTTON_BACK) != false)
    {
        debug_print("event managed: FLAG_TEST_MIN_BUTTON_BACK");
        TestMin_GuiState = TEST_MIN_GUI_STATE_INIT;
        TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_INIT);
        FLAG_Set(FLAG_TEST_MIN_SCREEN_COMMAND_BACK); 
        (void)HMI_ChangeHmi(HMI_ID_TEST, localHmiId); 
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_MIN_BUTTON_START) != false)
    {
        debug_print("event managed: FLAG_TEST_MIN_BUTTON_START");
        TestMin_GuiState = TEST_MIN_GUI_STATE_RUNNING;
        TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_RUNNING);
        FLAG_Set(FLAG_TEST_MIN_SCREEN_COMMAND_START);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_MIN_BUTTON_STOP) != false)
    {
        debug_print("event managed: FLAG_TEST_MIN_BUTTON_STOP");
        TestMin_GuiState = TEST_MIN_GUI_STATE_PUMP_STOPPING;
        TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_PUMP_STOPPING);
        FLAG_Set(FLAG_TEST_MIN_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_EVENT_PUMP_STOPPED) != false)
    {
        debug_print("event managed: FLAG_TEST_MIN_SCREEN_EVENT_PUMP_STOPPED");
        TestMin_GuiState = TEST_MIN_GUI_STATE_STOPPED;
        TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_STOPPED);
    }
    
    /* Test stopped due to automatic condition */
    if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_EVENT_PWM_FOUND) != false)
    {         
        debug_print("event managed: FLAG_TEST_MIN_SCREEN_EVENT_PWM_FOUND");
        BUZM_BUTTON_SOUND(); /* manual sound */
        TestMin_GuiState = TEST_MIN_GUI_STATE_PUMP_STOPPING;
        TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_PUMP_STOPPING);
        MessageScreen_openInfo(DYNAMIC_STRING_INFO_TEST_MOTOR_MIN_PWM_FOUND, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_EVENT_PWM_TOO_HIGH) != false)
    {        
        debug_print("event managed: FLAG_TEST_MIN_SCREEN_EVENT_PWM_TOO_HIGH");
        BUZM_BUTTON_SOUND(); /* manual sound */
        TestMin_GuiState = TEST_MIN_GUI_STATE_PUMP_STOPPING;
        TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_PUMP_STOPPING);
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_TEST_MOTOR_MIN_PWM_TOO_HIGH, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_EVENT_TEST_STOPPED_BY_OPERATOR) != false)
    {     
        debug_print("event managed: FLAG_TEST_MIN_SCREEN_EVENT_TEST_STOPPED_BY_OPERATOR");
        BUZM_BUTTON_SOUND(); /* manual sound */
        TestMin_GuiState = TEST_MIN_GUI_STATE_PUMP_STOPPING;
        TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_PUMP_STOPPING);
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_TEST_STOPPED_BY_OPERATOR, localHmiId);
    }
    
    /* Test stopped due to error */
    if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR) != false)
    {
        BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
        MessageScreen_ManageErrorPopup(Sampling_Error, HMI_ID_MAIN, localHmiId);
        (void)HMI_ChangeHmi(HMI_ID_MAIN, HMI_GetHmiIdOnScreen());
        TestMin_GuiState = TEST_MIN_GUI_STATE_INIT;
        TestMin_manageScreenFieldsEnable(TEST_MIN_GUI_STATE_INIT);
    }
    
    /* If i'm inside the screen */
    if(localHmiId == HMI_GetHmiIdOnScreen())
    {
        TestMin_showDataOnCharts();
    }
    
    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef TEST_MIN_THREAD_ENABLE
static STD_RETURN_t TestMin_RestoreDataForNewSampling(void)
{
    return NETWORK_RestoreDataNewSampling(); /* set to 0 the counters on GR */
}

static STD_RETURN_t TestMin_sendPumpSetPointPercentage(float percentage)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = NETWORK_SetPumpPercentageSetpoint(percentage);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_sendPumpSetPointPercentage %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestMin_savePumpSetPointPercentageMin(float percentage)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = NETWORK_SetPumpPercentageMin(percentage);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_savePumpSetPointPercentageMin %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestMin_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    TestMin_ModelState = TEST_MIN_MODEL_STATE_INIT;
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_MIN_THREAD_ENABLE */

#ifdef TEST_MIN_THREAD_ENABLE


static STD_RETURN_t TestMin_ErrorDetected(SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SAMPLING_ERROR_t error = SAMPLING_ERROR_NONE;
    ubyte noComm = NETWORK_IsInNoCommunicationState();
    
    if(samplingError != NULL)
    {
        if((error == SAMPLING_ERROR_NONE) && (noComm != false))
        {
            error = SAMPLING_ERROR_NO_COMMUNICATION;
        }
        if(error == SAMPLING_ERROR_NONE)
        {
            ;
        }
        *samplingError = error;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_ErrorDetected: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t TestMin_CheckAndManageAutoTerminationCondition(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    ubyte samplingTerminationCondition;
    returnValue = TestMin_AreSamplingTerminationConditionReached(&samplingTerminationCondition, &Sampling_StopCause, &Sampling_Error);
    if(returnValue == STD_RETURN_OK)
    {
        if(samplingTerminationCondition != false)
        {
            debug_print("TEST_MIN TERMINATION CONDITION REACHED Sampling_StopCause %d", Sampling_StopCause);

            switch(Sampling_StopCause)
            {
                case SAMPLING_STOP_CAUSE_TIME:
                    //BUZM_BUTTON_SOUND(); /* manual sound */
                    //MessageScreen_openInfo(DYNAMIC_STRING_INFO_TEST_STOPPED_BY_TIME_REACHED, HMI_GetHmiIdOnScreen());
                    //TestLeak_ModelState = TEST_LEAK_MODEL_STATE_PUMP_STOP;
                    //FLAG_Set(FLAG_TEST_LEAK_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED);
                    break;
                case SAMPLING_STOP_CAUSE_MANUAL:
                    /* manual sound not called, because manual stop is by pressing button */
                    //MessageScreen_openWarning(DYNAMIC_STRING_WARNING_TEST_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen());
                    //TestLeak_ModelState = TEST_LEAK_MODEL_STATE_PUMP_STOP;
                    //FLAG_Set(FLAG_TEST_LEAK_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED);
                    break;
                case SAMPLING_STOP_CAUSE_ERROR:
                    FLAG_Set(FLAG_TEST_MIN_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR);
                    TestMin_ModelState = TEST_MIN_MODEL_STATE_INIT; /* MODEL */
                    break;
                default:
                    break;
            }
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_CheckAndManageAutoTerminationCondition %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestMin_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK; 
    STD_RETURN_t tempReturnValue = STD_RETURN_OK; 
    ulong sampleTimeInMinutes = 0U;
    ushort SecondToTheEnd;
    
    if((reached != NULL) && (stopCause != NULL) && (samplingError != NULL))
    {
        returnValue = TestMin_ErrorDetected(samplingError);
        
        if(returnValue == STD_RETURN_OK)
        {
            if(*samplingError != SAMPLING_ERROR_NONE) /* if no errors continue, otherwise stop */
            {
                *reached = true;
                *stopCause = SAMPLING_STOP_CAUSE_ERROR;
                /* samplingError already filled */
            }
            else
            {
                if(TestMin_ModelState == TEST_MIN_MODEL_STATE_RUNNING)
                {
                    {
                        *reached = false;
                    }
                }
                else
                {
                    /* not in running */
                    *reached = false;
                }
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_AreSamplingTerminationConditionReached: %d", returnValue);
    }
    
    return returnValue;
}

static Timer_t TestMin_TimerIncrement;

static STD_RETURN_t TestMin_modelStartFromReadyOrStopped(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte eepromUbyteValue;
    
    TestMin_RestoreDataForNewSampling();
    
    TestMin_actualPercentage = 0.5f;
    TestMin_FlowFound = 0.0f;
    
    if(STD_RETURN_ERROR_PARAM == TestMin_sendPumpSetPointPercentage(TestMin_actualPercentage))
    {
        MessageScreen_openError(DYNAMIC_STRING_ERROR_IMPOSSIBLE_TO_START_TEST, STD_ERROR_PARAM_ERROR_TEST_FLOW, HMI_GetHmiIdOnScreen());
    }
    else
    {
        TestMin_ModelState = TEST_MIN_MODEL_STATE_RUNNING;
        
        returnValue = EEPM_GetId(EEPM_ID_MOTC_FLOW_FILTERED_TIME_CALCULATION_SEC, &eepromUbyteValue, EEPM_VALUE_TYPE_UBYTE, 1);
        
        UTIL_TimerInit(&TestMin_TimerIncrement);
        UTIL_TimerStart(&TestMin_TimerIncrement, (((ulong)(eepromUbyteValue)) * 1000U) * 2U);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_modelStartFromReadyOrStopped %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestMin_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte pumpRunning = false;
    ubyte pumpRegulation = false;
    ubyte isExpired = false;
    ubyte eepromUbyteValue;
    float actualFlow;
    /* START CODE */
    static TEST_MIN_MODEL_STATE_t TestMin_LastModelState = TEST_MIN_MODEL_STATE_INIT;
    
    if(TestMin_ModelState != TestMin_LastModelState)
    {
        debug_print("TMI MOD STATE from %d %s to %d %s", TestMin_LastModelState, TestMin_modelStateNames[TestMin_LastModelState], TestMin_ModelState, TestMin_modelStateNames[TestMin_ModelState]);
        TestMin_LastModelState = TestMin_ModelState;
    }
    
    UTIL_TimerIncrement(&TestMin_TimerIncrement, execTimeMs);
    
    switch(TestMin_ModelState)
    {
        case TEST_MIN_MODEL_STATE_INIT:
            if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_COMMAND_GO_TO_READY)  != false)
            {
                TestMin_ModelState = TEST_MIN_MODEL_STATE_READY;
                //Sampling_manageSamplingTimeInit(0);
            }
            break;
        case TEST_MIN_MODEL_STATE_READY:
            if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_COMMAND_START) != false)
            {
                returnValue = TestMin_modelStartFromReadyOrStopped();
            }
            if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_COMMAND_BACK) != false)
            {
                TestMin_ModelState = TEST_MIN_MODEL_STATE_INIT;
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestMin_CheckAndManageAutoTerminationCondition();
            }
            break;
        /*case TEST_FLOW_MODEL_STATE_WAIT_START:

            break;*/
        /*case TEST_MIN_MODEL_STATE_PUMP_START:

            break;*/
        case TEST_MIN_MODEL_STATE_RUNNING:
            
            (void)UTIL_TimerGetRemainingTimeMs(&TestMin_TimerIncrement, &TestMin_RemainingMsToNextStep);
        
            UTIL_TimerIsExpired(&TestMin_TimerIncrement, &isExpired);
            if(isExpired != false)
            {
                (void)NETWORK_GetLitersMinuteRead(&actualFlow);
                if(actualFlow >= 1.00f)
                {
                    /* Finish */
                    debug_print("pwm found %.2f to start the pump with flow %.2f, now turn off", TestMin_actualPercentage, actualFlow);
                    TestMin_FlowFound = actualFlow;
                    TestMin_sendPumpSetPointPercentage(0.0f);
                    FLAG_Set(FLAG_TEST_MIN_SCREEN_EVENT_PWM_FOUND);
                    TestMin_ModelState = TEST_MIN_MODEL_STATE_PUMP_STOPPING;
                    TestMin_savePumpSetPointPercentageMin(TestMin_actualPercentage);
                }
                else
                {
                    UTIL_TimerStop(&TestMin_TimerIncrement);
                    UTIL_TimerInit(&TestMin_TimerIncrement);
                    (void)EEPM_GetId(EEPM_ID_MOTC_FLOW_FILTERED_TIME_CALCULATION_SEC, &eepromUbyteValue, EEPM_VALUE_TYPE_UBYTE, 1);
                    UTIL_TimerStart(&TestMin_TimerIncrement, (((ulong)(eepromUbyteValue)) * 1000U) * 2U);
                    TestMin_actualPercentage += 0.5f;
                    if(TestMin_actualPercentage > 10.0f)
                    {
                        TestMin_actualPercentage = 10.0f;
                        debug_print("Start pump pwm is too high");
                        FLAG_Set(FLAG_TEST_MIN_SCREEN_EVENT_PWM_TOO_HIGH);
                        TestMin_ModelState = TEST_MIN_MODEL_STATE_PUMP_STOPPING;
                        TestMin_sendPumpSetPointPercentage(0.0f);
                        TestMin_savePumpSetPointPercentageMin(TestMin_actualPercentage);
                    }
                    else
                    {
                        debug_print("new pwm to try %.2f", TestMin_actualPercentage);
                        TestMin_sendPumpSetPointPercentage(TestMin_actualPercentage);
                    }
                }
            }
        
            if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_COMMAND_STOP) != FALSE)
            {
                TestMin_sendPumpSetPointPercentage(0.0f);
                //Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
                //Sampling_Error = SAMPLING_ERROR_NONE;
                TestMin_ModelState = TEST_MIN_MODEL_STATE_PUMP_STOPPING;
                FLAG_Set(FLAG_TEST_MIN_SCREEN_EVENT_TEST_STOPPED_BY_OPERATOR);
                //MessageScreen_openWarning(DYNAMIC_STRING_WARNING_TEST_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestMin_CheckAndManageAutoTerminationCondition();
            }
            break;
        /*case TEST_FLOW_MODEL_STATE_PAUSED: 

            break;*/
        case TEST_MIN_MODEL_STATE_PUMP_STOPPING:
            if( 
                (NETWORK_GetPumpRunning(&pumpRunning) == STD_RETURN_OK) &&
                (NETWORK_GetPumpRegulation(&pumpRegulation) == STD_RETURN_OK)
            )
            {
                if((pumpRunning == false) && (pumpRegulation == false))
                {
                    /* pump stopped and regulated */
                    TestMin_ModelState = TEST_MIN_MODEL_STATE_STOPPED;
                    FLAG_Set(FLAG_TEST_MIN_SCREEN_EVENT_PUMP_STOPPED);
                }
            }
            
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestMin_CheckAndManageAutoTerminationCondition();
            }
            break;
        case TEST_MIN_MODEL_STATE_STOPPED:
            if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_COMMAND_BACK) != false)
            {
                TestMin_ModelState = TEST_MIN_MODEL_STATE_INIT;
            }
            if(FLAG_GetAndReset(FLAG_TEST_MIN_SCREEN_COMMAND_START) != false)
            {
                returnValue = TestMin_modelStartFromReadyOrStopped();
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestMin_CheckAndManageAutoTerminationCondition();
            }
            break;
        case TEST_MIN_MODEL_STATE_ERROR:

            break;
    }
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestMin_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_MIN_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void test_min_init()
{
    (void)TestMin_initVariable();
}

#ifdef TEST_MIN_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void test_min()
{
#ifdef TEST_MIN_ENABLE_DEBUG_COUNTERS
    TestMin_NormalRoutine++;
#endif
    (void)TestMin_manageButtons(Execution_Normal);
    (void)TestMin_manageScreen(Execution_Normal);
}
#endif /* #ifdef TEST_MIN_NORMAL_ENABLE */

#ifdef TEST_MIN_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void test_min_fast()
{
#ifdef TEST_MIN_ENABLE_DEBUG_COUNTERS
    TestMin_FastRoutine++;
#endif
}
#endif /* #ifdef TEST_MIN_FAST_ENABLE */

#ifdef TEST_MIN_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void test_min_low_priority()
{
#ifdef TEST_MIN_ENABLE_DEBUG_COUNTERS
    TestMin_LowPrioRoutine++;
#endif
}
#endif /* #ifdef TEST_MIN_LOW_PRIO_ENABLE */

#ifdef TEST_MIN_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void test_min_thread()
{
    ulong execTime = TEST_MIN_THREAD_SLEEP_MS;
    
    (void)TestMin_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef TEST_MIN_ENABLE_DEBUG_COUNTERS
        TestMin_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)TestMin_model(execTime);
    }
}
#endif /* #ifdef TEST_MIN_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void test_min_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void test_min_shutdown()
//{
//}
