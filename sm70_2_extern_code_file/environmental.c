/*
 * environmental.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define ENVIRONMENTAL_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"
#include "eepm_data.h"

/* Private define ------------------------------------------------------------*/
/* #define ENVIRONMENTAL_ENABLE_DEBUG_COUNTERS */
#define ENVIRONMENTAL_NORMAL_ENABLE
/* #define ENVIRONMENTAL_FAST_ENABLE */
/* #define ENVIRONMENTAL_LOW_PRIO_ENABLE */
#define ENVIRONMENTAL_THREAD_ENABLE

#define ENVIRONMENTAL_THREAD_SLEEP_MS 50U

//#define ENVIRONMENTAL_ISOKINETICK_SAMPLING_TIME_MAX 9999U

//#define ENVIRONMENTAL_ISOKINETICK_SAMPLING_TIME_MIN 2U
//#define ENVIRONMENTAL_ISOKINETICK_SAMPLING_VOLUME_MAX 65000U
//#define ENVIRONMENTAL_ISOKINETICK_SAMPLING_VOLUME_MIN 1U

/* Private typedef -----------------------------------------------------------*/
typedef enum ENVIRONMENTAL_GUI_STATE_e
{
    ENVIRONMENTAL_GUI_STATE_INIT = 0,
    ENVIRONMENTAL_GUI_STATE_READY,
    ENVIRONMENTAL_GUI_STATE_WAIT_START,
    ENVIRONMENTAL_GUI_STATE_RUNNING,
    ENVIRONMENTAL_GUI_STATE_PAUSED,
    ENVIRONMENTAL_GUI_STATE_PAUSED_NO_POWER,
    ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING,
    ENVIRONMENTAL_GUI_STATE_STOPPED,
    ENVIRONMENTAL_GUI_STATE_MAX_NUMBER
}ENVIRONMENTAL_GUI_STATE_t;

typedef enum ENVIRONMENTAL_MODEL_STATE_e
{
    ENVIRONMENTAL_MODEL_STATE_INIT = 0,
    ENVIRONMENTAL_MODEL_STATE_READY,
    ENVIRONMENTAL_MODEL_STATE_WAIT_START,
    ENVIRONMENTAL_MODEL_STATE_PUMP_START,
    ENVIRONMENTAL_MODEL_STATE_RUNNING,
    ENVIRONMENTAL_MODEL_STATE_PAUSED,
    ENVIRONMENTAL_MODEL_STATE_PAUSED_NO_POWER,
    ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING,
    ENVIRONMENTAL_MODEL_STATE_STOPPED,
    ENVIRONMENTAL_MODEL_STATE_ERROR,
    ENVIRONMENTAL_MODEL_STATE_MAX_NUMBER
}ENVIRONMENTAL_MODEL_STATE_t;

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static ENVIRONMENTAL_GUI_STATE_t Environmental_GuiState;
static ENVIRONMENTAL_MODEL_STATE_t Environmental_ModelState;
static float Environmental_LastAspirationFlowValue;
static ubyte Environmental_EnableGraphDrawing;
static GRAPH_DATA_TO_SHOW_t EnvironmentalGraph_DataToShow;
static ubyte Environmental_modelCalculateStatisticRefreshCounter;
static ubyte Environmental_modelCalculateStatisticNewDataForGraph;

static SYST_WARNING_t Environmental_warningCode;
static ubyte Environmental_warningSubCode; 
static SYST_ERROR_t Environmental_errorCode;
static ubyte Environmental_errorSubCode; 

static const sbyte* Environmental_guiStateNames[ENVIRONMENTAL_GUI_STATE_MAX_NUMBER] =
{
	/* ENVIRONMENTAL_GUI_STATE_INIT,              */ "Init         ",
	/* ENVIRONMENTAL_GUI_STATE_READY,             */ "Ready        ",
	/* ENVIRONMENTAL_GUI_STATE_WAIT_START,        */ "Wait Start   ",
	/* ENVIRONMENTAL_GUI_STATE_RUNNING,           */ "Running      ",
	/* ENVIRONMENTAL_GUI_STATE_PAUSED,            */ "Paused       ",
	/* ENVIRONMENTAL_GUI_STATE_PAUSED_NO_POWER    */ "Paused No Pow",
	/* ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING,     */ "Pump Stopping",
	/* ENVIRONMENTAL_GUI_STATE_STOPPED,           */ "Stopped      ",
};

static const sbyte* Environmental_modelStateNames[ENVIRONMENTAL_MODEL_STATE_MAX_NUMBER] =
{
	/* ENVIRONMENTAL_MODEL_STATE_INIT,            */ "Init         ",
	/* ENVIRONMENTAL_MODEL_STATE_READY,           */ "Ready        ",
	/* ENVIRONMENTAL_MODEL_STATE_WAIT_START,      */ "Wait Start   ",
	/* ENVIRONMENTAL_MODEL_STATE_PUMP_START,      */ "Pump Starting",
	/* ENVIRONMENTAL_MODEL_STATE_RUNNING,         */ "Running      ",
	/* ENVIRONMENTAL_MODEL_STATE_PAUSED,          */ "Paused       ",
	/* ENVIRONMENTAL_MODEL_STATE_PAUSED_NO_POWER  */ "Paused No Pow",
	/* ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING,   */ "Pump Stopping",
	/* ENVIRONMENTAL_MODEL_STATE_STOPPED,         */ "Stopped      ",
	/* ENVIRONMENTAL_MODEL_STATE_ERROR,           */ "Error        ",
};

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Environmental_logFlag(FLAG_t flag);
static STD_RETURN_t Environmental_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Environmental_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Environmental_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Environmental_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t Environmental_initVariable(void);
#ifdef ENVIRONMENTAL_NORMAL_ENABLE
static STD_RETURN_t EnvironmentalSetup_manageButtons(ulong execTimeMs);
static STD_RETURN_t EnvironmentalSetup_manageScreen(ulong execTimeMs);
static STD_RETURN_t EnvironmentalData_manageButtons(ulong execTimeMs);
static STD_RETURN_t EnvironmentalData_manageScreen(ulong execTimeMs);
static STD_RETURN_t EnvironmentalGraph_manageButtons(ulong execTimeMs);
static STD_RETURN_t EnvironmentalGraph_manageScreen(ulong execTimeMs);
static STD_RETURN_t EnvironmentalWaiting_manageButtons(ulong execTimeMs);
static STD_RETURN_t EnvironmentalWaiting_manageScreen(ulong execTimeMs);

#endif /* #ifdef ENVIRONMENTAL_NORMAL_ENABLE */
#ifdef ENVIRONMENTAL_THREAD_ENABLE
static STD_RETURN_t Environmental_modelInit(void);
static STD_RETURN_t Environmental_model(ulong execTimeMs);
#endif /* #ifdef ENVIRONMENTAL_THREAD_ENABLE */

static STD_RETURN_t Environmental_modelStartFromReadyOrStopped(void);
static STD_RETURN_t Environmental_CheckAndManageAutoTerminationCondition(void);
static STD_RETURN_t Environmental_showDataOnCharts(void);

static STD_RETURN_t Environmental_sendPumpSetPoint(void);
static STD_RETURN_t Environmental_sendPumpStop(void);
static STD_RETURN_t Environmental_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError);
static STD_RETURN_t Environmental_CheckAndManageAutoTerminationCondition(void);
static STD_RETURN_t Environmental_ErrorDetected(SAMPLING_ERROR_t* samplingError);
static STD_RETURN_t Environmental_RestoreDataForNewSampling(void);


static STD_RETURN_t Environmental_model_writeFinalData(void);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Environmental_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Environmental_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Environmental_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Environmental_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
}

static STD_RETURN_t Environmental_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t Environmental_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_INIT;
    Environmental_EnableGraphDrawing = false;
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_initVariable %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MANAGE BUTTON                                                                  */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */

#ifdef ENVIRONMENTAL_NORMAL_ENABLE
static STD_RETURN_t EnvironmentalSetup_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(environmentalScreen_backButton_released != false) /* These flag is reset by GUI */
    {
    	environmentalScreen_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_SETUP_BUTTON_BACK);
    }
    if(environmentalScreen_startButton_released != false) /* These flag is reset by GUI */
    {
    	environmentalScreen_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_SETUP_BUTTON_START);
    }
    if(environmentalScreen_pauseButton_released != false) /* These flag is reset by GUI */
    {
    	environmentalScreen_pauseButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_SETUP_BUTTON_PAUSE);
    }
    if(environmentalScreen_stopButton_released != false) /* These flag is reset by GUI */
    {
    	environmentalScreen_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_SETUP_BUTTON_STOP);
    }
    if(environmental_setup_buttonPrev_release != false) /* These flag is reset by GUI */
    {
    	environmental_setup_buttonPrev_release = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_SETUP_BUTTON_PREV);
    }
    if(environmental_setup_buttonNext_release != false) /* These flag is reset by GUI */
    {
    	environmental_setup_buttonNext_release = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_SETUP_BUTTON_NEXT);
    }
    if(environmental_setup_buttonTime_released != false) /* These flag is reset by GUI */
    {
    	environmental_setup_buttonTime_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_SETUP_BUTTON_TIME);
    }
    if(environmental_setup_buttonVolume_released != false) /* These flag is reset by GUI */
    {
    	environmental_setup_buttonVolume_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_SETUP_BUTTON_VOLUME);
    }
    if(environmentalScreen_isokineticSamplingValue_eventAccepted != false) /* user must set the value to false */
    {
        environmentalScreen_isokineticSamplingValue_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logUnsignedValue("Isokinetic sampling time or volume value", common_isokineticSamplingValue);
    }
    if(environmentalScreen_aspirationFlow_eventAccepted != false)
    {
        environmentalScreen_aspirationFlow_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFloatValue("Aspiration Flow value", environmentalScreen_aspirationFlow_value);
    }
    if(environmentalScreen_aspirationFlowError_eventAccepted != false)
    {
        environmentalScreen_aspirationFlowError_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFloatValue("Flow Stop value", environmentalScreen_aspirationFlowError_value);
    }  
    if(environmentalScreen_startDelayValue_eventAccepted != false)
    {
        environmentalScreen_startDelayValue_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logUnsignedValue("Start Delay value", common_startDelayValue);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalSetup_manageButtons %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t EnvironmentalData_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */    
    if(environmental_data_backButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_data_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_DATA_BUTTON_BACK);
    }
    if(environmental_data_startButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_data_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_DATA_BUTTON_START);
    }
    if(environmental_data_pauseButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_data_pauseButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_DATA_BUTTON_PAUSE);
    }
    if(environmental_data_stopButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_data_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_DATA_BUTTON_STOP);
    }
    if(environmental_data_buttonPrev_release != false) /* These flag is reset by GUI */
    {
    	environmental_data_buttonPrev_release = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_DATA_BUTTON_PREV);
    }
    if(environmental_data_buttonNext_release != false) /* These flag is reset by GUI */
    {
    	environmental_data_buttonNext_release = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_DATA_BUTTON_NEXT);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalData_manageButtons %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t EnvironmentalGraph_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */    
    if(environmental_graph_backButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_graph_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_BACK);
    }
    if(environmental_graph_startButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_graph_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_START);
    }
    if(environmental_graph_pauseButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_graph_pauseButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_PAUSE);
    }
    if(environmental_graph_stopButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_graph_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_STOP);
    }
    if(environmental_graph_buttonPrev_release != false) /* These flag is reset by GUI */
    {
    	environmental_graph_buttonPrev_release = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_PREV);
    }
    if(environmental_graph_buttonNext_release != false) /* These flag is reset by GUI */
    {
    	environmental_graph_buttonNext_release = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_NEXT);
    }
    if(environmental_graph_flowButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_graph_flowButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_FLOW);
    }
    if(environmental_graph_errorButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_graph_errorButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_ERROR);
    }
    if(environmental_graph_vacuumButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_graph_vacuumButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_VACUUM);
    }
    if(environmental_graph_meterTemperatureButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_graph_meterTemperatureButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_METER_TEMPERATURE);
    }
    if(graph_buttonPrev_release != false) /* These flag is reset by GUI */
    {
    	graph_buttonPrev_release = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_PREV_GRAPH);
    }
    if(graph_buttonNext_release != false) /* These flag is reset by GUI */
    {
    	graph_buttonNext_release = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_NEXT_GRAPH);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalGraph_manageButtons %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t EnvironmentalWaiting_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */    
    if(environmental_waiting_startButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_waiting_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_WAITING_BUTTON_START);
    }
    if(environmental_waiting_pauseButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_waiting_pauseButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_WAITING_BUTTON_PAUSE);
    }
    if(environmental_waiting_stopButton_released != false) /* These flag is reset by GUI */
    {
    	environmental_waiting_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Environmental_logFlag(FLAG_ENVIRONMENTAL_WAITING_BUTTON_STOP);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalWaiting_manageButtons %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MANAGE SCREEN                                                                  */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t EnvironmentalSetup_manageScreenTimeOrVolumeButton(void)
{
    switch(common_isokineticSampling)
    {
        case SAMPLING_ISOKINETIC_SAMPLING_TIME:
            environmental_setup_buttonTime_backgroundColor = DARKGREEN;
            environmental_setup_buttonVolume_backgroundColor = YELLOW;
            break;
        case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
            environmental_setup_buttonTime_backgroundColor = YELLOW;
            environmental_setup_buttonVolume_backgroundColor = DARKGREEN;
            break;
    }
    return STD_RETURN_OK;
}

static STD_RETURN_t EnvironmentalSetup_manageScreenAspirationFlow(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ushort ushortValue;
    returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MIN, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1);
    if(STD_RETURN_OK == returnValue)
    {
        environmentalScreen_aspirationFlow_rangeMin = FLOAT_TO_WORD(((float)ushortValue) / 100);
    }
    if(STD_RETURN_OK == returnValue)
    {
        returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MAX, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1);
    }
    if(STD_RETURN_OK == returnValue)
    {
        environmentalScreen_aspirationFlow_rangeMax = FLOAT_TO_WORD(((float)ushortValue) / 100);
    }
    environmentalScreen_aspirationFlow_value = FLOAT_TO_WORD(10.0);
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalSetup_manageScreenAspirationFlow %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t EnvironmentalSetup_manageScreenStopFlow(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ushort ushortValue;
    environmentalScreen_aspirationFlowError_rangeMin = FLOAT_TO_WORD(0.01f);
    returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MIN, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1);
    if(STD_RETURN_OK == returnValue)
    {
        environmentalScreen_aspirationFlowError_rangeMax = FLOAT_TO_WORD(((float)(ushortValue - 1U)) / 100);
        environmentalScreen_aspirationFlowError_value = FLOAT_TO_WORD(0.10f);  
        if(environmentalScreen_aspirationFlowError_value > environmentalScreen_aspirationFlowError_rangeMax)
        {
            environmentalScreen_aspirationFlow_value = environmentalScreen_aspirationFlowError_rangeMax;
        }
    }
    /* TODO stringa per mostrare i limiti */
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalSetup_manageScreenStopFlow %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_t stateOfArrival)
{
    debug_print("FIELD_ENABLE %d %s", stateOfArrival, Environmental_guiStateNames[stateOfArrival]);
    switch(stateOfArrival)
    {
        case ENVIRONMENTAL_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
            environmental_setup_dataFields_enable = true;
            environmental_setup_dataFields_backgroundColor = WHITE;
            environmentalScreen_backButton_enabled = true;
            
            environmentalScreen_startButton_enabled = true;
            environmentalScreen_pauseButton_enabled = false;
            environmentalScreen_stopButton_enabled = false;
            environmental_buttonPrevAndNext_enable = false;
            environmental_buttonPrevAndNext_enable = false;
            break;
        case ENVIRONMENTAL_GUI_STATE_READY: /* entered first time */
            environmental_setup_dataFields_enable = true;
            environmental_setup_dataFields_backgroundColor = WHITE;
            environmentalScreen_backButton_enabled = true;
            
            environmentalScreen_startButton_enabled = true;
            environmentalScreen_pauseButton_enabled = false;
            environmentalScreen_stopButton_enabled = false;
            environmental_buttonPrevAndNext_enable = false;
            environmental_buttonPrevAndNext_enable = false;
            break;
        case ENVIRONMENTAL_GUI_STATE_WAIT_START:
            environmental_setup_dataFields_enable = false;
            environmental_setup_dataFields_backgroundColor = DARKGRAY;
            environmentalScreen_backButton_enabled = false;
            
            environmentalScreen_startButton_enabled = true;
            environmentalScreen_pauseButton_enabled = true;
            environmentalScreen_stopButton_enabled = true;
            environmental_buttonPrevAndNext_enable = false;
            environmental_buttonPrevAndNext_enable = false;
            break;
        case ENVIRONMENTAL_GUI_STATE_RUNNING: /* running */
            environmental_setup_dataFields_enable = false;
            environmental_setup_dataFields_backgroundColor = DARKGRAY;
            environmentalScreen_backButton_enabled = false;
            
            environmentalScreen_startButton_enabled = false;
            environmentalScreen_pauseButton_enabled = true;
            environmentalScreen_stopButton_enabled = true;
            environmental_buttonPrevAndNext_enable = true;
            environmental_buttonPrevAndNext_enable = true;
            break;
        case ENVIRONMENTAL_GUI_STATE_PAUSED: /* paused */
            environmental_setup_dataFields_enable = false;
            environmental_setup_dataFields_backgroundColor = DARKGRAY;
            environmentalScreen_backButton_enabled = false;
            
            environmentalScreen_startButton_enabled = true;
            environmentalScreen_pauseButton_enabled = false;
            environmentalScreen_stopButton_enabled = true;
            environmental_buttonPrevAndNext_enable = true;
            environmental_buttonPrevAndNext_enable = true;
            break;
        case ENVIRONMENTAL_GUI_STATE_PAUSED_NO_POWER: /* paused */
            environmental_setup_dataFields_enable = false;
            environmental_setup_dataFields_backgroundColor = DARKGRAY;
            environmentalScreen_backButton_enabled = false;
            
            environmentalScreen_startButton_enabled = false;
            environmentalScreen_pauseButton_enabled = false;
            environmentalScreen_stopButton_enabled = true;
            environmental_buttonPrevAndNext_enable = true;
            environmental_buttonPrevAndNext_enable = true;
            break;
        case ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING:
            /* Enabel the buttons only when pump is really stopped */
            /* Model in pump stop */
            environmental_setup_dataFields_enable = false;
            environmental_setup_dataFields_backgroundColor = DARKGRAY;
            environmentalScreen_backButton_enabled = false;
            
            environmentalScreen_startButton_enabled = false;
            environmentalScreen_pauseButton_enabled = false;
            environmentalScreen_stopButton_enabled = false;
            environmental_buttonPrevAndNext_enable = true;
            environmental_buttonPrevAndNext_enable = true;
            break;
        case ENVIRONMENTAL_GUI_STATE_STOPPED: /* really stopped */
            environmental_setup_dataFields_enable = true;
            environmental_setup_dataFields_backgroundColor = WHITE;
            environmentalScreen_backButton_enabled = true;
            
            environmentalScreen_startButton_enabled = true;
            environmentalScreen_pauseButton_enabled = false;
            environmentalScreen_stopButton_enabled = false;
            environmental_buttonPrevAndNext_enable = true;
            environmental_buttonPrevAndNext_enable = true;
            break;
        default:
            break;
    }
    return STD_RETURN_OK;
}

static STD_RETURN_t EnvironmentalGraph_manageScreenButtonDataToShow(void)
{
    switch(EnvironmentalGraph_DataToShow)
    {
        case GRAPH_DATA_TO_SHOW_FLOW:
            environmental_graph_flowButton_backgroundColor = DARKGREEN;
            environmental_graph_errorButton_backgroundColor = YELLOW;
            environmental_graph_vacuumButton_backgroundColor = YELLOW;
            environmental_graph_meterTemperatureButton_backgroundColor = YELLOW;
            break;
        case GRAPH_DATA_TO_SHOW_ERROR:
            environmental_graph_flowButton_backgroundColor = YELLOW;
            environmental_graph_errorButton_backgroundColor = DARKGREEN;
            environmental_graph_vacuumButton_backgroundColor = YELLOW;
            environmental_graph_meterTemperatureButton_backgroundColor = YELLOW;
            break;
        case GRAPH_DATA_TO_SHOW_VACUUM:
            environmental_graph_flowButton_backgroundColor = YELLOW;
            environmental_graph_errorButton_backgroundColor = YELLOW;
            environmental_graph_vacuumButton_backgroundColor = DARKGREEN;
            environmental_graph_meterTemperatureButton_backgroundColor = YELLOW;
            break;
        case GRAPH_DATA_TO_SHOW_METER_TEMPERATURE:
            environmental_graph_flowButton_backgroundColor = YELLOW;
            environmental_graph_errorButton_backgroundColor = YELLOW;
            environmental_graph_vacuumButton_backgroundColor = YELLOW;
            environmental_graph_meterTemperatureButton_backgroundColor = DARKGREEN;
            break;
        default:
            break;
    }
    return STD_RETURN_OK;
}

static STD_RETURN_t Environmental_manageScreenCommonEvent(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    /* Sampling stopped due to error */   
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_INIT;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_INIT);
        MessageScreen_ManageErrorPopup(Sampling_Error, HMI_ID_MAIN, HMI_GetHmiIdOnScreen());
    }
    
     /* Sampling stopped due to automatic condition, time, volume */
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_TIME) != false) /* COMMON EVENT now wait the pump stop*/
    {         
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_TIME");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING);
        MessageScreen_openInfo(DYNAMIC_STRING_INFO_SAMPLING_STOPPED_TIME_REACHED, HMI_GetHmiIdOnScreen());
    }
    
    /* Sampling stopped due to automatic condition, time, volume */
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_VOLUME) != false) /* COMMON EVENT now wait the pump stop*/
    {         
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_VOLUME");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING);
        MessageScreen_openInfo(DYNAMIC_STRING_INFO_SAMPLING_STOPPED_VOLUME_REACHED, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_BY_OPERATOR) != false) /* COMMON EVENT now wait the pump stop*/
    {         
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_VOLUME");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING);
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_MODEL_EVENT_NO_POWER_SUPPLY) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_MODEL_EVENT_NO_POWER_SUPPLY");
        BUZM_BUTTON_SOUND(); /* manual sound */
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_NO_POWER_SUPPLY, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_MODEL_EVENT_NO_POWER_SUPPLY_RUNNING) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_MODEL_EVENT_NO_POWER_SUPPLY_RUNNING");
        BUZM_BUTTON_SOUND(); /* manual sound */
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PAUSED_NO_POWER;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PAUSED_NO_POWER);
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_NO_POWER_SUPPLY_RUNNING, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_MODEL_EVENT_BACK_FROM_NO_POWER_SUPPLY_RUNNING) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_MODEL_EVENT_BACK_FROM_NO_POWER_SUPPLY_RUNNING");
        BUZM_BUTTON_SOUND(); /* manual sound */
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_RUNNING;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_RUNNING);
    }
    
    /* Event from model for a real stop - MANAGE AFTER A STOP */
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_EVENT_PUMP_STOPPED) != false) /* COMMON EVENT pump really stopped */
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_EVENT_PUMP_STOPPED");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_STOPPED;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_STOPPED);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_RUNNING) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_RUNNING");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_RUNNING;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_RUNNING);
        if(HMI_GetHmiIdOnScreen() != HMI_ID_ENVIRONMENTAL_DATA) /* Change screen only if in stopped */
        {
            (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_DATA, HMI_GetHmiIdOnScreen());
        }
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_WAITING) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_WAITING");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_WAIT_START;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_WAIT_START);
        (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_WAITING, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_ERROR) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_ERROR");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_INIT;
        (void)HMI_ChangeHmi(HMI_ID_OPERATING_PROGRAM, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_MODEL_EVENT_WARNING_DETECTED) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_MODEL_EVENT_WARNING_DETECTED");        
        BUZM_BUTTON_SOUND(); /* manual sound */
        MessageScreen_ManageWarningPopup(Environmental_warningCode, Environmental_warningSubCode, HMI_GetHmiIdOnScreen());
    }
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_manageScreenCommonEvent %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t EnvironmentalSetup_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    const HMI_ID_t localHmiId = HMI_ID_ENVIRONMENTAL_SETUP;
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            switch(Environmental_GuiState)
            {
                case ENVIRONMENTAL_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
                    common_isokineticSampling = SAMPLING_ISOKINETIC_SAMPLING_TIME; /* SAMPLING_ISOKINETIC_SAMPLING_VOLUME */
                    (void)EnvironmentalSetup_manageScreenTimeOrVolumeButton();
                    Sampling_manageIsokineticSamplingLimits();
                    (void)EnvironmentalSetup_manageScreenAspirationFlow();
                    (void)EnvironmentalSetup_manageScreenStopFlow();
                    common_startDelayValue = 0;
                    (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_INIT);
                    
                    /* Screen Data and Graph */
                    common_samplingVacuum = FLOAT_TO_WORD(0.00f);
                    common_samplingMeterTemperature = FLOAT_TO_WORD(0.00f);
                    common_samplingVolume = FLOAT_TO_WORD(0.00f);
                    common_samplingFlowActual = FLOAT_TO_WORD(0.00f);
                    common_samplingFlowError = FLOAT_TO_WORD(0.00f);
                    environmental_charts_volume_color = WHITE;
                    environmental_charts_flow_color = WHITE;
                    environmental_charts_flow_color = WHITE;
                    environmental_charts_vacuum_color = WHITE;
                    environmental_charts_meter_temperature_color = WHITE;
                    
                    /* Screen Graph */
                    EnvironmentalGraph_DataToShow = GRAPH_DATA_TO_SHOW_FLOW;
                    (void)EnvironmentalGraph_manageScreenButtonDataToShow();

                    /* Screen Waiting */
                    
                    /* Jump to ready */
                    Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_READY;
                    (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_READY);
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_GO_TO_READY); 
                    break;
                case ENVIRONMENTAL_GUI_STATE_READY: /* entered first time */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_WAIT_START: /* start delay */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_RUNNING: /* running */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_PAUSED: /* paused */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING:
                
                    break;
                case ENVIRONMENTAL_GUI_STATE_STOPPED: /* stopped */
                    
                    break;
            }
        }
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SETUP_BUTTON_START) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SETUP_BUTTON_START");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_RUNNING;
        /* Wait event from model to know if i'm in waiting or real running' in COMMON EVENT to set the enable on buttons */
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START); 
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SETUP_BUTTON_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SETUP_BUTTON_PAUSE");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PAUSED;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PAUSED);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE); 
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SETUP_BUTTON_STOP) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SETUP_BUTTON_STOP");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SETUP_BUTTON_PREV) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SETUP_BUTTON_PREV");
        (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_GRAPH, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SETUP_BUTTON_NEXT) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SETUP_BUTTON_NEXT");
        (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_DATA, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SETUP_BUTTON_TIME) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SETUP_BUTTON_TIME");
        common_isokineticSampling = SAMPLING_ISOKINETIC_SAMPLING_TIME;
        Sampling_manageIsokineticSamplingLimits();
        (void)EnvironmentalSetup_manageScreenTimeOrVolumeButton();
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SETUP_BUTTON_VOLUME) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SETUP_BUTTON_VOLUME");
        common_isokineticSampling = SAMPLING_ISOKINETIC_SAMPLING_VOLUME;
        Sampling_manageIsokineticSamplingLimits();
        (void)EnvironmentalSetup_manageScreenTimeOrVolumeButton();
    }

    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SETUP_BUTTON_BACK) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SETUP_BUTTON_BACK");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_INIT;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_INIT);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_BACK); 
        (void)HMI_ChangeHmi(HMI_ID_OPERATING_PROGRAM, HMI_GetHmiIdOnScreen());
    }
    
    /* If i'm inside the screen */
    if(localHmiId == HMI_GetHmiIdOnScreen())
    {

    }

    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalSetup_manageScreen %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t EnvironmentalData_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    const HMI_ID_t localHmiId = HMI_ID_ENVIRONMENTAL_DATA;
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            switch(Environmental_GuiState)
            {
                case ENVIRONMENTAL_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_READY: /* entered first time */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_WAIT_START:
                
                    break;
                case ENVIRONMENTAL_GUI_STATE_RUNNING: /* running */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_PAUSED: /* paused */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING:
                
                    break;
                case ENVIRONMENTAL_GUI_STATE_STOPPED: /* stopped */
                    
                    break;
            }
        }
    } 
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_DATA_BUTTON_START) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_DATA_BUTTON_START");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_RUNNING;
        /* Wait event from model to know if i'm in waiting or real running' in COMMON EVENT to set the enable on buttons */
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START); 
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_DATA_BUTTON_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_DATA_BUTTON_PAUSE");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PAUSED;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PAUSED);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE); 
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_DATA_BUTTON_STOP) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_DATA_BUTTON_STOP");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_DATA_BUTTON_PREV) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_DATA_BUTTON_PREV");
        (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_SETUP, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_DATA_BUTTON_NEXT) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_DATA_BUTTON_NEXT");
        (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_GRAPH, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_DATA_BUTTON_BACK) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_DATA_BUTTON_BACK");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_INIT;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_INIT);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_BACK);
        (void)HMI_ChangeHmi(HMI_ID_OPERATING_PROGRAM, HMI_GetHmiIdOnScreen()); 
    }
    
    /* If i'm inside the screen */
    if(localHmiId == HMI_GetHmiIdOnScreen())
    {
        Sampling_manageSamplingTimeOnScreen();
        
        Sampling_manageSamplingRemainingTimeOnScreen();
        
        Environmental_showDataOnCharts();
    }

    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalData_manageScreen %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t EnvironmentalGraph_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    const HMI_ID_t localHmiId = HMI_ID_ENVIRONMENTAL_GRAPH;
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            switch(Environmental_GuiState)
            {
                case ENVIRONMENTAL_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_READY: /* entered first time */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_WAIT_START:
                
                    break;
                case ENVIRONMENTAL_GUI_STATE_RUNNING: /* running */

                    break;
                case ENVIRONMENTAL_GUI_STATE_PAUSED: /* paused */

                    break;
                case ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING:

                    break;
                case ENVIRONMENTAL_GUI_STATE_STOPPED: /* stopped */

                    break;
            }
        }
        Environmental_modelCalculateStatisticNewDataForGraph = true;
        Sampling_modelManageButtonMobileScreen();
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_START) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_START");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_RUNNING;
        /* Wait event from model to know if i'm in waiting or real running' in COMMON EVENT to set the enable on buttons */
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START); 
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_PAUSE");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PAUSED;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PAUSED);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE); 
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_STOP) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_STOP");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_PREV) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_PREV");
        (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_DATA, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_NEXT) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_NEXT");
        (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_SETUP, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_BACK) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_BACK");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_INIT;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_INIT);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_BACK);
        (void)HMI_ChangeHmi(HMI_ID_OPERATING_PROGRAM, HMI_GetHmiIdOnScreen()); 
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_FLOW) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_FLOW");
        EnvironmentalGraph_DataToShow = GRAPH_DATA_TO_SHOW_FLOW;
        (void)EnvironmentalGraph_manageScreenButtonDataToShow();
        Environmental_modelCalculateStatisticNewDataForGraph = true;
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_ERROR) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_ERROR");
        EnvironmentalGraph_DataToShow = GRAPH_DATA_TO_SHOW_ERROR;
        (void)EnvironmentalGraph_manageScreenButtonDataToShow();
        Environmental_modelCalculateStatisticNewDataForGraph = true;
    }   
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_VACUUM) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_VACUUM");
        EnvironmentalGraph_DataToShow = GRAPH_DATA_TO_SHOW_VACUUM;
        (void)EnvironmentalGraph_manageScreenButtonDataToShow();
        Environmental_modelCalculateStatisticNewDataForGraph = true;
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_METER_TEMPERATURE) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_METER_TEMPERATURE");
        EnvironmentalGraph_DataToShow = GRAPH_DATA_TO_SHOW_METER_TEMPERATURE;
        (void)EnvironmentalGraph_manageScreenButtonDataToShow();
        Environmental_modelCalculateStatisticNewDataForGraph = true;
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_PREV_GRAPH) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_PREV_GRAPH");
        Environmental_modelCalculateStatisticNewDataForGraph = true;
        Sampling_modelInitMobileScreenPositionMoveToLeft();
        //Environmental_modelManageButtonMobileScreen();
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_GRAPH_BUTTON_NEXT_GRAPH) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_GRAPH_BUTTON_NEXT_GRAPH");
        Environmental_modelCalculateStatisticNewDataForGraph = true;
        Sampling_modelInitMobileScreenPositionMoveToRight();
        //Environmental_modelManageButtonMobileScreen();
    }
    
    /* If i'm inside the screen */
    if(localHmiId == HMI_GetHmiIdOnScreen())
    {
        Environmental_showDataOnCharts();
    }
    
    if(Environmental_modelCalculateStatisticNewDataForGraph != false)
    {
        Environmental_modelCalculateStatisticNewDataForGraph = false;
        
        Sampling_modelCalculateMobileScreenPosition();
        
        Sampling_modelManageButtonMobileScreen();
        
        if(localHmiId == HMI_GetHmiIdOnScreen())
        {
            Sampling_drawDataOnGraph(EnvironmentalGraph_DataToShow);
        }
    }

    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalGraph_manageScreen %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t EnvironmentalWaiting_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    const HMI_ID_t localHmiId = HMI_ID_ENVIRONMENTAL_WAITING;
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            switch(Environmental_GuiState)
            {
                case ENVIRONMENTAL_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_READY: /* entered first time */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_WAIT_START:
                
                    break;
                case ENVIRONMENTAL_GUI_STATE_RUNNING: /* running */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_PAUSED: /* paused */
                    
                    break;
                case ENVIRONMENTAL_GUI_STATE_PUMP_STOPPING:
                
                    break;
                case ENVIRONMENTAL_GUI_STATE_STOPPED: /* stopped */
                    
                    break;
            }
        }
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_WAITING_BUTTON_START) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_WAITING_BUTTON_START");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_RUNNING;
        /* Wait event from model to know if i'm in waiting or real running' in COMMON EVENT to set the enable on buttons */
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START); 
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_WAITING_BUTTON_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_WAITING_BUTTON_PAUSE");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_PAUSED;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_PAUSED);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE); 
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_WAITING_BUTTON_STOP) != false)
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_WAITING_BUTTON_STOP");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_STOPPED;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_STOPPED);
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_STOPPED_IN_WAIT_START) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_STOPPED_IN_WAIT_START");
        Environmental_GuiState = ENVIRONMENTAL_GUI_STATE_READY;
        (void)EnvironmentalSetup_manageScreenFieldsEnable(ENVIRONMENTAL_GUI_STATE_READY);
        (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_SETUP, HMI_GetHmiIdOnScreen());
    }
    
    /* If i'm inside the screen */
    if(localHmiId == HMI_GetHmiIdOnScreen())
    {
        Sampling_manageSamplingRemainingTimeOnScreen();
    }

    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalWaiting_manageScreen %d", returnValue);
    }
    return returnValue;
}

#endif /* #ifdef ENVIRONMENTAL_NORMAL_ENABLE */

#ifdef ENVIRONMENTAL_NORMAL_ENABLE
static STD_RETURN_t Environmental_showDataOnCharts(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    float meterTemperature = 0.0f;
    float volume = 0.0f;
    float flowActual = 0.0f;
    float meterPressure = 0.0f;
    float vacuum = 0.0f;
    float isoFlow = WORD_TO_FLOAT(environmentalScreen_aspirationFlow_value);
    
    if(NETWORK_GetMeterPressure(&meterPressure) == STD_RETURN_OK)
    {
        vacuum = meterPressure;
        //switch(common_measureUnit)
        //{
            //case SAMPLING_MEASURE_UNIT_MMH20:
                common_samplingVacuum = FLOAT_TO_WORD(vacuum);
                environmental_charts_vacuum_color = WHITE;
            //    break;
            //case SAMPLING_MEASURE_UNIT_PA:
            //    common_samplingVacuum = FLOAT_TO_WORD(MMH2O_TO_PA(vacuum));
            //    environmental_charts_vacuum_color = WHITE;
            //    break;
            //default:
            //    common_samplingVacuum = FLOAT_TO_WORD(vacuum);
            //    environmental_charts_vacuum_color = RED;
            //    break;
        //}
    }
    else
    {
        common_samplingVacuum = FLOAT_TO_WORD(vacuum);
        environmental_charts_vacuum_color = RED;
    }
    
    if(NETWORK_GetMeterTemperature(&meterTemperature) == STD_RETURN_OK)
    {
        common_samplingMeterTemperature = FLOAT_TO_WORD(meterTemperature);
        environmental_charts_meter_temperature_color = WHITE;
    }
    else
    {
        common_samplingMeterTemperature = FLOAT_TO_WORD(meterTemperature);
        environmental_charts_meter_temperature_color = RED;
    }
    
    if(NETWORK_GetLitersMinuteRead(&flowActual) == STD_RETURN_OK)
    {
        common_samplingFlowActual = FLOAT_TO_WORD(flowActual);
        environmental_charts_flow_color = WHITE;
        
        if(flowActual > 0.0f)
        {
            common_samplingFlowError = FLOAT_TO_WORD((((flowActual - isoFlow) * 100.0f)/isoFlow));
        }
        else
        {
            common_samplingFlowError = FLOAT_TO_WORD(0.0f);
        }
    }
    else
    {
        common_samplingFlowActual = FLOAT_TO_WORD(flowActual);
        environmental_charts_flow_color = RED;
        common_samplingFlowError = FLOAT_TO_WORD(0.0f);
    }
    
    if(NETWORK_GetLitersTotalFromSamplingStart(&volume) == STD_RETURN_OK)
    {
        common_samplingVolume = FLOAT_TO_WORD(volume);
        environmental_charts_volume_color = WHITE;
    }
    else
    {
        common_samplingVolume = FLOAT_TO_WORD(volume);
        environmental_charts_volume_color = RED;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_showDataOnCharts %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef ENVIRONMENTAL_THREAD_ENABLE
static STD_RETURN_t Environmental_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_INIT;
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef ENVIRONMENTAL_THREAD_ENABLE */

#ifdef ENVIRONMENTAL_THREAD_ENABLE

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL                                                                          */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Environmental_GetDataFromView(ENVIRONMENTAL_InterfaceData_t* interfaceData)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    
    if(interfaceData != NULL)
    {
        interfaceData->samplingType = common_isokineticSampling;
        interfaceData->time = common_isokineticSamplingValue;
        interfaceData->volume = common_isokineticSamplingValue;
        interfaceData->normalizationTemperature = parameter_normalizationTemperatureValue;
        interfaceData->aspirationFlow = WORD_TO_FLOAT(environmentalScreen_aspirationFlow_value);
        interfaceData->aspirationFlowError = WORD_TO_FLOAT(environmentalScreen_aspirationFlowError_value);
        interfaceData->samplingStartDelayMinutes = common_startDelayValue;
        returnValue = STD_RETURN_OK;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_GetDataFromView %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Environmental_RestoreDataForNewSampling(void)
{
    return NETWORK_RestoreDataNewSampling(); /* set to 0 the counters on GR */
}

static STD_RETURN_t Environmental_modelStartFromReadyOrStopped(void)
{
    const REPORT_NAME_CODE_t reportType = REPORT_NAME_CODE_ENVIRONMENTAL;
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    ENVIRONMENTAL_InterfaceData_t interfaceData;
    
    returnValue = Environmental_GetDataFromView(&interfaceData);
    
    if(STD_RETURN_OK == returnValue)
    {
        returnValue = ReportFile_createName(reportType);
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeStarReportHeader(reportType);
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeInstrumentIds();
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeClientId(); /* anagrafica*/
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeOperatorId(); 
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeInitDataEnvironmental(&interfaceData);
    }
    if(returnValue == STD_RETURN_OK)
    {
        slong delaySeconds = MINS_TO_SECS(interfaceData.samplingStartDelayMinutes);
        
        if(delaySeconds > 0U)
        {
            Environmental_RestoreDataForNewSampling(); /* set to 0 the counters on GR */
            returnValue = ReportFile_writeDelayedStart();            
            Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_WAIT_START;
            FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_WAITING);
        }
        else
        {
            Sampling_modelInitMobileScreenPosition();
            Environmental_modelCalculateStatisticNewDataForGraph = false;
            Environmental_modelCalculateStatisticRefreshCounter = MODEL_CALCULATE_STATISTIC_REFRESH_COUNTER_VALUE;
            Sampling_modelCalculateStatisticInit();
            Environmental_RestoreDataForNewSampling();
            Environmental_sendPumpSetPoint();
            returnValue = ReportFile_writeStart();            
            Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_START;
            FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_RUNNING);
        }
        Sampling_manageSamplingTimeInit(delaySeconds);
    }
    else
    {
        /* Stay in this state but re-enable the buttons */
        MessageScreen_openError(DYNAMIC_STRING_ERROR_SAMPLING_START_ERROR, returnValue, HMI_GetHmiIdOnScreen());
        FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_ERROR);
        Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_INIT;
    }
    Environmental_LastAspirationFlowValue = interfaceData.aspirationFlow;
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_modelStartFromReadyOrStopped %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Environmental_sendPumpSetPoint(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    ENVIRONMENTAL_InterfaceData_t interfaceData;
    
    returnValue = Environmental_GetDataFromView(&interfaceData);
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = NETWORK_SetPumpSetpoint(interfaceData.aspirationFlow);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_sendPumpSetPoint %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Environmental_sendPumpStop(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    returnValue = NETWORK_SetPumpSetpoint(0.0f);
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_sendPumpStop %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Environmental_ErrorDetected(SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SAMPLING_ERROR_t error = SAMPLING_ERROR_NONE;
    ENVIRONMENTAL_InterfaceData_t interfaceData;
    float actualFlow;
    ubyte noComm = NETWORK_IsInNoCommunicationState();
    
    returnValue = Environmental_GetDataFromView(&interfaceData);
    
    if(samplingError != NULL)
    {
        if((error == SAMPLING_ERROR_NONE) && (noComm != false))
        {
            error = SAMPLING_ERROR_NO_COMMUNICATION;
            debug_print("NO COM ERROR DETECTED");
        }
        if(error == SAMPLING_ERROR_NONE)
        {
            if(
                (returnValue == STD_RETURN_OK) && 
                (NETWORK_GetLitersMinuteRead(&actualFlow) == STD_RETURN_OK)
            )
            {
                error = Sampling_LowFlowErrorFct(actualFlow, interfaceData.aspirationFlowError, (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_RUNNING));
            }
        }
        if(error == SAMPLING_ERROR_NONE)
        {
            (void)NETWORK_GetErrorCodeAndSubCode(&Environmental_errorCode, &Environmental_errorSubCode, true);
            if(Environmental_errorCode != SYST_ERROR_NONE)
            {
                debug_print("Environmental_ErrorDetected %d %d", Environmental_errorCode, Environmental_errorSubCode);
                switch(Environmental_errorCode)
                {
                    case SYST_ERROR_NONE:
                        /* Impossible but */
                        break;
                    case SYST_ERROR_PUMP_NO_INPULSE:
                        debug_print("error = SAMPLING_ERROR_PUMP_NO_INPULSE;");
                        error = SAMPLING_ERROR_PUMP_NO_INPULSE;
                        break;
                    case SYST_ERROR_PUMP_NO_REGULATION_CAPABILITIES:
                        debug_print("error = SAMPLING_ERROR_PUMP_NO_REGULATION_CAPABILITIES;");
                        error = SAMPLING_ERROR_PUMP_NO_REGULATION_CAPABILITIES;
                        break;
                    case SYST_ERROR_MEMORY_CORRUPTION:
                        debug_print("error = SAMPLING_ERROR_MEMORY_CORRUPTION;");
                        error = SAMPLING_ERROR_MEMORY_CORRUPTION;
                        break;
                    case SYST_ERROR_PROGRAM_ERROR:
                        debug_print("error = SAMPLING_ERROR_PROGRAM_ERROR;");
                        error = SAMPLING_ERROR_PROGRAM_ERROR;
                        break;
                    case SYST_ERROR_INVERTER_NO_COM:
                        debug_print("error = SAMPLING_ERROR_INVERTER_NO_COM;");
                        error = SAMPLING_ERROR_INVERTER_NO_COM;
                        break;
                    default:
                        debug_print("error = SAMPLING_ERROR_UNKNOWN;");
                        error = SAMPLING_ERROR_UNKNOWN;
                        break;
                }
            }
        }
        *samplingError = error;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_ErrorDetected: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t Environmental_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ENVIRONMENTAL_InterfaceData_t interfaceData; 
    ulong sampleTimeInMinutes = 0U;
    ulong sampleVolumeInLiters = 0U;
    
    if((reached != NULL) && (stopCause != NULL) && (samplingError != NULL))
    {
        returnValue = Environmental_GetDataFromView(&interfaceData);
        
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = Environmental_ErrorDetected(samplingError);
            
            if(returnValue == STD_RETURN_OK)
            {
                if(*samplingError != SAMPLING_ERROR_NONE) /* if no errors continue, otherwise stop */
                {
                    *reached = true;
                    *stopCause = SAMPLING_STOP_CAUSE_ERROR;
                    /* samplingError already filled */
                }
                else
                {
                    if(
                        (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_PUMP_START) ||
                        (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_RUNNING)
                    )
                    {
                        switch(interfaceData.samplingType)
                        {
                            case SAMPLING_ISOKINETIC_SAMPLING_TIME:
                                sampleTimeInMinutes = Sampling_SampleTimeInMinutes();
                                if(sampleTimeInMinutes >= interfaceData.time)
                                {
                                    *reached = true;
                                    *stopCause = SAMPLING_STOP_CAUSE_TIME;
                                    *samplingError = SAMPLING_ERROR_NONE;
                                }
                                else
                                {
                                    *reached = false;
                                }
                                break;
                            case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
                                sampleVolumeInLiters = Sampling_SampleVolumeInLiters();
                                if(sampleVolumeInLiters >= interfaceData.volume)
                                {
                                    *reached = true;
                                    *stopCause = SAMPLING_STOP_CAUSE_VOLUME;
                                    *samplingError = SAMPLING_ERROR_NONE;
                                }
                                else
                                {
                                    *reached = false;
                                }
                                break;
                            default:
                                returnValue = STD_RETURN_ERROR_SAMPLING_TYPE_UNKNOWN;
                                break;
                        } 
                    }
                    else
                    {
                        /* not in running */
                        *reached = false;
                    }
                }
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_AreSamplingTerminationConditionReached: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t Environmental_CheckAndManageAutoTerminationCondition(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte samplingTerminationCondition;
    returnValue = Environmental_AreSamplingTerminationConditionReached(&samplingTerminationCondition, &Sampling_StopCause, &Sampling_Error);
    if(returnValue == STD_RETURN_OK)
    {
        if(samplingTerminationCondition != false)
        {
            Environmental_sendPumpStop();
            debug_print("TERMINATION CONDITION REACHED");

            switch(Sampling_StopCause)
            {
                case SAMPLING_STOP_CAUSE_TIME:
                    BUZM_BUTTON_SOUND(); /* manual sound */
                    //MessageScreen_openInfo(DYNAMIC_STRING_INFO_SAMPLING_STOPPED_TIME_REACHED, HMI_GetHmiIdOnScreen());
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING;
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_TIME);
                    break;
                case SAMPLING_STOP_CAUSE_VOLUME:
                    BUZM_BUTTON_SOUND(); /* manual sound */
                    //MessageScreen_openInfo(DYNAMIC_STRING_INFO_SAMPLING_STOPPED_VOLUME_REACHED, HMI_GetHmiIdOnScreen());
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING;
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_VOLUME);
                    break;
                case SAMPLING_STOP_CAUSE_MANUAL:
                    /* manual sound not called, because manual stop is by pressing button */
                    //MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen());
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING;
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_BY_OPERATOR);
                    break;
                case SAMPLING_STOP_CAUSE_ERROR:
                    BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
                    //MessageScreen_openError(DYNAMIC_STRING_ERROR_SAMPLING_STOPPED_DUE_TO_ERROR, Sampling_Error, HMI_GetHmiIdOnScreen());
                    //(void)HMI_ChangeHmi(HMI_ID_MAIN, HMI_GetHmiIdOnScreen());
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR);
                    if(Environmental_ModelState != ENVIRONMENTAL_MODEL_STATE_READY)
                    {
                        Environmental_model_writeFinalData();
                    }
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_INIT; /* MODEL */
                    break;
                default:
                    break;
            }
        }
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_CheckAndManageAutoTerminationCondition %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Environmental_CheckAspirationFlowChange()
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    ENVIRONMENTAL_InterfaceData_t interfaceData;
    
    returnValue = Environmental_GetDataFromView(&interfaceData);
    
    if(STD_RETURN_OK == returnValue)
    {
        if(interfaceData.aspirationFlow != Environmental_LastAspirationFlowValue)
        {
            if(
                (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_PUMP_START) || 
                (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_RUNNING)
            )
            {
                Environmental_sendPumpSetPoint();
            }
            returnValue = ReportFile_writeAspirationFlowChanged(interfaceData.aspirationFlow, Environmental_LastAspirationFlowValue); 
            Environmental_LastAspirationFlowValue = interfaceData.aspirationFlow;
        }
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_CheckAspirationFlowChange %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Environmental_model_writeFinalData(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SAMPLING_SamplingTime_t samplingTime = {0};
    ENVIRONMENTAL_FinalData_t finalData = {0};
    float aspiratedLiters = 0.0;
    
    /* Finalize data*/
    Sampling_modelCalculateStatisticFinalize();
    
    /* Prepare final data to send to report*/
    (void)Sampling_GetSamplingTime(&samplingTime);
    
    (void)NETWORK_GetLitersTotalFromSamplingStart(&aspiratedLiters);
    
    finalData.samplingLenghtMin  = ((float)(samplingTime.counterSecond)) / 60.0f;
    finalData.averageFlow        = aspiratedLiters / finalData.samplingLenghtMin; /*Sampling_GetFinalizedFlow();*/
    finalData.averageTemperature = Sampling_GetFinalizedMeterTemperature();
    finalData.aspiratedLiters    = aspiratedLiters;
    finalData.normalizedLiters   = Sampling_getNormalizedLiters(aspiratedLiters, (float)parameter_normalizationTemperatureValue, Sampling_GetFinalizedMeterTemperature());

    returnValue = ReportFile_writeFinalDataEnvironmental(&finalData);

    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeFinalDataStopCauseAndError();
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_model_writeFinalData %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Environmental_CalculateStatisticFillData(Sampling_StatisticData_t* sd)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    float isoFlow = 0.0f;
    float actualTemperature = 0.0f;
    float actualFlow = 0.0f;
    float actualError = 0.0f;
    float actualVacuum = 0.0f;
    
    if(sd != NULL)
    {
        isoFlow = WORD_TO_FLOAT(environmentalScreen_aspirationFlow_value);
        (void)NETWORK_GetMeterTemperature(&actualTemperature);
        if(NETWORK_GetLitersMinuteRead(&actualFlow) == STD_RETURN_OK)
        {        
            if(actualFlow > 0.0f)
            {
                actualError = (((actualFlow - isoFlow) * 100.0f)/isoFlow);
            }
        }
        (void)NETWORK_GetMeterPressure(&actualVacuum);
    
        sd->ErrorFlow = actualError;
        sd->Flow = actualFlow;
        sd->MeterTemperature = actualTemperature;
        sd->Vacuum = actualVacuum; 
        returnValue = STD_RETURN_OK;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_CalculateStatisticFillData %d", returnValue);
    }
    return returnValue;
    
    return returnValue;
}

static STD_RETURN_t Environmental_modelManageWarning(void)
{
    if(Environmental_ModelState != ENVIRONMENTAL_MODEL_STATE_INIT)
    {
        (void)NETWORK_GetWarningCode(&Environmental_warningCode);
        //(void)NETWORK_GetWarningCodeAndSubCode(&Environmental_warningCode, &Environmental_warningSubCode, true);
        if(Environmental_warningCode != SYST_WARNING_NONE)
        {
            debug_print("Environmental_warningCode %d Environmental_WarningSubCode %d", Environmental_warningCode, Environmental_warningSubCode);
            FLAG_Set(FLAG_ENVIRONMENTAL_MODEL_EVENT_WARNING_DETECTED);
            FLAG_Set(FLAG_ENVIRONMENTAL_MODEL_EVENT_WARNING_DETECTED_WRITE_REPORT);
        }
        
        ubyte writeReport = FLAG_GetAndReset(FLAG_ENVIRONMENTAL_MODEL_EVENT_WARNING_DETECTED_WRITE_REPORT);
        if(
            (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_WAIT_START)      || 
            (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_PUMP_START)      || 
            (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_RUNNING)         ||
            (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_PAUSED)          ||
            (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_PAUSED_NO_POWER) ||
            (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING)
        )
        {
            if(writeReport != false)
            {
                ReportFile_writeWarning(Environmental_warningCode, Environmental_warningSubCode);
            }
            
        }
        else
        {
            if(writeReport != false)
            {
                debug_print("Environmental_warningCode consumed but not written on report");
            }
        }
    }
    else
    {
        if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_MODEL_EVENT_WARNING_DETECTED_WRITE_REPORT) != false)
        {
            debug_print("Environmental_warningCode consumed but not written on report");
        }
    }
    return STD_RETURN_OK;
}

static STD_RETURN_t Environmental_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte pumpRunning = false;
    ubyte pumpRegulation = false;
    ubyte powerPresent = false;
    static ENVIRONMENTAL_MODEL_STATE_t Environmental_LastModelState = ENVIRONMENTAL_MODEL_STATE_INIT;
    
    if(Environmental_ModelState != Environmental_LastModelState)
    {
        debug_print("ENV MOD STATE from %d %s to %d %s", Environmental_LastModelState, Environmental_modelStateNames[Environmental_LastModelState], Environmental_ModelState, Environmental_modelStateNames[Environmental_ModelState]);
        Environmental_LastModelState = Environmental_ModelState;
    }
    
    Sampling_modelCalculateStatisticNewMinuteDetect();
    
    if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_MODEL_EVENT_NEW_NETWORK_DATA_MESSAGE) != false)
    {
        /* Evey time arrive a new message with all the data */
        if(
            (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_PUMP_START   ) || 
            (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_RUNNING      ) || 
            (Environmental_ModelState == ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING)
        )
        {
            Sampling_StatisticData_t sd;
            Sampling_modelCalculateStatisticStructInit(&sd);     /* FILL ALL FIELDS WITH 0's */       
            (void)Environmental_CalculateStatisticFillData(&sd); /* FILL ONLY NECESSARY FIELDS */
            Sampling_modelCalculateStatistic(&sd);               /* EVERY TIME A MESSAGE ARRIVE */
            
            Environmental_modelCalculateStatisticRefreshCounter++;
            if(Environmental_modelCalculateStatisticRefreshCounter >= MODEL_CALCULATE_STATISTIC_REFRESH_COUNTER_VALUE)
            {
                Environmental_modelCalculateStatisticRefreshCounter = 0U;
                Environmental_modelCalculateStatisticNewDataForGraph = true;
            }
        }
    }

    (void)Environmental_modelManageWarning();

    NETWORK_GetExternalPower(&powerPresent);

    /* START CODE */
    switch(Environmental_ModelState)
    {
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE INIT                                                               */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_INIT:            
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_GO_TO_READY) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_GO_TO_READY");
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_READY;
                Environmental_warningCode = SYST_WARNING_NONE;
                Environmental_warningSubCode = 0U; 
        
                //NETWORK_ResetWarning();
                Environmental_RestoreDataForNewSampling();
                
        
                if(powerPresent == false)
                {
                    debug_print("No power supply");
                    FLAG_Set(FLAG_ENVIRONMENTAL_MODEL_EVENT_NO_POWER_SUPPLY);
                }
        
                Sampling_manageSamplingTimeInit(0);
            }
            break;
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE READY                                                              */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_READY:        
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START");
                if(powerPresent != false)
                {
                    returnValue = Environmental_modelStartFromReadyOrStopped();
                }
                else
                {
                    debug_print("No power supply");
                    FLAG_Set(FLAG_ENVIRONMENTAL_MODEL_EVENT_NO_POWER_SUPPLY);
                }
            }
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_BACK) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_BACK");
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_INIT;
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAndManageAutoTerminationCondition();
            }
            break;
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE WAIT START                                                         */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_WAIT_START:        
            Sampling_manageSamplingTime(); /* manage timer, with true should be called previously */
            
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE");
                returnValue = ReportFile_writeDelayedPaused();
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PAUSED;
            }
            
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP) != FALSE)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP");
                Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
                Sampling_Error = SAMPLING_ERROR_NONE;
                FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_STOPPED_IN_WAIT_START);
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
                ReportFile_deleteReport();
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_READY;
            }
            
            if(returnValue == STD_RETURN_OK)
            {
                ubyte conditionTime = (Sampling_manageSamplingTimeIsInWaitStart() == false) ? true : false; /* time is expired */
                ubyte forceStart = FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START); /* The user want to start immedialtly*/
                
                if(conditionTime != false)  
                {
                    BUZM_BUTTON_SOUND(); /* manual sound */
                    returnValue = ReportFile_writeStart();
                }
                if(forceStart != false) 
                {
                    debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START");
                    Sampling_manageSamplingTimeForceStart();
                    returnValue = ReportFile_writeStartForced();
                }
                if((conditionTime != false) || (forceStart != false))
                {
                    Sampling_modelInitMobileScreenPosition();
                    Environmental_modelCalculateStatisticNewDataForGraph = false;
                    Environmental_modelCalculateStatisticRefreshCounter = MODEL_CALCULATE_STATISTIC_REFRESH_COUNTER_VALUE;
                    Sampling_modelCalculateStatisticInit();
                    Environmental_sendPumpSetPoint();
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_START;
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_RUNNING);
                }
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAspirationFlowChange();
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAndManageAutoTerminationCondition();
            }
            break;
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE PUMP START                                                         */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_PUMP_START:        
            Sampling_manageSamplingTime(); /* manage timer, with true should be called previously */
            
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE");
                Environmental_sendPumpStop();
                returnValue = ReportFile_writeRunningPaused();
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PAUSED;
            }
            if(powerPresent == false)
            {
                debug_print("Event: NO POWER SUPPLY");
                Environmental_sendPumpStop();
                returnValue = ReportFile_writePausedNoPowerSupply();
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PAUSED_NO_POWER;
                FLAG_Set(FLAG_ENVIRONMENTAL_MODEL_EVENT_NO_POWER_SUPPLY_RUNNING);
            }
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP) != FALSE)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP");
                Environmental_sendPumpStop();
                Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
                Sampling_Error = SAMPLING_ERROR_NONE;
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING;
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
            }
            
            if( 
                (NETWORK_GetPumpRunning(&pumpRunning) == STD_RETURN_OK) &&
                (NETWORK_GetPumpRegulation(&pumpRegulation) == STD_RETURN_OK)
            )
            {
                if((pumpRunning != false) && (pumpRegulation == false))
                {
                    /* pump running and regulated */
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_RUNNING;
                }
            }
            
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAspirationFlowChange();
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAndManageAutoTerminationCondition();
            }
            break;
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE RUNNING                                                            */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_RUNNING:        
            Sampling_manageSamplingTime(); /* manage timer, with true should be called previously */
            
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_PAUSE");
                Environmental_sendPumpStop();
                returnValue = ReportFile_writeRunningPaused();
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PAUSED;
            }
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP) != FALSE)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP");
                Environmental_sendPumpStop();
                Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
                Sampling_Error = SAMPLING_ERROR_NONE;
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING;
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
            }
            if(powerPresent == false)
            {
                debug_print("Event: NO POWER SUPPLY");
                Environmental_sendPumpStop();
                returnValue = ReportFile_writePausedNoPowerSupply();
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PAUSED_NO_POWER;
                FLAG_Set(FLAG_ENVIRONMENTAL_MODEL_EVENT_NO_POWER_SUPPLY_RUNNING);
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAspirationFlowChange();
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAndManageAutoTerminationCondition();
            }
            break;
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE PAUSED                                                             */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_PAUSED:
            /* NO MANAGE OF NO POWER - The no power is managed in ENVIRONMENTAL_MODEL_STATE_WAIT_START and ENVIRONMENTAL_MODEL_STATE_PUMP_START, in one run they will go in  */
        
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START");
                if(Sampling_manageSamplingTimeIsInWaitStart() != false)
                {
                    returnValue = ReportFile_writeDelayedResumed();
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_WAIT_START;
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_WAITING);
                }
                else
                {
                    Environmental_sendPumpSetPoint();
                    returnValue = ReportFile_writeRunningResumed();
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_START;
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_RUNNING);
                }
            }  
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP) != FALSE)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP");
                Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
                Sampling_Error = SAMPLING_ERROR_NONE;
                //TODO maybe send the event pump stopping also if the pump maybe is stopped 
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
                if(Sampling_manageSamplingTimeIsInWaitStart() != false)
                {
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_STOPPED_IN_WAIT_START);
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_READY;
                }
                else
                {
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING;
                }
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAspirationFlowChange();
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAndManageAutoTerminationCondition();
            }
            break;
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE PAUSED NO POWER                                                    */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_PAUSED_NO_POWER:
            if(powerPresent != false)
            {
                debug_print("Event: POWER SUPPLY ON");
                Environmental_sendPumpSetPoint();
                returnValue = ReportFile_writeRestartFromNoPowerSupply();
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_START;
                FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_MODEL_RUNNING);
                FLAG_Set(FLAG_ENVIRONMENTAL_MODEL_EVENT_BACK_FROM_NO_POWER_SUPPLY_RUNNING);
            } 
             
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP) != FALSE)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_STOP");
                Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
                Sampling_Error = SAMPLING_ERROR_NONE;
                //TODO maybe send the event pump stopping also if the pump maybe is stopped 
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING;
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAndManageAutoTerminationCondition();
            }
            break;
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE PUMP STOPPING                                                      */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_PUMP_STOPPING:
            Sampling_manageSamplingTime(); /* manage timer, with true should be called previously */
        
            if( 
                (NETWORK_GetPumpRunning(&pumpRunning) == STD_RETURN_OK) &&
                (NETWORK_GetPumpRegulation(&pumpRegulation) == STD_RETURN_OK)
            )
            {
                if((pumpRunning == false) && (pumpRegulation == false))
                {
                    /* pump running and regulated */
                    Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_STOPPED;
                    
                    Environmental_model_writeFinalData();
                    
                    FLAG_Set(FLAG_ENVIRONMENTAL_SCREEN_EVENT_PUMP_STOPPED);
                }
            }
            
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAspirationFlowChange();
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAndManageAutoTerminationCondition();
            }
            break;
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE STOPPED                                                            */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_STOPPED:        
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_BACK) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_BACK");
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_INIT;
            }
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_START");
                returnValue = Environmental_modelStartFromReadyOrStopped();
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = Environmental_CheckAndManageAutoTerminationCondition();
            }
            break;
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE ERROR                                                              */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
        case ENVIRONMENTAL_MODEL_STATE_ERROR:
            if(FLAG_GetAndReset(FLAG_ENVIRONMENTAL_SCREEN_COMMAND_BACK) != false)
            {
                debug_print("Event managed: FLAG_ENVIRONMENTAL_SCREEN_COMMAND_BACK");
                Environmental_ModelState = ENVIRONMENTAL_MODEL_STATE_INIT;
            }
            break;
        default:
            break;
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef ENVIRONMENTAL_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void environmental_init()
{
    (void)Environmental_initVariable();
}

#ifdef ENVIRONMENTAL_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void environmental()
{
    static ENVIRONMENTAL_GUI_STATE_t Environmental_LastGuiState = ENVIRONMENTAL_GUI_STATE_INIT;
#ifdef ENVIRONMENTAL_ENABLE_DEBUG_COUNTERS
    Environmental_NormalRoutine++;
#endif
    if(Environmental_LastGuiState != Environmental_GuiState)
    {
        debug_print("ENV GUI STATE from %d %s to %d %s", Environmental_LastGuiState, Environmental_guiStateNames[Environmental_LastGuiState], Environmental_GuiState, Environmental_guiStateNames[Environmental_GuiState]);
        Environmental_LastGuiState = Environmental_GuiState;
    }
    (void)Environmental_manageScreenCommonEvent(Execution_Normal);
    (void)EnvironmentalSetup_manageButtons(Execution_Normal);
    (void)EnvironmentalSetup_manageScreen(Execution_Normal);
    (void)EnvironmentalData_manageButtons(Execution_Normal);
    (void)EnvironmentalData_manageScreen(Execution_Normal);
    (void)EnvironmentalGraph_manageButtons(Execution_Normal);
    (void)EnvironmentalGraph_manageScreen(Execution_Normal);
    (void)EnvironmentalWaiting_manageButtons(Execution_Normal);
    (void)EnvironmentalWaiting_manageScreen(Execution_Normal);
}

#endif /* #ifdef ENVIRONMENTAL_NORMAL_ENABLE */

#ifdef ENVIRONMENTAL_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void environmental_fast()
{
#ifdef ENVIRONMENTAL_ENABLE_DEBUG_COUNTERS
    Environmental_FastRoutine++;
#endif
}
#endif /* #ifdef ENVIRONMENTAL_FAST_ENABLE */

#ifdef ENVIRONMENTAL_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void environmental_low_priority()
{
#ifdef ENVIRONMENTAL_ENABLE_DEBUG_COUNTERS
    Environmental_LowPrioRoutine++;
#endif
}
#endif /* #ifdef ENVIRONMENTAL_LOW_PRIO_ENABLE */

#ifdef ENVIRONMENTAL_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void environmental_thread()
{
    ulong execTime = ENVIRONMENTAL_THREAD_SLEEP_MS;

    (void)Environmental_modelInit();

    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef ENVIRONMENTAL_ENABLE_DEBUG_COUNTERS
        Environmental_ThreadRoutine++; /* Only for debug */
#endif

        (void)Environmental_model(execTime);
    }
}
#endif /* #ifdef ENVIRONMENTAL_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void environmental_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void environmental_shutdown()
//{
//}
