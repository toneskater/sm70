#ifndef SERIAL_COM_H
#define SERIAL_COM_H
/*
 * serial_com.h
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"

/* Private define ------------------------------------------------------------*/
#define SERIAL_COM_NO_RESPONSE 0U

/* Private typedef -----------------------------------------------------------*/
typedef enum SERIAL_COM_RETURN_e
{
    SERIAL_COM_RETURN_OK = 0,
    SERIAL_COM_RETURN_BUSY,
    SERIAL_COM_RETURN_TIMEOUT,
    SERIAL_COM_RETURN_TX_BUSY,
    SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY,
    SERIAL_COM_RETURN_ERROR_PARAM,
    SERIAL_COM_RETURN_ERROR_AUTOMA,
    SERIAL_COM_SERIAL_PORT_NOT_OPEN,
    SERIAL_COM_RETURN_PREPARE_ERROR,
    SERIAL_COM_RETURN_VALIDATION_ERROR,
    SERIAL_COM_RETURN_VALIDATION_MESSAGE_INVALID,
    SERIAL_COM_RETURN_CALLBACK_ERROR,
    SERIAL_COM_RETURN_SERIAL_BUFFER_SIZE_WRONG,
    SERIAL_COM_RETURN_BUFFER_RX_NOT_BIG_ENOUGTH,
    SERIAL_COM_RETURN_BUFFER_TX_NOT_BIG_ENOUGTH,
}SERIAL_COM_RETURN_t;

typedef enum SERIAL_COM_STATE_e
{
    SERIAL_COM_STATE_READY = 0,
    SERIAL_COM_STATE_WAIT_RESPONSE,
    SERIAL_COM_STATE_ERROR,
    SERIAL_COM_STATE_MAX_NUMBER
}SERIAL_COM_STATE_t;

/* PTR source message, source message length, PTR dest buffer, PTR dest buffer length, dest buffer max size */
typedef SERIAL_COM_RETURN_t (*SerialCom_txPrepareFct )(ubyte*, ulong, ubyte*, ulong*, ulong);
typedef SERIAL_COM_RETURN_t (*SerialCom_rxValidateFct)(ubyte*, ulong*);
typedef SERIAL_COM_RETURN_t (*SerialCom_rxCallbackFct)(ubyte*, ulong);
typedef SERIAL_COM_RETURN_t (*SerialCom_txCallbackFct)(ubyte*, ulong);
typedef SERIAL_COM_RETURN_t (*SerialCom_errorCallbackFct)(ubyte, ubyte);

typedef struct SERIAL_COM_ISOCHECK_s
{
    /* configuration */
    slong idComm; /* !!!!!!!!!SIGNED!!!!!!!*/
    ulong timeoutResponseMs;
    ulong maxNumberOfTimeout;
    ulong maxTryTxCommand;
    
    SerialCom_rxCallbackFct rxCallbackFct;
    
    ubyte* messageTxBufferPtr;
    ulong messageTxLength;
    ulong messageTxBufferSize;
    ubyte* messageRxBufferPtr;
    ulong messageRxLength;
    ulong messageRxBufferSize;
    ulong executionTime; 
	ulong timerSilenceMs;
    
    /* Internal variables */
    ubyte messageArrivedFlag; /* false */
    SERIAL_COM_STATE_t state; /* TEST_SEND_MESSAGE_STATE_READY */
    ulong retryTx; /* 0 */
    ulong timeoutNumberCounter; /* 0 */
    ulong timeoutMsCounter; /* 0 */
	ulong integrityFaultCounter; /* 0 */
	ubyte firstFrameReceived; /* false */
	ubyte validConfiguration; /* false */
	ulong rxCompleteCounter; /* 0 */
	ulong txDone; /* false */
}SERIAL_COM_ISOCHECK_t;

typedef struct SERIAL_COM_MODBUS_s
{
    /* configuration */
    slong idComm; /* !!!!!!!!!SIGNED!!!!!!!*/
    ulong timeoutResponseMs;
    ulong maxNumberOfTimeout;
    ulong maxTryTxCommand;
    
    SerialCom_txCallbackFct txCallbackFct;
    SerialCom_rxCallbackFct rxCallbackFct;
    
    ubyte* messageTxBufferPtr;
    ulong messageTxLength;
    ulong messageTxBufferSize;
    ubyte* messageRxBufferPtr;
    ulong messageRxLength;
    ulong messageRxBufferSize;
    ulong executionTime; 
	ulong timerSilenceMs;
    
    /* Internal variables */
    ubyte messageArrivedFlag; /* false */
    SERIAL_COM_STATE_t state; /* TEST_SEND_MESSAGE_STATE_READY */
    ulong retryTx; /* 0 */
    ulong timeoutNumberCounter; /* 0 */
    ulong timeoutMsCounter; /* 0 */
	ulong checksumFaultCounter; /* 0 */
	ulong txMessageCounter; /* 0 */
	ulong rxMessageCounter; /* 0 */
	ulong rxLengthFaultCounter; /* 0 */
	ulong txBusy;	 /* 0 */
    ulong rxDiscardedDueToTimeout; /* 0 */
	ulong rxCompleteCounter; /* 0 */
	ubyte firstFrameReceived; /* false */
	ubyte validConfiguration; /* false */
    ubyte inTimeout; /* false */
}SERIAL_COM_MODBUS_t;

typedef struct SERIAL_COM_s
{
    /* configuration */
    slong idComm; /* !!!!!!!!!SIGNED!!!!!!!*/
    ulong timeoutResponseMs;
    ulong maxNumberOfTimeout;
    ulong maxTryTxCommand;
    
    SerialCom_txPrepareFct txPrepareFct;
    SerialCom_txCallbackFct txCallbackFct;
    SerialCom_rxValidateFct rxValidateFct;
    SerialCom_rxCallbackFct rxCallbackFct;
    
    ubyte* messageTxBufferPtr;
    ulong messageTxLength;
    ulong messageTxBufferSize;
    ubyte* messageRxBufferPtr;
    ulong messageRxLength;
    ulong messageRxBufferSize;
    ulong executionTime; 
    
    /* Internal variables */
    ubyte messageArrivedFlag; /* false */
    ulong responseLength;
    SERIAL_COM_STATE_t state; /* TEST_SEND_MESSAGE_STATE_READY */
    ulong retryTx; /* 0 */
    ulong timeoutNumberCounter; /* 0 */
    ulong timeoutMsCounter; /* 0 */
    ubyte validConfigurationTx;
    ubyte validConfigurationRx;
}SERIAL_COM_t;

typedef struct SERIAL_COM_XBEE_PROTOCOL_s
{
    /* configuration */
    slong idComm; /* !!!!!!!!!SIGNED!!!!!!!*/
    ulong timeoutResponseMs;
    ulong maxNumberOfTimeout;
    ulong maxTryTxCommand;
    
    SerialCom_txCallbackFct txCallbackFct;
    SerialCom_rxCallbackFct rxCallbackFct;
    SerialCom_errorCallbackFct errorCallbackFct;
    
    ubyte* messageTxBufferPtr;
    ulong messageTxLength;
    ulong messageTxBufferSize;
    ubyte* messageRxBufferPtr;
    ulong messageRxLength;
    ulong messageRxBufferSize;
    ulong executionTime; 
    
    /* Internal variables */
    ubyte messageArrivedFlag; /* false */
    ubyte responseToWait;
    SERIAL_COM_STATE_t state; /* TEST_SEND_MESSAGE_STATE_READY */
    ulong retryTx; /* 0 */
    ulong timeoutNumberCounter; /* 0 */
    ulong timeoutMsCounter; /* 0 */
    ubyte validConfigurationTx;
    ubyte validConfigurationRx;
    ulong lengthFaultCounter;
    ulong checksumFaultCounter;
    ubyte errorDetected;
    ulong errorDetectedLength;
    ubyte messageStarted;
    ubyte escapeCharacterDetected;
    ulong nextByteOfMessage;
    ubyte checksum;
}SERIAL_COM_XBEE_PROTOCOL_t;

#define XBEM_START_CHARACTER 0x7E
#define XBEM_ESCAPE_CHARACTER 0x7D
#define XBEM_ESCAPE_XOR_CHARACTER 0x20
#define XBEM_SPECIAL_CHARACTER_0x11 0x11
#define XBEM_SPECIAL_CHARACTER_0x13 0x13

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
SERIAL_COM_RETURN_t SerialCom_init(SERIAL_COM_t* serial, ulong _timeoutResponseMs, ulong _maxNumberOfTimeout, ulong _maxTryTxCommand, SerialCom_txPrepareFct _txPrepareFct, SerialCom_txCallbackFct _txCallbackFct, SerialCom_rxValidateFct _rxValidateFct, SerialCom_rxCallbackFct _rxCallbackFct, ubyte* _messageTxBufferPtr, ulong _messageTxBufferSize, ubyte* _messageRxBufferPtr, ulong _messageRxBufferSize, ulong _executionTime);
SERIAL_COM_RETURN_t SerialCom_sendMessage(SERIAL_COM_t* serial, ubyte* message, ulong messageLength, ubyte _responseLength);
SERIAL_COM_RETURN_t SerialCom_receive(SERIAL_COM_t* serial);
SERIAL_COM_RETURN_t SerialCom_close(SERIAL_COM_t* serial);
SERIAL_COM_RETURN_t SerialCom_open(SERIAL_COM_t* serial, _SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stop);


SERIAL_COM_RETURN_t SerialCom_initModbus(SERIAL_COM_MODBUS_t* _serial, ulong _timeoutResponseMs, ulong _maxNumberOfTimeout, ulong _maxTryTxCommand, SerialCom_txCallbackFct _txCallbackFct, SerialCom_rxCallbackFct _rxCallbackFct, ubyte* _messageTxBufferPtr, ulong _messageTxBufferSize, ubyte* _messageRxBufferPtr, ulong _messageRxBufferSize, ulong _executionTime, ulong _timerSilence);
SERIAL_COM_RETURN_t SerialCom_receiveModbus(SERIAL_COM_MODBUS_t* serial);
SERIAL_COM_RETURN_t SerialCom_closeModbus(SERIAL_COM_MODBUS_t* serial);
SERIAL_COM_RETURN_t SerialCom_openModbus(SERIAL_COM_MODBUS_t* serial, _SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stop);
SERIAL_COM_RETURN_t SerialCom_sendMessageModbus(SERIAL_COM_MODBUS_t* serial, ubyte* message, ulong messageLength);

SERIAL_COM_RETURN_t SerialCom_initXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial, ulong _timeoutResponseMs, ulong _maxNumberOfTimeout, ulong _maxTryTxCommand, SerialCom_txCallbackFct _txCallbackFct, SerialCom_rxCallbackFct _rxCallbackFct, SerialCom_errorCallbackFct _errorCallbackFct, ubyte* _messageTxBufferPtr, ulong _messageTxBufferSize, ubyte* _messageRxBufferPtr, ulong _messageRxBufferSize, ulong _executionTime);
SERIAL_COM_RETURN_t SerialCom_openXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial, _SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stop);
SERIAL_COM_RETURN_t SerialCom_closeXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial);
SERIAL_COM_RETURN_t SerialCom_receiveXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial);
SERIAL_COM_RETURN_t SerialCom_sendMessageXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial, ubyte* message, ulong messageLength, ubyte _responseToWait);
SERIAL_COM_RETURN_t SerialCom_SetTimeoutModbus(SERIAL_COM_MODBUS_t* serial);

SERIAL_COM_RETURN_t SerialCom_initIsocheck(
    SERIAL_COM_ISOCHECK_t* _serial, 
    ulong _timeoutResponseMs, 
    ulong _maxNumberOfTimeout, 
    ulong _maxTryTxCommand, 
    SerialCom_rxCallbackFct _rxCallbackFct, 
    ubyte* _messageTxBufferPtr, 
    ulong _messageTxBufferSize, 
    ubyte* _messageRxBufferPtr, 
    ulong _messageRxBufferSize, 
    ulong _executionTime, 
    ulong _timerSilence
);
SERIAL_COM_RETURN_t SerialCom_receiveIsocheck(SERIAL_COM_ISOCHECK_t* serial);
SERIAL_COM_RETURN_t SerialCom_closeIsocheck(SERIAL_COM_ISOCHECK_t* serial);
SERIAL_COM_RETURN_t SerialCom_openIsocheck(SERIAL_COM_ISOCHECK_t* serial, _SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stop);
SERIAL_COM_RETURN_t SerialCom_sendMessageIsocheck(SERIAL_COM_ISOCHECK_t* serial, ubyte* message, ulong messageLength);

#endif // SERIAL_COM_H 
