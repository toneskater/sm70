/*
 * client.c
 *
 *  Created on: 06/11/2019
 *      Author: Andrea Tonello
 */
#define CLIENT_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "client_file.h"
#include "message_screen.h"
#include "logger.h"

/* Private typedef -----------------------------------------------------------*/
typedef enum CLIENT_FILE_SCREEN_TOP_TEXT_e
{
    CLIENT_FILE_SCREEN_TOP_TEXT_EMPTY = 0,
    CLIENT_FILE_SCREEN_TOP_TEXT_NO_CLIENT,
    CLIENT_FILE_SCREEN_TOP_TEXT_NEW_CLIENT,
    CLIENT_FILE_SCREEN_TOP_TEXT_EDIT_CLIENT,
    CLIENT_FILE_SCREEN_TOP_TEXT_MAX_NUMBER
}CLIENT_FILE_SCREEN_TOP_TEXT_t;

/* Private define ------------------------------------------------------------*/
#define CLIENT_FILE_SCREEN_TOP_TEXT_VISIBLE_TIMER_MS        1000U
#define CLIENT_FILE_SCREEN_TOP_TEXT_NOT_VISIBLE_TIMER_MS    500U

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static Timer_t ClientFile_TopTextTimer;
static ubyte ClientFile_clientNameBlinkState;

static ubyte ClientFile_PreviousSelectedOneForNewOperation = 0;
static ubyte ClientFile_NewOperationOnGoing = false;
static ubyte ClientFile_EditOperationOnGoing = false;

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t ClientFile_logFlag(FLAG_t flag);
static STD_RETURN_t ClientFile_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t ClientFile_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t ClientFile_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t ClientFile_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static void ClientFile_InitVariablesClient(void);
static void ClientFile_ManageClientButtons(ulong execTimeMs);
static void ClientFile_ManageClientScreen(ulong execTimeMs);

static void ClientFile_ManageNewButton(void);
static void ClientFile_ManageClientScreenNextAndPreviousButton(void);
static void ClientFile_ClearClientFields(void);
static STD_RETURN_t ClientFile_ShowClientFields(ubyte clientNumber);
static void ClientFile_ManageTopTextString(void);

static void ClientFile_model_init(void);
static void ClientFile_model(ulong execTimeMs);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t ClientFile_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t ClientFile_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t ClientFile_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t ClientFile_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t ClientFile_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static void ClientFile_ManageClientButtons(ulong execTimeMs)
{
    if(clientScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logFlag(FLAG_CLIENT_SCREEN_BUTTON_BACK);
    }
    if(clientScreen_previousButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logFlag(FLAG_CLIENT_SCREEN_BUTTON_PREVIOUS);
    }
    if(clientScreen_nextButton_released != false) /* These flag is reset by GUI */
    { 
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logFlag(FLAG_CLIENT_SCREEN_BUTTON_NEXT);
    }
    if(clientScreen_deleteButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logFlag(FLAG_CLIENT_SCREEN_BUTTON_DELETE);
    }
    if(clientScreen_saveButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logFlag(FLAG_CLIENT_SCREEN_BUTTON_SAVE);
    }
    if(clientScreen_editButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logFlag(FLAG_CLIENT_SCREEN_BUTTON_EDIT); 
    }
    if(clientScreen_newButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logFlag(FLAG_CLIENT_SCREEN_BUTTON_NEW);
    }
    if(clientScreen_name_eventAccepted != false)
    {
        clientScreen_name_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logString("Client Name", (ushort*)&clientScreen_name_1, 40U);
    }
    if(clientScreen_address_eventAccepted != false)
    {
        clientScreen_address_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logString("Client Address", (ushort*)&clientScreen_address_1, 40U);
    }
    if(clientScreen_cap_eventAccepted != false)
    {
        clientScreen_cap_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logString("Client CAP", (ushort*)&clientScreen_CAP_1, 40U);
    }
    if(clientScreen_city_eventAccepted != false)
    {
        clientScreen_city_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logString("Client City", (ushort*)&clientScreen_city_1, 40U);
    }
    if(clientScreen_district_eventAccepted != false)
    {
        clientScreen_district_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logString("Client District", (ushort*)&clientScreen_district_1, 40U);
    }
    if(clientScreen_plantNumber_eventAccepted != false)
    {
        clientScreen_plantNumber_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logString("Client Plant Number", (ushort*)&clientScreen_plantNumber_1, 40U);
    }
    if(clientScreen_note_eventAccepted != false)
    {
        clientScreen_note_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)ClientFile_logString("Client Note", (ushort*)&clientScreen_note_1, 40U);
    }
}

static void ClientFile_ManageClientScreen(ulong execTimeMs)
{
    const HMI_ID_t localHmiId = HMI_ID_CLIENT;
    
    ubyte isRunning = false;
    ubyte isExpired = false;
    
    (void) UTIL_TimerIncrement(&ClientFile_TopTextTimer, execTimeMs);
    
    (void) UTIL_TimerIsExpired(&ClientFile_TopTextTimer, &isExpired);
    if(isExpired != false)
    {
        (void) UTIL_TimerStop(&ClientFile_TopTextTimer);
        if(ClientFile_clientNameBlinkState != false)
        {
            ClientFile_clientNameBlinkState = false;
            /* Now set not visible */
            clientScreen_topText = CLIENT_FILE_SCREEN_TOP_TEXT_EMPTY;
            (void) UTIL_TimerStart(&ClientFile_TopTextTimer, CLIENT_FILE_SCREEN_TOP_TEXT_NOT_VISIBLE_TIMER_MS);
        }
        else
        {
            ClientFile_clientNameBlinkState = true;
            /* Now set visible */
            clientScreen_topText = CLIENT_FILE_SCREEN_TOP_TEXT_NO_CLIENT;
            (void) UTIL_TimerStart(&ClientFile_TopTextTimer, CLIENT_FILE_SCREEN_TOP_TEXT_VISIBLE_TIMER_MS);
        }
    }
    
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        /* On enter screen */
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
		{
            ; /* Nothing */
		}
		else
		{
			ClientFile_ClearClientFields();
			ClientFile_InitVariablesClient();
			clientScreen_topText = CLIENT_FILE_SCREEN_TOP_TEXT_EMPTY;
			/* Check clients file exists */
			FLAG_Set(FLAG_COMMAND_OPEN_CLIENT_FILE); /* Send the command to open the client file in the thread */
			clientScreen_enableClientFields = false;
			(void) UTIL_TimerIsRunning(&ClientFile_TopTextTimer, &isRunning);
			if(isRunning != false)
			{
				/* Already Started, stop */
				(void) UTIL_TimerStop(&ClientFile_TopTextTimer);
			}
		}
    }
    
    if(HMI_GetHmiIdOnScreen() == localHmiId)
    {
        ubyte clientSelectedString[10U] = {0x00U};
        ushort selectedOneTemp = Client_SelectedOne;
        if(Client_ClientsTotalNumber > 0U)
        {
            selectedOneTemp += 1;
        }
        string_snprintf(&clientSelectedString[0], sizeof(clientSelectedString), "%d / %d", selectedOneTemp, Client_ClientsTotalNumber);
        UTIL_BufferFromSbyteToUShort(&clientSelectedString[0], sizeof(clientSelectedString), &clientScreen_clientSelected_1, 10U);
    }
    
    /* *********************************************************************/
    if(FLAG_Get(FLAG_EVENT_CLIENT_FILE_OPENED) != FALSE)
    {
        debug_print("FLAG_EVENT_CLIENT_FILE_OPENED");
    }
    if(FLAG_Get(FLAG_EVENT_CLIENT_FILE_SAVED) != FALSE)
    {
        debug_print("FLAG_EVENT_CLIENT_FILE_SAVED");
    }
    if(
        (FLAG_GetAndReset(FLAG_EVENT_CLIENT_FILE_OPENED) != FALSE) ||
        (FLAG_GetAndReset(FLAG_EVENT_CLIENT_FILE_SAVED) != FALSE)
    )
    {                
        ClientFile_ClearClientFields();
        
        if(Client_ClientsTotalNumber > 0)
        {
            Client_SelectedOne = clientScreen_selectedClientRetention;
            ClientFile_ShowClientFields(Client_SelectedOne);
        }
        
        /* Only when the file is opened and hopefully loaded, check */
        if(Client_ClientsTotalNumber > 0U)
        {
            /* There are clients */
            /* delete button enabled */
            clientScreen_deleteButton_enabled = true;
            /* save button disabled */
            clientScreen_saveButton_enabled = false;
            /* edit button enabled */
            clientScreen_editButton_enabled = true;
        }
        else
        {
            /* delete button disable */
            clientScreen_deleteButton_enabled = false;
            /* save button disabled */
            clientScreen_saveButton_enabled = false;
            /* edit button disabled */
            clientScreen_editButton_enabled = false;
        } 
        
        ClientFile_ManageTopTextString();
        ClientFile_ManageNewButton();
        ClientFile_ManageClientScreenNextAndPreviousButton();
    }
       
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_NEXT) != FALSE)
    {
        debug_print("FLAG_CLIENT_SCREEN_BUTTON_NEXT");
        if(Client_SelectedOne < (Client_ClientsTotalNumber - 1U))
        {
            Client_SelectedOne++;
            clientScreen_selectedClientRetention = Client_SelectedOne;
            ClientFile_ClearClientFields();
            ClientFile_ShowClientFields(Client_SelectedOne);
        }
        
        ClientFile_ManageClientScreenNextAndPreviousButton();
    }   
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_PREVIOUS) != FALSE)
    {
        debug_print("FLAG_CLIENT_SCREEN_BUTTON_PREVIOUS");
        if(Client_SelectedOne > 0U)
        {
            Client_SelectedOne--;
            clientScreen_selectedClientRetention = Client_SelectedOne;
            ClientFile_ClearClientFields();
            ClientFile_ShowClientFields(Client_SelectedOne);
        }
        
        ClientFile_ManageClientScreenNextAndPreviousButton();
    }
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_NEW) != FALSE)
    {
        debug_print("FLAG_CLIENT_SCREEN_BUTTON_NEW");
        
        (void) UTIL_TimerIsRunning(&ClientFile_TopTextTimer, &isRunning);
        if(isRunning != false)
        {
            /* Already Started, stop */
            (void) UTIL_TimerStop(&ClientFile_TopTextTimer);
        }
        
        /* enable save button */
        clientScreen_saveButton_enabled = true;
        /* enable clients fields */
        clientScreen_enableClientFields = true;
        /* new button disabled, i'm adding a new clients  */
        clientScreen_newButton_enabled = false;
        /* edit button disabled */
        clientScreen_editButton_enabled = false;
        /* block previus and next button */
        clientScreen_previousButton_enabled = false;
        clientScreen_nextButton_enabled = false;
        /* clear client fields in order to write down new stuff */
        ClientFile_ClearClientFields();
        /* In case I don't finalize with save operation */
        ClientFile_PreviousSelectedOneForNewOperation = Client_SelectedOne;
        ClientFile_NewOperationOnGoing = true;
        
        Client_SelectedOne = Client_ClientsTotalNumber;
        clientScreen_selectedClientRetention = Client_SelectedOne;
        
        Client_ClientsTotalNumber++;
        
        /* enable also the delete button in order to delete before saving */
        clientScreen_deleteButton_enabled = true;
        
        clientScreen_topText = CLIENT_FILE_SCREEN_TOP_TEXT_NEW_CLIENT;
    }
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_DELETE) != FALSE)
    {
        if(ClientFile_EditOperationOnGoing != false)
        {
            debug_print("FLAG_CLIENT_SCREEN_BUTTON_DELETE EDIT ONGOING");
            
            ClientFile_EditOperationOnGoing = false;
            clientScreen_editButton_enabled = true;
            /* Re-show the non edit fields */
            ClientFile_ShowClientFields(Client_SelectedOne);
        }
        else
        {
            if(ClientFile_NewOperationOnGoing != false)
            {
                debug_print("FLAG_CLIENT_SCREEN_BUTTON_DELETE NEW ONGOING");
                /* nothing finalized, show the last shown client */
                Client_ClientsTotalNumber--;
                Client_SelectedOne = ClientFile_PreviousSelectedOneForNewOperation;
                clientScreen_selectedClientRetention = Client_SelectedOne;
                
                if(Client_ClientsTotalNumber > 0)
                {
                    clientScreen_deleteButton_enabled = true;
                    clientScreen_editButton_enabled = true;
                }
                else
                {
                    clientScreen_deleteButton_enabled = false;
                    clientScreen_editButton_enabled = false;
                }
                
                ClientFile_NewOperationOnGoing = false;
                
                ClientFile_ShowClientFields(Client_SelectedOne);
            }
            else
            {
                debug_print("FLAG_CLIENT_SCREEN_BUTTON_DELETE");
                /* Delete from array and then save the file */
                int i = 0;
                int traslated = false;
                
                /* Set client entry as total empy writing 0 everywhere */
                memory_set((ubyte*)&ClientsFile_Clients[Client_SelectedOne], 0x00U, sizeof(ClientFile_Client_t));
                /* if there are clients after, traslate the next clients */
                for(i = Client_SelectedOne; i < (Client_ClientsTotalNumber - 1U); i++)
                {
                    memory_copy((ubyte*)&ClientsFile_Clients[i], (ubyte*)&ClientsFile_Clients[i + 1U], sizeof(ClientFile_Client_t));
                    traslated = true;
                }
                /* If I have moved the clients, remove the latest one */
                if(traslated != false)
                {
                    memory_set((ubyte*)&ClientsFile_Clients[i], 0x00U, sizeof(ClientFile_Client_t));
                }
                
                /* Manage the selectedOne */
                if(Client_SelectedOne == (Client_ClientsTotalNumber - 1U))
                {
                    /* If i had selected the last one */
                    if(Client_ClientsTotalNumber > 1U)
                    {
                        /* And there were at least two clients */
                        Client_SelectedOne--;
                    }
                }
                clientScreen_selectedClientRetention = Client_SelectedOne;
                
                Client_ClientsTotalNumber--;
                
                FLAG_Set(FLAG_COMMAND_SAVE_CLIENT_FILE);
            }
        }
        /* disable clients fields */
        clientScreen_enableClientFields = false;
        /* disable save button */
        clientScreen_saveButton_enabled = false;
        
        ClientFile_ManageTopTextString();
        ClientFile_ManageNewButton();
        ClientFile_ManageClientScreenNextAndPreviousButton();
    }
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_EDIT) != FALSE)
    {
        debug_print("FLAG_CLIENT_SCREEN_BUTTON_EDIT");
        /* enable save button */
        clientScreen_saveButton_enabled = true;
        /* enable clients fields */
        clientScreen_enableClientFields = true;
        /* new button disabled, i'm adding a new clients  */
        clientScreen_newButton_enabled = false;
        /* block previus and next button */
        clientScreen_previousButton_enabled = false;
        clientScreen_nextButton_enabled = false;
        clientScreen_editButton_enabled = false;
        ClientFile_EditOperationOnGoing = true;
        
        clientScreen_topText = CLIENT_FILE_SCREEN_TOP_TEXT_EDIT_CLIENT;
    }
   
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_SAVE) != FALSE)
    {        
        if(
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_name_1       , CLIENT_FILE_CLIENT_NAME_LENTGH           ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_address_1    , CLIENT_FILE_CLIENT_ADDRESS_LENTGH        ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_CAP_1        , CLIENT_FILE_CLIENT_CAP_LENTGH            ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_city_1       , CLIENT_FILE_CLIENT_CITY_LENTGH           ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_district_1   , CLIENT_FILE_CLIENT_DISTRICT_LENTGH       ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_plantNumber_1, CLIENT_FILE_CLIENT_PLANT_NUMBER_LENTGH   ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_note_1       , CLIENT_FILE_CLIENT_NOTE_LENTGH           ) > 0U)
        )
        {
            debug_print("FLAG_CLIENT_SCREEN_BUTTON_SAVE");
            /* If at least one of the fields are written, save */
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_name_1         , CLIENT_FILE_CLIENT_NAME_LENTGH           , &ClientsFile_Clients[Client_SelectedOne].name[0]          , CLIENT_FILE_CLIENT_NAME_LENTGH);      
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_address_1      , CLIENT_FILE_CLIENT_ADDRESS_LENTGH        , &ClientsFile_Clients[Client_SelectedOne].address[0]       , CLIENT_FILE_CLIENT_ADDRESS_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_CAP_1          , CLIENT_FILE_CLIENT_CAP_LENTGH            , &ClientsFile_Clients[Client_SelectedOne].CAP[0]           , CLIENT_FILE_CLIENT_CAP_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_city_1         , CLIENT_FILE_CLIENT_CITY_LENTGH           , &ClientsFile_Clients[Client_SelectedOne].city[0]          , CLIENT_FILE_CLIENT_CITY_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_district_1     , CLIENT_FILE_CLIENT_DISTRICT_LENTGH       , &ClientsFile_Clients[Client_SelectedOne].district[0]      , CLIENT_FILE_CLIENT_DISTRICT_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_plantNumber_1  , CLIENT_FILE_CLIENT_PLANT_NUMBER_LENTGH   , &ClientsFile_Clients[Client_SelectedOne].plantNumber[0]   , CLIENT_FILE_CLIENT_PLANT_NUMBER_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_note_1         , CLIENT_FILE_CLIENT_NOTE_LENTGH           , &ClientsFile_Clients[Client_SelectedOne].note[0]          , CLIENT_FILE_CLIENT_NOTE_LENTGH);
         
            FLAG_Set(FLAG_COMMAND_SAVE_CLIENT_FILE);
        
            /* disable save button */
            clientScreen_saveButton_enabled = false;
            /* disable clients fields */
            clientScreen_enableClientFields = false;
            /* new button disabled, i'm adding a new clients  */
            clientScreen_newButton_enabled = true;
            
            ClientFile_EditOperationOnGoing = false;
            ClientFile_NewOperationOnGoing = false;
        }
        else
        {
            /* No fields written discard */
            debug_print("FLAG_CLIENT_SCREEN_BUTTON_SAVE No fields written discard");
            /* Manage making the discard */
            
            /* disable clients fields */
            clientScreen_enableClientFields = false;
            clientScreen_deleteButton_enabled = false;
            
            if(
                (ClientFile_NewOperationOnGoing != false) || 
                (ClientFile_EditOperationOnGoing != false)
            )
            {
                /* also if i'm editing and all the fields during editing are set to empy, discard. Maybe I could delete the entry */
                FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_DELETE);
            }
        }
    }
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_BACK) != FALSE)
    {
        debug_print("Event managed: FLAG_CLIENT_SCREEN_BUTTON_BACK");
        (void)HMI_ChangeHmi(HMI_ID_MAIN, localHmiId);
    }
    
    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        /* On exit screen */
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* Nothing */
        }
        else
        {
			ClientFile_ClearClientFields();
			
			/* Same code of delete button when new operation is on goind */
			Client_ClientsTotalNumber--;
			if(ClientFile_NewOperationOnGoing != false)
			{
				Client_SelectedOne = ClientFile_PreviousSelectedOneForNewOperation;
				clientScreen_selectedClientRetention = Client_SelectedOne;
				ClientFile_NewOperationOnGoing = false;
			}
			if(ClientFile_EditOperationOnGoing != false)
			{
				ClientFile_EditOperationOnGoing = false;
			}
			clientScreen_enableClientFields = false;
			clientScreen_topText = CLIENT_FILE_SCREEN_TOP_TEXT_EMPTY;
			(void) UTIL_TimerStop(&ClientFile_TopTextTimer);
        }
    }
}

static STD_RETURN_t ClientFile_ShowClientFields(ubyte clientNumber)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((clientNumber >= 0) && (clientNumber < CLIENT_FILE_CLIENTS_MAX_NUMBER))
    {
        (void)UTIL_BufferFromSbyteToUShort(&ClientsFile_Clients[clientNumber].name[0]          , CLIENT_FILE_CLIENT_NAME_LENTGH           , &clientScreen_name_1         , CLIENT_FILE_CLIENT_NAME_LENTGH);      
        (void)UTIL_BufferFromSbyteToUShort(&ClientsFile_Clients[clientNumber].address[0]       , CLIENT_FILE_CLIENT_ADDRESS_LENTGH        , &clientScreen_address_1      , CLIENT_FILE_CLIENT_ADDRESS_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&ClientsFile_Clients[clientNumber].CAP[0]           , CLIENT_FILE_CLIENT_CAP_LENTGH            , &clientScreen_CAP_1          , CLIENT_FILE_CLIENT_CAP_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&ClientsFile_Clients[clientNumber].city[0]          , CLIENT_FILE_CLIENT_CITY_LENTGH           , &clientScreen_city_1         , CLIENT_FILE_CLIENT_CITY_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&ClientsFile_Clients[clientNumber].district[0]      , CLIENT_FILE_CLIENT_DISTRICT_LENTGH       , &clientScreen_district_1     , CLIENT_FILE_CLIENT_DISTRICT_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&ClientsFile_Clients[clientNumber].plantNumber[0]   , CLIENT_FILE_CLIENT_PLANT_NUMBER_LENTGH   , &clientScreen_plantNumber_1  , CLIENT_FILE_CLIENT_PLANT_NUMBER_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&ClientsFile_Clients[clientNumber].note[0]          , CLIENT_FILE_CLIENT_NOTE_LENTGH           , &clientScreen_note_1         , CLIENT_FILE_CLIENT_NOTE_LENTGH);
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

static void ClientFile_ManageTopTextString(void)
{
    (void) UTIL_TimerStop(&ClientFile_TopTextTimer);
    if(Client_ClientsTotalNumber > 0U)
    {
        clientScreen_topText = CLIENT_FILE_SCREEN_TOP_TEXT_EMPTY;
    }
    else
    {
        clientScreen_topText = CLIENT_FILE_SCREEN_TOP_TEXT_NO_CLIENT;
        (void) UTIL_TimerStart(&ClientFile_TopTextTimer, CLIENT_FILE_SCREEN_TOP_TEXT_VISIBLE_TIMER_MS);
    }  
}

static void ClientFile_ManageNewButton(void)
{
    if(Client_ClientsTotalNumber < CLIENT_FILE_CLIENTS_MAX_NUMBER)
    {
        /* new button enabled */
        clientScreen_newButton_enabled = true;
    }
    else
    {
         /* new button disabled */
        clientScreen_newButton_enabled = false;
    } 
}

static void ClientFile_ManageClientScreenNextAndPreviousButton(void)
{
    if(Client_ClientsTotalNumber == 0)
    {
        /* No clients */
        clientScreen_previousButton_enabled = false;
        clientScreen_nextButton_enabled = false;
    }
    else
    {
        if(Client_SelectedOne == 0)
        {
            clientScreen_previousButton_enabled = false; 
        }
        else
        {
            clientScreen_previousButton_enabled = true;
        }

        if(Client_SelectedOne == (Client_ClientsTotalNumber - 1U))
        {
            clientScreen_nextButton_enabled = false;
        }
        else
        {
            clientScreen_nextButton_enabled = true;
        }
    }
}

static void ClientFile_ClearClientFields(void)
{
    /* clear the clients fields */
    clientScreen_name_1 = '\0';
    clientScreen_address_1 = '\0';
    clientScreen_CAP_1 = '\0';
    clientScreen_city_1 = '\0';
    clientScreen_district_1 = '\0';
    clientScreen_plantNumber_1 = '\0';
    clientScreen_note_1 = '\0';
} 

static void ClientFile_model_init(void)
{

}

static void ClientFile_model(ulong execTimeMs)
{
    STD_RETURN_t threadReturn;
    if(FLAG_GetAndReset(FLAG_COMMAND_OPEN_CLIENT_FILE) != FALSE)
    {
        debug_print("FLAG_COMMAND_OPEN_CLIENT_FILE");
        threadReturn = ClientFile_OpenAndReadOrCreateFile((sbyte*)&ClientFile_FileName[0], ClientFile_clientLocationSave, (sbyte*)&ClientFile_FileHeader[0], CLIENT_FILE_CLIENTS_MAX_NUMBER);
        switch(threadReturn)
        {
            case STD_RETURN_OK:
               FLAG_Set(FLAG_EVENT_CLIENT_FILE_OPENED);
               break;
            default:
                debug_print("ERROR: %d", threadReturn);
                break;
        }
    }
    
    if(FLAG_GetAndReset(FLAG_COMMAND_SAVE_CLIENT_FILE) != FALSE)
    {
        debug_print("FLAG_COMMAND_SAVE_CLIENT_FILE");
        threadReturn = ClientFile_SaveFile((sbyte*)&ClientFile_FileName[0], ClientFile_clientLocationSave, CLIENT_FILE_CLIENTS_MAX_NUMBER);
        switch(threadReturn)
        {
            case STD_RETURN_OK:
               FLAG_Set(FLAG_EVENT_CLIENT_FILE_SAVED);
               break;
            default:
                debug_print("ERROR: %d", threadReturn);
                break;
        }
    }
}

static void ClientFile_InitVariablesClient(void)
{
    ClientFile_PreviousSelectedOneForNewOperation = 0;
    ClientFile_NewOperationOnGoing = false;
    ClientFile_EditOperationOnGoing = false;
    clientScreen_topText = CLIENT_FILE_SCREEN_TOP_TEXT_EMPTY;

    ClientFile_clientNameBlinkState = false;
    UTIL_TimerInit(&ClientFile_TopTextTimer);
    
    ClientFile_clientLocationSave = CLIENT_FILE_LOCATION_DEFAULT_SAVE;
    ClientFile_clientLocationExport = CLIENT_FILE_LOCATION_DEFAULT_EXPORT;
}

/* Public functions ----------------------------------------------------------*/

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void client_init()
{
    ClientFile_InitVariablesClient();
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void client()
{
    Client_NormalRoutine++;
    ClientFile_ManageClientButtons(Execution_Normal);
    ClientFile_ManageClientScreen(Execution_Normal);
}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void client_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void client_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void client_thread()
{
    float execTime = CLIENT_THREAD_SLEEP_MS;
    
    ClientFile_model_init();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
        Client_ThreadRoutine++; /* Only for debug */
        
        ClientFile_model(execTime);
    }
}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void client_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void client_shutdown()
//{
//}
