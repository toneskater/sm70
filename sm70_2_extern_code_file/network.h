#ifndef NETWORK_H
#define NETWORK_H
/*
 * network.h
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"

/* Private define ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/
typedef enum NETWORK_STATE_e
{
	NETWORK_STATE_INIT = 0,
	NETWORK_STATE_OPEN_PORT,
	NETWORK_STATE_RUNNING,	
	NETWORK_STATE_ERROR
}NETWORK_STATE_t;

typedef enum SYSTEM_STATE_e
{
	SYSTEM_STATE_INIT = 0,
	SYSTEM_STATE_GET_EEPROM_VERSION,
	SYSTEM_STATE_DOWNLOAD_EEPROM,
	SYSTEM_STATE_CHECK_BEFORE_RUNNING,	
	SYSTEM_STATE_SET_PUMP_TO_ZERO,
	SYSTEM_STATE_SET_PUMP_PERCENTAGE_TO_ZERO,	
    SYSTEM_STATE_RESET_COUNTERS,
    SYSTEM_STATE_RUNNING,	
	SYSTEM_STATE_REQUEST_REBOOT,
    SYSTEM_STATE_ERROR,
	SYSTEM_STATE_MAX_NUMBER
}SYSTEM_STATE_t;

typedef enum NETWORK_STATUS_e
{
	NETWORK_STATUS_NEVER_STARTED = 0,
	NETWORK_STATUS_ONLINE,
	NETWORK_STATUS_OFFLINE,
	NETWORK_STATUS_MAX_NUMBER
}NETWORK_STATUS_t;

typedef enum NETWORK_COMMAND_e
{
    /* 0x00 */ NETWORK_COMMAND_KEEP_ALIVE = 0,
    /* 0x01 */ NETWORK_COMMAND_GET_EEPROM_VERSION,
    /* 0x02 */ NETWORK_COMMAND_GET_ALL_DATA,
    /* 0x03 */ NETWORK_COMMAND_SET_PUMP,
    /* 0x04 */ NETWORK_COMMAND_GET_ERROR_CODE,
    /* 0x05 */ NETWORK_COMMAND_GET_WARNING_CODE,
    /* 0x06 */ NETWORK_COMMAND_CALIBRATION_DATE,
    /* 0x07 */ NETWORK_COMMAND_GET_MEMORY_DATA,
    /* 0x08 */ NETWORK_COMMAND_SET_MEMORY_DATA,
    /* 0x09 */ NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING,
    /* 0x0A */ NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER,
    /* 0x0B */ NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER,
    /* 0x0C */ NETWORK_COMMAND_CLEAR_EEPROM,
    /* 0x0D */ NETWORK_COMMAND_CPU_LOAD,
    /* 0x0E */ NETWORK_COMMAND_KILL_ME,
    /* 0x0F */ NETWORK_COMMAND_GET_DEBUG_DATA,
    /* 0x10 */ NETWORK_COMMAND_RESET,
    /* 0x11 */ NETWORK_COMMAND_VOID,
    /* 0x12 */ NETWORK_COMMAND_MEMORY_ID_IN_DEFAULT,
    /* 0x13 */ NETWORK_COMMAND_SAVE_PUMP_PERCENTAGE_MIN,
    NETWORK_COMMAND_MAX_NUMBER
}NETWORK_COMMAND_t;

typedef enum NETM_MEMORY_RETURN_e
{
  NETM_MEMORY_RETURN_OK = 0,
  NETM_MEMORY_RETURN_BUSY,
  NETM_MEMORY_RETURN_ID_NOT_IN_LIST,
  NETM_MEMORY_RETURN_GENERAL_ERROR,
  NETM_MEMORY_RETURN_WRONG_LENGTH,
  NETM_MEMORY_RETURN_PARAM_ERROR,
  NETM_MEMORY_RETURN_CONDITION_NOT_CORRECT,
  NETM_MEMORY_RETURN_MAX_NUMBER
}NETM_MEMORY_RETURN_t;

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
STD_RETURN_t NETWORK_SendMessageRequest(NETWORK_COMMAND_t messageType, ubyte* messageTxPayload, ulong messageTxLengthPayload, ubyte* messageRx, ulong* messageRxLength);

/* TX Command 0x03 */

STD_RETURN_t NETWORK_SetPumpSetpoint(float pumpSetpoint);

/* TX command 0x09 */
STD_RETURN_t NETWORK_RestoreDataNewSampling(void);
/* TX Command 0x0A */
STD_RETURN_t NETWORK_SetPumpPercentageSetpoint(float pumpPercentageSetpoint);

/* RX Command 0x01 */
STD_RETURN_t NETWORK_GetIdString(sbyte* idStringBufferDest, ulong idStringBufferDestSize, ulong* idStringLength);
/* RX Command 0x02 */
STD_RETURN_t NETWORK_GetExternTemperatureSensor(float* floatValue);
STD_RETURN_t NETWORK_GetExternDigitalBarometer(float* ushortValue);
STD_RETURN_t NETWORK_GetMeterPressure(float* ushortValue);
STD_RETURN_t NETWORK_GetMeterTemperature(float* floatValue);
STD_RETURN_t NETWORK_GetLitersMinuteRead(float* floatValue);
STD_RETURN_t NETWORK_GetLitersTotalFromSamplingStart(float* floatValue);
STD_RETURN_t NETWORK_GetFilterTemperature(float* floatValue);
STD_RETURN_t NETWORK_GetStorageTemperature(float* floatValue);
/* RX Command 0x04 */
//STD_RETURN_t NETWORK_GetErrorCodeRaw(ushort* ushortValue);
//STD_RETURN_t NETWORK_GetErrorCode(SYST_ERROR_t* byteValue);
//STD_RETURN_t NETWORK_GetErrorSubCode(ubyte* byteValue);
STD_RETURN_t NETWORK_GetErrorCodeAndSubCode(SYST_ERROR_t* errorCode, ubyte* errorSubCode, ubyte clear);

//STD_RETURN_t NETWORK_GetWarningCodeRaw(ushort* ushortValue);
STD_RETURN_t NETWORK_ResetWarning(void);
STD_RETURN_t NETWORK_GetWarningCode(SYST_WARNING_t* byteValue);
ubyte NETWORK_IsWarningPresent(void);
//STD_RETURN_t NETWORK_GetWarningSubCode(ubyte* byteValue);
//STD_RETURN_t NETWORK_GetWarningCodeAndSubCode(SYST_WARNING_t* warningCode, ubyte* warningSubCode, ubyte clear);

/* RX Command 0x05 */
STD_RETURN_t NETWORK_GetPumpRunning(ubyte* flagValue);
STD_RETURN_t NETWORK_GetPumpRegulation(ubyte* flagValue);
STD_RETURN_t NETWORK_GetMeterPressureMounted(ubyte* flagValue);
STD_RETURN_t NETWORK_GetMeterTemperatureMounted(ubyte* flagValue);
STD_RETURN_t NETWORK_GetExternTemperatureMounted(ubyte* flagValue);
STD_RETURN_t NETWORK_GetExternPressureMounted(ubyte* flagValue);
STD_RETURN_t NETWORK_GetInverterOrPwm(ubyte* flagValue);
STD_RETURN_t NETWORK_GetWarningFlag(ubyte* flagValue);
STD_RETURN_t NETWORK_GetErrorFlag(ubyte* flagValue);
STD_RETURN_t NETWORK_GetExternalPower(ubyte* flagValue);
STD_RETURN_t NETWORK_GetBatteryVoltageCritical(ubyte* flagValue);
STD_RETURN_t NETWORK_GetFilterTemperatureMounted(ubyte* flagValue);
STD_RETURN_t NETWORK_GetStorageTemperatureMounted(ubyte* flagValue);
/* RX Command 0x06 */

/* RX Command 0x0B */
STD_RETURN_t NETWORK_StartGetPumpPercentage(void);
STD_RETURN_t NETWORK_StopGetPumpPercentage(void);
STD_RETURN_t NETWORK_GetPumpPercentage(float* floatValue);

STD_RETURN_t NETWORK_GetCpuLoad(float* floatValue);

ubyte NETWORK_IsInNoCommunicationState(void);


SYSTEM_STATE_t SYSTEM_GetState(void);
STD_RETURN_t SYSTEM_GetEepromDownloadPercentage(float* percentage);

/* DEBUG DATA */
STD_RETURN_t NETWORK_GetPumpPwm(ushort* ushortValue);
STD_RETURN_t NETWORK_GetImpulseTotalFromSamplingStart(ulong* ulongValue);

STD_RETURN_t NETWORK_GetAdcBaro(ushort* ushortValue);
STD_RETURN_t NETWORK_GetAdcExtTemp(ushort* ushortValue);
STD_RETURN_t NETWORK_GetAdcMetTemp(ushort* ushortValue);
STD_RETURN_t NETWORK_GetAdcVacuum(ushort* ushortValue);
STD_RETURN_t NETWORK_GetAdcBatt(ushort* ushortValue);
STD_RETURN_t NETWORK_GetBatteryVoltage(float* floatValue);

STD_RETURN_t NETWORK_GetNetcTxCounter(ulong* ulongValue);
STD_RETURN_t NETWORK_GetNetcRxCounter(ulong* ulongValue);
STD_RETURN_t NETWORK_GetNetcCrcFaultCounter(ulong* ulongValue);
STD_RETURN_t NETWORK_GetNetcLenFaultCounter(ulong* ulongValue);

STD_RETURN_t NETWORK_GetInvcTxCounter(ulong* ulongValue);
STD_RETURN_t NETWORK_GetInvcRxCounter(ulong* ulongValue);
STD_RETURN_t NETWORK_GetInvcCrcFaultCounter(ulong* ulongValue);
STD_RETURN_t NETWORK_GetInvcLenFaultCounter(ulong* ulongValue);

STD_RETURN_t NETWORK_GetCrcNetcCounter(ushort* ushortValue);
STD_RETURN_t NETWORK_GetCrcInvcCounter(ushort* ushortValue);

STD_RETURN_t NETWORK_GetTestVoidLitersLost(float* floatValue);
STD_RETURN_t NETWORK_GetTestVoidMeterPressure(float* floatValue);
STD_RETURN_t NETWORK_GetTestVoidSecondsToTheEnd(ushort* ushortValue);

STD_RETURN_t NETWORK_InvalidateMessageAndData(NETWORK_COMMAND_t messageType);

STD_RETURN_t NETWORK_SetPumpPercentageMin(float percentageMin);

#endif // NETWORK_H 
