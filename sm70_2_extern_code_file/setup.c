/*
 * setup.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define SETUP_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"

/* Private define ------------------------------------------------------------*/
#define HMI_MY_ID HMI_ID_SETUP

/* #define SETUP_ENABLE_DEBUG_COUNTERS */
#define SETUP_NORMAL_ENABLE
/* #define SETUP_FAST_ENABLE */
/* #define SETUP_LOW_PRIO_ENABLE */
/* #define SETUP_THREAD_ENABLE */

#define SETUP_THREAD_SLEEP_MS 50U

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Setup_logFlag(FLAG_t flag);
static STD_RETURN_t Setup_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Setup_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Setup_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Setup_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t Setup_initVariable(void);
#ifdef SETUP_NORMAL_ENABLE
static STD_RETURN_t Setup_manageButtons(ulong execTimeMs);
static STD_RETURN_t Setup_manageScreen(ulong execTimeMs);
#endif /* #ifdef SETUP_NORMAL_ENABLE */
#ifdef SETUP_THREAD_ENABLE
static STD_RETURN_t Setup_modelInit(void);
static STD_RETURN_t Setup_model(ulong execTimeMs);
#endif /* #ifdef SETUP_THREAD_ENABLE */

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Setup_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Setup_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Setup_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Setup_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t Setup_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t Setup_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Setup_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef SETUP_NORMAL_ENABLE
static STD_RETURN_t Setup_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(setup_backButton_released != false) /* These flag is reset by GUI */
    {
        setup_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Setup_logFlag(FLAG_SETUP_BUTTON_BACK);
    }
    if(setup_testButton_released != false) /* These flag is reset by GUI */
    {
        setup_testButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Setup_logFlag(FLAG_SETUP_BUTTON_TEST);
    }
    if(setup_dateTimeButton_released != false) /* These flag is reset by GUI */
    {
        setup_dateTimeButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Setup_logFlag(FLAG_SETUP_BUTTON_DATETIME);
    }
    if(setup_languageButton_released != false) /* These flag is reset by GUI */
    {
        setup_languageButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Setup_logFlag(FLAG_SETUP_BUTTON_LANGUAGE);
    }
    if(setup_parameterButton_released != false) /* These flag is reset by GUI */
    {
        setup_parameterButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Setup_logFlag(FLAG_SETUP_BUTTON_PARAMETER);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Setup_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef SETUP_NORMAL_ENABLE */

#ifdef SETUP_NORMAL_ENABLE
static STD_RETURN_t Setup_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(MessageScreen_isEnteredHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
        }
    }
    
    if(FLAG_GetAndReset(FLAG_SETUP_BUTTON_BACK) != false)
    {
        HMI_ChangeHmi(HMI_ID_MAIN, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_SETUP_BUTTON_TEST) != false)
    {
        if(SYSTEM_GetState() != SYSTEM_STATE_RUNNING)
        {
            MessageScreen_openError(DYNAMIC_STRING_ERROR_NO_INTERNAL_COMMUNICATION, STD_ERROR_NO_COMMUNICATION, HMI_MY_ID);
        }
        else
        {
            HMI_ChangeHmi(HMI_ID_TEST, HMI_MY_ID);
        }
    }
    
    if(FLAG_GetAndReset(FLAG_SETUP_BUTTON_DATETIME) != false)
    {
        HMI_ChangeHmi(HMI_ID_DATETIME, HMI_MY_ID);
    }
    if(FLAG_GetAndReset(FLAG_SETUP_BUTTON_LANGUAGE) != false)
    {
        HMI_ChangeHmi(HMI_ID_LANGUAGE, HMI_MY_ID);
    }
    if(FLAG_GetAndReset(FLAG_SETUP_BUTTON_PARAMETER) != false)
    {
        HMI_ChangeHmi(HMI_ID_PARAMETER, HMI_MY_ID);
    }
    
    /* The beep if the GR disconnect is managed into main screen */
    
    if(MessageScreen_isExitedHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Setup_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef SETUP_THREAD_ENABLE
static STD_RETURN_t Setup_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Setup_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef SETUP_THREAD_ENABLE */

#ifdef SETUP_THREAD_ENABLE
static STD_RETURN_t Setup_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Setup_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef SETUP_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void setup_init()
{
    (void)Setup_initVariable();
}

#ifdef SETUP_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void setup()
{
#ifdef SETUP_ENABLE_DEBUG_COUNTERS
    Setup_NormalRoutine++;
#endif
    (void)Setup_manageButtons(Execution_Normal);
    (void)Setup_manageScreen(Execution_Normal);
}
#endif /* #ifdef SETUP_NORMAL_ENABLE */

#ifdef SETUP_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void setup_fast()
{
#ifdef SETUP_ENABLE_DEBUG_COUNTERS
    Setup_FastRoutine++;
#endif
}
#endif /* #ifdef SETUP_FAST_ENABLE */

#ifdef SETUP_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void setup_low_priority()
{
#ifdef SETUP_ENABLE_DEBUG_COUNTERS
    Setup_LowPrioRoutine++;
#endif
}
#endif /* #ifdef SETUP_LOW_PRIO_ENABLE */

#ifdef SETUP_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void setup_thread()
{
    ulong execTime = SETUP_THREAD_SLEEP_MS;
    
    (void)Setup_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef SETUP_ENABLE_DEBUG_COUNTERS
        Setup_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)Setup_model(execTime);
    }
}
#endif /* #ifdef SETUP_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void setup_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void setup_shutdown()
//{
//}
