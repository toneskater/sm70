/*
 * pm10.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define PM10_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"
#include "eepm_data.h"
#include "serial_com.h"

/* Private define ------------------------------------------------------------*/

/* #define PM10_ENABLE_DEBUG_COUNTERS */
#define PM10_NORMAL_ENABLE
/* #define PM10_FAST_ENABLE */
/* #define PM10_LOW_PRIO_ENABLE */
#define PM10_THREAD_ENABLE

#define PM10_THREAD_SLEEP_MS 50U

#define PM10_DEFAULT_FLOW 38.3f


#define PM10_SERIAL_COM_TX_BUFFER_SIZE 512U
/* Private typedef -----------------------------------------------------------*/
typedef enum PM10_GUI_STATE_e
{
    PM10_GUI_STATE_INIT = 0,
    PM10_GUI_STATE_READY,
    PM10_GUI_STATE_WAIT_START,
    PM10_GUI_STATE_RUNNING,
    PM10_GUI_STATE_PAUSED,
    PM10_GUI_STATE_PAUSED_NO_POWER,
    PM10_GUI_STATE_PUMP_STOPPING,
    PM10_GUI_STATE_STOPPED,
    PM10_GUI_STATE_MAX_NUMBER
}PM10_GUI_STATE_t;

typedef enum PM10_MODEL_STATE_e
{
    PM10_MODEL_STATE_INIT = 0,
    PM10_MODEL_STATE_READY,
    PM10_MODEL_STATE_WAIT_START,
    PM10_MODEL_STATE_PUMP_START,
    PM10_MODEL_STATE_RUNNING,
    PM10_MODEL_STATE_PAUSED,
    PM10_MODEL_STATE_PAUSED_NO_POWER,
    PM10_MODEL_STATE_PUMP_STOPPING,
    PM10_MODEL_STATE_STOPPED,
    PM10_MODEL_STATE_ERROR,
    PM10_MODEL_STATE_MAX_NUMBER
}PM10_MODEL_STATE_t;

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static PM10_GUI_STATE_t Pm10_GuiState;
static PM10_MODEL_STATE_t Pm10_ModelState;
static ubyte Pm10_h24enable;
static ubyte Pm10_FilterNumber;

static GRAPH_DATA_TO_SHOW_t Pm10Graph_DataToShow;
static ubyte Pm10_modelCalculateStatisticRefreshCounter;
static ubyte Pm10_modelCalculateStatisticNewDataForGraph; 
static slong Pm10_LogCounterSecond;

static SERIAL_COM_t Pm10_serialCom;
static ubyte Pm10_TxBuffer[PM10_SERIAL_COM_TX_BUFFER_SIZE];

static const sbyte* Pm10_guiStateNames[PM10_GUI_STATE_MAX_NUMBER] =
{
	/* PM10_GUI_STATE_INIT = 0,         */ "Init         ",
	/* PM10_GUI_STATE_READY,            */ "Ready        ",
	/* PM10_GUI_STATE_WAIT_START,       */ "Wait Start   ",
	/* PM10_GUI_STATE_RUNNING,          */ "Running      ",
	/* PM10_GUI_STATE_PAUSED,           */ "Paused       ",
	/* PM10_GUI_STATE_PAUSED_NO_POWER,  */ "Paused No Pow",	
	/* PM10_GUI_STATE_PUMP_STOPPING,    */ "Pump Stopping",
	/* PM10_GUI_STATE_STOPPED,          */ "Stopped      ",
};

static const sbyte* Pm10_modelStateNames[PM10_MODEL_STATE_MAX_NUMBER] =
{
	/* PM10_MODEL_STATE_INIT = 0,        */ "Init         ",
	/* PM10_MODEL_STATE_READY,           */ "Ready        ",
	/* PM10_MODEL_STATE_WAIT_START,      */ "Wait Start   ",
	/* PM10_MODEL_STATE_PUMP_START,      */ "Pump Starting",
	/* PM10_MODEL_STATE_RUNNING,         */ "Running      ",
	/* PM10_MODEL_STATE_PAUSED,          */ "Paused       ",
	/* PM10_MODEL_STATE_PAUSED_NO_POWER, */ "Paused No Pow",
	/* PM10_MODEL_STATE_PUMP_STOPPING,   */ "Pump Stopping",
	/* PM10_MODEL_STATE_STOPPED,         */ "Stopped      ",
	/* PM10_MODEL_STATE_ERROR,           */ "Error        ",
};

static ubyte Pm10_sendOnSerial;
static ulong Pm10_LogDataToSend;

static SYST_WARNING_t Pm10_warningCode;
static ubyte Pm10_warningSubCode; 

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Pm10_logFlag(FLAG_t flag);
static STD_RETURN_t Pm10_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Pm10_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Pm10_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Pm10_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t Pm10_initVariable(void);
#ifdef PM10_NORMAL_ENABLE
static STD_RETURN_t Pm10Graph_manageScreenButtonDataToShow(void);
static STD_RETURN_t Pm10Setup1_manageScreenTimeOrVolumeButton(void);
static STD_RETURN_t Pm10_GetDataFromView(PM10_InterfaceData_t* interfaceData);
static STD_RETURN_t Pm10_showDataOnCharts(void);
static STD_RETURN_t Pm10Setup1_manageScreenH24Button(void);
static STD_RETURN_t Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_t stateOfArrival);
static STD_RETURN_t Pm10_manageScreenH24FieldsEnable(ubyte manage, ubyte manageIsoLimit);
static STD_RETURN_t Pm10Setup1_manageScreenAspirationFlow(void);
static STD_RETURN_t Pm10Setup1_manageScreenStopFlow(void);

static STD_RETURN_t Pm10Setup1_manageButtons(ulong execTimeMs);
static STD_RETURN_t Pm10Data1_manageButtons(ulong execTimeMs);
static STD_RETURN_t Pm10Graph_manageButtons(ulong execTimeMs);
static STD_RETURN_t Pm10_manageScreenCommonEvent(ulong execTimeMs);
static STD_RETURN_t Pm10Waiting_manageButtons(ulong execTimeMs);
static STD_RETURN_t Pm10Setup1_manageScreen(ulong execTimeMs);
static STD_RETURN_t Pm10Data1_manageScreen(ulong execTimeMs);
static STD_RETURN_t Pm10Graph_manageScreen(ulong execTimeMs);
static STD_RETURN_t Pm10Waiting_manageScreen(ulong execTimeMs);
#endif /* #ifdef PM10_NORMAL_ENABLE */

#ifdef PM10_THREAD_ENABLE
static STD_RETURN_t Pm10_CalculateStatisticFillData(Sampling_StatisticData_t* sd);
static STD_RETURN_t Pm10_ErrorDetected(SAMPLING_ERROR_t* samplingError);
static STD_RETURN_t Pm10_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError);
static STD_RETURN_t Pm10_CheckAndManageAutoTerminationCondition(void);
static STD_RETURN_t Pm10_sendPumpStop(void);
static STD_RETURN_t Pm10_model_writeFinalData(void);
static STD_RETURN_t Pm10_modelStartFromReadyOrStopped(void);
static STD_RETURN_t Pm10_RestoreDataForNewSampling(void);
static STD_RETURN_t Pm10_sendPumpSetPoint(void);
static STD_RETURN_t Pm10_modelManageWarning(void);

static STD_RETURN_t Pm10_modelStateInit(ulong execTimeMs);
static STD_RETURN_t Pm10_modelStateReady(ulong execTimeMs);
static STD_RETURN_t Pm10_modelStateWaitStart(ulong execTimeMs);
static STD_RETURN_t Pm10_modelStatePumpStart(ulong execTimeMs);
static STD_RETURN_t Pm10_modelStateRunning(ulong execTimeMs);
static STD_RETURN_t Pm10_modelStatePaused(ulong execTimeMs);
static STD_RETURN_t Pm10_modelStatePausedNoPower(ulong execTimeMs);
static STD_RETURN_t Pm10_modelStatePumpStopping(ulong execTimeMs);
static STD_RETURN_t Pm10_modelStateStopped(ulong execTimeMs);
static STD_RETURN_t Pm10_modelStateError(ulong execTimeMs);

static STD_RETURN_t Pm10_modelInit(void);
static STD_RETURN_t Pm10_model(ulong execTimeMs);

static STD_RETURN_t Pm10_LogOnSerialInit(void);
static STD_RETURN_t Pm10_LogOnSerialCheckFct(void);
static STD_RETURN_t Pm10_LogOnSerialWriteFct(void);
static STD_RETURN_t Pm10_LogOnSerialClose(void);

#endif /* #ifdef PM10_THREAD_ENABLE */

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Pm10_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Pm10_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Pm10_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Pm10_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
}

static STD_RETURN_t Pm10_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t Pm10_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    Pm10_GuiState = PM10_GUI_STATE_INIT;
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_initVariable %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MANAGE BUTTON                                                                  */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */

#ifdef PM10_NORMAL_ENABLE
static STD_RETURN_t Pm10Setup1_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(pm10_setup_1_backButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_BACK);
    }
    if(pm10_setup_1_startButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_START);
    }
    if(pm10_setup_1_pauseButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_pauseButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_PAUSE);
    }
    if(pm10_setup_1_stopButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_STOP);
    }
    if(pm10_setup_1_buttonPrev_release != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_buttonPrev_release = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_PREV);
    }
    if(pm10_setup_1_buttonNext_release != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_buttonNext_release = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_NEXT);
    }
    if(pm10_setup_1_buttonTime_released != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_buttonTime_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_TIME);
    }
    if(pm10_setup_1_buttonVolume_released != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_buttonVolume_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_VOLUME);
    }
    if(pm10_setup_1_button24Enable_released != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_button24Enable_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_H24_ENABLE);
    }
    if(pm10_setup_1_button24Disable_released != false) /* These flag is reset by GUI */
    {
    	pm10_setup_1_button24Disable_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_SETUP_1_BUTTON_H24_DISABLE);
    }
    if(pm10_setup_1_isokineticSamplingValue_eventAccepted != false) /* user must set the value to false */
    {
        pm10_setup_1_isokineticSamplingValue_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logUnsignedValue("Isokinetic sampling time or volume value", common_isokineticSamplingValue);
    }
    if(pm10_setup_1_aspirationFlow_eventAccepted != false)
    {
        pm10_setup_1_aspirationFlow_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFloatValue("Aspiration Flow value", pm10_setup_1_aspirationFlow_value);
    }
    if(pm10_setup_1_aspirationFlowError_eventAccepted != false)
    {
        pm10_setup_1_aspirationFlowError_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFloatValue("Flow Stop value", pm10_setup_1_aspirationFlowError_value);
    }  
    if(pm10_setup_1_startDelayValue_eventAccepted != false)
    {
        pm10_setup_1_startDelayValue_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logUnsignedValue("Start Delay value", common_startDelayValue);
    }
    if(pm10_setup_1_serialLogTimeMin_eventAccepted != false)
    {
        pm10_setup_1_serialLogTimeMin_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logUnsignedValue("Serial Log Time Interval min", common_startDelayValue);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10Setup_manageButtons %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10Data1_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(pm10_data_1_backButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_data_1_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_DATA_1_BUTTON_BACK);
    }
    if(pm10_data_1_startButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_data_1_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_DATA_1_BUTTON_START);
    }
    if(pm10_data_1_pauseButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_data_1_pauseButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_DATA_1_BUTTON_PAUSE);
    }
    if(pm10_data_1_stopButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_data_1_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_DATA_1_BUTTON_STOP);
    }
    if(pm10_data_1_buttonPrev_release != false) /* These flag is reset by GUI */
    {
    	pm10_data_1_buttonPrev_release = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_DATA_1_BUTTON_PREV);
    }
    if(pm10_data_1_buttonNext_release != false) /* These flag is reset by GUI */
    {
    	pm10_data_1_buttonNext_release = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_DATA_1_BUTTON_NEXT);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10Data_manageButtons %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10Graph_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(pm10_graph_backButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_graph_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_BACK);
    }
    if(pm10_graph_startButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_graph_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_START);
    }
    if(pm10_graph_pauseButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_graph_pauseButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_PAUSE);
    }
    if(pm10_graph_stopButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_graph_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_STOP);
    }
    if(pm10_graph_buttonPrev_release != false) /* These flag is reset by GUI */
    {
    	pm10_graph_buttonPrev_release = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_PREV);
    }
    if(pm10_graph_buttonNext_release != false) /* These flag is reset by GUI */
    {
    	pm10_graph_buttonNext_release = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_NEXT);
    }
    if(pm10_graph_flowButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_graph_flowButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_FLOW);
    }
    if(pm10_graph_errorButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_graph_errorButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_ERROR);
    }
    if(pm10_graph_vacuumButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_graph_vacuumButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_VACUUM);
    }
    if(pm10_graph_meterTemperatureButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_graph_meterTemperatureButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_METER_TEMPERATURE);
    }
    if(pm10_graph_buttonDataPrev_release != false) /* These flag is reset by GUI */
    {
    	pm10_graph_buttonDataPrev_release = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_PREV_GRAPH);
    }
    if(pm10_graph_buttonDataNext_release != false) /* These flag is reset by GUI */
    {
    	pm10_graph_buttonDataNext_release = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_GRAPH_BUTTON_NEXT_GRAPH);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10Graph_manageButtons %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10Waiting_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(pm10_waiting_startButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_waiting_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_WAITING_BUTTON_START);
    }
    if(pm10_waiting_pauseButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_waiting_pauseButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_WAITING_BUTTON_PAUSE);
    }
    if(pm10_waiting_stopButton_released != false) /* These flag is reset by GUI */
    {
    	pm10_waiting_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Pm10_logFlag(FLAG_PM10_WAITING_BUTTON_STOP);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10Waiting_manageButtons %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MANAGE SCREEN                                                                  */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_showDataOnCharts(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    float meterTemperature = 0.0f;
    float volume = 0.0f;
    float flowActual = 0.0f;
    float meterPressure = 0.0f;
    float vacuum = 0.0f;
    float isoFlow = WORD_TO_FLOAT(pm10_setup_1_aspirationFlow_value);
    
    if(NETWORK_GetMeterPressure(&meterPressure) == STD_RETURN_OK)
    {
        vacuum = meterPressure;
        //switch(common_measureUnit)
        //{
            //case SAMPLING_MEASURE_UNIT_MMH20:
                common_samplingVacuum = FLOAT_TO_WORD(vacuum);
                pm10_data_1_vacuum_color = WHITE;
            //    break;
            //case SAMPLING_MEASURE_UNIT_PA:
            //    common_samplingVacuum = FLOAT_TO_WORD(MMH2O_TO_PA(vacuum));
            //    pm10_charts_vacuum_color = WHITE;
            //    break;
            //default:
            //    common_samplingVacuum = FLOAT_TO_WORD(vacuum);
            //    pm10_charts_vacuum_color = RED;
            //    break;
        //}
    }
    else
    {
        common_samplingVacuum = FLOAT_TO_WORD(vacuum);
        pm10_data_1_vacuum_color = RED;
    }
    
    if(NETWORK_GetMeterTemperature(&meterTemperature) == STD_RETURN_OK)
    {
        common_samplingMeterTemperature = FLOAT_TO_WORD(meterTemperature);
        pm10_data_1_meter_temperature_color = WHITE;
    }
    else
    {
        common_samplingMeterTemperature = FLOAT_TO_WORD(meterTemperature);
        pm10_data_1_meter_temperature_color = RED;
    }
    
    if(NETWORK_GetLitersMinuteRead(&flowActual) == STD_RETURN_OK)
    {
        common_samplingFlowActual = FLOAT_TO_WORD(flowActual);
        pm10_data_1_flow_color = WHITE;
        
        if(flowActual > 0.0f)
        {
            common_samplingFlowError = FLOAT_TO_WORD((((flowActual - isoFlow) * 100.0f)/isoFlow));
        }
        else
        {
            common_samplingFlowError = FLOAT_TO_WORD(0.0f);
        }
    }
    else
    {
        common_samplingFlowActual = FLOAT_TO_WORD(flowActual);
        pm10_data_1_flow_color = RED;
        common_samplingFlowError = FLOAT_TO_WORD(0.0f);
    }
    
    if(NETWORK_GetLitersTotalFromSamplingStart(&volume) == STD_RETURN_OK)
    {
        common_samplingVolume = FLOAT_TO_WORD(volume);
        pm10_data_1_volume_color = WHITE;
    }
    else
    {
        common_samplingVolume = FLOAT_TO_WORD(volume);
        pm10_data_1_volume_color = RED;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_showDataOnCharts %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_GetDataFromView(PM10_InterfaceData_t* interfaceData)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    
    if(interfaceData != NULL)
    {
        interfaceData->samplingType = common_isokineticSampling;
        interfaceData->time = common_isokineticSamplingValue;
        interfaceData->volume = common_isokineticSamplingValue;
        interfaceData->normalizationTemperature = parameter_normalizationTemperatureValue;
        interfaceData->aspirationFlow = WORD_TO_FLOAT(pm10_setup_1_aspirationFlow_value);
        interfaceData->aspirationFlowError = WORD_TO_FLOAT(pm10_setup_1_aspirationFlowError_value);
        interfaceData->samplingStartDelayMinutes = common_startDelayValue;
        interfaceData->serialLogTineMin = pm10_setup_1_serialLogTimeMin_value;
        interfaceData->h24Enable = Pm10_h24enable;
        returnValue = STD_RETURN_OK;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_GetDataFromView %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10Graph_manageScreenButtonDataToShow(void)
{
    switch(Pm10Graph_DataToShow)
    {
        case GRAPH_DATA_TO_SHOW_FLOW:
            pm10_graph_flowButton_backgroundColor = DARKGREEN;
            pm10_graph_errorButton_backgroundColor = YELLOW;
            pm10_graph_vacuumButton_backgroundColor = YELLOW;
            pm10_graph_meterTemperatureButton_backgroundColor = YELLOW;
            break;
        case GRAPH_DATA_TO_SHOW_ERROR:
            pm10_graph_flowButton_backgroundColor = YELLOW;
            pm10_graph_errorButton_backgroundColor = DARKGREEN;
            pm10_graph_vacuumButton_backgroundColor = YELLOW;
            pm10_graph_meterTemperatureButton_backgroundColor = YELLOW;
            break;
        case GRAPH_DATA_TO_SHOW_VACUUM:
            pm10_graph_flowButton_backgroundColor = YELLOW;
            pm10_graph_errorButton_backgroundColor = YELLOW;
            pm10_graph_vacuumButton_backgroundColor = DARKGREEN;
            pm10_graph_meterTemperatureButton_backgroundColor = YELLOW;
            break;
        case GRAPH_DATA_TO_SHOW_METER_TEMPERATURE:
            pm10_graph_flowButton_backgroundColor = YELLOW;
            pm10_graph_errorButton_backgroundColor = YELLOW;
            pm10_graph_vacuumButton_backgroundColor = YELLOW;
            pm10_graph_meterTemperatureButton_backgroundColor = DARKGREEN;
            break;
        default:
            break;
    }
    return STD_RETURN_OK;
}

static STD_RETURN_t Pm10Setup1_manageScreenTimeOrVolumeButton(void)
{
    switch(common_isokineticSampling)
    {
        case SAMPLING_ISOKINETIC_SAMPLING_TIME:
            pm10_setup_1_buttonTime_backgroundColor = DARKGREEN;
            pm10_setup_1_buttonVolume_backgroundColor = YELLOW;
            break;
        case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
            pm10_setup_1_buttonTime_backgroundColor = YELLOW;
            pm10_setup_1_buttonVolume_backgroundColor = DARKGREEN;
            break;
    }
    return STD_RETURN_OK;
}

static STD_RETURN_t Pm10Setup1_manageScreenH24Button(void)
{
    if(common_isokineticSampling == SAMPLING_ISOKINETIC_SAMPLING_TIME)
    {
        if(Pm10_h24enable != false)
        {
            pm10_setup_1_button24Enable_backgroundColor = DARKGREEN;
            pm10_setup_1_button24Disable_backgroundColor = YELLOW;
        }
        else
        {
            pm10_setup_1_button24Enable_backgroundColor = YELLOW;
            pm10_setup_1_button24Disable_backgroundColor = DARKGREEN;
        }
    }
    else
    {
        pm10_setup_1_button24Enable_backgroundColor = DARKGRAY;
        pm10_setup_1_button24Disable_backgroundColor = DARKGRAY;
    }
    return STD_RETURN_OK;
}

static STD_RETURN_t Pm10_manageScreenH24FieldsEnable(ubyte manage, ubyte manageIsoLimit)
{
    if(manage != false)
    {
        if(common_isokineticSampling == SAMPLING_ISOKINETIC_SAMPLING_TIME)
        {
            pm10_setup_h24dataFields_enable = true;
            pm10_setup_1_dataFields_backgroundColor = WHITE;
            if(Pm10_h24enable != false)
            {
                pm10_setup_delayedStart_enable = false;
                pm10_setup_1_delayedStart_backgroundColor = DARKGRAY;
                pm10_setup_1_isokineticSamplingValue_backgroundColor = DARKGRAY;
                pm10_setup_isokineticSamplingValue_enable = false;
                common_isokineticSamplingValue = (60*24); /* 60 min x 24h */
            }
            else
            {
                pm10_setup_delayedStart_enable = true;
                pm10_setup_1_delayedStart_backgroundColor = WHITE;
                pm10_setup_1_isokineticSamplingValue_backgroundColor = WHITE;
                pm10_setup_isokineticSamplingValue_enable = true;
                if(manageIsoLimit != false)
                {
                    Sampling_manageIsokineticSamplingLimits();
                    debug_print("Sampling_manageIsokineticSamplingLimits Pm10_manageScreenH24FieldsEnable 1");
                }
            }
        }
        else
        {
            /* Volume */
            pm10_setup_h24dataFields_enable = false;
            pm10_setup_delayedStart_enable = true;
            pm10_setup_1_delayedStart_backgroundColor = WHITE;
            pm10_setup_1_dataFields_backgroundColor = WHITE;
            pm10_setup_1_isokineticSamplingValue_backgroundColor = WHITE;
            pm10_setup_isokineticSamplingValue_enable = true;
            if(manageIsoLimit != false)
            {
                Sampling_manageIsokineticSamplingLimits();
                debug_print("Sampling_manageIsokineticSamplingLimits Pm10_manageScreenH24FieldsEnable 2");
            }
        }
    }
    else
    {
        pm10_setup_h24dataFields_enable = false;
        pm10_setup_delayedStart_enable = false;
        pm10_setup_1_delayedStart_backgroundColor = DARKGRAY;
        pm10_setup_1_dataFields_backgroundColor = DARKGRAY;
        pm10_setup_1_isokineticSamplingValue_backgroundColor = DARKGRAY;
        pm10_setup_isokineticSamplingValue_enable = false;
    }
}

static STD_RETURN_t Pm10Setup1_manageScreenAspirationFlow(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ushort ushortValue;
    float pumpMin = 0.0f;
    float pumpMax = 0.0f;
    returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MIN, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1);
    if(STD_RETURN_OK == returnValue)
    {
        pumpMin = (((float)ushortValue) / 100);
    }
    if(STD_RETURN_OK == returnValue)
    {
        returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MAX, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1);
    }
    if(STD_RETURN_OK == returnValue)
    {
        pumpMax = (((float)ushortValue) / 100);
    }
    
    pm10_setup_1_aspirationFlow_rangeMax = FLOAT_TO_WORD(pumpMax);
    pm10_setup_1_aspirationFlow_rangeMin = FLOAT_TO_WORD(pumpMin);
    
    
    if(PM10_DEFAULT_FLOW <= pumpMax)
    {
        pm10_setup_1_aspirationFlow_value = FLOAT_TO_WORD(PM10_DEFAULT_FLOW);
    }
    else
    {
        pm10_setup_1_aspirationFlow_value = FLOAT_TO_WORD(pumpMax);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10Setup1_manageScreenAspirationFlow %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t Pm10Setup1_manageScreenStopFlow(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ushort ushortValue;
    pm10_setup_1_aspirationFlowError_rangeMin = FLOAT_TO_WORD(0.01f);
    returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MIN, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1);
    if(STD_RETURN_OK == returnValue)
    {
        pm10_setup_1_aspirationFlowError_rangeMax = FLOAT_TO_WORD(((float)(ushortValue - 1U)) / 100);
        pm10_setup_1_aspirationFlowError_value = FLOAT_TO_WORD(0.10f);  
        if(pm10_setup_1_aspirationFlowError_value > pm10_setup_1_aspirationFlowError_rangeMax)
        {
            pm10_setup_1_aspirationFlow_value = pm10_setup_1_aspirationFlowError_rangeMax;
        }
    }
    /* TODO stringa per mostrare i limiti */
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] EnvironmentalSetup_manageScreenStopFlow %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_t stateOfArrival)
{
    debug_print("FIELD_ENABLE %d %s", stateOfArrival, Pm10_guiStateNames[stateOfArrival]);
    switch(stateOfArrival)
    {
        case PM10_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
            pm10_setup_dataFields_enable = true;
            pm10_setup_1_dataFields_backgroundColor = WHITE;
            Pm10_manageScreenH24FieldsEnable(true, false);
            pm10_backButton_enabled = true;
            
            pm10_startButton_enabled = true;
            pm10_pauseButton_enabled = false;
            pm10_stopButton_enabled = false;
            pm10_buttonPrevAndNext_enable = false;
            pm10_buttonPrevAndNext_enable = false;
            break;
        case PM10_GUI_STATE_READY: /* entered first time */
            pm10_setup_dataFields_enable = true;
            pm10_setup_1_dataFields_backgroundColor = WHITE;
            Pm10_manageScreenH24FieldsEnable(true, false);
            pm10_backButton_enabled = true;
            
            pm10_startButton_enabled = true;
            pm10_pauseButton_enabled = false;
            pm10_stopButton_enabled = false;
            pm10_buttonPrevAndNext_enable = false;
            pm10_buttonPrevAndNext_enable = false;
            break;
        case PM10_GUI_STATE_WAIT_START:
            pm10_setup_dataFields_enable = false;
            pm10_setup_1_dataFields_backgroundColor = DARKGRAY;
            Pm10_manageScreenH24FieldsEnable(false, false);
            pm10_backButton_enabled = false;
            /* If Time and H24 enable is not possible to start immediatly or make pause */
            if((common_isokineticSampling == SAMPLING_ISOKINETIC_SAMPLING_TIME) && (Pm10_h24enable != false))
            {
                pm10_startButton_enabled = false;
                pm10_pauseButton_enabled = false;
            }
            else
            {
                pm10_startButton_enabled = true;
                pm10_pauseButton_enabled = true;
            }
            
            pm10_stopButton_enabled = true;
            pm10_buttonPrevAndNext_enable = false;
            pm10_buttonPrevAndNext_enable = false;
            break;
        case PM10_GUI_STATE_RUNNING: /* running */
            pm10_setup_dataFields_enable = false;
            pm10_setup_1_dataFields_backgroundColor = DARKGRAY;
            Pm10_manageScreenH24FieldsEnable(false, false);
            pm10_backButton_enabled = false;
            
            pm10_startButton_enabled = false;
            /* If Time and H24 enable is not possible to make pause */
            if((common_isokineticSampling == SAMPLING_ISOKINETIC_SAMPLING_TIME) && (Pm10_h24enable != false))
            {
                pm10_pauseButton_enabled = false;
            }
            else
            {
                pm10_pauseButton_enabled = true;
            }
            pm10_stopButton_enabled = true;
            pm10_buttonPrevAndNext_enable = true;
            pm10_buttonPrevAndNext_enable = true;
            break;
        case PM10_GUI_STATE_PAUSED: /* paused */
            pm10_setup_dataFields_enable = false;
            pm10_setup_1_dataFields_backgroundColor = DARKGRAY;
            Pm10_manageScreenH24FieldsEnable(false, false);
            pm10_backButton_enabled = false;
            
            pm10_startButton_enabled = true;
            pm10_pauseButton_enabled = false;
            pm10_stopButton_enabled = true;
            pm10_buttonPrevAndNext_enable = true;
            pm10_buttonPrevAndNext_enable = true;
            break;
        case PM10_GUI_STATE_PAUSED_NO_POWER: /* paused */
            pm10_setup_dataFields_enable = false;
            pm10_setup_1_dataFields_backgroundColor = DARKGRAY;
            Pm10_manageScreenH24FieldsEnable(false, false);
            pm10_backButton_enabled = false;
            
            pm10_startButton_enabled = false;
            pm10_pauseButton_enabled = false;
            pm10_stopButton_enabled = true;
            pm10_buttonPrevAndNext_enable = true;
            pm10_buttonPrevAndNext_enable = true;
            break;
        case PM10_GUI_STATE_PUMP_STOPPING:
            pm10_setup_dataFields_enable = false;
            pm10_setup_1_dataFields_backgroundColor = DARKGRAY;
            Pm10_manageScreenH24FieldsEnable(false, false);
            pm10_backButton_enabled = false;
            
            pm10_startButton_enabled = false;
            pm10_pauseButton_enabled = false;
            pm10_stopButton_enabled = false;
            pm10_buttonPrevAndNext_enable = true;
            pm10_buttonPrevAndNext_enable = true;
            break;
        case PM10_GUI_STATE_STOPPED: /* really stopped */
            pm10_setup_dataFields_enable = true;
            pm10_setup_1_dataFields_backgroundColor = WHITE;
            Pm10_manageScreenH24FieldsEnable(true, false);
            pm10_backButton_enabled = true;
            
            pm10_startButton_enabled = true;
            pm10_pauseButton_enabled = false;
            pm10_stopButton_enabled = false;
            pm10_buttonPrevAndNext_enable = true;
            pm10_buttonPrevAndNext_enable = true;
            break;
        default:
            break;
    }
    return STD_RETURN_OK;
}

static STD_RETURN_t Pm10_manageScreenCommonEvent(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    /* Sampling stopped due to error */
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR");
        Pm10_GuiState = PM10_GUI_STATE_INIT;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_INIT);
        /* TODO Sarebbe da mettere qua il santo della schermata, e non nel modello */
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_MODEL_EVENT_NO_POWER_SUPPLY) != false)
    {
        debug_print("Event managed: FLAG_PM10_MODEL_EVENT_NO_POWER_SUPPLY");
        BUZM_BUTTON_SOUND(); /* manual sound */
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_NO_POWER_SUPPLY, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_MODEL_EVENT_NO_POWER_SUPPLY_RUNNING) != false)
    {
        debug_print("Event managed: FLAG_PM10_MODEL_EVENT_NO_POWER_SUPPLY_RUNNING");
        BUZM_BUTTON_SOUND(); /* manual sound */
        Pm10_GuiState = PM10_GUI_STATE_PAUSED_NO_POWER;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_PAUSED_NO_POWER);
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_NO_POWER_SUPPLY_RUNNING, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_MODEL_EVENT_BACK_FROM_NO_POWER_SUPPLY_RUNNING) != false)
    {
        debug_print("Event managed: FLAG_PM10_MODEL_EVENT_BACK_FROM_NO_POWER_SUPPLY_RUNNING");
        BUZM_BUTTON_SOUND(); /* manual sound */
        Pm10_GuiState = PM10_GUI_STATE_RUNNING;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_RUNNING);
    }
    
    /* Sampling stopped due to automatic condition, time, volume */
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED) != false) /* COMMON EVENT now wait the pump stop*/
    {         
        debug_print("Event managed: FLAG_PM10_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED");
        Pm10_GuiState = PM10_GUI_STATE_PUMP_STOPPING;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_PUMP_STOPPING);
    }
    
    /* Event from model for a real stop - MANAGE AFTER A STOP */
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_EVENT_PUMP_STOPPED) != false) /* COMMON EVENT pump really stopped */
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_EVENT_PUMP_STOPPED");
        Pm10_GuiState = PM10_GUI_STATE_STOPPED;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_STOPPED);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_EVENT_MODEL_RUNNING) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_EVENT_MODEL_RUNNING");
        Pm10_GuiState = PM10_GUI_STATE_RUNNING;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_RUNNING);
        if(HMI_GetHmiIdOnScreen() != HMI_ID_PM10_DATA_1) /* Change screen only if in stopped */
        {
            (void)HMI_ChangeHmi(HMI_ID_PM10_DATA_1, HMI_GetHmiIdOnScreen());
        }
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_EVENT_MODEL_WAITING) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_EVENT_MODEL_WAITING");
        Pm10_GuiState = PM10_GUI_STATE_WAIT_START;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_WAIT_START);
        (void)HMI_ChangeHmi(HMI_ID_PM10_WAITING, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_EVENT_MODEL_ERROR) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_EVENT_MODEL_ERROR");
        Pm10_GuiState = PM10_GUI_STATE_INIT;
        (void)HMI_ChangeHmi(HMI_ID_OPERATING_PROGRAM, HMI_GetHmiIdOnScreen());
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_MODEL_EVENT_WARNING_DETECTED) != false)
    {
        debug_print("Event managed: FLAG_PM10_MODEL_EVENT_WARNING_DETECTED");
        BUZM_BUTTON_SOUND(); /* manual sound */
        MessageScreen_ManageWarningPopup(Pm10_warningCode, Pm10_warningSubCode, HMI_GetHmiIdOnScreen());
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_manageScreenCommonEvent %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10Setup1_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    const HMI_ID_t localHmiId = HMI_ID_PM10_SETUP_1;
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            switch(Pm10_GuiState)
            {
                case PM10_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
                    common_isokineticSampling = SAMPLING_ISOKINETIC_SAMPLING_TIME; /* SAMPLING_ISOKINETIC_SAMPLING_VOLUME */
                    (void)Pm10Setup1_manageScreenTimeOrVolumeButton();
                    Pm10_h24enable = false;
                    Pm10Setup1_manageScreenH24Button();
                    
                    Sampling_manageIsokineticSamplingLimits();
                    debug_print("Sampling_manageIsokineticSamplingLimits Pm10Setup1_manageScreen");
                    (void)Pm10Setup1_manageScreenAspirationFlow();
                    (void)Pm10Setup1_manageScreenStopFlow();
                    common_startDelayValue = 0;
                    pm10_setup_1_serialLogTimeMin_value = 0;
                    (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_INIT);
                    
                    /* Screen Data and Graph */
                    common_samplingVacuum = FLOAT_TO_WORD(0.00f);
                    common_samplingMeterTemperature = FLOAT_TO_WORD(0.00f);
                    common_samplingVolume = FLOAT_TO_WORD(0.00f);
                    common_samplingFlowActual = FLOAT_TO_WORD(0.00f);
                    common_samplingFlowError = FLOAT_TO_WORD(0.00f);
                    pm10_data_1_volume_color = WHITE;
                    pm10_data_1_flow_color = WHITE;
                    pm10_data_1_flow_color = WHITE;
                    pm10_data_1_vacuum_color = WHITE;
                    pm10_data_1_meter_temperature_color = WHITE;
                    
                    /* Screen Graph */
                    Pm10Graph_DataToShow = GRAPH_DATA_TO_SHOW_FLOW;
                    (void)Pm10Graph_manageScreenButtonDataToShow();

                    /* Screen Waiting */
                    
                    /* Jump to ready */
                    Pm10_GuiState = PM10_GUI_STATE_READY;
                    (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_READY);
                    FLAG_Set(FLAG_PM10_SCREEN_COMMAND_GO_TO_READY); 
                    break;
                case PM10_GUI_STATE_READY: /* entered first time */
                    
                    break;
                case PM10_GUI_STATE_WAIT_START: /* start delay */
                    
                    break;
                case PM10_GUI_STATE_RUNNING: /* running */
                    
                    break;
                case PM10_GUI_STATE_PAUSED: /* paused */
                    
                    break;
                case PM10_GUI_STATE_PUMP_STOPPING:
                
                    break;
                case PM10_GUI_STATE_STOPPED: /* stopped */
                    
                    break;
            }
        }
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_START) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_START");
        Pm10_GuiState = PM10_GUI_STATE_RUNNING;
        /* Wait event from model to know if i'm in waiting or real running' in COMMON EVENT to set the enable on buttons */
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_START); 
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_PAUSE");
        Pm10_GuiState = PM10_GUI_STATE_PAUSED;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_PAUSED);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_PAUSE); 
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_STOP) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_STOP");
        Pm10_GuiState = PM10_GUI_STATE_STOPPED;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_STOPPED);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_PREV) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_PREV");
        (void)HMI_ChangeHmi(HMI_ID_PM10_GRAPH, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_NEXT) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_NEXT");
        (void)HMI_ChangeHmi(HMI_ID_PM10_DATA_1, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_TIME) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_TIME");
        common_isokineticSampling = SAMPLING_ISOKINETIC_SAMPLING_TIME;
        Sampling_manageIsokineticSamplingLimits();
        debug_print("Sampling_manageIsokineticSamplingLimits FLAG_PM10_SETUP_1_BUTTON_TIME");
        (void)Pm10Setup1_manageScreenTimeOrVolumeButton();
        Pm10Setup1_manageScreenH24Button();
        Pm10_manageScreenH24FieldsEnable(true, true);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_VOLUME) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_VOLUME");
        common_isokineticSampling = SAMPLING_ISOKINETIC_SAMPLING_VOLUME;
        Sampling_manageIsokineticSamplingLimits();
        debug_print("Sampling_manageIsokineticSamplingLimits FLAG_PM10_SETUP_1_BUTTON_VOLUME");
        (void)Pm10Setup1_manageScreenTimeOrVolumeButton();
        Pm10Setup1_manageScreenH24Button();
        Pm10_manageScreenH24FieldsEnable(true, true);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_H24_ENABLE) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_H24_ENABLE");
        Pm10_h24enable = true;
        Pm10Setup1_manageScreenH24Button();
        Pm10_manageScreenH24FieldsEnable(true, true);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_H24_DISABLE) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_H24_DISABLE");
        Pm10_h24enable = false;
        Pm10Setup1_manageScreenH24Button();
        Pm10_manageScreenH24FieldsEnable(true, true);
        Sampling_manageIsokineticSamplingLimits();
        debug_print("Sampling_manageIsokineticSamplingLimits FLAG_PM10_SETUP_1_BUTTON_H24_DISABLE");
    }

    if(FLAG_GetAndReset(FLAG_PM10_SETUP_1_BUTTON_BACK) != false)
    {
        debug_print("Event managed: FLAG_PM10_SETUP_1_BUTTON_BACK");
        Pm10_GuiState = PM10_GUI_STATE_INIT;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_INIT);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_BACK); 
        (void)HMI_ChangeHmi(HMI_ID_OPERATING_PROGRAM, HMI_GetHmiIdOnScreen());
    }
    
    /* If i'm inside the screen */
    if(localHmiId == HMI_GetHmiIdOnScreen())
    {

    }

    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10Setup_manageScreen %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10Data1_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    const HMI_ID_t localHmiId = HMI_ID_PM10_DATA_1;
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            switch(Pm10_GuiState)
            {
                case PM10_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
                    
                    break;
                case PM10_GUI_STATE_READY: /* entered first time */
                    
                    break;
                case PM10_GUI_STATE_WAIT_START: /* start delay */
                    
                    break;
                case PM10_GUI_STATE_RUNNING: /* running */
                    
                    break;
                case PM10_GUI_STATE_PAUSED: /* paused */
                    
                    break;
                case PM10_GUI_STATE_PUMP_STOPPING:
                
                    break;
                case PM10_GUI_STATE_STOPPED: /* stopped */
                    
                    break;
            }
        }
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_DATA_1_BUTTON_START) != false)
    {
        debug_print("Event managed: FLAG_PM10_DATA_1_BUTTON_START");
        Pm10_GuiState = PM10_GUI_STATE_RUNNING;
        /* Wait event from model to know if i'm in waiting or real running' in COMMON EVENT to set the enable on buttons */
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_START); 
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_DATA_1_BUTTON_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_PM10_DATA_1_BUTTON_PAUSE");
        Pm10_GuiState = PM10_GUI_STATE_PAUSED;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_PAUSED);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_PAUSE); 
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_DATA_1_BUTTON_STOP) != false)
    {
        debug_print("Event managed: FLAG_PM10_DATA_1_BUTTON_STOP");
        Pm10_GuiState = PM10_GUI_STATE_STOPPED;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_STOPPED);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_DATA_1_BUTTON_PREV) != false)
    {
        debug_print("Event managed: FLAG_PM10_DATA_1_BUTTON_PREV");
        (void)HMI_ChangeHmi(HMI_ID_PM10_SETUP_1, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_DATA_1_BUTTON_NEXT) != false)
    {
        debug_print("Event managed: FLAG_PM10_DATA_1_BUTTON_NEXT");
        (void)HMI_ChangeHmi(HMI_ID_PM10_GRAPH, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_DATA_1_BUTTON_BACK) != false)
    {
        debug_print("Event managed: FLAG_PM10_DATA_1_BUTTON_BACK");
        Pm10_GuiState = PM10_GUI_STATE_INIT;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_INIT);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_BACK);
        (void)HMI_ChangeHmi(HMI_ID_OPERATING_PROGRAM, HMI_GetHmiIdOnScreen()); 
    }
    
    /* If i'm inside the screen */
    if(localHmiId == HMI_GetHmiIdOnScreen())
    {
        Sampling_manageSamplingTimeOnScreen();
        
        Sampling_manageSamplingRemainingTimeOnScreen();
        
        Pm10_showDataOnCharts();
    }

    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10Data_manageScreen %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10Graph_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    const HMI_ID_t localHmiId = HMI_ID_PM10_GRAPH;
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            switch(Pm10_GuiState)
            {
                case PM10_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
                    
                    break;
                case PM10_GUI_STATE_READY: /* entered first time */
                    
                    break;
                case PM10_GUI_STATE_WAIT_START: /* start delay */
                    
                    break;
                case PM10_GUI_STATE_RUNNING: /* running */
                    
                    break;
                case PM10_GUI_STATE_PAUSED: /* paused */
                    
                    break;
                case PM10_GUI_STATE_PUMP_STOPPING:
                
                    break;
                case PM10_GUI_STATE_STOPPED: /* stopped */
                    
                    break;
            }
        }
        Pm10_modelCalculateStatisticNewDataForGraph = true;
        Sampling_modelManageButtonMobileScreen();
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_START) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_START");
        Pm10_GuiState = PM10_GUI_STATE_RUNNING;
        /* Wait event from model to know if i'm in waiting or real running' in COMMON EVENT to set the enable on buttons */
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_START); 
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_PAUSE");
        Pm10_GuiState = PM10_GUI_STATE_PAUSED;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_PAUSED);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_PAUSE); 
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_STOP) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_STOP");
        Pm10_GuiState = PM10_GUI_STATE_STOPPED;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_STOPPED);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_PREV) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_PREV");
        (void)HMI_ChangeHmi(HMI_ID_PM10_DATA_1, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_NEXT) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_NEXT");
        (void)HMI_ChangeHmi(HMI_ID_PM10_SETUP_1, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_BACK) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_BACK");
        Pm10_GuiState = PM10_GUI_STATE_INIT;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_INIT);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_BACK);
        (void)HMI_ChangeHmi(HMI_ID_OPERATING_PROGRAM, HMI_GetHmiIdOnScreen()); 
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_FLOW) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_FLOW");
        Pm10Graph_DataToShow = GRAPH_DATA_TO_SHOW_FLOW;
        (void)Pm10Graph_manageScreenButtonDataToShow();
        Pm10_modelCalculateStatisticNewDataForGraph = true;
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_ERROR) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_ERROR");
        Pm10Graph_DataToShow = GRAPH_DATA_TO_SHOW_ERROR;
        (void)Pm10Graph_manageScreenButtonDataToShow();
        Pm10_modelCalculateStatisticNewDataForGraph = true;
    }   
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_VACUUM) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_VACUUM");
        Pm10Graph_DataToShow = GRAPH_DATA_TO_SHOW_VACUUM;
        (void)Pm10Graph_manageScreenButtonDataToShow();
        Pm10_modelCalculateStatisticNewDataForGraph = true;
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_METER_TEMPERATURE) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_METER_TEMPERATURE");
        Pm10Graph_DataToShow = GRAPH_DATA_TO_SHOW_METER_TEMPERATURE;
        (void)Pm10Graph_manageScreenButtonDataToShow();
        Pm10_modelCalculateStatisticNewDataForGraph = true;
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_PREV_GRAPH) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_PREV_GRAPH");
        Pm10_modelCalculateStatisticNewDataForGraph = true;
        Sampling_modelInitMobileScreenPositionMoveToLeft();
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_GRAPH_BUTTON_NEXT_GRAPH) != false)
    {
        debug_print("Event managed: FLAG_PM10_GRAPH_BUTTON_NEXT_GRAPH");
        Pm10_modelCalculateStatisticNewDataForGraph = true;
        Sampling_modelInitMobileScreenPositionMoveToRight();
    }
    
    /* If i'm inside the screen */
    if(localHmiId == HMI_GetHmiIdOnScreen())
    {
        Pm10_showDataOnCharts();
    }
    
    if(Pm10_modelCalculateStatisticNewDataForGraph != false)
    {
        Pm10_modelCalculateStatisticNewDataForGraph = false;
        
        Sampling_modelCalculateMobileScreenPosition();
        
        Sampling_modelManageButtonMobileScreen();
        
        if(localHmiId == HMI_GetHmiIdOnScreen())
        {
            Sampling_drawDataOnGraph(Pm10Graph_DataToShow);
        }
    }

    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10Graph_manageScreen %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10Waiting_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    const HMI_ID_t localHmiId = HMI_ID_PM10_WAITING;
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            switch(Pm10_GuiState)
            {
                case PM10_GUI_STATE_INIT: /* IF THIS IS THE ENTERING SCREEN, handle here in INIT STATE THE VARIABLE FOR THE OTHERS SET OF SCREENS */
                    
                    break;
                case PM10_GUI_STATE_READY: /* entered first time */
                    
                    break;
                case PM10_GUI_STATE_WAIT_START: /* start delay */
                    
                    break;
                case PM10_GUI_STATE_RUNNING: /* running */
                    
                    break;
                case PM10_GUI_STATE_PAUSED: /* paused */
                    
                    break;
                case PM10_GUI_STATE_PUMP_STOPPING:
                
                    break;
                case PM10_GUI_STATE_STOPPED: /* stopped */
                    
                    break;
            }
        }
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_WAITING_BUTTON_START) != false)
    {
        debug_print("Event managed: FLAG_PM10_WAITING_BUTTON_START");
        Pm10_GuiState = PM10_GUI_STATE_RUNNING;
        /* Wait event from model to know if i'm in waiting or real running' in COMMON EVENT to set the enable on buttons */
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_START); 
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_WAITING_BUTTON_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_PM10_WAITING_BUTTON_PAUSE");
        Pm10_GuiState = PM10_GUI_STATE_PAUSED;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_PAUSED);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_PAUSE); 
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_WAITING_BUTTON_STOP) != false)
    {
        debug_print("Event managed: FLAG_PM10_WAITING_BUTTON_STOP");
        Pm10_GuiState = PM10_GUI_STATE_STOPPED;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_STOPPED);
        FLAG_Set(FLAG_PM10_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_EVENT_MODEL_STOPPED_IN_WAIT_START) != false) /* COMMON EVENT */
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_EVENT_MODEL_STOPPED_IN_WAIT_START");
        Pm10_GuiState = PM10_GUI_STATE_READY;
        (void)Pm10_manageScreenFieldsEnable(PM10_GUI_STATE_READY);
        (void)HMI_ChangeHmi(HMI_ID_PM10_SETUP_1, HMI_GetHmiIdOnScreen());
    }
    
    /* If i'm inside the screen */
    if(localHmiId == HMI_GetHmiIdOnScreen())
    {
        Sampling_manageSamplingRemainingTimeOnScreen();
    }

    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10Waiting_manageScreen %d", returnValue);
    }
    return returnValue;
}

#endif /* PM10_NORMAL_ENABLE */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL                                                                          */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
#ifdef PM10_THREAD_ENABLE
static STD_RETURN_t Pm10_CalculateStatisticFillData(Sampling_StatisticData_t* sd)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    float isoFlow = 0.0f;
    float actualTemperature = 0.0f;
    float actualFlow = 0.0f;
    float actualError = 0.0f;
    float actualVacuum = 0.0f;
    
    if(sd != NULL)
    {
        isoFlow = WORD_TO_FLOAT(pm10_setup_1_aspirationFlow_value);
        (void)NETWORK_GetMeterTemperature(&actualTemperature);
        if(NETWORK_GetLitersMinuteRead(&actualFlow) == STD_RETURN_OK)
        {        
            if(actualFlow > 0.0f)
            {
                actualError = (((actualFlow - isoFlow) * 100.0f)/isoFlow);
            }
        }
        (void)NETWORK_GetMeterPressure(&actualVacuum);
    
        sd->ErrorFlow = actualError;
        sd->Flow = actualFlow;
        sd->MeterTemperature = actualTemperature;
        sd->Vacuum = actualVacuum; 
        returnValue = STD_RETURN_OK;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_CalculateStatisticFillData %d", returnValue);
    }
    return returnValue;
    
    return returnValue;
}

static STD_RETURN_t Pm10_sendPumpStop(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    returnValue = NETWORK_SetPumpSetpoint(0.0f);
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_sendPumpStop %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_ErrorDetected(SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SAMPLING_ERROR_t error = SAMPLING_ERROR_NONE;
    PM10_InterfaceData_t interfaceData;
    ubyte noComm = NETWORK_IsInNoCommunicationState();
    float actualFlow;
    
    if(samplingError != NULL)
    {
        if((error == SAMPLING_ERROR_NONE) && (noComm != false))
        {
            error = SAMPLING_ERROR_NO_COMMUNICATION;
        }
        if(error == SAMPLING_ERROR_NONE)
        {
            returnValue = Pm10_GetDataFromView(&interfaceData);
            if(
                (returnValue == STD_RETURN_OK) && 
                (NETWORK_GetLitersMinuteRead(&actualFlow) == STD_RETURN_OK)
            )
            {
                if(Pm10_ModelState == PM10_MODEL_STATE_RUNNING)
                {
                    if(actualFlow < interfaceData.aspirationFlowError)
                    {
                        debug_print("CLOGGED FILTER %f %f", actualFlow, interfaceData.aspirationFlowError);
                        error = SAMPLING_ERROR_CLOGGED_FILTER;
                    }
                }
            }
        }
        *samplingError = error;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_ErrorDetected: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t Pm10_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    PM10_InterfaceData_t interfaceData; 
    ulong sampleTimeInMinutes = 0U;
    ulong sampleVolumeInLiters = 0U;
    
    if((reached != NULL) && (stopCause != NULL) && (samplingError != NULL))
    {
        returnValue = Pm10_GetDataFromView(&interfaceData);
        
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = Pm10_ErrorDetected(samplingError);
            
            if(returnValue == STD_RETURN_OK)
            {
                if(*samplingError != SAMPLING_ERROR_NONE) /* if no errors continue, otherwise stop */
                {
                    *reached = true;
                    *stopCause = SAMPLING_STOP_CAUSE_ERROR;
                    /* samplingError already filled */
                }
                else
                {
                    if(Pm10_ModelState == PM10_MODEL_STATE_RUNNING)
                    {
                        switch(interfaceData.samplingType)
                        {
                            case SAMPLING_ISOKINETIC_SAMPLING_TIME:
                                sampleTimeInMinutes = Sampling_SampleTimeInMinutes();
                                if(sampleTimeInMinutes >= interfaceData.time)
                                {
                                    *reached = true;
                                    *stopCause = SAMPLING_STOP_CAUSE_TIME;
                                    *samplingError = SAMPLING_ERROR_NONE;
                                }
                                else
                                {
                                    *reached = false;
                                }
                                break;
                            case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
                                sampleVolumeInLiters = Sampling_SampleVolumeInLiters();
                                if(sampleVolumeInLiters >= interfaceData.volume)
                                {
                                    *reached = true;
                                    *stopCause = SAMPLING_STOP_CAUSE_VOLUME;
                                    *samplingError = SAMPLING_ERROR_NONE;
                                }
                                else
                                {
                                    *reached = false;
                                }
                                break;
                            default:
                                returnValue = STD_RETURN_ERROR_SAMPLING_TYPE_UNKNOWN;
                                break;
                        } 
                    }
                    else
                    {
                        /* not in running */
                        *reached = false;
                    }
                }
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_AreSamplingTerminationConditionReached: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t Pm10_CheckAndManageAutoTerminationCondition(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte samplingTerminationCondition;
    returnValue = Pm10_AreSamplingTerminationConditionReached(&samplingTerminationCondition, &Sampling_StopCause, &Sampling_Error);
    if(returnValue == STD_RETURN_OK)
    {
        if(samplingTerminationCondition != false)
        {
            Pm10_sendPumpStop();
            debug_print("TERMINATION CONDITION REACHED");

            switch(Sampling_StopCause)
            {
                case SAMPLING_STOP_CAUSE_TIME:
                    BUZM_BUTTON_SOUND(); /* manual sound */
                    MessageScreen_openInfo(DYNAMIC_STRING_INFO_SAMPLING_STOPPED_TIME_REACHED, HMI_GetHmiIdOnScreen());
                    Pm10_ModelState = PM10_MODEL_STATE_PUMP_STOPPING;
                    FLAG_Set(FLAG_PM10_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED);
                    break;
                case SAMPLING_STOP_CAUSE_VOLUME:
                    BUZM_BUTTON_SOUND(); /* manual sound */
                    MessageScreen_openInfo(DYNAMIC_STRING_INFO_SAMPLING_STOPPED_VOLUME_REACHED, HMI_GetHmiIdOnScreen());
                    Pm10_ModelState = PM10_MODEL_STATE_PUMP_STOPPING;
                    FLAG_Set(FLAG_PM10_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED);
                    break;
                case SAMPLING_STOP_CAUSE_MANUAL:
                    /* manual sound not called, because manual stop is by pressing button */
                    MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen());
                    Pm10_ModelState = PM10_MODEL_STATE_PUMP_STOPPING;
                    FLAG_Set(FLAG_PM10_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED);
                    break;
                case SAMPLING_STOP_CAUSE_ERROR:
                    BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
                    MessageScreen_openError(DYNAMIC_STRING_ERROR_SAMPLING_STOPPED_DUE_TO_ERROR, Sampling_Error, HMI_GetHmiIdOnScreen());
                    (void)HMI_ChangeHmi(HMI_ID_MAIN, HMI_GetHmiIdOnScreen());
                    FLAG_Set(FLAG_PM10_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR);
                    if(Pm10_ModelState != PM10_MODEL_STATE_READY)
                    {
                        Pm10_model_writeFinalData();
                    }
                    Pm10_ModelState = PM10_MODEL_STATE_INIT; /* MODEL */
                    break;
                default:
                    break;
            }
        }
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_CheckAndManageAutoTerminationCondition %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_model_writeFinalData(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SAMPLING_SamplingTime_t samplingTime = {0};
    PM10_FinalData_t finalData = {0};
    float aspiratedLiters = 0.0;
    
    /* Finalize data*/
    Sampling_modelCalculateStatisticFinalize();
    
    /* Prepare final data to send to report*/
    (void)Sampling_GetSamplingTime(&samplingTime);
    
    (void)NETWORK_GetLitersTotalFromSamplingStart(&aspiratedLiters);
    
    finalData.samplingLenghtMin  = ((float)(samplingTime.counterSecond)) / 60.0f;
    finalData.averageFlow        = Sampling_GetFinalizedFlow();
    finalData.averageTemperature = Sampling_GetFinalizedMeterTemperature();
    finalData.aspiratedLiters    = aspiratedLiters;
    finalData.normalizedLiters   = Sampling_getNormalizedLiters(aspiratedLiters, (float)parameter_normalizationTemperatureValue, Sampling_GetFinalizedMeterTemperature());

    returnValue = ReportFile_writeFinalDataPm10(&finalData);

    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeFinalDataStopCauseAndError();
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_model_writeFinalData %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_RestoreDataForNewSampling(void)
{
    return NETWORK_RestoreDataNewSampling(); /* set to 0 the counters on GR */
}

static STD_RETURN_t Pm10_sendPumpSetPoint(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    PM10_InterfaceData_t interfaceData;
    
    returnValue = Pm10_GetDataFromView(&interfaceData);
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = NETWORK_SetPumpSetpoint(interfaceData.aspirationFlow);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_sendPumpSetPoint %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_modelStartFromReadyOrStopped(void)
{
    const REPORT_NAME_CODE_t reportType = REPORT_NAME_CODE_PM10;
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    PM10_InterfaceData_t interfaceData;
    
    returnValue = Pm10_GetDataFromView(&interfaceData);
    
    if(STD_RETURN_OK == returnValue)
    {
        returnValue = ReportFile_createName(reportType);
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeStarReportHeader(reportType);
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeInstrumentIds();
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeClientId(); /* anagrafica*/
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeOperatorId(); 
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_writeInitDataPm10(&interfaceData);
    }
    if(returnValue == STD_RETURN_OK)
    {
        slong delaySeconds = MINS_TO_SECS(interfaceData.samplingStartDelayMinutes);
        
        if((interfaceData.samplingType == SAMPLING_ISOKINETIC_SAMPLING_TIME) && (interfaceData.h24Enable != false))
        {
            /* delay is 0 in h24mode, but i set the delaySeconds to the seconds remaining till 00:00 in order to reuse the code */
            ubyte actualSecond = 0;
            ubyte actualMinute = 0;
            ubyte actualHour = 0;
            ubyte safetyCheck = 0;
            
            do
            {
                actualSecond = datarioSecondi;
                actualMinute = datarioMinuti;
                actualHour = datarioOre;
                safetyCheck++;
            }while(
                (safetyCheck < 10) && 
                ((actualSecond != datarioSecondi) || (actualMinute != datarioMinuti) || (actualHour != datarioOre))
            ); /* Avoid minute change, checking the values are the same */
                       
            delaySeconds = (60U - actualSecond);
            delaySeconds += ((59U - actualMinute) * 60U);
            delaySeconds += ((23U - actualHour) * 3600U);
            
            debug_print("H24 enabled, %2d:%2d:%2d - second to start: %d", actualHour, actualMinute, actualSecond, delaySeconds);
        }
        
        if(delaySeconds > 0U)
        {
            Pm10_RestoreDataForNewSampling(); /* set to 0 the counters on GR */
            if((interfaceData.samplingType == SAMPLING_ISOKINETIC_SAMPLING_TIME) && (interfaceData.h24Enable != false))
            {
                returnValue = ReportFile_writeH24DelayedStart();
            }
            else
            {   
                returnValue = ReportFile_writeDelayedStart();
            }
            Pm10_ModelState = PM10_MODEL_STATE_WAIT_START;
            FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_WAITING);
        }
        else
        {
            Sampling_modelInitMobileScreenPosition();
            Pm10_modelCalculateStatisticNewDataForGraph = false;
            Pm10_modelCalculateStatisticRefreshCounter = MODEL_CALCULATE_STATISTIC_REFRESH_COUNTER_VALUE;
            Sampling_modelCalculateStatisticInit();
            Pm10_RestoreDataForNewSampling();
            Pm10_sendPumpSetPoint();
            returnValue = ReportFile_writeStart();   
            (void)Pm10_LogOnSerialInit();
            Pm10_ModelState = PM10_MODEL_STATE_PUMP_START;
            FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_RUNNING);
        }
        Sampling_manageSamplingTimeInit(delaySeconds);
    }
    else
    {
        /* Stay in this state but re-enable the buttons */
        MessageScreen_openError(DYNAMIC_STRING_ERROR_SAMPLING_START_ERROR, returnValue, HMI_GetHmiIdOnScreen());
        FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_ERROR);
        Pm10_ModelState = PM10_MODEL_STATE_INIT;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStartFromReadyOrStopped %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_LogOnSerialInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    Pm10_LogCounterSecond = 0;
    PM10_InterfaceData_t interfaceData;
    
    returnValue = Pm10_GetDataFromView(&interfaceData);
    
    if((returnValue == STD_RETURN_OK) && (interfaceData.serialLogTineMin > 0U)) /* If log request init and open port */
    {        
        debug_print("OPEN PORT!!!!!!");
        SERIAL_COM_RETURN_t ret = SerialCom_open(
                                    &Pm10_serialCom, 
                                    PM10_LOG_PORT_DEFAULT,
                                    SERIAL_PORT_BAUDRATE_9600,
                                    SERIAL_PORT_DATA_BITS_8,
                                    SERIAL_PORT_PARITY_NONE,
                                    SERIAL_PORT_STOP_BITS_1
        );
        debug_print("SerialCom_open = %d", ret);
        if(ret != SERIAL_COM_RETURN_OK)
        {
            
        }
        else
        {
            
        }
        Pm10_sendOnSerial = false;
        Pm10_LogDataToSend = 0;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_LogOnSerialInit %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_LogOnSerialClose(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    Pm10_LogCounterSecond = 0;
    
    debug_print("CLOSE PORT!!!!!!");
    SerialCom_close(&Pm10_serialCom);
    
    Pm10_sendOnSerial = false;
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_LogOnSerialClose %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_LogOnSerialCheckFct(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SERIAL_COM_RETURN_t sendReturn;
    PM10_InterfaceData_t interfaceData; 
    SAMPLING_SamplingTime_t samplingTime;
    ulong actualIndex = 0;
    float floatValue = 0.0f;
    ubyte mounted = false;
    
    if(STD_RETURN_OK == Pm10_GetDataFromView(&interfaceData))
    {
        if((interfaceData.serialLogTineMin > 0) && (STD_RETURN_OK == Sampling_GetSamplingTime(&samplingTime)))
        {
            if((samplingTime.counterSecond % (interfaceData.serialLogTineMin * 5)) == 0)
            {
                if(Pm10_LogCounterSecond != samplingTime.counterSecond)
                {
                    Pm10_LogCounterSecond = samplingTime.counterSecond;
                    
                    Pm10_sendOnSerial = true;
                    
                    actualIndex += string_snprintf(
                                    &Pm10_TxBuffer[0], 
                                    sizeof(Pm10_TxBuffer), 
                                    "@7T-RUN;%02d/%02d/%04d;%02d:%02d:%02d;%03d;", 
                                    datarioGiorno,datarioMese,datarioAnno,
                                    datarioOre, datarioMinuti, datarioSecondi,
                                    Pm10_FilterNumber
                    );
                    
                    //actualFlow %.2f;
                    if(NETWORK_GetLitersMinuteRead(&floatValue) == STD_RETURN_OK)
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "%.2f", floatValue);
                    }
                    else
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "INV");
                    }
                    
                    actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), ";");

                    //actualMeterTemperature %.1f;
                    if(NETWORK_GetMeterTemperature(&floatValue) == STD_RETURN_OK)
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "%.1f", floatValue);
                    }
                    else
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "INV");
                    } 
                    
                    actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), ";");
                    
                    //actualLiterAspirated   %05d;
                    if(NETWORK_GetLitersTotalFromSamplingStart(&floatValue) == STD_RETURN_OK)
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "%05d", ((ulong)(floatValue)));
                    }
                    else
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "INV");
                    }
                    
                    actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), ";");
                    
                    //actualVoid   %.1f;
                    if(NETWORK_GetMeterPressure(&floatValue) == STD_RETURN_OK)
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "%.1f", floatValue);
                    }
                    else
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "INV");
                    }
                    
                    actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), ";");
                    
                    //actualBarometriPressure %.1f;
                    NETWORK_GetExternPressureMounted(&mounted);
                    
                    if((mounted != false) && (parameter_barometricPressure_useSensorValue != false))
                    {
                        if(STD_RETURN_OK == NETWORK_GetExternDigitalBarometer(&floatValue))
                        {
                            actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "%.1f", floatValue);
                        }
                        else
                        {
                            actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "INV");
                        } 
                    }
                    else
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "%.1f", ((float)(param_barometriPressure_value)));
                    }
                    
                    actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), ";");
                    
                    //ambientTemperature %.1f;
                    NETWORK_GetExternTemperatureMounted(&mounted);
                    
                    if(mounted != false)
                    {
                        if(STD_RETURN_OK == NETWORK_GetExternTemperatureSensor(&floatValue))
                        {
                            actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "%.1f", floatValue);
                        }
                        else
                        {
                            actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "INV");
                        } 
                    }
                    else
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "NA");
                    }   
                    
                    actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), ";");
                    
                    //filterTemperature %.1f; 
                    NETWORK_GetFilterTemperatureMounted(&mounted);
                    
                    if(mounted != false)
                    {
                        if(STD_RETURN_OK == NETWORK_GetFilterTemperature(&floatValue))
                        {
                            actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "%.1f", floatValue);
                        }
                        else
                        {
                            actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "INV");
                        } 
                    }
                    else
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "NA");
                    }
                    
                    actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), ";");
                    
                    //storageTemperature %.1f  
                    NETWORK_GetStorageTemperatureMounted(&mounted);
                    
                    if(mounted != false)
                    {
                        if(STD_RETURN_OK == NETWORK_GetStorageTemperature(&floatValue))
                        {
                            actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "%.1f", floatValue);
                        }
                        else
                        {
                            actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "INV");
                        } 
                    }
                    else
                    {
                        actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "NA");
                    }
                    
                    actualIndex += string_snprintf(&Pm10_TxBuffer[actualIndex], (sizeof(Pm10_TxBuffer) - actualIndex), "\r\n");

                    debug_print("LOG SERIAL REQUEST %d!!!!!!", actualIndex);
                    Pm10_LogDataToSend = actualIndex;
                    
                    debug_print("LOG SERIAL REQUEST!!!!!!");
                }
            }
        } 
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_LogOnSerialCheckFct %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_LogOnSerialWriteFct(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SERIAL_COM_RETURN_t sendReturn;
    
    if(Pm10_sendOnSerial != false)
    {
        /* Send data via serial */
        
        debug_print("Pm10_LogOnSerialWriteFct %d", Pm10_LogDataToSend);
        sendReturn = SerialCom_sendMessage(&Pm10_serialCom, (ubyte*)&Pm10_TxBuffer[0], Pm10_LogDataToSend, SERIAL_COM_NO_RESPONSE);
                
        switch(sendReturn)
        {
            case SERIAL_COM_RETURN_OK:
                Pm10_sendOnSerial = false;
                Pm10_LogDataToSend = 0U;
                debug_print("LOG SERIAL DONE!!!!!!");
                break;
            case SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY:
                debug_print("LOG TOO BUSY SERIAL!!!!!!");
                Pm10_sendOnSerial = false;
                Pm10_LogDataToSend = 0U;
                break;
            case SERIAL_COM_SERIAL_PORT_NOT_OPEN:
                debug_print("LOG PORT CLOSE - SEND ABORTED!!!!!!");
                Pm10_sendOnSerial = false;
                Pm10_LogDataToSend = 0U;
                break;
            default:
                break;
        }
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_LogOnSerialWriteFct %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Pm10_modelManageWarning(void)
{
    if(Pm10_ModelState != PM10_MODEL_STATE_INIT)
    {
        //(void)NETWORK_GetWarningCodeAndSubCode(&Pm10_warningCode, &Pm10_warningSubCode, true);
        if(Pm10_warningCode != SYST_WARNING_NONE)
        {
            debug_print("Pm10_warningCode %d Pm10_WarningSubCode %d", Pm10_warningCode, Pm10_warningSubCode);
            FLAG_Set(FLAG_PM10_MODEL_EVENT_WARNING_DETECTED);
            FLAG_Set(FLAG_PM10_MODEL_EVENT_WARNING_DETECTED_WRITE_REPORT);
        }
        
        ubyte writeReport = FLAG_GetAndReset(FLAG_PM10_MODEL_EVENT_WARNING_DETECTED_WRITE_REPORT);
        if(
            (Pm10_ModelState == PM10_MODEL_STATE_WAIT_START)      || 
            (Pm10_ModelState == PM10_MODEL_STATE_PUMP_START)      || 
            (Pm10_ModelState == PM10_MODEL_STATE_RUNNING)         ||
            (Pm10_ModelState == PM10_MODEL_STATE_PAUSED)          ||
            (Pm10_ModelState == PM10_MODEL_STATE_PAUSED_NO_POWER) ||
            (Pm10_ModelState == PM10_MODEL_STATE_PUMP_STOPPING)
        )
        {
            if(writeReport != false)
            {
                ReportFile_writeWarning(Pm10_warningCode, Pm10_warningSubCode);
            }
            
        }
        else
        {
            if(writeReport != false)
            {
                debug_print("Pm10_warningCode consumed but not written on report");
            }
        }
    }
    else
    {
        if(FLAG_GetAndReset(FLAG_PM10_MODEL_EVENT_WARNING_DETECTED_WRITE_REPORT) != false)
        {
            debug_print("Pm10_warningCode consumed but not written on report");
        }
    }
    return STD_RETURN_OK;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL INIT                                                                     */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    Pm10_ModelState = PM10_MODEL_STATE_INIT;
    Pm10_sendOnSerial = false;
    Pm10_LogDataToSend = 0;
    Pm10_FilterNumber = 1;
    Pm10_warningCode = SYST_WARNING_NONE;
    Pm10_warningSubCode = 0U; 
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelInit %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE INIT                                                               */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStateInit(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SERIAL_COM_RETURN_t comReturnValue;
    ubyte powerPresent;
        
    /* START CODE */
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_GO_TO_READY) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_GO_TO_READY");
        Pm10_ModelState = PM10_MODEL_STATE_READY;
        Pm10_warningCode = SYST_WARNING_NONE;
        Pm10_warningSubCode = 0U;      
        
        comReturnValue = SerialCom_init(
            /* SERIAL_COM_t* serial,                  */ &Pm10_serialCom, 
            /* ulong _timeoutResponseMs,              */ 250U, /* Useless used only in TX */
            /* ulong _maxNumberOfTimeout,             */ 1U,   /* Useless used only in TX */
            /* ulong _maxTryTxCommand,                */ 10U, 
            /* SerialCom_txPrepareFct _txPrepareFct,  */ NULL,
            /* SerialCom_txCallbackFct _txCallbackFct,*/ NULL,
            /* SerialCom_rxValidateFct _rxValidateFct,*/ NULL,
            /* SerialCom_rxCallbackFct _rxCallbackFct,*/ NULL,
            /* ubyte* _messageTxBufferPtr,            */ &Pm10_TxBuffer[0],
            /* ulong _messageTxBufferSize,            */ sizeof(Pm10_TxBuffer),
            /* ubyte* _messageRxBufferPtr,            */ NULL,
            /* ulong _messageRxBufferSize,            */ 0U,
            /* ulong _executionTime                   */ execTimeMs
        );
        
        if(SERIAL_COM_RETURN_OK == comReturnValue)
        {
            debug_print("Pm10 SerialCom_init OK");
        }
        else
        {
            debug_print("Pm10 SerialCom_init ERROR %d", comReturnValue);
        }
        
        NETWORK_GetExternalPower(&powerPresent);
        if(powerPresent == false)
        {
            debug_print("No power supply");
            FLAG_Set(FLAG_PM10_MODEL_EVENT_NO_POWER_SUPPLY);
        }
        
        Sampling_manageSamplingTimeInit(0);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStateInit %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE READY                                                              */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStateReady(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte powerPresent;
    /* START CODE */
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_START) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_START");
        NETWORK_GetExternalPower(&powerPresent);
        if(powerPresent != false)
        {
            returnValue = Pm10_modelStartFromReadyOrStopped();
        }
        else
        {
            debug_print("No power supply");
            FLAG_Set(FLAG_PM10_MODEL_EVENT_NO_POWER_SUPPLY);
        }
    }
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_BACK) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_BACK");
        Pm10_ModelState = PM10_MODEL_STATE_INIT;
        Pm10_LogOnSerialClose();
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_CheckAndManageAutoTerminationCondition();
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStateReady %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE WAIT START                                                         */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStateWaitStart(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte powerPresent;
    /* START CODE */
    NETWORK_GetExternalPower(&powerPresent);
    
    Sampling_manageSamplingTime(); /* manage timer, with true should be called previously */
            
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_PAUSE");
        returnValue = ReportFile_writeDelayedPaused();
        Pm10_ModelState = PM10_MODEL_STATE_PAUSED;
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_STOP) != FALSE)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_STOP");
        Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
        Sampling_Error = SAMPLING_ERROR_NONE;
        FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_STOPPED_IN_WAIT_START);
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
        ReportFile_deleteReport();
        Pm10_ModelState = PM10_MODEL_STATE_READY;
    }
    
    if(powerPresent == false)
    {
        debug_print("Event: NO POWER SUPPLY");
        FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_STOPPED_IN_WAIT_START);
        FLAG_Set(FLAG_PM10_MODEL_EVENT_NO_POWER_SUPPLY);
        ReportFile_deleteReport();
        Pm10_ModelState = PM10_MODEL_STATE_READY;
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ubyte conditionTime = (Sampling_manageSamplingTimeIsInWaitStart() == false) ? true : false; /* time is expired */
        ubyte forceStart = FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_START); /* The user want to start immedialtly*/
        
        if(conditionTime != false)  
        {
            BUZM_BUTTON_SOUND(); /* manual sound */
            returnValue = ReportFile_writeStart();
        }
        if(forceStart != false)
        {
            debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_START");
            Sampling_manageSamplingTimeForceStart();
            returnValue = ReportFile_writeStartForced();
        }
        if((conditionTime != false) || (forceStart != false))
        {
            Sampling_modelInitMobileScreenPosition();
            Pm10_modelCalculateStatisticNewDataForGraph = false;
            Pm10_modelCalculateStatisticRefreshCounter = MODEL_CALCULATE_STATISTIC_REFRESH_COUNTER_VALUE;
            Sampling_modelCalculateStatisticInit();
            Pm10_sendPumpSetPoint();
            (void)Pm10_LogOnSerialInit();
            Pm10_ModelState = PM10_MODEL_STATE_PUMP_START;
            FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_RUNNING);
        }
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_CheckAndManageAutoTerminationCondition();
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStateWaitStart %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE PUMP START                                                         */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStatePumpStart(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte pumpRunning = false;
    ubyte pumpRegulation = false;
    ubyte powerPresent;
    /* START CODE */
    NETWORK_GetExternalPower(&powerPresent);
    
    Sampling_manageSamplingTime(); /* manage timer, with true should be called previously */
            
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_PAUSE");
        Pm10_sendPumpStop();
        returnValue = ReportFile_writeRunningPaused();
        Pm10_ModelState = PM10_MODEL_STATE_PAUSED;
    }
    
    if(powerPresent == false)
    {
        debug_print("Event: NO POWER SUPPLY");
        Pm10_sendPumpStop();
        returnValue = ReportFile_writePausedNoPowerSupply();
        Pm10_ModelState = PM10_MODEL_STATE_PAUSED_NO_POWER;
        FLAG_Set(FLAG_PM10_MODEL_EVENT_NO_POWER_SUPPLY_RUNNING);
    }
    
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_STOP) != FALSE)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_STOP");
        Pm10_sendPumpStop();
        Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
        Sampling_Error = SAMPLING_ERROR_NONE;
        Pm10_ModelState = PM10_MODEL_STATE_PUMP_STOPPING;
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
    }
    
    if( 
        (NETWORK_GetPumpRunning(&pumpRunning) == STD_RETURN_OK) &&
        (NETWORK_GetPumpRegulation(&pumpRegulation) == STD_RETURN_OK)
    )
    {
        if((pumpRunning != false) && (pumpRegulation == false))
        {
            /* pump running and regulated */
            Pm10_ModelState = PM10_MODEL_STATE_RUNNING;
        }
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_LogOnSerialCheckFct();
    }    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_CheckAndManageAutoTerminationCondition();
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStatePumpStart %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE RUNNING                                                            */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStateRunning(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte powerPresent;
    /* START CODE */
    NETWORK_GetExternalPower(&powerPresent);
    Sampling_manageSamplingTime(); /* manage timer, with true should be called previously */
            
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_PAUSE) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_PAUSE");
        Pm10_sendPumpStop();
        returnValue = ReportFile_writeRunningPaused();
        Pm10_ModelState = PM10_MODEL_STATE_PAUSED;
    }
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_STOP) != FALSE)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_STOP");
        Pm10_sendPumpStop();
        Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
        Sampling_Error = SAMPLING_ERROR_NONE;
        Pm10_ModelState = PM10_MODEL_STATE_PUMP_STOPPING;
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
    }
    
    if(powerPresent == false)
    {
        debug_print("Event: NO POWER SUPPLY");
        Pm10_sendPumpStop();
        returnValue = ReportFile_writePausedNoPowerSupply();
        Pm10_ModelState = PM10_MODEL_STATE_PAUSED_NO_POWER;
        FLAG_Set(FLAG_PM10_MODEL_EVENT_NO_POWER_SUPPLY_RUNNING);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_LogOnSerialCheckFct();
    }    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_CheckAndManageAutoTerminationCondition();
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStateRunning %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE PAUSED                                                             */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStatePaused(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* NO MANAGE OF NO POWER - The no power is managed in PM10_MODEL_STATE_WAIT_START and PM10_MODEL_STATE_PUMP_START, in one run they will go in  */
    
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_START) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_START");
        if(Sampling_manageSamplingTimeIsInWaitStart() != false)
        {
            returnValue = ReportFile_writeDelayedResumed();
            Pm10_ModelState = PM10_MODEL_STATE_WAIT_START;
            FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_WAITING);
        }
        else
        {
            Pm10_sendPumpSetPoint();
            returnValue = ReportFile_writeRunningResumed();
            Pm10_ModelState = PM10_MODEL_STATE_PUMP_START;
            FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_RUNNING);
        }
    }  
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_STOP) != FALSE)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_STOP");
        Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
        Sampling_Error = SAMPLING_ERROR_NONE;
        //TODO maybe send the event pump stopping also if the pump maybe is stopped 
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
        if(Sampling_manageSamplingTimeIsInWaitStart() != false)
        {
            FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_STOPPED_IN_WAIT_START);
            Pm10_ModelState = PM10_MODEL_STATE_READY;
        }
        else
        {
            Pm10_ModelState = PM10_MODEL_STATE_PUMP_STOPPING;
        }
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_CheckAndManageAutoTerminationCondition();
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStatePaused %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE PAUSED NO POWER                                                    */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStatePausedNoPower(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte powerPresent;
    /* START CODE */
    NETWORK_GetExternalPower(&powerPresent);

    if(powerPresent != false)
    {
        debug_print("Event: POWER SUPPLY ON");
        Pm10_sendPumpSetPoint();
        returnValue = ReportFile_writeRestartFromNoPowerSupply();
        Pm10_ModelState = PM10_MODEL_STATE_PUMP_START;
        FLAG_Set(FLAG_PM10_SCREEN_EVENT_MODEL_RUNNING);
        FLAG_Set(FLAG_PM10_MODEL_EVENT_BACK_FROM_NO_POWER_SUPPLY_RUNNING);
    } 
     
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_STOP) != FALSE)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_STOP");
        Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
        Sampling_Error = SAMPLING_ERROR_NONE;
        //TODO maybe send the event pump stopping also if the pump maybe is stopped 
        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SAMPLING_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
        Pm10_ModelState = PM10_MODEL_STATE_PUMP_STOPPING;
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_CheckAndManageAutoTerminationCondition();
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStatePaused %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE PUMP STOPPING                                                      */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStatePumpStopping(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte pumpRunning = false;
    ubyte pumpRegulation = false;
    /* START CODE */
    if( 
        (NETWORK_GetPumpRunning(&pumpRunning) == STD_RETURN_OK) &&
        (NETWORK_GetPumpRegulation(&pumpRegulation) == STD_RETURN_OK)
    )
    {
        if((pumpRunning == false) && (pumpRegulation == false))
        {
            /* pump running and regulated */
            Pm10_ModelState = PM10_MODEL_STATE_STOPPED;
            
            Pm10_model_writeFinalData();
            
            FLAG_Set(FLAG_PM10_SCREEN_EVENT_PUMP_STOPPED);
            
            Pm10_LogOnSerialClose();
        }
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_LogOnSerialCheckFct();
    }    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_CheckAndManageAutoTerminationCondition();
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStatePumpStopping %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE STOPPED                                                            */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStateStopped(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_BACK) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_BACK");
        Pm10_ModelState = PM10_MODEL_STATE_INIT;
        Pm10_LogOnSerialClose();
    }
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_START) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_START");
        returnValue = Pm10_modelStartFromReadyOrStopped();
    }
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Pm10_CheckAndManageAutoTerminationCondition();
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStateStopped %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE ERROR                                                              */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_modelStateError(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(FLAG_GetAndReset(FLAG_PM10_SCREEN_COMMAND_BACK) != false)
    {
        debug_print("Event managed: FLAG_PM10_SCREEN_COMMAND_BACK");
        Pm10_ModelState = PM10_MODEL_STATE_INIT;
        
        Pm10_LogOnSerialClose();
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_modelStateError %d", returnValue);
    }
    return returnValue;
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */
/*                                                          MODEL STATE MACHINE                                                            */
/* --------------------------------------------------------------------------------------------------------------------------------------- */
static STD_RETURN_t Pm10_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SYST_ERROR_t systErrorCode;
    ubyte systErrorSubCode;
    /* START CODE */
    static PM10_MODEL_STATE_t Pm10_LastModelState = PM10_MODEL_STATE_INIT;
    if(Pm10_ModelState != Pm10_LastModelState)
    {
        debug_print("P10 MOD STATE from %d %s to %d %s", Pm10_LastModelState, Pm10_modelStateNames[Pm10_LastModelState], Pm10_ModelState, Pm10_modelStateNames[Pm10_ModelState]);
        Pm10_LastModelState = Pm10_ModelState;
    }
    
    Sampling_modelCalculateStatisticNewMinuteDetect();
    
    if(FLAG_GetAndReset(FLAG_PM10_MODEL_EVENT_NEW_NETWORK_DATA_MESSAGE) != false)
    {
        /* Evey time arrive a new message with all the data */
        if(
            (Pm10_ModelState == PM10_MODEL_STATE_PUMP_START   ) || 
            (Pm10_ModelState == PM10_MODEL_STATE_RUNNING      ) || 
            (Pm10_ModelState == PM10_MODEL_STATE_PUMP_STOPPING)
        )
        {
            Sampling_StatisticData_t sd;
            Sampling_modelCalculateStatisticStructInit(&sd);     /* FILL ALL FIELDS WITH 0's */       
            (void)Pm10_CalculateStatisticFillData(&sd);          /* FILL ONLY NECESSARY FIELDS */
            Sampling_modelCalculateStatistic(&sd);               /* EVERY TIME A MESSAGE ARRIVE */
            
            Pm10_modelCalculateStatisticRefreshCounter++;
            if(Pm10_modelCalculateStatisticRefreshCounter >= MODEL_CALCULATE_STATISTIC_REFRESH_COUNTER_VALUE)
            {
                Pm10_modelCalculateStatisticRefreshCounter = 0U;
                Pm10_modelCalculateStatisticNewDataForGraph = true;
            }
        }
    }
    
//    TEST FOR READ THE ERROR OR WARNING FROM GR
//    (void)NETWORK_GetErrorCodeAndSubCode(&systErrorCode, &systErrorSubCode, true);
//    if(systErrorCode != SYST_ERROR_NONE)
//    {
//        debug_print("systErrorCode %d systErrorSubCode %d", systErrorCode, systErrorSubCode);
//    }
    
    (void)Pm10_modelManageWarning();
    
    (void)Pm10_LogOnSerialWriteFct();
    
    switch(Pm10_ModelState)
    {
        case PM10_MODEL_STATE_INIT:
            returnValue = Pm10_modelStateInit(execTimeMs);
            break;
        case PM10_MODEL_STATE_READY:
            returnValue = Pm10_modelStateReady(execTimeMs);
            break;
        case PM10_MODEL_STATE_WAIT_START:
            returnValue = Pm10_modelStateWaitStart(execTimeMs);
            break;
        case PM10_MODEL_STATE_PUMP_START:
            returnValue = Pm10_modelStatePumpStart(execTimeMs);
            break;
        case PM10_MODEL_STATE_RUNNING:
            returnValue = Pm10_modelStateRunning(execTimeMs);
            break;
        case PM10_MODEL_STATE_PAUSED:
            returnValue = Pm10_modelStatePaused(execTimeMs);
            break;
        case PM10_MODEL_STATE_PAUSED_NO_POWER:
            Pm10_modelStatePausedNoPower(execTimeMs);
            break;
        case PM10_MODEL_STATE_PUMP_STOPPING:
            returnValue = Pm10_modelStatePumpStopping(execTimeMs);
            break;
        case PM10_MODEL_STATE_STOPPED:
            returnValue = Pm10_modelStateStopped(execTimeMs);
            break;
        case PM10_MODEL_STATE_ERROR:
            returnValue = Pm10_modelStateError(execTimeMs);
            break;
        default:
            break;
    }
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Pm10_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef PM10_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void pm10_init()
{
    (void)Pm10_initVariable();
}

#ifdef PM10_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void pm10()
{
    static PM10_GUI_STATE_t Pm10_LastGuiState = PM10_GUI_STATE_INIT;
#ifdef PM10_ENABLE_DEBUG_COUNTERS
    Pm10_NormalRoutine++;
#endif
    if(Pm10_LastGuiState != Pm10_GuiState)
    {
        debug_print("P10 GUI STATE from %d %s to %d %s", Pm10_LastGuiState, Pm10_guiStateNames[Pm10_LastGuiState], Pm10_GuiState, Pm10_guiStateNames[Pm10_GuiState]);
        Pm10_LastGuiState = Pm10_GuiState;
    }
    (void)Pm10Setup1_manageButtons(Execution_Normal);
    (void)Pm10Data1_manageButtons(Execution_Normal);
    (void)Pm10Graph_manageButtons(Execution_Normal);
    (void)Pm10Waiting_manageButtons(Execution_Normal);
    (void)Pm10_manageScreenCommonEvent(Execution_Normal);
    (void)Pm10Setup1_manageScreen(Execution_Normal);
    (void)Pm10Data1_manageScreen(Execution_Normal);
    (void)Pm10Graph_manageScreen(Execution_Normal);
    (void)Pm10Waiting_manageScreen(Execution_Normal);
}
#endif /* #ifdef PM10_NORMAL_ENABLE */

#ifdef PM10_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void pm10_fast()
{
#ifdef PM10_ENABLE_DEBUG_COUNTERS
    Pm10_FastRoutine++;
#endif
}
#endif /* #ifdef PM10_FAST_ENABLE */

#ifdef PM10_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void pm10_low_priority()
{
#ifdef PM10_ENABLE_DEBUG_COUNTERS
    Pm10_LowPrioRoutine++;
#endif
}
#endif /* #ifdef PM10_LOW_PRIO_ENABLE */

#ifdef PM10_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void pm10_thread()
{
    ulong execTime = PM10_THREAD_SLEEP_MS;

    (void)Pm10_modelInit();

    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef PM10_ENABLE_DEBUG_COUNTERS
        Pm10_ThreadRoutine++; /* Only for debug */
#endif

        (void)Pm10_model(execTime);
    }
}
#endif /* #ifdef PM10_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
void pm10_close()
{
    Pm10_LogOnSerialClose();
}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void pm10_shutdown()
//{
//}
