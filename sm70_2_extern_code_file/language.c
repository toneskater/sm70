/*
 * language.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define LANGUAGE_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"

/* Private define ------------------------------------------------------------*/
#define HMI_MY_ID HMI_ID_LANGUAGE

/* #define LANGUAGE_ENABLE_DEBUG_COUNTERS */
#define LANGUAGE_NORMAL_ENABLE
/* #define LANGUAGE_FAST_ENABLE */
/* #define LANGUAGE_LOW_PRIO_ENABLE */
/* #define LANGUAGE_THREAD_ENABLE */

#define LANGUAGE_THREAD_SLEEP_MS 50U

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Language_logFlag(FLAG_t flag);
static STD_RETURN_t Language_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Language_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Language_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Language_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t Language_initVariable(void);
#ifdef LANGUAGE_NORMAL_ENABLE
static STD_RETURN_t Language_manageButtons(ulong execTimeMs);
static STD_RETURN_t Language_manageScreen(ulong execTimeMs);
#endif /* #ifdef LANGUAGE_NORMAL_ENABLE */
#ifdef LANGUAGE_THREAD_ENABLE
static STD_RETURN_t Language_modelInit(void);
static STD_RETURN_t Language_model(ulong execTimeMs);
#endif /* #ifdef LANGUAGE_THREAD_ENABLE */

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Language_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Language_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Language_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Language_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t Language_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t Language_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(Languages_ID != language_savedLanguage) /* At startup the language id is set to zero */
    {
        debug_print("Loaded language from retention variable");
        Languages_ID = language_savedLanguage;
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Language_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef LANGUAGE_NORMAL_ENABLE
static STD_RETURN_t Language_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(language_backButton_released != false) /* These flag is reset by GUI */
    {
        language_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Language_logFlag(FLAG_LANGUAGE_BUTTON_BACK);
    }
    if(language_italianButton_released != false) /* These flag is reset by GUI */
    {
        language_italianButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Language_logFlag(FLAG_LANGUAGE_BUTTON_ITALIAN);
    }
    if(language_englishButton_released != false) /* These flag is reset by GUI */
    {
        language_englishButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Language_logFlag(FLAG_LANGUAGE_BUTTON_ENGLISH);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Language_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef LANGUAGE_NORMAL_ENABLE */

#ifdef LANGUAGE_NORMAL_ENABLE
static void Language_ManageButtonColor(void)
{
    Language_selectedLanguage = Languages_ID;
    switch(Languages_ID)
    {
        case LANGUAGE_ENGLISH_DEFAULT:
            language_italianButton_backgroundColor = YELLOW;
            language_englishButton_backgroundColor = DARKGREEN;
            break;
        case LANGUAGE_ITALIAN:
            language_italianButton_backgroundColor = DARKGREEN;
            language_englishButton_backgroundColor = YELLOW;
            break;
        default:
            break;
    }
}

static STD_RETURN_t Language_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(MessageScreen_isEnteredHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            Language_ManageButtonColor();
        }
    }
    
    if(FLAG_GetAndReset(FLAG_LANGUAGE_BUTTON_BACK) != false)
    {
        HMI_ChangeHmi(HMI_ID_SETUP, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_LANGUAGE_BUTTON_ITALIAN) != false)
    {
        Language_selectedLanguage = LANGUAGE_ITALIAN;
        Languages_ID = Language_selectedLanguage;
        language_savedLanguage = Language_selectedLanguage;
        Language_ManageButtonColor();
    }
    
    if(FLAG_GetAndReset(FLAG_LANGUAGE_BUTTON_ENGLISH) != false)
    {
        Language_selectedLanguage = LANGUAGE_ENGLISH_DEFAULT;
        Languages_ID = Language_selectedLanguage;
        language_savedLanguage = Language_selectedLanguage;
        Language_ManageButtonColor();
    }
    
    if(MessageScreen_isExitedHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Language_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef LANGUAGE_THREAD_ENABLE
static STD_RETURN_t Language_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Language_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef LANGUAGE_THREAD_ENABLE */

#ifdef LANGUAGE_THREAD_ENABLE
static STD_RETURN_t Language_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Language_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef LANGUAGE_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void language_init()
{
    (void)Language_initVariable();
}

#ifdef LANGUAGE_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void language()
{
#ifdef LANGUAGE_ENABLE_DEBUG_COUNTERS
    Language_NormalRoutine++;
#endif
    (void)Language_manageButtons(Execution_Normal);
    (void)Language_manageScreen(Execution_Normal);
}
#endif /* #ifdef LANGUAGE_NORMAL_ENABLE */

#ifdef LANGUAGE_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void language_fast()
{
#ifdef LANGUAGE_ENABLE_DEBUG_COUNTERS
    Language_FastRoutine++;
#endif
}
#endif /* #ifdef LANGUAGE_FAST_ENABLE */

#ifdef LANGUAGE_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void language_low_priority()
{
#ifdef LANGUAGE_ENABLE_DEBUG_COUNTERS
    Language_LowPrioRoutine++;
#endif
}
#endif /* #ifdef LANGUAGE_LOW_PRIO_ENABLE */

#ifdef LANGUAGE_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void language_thread()
{
    ulong execTime = LANGUAGE_THREAD_SLEEP_MS;
    
    (void)Language_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef LANGUAGE_ENABLE_DEBUG_COUNTERS
        Language_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)Language_model(execTime);
    }
}
#endif /* #ifdef LANGUAGE_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void language_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void language_shutdown()
//{
//}
