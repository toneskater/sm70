/*
 * parameter.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define PARAMETER_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"

/* Private define ------------------------------------------------------------*/

/* #define PARAMETER_ENABLE_DEBUG_COUNTERS */
#define PARAMETER_NORMAL_ENABLE
/* #define PARAMETER_FAST_ENABLE */
/* #define PARAMETER_LOW_PRIO_ENABLE */
/* #define PARAMETER_THREAD_ENABLE */

#define PARAMETER_THREAD_SLEEP_MS 50U

#define PARAMETER_BAROMETRIC_PRESSUR_MIN 600U
#define PARAMETER_BAROMETRIC_PRESSUR_MAX 1400U
#define PARAMETER_BAROMETRIC_PRESSUR_DEFAULT_VALUE 1013U

#define PARAMETER_K_VALUE_MIN 0.5f
#define PARAMETER_K_VALUE_MAX 1.5f
#define PARAMETER_K_VALUE_DEFAULT_VALUE 0.831f

#define PARAMETER_NORM_TEMP_MIN 0U
#define PARAMETER_NORM_TEMP_MAX 50U
#define PARAMETER_NORM_TEMP_DEFAULT_VALUE 0U

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Parameter_logFlag(FLAG_t flag);
static STD_RETURN_t Parameter_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Parameter_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Parameter_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Parameter_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t Parameter_initVariable(void);
static void Parameter_ManageMeasureUnit(void);


#ifdef PARAMETER_NORMAL_ENABLE
static STD_RETURN_t Parameter_manageButtons(ulong execTimeMs);
static STD_RETURN_t Parameter_manageScreen(ulong execTimeMs);
#endif /* #ifdef PARAMETER_NORMAL_ENABLE */
#ifdef PARAMETER_THREAD_ENABLE
static STD_RETURN_t Parameter_modelInit(void);
static STD_RETURN_t Parameter_model(ulong execTimeMs);
#endif /* #ifdef PARAMETER_THREAD_ENABLE */

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Parameter_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Parameter_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Parameter_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Parameter_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t Parameter_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t Parameter_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    /* Load parameter to store in other variables */
    Parameter_ManageMeasureUnit();
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Parameter_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef PARAMETER_NORMAL_ENABLE
static STD_RETURN_t Parameter_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(parameter_backButton_released != false) /* These flag is reset by GUI */
    {
        parameter_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Parameter_logFlag(FLAG_PARAMETER_BUTTON_BACK);
    }
    if(parameter_measureUnitmmH2OButton_released != false) /* These flag is reset by GUI */
    {
        parameter_measureUnitmmH2OButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Parameter_logFlag(FLAG_PARAMETER_BUTTON_MEASUR_UNIT_MMH2O);
    }
    if(parameter_measureUnitPaButton_released != false) /* These flag is reset by GUI */
    {
        parameter_measureUnitPaButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Parameter_logFlag(FLAG_PARAMETER_BUTTON_MEASUR_UNIT_PA);
    }
    if(parameter_barometricPressureButton_released != false) /* These flag is reset by GUI */
    {
        parameter_barometricPressureButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Parameter_logFlag(FLAG_PARAMETER_BUTTON_BAROMETRIC_PRESSURE_ENABLE);
    }
    if(parameter_operator_eventAccepted != false)
    {
        parameter_operator_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Parameter_logString("Operator", (ushort*)&parameter_operator_string_1, 20U);
    }
    
    if(parameter_barometricPressure_eventAccepted != false)
    {
        parameter_barometricPressure_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Parameter_logUnsignedValue("Barometric Pressure", param_barometriPressure_value);
    }
    if(parameter_normalizationTemperatureValue_eventAccepted != false)
    {
        parameter_normalizationTemperatureValue_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Parameter_logUnsignedValue("Normalization Temperature", parameter_normalizationTemperatureValue);
    }
    
    if(parameter_kCostant_eventAccepted != false)
    {
        parameter_kCostant_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)Parameter_logFloatValue("K Value", parameter_kConstant_value);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Parameter_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef PARAMETER_NORMAL_ENABLE */

#ifdef PARAMETER_NORMAL_ENABLE
static void Parameter_ManageMeasureUnit(void)
{
    common_measureUnit = parameter_measureUnit;
    switch(parameter_measureUnit)
    {
        case SAMPLING_MEASURE_UNIT_MMH20:
            parameter_measureUnitmmH2OButton_backgroundColor = DARKGREEN;
            parameter_measureUnitPaButton_backgroundColor = YELLOW;
            break;
        case SAMPLING_MEASURE_UNIT_PA:
            parameter_measureUnitmmH2OButton_backgroundColor = YELLOW;
            parameter_measureUnitPaButton_backgroundColor = DARKGREEN;
            break;
        default:
            break;
    }
}

static void Parameter_ManageBarometricPressure(void)
{
    if(parameter_barometricPressure_useSensorValue != false)
    {
        parameter_barometricPressureButton_backgroundColor = DARKGREEN;
        parameter_barometricPressure_enable = false;
        param_barometriPressure_backgroundColor = DARKGRAY;
    }
    else
    {
        parameter_barometricPressureButton_backgroundColor = YELLOW;
        parameter_barometricPressure_enable = true;
        param_barometriPressure_backgroundColor = WHITE;
    }
}

static STD_RETURN_t Parameter_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    const HMI_ID_t localHmiId = HMI_ID_PARAMETER;
    ubyte flag;
    /* START CODE */
    
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            /* Init the retained value if out of range */
            if((param_barometriPressure_value < PARAMETER_BAROMETRIC_PRESSUR_MIN) || (param_barometriPressure_value > PARAMETER_BAROMETRIC_PRESSUR_MAX))
            {
                param_barometriPressure_value = PARAMETER_BAROMETRIC_PRESSUR_DEFAULT_VALUE;
            }
            /* Init the retained value if out of range */
            if((parameter_kConstant_value < FLOAT_TO_WORD(PARAMETER_K_VALUE_MIN)) || (parameter_kConstant_value > FLOAT_TO_WORD(PARAMETER_K_VALUE_MAX)))
            {
                parameter_kConstant_value = FLOAT_TO_WORD(PARAMETER_K_VALUE_DEFAULT_VALUE);
            }
            if((parameter_normalizationTemperatureValue < PARAMETER_NORM_TEMP_MIN) || (parameter_normalizationTemperatureValue > PARAMETER_NORM_TEMP_MAX))
            {
                parameter_normalizationTemperatureValue = PARAMETER_NORM_TEMP_DEFAULT_VALUE;
            }
            if(parameter_measureUnit >= SAMPLING_MEASURE_UNIT_MAX_NUMBER)
            {
                parameter_measureUnit = SAMPLING_MEASURE_UNIT_MMH20;
            }
            (void)NETWORK_GetExternPressureMounted(&flag);
            parameter_barometricPressureButton_visible2 = flag;
            if(parameter_barometricPressureButton_visible2 != false)
            {
                Parameter_ManageBarometricPressure();
            }
            else
            {
                param_barometriPressure_backgroundColor = WHITE;
                parameter_barometricPressure_enable = true;
            }
            Parameter_ManageMeasureUnit();
        }
    }
    
    if(FLAG_GetAndReset(FLAG_PARAMETER_BUTTON_BACK) != false)
    {
        HMI_ChangeHmi(HMI_ID_SETUP, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_PARAMETER_BUTTON_MEASUR_UNIT_MMH2O) != false) 
    {
        parameter_measureUnit = SAMPLING_MEASURE_UNIT_MMH20;
        Parameter_ManageMeasureUnit();
    }
    if(FLAG_GetAndReset(FLAG_PARAMETER_BUTTON_MEASUR_UNIT_PA) != false) 
    {
        parameter_measureUnit = SAMPLING_MEASURE_UNIT_PA;
        Parameter_ManageMeasureUnit();
    }
    if(FLAG_GetAndReset(FLAG_PARAMETER_BUTTON_BAROMETRIC_PRESSURE_ENABLE) != false) 
    {
        if(parameter_barometricPressure_useSensorValue != false)
        {
            parameter_barometricPressure_useSensorValue = false;
        }
        else
        {
            parameter_barometricPressure_useSensorValue = true;
        }
        Parameter_ManageBarometricPressure();
    }
    
    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Parameter_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef PARAMETER_THREAD_ENABLE
static STD_RETURN_t Parameter_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Parameter_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef PARAMETER_THREAD_ENABLE */

#ifdef PARAMETER_THREAD_ENABLE
static STD_RETURN_t Parameter_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Parameter_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef PARAMETER_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void parameter_init()
{
    (void)Parameter_initVariable();
}

#ifdef PARAMETER_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void parameter()
{
#ifdef PARAMETER_ENABLE_DEBUG_COUNTERS
    Parameter_NormalRoutine++;
#endif
    (void)Parameter_manageButtons(Execution_Normal);
    (void)Parameter_manageScreen(Execution_Normal);
}
#endif /* #ifdef PARAMETER_NORMAL_ENABLE */

#ifdef PARAMETER_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void parameter_fast()
{
#ifdef PARAMETER_ENABLE_DEBUG_COUNTERS
    Parameter_FastRoutine++;
#endif
}
#endif /* #ifdef PARAMETER_FAST_ENABLE */

#ifdef PARAMETER_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void parameter_low_priority()
{
#ifdef PARAMETER_ENABLE_DEBUG_COUNTERS
    Parameter_LowPrioRoutine++;
#endif
}
#endif /* #ifdef PARAMETER_LOW_PRIO_ENABLE */

#ifdef PARAMETER_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void parameter_thread()
{
    ulong execTime = PARAMETER_THREAD_SLEEP_MS;
    
    (void)Parameter_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef PARAMETER_ENABLE_DEBUG_COUNTERS
        Parameter_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)Parameter_model(execTime);
    }
}
#endif /* #ifdef PARAMETER_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void parameter_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void parameter_shutdown()
//{
//}
