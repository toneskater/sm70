/*
 * splash.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define SPLASH_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"
#include "eepm_data.h"
#include "sw_version.h"

/* Private define ------------------------------------------------------------*/
#define HMI_MY_ID HMI_ID_SPLASH

/* #define SPLASH_ENABLE_DEBUG_COUNTERS */
#define SPLASH_NORMAL_ENABLE
/* #define SPLASH_FAST_ENABLE */
/* #define SPLASH_LOW_PRIO_ENABLE */
/* #define SPLASH_THREAD_ENABLE */

#define SPLASH_THREAD_SLEEP_MS 50U

#define SPLASH_STRING_ID_MAX_LENGTH 30U

#define SPLASH_TIMER_LOADING_MS 10000U

#define SPLASH_TIMER_SHOW_ID_MS 50U
#define SPLASH_STARTING_HMI_ID HMI_ID_MAIN

/* Private typedef -----------------------------------------------------------*/
typedef enum SPLASH_STATE_e
{
    SPLASH_STATE_WAIT_LOADING = 0,
    SPLASH_STATE_SHOW_ID,
    SPLASH_STATE_TERMINATED,
    SPLASH_STATE_MAX_NUMBER
}SPLASH_STATE_t;
    
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static Timer_t Splash_TimerLoading;
static Timer_t Splash_TimerIdString;
static SPLASH_STATE_t Splash_State;

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Splash_logFlag(FLAG_t flag);
static STD_RETURN_t Splash_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Splash_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Splash_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Splash_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t Splash_initVariable(void);
#ifdef SPLASH_NORMAL_ENABLE
static STD_RETURN_t Splash_manageButtons(ulong execTimeMs);
static STD_RETURN_t Splash_manageScreen(ulong execTimeMs);
#endif /* #ifdef SPLASH_NORMAL_ENABLE */
#ifdef SPLASH_THREAD_ENABLE
static STD_RETURN_t Splash_modelInit(void);
static STD_RETURN_t Splash_model(ulong execTimeMs);
#endif /* #ifdef SPLASH_THREAD_ENABLE */

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Splash_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Splash_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Splash_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Splash_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t Splash_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

STD_RETURN_t DynamicString_GetVariousString(ubyte* buffer, ulong bufferSize, DYNAMIC_STRING_VARIOUS_t idVarious)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    if(Languages_ID != language_savedLanguage) /* At startup the language id is set to zero */
    {
        debug_print("Loaded language from retention variable");
        Languages_ID = language_savedLanguage;
    }
    if(buffer != NULL && bufferSize > 1U && idVarious < DYNAMIC_STRING_VARIOUS_MAX_NUMBER)
    {
        graphic_get_dynamic_string_text(&buffer[0], bufferSize - 1U, DYNAMIC_STRING_VARIOUS, idVarious);
        debug_print("DynamicString_GetVariousString %s", &buffer[0]);
        returnValue = STD_RETURN_OK;
    }
    return returnValue;
}

static STD_RETURN_t Splash_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte stringTemp[32] = {0x00U};
    /* START CODE */
    Splash_State = SPLASH_STATE_WAIT_LOADING;
    
    (void)DynamicString_GetVariousString(&stringTemp[0U], sizeof(stringTemp), DYNAMIC_STRING_VARIOUS_SPLASH_LOADING_STRING);
    returnValue = UTIL_BufferFromSbyteToUShort(&stringTemp[0U], sizeof(stringTemp), &splash_string_id_1, SPLASH_STRING_ID_MAX_LENGTH);
    
    (void)UTIL_TimerInit(&Splash_TimerLoading);
    (void)UTIL_TimerInit(&Splash_TimerIdString);
    
    splashScreen_progressBar_word = FLOAT_TO_WORD(0.0f);
    splashScreen_progressBar_MaximumValue = FLOAT_TO_WORD(100.0f);
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Splash_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef SPLASH_NORMAL_ENABLE
static STD_RETURN_t Splash_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */

    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Splash_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef SPLASH_NORMAL_ENABLE */

#ifdef SPLASH_NORMAL_ENABLE
static STD_RETURN_t Splash_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte isExpired;
    ulong idStringLength;
    ubyte idString[SPLASH_STRING_ID_MAX_LENGTH] = {0x00};
    ubyte grSwVersion[4];
    ubyte grHwVersion[4];
    ubyte DateTimeLastCalibration[6U] = {0x00U};
    ushort DaysToNextCalibration;
    float percentageDownload = 0.0f;
    static float LastPercentageDownload = 0.0f;
    /* START CODE */
    
    (void)UTIL_TimerIncrement(&Splash_TimerLoading, execTimeMs);
    (void)UTIL_TimerIncrement(&Splash_TimerIdString, execTimeMs);
    
    if(MessageScreen_isEnteredHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            splashScreen_progressBar_word = FLOAT_TO_WORD(0.0f);
            splashScreen_progressBar_MaximumValue = FLOAT_TO_WORD(100.0f);
            (void)UTIL_TimerStart(&Splash_TimerLoading, SPLASH_TIMER_LOADING_MS);
        }
    }

    switch(Splash_State)
    {
        case SPLASH_STATE_WAIT_LOADING:
            (void)UTIL_TimerIsExpired(&Splash_TimerLoading, &isExpired);
            if(isExpired != false)
            {
                (void)UTIL_TimerStop(&Splash_TimerLoading);
                (void)UTIL_TimerInit(&Splash_TimerLoading);
                MessageScreen_openError(DYNAMIC_STRING_ERROR_NO_INTERNAL_COMMUNICATION, STD_ERROR_NO_COMMUNICATION, HMI_MY_ID);
                BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
                HMI_ChangeHmi(HMI_ID_MAIN, HMI_MY_ID);
                Splash_State = SPLASH_STATE_TERMINATED;
            }
            else
            {
                /* the system is ready? */
                switch(SYSTEM_GetState())
                {
                    case SYSTEM_STATE_DOWNLOAD_EEPROM:
                        /* Here I can load a percetage during eeprom load */
                        
                        if(STD_RETURN_OK == SYSTEM_GetEepromDownloadPercentage(&percentageDownload))
                        {
                            splashScreen_progressBar_word = FLOAT_TO_WORD(percentageDownload);
                            if(LastPercentageDownload != percentageDownload)
                            {
                                (void)UTIL_TimerStart(&Splash_TimerLoading, SPLASH_TIMER_LOADING_MS);
                                LastPercentageDownload = percentageDownload;
                            }
                        }
                        break;
                    case SYSTEM_STATE_RUNNING:
                        splashScreen_progressBar_word = FLOAT_TO_WORD(100.0f);
                        idStringLength = 0U;
                        (void)NETWORK_GetIdString(&idString[0U], sizeof(idString), &idStringLength);
                        UTIL_BufferFromSbyteToUShort(&idString[0], idStringLength, &splash_string_id_1, SPLASH_STRING_ID_MAX_LENGTH);    
                        
                        if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_EEPM_SW_VERSION_NUMBER, &grSwVersion[0], EEPM_VALUE_TYPE_UBYTE, 4))
                        {
                            sbyte grSwString[30U] = {0x00U};
                            if((grSwVersion[0] & 0xF0) == 0xA0)
                            {
                                /* simulator */
                                grSwVersion[0] = grSwVersion[0] & (~0xF0);
                                string_snprintf(
                                        &grSwString[0], sizeof(grSwString), "SIM %c%c.%c%c.%c%c.%c%c", 
                                        BCD_HI_TO_CHAR(grSwVersion[0]), BCD_LO_TO_CHAR(grSwVersion[0]),
                                        BCD_HI_TO_CHAR(grSwVersion[1]), BCD_LO_TO_CHAR(grSwVersion[1]),
                                        BCD_HI_TO_CHAR(grSwVersion[2]), BCD_LO_TO_CHAR(grSwVersion[2]),
                                        BCD_HI_TO_CHAR(grSwVersion[3]), BCD_LO_TO_CHAR(grSwVersion[3])
                                );
                            }
                            else
                            {
                                string_snprintf(
                                        &grSwString[0], sizeof(grSwString), "%c%c.%c%c.%c%c.%c%c", 
                                        BCD_HI_TO_CHAR(grSwVersion[0]), BCD_LO_TO_CHAR(grSwVersion[0]),
                                        BCD_HI_TO_CHAR(grSwVersion[1]), BCD_LO_TO_CHAR(grSwVersion[1]),
                                        BCD_HI_TO_CHAR(grSwVersion[2]), BCD_LO_TO_CHAR(grSwVersion[2]),
                                        BCD_HI_TO_CHAR(grSwVersion[3]), BCD_LO_TO_CHAR(grSwVersion[3])
                                );
                            }
                            UTIL_BufferFromSbyteToUShort(&grSwString[0], sizeof(grSwString), &splashScreen_grSwVersion_1, 30U);    
                        }
                         
                        if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_EEPM_HW_VERSION_NUMBER, &grHwVersion[0], EEPM_VALUE_TYPE_UBYTE, 4))
                        {
                            sbyte grHwString[30U] = {0x00U};
                            string_snprintf(
                                    &grHwString[0], sizeof(grHwString), "%c%c.%c%c.%c%c.%c%c", 
                                    BCD_HI_TO_CHAR(grHwVersion[0]), BCD_LO_TO_CHAR(grHwVersion[0]),
                                    BCD_HI_TO_CHAR(grHwVersion[1]), BCD_LO_TO_CHAR(grHwVersion[1]),
                                    BCD_HI_TO_CHAR(grHwVersion[2]), BCD_LO_TO_CHAR(grHwVersion[2]),
                                    BCD_HI_TO_CHAR(grHwVersion[3]), BCD_LO_TO_CHAR(grHwVersion[3])
                            );
                            UTIL_BufferFromSbyteToUShort(&grHwString[0], sizeof(grHwString), &splashScreen_grHwVersion_1, 30U);    
                        }
                        
                        sbyte smartSwString[30U] = {0x00U};
                        string_snprintf(
                                &smartSwString[0], sizeof(smartSwString), "%c%c.%c%c.%c%c.%c%c", 
                                BCD_HI_TO_CHAR(SMART_SW_VERSION_BYTE3), BCD_LO_TO_CHAR(SMART_SW_VERSION_BYTE3),
                                BCD_HI_TO_CHAR(SMART_SW_VERSION_BYTE2), BCD_LO_TO_CHAR(SMART_SW_VERSION_BYTE2),
                                BCD_HI_TO_CHAR(SMART_SW_VERSION_BYTE1), BCD_LO_TO_CHAR(SMART_SW_VERSION_BYTE1),
                                BCD_HI_TO_CHAR(SMART_SW_VERSION_BYTE0), BCD_LO_TO_CHAR(SMART_SW_VERSION_BYTE0)
                        );
                        UTIL_BufferFromSbyteToUShort(&smartSwString[0], sizeof(smartSwString), &splashScreen_smartSwVersion_1, 30U);    

                        (void)UTIL_TimerStart(&Splash_TimerIdString, SPLASH_TIMER_SHOW_ID_MS);
                        Splash_State = SPLASH_STATE_SHOW_ID;
                        break;
                        
                }
            }
            break;
        case SPLASH_STATE_SHOW_ID:
            (void)UTIL_TimerIsExpired(&Splash_TimerIdString, &isExpired);
            if(isExpired != false)
            {
                (void)UTIL_TimerStop(&Splash_TimerIdString);
                (void)UTIL_TimerInit(&Splash_TimerIdString);
                
                if((STD_RETURN_OK == EEPM_GetId(EEPM_ID_SYST_DATETIME_OF_LAST_CALIBRATION, &DateTimeLastCalibration[0], EEPM_VALUE_TYPE_UBYTE, 6U)) && (STD_RETURN_OK == EEPM_GetId(EEPM_ID_SYST_DAYS_TO_NEXT_CALIBRATION, &DaysToNextCalibration, EEPM_VALUE_TYPE_USHORT, 1U)))
                {
                    ushort year  =((DateTimeLastCalibration[0] & 0xF0U) >> 4) * 1000U + 
                                  ((DateTimeLastCalibration[0] & 0x0FU) >> 0) * 100U +
                                  ((DateTimeLastCalibration[1] & 0xF0U) >> 4) * 10U + 
                                  ((DateTimeLastCalibration[1] & 0x0FU) >> 0) * 1U;
                    ubyte month = ((DateTimeLastCalibration[2] & 0xF0U) >> 4) * 10U + 
                                  ((DateTimeLastCalibration[2] & 0x0FU) >> 0) * 1U; 
                    ubyte day   = ((DateTimeLastCalibration[3] & 0xF0U) >> 4) * 10U + 
                                  ((DateTimeLastCalibration[3] & 0x0FU) >> 0) * 1U;
                                  
                    UTIL_IncreaseAddDaysToDate(&year, &month, &day, DaysToNextCalibration);

                    if(UTIL_DateIsOverAnotherDate(year, month, day, datarioAnno, datarioMese, datarioGiorno) != false)
                    {
                        BUZM_BUTTON_SOUND();
                        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_NEW_CALIBRATION_REQUESTED_CALL_ASSISTANCE, HMI_MY_ID);
                    }      
                }
                HMI_ChangeHmi(SPLASH_STARTING_HMI_ID, HMI_MY_ID);
                Splash_State = SPLASH_STATE_TERMINATED;
            }
            break;
        case SPLASH_STATE_TERMINATED:
            ;/* Nothing stay here forever */
            break;
    }
    
    if(MessageScreen_isExitedHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Splash_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef SPLASH_THREAD_ENABLE
static STD_RETURN_t Splash_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Splash_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef SPLASH_THREAD_ENABLE */

#ifdef SPLASH_THREAD_ENABLE
static STD_RETURN_t Splash_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Splash_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef SPLASH_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void splash_init()
{
    (void)Splash_initVariable();
}

#ifdef SPLASH_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void splash()
{
#ifdef SPLASH_ENABLE_DEBUG_COUNTERS
    Splash_NormalRoutine++;
#endif
    (void)Splash_manageButtons(Execution_Normal);
    (void)Splash_manageScreen(Execution_Normal);
}
#endif /* #ifdef SPLASH_NORMAL_ENABLE */

#ifdef SPLASH_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void splash_fast()
{
#ifdef SPLASH_ENABLE_DEBUG_COUNTERS
    Splash_FastRoutine++;
#endif
}
#endif /* #ifdef SPLASH_FAST_ENABLE */

#ifdef SPLASH_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void splash_low_priority()
{
#ifdef SPLASH_ENABLE_DEBUG_COUNTERS
    Splash_LowPrioRoutine++;
#endif
}
#endif /* #ifdef SPLASH_LOW_PRIO_ENABLE */

#ifdef SPLASH_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void splash_thread()
{
    ulong execTime = SPLASH_THREAD_SLEEP_MS;
    
    (void)Splash_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef SPLASH_ENABLE_DEBUG_COUNTERS
        Splash_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)Splash_model(execTime);
    }
}
#endif /* #ifdef SPLASH_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void splash_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void splash_shutdown()
//{
//}
