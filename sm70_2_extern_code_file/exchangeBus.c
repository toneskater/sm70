/*
 * exchangeBus.c
 *
 *  Created on: 06/11/2019
 *      Author: Andrea Tonello
 */
#define EXCHANGE_BUS_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "exchangeBus.h"
#include "util.h"
#include "network.h"
#include "eepm_data.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define SAMPLING_NOZZLE_SLOT_NUMBER 5U

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static SAMPLING_SamplingTime_t Sampling_Time;
static ubyte Sampling_ForceStart;

/* Public variables ----------------------------------------------------------*/
/* WRITE ONLY THE VARIABLES THAT ARE EXCHANGED BETWEEN MODULES */
sbyte* MemoryReport_FileNameToOpen;
slong MemoryReport_FileStringList;
_FILE_PATH ReportFile_reportLocationSave;
_FILE_PATH ReportFile_reportLocationExport;
_FILE_PATH ClientFile_clientLocationSave;
_FILE_PATH ClientFile_clientLocationExport;

const sbyte* ReportFile_reportTags[REPORT_TAG_MAX_NUMBER] = 
{
    "XX", "X1", "12", "24", "14",
    "13", "X2", "X3", "X4", "X5",
    "X6", "X6", "X7", "X8", "X9",
    "Y0", "Y1", "Y2", "Y3", "Y4"
};

const sbyte* ReportFile_reportNameCodes[REPORT_NAME_CODE_MAX_NUMBER] = {"EN", "PM", "DC", "TS", "SR"};


/* Client file */
ushort Client_ClientsTotalNumber;
ushort Client_SelectedOne;
ClientFile_Client_t ClientsFile_Clients[CLIENT_FILE_CLIENTS_MAX_NUMBER];

const sbyte ClientFile_FileName[] = "Clients.csv";
const sbyte ClientFile_FileHeader[] = "NAME;ADDRESS;CAP;CITY;DISTRICT;PLANT NUMBER;NOTE";

/* could be called only by memory.c*/
MEMORY_OPERATION_t Memory_Operation;

SAMPLING_STOP_CAUSE_t Sampling_StopCause;
SAMPLING_ERROR_t Sampling_Error;


REMOTE_SRB_SetupMessage_t RemoteSrb_lastSetupMessage;

SAMPLING_NOZZLE_DIAMETER_t ReportFile_actualNozzleDiameter;
SAMPLING_NOZZLE_DIAMETER_t ReportFile_lastNozzleDiameter;

/* **********************************************************************************/
/* STATISTIC AND GRAPH                                                              */
/* **********************************************************************************/

static ulong Sampling_SampleNumber;

static float Sampling_SampleSumErrorFlow;
static float Sampling_SampleSumFlow;
static float Sampling_SampleSumMeterTemperature;
static float Sampling_SampleSumVacuum;
static float Sampling_SampleSumTemperatureDuctCelsius;
static float Sampling_SampleSumAbsoluteStaticPressurePa;
static float Sampling_SampleSumSpeed;
static float Sampling_SampleSumQv;
static float Sampling_SampleSumQvn;
static float Sampling_SampleSumBarometricPressureMbar;
static float Sampling_SampleSumCondense;
static float Sampling_SampleSumDifferentialPressurePa;

static float Sampling_FinalizedErrorFlow;
static float Sampling_FinalizedFlow;
static float Sampling_FinalizedMeterTemperature;
static float Sampling_FinalizedVacuum;
static float Sampling_FinalizedTemperatureDuctCelsius;
static float Sampling_FinalizedAbsoluteStaticPressurePa;
static float Sampling_FinalizedSpeed;
static float Sampling_FinalizedQv;
static float Sampling_FinalizedQvn;
static float Sampling_FinalizedBarometricPressureMbar;
static float Sampling_FinalizedCondense;
static float Sampling_FinalizedDifferentialPressurePa;

static slong Sampling_LastMinuteSamplingCall = -1;

static ulong Sampling_GraphSampleInThisMinute = 0;
static float Sampling_GraphSumFlow = 0.0f;
static float Sampling_GraphSumFlowError = 0.0f;
static float Sampling_GraphSumTemperature = 0.0f;
static float Sampling_GraphSumVacuum = 0.0f;

static ulong Sampling_GraphHystoryMinuteIndex = 0U;
static float Sampling_GraphHystoryFlow[GRAPH_MINUTE_TO_KEEP_DATA] = {0.0f};
static float Sampling_GraphHystoryFlowError[GRAPH_MINUTE_TO_KEEP_DATA] = {0.0f};
static float Sampling_GraphHystoryTemperature[GRAPH_MINUTE_TO_KEEP_DATA] = {0.0f};
static float Sampling_GraphHystoryVacuum[GRAPH_MINUTE_TO_KEEP_DATA] = {0U};
static ulong Sampling_GraphHystoryMinuteValue[GRAPH_MINUTE_TO_KEEP_DATA] = {0U};

static ubyte Sampling_NewMinuteDetected = false;
static ubyte Sampling_modelCalculateStatisticRunning = false;
static ulong Sampling_LastIndexToShowOnGraph = 0;
static ulong Sampling_LastGraphHystoryMinuteIndex = 0;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/

/* function to call a variable in client_file.c by other modules */
ushort Client_GetClientsTotalNumber(void)
{
    return Client_ClientsTotalNumber;
}

void Client_SetClientsTotalNumber(ushort c)
{
    Client_ClientsTotalNumber = c;
}

ushort Client_GetSelectedOne(void)
{
    return Client_SelectedOne;
}

void Client_SetSelectedOne(ushort s)
{
    Client_SelectedOne = s;
}

/* function to call a variable in memory.c by other modules */
MEMORY_OPERATION_t Memory_GetOperation(void)
{
    return Memory_Operation;
}

void Memory_SetOperation(MEMORY_OPERATION_t m)
{
    Memory_Operation = m;
}

/* function to call a variable in report_file.c by other modules */
sbyte* ReportFile_reportGetTag(REPORT_TAG_t tag)
{
    sbyte* returnValue = (sbyte*)ReportFile_reportTags[REPORT_TAG_DUMMY];
    if(tag < REPORT_TAG_MAX_NUMBER)
    {
        returnValue = (sbyte*)ReportFile_reportTags[tag];
    }
    return returnValue;
}

STD_RETURN_t Sampling_GetSamplingTime(SAMPLING_SamplingTime_t* samplingTime)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    if(samplingTime != NULL)
    {
        samplingTime->days = Sampling_Time.days;
        samplingTime->hours = Sampling_Time.hours;
        samplingTime->minutes = Sampling_Time.minutes;
        samplingTime->seconds = Sampling_Time.seconds;
        samplingTime->daysRemaining =  Sampling_Time.daysRemaining;
        samplingTime->hoursRemaining = Sampling_Time.hoursRemaining;
        samplingTime->minutesRemaining = Sampling_Time.minutesRemaining;
        samplingTime->secondsRemaining = Sampling_Time.secondsRemaining ;
        samplingTime->counterSecond = Sampling_Time.counterSecond;
        returnValue = STD_RETURN_OK;
    }
    return returnValue;
}

void Sampling_manageSamplingTimeOnScreen(void)
{
    sbyte samplingTimeStringChar[12U];
    
    /* Format string */
    string_snprintf(&samplingTimeStringChar[0],
                    sizeof(samplingTimeStringChar),
                    "%d:%02d:%02d",
                    ((Sampling_Time.days * 24) + Sampling_Time.hours),  
                    Sampling_Time.minutes,
                    Sampling_Time.seconds
    );

    (void)UTIL_BufferFromSbyteToUShort(
                    &samplingTimeStringChar[0], 
                    sizeof(samplingTimeStringChar), 
                    (ushort*)&common_samplingTimeString_1, 
                    12U
    );
}

void Sampling_manageSamplingRemainingTimeOnScreen(void)
{
    sbyte samplingTimeRemainingStringChar[12U];
    
    /* Format string */
    string_snprintf(&samplingTimeRemainingStringChar[0],
                    sizeof(samplingTimeRemainingStringChar),
                    "%d:%02d:%02d",
                    ((Sampling_Time.daysRemaining * 24) + Sampling_Time.hoursRemaining),  
                    Sampling_Time.minutesRemaining,
                    Sampling_Time.secondsRemaining
    );

    (void)UTIL_BufferFromSbyteToUShort(
                    &samplingTimeRemainingStringChar[0], 
                    sizeof(samplingTimeRemainingStringChar), 
                    (ushort*)&common_samplingTimeRemainingString_1, 
                    12U
    );
}

void Sampling_manageSamplingTimeInit(slong startDelaySeconds)
{
    Sampling_ForceStart = false;
    
    Sampling_Time.days = 0;
    Sampling_Time.hours = 0;
    Sampling_Time.minutes = 0;
    Sampling_Time.seconds = 0;
    Sampling_Time.daysRemaining = 0;
    Sampling_Time.hoursRemaining = 0;
    Sampling_Time.minutesRemaining = 0;
    Sampling_Time.secondsRemaining = 0;
    Sampling_Time.counterSecond = (0 - startDelaySeconds);
    debug_print("Sampling_manageSamplingInit %d", Sampling_Time.counterSecond);
}

void Sampling_manageSamplingTimeForceStart(void)
{
    Sampling_ForceStart = true;
}

void Sampling_manageSamplingTime(void)
{
    static ulong lastSec = 0;
    float volumeToSample;
    
    if(datarioSecondi != lastSec)
    {
        lastSec = datarioSecondi;
        /* second has changed */
        if(Sampling_ForceStart != false)
        {
            Sampling_ForceStart = false;
            if(Sampling_Time.counterSecond < 0)
            {
                Sampling_Time.counterSecond = 0; /* or -1 ???? */
            }
        }

        Sampling_Time.counterSecond++;
        
        if(Sampling_Time.counterSecond > 0)
        {
            
            Sampling_Time.seconds = (Sampling_Time.counterSecond % 60); /* sec */
                
            Sampling_Time.minutes = (Sampling_Time.counterSecond / 60) % 60; /* 60 sec in 1 min */
            
            Sampling_Time.hours = (Sampling_Time.counterSecond / 3600) % 24; /* 3600 sec in 1 hour */
            
            Sampling_Time.days = (Sampling_Time.counterSecond / 86400); /* 86400U sec in 1 day */
            
            slong secondsRemaing = 0U;
            
            /* remaining time depends by Time or Volume */
            switch(common_isokineticSampling)
            {
                case SAMPLING_ISOKINETIC_SAMPLING_TIME:
                    /* common_isokineticSamplingValue -> this variable contain the minute to sample if time sampling is ongoing */                    
                    /* Now cast to int with NO SIGN */
                    
                    secondsRemaing = (((slong)(common_isokineticSamplingValue)) * 60) - Sampling_Time.counterSecond;

                    break;
                case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
                    /* common_isokineticSamplingValue -> this variable contain the minute to sample if time sampling is ongoing */
                    volumeToSample = (float)common_isokineticSamplingValue; 
                    
                    float volumeRemaining = 0.0f;
                
                    float actualFlow = 0.0f;
                    
                    float volumeSampled = 0.0f;
    
                    if(
                        (NETWORK_GetLitersMinuteRead(&actualFlow) == STD_RETURN_OK) && 
                        (NETWORK_GetLitersTotalFromSamplingStart(&volumeSampled) == STD_RETURN_OK)
                    )
                    {
                        volumeRemaining = volumeToSample - volumeSampled;
                        
                        if(volumeRemaining >= 0)
                        {
                            if(actualFlow > 0.0f)
                            {
                                float secondsRemainingFloat = (volumeRemaining / actualFlow) * 60; /* minute * 60 = sec */
                                
                                if(secondsRemainingFloat > 2147483647) /* Max value for integer with sign */
                                {
                                    secondsRemainingFloat = 2147483647;
                                }
                                
                                /* Now cast to int with NO SIGN */
                                secondsRemaing = secondsRemainingFloat;
                            }
                            else
                            {
                                ; /* Time infinite */
                            }
                        }
                        else
                        {
                            secondsRemaing = 0;
                        }
                        
                    }
                    else
                    {
                        ; /* impossible to calculate remaining time */
                    }
                    break;
                default:
                    break;
            }
            
            if(secondsRemaing > 0)
            {
                Sampling_Time.secondsRemaining = (secondsRemaing % 60); /* sec */
                    
                Sampling_Time.minutesRemaining = (secondsRemaing / 60) % 60; /* 60 sec in 1 min */
                
                Sampling_Time.hoursRemaining = (secondsRemaing / 3600) % 24; /* 3600 sec in 1 hour */
                
                Sampling_Time.daysRemaining = (secondsRemaing / 86400); /* 86400U sec in 1 day */
            }
            else
            {
                Sampling_Time.secondsRemaining = 0; /* sec */
                    
                Sampling_Time.minutesRemaining = 0; /* 60 sec in 1 min */
                
                Sampling_Time.hoursRemaining = 0; /* 3600 sec in 1 hour */
                
                Sampling_Time.daysRemaining = 0; /* 86400U sec in 1 day */
            }
        }
        else
        {
            if(Sampling_Time.counterSecond < 0)
            {
                slong secondToStart = (0 - Sampling_Time.counterSecond);
                
                Sampling_Time.seconds = 0U; 
                
                Sampling_Time.minutes = 0U; 
                
                Sampling_Time.hours = 0U; 
                
                Sampling_Time.days = 0U; 
                
                Sampling_Time.secondsRemaining = (secondToStart % 60); /* sec */
                    
                Sampling_Time.minutesRemaining = (secondToStart / 60) % 60; /* 60 sec in 1 min */
                
                Sampling_Time.hoursRemaining = (secondToStart / 3600) % 24; /* 3600 sec in 1 hour */
                
                Sampling_Time.daysRemaining = (secondToStart / 86400); /* 86400U sec in 1 day */
            }
            else 
            { /* Sampling_Time.counterSecon == 0*/            
                Sampling_Time.seconds = 0U; /* sec */
                
                Sampling_Time.minutes = 0U; /* 60 sec in 1 min */
                
                Sampling_Time.hours = 0U; /* 3600 sec in 1 hour */
                
                Sampling_Time.days = 0U; /* 86400U sec in 1 day */
                
                Sampling_Time.secondsRemaining = 0U; /* sec */
                    
                Sampling_Time.minutesRemaining = 0U; /* 60 sec in 1 min */
                
                Sampling_Time.hoursRemaining = 0U; /* 3600 sec in 1 hour */
                
                Sampling_Time.daysRemaining = 0U; /* 86400U sec in 1 day */
            }
        }
    }
}

ubyte Sampling_manageSamplingTimeIsInWaitStart(void)
{
    ubyte returnValue = false;
    if(Sampling_Time.counterSecond < 0) /* It is signed this value */
    {
        returnValue = true;
    }
    return returnValue;
}

ulong Sampling_SampleTimeInMinutes(void)
{
    return (Sampling_Time.minutes + (Sampling_Time.hours * 60U) + (Sampling_Time.days * 1440U));
}

ulong Sampling_SampleVolumeInLiters(void)
{
    float totalLiters = 0.0f;
    if(STD_RETURN_OK != NETWORK_GetLitersTotalFromSamplingStart(&totalLiters))
    {
        totalLiters = 0.0;
    }
    return (ulong)totalLiters;
}

void Sampling_manageIsokineticSamplingLimits(void)
{
    common_isokineticSamplingLimits = common_isokineticSampling;
    switch(common_isokineticSampling)
    {
        case SAMPLING_ISOKINETIC_SAMPLING_TIME:
            common_isokineticSamplingValue_rangeMin = SAMPLING_ISOKINETICK_SAMPLING_TIME_MIN; /* min */
            common_isokineticSamplingValue_rangeMax = SAMPLING_ISOKINETICK_SAMPLING_TIME_MAX;
            common_isokineticSamplingValue = common_isokineticSamplingValue_rangeMin;
            break;
        case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
            common_isokineticSamplingValue_rangeMin = SAMPLING_ISOKINETICK_SAMPLING_VOLUME_MIN; /* min */
            common_isokineticSamplingValue_rangeMax = SAMPLING_ISOKINETICK_SAMPLING_VOLUME_MAX;
            common_isokineticSamplingValue = common_isokineticSamplingValue_rangeMin;
            break;
        default:
            break;
    }
}

float Sampling_GetNozzleDiameter(SAMPLING_NOZZLE_DIAMETER_t nozzle)
{
    const float diameters[SAMPLING_NOZZLE_DIAMETER_MAX_NUMBER] = {4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 12.0f, 14.0f, 16.0f};
    float returnValue = 0.0f;
    if(nozzle < SAMPLING_NOZZLE_DIAMETER_MAX_NUMBER)
    {
        returnValue = diameters[nozzle];
    }
    return returnValue;
}

void Sampling_ManageNozzleDiameterSelectedFlow(void)
{
    if(common_isokineticFlow == SAMPLING_ISOKINETIC_FLOW_AUTOMATIC)
    {
        switch(common_nozzleDiameter)
        {
            case SAMPLING_NOZZLE_DIAMETER_4MM:
                common_selectedFlowFromAutoManual_value = common_flow4mm_value;
                break;
            case SAMPLING_NOZZLE_DIAMETER_5MM:
                common_selectedFlowFromAutoManual_value = common_flow5mm_value;
                break;
            case SAMPLING_NOZZLE_DIAMETER_6MM:
                common_selectedFlowFromAutoManual_value = common_flow6mm_value;
                break;
            case SAMPLING_NOZZLE_DIAMETER_7MM:
                common_selectedFlowFromAutoManual_value = common_flow7mm_value;
                break;
            case SAMPLING_NOZZLE_DIAMETER_8MM:
                common_selectedFlowFromAutoManual_value = common_flow8mm_value;
                break;
            case SAMPLING_NOZZLE_DIAMETER_9MM:
                common_selectedFlowFromAutoManual_value = common_flow9mm_value;
                break;
            case SAMPLING_NOZZLE_DIAMETER_10MM:
                common_selectedFlowFromAutoManual_value = common_flow10mm_value;
                break;
            case SAMPLING_NOZZLE_DIAMETER_12MM:
                common_selectedFlowFromAutoManual_value = common_flow12mm_value;
                break;
            case SAMPLING_NOZZLE_DIAMETER_14MM:
                common_selectedFlowFromAutoManual_value = common_flow14mm_value;
                break;
            case SAMPLING_NOZZLE_DIAMETER_16MM:
                common_selectedFlowFromAutoManual_value = common_flow16mm_value;
                break;
            default:
                
                break;
        }
    }
    else
    {
        /* Manual flow */
        common_selectedFlowFromAutoManual_value = common_isokineticFlowManualValue;
    }
}

void ManageDateTimeOnScreen(void)
{
    static ubyte lastSecondi = 0;
    if(lastSecondi != datarioSecondi)
    {
        ubyte dateTimeString[22U];
        ubyte dateTimeSize = string_snprintf(&dateTimeString[0], sizeof(dateTimeString), "%04d/%02d/%02d\n%02d:%02d:%02d", datarioAnno, datarioMese, datarioGiorno, datarioOre, datarioMinuti, datarioSecondi);
        (void)UTIL_BufferFromSbyteToUShort(&dateTimeString[0], dateTimeSize, (ushort*)&DateTimeString_1, 22U);
    }
    lastSecondi = datarioSecondi;
}     

float Sampling_getNormalizedLiters(float aspiratedLiters, float normalizedTemperature, float averageTemperature)
{
    float returnValue = aspiratedLiters * ((KELVIN_VALUE + normalizedTemperature) /(KELVIN_VALUE + averageTemperature));    
    return returnValue;
}    


STD_RETURN_t Sampling_modelCalculateStatisticInit(void)
{
    ulong i = 0;
    STD_RETURN_t returnValue = STD_RETURN_OK;
    debug_print("Environmental_modelCalculateStatisticInit");
    
    Sampling_SampleNumber = 0;
    Sampling_SampleSumErrorFlow = 0.0f;
    Sampling_SampleSumFlow = 0.0f;
    Sampling_SampleSumVacuum = 0.0f;
    Sampling_SampleSumMeterTemperature = 0.0f;
    Sampling_SampleSumTemperatureDuctCelsius = 0.0f;
    Sampling_SampleSumAbsoluteStaticPressurePa = 0.0f;
    Sampling_SampleSumSpeed = 0.0f;
    Sampling_SampleSumQv = 0.0f;
    Sampling_SampleSumQvn = 0.0f;
    Sampling_SampleSumBarometricPressureMbar = 0.0f;
    Sampling_SampleSumCondense = 0.0f;
    Sampling_SampleSumDifferentialPressurePa = 0.0f;
    
    Sampling_GraphSampleInThisMinute = 0;
    
    Sampling_LastMinuteSamplingCall = -1;
    
    Sampling_NewMinuteDetected = false;
    Sampling_modelCalculateStatisticRunning = false;
    
    Sampling_GraphHystoryMinuteIndex = 0U;
    for(i = 0; i < GRAPH_MINUTE_TO_KEEP_DATA; i++)
    {
        Sampling_GraphHystoryFlow[i]        = 0.0f;
        Sampling_GraphHystoryFlowError[i]   = 0.0f;
        Sampling_GraphHystoryTemperature[i] = 0.0f;
        Sampling_GraphHystoryVacuum[i] = 0.0f;
        Sampling_GraphHystoryMinuteValue[i] = 0U;
    }

    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Environmental_modelCalculateStatisticInit %d", returnValue);
    }
    return returnValue;
}

void Sampling_modelCalculateStatisticNewMinuteDetect(void)
{
    ulong actualMinute = Sampling_Time.minutes;
    if(Sampling_modelCalculateStatisticRunning != false)
    {
        //debug_print("actualMinute: %d", actualMinute);
        if(actualMinute != Sampling_LastMinuteSamplingCall)
        {
            debug_print("CALCULATE STATISTIC NEW MINUTE");
            Sampling_NewMinuteDetected = true;
        }
        Sampling_LastMinuteSamplingCall = actualMinute;
    }
}

static void Sampling_modelCalculateDataFroGraph(float actualFlow, float actualError, float actualTemperature, float actualVacuum)
{
    ulong i;
    ulong actualMinute = Sampling_Time.minutes + 1;
    if(Sampling_LastMinuteSamplingCall == -1)
    {
        /* Init first call */
        Sampling_LastMinuteSamplingCall = Sampling_Time.minutes; /* valore del secondo in cui si inizia a fare la media sul minuto da mostrare a video sul grafico */
        
        Sampling_GraphSumFlow = 0.0f;
        Sampling_GraphSumFlowError = 0.0f;
        Sampling_GraphSumTemperature = 0.0f;
        Sampling_GraphSumVacuum = 0.0f;
        Sampling_GraphHystoryMinuteValue[Sampling_GraphHystoryMinuteIndex] = actualMinute;
        //debug_print("Sampling_FirstMinuteSamplingCall: %d", Sampling_FirstMinuteSamplingCall);
        //debug_print("Actual Minute: %d", actualMinute);
        Sampling_modelCalculateStatisticRunning = true;
        debug_print("CALCULATE STATISTIC RUNNING");
    }
    else
    {    
        if(Sampling_NewMinuteDetected != false) /* Minute terminated, shif the data */
        {
            Sampling_NewMinuteDetected = false;
            
            //debug_print("Minute passed sec %d", actualSecond);
            //debug_print("HystoryMinuteIndex %d", Environmental_GraphHystoryMinuteIndex);
            debug_print("------------------------");
            debug_print("Minute         : %d",   Sampling_GraphHystoryMinuteValue[Sampling_GraphHystoryMinuteIndex]);
            debug_print("AVG Flow       : %.2f", Sampling_GraphHystoryFlow[Sampling_GraphHystoryMinuteIndex]);
            debug_print("AVG FlowError  : %.2f", Sampling_GraphHystoryFlowError[Sampling_GraphHystoryMinuteIndex]);
            debug_print("AVG Temperature: %.2f", Sampling_GraphHystoryTemperature[Sampling_GraphHystoryMinuteIndex]);
            debug_print("AVG Vacuum     : %.2f", Sampling_GraphHystoryVacuum[Sampling_GraphHystoryMinuteIndex]);
            
            /* Insert the data into a my array of data*/
            if(Sampling_GraphHystoryMinuteIndex < (GRAPH_MINUTE_TO_KEEP_DATA - 1U))
            {
                Sampling_GraphHystoryMinuteIndex++;
                //debug_print("Sampling_GraphHystoryMinuteIndex++ %d", Sampling_GraphHystoryMinuteIndex);
            }
            else
            {
                for(i = 0; i < (GRAPH_MINUTE_TO_KEEP_DATA - 1U); i++)
                {
                    Sampling_GraphHystoryFlow[i]        = Sampling_GraphHystoryFlow[i + 1U];
                    Sampling_GraphHystoryFlowError[i]   = Sampling_GraphHystoryFlowError[i + 1U];
                    Sampling_GraphHystoryTemperature[i] = Sampling_GraphHystoryTemperature[i + 1U];
                    Sampling_GraphHystoryVacuum[i]      = Sampling_GraphHystoryVacuum[i + 1U];
                    Sampling_GraphHystoryMinuteValue[i] = Sampling_GraphHystoryMinuteValue[i + 1U];
                }
                Sampling_GraphHystoryFlow[i]        = 0.0f;
                Sampling_GraphHystoryFlowError[i]   = 0.0f;
                Sampling_GraphHystoryTemperature[i] = 0.0f;
                Sampling_GraphHystoryVacuum[i] = 0.0f;
                Sampling_GraphHystoryMinuteValue[i] = 0U;
                //debug_print("Shifted data");
            }
            //debug_print("Second restart");
            Sampling_GraphSampleInThisMinute = 0U; /* Second restart */
            Sampling_GraphSumFlow = 0.0f;
            Sampling_GraphSumFlowError = 0.0f;
            Sampling_GraphSumTemperature = 0.0f;
            Sampling_GraphSumVacuum = 0.0f;
            Sampling_GraphHystoryMinuteValue[Sampling_GraphHystoryMinuteIndex] = actualMinute;
            //debug_print("Actual Minute: %d", actualMinute);
        }
    }
    
    if(Sampling_GraphSampleInThisMinute < 0xFFFFFFFFU)
    {
        Sampling_GraphSampleInThisMinute++;
    }
    
    Sampling_GraphSumFlow += actualFlow;
    Sampling_GraphSumFlowError += actualError;
    Sampling_GraphSumTemperature += actualTemperature;
    Sampling_GraphSumVacuum += actualVacuum;

    Sampling_GraphHystoryFlow[Sampling_GraphHystoryMinuteIndex] = (Sampling_GraphSumFlow / Sampling_GraphSampleInThisMinute);
    Sampling_GraphHystoryFlowError[Sampling_GraphHystoryMinuteIndex] = (Sampling_GraphSumFlowError / Sampling_GraphSampleInThisMinute);
    Sampling_GraphHystoryTemperature[Sampling_GraphHystoryMinuteIndex] = (Sampling_GraphSumTemperature / Sampling_GraphSampleInThisMinute);
    Sampling_GraphHystoryVacuum[Sampling_GraphHystoryMinuteIndex] = (Sampling_GraphSumVacuum / Sampling_GraphSampleInThisMinute);

    //debug_print("-------------------------");
    //debug_print("Sample#    : %d"  , Sampling_GraphSampleInThisMinute);
    //debug_print("Flow       : %.2f", Sampling_GraphHystoryFlow[Sampling_GraphHystoryMinuteIndex]);
    //debug_print("FlowError  : %.2f", Sampling_GraphHystoryFlowError[Sampling_GraphHystoryMinuteIndex]);
    //debug_print("Temperature: %.2f", Sampling_GraphHystoryTemperature[Sampling_GraphHystoryMinuteIndex]);
}

/* IN DUCT MUST BE CALLED BEFORE the Duct_RecalculateSamplingData function */
STD_RETURN_t Sampling_modelCalculateStatisticStructInit(Sampling_StatisticData_t* sd)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    if(sd != NULL)
    {
        sd->ErrorFlow = 0.0f;
        sd->Flow = 0.0f;
        sd->MeterTemperature = 0.0f;
        sd->Vacuum = 0.0f;
        sd->TemperatureDuctCelsius = 0.0f;
        sd->AbsoluteStaticPressurePa = 0.0f;
        sd->Speed = 0.0f;
        sd->Qv = 0.0f;
        sd->Qvn = 0.0f;
        sd->BarometricPressureMbar = 0.0f;
        sd->Condense = 0.0f;
        sd->DifferentialPressurePa = 0.0f;   
        returnValue = STD_RETURN_OK;
    }
    return returnValue;
}

STD_RETURN_t Sampling_modelCalculateStatistic(Sampling_StatisticData_t* sd)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    
    if(sd != NULL)
    {
        /* Called every time a new message arrive */
        if(Sampling_SampleNumber < 0xFFFFFFFFU)
        {
            Sampling_SampleNumber++;
            
            Sampling_SampleSumErrorFlow                += sd->ErrorFlow;
            Sampling_SampleSumFlow                     += sd->Flow;
            Sampling_SampleSumMeterTemperature         += sd->MeterTemperature;
            Sampling_SampleSumVacuum                   += sd->Vacuum;
            Sampling_SampleSumTemperatureDuctCelsius   += sd->TemperatureDuctCelsius;
            Sampling_SampleSumAbsoluteStaticPressurePa += sd->AbsoluteStaticPressurePa;
            Sampling_SampleSumSpeed                    += sd->Speed;
            Sampling_SampleSumQv                       += sd->Qv;
            Sampling_SampleSumQvn                      += sd->Qvn;
            Sampling_SampleSumBarometricPressureMbar   += sd->BarometricPressureMbar;
            Sampling_SampleSumCondense                 += sd->Condense;
            Sampling_SampleSumDifferentialPressurePa   += sd->DifferentialPressurePa;
        }
        
        Sampling_modelCalculateDataFroGraph(sd->Flow, sd->ErrorFlow, sd->MeterTemperature, sd->Vacuum);
        
        //debug_print("---SAMPLE %d", Sampling_SampleNumber);
        //debug_print("TEMP\t%.3f", Sampling_MeterTemperature);
        //debug_print("FLOW\t%.3f", Sampling_Flow);
        //debug_print("ERRO\t%.3f", Sampling_ErrorFlow);
        returnValue = STD_RETURN_OK;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Sampling_modelCalculateStatistic %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t Sampling_modelCalculateStatisticFinalize(void /*float totalSamplingLiters, ulong totalSamplingSeconds*/)
{
    ulong i = 0U;
    debug_print("SAMPLES TOT# %d", Sampling_SampleNumber);
    if(Sampling_SampleNumber > 0)
    {
        Sampling_FinalizedMeterTemperature         = (Sampling_SampleSumMeterTemperature         / Sampling_SampleNumber);
        Sampling_FinalizedFlow                     = (Sampling_SampleSumFlow                     / Sampling_SampleNumber);
        Sampling_FinalizedErrorFlow                = (Sampling_SampleSumErrorFlow                / Sampling_SampleNumber);
        //debug_print("FLOW - OLD         %.3f - %.3f", Sampling_FinalizedFlow                    , Sampling_SampleSumFlow            );
        //debug_print("ERRO - OLD         %.3f - %.3f", Sampling_FinalizedErrorFlow               , Sampling_SampleSumErrorFlow       );
        //Sampling_FinalizedFlow                     = (totalSamplingLiters                        / totalSamplingSeconds);
        //Sampling_FinalizedErrorFlow                = 
        
        Sampling_FinalizedVacuum                   = (Sampling_SampleSumVacuum                   / Sampling_SampleNumber);
        Sampling_FinalizedTemperatureDuctCelsius   = (Sampling_SampleSumTemperatureDuctCelsius   / Sampling_SampleNumber);
		Sampling_FinalizedAbsoluteStaticPressurePa = (Sampling_SampleSumAbsoluteStaticPressurePa / Sampling_SampleNumber);
		Sampling_FinalizedSpeed                    = (Sampling_SampleSumSpeed                    / Sampling_SampleNumber);
		Sampling_FinalizedQv                       = (Sampling_SampleSumQv                       / Sampling_SampleNumber);
		Sampling_FinalizedQvn                      = (Sampling_SampleSumQvn                      / Sampling_SampleNumber);
		Sampling_FinalizedBarometricPressureMbar   = (Sampling_SampleSumBarometricPressureMbar   / Sampling_SampleNumber);
		Sampling_FinalizedCondense                 = (Sampling_SampleSumCondense                 / Sampling_SampleNumber);
		Sampling_FinalizedDifferentialPressurePa   = (Sampling_SampleSumDifferentialPressurePa   / Sampling_SampleNumber);
        
        debug_print("AVG");
        debug_print("DGM              %.3f - %.3f", Sampling_FinalizedMeterTemperature        , Sampling_SampleSumMeterTemperature);
        debug_print("FLOW             %.3f - %.3f", Sampling_FinalizedFlow                    , Sampling_SampleSumFlow            );
        debug_print("ERRO             %.3f - %.3f", Sampling_FinalizedErrorFlow               , Sampling_SampleSumErrorFlow       );
        debug_print("VACUUM           %.3f - %.3f", Sampling_FinalizedVacuum                  , Sampling_SampleSumVacuum       );
        debug_print("TEMP DUCT        %.3f - %.3f", Sampling_FinalizedTemperatureDuctCelsius  , Sampling_SampleSumTemperatureDuctCelsius);
        debug_print("ABS STAT. PRESS. %.3f - %.3f", Sampling_FinalizedAbsoluteStaticPressurePa, Sampling_SampleSumAbsoluteStaticPressurePa);
        debug_print("SPEED            %.3f - %.3f", Sampling_FinalizedSpeed                   , Sampling_SampleSumSpeed);
        debug_print("QV               %.3f - %.3f", Sampling_FinalizedQv                      , Sampling_SampleSumQv);
        debug_print("QVN              %.3f - %.3f", Sampling_FinalizedQvn                     , Sampling_SampleSumQvn);
        debug_print("BARO PRESS.      %.3f - %.3f", Sampling_FinalizedBarometricPressureMbar  , Sampling_SampleSumBarometricPressureMbar);
        debug_print("CONDENSE         %.3f - %.3f", Sampling_FinalizedCondense                , Sampling_SampleSumCondense);
        debug_print("DIFF PRESS       %.3f - %.3f", Sampling_FinalizedDifferentialPressurePa  , Sampling_SampleSumDifferentialPressurePa);
        
        //debug_print("GRAPH HYST");
        //for(i = 0; i < (Sampling_GraphHystoryMinuteIndex + 1U); i++)
        //{
        //    debug_print("AVG min index %d - minute %d", i, Sampling_GraphHystoryMinuteValue[i]);
        //    debug_print("Flow       : %.2f", Sampling_GraphHystoryFlow[i]);
        //    debug_print("FlowError  : %.2f", Sampling_GraphHystoryFlowError[i]);
        //    debug_print("Temperature: %.2f", Sampling_GraphHystoryTemperature[i]);
        //}
    }
    else
    {
        debug_print("NO SAMPLES TO CALCULATE AVERAGES");
    }
    debug_print("CALCULATE STATISTIC STOP");
    Sampling_modelCalculateStatisticRunning = false;
}

/* passed a minute value also negative 
    for example -2 = 58
*/
static slong Sampling_drawDataOnGraphManageXValue(slong value)
{
    slong returnValue = value;
    ulong safeIndex = 0;
    while((returnValue < 0) && (safeIndex < 10))
    {
        safeIndex++;
        returnValue += 60;
    }
    return returnValue;
}

void Sampling_modelInitMobileScreenPosition(void)
{
    Sampling_LastIndexToShowOnGraph = 0;
    Sampling_LastGraphHystoryMinuteIndex = 0;
}

void Sampling_modelManageButtonMobileScreen(void)
{
    slong totalData = Sampling_LastGraphHystoryMinuteIndex + 1;
    slong rightData = Sampling_LastIndexToShowOnGraph + 1;
    slong leftData = ((rightData - CHARTS_GRAPH_MAX_POINTS_NUMBER) + 1);
    sbyte tempStringArray[20];
    slong tempStringSize;
    
    //debug_print("Sampling_modelManageButtonMobileScreen");
    //debug_print("LastIndexToShowOnGraph %d MAX_POINTS_NUMBER %d GraphHystoryMinuteIndex %d", LastIndexToShowOnGraph, ENVIRONMENTAL_CHARTS_GRAPH_MAX_POINTS_NUMBER, GraphHystoryMinuteIndex);
    
    if(leftData < 1)
    {
        leftData = 1;
    }
    
    if(Sampling_LastIndexToShowOnGraph >= CHARTS_GRAPH_MAX_POINTS_NUMBER)
    {
        //Button LEFT enabled
        graph_buttonPrev_enable = true;
    }
    else
    {
        //Button LEFT disabled
        graph_buttonPrev_enable = false;
    }
    
    if(Sampling_LastIndexToShowOnGraph < Sampling_GraphHystoryMinuteIndex)
    {
        //Button RIGHT enabled
        graph_buttonNext_enable = true;
    }
    else
    {
        //Button RIGHT disabled
        graph_buttonNext_enable = false;
    }
    
    tempStringSize = string_snprintf(&tempStringArray[0U], sizeof(tempStringArray), "%d / %d - %d", leftData, rightData, totalData);
    (void)UTIL_BufferFromSbyteToUShort(&tempStringArray[0], tempStringSize, (ushort*)&graph_dataShowString_1, 20); 
}

void Sampling_modelInitMobileScreenPositionMoveToLeft(void)
{    
    if(Sampling_LastIndexToShowOnGraph >= CHARTS_GRAPH_MAX_POINTS_NUMBER)
    {
        Sampling_LastIndexToShowOnGraph--;
    }
}

void Sampling_modelInitMobileScreenPositionMoveToRight(void)
{
    if(Sampling_LastIndexToShowOnGraph < Sampling_GraphHystoryMinuteIndex)
    {
        Sampling_LastIndexToShowOnGraph++;
    }
}

void Sampling_modelCalculateMobileScreenPosition(void)
{
    /* There was an increment every minute */
    if(Sampling_GraphHystoryMinuteIndex > Sampling_LastGraphHystoryMinuteIndex)
    {
        //debug_print("Increment of GraphHystoryMinuteIndex");
        if(Sampling_LastIndexToShowOnGraph == Sampling_LastGraphHystoryMinuteIndex) /* The mobile screen was attached to the last index of data, so follow it */
        {
            Sampling_LastIndexToShowOnGraph = Sampling_GraphHystoryMinuteIndex;
            //debug_print("Mobile screen MOVE index %d", Sampling_LastIndexToShowOnGraph);
        }
        else
        {
            /* The mobile screen was on another position of the data */
            //debug_print("Mobile screen DONT move index %d", Sampling_LastIndexToShowOnGraph);
        }
    }
    Sampling_LastGraphHystoryMinuteIndex = Sampling_GraphHystoryMinuteIndex;
}

STD_RETURN_t Sampling_drawDataOnGraph(GRAPH_DATA_TO_SHOW_t DataToShow)
{
    ulong i = 0;
    ubyte somethingToDraw = false;
    ulong* ptrGraphX = &xValues_1;
    ulong* ptrGraphY = &yValues1_1;
    ulong* ptrGraphYMaxValues = &yMaxValueArray_1;
    ulong* ptrGraphYMinValues = &yMinValueArray_1;

    float dataMaxValue = 0.0f;
    float dataMinValue = 0.0f;
    
    float* bufferDataPtrY = NULL;
    ulong* bufferDataPtrX = NULL;
    ulong bufferDataSize = 0;
    ulong bufferDataCounter = (Sampling_GraphHystoryMinuteIndex + 1); /* from 0 to ENVIRONMENTAL_GRAPH_MINUTE_TO_KEEP_DATA - 1*/
    
    //debug_print("EnvironmentalGraph_DataToShow %d", EnvironmentalGraph_DataToShow);
    switch(DataToShow)
    {
        case GRAPH_DATA_TO_SHOW_FLOW:
            bufferDataPtrY = &Sampling_GraphHystoryFlow[0U];
            bufferDataPtrX = &Sampling_GraphHystoryMinuteValue[0U];
            bufferDataSize = GRAPH_MINUTE_TO_KEEP_DATA;
            somethingToDraw = true;
            break;
        case GRAPH_DATA_TO_SHOW_ERROR:
            bufferDataPtrY = &Sampling_GraphHystoryFlowError[0U];
            bufferDataPtrX = &Sampling_GraphHystoryMinuteValue[0U];
            bufferDataSize = GRAPH_MINUTE_TO_KEEP_DATA;
            somethingToDraw = true;
            break;
        case GRAPH_DATA_TO_SHOW_VACUUM:
            bufferDataPtrY = &Sampling_GraphHystoryVacuum[0U];
            bufferDataPtrX = &Sampling_GraphHystoryMinuteValue[0U];
            bufferDataSize = GRAPH_MINUTE_TO_KEEP_DATA;
            somethingToDraw = true;
            break;
        case GRAPH_DATA_TO_SHOW_METER_TEMPERATURE:
            bufferDataPtrY = &Sampling_GraphHystoryTemperature[0U];
            bufferDataPtrX = &Sampling_GraphHystoryMinuteValue[0U];
            bufferDataSize = GRAPH_MINUTE_TO_KEEP_DATA;
            somethingToDraw = true;
            break;
        default:
            break;
    }
    
    if(somethingToDraw != false)
    {
        //debug_print("somethingToDraw");
        slong firstMinuteUsedOnX = -1;
        
        /* If there are mode data inside the statistic, set the max to the max pointer of graph */
        ulong bufferDataCounterClamped = bufferDataCounter;
        if(bufferDataCounterClamped > CHARTS_GRAPH_MAX_POINTS_NUMBER)
        {
            bufferDataCounterClamped = CHARTS_GRAPH_MAX_POINTS_NUMBER;
        }
        
        /* copy data desired data to draw inside the xValues_1 and yValues1_1 */
        for(i = 0; (i < bufferDataCounter) && (i < bufferDataSize) && (i < CHARTS_GRAPH_MAX_POINTS_NUMBER); i++)
        {
            ulong graphIndex = ((CHARTS_GRAPH_MAX_POINTS_NUMBER - bufferDataCounterClamped) + i);
            //ulong dataIndex = (bufferDataCounter - bufferDataCounterClamped) + i; /* SCREEN DATA BLOCKED ON LAST ITEMS*/
            slong dataIndex = ((Sampling_LastIndexToShowOnGraph + 1) - CHARTS_GRAPH_MAX_POINTS_NUMBER); /* SCREEN FREE TO MOVE*/
            if(dataIndex < 0)
            {
                dataIndex = 0;
            }
            dataIndex += i;
            
            //debug_print("GI %d - DI %d", graphIndex, dataIndex);
            
            if(firstMinuteUsedOnX == -1)
            {
                firstMinuteUsedOnX = bufferDataPtrX[dataIndex];
                //debug_print("firstMinuteUsedOnX %d", firstMinuteUsedOnX);
            }
            
            /* Draw into graph array buffer */
            ptrGraphX[graphIndex] = FLOAT_TO_WORD(bufferDataPtrX[dataIndex]);
            ptrGraphY[graphIndex] = FLOAT_TO_WORD(bufferDataPtrY[dataIndex]);
            
            /* find min and max, using real data */
            if(bufferDataPtrY[dataIndex] > dataMaxValue)
            {
                dataMaxValue = bufferDataPtrY[dataIndex];
            }
            if(bufferDataPtrY[dataIndex] < dataMinValue)
            {
                dataMinValue = bufferDataPtrY[dataIndex];
            }
        }
        
        /* Fill with 0's */
        
        for(i = 0; i < (CHARTS_GRAPH_MAX_POINTS_NUMBER - bufferDataCounterClamped); i++)
        {
            /* Draw into graph array buffer */
            ptrGraphX[i] = FLOAT_TO_WORD(0.0f);
            /* 
                FLOAT_TO_WORD(
                                Sampling_drawDataOnGraphManageXValue(
                                    (
                                        firstMinuteUsedOnX - 
                                        (CHARTS_GRAPH_MAX_POINTS_NUMBER - bufferDataCounterClamped)
                                    )
                                    + i
                                )
                );*/
            ptrGraphY[i] = FLOAT_TO_WORD(0.0f);
        }
        
        dataMaxValue += 1.0f;
        dataMinValue -= 1.0f;
        
        graph_pointsNumber = CHARTS_GRAPH_MAX_POINTS_NUMBER; /* Max values of points */
        
        yMaxValue = FLOAT_TO_WORD(dataMaxValue);
        yMinValue = FLOAT_TO_WORD(dataMinValue);
        
        xMax = graph_pointsNumber;
        
        /* Fill min and max axis with the min max value finded  */
        for(i = 0; i < bufferDataSize; i++)
        {
            ptrGraphYMaxValues[i] = yMaxValue;
            ptrGraphYMinValues[i] = yMinValue;
        }
        
        graph_redraw = true;
    }
}   

float Sampling_GetFinalizedFlow(void)
{
    return Sampling_FinalizedFlow;
}

float Sampling_GetFinalizedMeterTemperature(void)
{
    return Sampling_FinalizedMeterTemperature;
}   

float Sampling_GetFinalizedTemperatureDuctCelsius(void)
{
    return Sampling_FinalizedTemperatureDuctCelsius;
}

float Sampling_GetFinalizedAbsoluteStaticPressurePa(void)
{
    //debug_print("Sampling_GetFinalizedAbsoluteStaticPressurePa %f", Sampling_FinalizedAbsoluteStaticPressurePa);
    return Sampling_FinalizedAbsoluteStaticPressurePa;
}

float Sampling_GetFinalizedSpeed(void)
{
    return Sampling_FinalizedSpeed;
}

float Sampling_GetFinalizedQv(void)
{
    return Sampling_FinalizedQv;
}

float Sampling_GetFinalizedQvn(void)
{
    return Sampling_FinalizedQvn;
}

float Sampling_GetFinalizedBarometricPressureMbar(void)
{
    //debug_print("Sampling_GetFinalizedBarometricPressureMbar %f", Sampling_FinalizedBarometricPressureMbar);
    return Sampling_FinalizedBarometricPressureMbar;
}

float Sampling_GetFinalizedCondense(void)
{
    //debug_print("Sampling_GetFinalizedCondense %f", Sampling_FinalizedCondense);
    return Sampling_FinalizedCondense;
}

float Sampling_GetFinalizedDifferentialPressurePa(void)
{
    //debug_print("Sampling_GetFinalizedDifferentialPressurePa %f", Sampling_FinalizedDifferentialPressurePa);
    return Sampling_FinalizedDifferentialPressurePa;
}

void Sampling_ManageNozzleDiameterUpAndDownButton(void)
{
    if(common_nozzleDiameter_firstSlotSelected > 0)
    {
        common_nozzleDiameterButtonDown_enable = true;
    }
    else
    {
        common_nozzleDiameterButtonDown_enable = false;
    }
    
    if(common_nozzleDiameter_firstSlotSelected < (SAMPLING_NOZZLE_DIAMETER_MAX_NUMBER - SAMPLING_NOZZLE_SLOT_NUMBER))
    {
        common_nozzleDiameterButtonUp_enable = true;
    }
    else
    {
        common_nozzleDiameterButtonUp_enable = false;
    }
   debug_print("common_nozzleDiameter_firstSlotSelected %d", common_nozzleDiameter_firstSlotSelected);
} 

ulong Sampling_ManageNozzleFlowGetColorSelected(ubyte isSelected)
{
    ulong returnValue = YELLOW;
    if(isSelected != false)
    {
        returnValue = DARKGREEN;
    }
    
    if(common_isokineticFlow == SAMPLING_ISOKINETIC_FLOW_MANUAL)
    {
        returnValue = DARKGRAY;
    }
    
    return returnValue;
}

ulong Sampling_ManageNozzleDiameterGetColorSelected(ubyte isSelected)
{
    ulong returnValue = YELLOW;
    if(isSelected != false)
    {
        returnValue = DARKGREEN;
    }
    
    return returnValue;
}

float Sampling_ManageNozzleDiameterGetCalculatedFlow(SAMPLING_NOZZLE_DIAMETER_t nozzle)
{
    float returnValue = 0.0f;
    switch(nozzle)
    {
        case SAMPLING_NOZZLE_DIAMETER_4MM:
            returnValue = WORD_TO_FLOAT(common_flow4mm_value);
            break;
        case SAMPLING_NOZZLE_DIAMETER_5MM:
            returnValue = WORD_TO_FLOAT(common_flow5mm_value);
            break;
        case SAMPLING_NOZZLE_DIAMETER_6MM:
            returnValue = WORD_TO_FLOAT(common_flow6mm_value);
            break;
        case SAMPLING_NOZZLE_DIAMETER_7MM:
            returnValue = WORD_TO_FLOAT(common_flow7mm_value);
            break;
        case SAMPLING_NOZZLE_DIAMETER_8MM:
            returnValue = WORD_TO_FLOAT(common_flow8mm_value);
            break;
        case SAMPLING_NOZZLE_DIAMETER_9MM:
            returnValue = WORD_TO_FLOAT(common_flow9mm_value);
            break;
        case SAMPLING_NOZZLE_DIAMETER_10MM:
            returnValue = WORD_TO_FLOAT(common_flow10mm_value);
            break;
        case SAMPLING_NOZZLE_DIAMETER_12MM:
            returnValue = WORD_TO_FLOAT(common_flow12mm_value);
            break;
        case SAMPLING_NOZZLE_DIAMETER_14MM:
            returnValue = WORD_TO_FLOAT(common_flow14mm_value);
            break;
        case SAMPLING_NOZZLE_DIAMETER_16MM:
            returnValue = WORD_TO_FLOAT(common_flow16mm_value);
            break;
        default:
            break;
    }
    return returnValue;
}

void Sampling_ManageNozzleDiameterPopulate(ubyte managePlayEnable, ubyte nozzleButtonEnable, BooleanSetCallbackFct bSet)
{
    ushort ushortValue = 0;
    float pumpMax = 0.0f;
    float pumpMin = 0.0f;
    float nozzleDiameterSelected = Sampling_GetNozzleDiameter(common_nozzleDiameter);
    ubyte selectAnother = false; 
    
    if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MIN, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1))
    {
        pumpMin = (((float)ushortValue) / 100);
    }
    if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MAX, &ushortValue, EEPM_VALUE_TYPE_USHORT, 1))
    {
        pumpMax = (((float)ushortValue) / 100);
    }
    
    /* ******************************************************************************************************/
    /*                                              SLOT 0                                                  */
    /* ******************************************************************************************************/
    common_nozzleDiameter_slot_0 = Sampling_GetNozzleDiameter(common_nozzleDiameter_firstSlotSelected + 0);
    common_nozzleDiameter_slot_0_backgroundColor = Sampling_ManageNozzleDiameterGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_0);
    common_nozzleFlow_slot_0_backgroundColor = Sampling_ManageNozzleFlowGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_0);
    common_nozzleFlow_slot_0_value = FLOAT_TO_WORD(Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 0));
    if(common_isokineticFlow == SAMPLING_ISOKINETIC_FLOW_AUTOMATIC /* SAMPLING_ISOKINETIC_FLOW_AUTOMATIC SAMPLING_ISOKINETIC_FLOW_MANUAL*/)
    {
        if(
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 0) < pumpMin) ||
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 0) > pumpMax)
        )
        {
            common_nozzleDiameter_slot_0_backgroundColor = DARKGRAY;
            common_nozzleFlow_slot_0_backgroundColor = DARKGRAY;
            common_nozzle_slot0_enable = false;
            if(nozzleDiameterSelected == common_nozzleDiameter_slot_0)
            {
                /* */
                //debug_print("SLOT 0 selected mm %.2f - SELECT ANOTHER", nozzleDiameterSelected);
                selectAnother = true;
            }
        }
        else
        {
            common_nozzle_slot0_enable = true;
        }
    }
    else
    {
        /* Manuale */
        common_nozzle_slot0_enable = true;
    }
    /* ******************************************************************************************************/
    /*                                              SLOT 1                                                  */
    /* ******************************************************************************************************/
    common_nozzleDiameter_slot_1 = Sampling_GetNozzleDiameter(common_nozzleDiameter_firstSlotSelected + 1);
    common_nozzleDiameter_slot_1_backgroundColor = Sampling_ManageNozzleDiameterGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_1);
    common_nozzleFlow_slot_1_backgroundColor = Sampling_ManageNozzleFlowGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_1);
    common_nozzleFlow_slot_1_value = FLOAT_TO_WORD(Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 1));
    if(common_isokineticFlow == SAMPLING_ISOKINETIC_FLOW_AUTOMATIC /* SAMPLING_ISOKINETIC_FLOW_AUTOMATIC SAMPLING_ISOKINETIC_FLOW_MANUAL*/)
    {
        if(
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 1) < pumpMin) ||
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 1) > pumpMax)
        )
        {
            common_nozzleDiameter_slot_1_backgroundColor = DARKGRAY;
            common_nozzleFlow_slot_1_backgroundColor = DARKGRAY;
            common_nozzle_slot1_enable = false;
            if(nozzleDiameterSelected == common_nozzleDiameter_slot_1)
            {
                /* */
                //debug_print("SLOT 1 selected mm %.2f - SELECT ANOTHER", nozzleDiameterSelected);
                selectAnother = true;
            }
        }
        else
        {
            common_nozzle_slot1_enable = true;
        }
    }
    else
    {
        /* Manuale */
        common_nozzle_slot1_enable = true;
    }
    
    /* ******************************************************************************************************/
    /*                                              SLOT 2                                                  */
    /* ******************************************************************************************************/
    common_nozzleDiameter_slot_2 = Sampling_GetNozzleDiameter(common_nozzleDiameter_firstSlotSelected + 2);
    common_nozzleDiameter_slot_2_backgroundColor = Sampling_ManageNozzleDiameterGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_2);
    common_nozzleFlow_slot_2_backgroundColor = Sampling_ManageNozzleFlowGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_2);
    common_nozzleFlow_slot_2_value = FLOAT_TO_WORD(Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 2));
    if(common_isokineticFlow == SAMPLING_ISOKINETIC_FLOW_AUTOMATIC /* SAMPLING_ISOKINETIC_FLOW_AUTOMATIC SAMPLING_ISOKINETIC_FLOW_MANUAL*/)
    {
        if(
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 2) < pumpMin) ||
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 2) > pumpMax)
        )
        {
            common_nozzleDiameter_slot_2_backgroundColor = DARKGRAY;
            common_nozzleFlow_slot_2_backgroundColor = DARKGRAY;
            common_nozzle_slot2_enable = false;
            if(nozzleDiameterSelected == common_nozzleDiameter_slot_2)
            {
                /* */
                //debug_print("SLOT 2 selected mm %.2f - SELECT ANOTHER", nozzleDiameterSelected);
                selectAnother = true;
            }
        }
        else
        {
            common_nozzle_slot2_enable = true;
        }
    }
    else
    {
        /* Manuale */
        common_nozzle_slot2_enable = true;
    }
    /* ******************************************************************************************************/
    /*                                              SLOT 3                                                  */
    /* ******************************************************************************************************/
    common_nozzleDiameter_slot_3 = Sampling_GetNozzleDiameter(common_nozzleDiameter_firstSlotSelected + 3);
    common_nozzleDiameter_slot_3_backgroundColor = Sampling_ManageNozzleDiameterGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_3);
    common_nozzleFlow_slot_3_backgroundColor = Sampling_ManageNozzleFlowGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_3);
    common_nozzleFlow_slot_3_value = FLOAT_TO_WORD(Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 3));
    if(common_isokineticFlow == SAMPLING_ISOKINETIC_FLOW_AUTOMATIC /* SAMPLING_ISOKINETIC_FLOW_AUTOMATIC SAMPLING_ISOKINETIC_FLOW_MANUAL*/)
    {
        if(
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 3) < pumpMin) ||
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 3) > pumpMax)
        )
        {
            common_nozzleDiameter_slot_3_backgroundColor = DARKGRAY;
            common_nozzleFlow_slot_3_backgroundColor = DARKGRAY;
            common_nozzle_slot3_enable = false;
            if(nozzleDiameterSelected == common_nozzleDiameter_slot_3)
            {
                /* */
                //debug_print("SLOT 3 selected mm %.2f - SELECT ANOTHER", nozzleDiameterSelected);
                selectAnother = true;
            }
        }
        else
        {
            common_nozzle_slot3_enable = true;
        }
    }
    else
    {
        /* Manuale */
        common_nozzle_slot3_enable = true;
    }
    /* ******************************************************************************************************/
    /*                                              SLOT 4                                                  */
    /* ******************************************************************************************************/
    common_nozzleDiameter_slot_4 = Sampling_GetNozzleDiameter(common_nozzleDiameter_firstSlotSelected + 4);
    common_nozzleDiameter_slot_4_backgroundColor = Sampling_ManageNozzleDiameterGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_4);
    common_nozzleFlow_slot_4_backgroundColor = Sampling_ManageNozzleFlowGetColorSelected(nozzleDiameterSelected == common_nozzleDiameter_slot_4);
    common_nozzleFlow_slot_4_value = FLOAT_TO_WORD(Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 4));
    
    if(common_isokineticFlow == SAMPLING_ISOKINETIC_FLOW_AUTOMATIC /* SAMPLING_ISOKINETIC_FLOW_AUTOMATIC SAMPLING_ISOKINETIC_FLOW_MANUAL*/)
    {
        if(
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 4) < pumpMin) ||
            (Sampling_ManageNozzleDiameterGetCalculatedFlow(common_nozzleDiameter_firstSlotSelected + 4) > pumpMax)
        )
        {
            common_nozzleDiameter_slot_4_backgroundColor = DARKGRAY;
            common_nozzleFlow_slot_4_backgroundColor = DARKGRAY;
            common_nozzle_slot4_enable = false;
            if(nozzleDiameterSelected == common_nozzleDiameter_slot_4)
            {
                /* */
                //debug_print("SLOT 4 selected mm %.2f - SELECT ANOTHER", nozzleDiameterSelected);
                selectAnother = true;
            }
        }
        else
        {
            common_nozzle_slot4_enable = true;
        }
    }
    else
    {
        /* Manuale */
        common_nozzle_slot4_enable = true;
    }
    
    /* If I'm running, the slots are not enabled */
    if(managePlayEnable == false)
    {
        common_nozzle_slot0_enable = false;
        common_nozzle_slot1_enable = false;
        common_nozzle_slot2_enable = false;
        common_nozzle_slot3_enable = false;
        common_nozzle_slot4_enable = false;
    }
    
    /* ******************************************************************************************************/
    /*                                              SELECT ANOTHER NOZZLE                                   */
    /* ******************************************************************************************************/
    if(selectAnother != false)
    {
        //debug_print("SELECT ANOTHER FUNCTION", nozzleDiameterSelected);
        common_selectedFlowRow_backgroundColor = RED;
        if(managePlayEnable != false)
        {
            if(bSet != NULL)
            {
                bSet(false);
            }
        }
    }
    else
    {
        common_selectedFlowRow_backgroundColor = WHITE;
        if(managePlayEnable != false)
        {
            if(bSet != NULL)
            {
                bSet(true);
            }
        }
    }
}

void Sampling_ManageNozzleDiameterInit(void)
{
    ulong i;
    common_nozzleDiameter = SAMPLING_NOZZLE_DIAMETER_4MM;
    common_nozzleDiameter_firstSlotSelected = common_nozzleDiameter; /* Variable containing the first diameter of the mobilescreen */
    
    Sampling_ManageNozzleDiameterUpAndDownButton();
}

void Sampling_ManageNozzleDiameterDownButton(void)
{
    if(common_nozzleDiameter_firstSlotSelected > 0)
    {
        common_nozzleDiameter_firstSlotSelected--;
    }
    //Sampling_ManageNozzleDiameterPopulate();
    Sampling_ManageNozzleDiameterUpAndDownButton();
}

void Sampling_ManageNozzleDiameterUpButton(void)
{
    if(common_nozzleDiameter_firstSlotSelected < (SAMPLING_NOZZLE_DIAMETER_MAX_NUMBER - SAMPLING_NOZZLE_SLOT_NUMBER))
    {
        common_nozzleDiameter_firstSlotSelected++;
    }
    //Sampling_ManageNozzleDiameterPopulate();
    Sampling_ManageNozzleDiameterUpAndDownButton();
}

void Sampling_ManageNozzleDiameterSlotSelected(ulong slotSelected)
{
    if(slotSelected < SAMPLING_NOZZLE_SLOT_NUMBER)
    {
        debug_print("Slot selected %d", slotSelected);
        common_nozzleDiameter = common_nozzleDiameter_firstSlotSelected + slotSelected;
        //Sampling_ManageNozzleDiameterPopulate();
        Sampling_ManageNozzleDiameterSelectedFlow();
    }
}     

float Sampling_GetBarometricPressure(void)
{
    /* If sensore = mounted && sensore = enabled read the sensor 
            read data from network
       else
            use the value in parameter
    */
    float returnValue = 0.0f;
    ubyte mounted = false;
    ubyte enabled = false;
    NETWORK_GetExternPressureMounted(&mounted);
    
    if((mounted != false) && (parameter_barometricPressure_useSensorValue != false))
    {
        NETWORK_GetExternDigitalBarometer(&returnValue);
    }
    else
    {
        returnValue = (float)param_barometriPressure_value;
    }
    
    return returnValue;
}    

ulong Get_InstrumentSerialNumber(void)
{
    /* TODO */
    return 1;
}

SAMPLING_ERROR_t Sampling_LowFlowErrorFct(float actualFlow, float flowLimit, ubyte runCheck)
{
#define SAMPLING_LOW_FLOW_ERROR_NUMBER_HYSTERESYS  3U 
    SAMPLING_ERROR_t errorDetected = SAMPLING_ERROR_NONE;
    ubyte eepromUbyteValue;
    static ubyte lowFlowLastSec = 0;
    static ubyte lowFlowWarning = false;
    static ulong lowFlowCounterSec = 0;
    static ulong lowFlowCounterLimit = 0;

    if(runCheck != false)
    {
        if(actualFlow < flowLimit)
        {
            if(lowFlowWarning != false)
            {
                if(lowFlowLastSec != datarioSecondi)
                {
                    lowFlowLastSec = datarioSecondi;
                    lowFlowCounterSec++;
                    debug_print("Increment low flow warning sec %d - remaining sec %d", lowFlowCounterSec, (lowFlowCounterLimit - lowFlowCounterSec));
                }
                
                if(lowFlowCounterSec > lowFlowCounterLimit) /* Maggiore perch� guardando il datario il primo secondo potrebbe non essere mai un vero secondo allora maggiore */
                {
                    debug_print("CLOGGED FILTER %f %f", actualFlow, flowLimit);
                    errorDetected = SAMPLING_ERROR_CLOGGED_FILTER;
                    
                    lowFlowWarning = false;
                    lowFlowCounterLimit = 0;
                    lowFlowCounterSec = 0;
                    lowFlowLastSec = 0;
                }
            }
            else
            {
                /* First low flow */
                debug_print("Start low flow warning at %2d:%2d:%2d", datarioOre, datarioMinuti, datarioSecondi);
                lowFlowWarning = true;
                lowFlowLastSec = datarioSecondi;
                lowFlowCounterSec = 0;
                if(
                    //(STD_RETURN_OK == EEPM_GetId(EEPM_ID_MOTC_FLOW_FILTERED_TIME_CALCULATION_SEC, &eepromUbyteValue, EEPM_VALUE_TYPE_UBYTE, 1)) &&
                    (STD_RETURN_OK == EEPM_GetId(EEPM_ID_MOTC_LONGER_METER_SPEED_CORRECTION_SEC, &eepromUbyteValue, EEPM_VALUE_TYPE_UBYTE, 1))
                )
                {
                    lowFlowCounterLimit = (((ulong)(eepromUbyteValue)) * SAMPLING_LOW_FLOW_ERROR_NUMBER_HYSTERESYS);
                    debug_print("Flow filtered time in eeprom %d sec, time limit sec %d", eepromUbyteValue, lowFlowCounterLimit);
                }
                else
                {
                    debug_print("[ERRO] Environmental_LowFlowErrorFct impossible read EEPM_ID_MOTC_FLOW_FILTERED_TIME_CALCULATION_SEC");
                }
            }
        }
        else
        {
            /* Reset low flow warning */
            if(lowFlowWarning != false)
            {
                debug_print("Reset low flow warning at %2d:%2d:%2d after %d sec", datarioOre, datarioMinuti, datarioSecondi, lowFlowCounterSec);
                lowFlowWarning = false;
            }
        }
    }
    
    return errorDetected;
}

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void exchangeBus_init()
{
    ulong i;
    
    ReportFile_reportLocationSave = REPORT_FILE_LOCATION_DEFAULT_SAVE;
    ReportFile_reportLocationExport = REPORT_FILE_LOCATION_DEFAULT_EXPORT;

    Client_ClientsTotalNumber = 0;
    Client_SelectedOne = 0;
    
        
    for(i = 0; i < CLIENT_FILE_CLIENTS_MAX_NUMBER; i++)
    {
        memory_set(&ClientsFile_Clients[0], 0x00U, sizeof(ClientsFile_Clients)); 
    }
    Client_ClientsTotalNumber = 0;
    Client_SelectedOne = 0;
    
    Sampling_manageSamplingTimeInit(0);
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
//void exchangeBus()
//{
//}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void exchangeBus_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void exchangeBus_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
//void exchangeBus_thread()
//{
//}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void exchangeBus_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void exchangeBus_shutdown()
//{
//}
