/*
 * util.h
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */

#ifndef UTIL_H_
#define UTIL_H_

/* Includes ------------------------------------------------------------------*/
#include "typedef.h"

/* Public define ------------------------------------------------------------*/
typedef struct Timer_s
{
    ulong counter;
    ulong counterLimit;
    ubyte running;
    ubyte expired;
}Timer_t;

typedef struct Queue_s
{
    ulong head;
    ulong tail;
    ulong size;
    ulong sizeMax;
    ubyte* flags;   /* pointer to array in dynamic memory of flags */
    ubyte** elements; /* pointer to array of pointers in dynamic memory of pointers, use of ubyte pointer only to avoid mempory disalignment */
}Queue_t;

#define MINS_TO_SECS(x) ((x) * 60U)
#define MS_TO_US(x) ((x) * 1000)

#define MEGABYTES_TO_BYTES(x) ((x) * 1048576U)
#define KILOBYTES_TO_BYTES(x) ((x) * 1024U)

#define BCD_HI_TO_CHAR(x) ((sbyte)(((x & 0xF0) >> 4U) + 0x30U))
#define BCD_LO_TO_CHAR(x) ((sbyte)(((x & 0x0F) >> 0U) + 0x30U))

/* Public variables ----------------------------------------------------------*/

/* Public function prototypes -----------------------------------------------*/
void UTIL_IncreaseAddDaysToDate(ushort* year, ubyte* month, ubyte* day, ulong dayToAdd);
ubyte UTIL_DateIsOverAnotherDate(ushort yearRef, ubyte monthRef, ubyte dayRef, ushort yearActual, ubyte monthActual, ubyte dayActual);

STD_RETURN_t UTIL_PrintBufferOnHex(ubyte* buffer, ulong bufferSize);

void UTIL_SetFlag(ubyte* flags, ubyte flagValue, ulong flagPosition, ulong maxSize);
ubyte UTIL_GetFlag(ubyte* flags, ulong flagPosition, ulong maxSize);   
ubyte UTIL_GetFlagAndReset(ubyte* flags, ulong flagPosition, ulong maxSize);

ulong UTIL_GetBufferStringOfUshortLength(ushort* srcPtr, ulong srcPtrSize);
STD_RETURN_t UTIL_BufferFromUShortToSByte(ushort* srcPtr, ulong srcPtrSize, sbyte* destPtr, ulong destPtrSize);
STD_RETURN_t UTIL_BufferFromSbyteToUShort(sbyte* srcPtr, ulong srcPtrSize, ushort* destPtr, ulong destPtrSize);


STD_RETURN_t UTIL_TimerInit(Timer_t *timer);
STD_RETURN_t UTIL_TimerStart(Timer_t *timer, ulong timeMs);
STD_RETURN_t UTIL_TimerIsExpired(Timer_t *timer, ubyte* isExpired);
STD_RETURN_t UTIL_TimerIsRunning(Timer_t *timer, ubyte* isRunning);
STD_RETURN_t UTIL_TimerStop(Timer_t *timer);
STD_RETURN_t UTIL_TimerPause(Timer_t *timer);
STD_RETURN_t UTIL_TimerResume(Timer_t *timer);
STD_RETURN_t UTIL_TimerIncrement(Timer_t *timer, ulong timeMsElapsed);
STD_RETURN_t UTIL_TimerGetRemainingTimeMs(Timer_t *timer, ulong* remainingTime);

ubyte UTIL_string_contain(sbyte* str, ulong strSize, ulong strStartIndex, sbyte* pattern, ulong patternSize);
ulong UTIL_string_contain_regular(sbyte* str, ulong str_size, sbyte* pattern, ulong pattern_size);

ulong UTIL_ClampULongValue(ulong value, ulong min, ulong max);
ulong UTIL_ClampSLongValue(slong value, slong min, slong max);
ushort UTIL_ClampUShortValue(ushort value, ushort min, ushort max);
sshort UTIL_ClampSShortValue(sshort value, sshort min, sshort max);
ubyte UTIL_ClampUByteValue(ubyte value, ubyte min, ubyte max);
sbyte UTIL_ClampSByteValue(sbyte value, sbyte min, sbyte max);

STD_RETURN_t UTIL_QueueInit(Queue_t* queuePtr, ulong sizeMax);
STD_RETURN_t UTIL_QueueSend(Queue_t* queuePtr, void* ptrElement);
STD_RETURN_t UTIL_QueueReceive(Queue_t* queuePtr, void** ptrElement);
STD_RETURN_t UTIL_QueueClear(Queue_t* queuePtr);
STD_RETURN_t UTIL_QueueSize(Queue_t* queuePtr, ulong* sizePtr);
STD_RETURN_t UTIL_QueueIsEmpty(Queue_t* queuePtr, ubyte* isEmptyPtr);
STD_RETURN_t UTIL_QueueIsFull(Queue_t* queuePtr, ubyte* isFullPtr);
STD_RETURN_t UTIL_QueueDelete(Queue_t* queuePtr);

ubyte UTIL_MemoryEqualTo(ubyte* memoryPtr, ulong memoryLength, ubyte value);

#ifdef __cplusplus
}
#endif

#endif /* UTIL_H_ */