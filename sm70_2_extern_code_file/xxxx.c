/*
 * xxxx.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define XXXX_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"

/* Private define ------------------------------------------------------------*/
#define HMI_MY_ID HMI_ID_XXXX

/* #define XXXX_ENABLE_DEBUG_COUNTERS */
/* #define XXXX_NORMAL_ENABLE */
/* #define XXXX_FAST_ENABLE */
/* #define XXXX_LOW_PRIO_ENABLE */
/* #define XXXX_THREAD_ENABLE */

#define XXXX_THREAD_SLEEP_MS 50U

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Xxxx_logFlag(FLAG_t flag);
static STD_RETURN_t Xxxx_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Xxxx_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Xxxx_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Xxxx_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t Xxxx_initVariable(void);
#ifdef XXXX_NORMAL_ENABLE
static STD_RETURN_t Xxxx_manageButtons(ulong execTimeMs);
static STD_RETURN_t Xxxx_manageScreen(ulong execTimeMs);
#endif /* #ifdef XXXX_NORMAL_ENABLE */
#ifdef XXXX_THREAD_ENABLE
static STD_RETURN_t Xxxx_modelInit(void);
static STD_RETURN_t Xxxx_model(ulong execTimeMs);
#endif /* #ifdef XXXX_THREAD_ENABLE */

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Xxxx_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Xxxx_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Xxxx_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Xxxx_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t Xxxx_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t Xxxx_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Xxxx_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef XXXX_NORMAL_ENABLE
static STD_RETURN_t Xxxx_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(xxflagxx_backButton_released != false) /* These flag is reset by GUI */
    {
        xxflagxx_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Xxxx_logFlag(FLAG_XXXX_BUTTON_BACK);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Xxxx_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef XXXX_NORMAL_ENABLE */

#ifdef XXXX_NORMAL_ENABLE
static STD_RETURN_t Xxxx_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(MessageScreen_isEnteredHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
        }
    }
    
    if(MessageScreen_isExitedHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Xxxx_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef XXXX_THREAD_ENABLE
static STD_RETURN_t Xxxx_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Xxxx_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef XXXX_THREAD_ENABLE */

#ifdef XXXX_THREAD_ENABLE
static STD_RETURN_t Xxxx_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Xxxx_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef XXXX_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void xxxx_init()
{
    (void)Xxxx_initVariable();
}

#ifdef XXXX_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void xxxx()
{
#ifdef XXXX_ENABLE_DEBUG_COUNTERS
    Xxxx_NormalRoutine++;
#endif
    (void)Xxxx_manageButtons(Execution_Normal);
    (void)Xxxx_manageScreen(Execution_Normal);
}
#endif /* #ifdef XXXX_NORMAL_ENABLE */

#ifdef XXXX_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void xxxx_fast()
{
#ifdef XXXX_ENABLE_DEBUG_COUNTERS
    Xxxx_FastRoutine++;
#endif
}
#endif /* #ifdef XXXX_FAST_ENABLE */

#ifdef XXXX_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void xxxx_low_priority()
{
#ifdef XXXX_ENABLE_DEBUG_COUNTERS
    Xxxx_LowPrioRoutine++;
#endif
}
#endif /* #ifdef XXXX_LOW_PRIO_ENABLE */

#ifdef XXXX_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void xxxx_thread()
{
    ulong execTime = XXXX_THREAD_SLEEP_MS;
    
    (void)Xxxx_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef XXXX_ENABLE_DEBUG_COUNTERS
        Xxxx_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)Xxxx_model(execTime);
    }
}
#endif /* #ifdef XXXX_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void xxxx_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void xxxx_shutdown()
//{
//}
