/*
 * buzm.c
 *
 *  Created on: 19/09/2019
 *      Author: Andrea Tonello
 */
#define BUZM_C
/* Includes ------------------------------------------------------------------*/
#include "buzm.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
/* #define BUZM_DEBUG_PRINTF */

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static ubyte timerStartCommand = false;
static ubyte timerStopCommand = false;
static ulong timerSoundCycle = 0;

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
void Buzm_InitVariables(void);

/* Private functions ---------------------------------------------------------*/
void Buzm_InitVariables(void)
{
    timerStartCommand = false;
    timerStopCommand = false;
    timerSoundCycle = 0;
}

/* Public functions ----------------------------------------------------------*/
void BUZM_startBuzzer(void)
{
    timerStartCommand = true;
    timerSoundCycle = 0xFFFFFFFFU; /* infinite */
#ifdef BUZM_DEBUG_PRINTF
    debug_print("start buzzer infinite");
#endif
}

void BUZM_startBuzzerForMs(ulong timeMs)
{
    timerSoundCycle = (timeMs / BUZM_SOUND_TICK_MS);
    BuzzerFrequency = BUZM_SOUND_DEFAULT_FREQUENCY;
    timerStartCommand = true;
#ifdef BUZM_DEBUG_PRINTF
    debug_print("start buzzer ms %d and cycle %d", timeMs, timerSoundCycle);
#endif
}

void BUZM_startBuzzerForMsAndFrequency(ulong timeMs, ushort frequency)
{
    timerSoundCycle = (timeMs / BUZM_SOUND_TICK_MS);
    if((frequency >= BUZM_SOUND_FREQUENCY_MIN) && (frequency <= BUZM_SOUND_FREQUENCY_MAX))
    {
        BuzzerFrequency = frequency;
    }
    else
    {
        BuzzerFrequency = BUZM_SOUND_DEFAULT_FREQUENCY;
    }
    
    timerStartCommand = true;
}

void BUZM_stopBuzzer(void)
{
    timerStopCommand = false;
#ifdef BUZM_DEBUG_PRINTF
    debug_print("stop buzzer");
#endif
}

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void BUZM_init()
{
    BuzzerEnable = false;
    BuzzerFrequency = BUZM_SOUND_DEFAULT_FREQUENCY;
    Buzm_InitVariables();
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void BUZM()
{
    /* stop before the start because if the buzzer must run for at minimum of BUZM_SOUND_TICK_MS 
    It is necessary at least one run to stop the buzzer 
    */
    if(timerStopCommand != false)
    {
        timerStopCommand = false;
        /* disable buzzer */
        BuzzerEnable = false;
        timerSoundCycle = 0;
#ifdef BUZM_DEBUG_PRINTF
        debug_print("timerStopCommand managed");
#endif
    }

    if(timerStartCommand != false) 
    {
        timerStartCommand = false;
        if(timerSoundCycle > 0) /* check if timerSoundCycle has been set, otherwise an error occurs */
        {
            BuzzerEnable = true;
#ifdef BUZM_DEBUG_PRINTF
            debug_print("timerStartCommand managed cycle %d", timerSoundCycle);
#endif
            /* buzzer must star with BUZM_startBuzzer or BUZM_startBuzzerForMs*/
        }
        else
        {
#ifdef BUZM_DEBUG_PRINTF
            debug_print("timerStartCommand managed with no  cycle");
#endif
        }
    }

    /* running */
    if(timerSoundCycle > 0)
    {
        if(timerSoundCycle != 0xFFFFFFFFU) /* if it is not infinite decrease the counter */
        {
            timerSoundCycle--;
#ifdef BUZM_DEBUG_PRINTF
            debug_print("timerSoundcycle decremented: %d", timerSoundCycle);
#endif
        }
        
        if(timerSoundCycle <= 0) /* in case the counter reach the 0 cycle, stop the buzzer */
        {
            timerStopCommand = true;
#ifdef BUZM_DEBUG_PRINTF
            debug_print("timerSoundcycle reachs 0 set timerStopCommand to true");
#endif
        }
    }
    else
    {
        timerSoundCycle = 0;
    }
}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void BUZM_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void BUZM_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
//void BUZM_thread()
//{
//}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
void BUZM_close()
{
    BuzzerEnable = false;
}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void BUZM_shutdown()
//{
//}
