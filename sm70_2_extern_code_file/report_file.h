#ifndef REPORT_FILE_H
#define REPORT_FILE_H
/*
 * report_file.h
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */
/* Includes ------------------------------------------------------------------*/
#include "typedef.h"

/* Public define ------------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/
extern const sbyte* ReportFile_reportDuctShapes[SAMPLING_DUCT_SHAPE_MAX_NUMBER];
extern const sbyte* ReportFile_reportMeasureUnit[SAMPLING_MEASURE_UNIT_MAX_NUMBER];

/* Public function prototypes -----------------------------------------------*/
STD_RETURN_t ReportFile_createName(REPORT_NAME_CODE_t reportType);
STD_RETURN_t ReportFile_deleteReport(void);

STD_RETURN_t ReportFile_writeStarReportHeader(REPORT_NAME_CODE_t reportType);
STD_RETURN_t ReportFile_writeInstrumentIds(void);
STD_RETURN_t ReportFile_writeClientId(void); /* anagrafica*/
STD_RETURN_t ReportFile_writeOperatorId(void);
STD_RETURN_t ReportFile_writeFinalDataStopCauseAndError(void);
STD_RETURN_t ReportFile_writeDelayedStart(void);
STD_RETURN_t ReportFile_writeH24DelayedStart(void);
STD_RETURN_t ReportFile_writeStart(void);
STD_RETURN_t ReportFile_writeStartForced(void);
STD_RETURN_t ReportFile_writeDelayedPaused(void);
STD_RETURN_t ReportFile_writeRunningPaused(void);
STD_RETURN_t ReportFile_write5MinutesData(void);
STD_RETURN_t ReportFile_writeDelayedResumed(void);
STD_RETURN_t ReportFile_writeRunningResumed(void);
STD_RETURN_t ReportFile_writeNozzleDiameterChanged(void);
STD_RETURN_t ReportFile_writeAspirationFlowChanged(float newAspirationFlow, float lastAspirationFlow);
STD_RETURN_t ReportFile_writeWarning(SYST_WARNING_t warningCode, ubyte warningSubCode);
STD_RETURN_t ReportFile_writeError(SYST_ERROR_t errorCode, ubyte errorSubCode);
STD_RETURN_t ReportFile_writePausedNoPowerSupply(void);
STD_RETURN_t ReportFile_writeRestartFromNoPowerSupply(void);

STD_RETURN_t ReportFile_writeInitDataEnvironmental(ENVIRONMENTAL_InterfaceData_t* interfaceData);
STD_RETURN_t ReportFile_writeFinalDataEnvironmental(ENVIRONMENTAL_FinalData_t* finalData);

STD_RETURN_t ReportFile_writeInitDataDuct(DUCT_InterfaceData_t* interfaceData);
STD_RETURN_t ReportFile_writeFinalDataDuct(DUCT_FinalData_t* finalData);

STD_RETURN_t ReportFile_writeInitDataSrb(SRB_InterfaceData_t* interfaceData);
STD_RETURN_t ReportFile_writeFinalDataSrb(SRB_FinalData_t* finalData);

STD_RETURN_t ReportFile_writeInitDataPm10(PM10_InterfaceData_t* interfaceData);
STD_RETURN_t ReportFile_writeFinalDataPm10(PM10_FinalData_t* finalData);

#endif // REPORT_FILE_H
