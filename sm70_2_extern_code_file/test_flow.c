/*
 * test_flow.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define TEST_FLOW_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"

/* Private define ------------------------------------------------------------*/

/* #define TEST_FLOW_ENABLE_DEBUG_COUNTERS */
#define TEST_FLOW_NORMAL_ENABLE
/* #define TEST_FLOW_FAST_ENABLE */
/* #define TEST_FLOW_LOW_PRIO_ENABLE */
#define TEST_FLOW_THREAD_ENABLE

#define TEST_FLOW_THREAD_SLEEP_MS 50U

#define TEST_FLOW_TEST_TIME_IN_MINUTES 1U
#define TEST_FLOW_PUMP_SET_POINT_PERCENTAGE_INIT_VALUE 100.0f

/* Private typedef -----------------------------------------------------------*/
typedef enum TEST_FLOW_GUI_STATE_e
{
    TEST_FLOW_GUI_STATE_INIT = 0,
    TEST_FLOW_GUI_STATE_READY,
    TEST_FLOW_GUI_STATE_RUNNING,
    /* TEST_FLOW_GUI_STATE_PAUSED, */
    TEST_FLOW_GUI_STATE_PUMP_STOPPING,
    TEST_FLOW_GUI_STATE_STOPPED,
    TEST_FLOW_GUI_STATE_MAX_NUMBER
}TEST_FLOW_GUI_STATE_t;

typedef enum TEST_FLOW_MODEL_STATE_e
{
    TEST_FLOW_MODEL_STATE_INIT = 0,
    TEST_FLOW_MODEL_STATE_READY,
    /* TEST_FLOW_MODEL_STATE_WAIT_START, */
    TEST_FLOW_MODEL_STATE_PUMP_START,
    TEST_FLOW_MODEL_STATE_RUNNING,
    /* TEST_FLOW_MODEL_STATE_PAUSED, */
    TEST_FLOW_MODEL_STATE_PUMP_STOPPING,
    TEST_FLOW_MODEL_STATE_STOPPED,
    TEST_FLOW_MODEL_STATE_ERROR,
    TEST_FLOW_MODEL_STATE_MAX_NUMBER
}TEST_FLOW_MODEL_STATE_t;

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static TEST_FLOW_GUI_STATE_t TestFlow_GuiState;
static TEST_FLOW_MODEL_STATE_t TestFlow_ModelState;
static float TestFlow_actualFlowSum;
static ulong TestFlow_actualFlowCounter;

static const sbyte* TestFlow_guiStateNames[TEST_FLOW_GUI_STATE_MAX_NUMBER] =
{
	/* TEST_FLOW_GUI_STATE_INIT,              */ "Init         ",
	/* TEST_FLOW_GUI_STATE_READY,             */ "Ready        ",
	/* TEST_FLOW_GUI_STATE_RUNNING,           */ "Running      ",
	/* TEST_FLOW_GUI_STATE_PUMP_STOPPING,     */ "Pump Stopping",
	/* TEST_FLOW_GUI_STATE_STOPPED,           */ "Stopped      ",
};

static const sbyte* TestFlow_modelStateNames[TEST_FLOW_MODEL_STATE_MAX_NUMBER] =
{
	/* TEST_FLOW_MODEL_STATE_INIT,            */ "Init         ",
	/* TEST_FLOW_MODEL_STATE_READY,           */ "Ready        ",
	/* TEST_FLOW_MODEL_STATE_PUMP_START,      */ "Pump Starting",
	/* TEST_FLOW_MODEL_STATE_RUNNING,         */ "Running      ",
	/* TEST_FLOW_MODEL_STATE_PUMP_STOP,       */ "Pump Stopping",
	/* TEST_FLOW_MODEL_STATE_STOPPED,         */ "Stopped      ",
	/* TEST_FLOW_MODEL_STATE_ERROR,           */ "Error        ",
};

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t TestFlow_logFlag(FLAG_t flag);
static STD_RETURN_t TestFlow_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t TestFlow_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t TestFlow_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t TestFlow_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t TestFlow_initVariable(void);
#ifdef TEST_FLOW_NORMAL_ENABLE
static STD_RETURN_t TestFlow_manageButtons(ulong execTimeMs);
static STD_RETURN_t TestFlow_manageScreen(ulong execTimeMs);
#endif /* #ifdef TEST_FLOW_NORMAL_ENABLE */
#ifdef TEST_FLOW_THREAD_ENABLE
static STD_RETURN_t TestFlow_modelInit(void);
static STD_RETURN_t TestFlow_model(ulong execTimeMs);
#endif /* #ifdef TEST_FLOW_THREAD_ENABLE */

static STD_RETURN_t TestFlow_showDataOnCharts(void);
static STD_RETURN_t TestFlow_RestoreDataForNewSampling(void);
static STD_RETURN_t TestFlow_sendPumpSetPointPercentage(float percentage);
static STD_RETURN_t TestFlow_modelStartFromReadyOrStopped(void);
static STD_RETURN_t TestFlow_CheckAndManageAutoTerminationCondition(void);
static STD_RETURN_t TestFlow_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError);
static STD_RETURN_t TestFlow_ErrorDetected(SAMPLING_ERROR_t* samplingError);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t TestFlow_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t TestFlow_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t TestFlow_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t TestFlow_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t TestFlow_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t TestFlow_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    TestFlow_GuiState = TEST_FLOW_GUI_STATE_INIT;
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef TEST_FLOW_NORMAL_ENABLE
static STD_RETURN_t TestFlow_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(test_flowScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        test_flowScreen_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestFlow_logFlag(FLAG_TEST_FLOW_BUTTON_BACK);
    }
    if(testFlowScreen_startButton_released != false) /* These flag is reset by GUI */
    {
    	testFlowScreen_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestFlow_logFlag(FLAG_TEST_FLOW_BUTTON_START);
    }
    if(testFlowScreen_stopButton_released != false) /* These flag is reset by GUI */
    {
    	testFlowScreen_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestFlow_logFlag(FLAG_TEST_FLOW_BUTTON_STOP);
    }
    if(testFlowScreen_setPointPumpPercentage_eventAccepted != false)
    {
        testFlowScreen_setPointPumpPercentage_eventAccepted = false;
        FLAG_Set(FLAG_TEST_FLOW_SCREEN_SET_POINT_PUMP_PERCENTAGE_VALUE_CHANGED);
        BUZM_BUTTON_SOUND();
        (void)TestFlow_logFloatValue("Pump Set Point %", testFlowScreen_setPointPumpPercentage_value);
    }  
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_FLOW_NORMAL_ENABLE */

#ifdef TEST_FLOW_NORMAL_ENABLE
static void TestFlow_manageScreenFieldsEnable(TEST_FLOW_GUI_STATE_t stateOfArrival)
{
    debug_print("TestFlow_manageScreenFieldsEnable %d", stateOfArrival);
    switch(stateOfArrival)
    {
        case TEST_FLOW_GUI_STATE_INIT:
            testFlowScreen_backButton_enable = true;
            testFlowScreen_dataFields_enable = true;
            testFlowScreen_setPointPumpPercentage_backgroundColor = WHITE;
            testFlowScreen_startButton_enabled = true;
            testFlowScreen_stopButton_enabled = false;
            break;
        case TEST_FLOW_GUI_STATE_READY:
            testFlowScreen_backButton_enable = true;
            testFlowScreen_dataFields_enable = true;
            testFlowScreen_setPointPumpPercentage_backgroundColor = WHITE;
            testFlowScreen_startButton_enabled = true;
            testFlowScreen_stopButton_enabled = false;
            break;
        case TEST_FLOW_GUI_STATE_RUNNING:        
            testFlowScreen_backButton_enable = false;
            testFlowScreen_dataFields_enable = false;
            testFlowScreen_setPointPumpPercentage_backgroundColor = DARKGRAY;
            testFlowScreen_startButton_enabled = false;
            testFlowScreen_stopButton_enabled = true;
            break;
        case TEST_FLOW_GUI_STATE_PUMP_STOPPING:
            testFlowScreen_backButton_enable = false;
            testFlowScreen_dataFields_enable = false;
            testFlowScreen_setPointPumpPercentage_backgroundColor = DARKGRAY;
            testFlowScreen_startButton_enabled = false;
            testFlowScreen_stopButton_enabled = false;
            break;
        case TEST_FLOW_GUI_STATE_STOPPED:
            testFlowScreen_backButton_enable = true;
            testFlowScreen_dataFields_enable = true;
            testFlowScreen_setPointPumpPercentage_backgroundColor = WHITE;
            testFlowScreen_startButton_enabled = true;
            testFlowScreen_stopButton_enabled = false;
            break;
    }
}

static STD_RETURN_t TestFlow_showDataOnCharts(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    float flowActual = 0.0f;
    
    if(NETWORK_GetLitersMinuteRead(&flowActual) == STD_RETURN_OK)
    {
        common_samplingFlowActual = FLOAT_TO_WORD(flowActual);
        testFlow_flow_color = WHITE;
    }
    else
    {
        common_samplingFlowActual = FLOAT_TO_WORD(flowActual);
        testFlow_flow_color = RED;
    }
    
    
    if(TestFlow_actualFlowCounter > 0U)
    {
        testFlowScreen_averagePumpFlow_value = FLOAT_TO_WORD(TestFlow_actualFlowSum / ((float)(TestFlow_actualFlowCounter)));
    }   
    else
    {
        testFlowScreen_averagePumpFlow_value = FLOAT_TO_WORD(0.0f);
    }                 
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_showDataOnCharts %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestFlow_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    const HMI_ID_t localHmiId = HMI_ID_TEST_FLOW;
    /* START CODE */
    static TEST_FLOW_GUI_STATE_t TestFlow_LastGuiState = TEST_FLOW_GUI_STATE_INIT;
    
    if(TestFlow_LastGuiState != TestFlow_GuiState)
    {
        debug_print("TFL GUI STATE from %d %s to %d %s", TestFlow_LastGuiState, TestFlow_guiStateNames[TestFlow_LastGuiState], TestFlow_GuiState, TestFlow_guiStateNames[TestFlow_GuiState]);
        TestFlow_LastGuiState = TestFlow_GuiState;
    }
    
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
             /* Code on enter  screen */
            switch(TestFlow_GuiState)
            {
                case TEST_FLOW_GUI_STATE_INIT:
                    TestFlow_manageScreenFieldsEnable(TEST_FLOW_GUI_STATE_INIT);
                    testFlowScreen_setPointPumpPercentage_value = FLOAT_TO_WORD(TEST_FLOW_PUMP_SET_POINT_PERCENTAGE_INIT_VALUE);
                    testFlowScreen_averagePumpFlow_value = FLOAT_TO_WORD(0.0f);
                    common_samplingFlowActual = FLOAT_TO_WORD(0.0f);
                    testFlow_flow_color = WHITE;
                    TestFlow_GuiState = TEST_FLOW_GUI_STATE_READY;
                    TestFlow_manageScreenFieldsEnable(TEST_FLOW_GUI_STATE_READY);
                    FLAG_Set(FLAG_TEST_FLOW_SCREEN_COMMAND_GO_TO_READY); 
                    //debug_print("sizeof(float) %d", sizeof(float));
                    //debug_print("sizeof(double) %d", sizeof(double));
                    break;
                default:
                    break;
            }
        }
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_FLOW_BUTTON_START) != false)
    {
        TestFlow_GuiState = TEST_FLOW_GUI_STATE_RUNNING;
        TestFlow_manageScreenFieldsEnable(TEST_FLOW_GUI_STATE_RUNNING);
        FLAG_Set(FLAG_TEST_FLOW_SCREEN_COMMAND_START);
        testFlow_flow_color = WHITE;
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_FLOW_BUTTON_STOP) != false)
    {
        TestFlow_GuiState = TEST_FLOW_GUI_STATE_PUMP_STOPPING;
        TestFlow_manageScreenFieldsEnable(TEST_FLOW_GUI_STATE_PUMP_STOPPING);
        FLAG_Set(FLAG_TEST_FLOW_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_FLOW_BUTTON_BACK) != false)
    {
        TestFlow_GuiState = TEST_FLOW_GUI_STATE_INIT;
        TestFlow_manageScreenFieldsEnable(TEST_FLOW_GUI_STATE_INIT);
        FLAG_Set(FLAG_TEST_FLOW_SCREEN_COMMAND_BACK); 
        (void)HMI_ChangeHmi(HMI_ID_TEST, HMI_GetHmiIdOnScreen()); 
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_EVENT_PUMP_STOPPED) != false)
    {
        TestFlow_GuiState = TEST_FLOW_GUI_STATE_STOPPED;
        TestFlow_manageScreenFieldsEnable(TEST_FLOW_GUI_STATE_STOPPED);
    }
    
    /* Test stopped due to automatic condition */
    if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED) != false)
    {         
        TestFlow_GuiState = TEST_FLOW_GUI_STATE_PUMP_STOPPING;
        TestFlow_manageScreenFieldsEnable(TEST_FLOW_GUI_STATE_PUMP_STOPPING);
    }
    
    /* Test stopped due to error */
    if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR) != false)
    {
        TestFlow_GuiState = TEST_FLOW_GUI_STATE_INIT;
        TestFlow_manageScreenFieldsEnable(TEST_FLOW_GUI_STATE_INIT);
    }
    
    if(HMI_GetHmiIdOnScreen() == localHmiId)
    {
        Sampling_manageSamplingTimeOnScreen();
        Sampling_manageSamplingRemainingTimeOnScreen();
        TestFlow_showDataOnCharts();
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_SET_POINT_PUMP_PERCENTAGE_VALUE_CHANGED) != false)
    {
        if(WORD_TO_FLOAT(testFlowScreen_setPointPumpPercentage_value) > 0.0f)
        {
            testFlowScreen_startButton_enabled = true;
        }
        else
        {
            testFlowScreen_startButton_enabled = false;
        }
    }
    
    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef TEST_FLOW_THREAD_ENABLE
static STD_RETURN_t TestFlow_RestoreDataForNewSampling(void)
{
    return NETWORK_RestoreDataNewSampling(); /* set to 0 the counters on GR */
}

static STD_RETURN_t TestFlow_sendPumpSetPointPercentage(float percentage)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = NETWORK_SetPumpPercentageSetpoint(percentage);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_sendPumpSetPointPercentage %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestFlow_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    TestFlow_ModelState = TEST_FLOW_MODEL_STATE_INIT;
    TestFlow_actualFlowSum = 0.0f;
    TestFlow_actualFlowCounter = 0U;
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_modelInit %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestFlow_getDataFromView(TEST_FLOW_InterfaceData_t* interfaceData)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    
    if(interfaceData != NULL)
    {
        interfaceData->pumpSetPointPercentage = WORD_TO_FLOAT(testFlowScreen_setPointPumpPercentage_value);
        returnValue = STD_RETURN_OK;
    }
        
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_getDataFromView %d", returnValue);
    }
    return returnValue;
}

#endif /* #ifdef TEST_FLOW_THREAD_ENABLE */

#ifdef TEST_FLOW_THREAD_ENABLE
static STD_RETURN_t TestFlow_modelStartFromReadyOrStopped(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    TEST_FLOW_InterfaceData_t interfaceData;
    
    returnValue = TestFlow_getDataFromView(&interfaceData);
    
    if(returnValue == STD_RETURN_OK)
    {
        Sampling_manageSamplingTimeInit(0U);        
        TestFlow_RestoreDataForNewSampling();
        TestFlow_actualFlowSum = 0.0f;
        TestFlow_actualFlowCounter = 0U;
        
        common_isokineticSampling = SAMPLING_ISOKINETIC_SAMPLING_TIME;
        common_isokineticSamplingValue = TEST_FLOW_TEST_TIME_IN_MINUTES;
        
        if(STD_RETURN_ERROR_PARAM == TestFlow_sendPumpSetPointPercentage(interfaceData.pumpSetPointPercentage))
        {
            MessageScreen_openError(DYNAMIC_STRING_ERROR_IMPOSSIBLE_TO_START_TEST, STD_ERROR_PARAM_ERROR_TEST_FLOW, HMI_GetHmiIdOnScreen());
        }
        else
        {
            TestFlow_ModelState = TEST_FLOW_MODEL_STATE_PUMP_START;
        }
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_modelStartFromReadyOrStopped %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestFlow_ErrorDetected(SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SAMPLING_ERROR_t error = SAMPLING_ERROR_NONE;
    ubyte noComm = NETWORK_IsInNoCommunicationState();
    
    if(samplingError != NULL)
    {
        if((error == SAMPLING_ERROR_NONE) && (noComm != false))
        {
            error = SAMPLING_ERROR_NO_COMMUNICATION;
        }
        if(error == SAMPLING_ERROR_NONE)
        {
            ;
        }
        *samplingError = error;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_ErrorDetected: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t TestFlow_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK; 
    ulong sampleTimeInMinutes = 0U;
    
    if((reached != NULL) && (stopCause != NULL) && (samplingError != NULL))
    {
        returnValue = TestFlow_ErrorDetected(samplingError);
        
        if(returnValue == STD_RETURN_OK)
        {
            if(*samplingError != SAMPLING_ERROR_NONE) /* if no errors continue, otherwise stop */
            {
                *reached = true;
                *stopCause = SAMPLING_STOP_CAUSE_ERROR;
                /* samplingError already filled */
            }
            else
            {
                if(TestFlow_ModelState == TEST_FLOW_MODEL_STATE_RUNNING)
                {
                    sampleTimeInMinutes = Sampling_SampleTimeInMinutes();
                    if(sampleTimeInMinutes >= TEST_FLOW_TEST_TIME_IN_MINUTES)
                    {
                        *reached = true;
                        *stopCause = SAMPLING_STOP_CAUSE_TIME;
                        *samplingError = SAMPLING_ERROR_NONE;
                    }
                    else
                    {
                        *reached = false;
                    }
                }
                else
                {
                    /* not in running */
                    *reached = false;
                }
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_AreSamplingTerminationConditionReached: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t TestFlow_CheckAndManageAutoTerminationCondition(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    ubyte samplingTerminationCondition;
    returnValue = TestFlow_AreSamplingTerminationConditionReached(&samplingTerminationCondition, &Sampling_StopCause, &Sampling_Error);
    if(returnValue == STD_RETURN_OK)
    {
        if(samplingTerminationCondition != false)
        {
            TestFlow_sendPumpSetPointPercentage(0.0f);
            debug_print("TEST_FLOW TERMINATION CONDITION REACHED");

            switch(Sampling_StopCause)
            {
                case SAMPLING_STOP_CAUSE_TIME:
                    BUZM_BUTTON_SOUND(); /* manual sound */
                    MessageScreen_openInfo(DYNAMIC_STRING_INFO_TEST_STOPPED_BY_TIME_REACHED, HMI_GetHmiIdOnScreen());
                    TestFlow_ModelState = TEST_FLOW_MODEL_STATE_PUMP_STOPPING;
                    FLAG_Set(FLAG_TEST_FLOW_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED);
                    break;
                case SAMPLING_STOP_CAUSE_MANUAL:
                    /* manual sound not called, because manual stop is by pressing button */
                    MessageScreen_openWarning(DYNAMIC_STRING_WARNING_TEST_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen());
                    TestFlow_ModelState = TEST_FLOW_MODEL_STATE_PUMP_STOPPING;
                    FLAG_Set(FLAG_TEST_FLOW_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED);
                    break;
                case SAMPLING_STOP_CAUSE_ERROR:
                    BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
                    MessageScreen_openError(DYNAMIC_STRING_ERROR_TEST_BLOCKED_DUE_TO_ERROR, Sampling_Error, HMI_GetHmiIdOnScreen());
                    (void)HMI_ChangeHmi(HMI_ID_MAIN, HMI_GetHmiIdOnScreen());
                    FLAG_Set(FLAG_TEST_FLOW_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR);
                    TestFlow_ModelState = TEST_FLOW_MODEL_STATE_INIT; /* MODEL */
                    break;
                default:
                    break;
            }
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_CheckAndManageAutoTerminationCondition %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestFlow_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte pumpRunning = false;
    ubyte pumpRegulation = false;
    float flowActual = 0.0f;

    /* START CODE */
    static TEST_FLOW_MODEL_STATE_t TestFlow_LastModelState = TEST_FLOW_MODEL_STATE_INIT;
    
    if(TestFlow_ModelState != TestFlow_LastModelState)
    {
        debug_print("TFL MOD STATE from %d %s to %d %s", TestFlow_LastModelState, TestFlow_modelStateNames[TestFlow_LastModelState], TestFlow_ModelState, TestFlow_modelStateNames[TestFlow_ModelState]);
        TestFlow_LastModelState = TestFlow_ModelState;
    }
    
    switch(TestFlow_ModelState)
    {
        case TEST_FLOW_MODEL_STATE_INIT:
            if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_COMMAND_GO_TO_READY)  != false)
            {
                TestFlow_ModelState = TEST_FLOW_MODEL_STATE_READY;
                Sampling_manageSamplingTimeInit(0);
            }
            break;
        case TEST_FLOW_MODEL_STATE_READY:
            if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_COMMAND_START) != false)
            {
                returnValue = TestFlow_modelStartFromReadyOrStopped();
            }
            if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_COMMAND_BACK) != false)
            {
                TestFlow_ModelState = TEST_FLOW_MODEL_STATE_INIT;
                TestFlow_actualFlowCounter = 0U;
                TestFlow_actualFlowSum = 0.0f;
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestFlow_CheckAndManageAutoTerminationCondition();
            }
            break;
        /*case TEST_FLOW_MODEL_STATE_WAIT_START:

            break;*/
        case TEST_FLOW_MODEL_STATE_PUMP_START:
            if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_COMMAND_STOP) != FALSE)
            {
                TestFlow_sendPumpSetPointPercentage(0.0f);
                Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
                Sampling_Error = SAMPLING_ERROR_NONE;
                TestFlow_ModelState = TEST_FLOW_MODEL_STATE_PUMP_STOPPING;
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_TEST_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
            }
            if( 
                (NETWORK_GetPumpRunning(&pumpRunning) == STD_RETURN_OK) &&
                (NETWORK_GetPumpRegulation(&pumpRegulation) == STD_RETURN_OK)
            )
            {
                if((pumpRunning != false) && (pumpRegulation == false))
                {
                    /* pump running and regulated */
                    
                    TestFlow_ModelState = TEST_FLOW_MODEL_STATE_RUNNING;
                }
            }
            
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestFlow_CheckAndManageAutoTerminationCondition();
            }
            break;
        case TEST_FLOW_MODEL_STATE_RUNNING:
            Sampling_manageSamplingTime(); /* manage timer, with true should be called previously */
            
            if(FLAG_GetAndReset(FLAG_TEST_FLOW_MODEL_EVENT_NEW_NETWORK_MESSAGE) != FALSE)
            {
                if(NETWORK_GetLitersMinuteRead(&flowActual) == STD_RETURN_OK)
                {
                    TestFlow_actualFlowSum += flowActual;
                    TestFlow_actualFlowCounter++;
                }
            }
            
            if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_COMMAND_STOP) != FALSE)
            {
                TestFlow_sendPumpSetPointPercentage(0.0f);
                Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
                Sampling_Error = SAMPLING_ERROR_NONE;
                TestFlow_ModelState = TEST_FLOW_MODEL_STATE_PUMP_STOPPING;
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_TEST_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestFlow_CheckAndManageAutoTerminationCondition();
            }
            break;
        /*case TEST_FLOW_MODEL_STATE_PAUSED:

            break;*/
        case TEST_FLOW_MODEL_STATE_PUMP_STOPPING:
            if( 
                (NETWORK_GetPumpRunning(&pumpRunning) == STD_RETURN_OK) &&
                (NETWORK_GetPumpRegulation(&pumpRegulation) == STD_RETURN_OK)
            )
            {
                if((pumpRunning == false) && (pumpRegulation == false))
                {
                    /* pump stopped and regulated */
                    TestFlow_ModelState = TEST_FLOW_MODEL_STATE_STOPPED;
                    FLAG_Set(FLAG_TEST_FLOW_SCREEN_EVENT_PUMP_STOPPED);
                }
            }
            
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestFlow_CheckAndManageAutoTerminationCondition();
            }
            break;
        case TEST_FLOW_MODEL_STATE_STOPPED:
            if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_COMMAND_BACK) != false)
            {
                TestFlow_ModelState = TEST_FLOW_MODEL_STATE_INIT;
                TestFlow_actualFlowCounter = 0U;
                TestFlow_actualFlowSum = 0.0f;
            }
            if(FLAG_GetAndReset(FLAG_TEST_FLOW_SCREEN_COMMAND_START) != false)
            {
                returnValue = TestFlow_modelStartFromReadyOrStopped();
            }
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestFlow_CheckAndManageAutoTerminationCondition();
            }
            break;
        case TEST_FLOW_MODEL_STATE_ERROR:

            break;
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_FLOW_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void test_flow_init()
{
    (void)TestFlow_initVariable();
}

#ifdef TEST_FLOW_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void test_flow()
{
#ifdef TEST_FLOW_ENABLE_DEBUG_COUNTERS
    TestFlow_NormalRoutine++;
#endif
    (void)TestFlow_manageButtons(Execution_Normal);
    (void)TestFlow_manageScreen(Execution_Normal);
}
#endif /* #ifdef TEST_FLOW_NORMAL_ENABLE */

#ifdef TEST_FLOW_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void test_flow_fast()
{
#ifdef TEST_FLOW_ENABLE_DEBUG_COUNTERS
    TestFlow_FastRoutine++;
#endif
}
#endif /* #ifdef TEST_FLOW_FAST_ENABLE */

#ifdef TEST_FLOW_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void test_flow_low_priority()
{
#ifdef TEST_FLOW_ENABLE_DEBUG_COUNTERS
    TestFlow_LowPrioRoutine++;
#endif
}
#endif /* #ifdef TEST_FLOW_LOW_PRIO_ENABLE */

#ifdef TEST_FLOW_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void test_flow_thread()
{
    ulong execTime = TEST_FLOW_THREAD_SLEEP_MS;
    
    (void)TestFlow_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef TEST_FLOW_ENABLE_DEBUG_COUNTERS
        TestFlow_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)TestFlow_model(execTime);
    }
}
#endif /* #ifdef TEST_FLOW_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void test_flow_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void test_flow_shutdown()
//{
//}
