/*
 * memory.c
 *
 *  Created on: 06/11/2019
 *      Author: Andrea Tonello
 */
#define MEMORY_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "message_screen.h"
#include "logger.h"

/* Private typedef -----------------------------------------------------------*/
typedef enum MEMORY_FILTER_REPORT_e
{
	MEMORY_FILTER_REPORT_ALL = 0,
	MEMORY_FILTER_REPORT_ENVIRONMENTAL,
	MEMORY_FILTER_REPORT_PM10,
	MEMORY_FILTER_REPORT_DUCT,
	MEMORY_FILTER_REPORT_REMOTE_TSB,
	MEMORY_FILTER_REPORT_REMOTE_RSB,
	MEMORY_FILTER_REPORT_MAX_NUMBER
}MEMORY_FILTER_REPORT_t;

/* Private define ------------------------------------------------------------*/
#define MEMORY_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE 20U

#define MEMORY_FILE_NAME_MAX_NUMBER 5U

#define MEMORY_NO_REPORTS_TEXT_NOT_VISIBLE_TIMER_MS 500U
#define MEMORY_NO_REPORTS_TEXT_VISIBLE_TIMER_MS     1500U

#define MEMORY_FILE_MANIPULATION_TIME_MS 100U
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static Timer_t Memory_TimerNoReportsText;
static ulong Memory_firstPageActive;
static sbyte Memory_pageActivePageNumber[MEMORY_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE] = {0x00U};
static Timer_t Memory_TimerManipulateEachFile;

static slong Memory_FileListId; /* KEEP IT SIGNED */

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Memory_logFlag(FLAG_t flag);
static STD_RETURN_t Memory_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Memory_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Memory_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Memory_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static void Memory_InitVariable(void);
static void Memory_ManageButtons(ulong execTimeMs);
static void Memory_ManageScreen(ulong execTimeMs);
static void Memory_InitPage(void);

static void Memory_FileNameSetForeGround(ulong id, ushort color);
static void Memory_FileNameSetButtonVisible(ulong id, ubyte visible);
static void Memory_FileNameClearText(ulong id);
static void Memory_FileNameSetText(ulong id, sbyte* text, ulong textLength);
static void Memory_DrawData(void);
static void Memory_OpenMemoryReport(ulong buttonId);

static void Memory_model_init(void);
static void Memory_model(ulong execTimeMs);

static ulong Memory_GetIndexByOrder(ulong index, ulong maxSize, ubyte revert);
static void Memory_CreateFileList(void);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Memory_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Memory_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Memory_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Memory_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t Memory_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
	return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static void Memory_InitVariable(void)
{
    Memory_FileListId = -1;
    UTIL_TimerInit(&Memory_TimerNoReportsText);
}

static void Memory_ManageButtons(ulong execTimeMs)
{
    if(memoryScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_BACK);
    }
    if(memoryScreen_eraseAllButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_ERASE_ALL);
    }
    if(memoryScreen_exportAllButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_EXPORT_ALL);
    }
    if(memoryScreen_fileName_00_button_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_00);
    }
    if(memoryScreen_fileName_01_button_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_01);
    }
    if(memoryScreen_fileName_02_button_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_02);
    }
    if(memoryScreen_fileName_03_button_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_03);
    }
    if(memoryScreen_fileName_04_button_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_04);
    }
    if(memoryScreen_filterReport_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_COMBOBOX_FILTER_REPORT);
    }
    if(memoryScreen_previousButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_PREVIOUS);
    }
    if(memoryScreen_nextButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)Memory_logFlag(FLAG_MEMORY_SCREEN_BUTTON_NEXT);
    }
}

static void Memory_FileNameSetForeGround(ulong id, ushort color)
{
    switch(id)
    {
        case 0U:
            memoryScreen_fileName_00_foregroundColor = color;
            break;
        case 1U:
            memoryScreen_fileName_01_foregroundColor = color;
            break;
        case 2U:
            memoryScreen_fileName_02_foregroundColor = color;
            break;
        case 3U:
            memoryScreen_fileName_03_foregroundColor = color;
            break;
        case 4U:
            memoryScreen_fileName_04_foregroundColor = color;
            break;
        default:
            break;
    }
}

static void Memory_FileNameSetButtonVisible(ulong id, ubyte visible)
{
    switch(id)
    {
        case 0U:
            memoryScreen_fileName_00_button_visible = visible;
            break;
        case 1U:
            memoryScreen_fileName_01_button_visible = visible;
            break;
        case 2U:
            memoryScreen_fileName_02_button_visible = visible;
            break;
        case 3U:
            memoryScreen_fileName_03_button_visible = visible;
            break;
        case 4U:
            memoryScreen_fileName_04_button_visible = visible;
            break;
        default:
            break;
    }
}

static void Memory_FileNameClearText(ulong id)
{
    switch(id)
    {
        case 0U:
            memoryScreen_fileName_00_text_1 = '\0';
            break;
        case 1U:
            memoryScreen_fileName_01_text_1 = '\0';
            break;
        case 2U:
            memoryScreen_fileName_02_text_1 = '\0';
            break;
        case 3U:
            memoryScreen_fileName_03_text_1 = '\0';
            break;
        case 4U:
            memoryScreen_fileName_04_text_1 = '\0';
            break;
        default:
            break;
    }
}

static void Memory_FileNameSetText(ulong id, sbyte* text, ulong textLength)
{
    if(text != NULL)
    {
        /* debug_print("TEXT %d=%s", textLength, text); */
        switch(id)
        {
            case 0U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_00_text_1, 40U);
                break;
            case 1U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_01_text_1, 40U);
                break;
            case 2U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_02_text_1, 40U);
                break;
            case 3U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_03_text_1, 40U);
                break;
            case 4U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_04_text_1, 40U);
                break;
            default:
                break;
        }
    }
}

static void Memory_DrawData(void)
{
    slong listSize;
    ulong i;
    ulong lastPageActive;
    
    /* File list created write on display */
    if(Memory_FileListId >= 0)
    {
        /* File list exists write on screen */
        listSize = list_size(Memory_FileListId);
        
        if(listSize > 0U)
        {
            memoryScreen_eraseAllButton_enabled = true;
            memoryScreen_exportAllButton_enabled = true;

            memoryScreen_NoReportsText_visible = false;
            (void) UTIL_TimerStop(&Memory_TimerNoReportsText);
            (void) UTIL_TimerInit(&Memory_TimerNoReportsText);
        }
        else
        {
            memoryScreen_eraseAllButton_enabled = false;
            memoryScreen_exportAllButton_enabled = false;
            
            memoryScreen_NoReportsText_visible = true;
            /* Now set visible */
            (void) UTIL_TimerStart(&Memory_TimerNoReportsText, MEMORY_NO_REPORTS_TEXT_VISIBLE_TIMER_MS);
        }
        
        if(listSize > 0U)
        {
            if(listSize > (Memory_firstPageActive + (MEMORY_FILE_NAME_MAX_NUMBER - 1U)))
            {
                lastPageActive = Memory_firstPageActive + (MEMORY_FILE_NAME_MAX_NUMBER - 1U);
            }
            else
            {
                lastPageActive = listSize;
            }
            string_snprintf(&Memory_pageActivePageNumber[0], sizeof(Memory_pageActivePageNumber), "%03d - %03d / %03d", Memory_firstPageActive, lastPageActive, listSize);
        }
        else
        {
            string_snprintf(&Memory_pageActivePageNumber[0], sizeof(Memory_pageActivePageNumber), "--- - --- / ---");
        }
        UTIL_BufferFromSbyteToUShort(
                                    &Memory_pageActivePageNumber[0],
                                    MEMORY_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE,
                                    &memoryScreen_pageActivePageNumber_1,
                                    MEMORY_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE
        );
        
        /* manage enable of previous and next button */
        if(listSize > 0U)
        {
            if(Memory_firstPageActive > MEMORY_FILE_NAME_MAX_NUMBER)
            {
                memoryScreen_previousButton_enabled = true;
            }
            else
            {
                memoryScreen_previousButton_enabled = false;
            }
            
            if(listSize > (Memory_firstPageActive + (MEMORY_FILE_NAME_MAX_NUMBER - 1U)))
            {
                memoryScreen_nextButton_enabled = true;
            }
            else
            {
                memoryScreen_nextButton_enabled = false;
            }
        }
        else
        {
            memoryScreen_previousButton_enabled = false;
            memoryScreen_nextButton_enabled = false;
        }
        
        i = 0;
        if(listSize > 0U)
        {
            for(; ((i + (Memory_firstPageActive - 1U)) < listSize) && (i < MEMORY_FILE_NAME_MAX_NUMBER); i++)
            {
                sbyte* str = list_item_at(Memory_FileListId, (i + (Memory_firstPageActive - 1U)));
                /* debug_print("str len: %d", string_length(str)); */
                Memory_FileNameSetText(i, str, string_length(str));
                Memory_FileNameSetForeGround(i, WHITE);
                Memory_FileNameSetButtonVisible(i, true);
            }
        }

        /* in case i < MEMORY_FILE_NAME_MAX_NUMBER, set to invisible the other buttons */
        for(; i < MEMORY_FILE_NAME_MAX_NUMBER; i++)
        {
            Memory_FileNameClearText(i);
            Memory_FileNameSetForeGround(i, WHITE);
            Memory_FileNameSetButtonVisible(i, false);
        }
    }
    else
    {
        /* ERROR???? */
        debug_print("List not created but called the event? What the fuck?");
    }
}

static void Memory_OpenMemoryReport(ulong buttonId)
{
    const HMI_ID_t localHmiId = HMI_ID_MEMORY;
    if(buttonId < MEMORY_FILE_NAME_MAX_NUMBER)
    {
        if(Memory_FileListId >= 0)
        {
            MemoryReport_FileNameToOpen = list_item_at(Memory_FileListId, (Memory_firstPageActive - 1U) + buttonId);
            debug_print("Apertura file %s", MemoryReport_FileNameToOpen);
            HMI_ChangeHmi(HMI_ID_MEMORY_REPORT, localHmiId);
        }
        else
        {
            debug_print("La lista non esiste");
        }
    }
    else
    {
        debug_print("button id inesistente");
    }
}    

static void Memory_InitPage(void)
{
    ulong i;
    memoryScreen_filterReport = MEMORY_FILTER_REPORT_ALL;
    memoryScreen_eraseAllButton_enabled = false;
    memoryScreen_exportAllButton_enabled = false;
    for(i = 0; i < MEMORY_FILE_NAME_MAX_NUMBER; i++)
    {
        Memory_FileNameClearText(i);
        Memory_FileNameSetForeGround(i, WHITE);
        Memory_FileNameSetButtonVisible(i, false);
    }
    string_snprintf(&Memory_pageActivePageNumber[0], sizeof(Memory_pageActivePageNumber), "--- - --- / ---");
    UTIL_BufferFromSbyteToUShort(
                                &Memory_pageActivePageNumber[0],
                                MEMORY_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE,
                                &memoryScreen_pageActivePageNumber_1,
                                MEMORY_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE
    );
    
    memoryScreen_previousButton_enabled = false;
    memoryScreen_nextButton_enabled = false;
}
                    
static void Memory_ManageScreen(ulong execTimeMs)
{
    const HMI_ID_t localHmiId = HMI_ID_MEMORY;
    ulong i = 0;
    ubyte isRunning = false;
    ubyte isExpired = false;
    
    (void) UTIL_TimerIncrement(&Memory_TimerNoReportsText, execTimeMs);
    
    (void) UTIL_TimerIsExpired(&Memory_TimerNoReportsText, &isExpired);
    if(isExpired != false)
    {
        (void) UTIL_TimerStop(&Memory_TimerNoReportsText);
        (void) UTIL_TimerInit(&Memory_TimerNoReportsText);
        if(memoryScreen_NoReportsText_visible != false)
        {
            memoryScreen_NoReportsText_visible = false;
            (void) UTIL_TimerStart(&Memory_TimerNoReportsText, MEMORY_NO_REPORTS_TEXT_NOT_VISIBLE_TIMER_MS);
        }
        else
        {
            memoryScreen_NoReportsText_visible = true;
            /* Now set visible */
            (void) UTIL_TimerStart(&Memory_TimerNoReportsText, MEMORY_NO_REPORTS_TEXT_VISIBLE_TIMER_MS);
        }
    }
    
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        /* Code */
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {     
            ; /* MISRA */
        }
        else
        {
            memoryScreen_NoReportsText_visible = false;
            
            switch(Memory_Operation)
            {
                case MEMORY_OPERATION_NO_OPERATION: /* entering from the main menu */
                case MEMORY_OPERATION_ERASE:
                case MEMORY_OPERATION_SINGLE_REPORT_ERASED:
                    Memory_InitPage();
                    FLAG_Set(FLAG_MEMORY_SCREEN_COMMAND_CREATE_FILE_LIST);
                    break;
                case MEMORY_OPERATION_EXPORT:
                    Memory_DrawData();
                    break;
                case MEMORY_OPERATION_SINGLE_REPORT_VIEW:
                    Memory_DrawData();
                    break;
                default:
                    break;
            }
            Memory_Operation = MEMORY_OPERATION_NO_OPERATION;
        }
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_00) != false)
    {
        Memory_OpenMemoryReport(0U);
    }
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_01) != false)
    {
        Memory_OpenMemoryReport(1U);
    }
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_02) != false)
    {
        Memory_OpenMemoryReport(2U);
    }
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_03) != false)
    {
        Memory_OpenMemoryReport(3U);
    }
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_04) != false)
    {
        Memory_OpenMemoryReport(4U);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_COMBOBOX_FILTER_REPORT) != false)
    {
        FLAG_Set(FLAG_MEMORY_SCREEN_COMMAND_CREATE_FILE_LIST);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_EVENT_FILE_LIST_CREATED) != false)
    {
        Memory_firstPageActive = 1;
        
        Memory_DrawData();
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_NEXT) != false)
    {
        Memory_firstPageActive += MEMORY_FILE_NAME_MAX_NUMBER;
        
        Memory_DrawData();
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_PREVIOUS) != false)
    {
        Memory_firstPageActive -= MEMORY_FILE_NAME_MAX_NUMBER;
        
        Memory_DrawData();
    }

    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_EXPORT_ALL) != false)
    {
        Memory_Operation = MEMORY_OPERATION_EXPORT;
        FLAG_Set(FLAG_MEMORY_COMMAND_EXPORT_FILE_LIST);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_ERASE_ALL) != false)
    {
        Memory_Operation = MEMORY_OPERATION_ERASE;
        FLAG_Set(FLAG_MEMORY_COMMAND_ERASE_FILE_LIST);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_BACK) != false)
    {
        debug_print("Event managed: FLAG_MEMORY_SCREEN_BUTTON_BACK");
        (void)HMI_ChangeHmi(HMI_ID_MAIN, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_EVENT_ERASE_ALL) != false)
    {
        
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_EVENT_EXPORT_ALL) != false)
    {
    
    }
    
    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ;
        }
        else
        {
            if(
                (Memory_Operation != MEMORY_OPERATION_EXPORT) &&
                (Memory_Operation != MEMORY_OPERATION_SINGLE_REPORT_VIEW)
            )
            {
                memoryScreen_filterReport = MEMORY_FILTER_REPORT_ALL;
                string_snprintf(&Memory_pageActivePageNumber[0], sizeof(Memory_pageActivePageNumber), "--- - --- / ---");
                UTIL_BufferFromSbyteToUShort(
                                            &Memory_pageActivePageNumber[0],
                                            MEMORY_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE,
                                            &memoryScreen_pageActivePageNumber_1,
                                            MEMORY_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE
                );
            }
            
            memoryScreen_eraseAllButton_enabled = false;
            memoryScreen_exportAllButton_enabled = false;
            for(i = 0; i < MEMORY_FILE_NAME_MAX_NUMBER; i++)
            {
                Memory_FileNameClearText(i);
                Memory_FileNameSetForeGround(i, WHITE);
                Memory_FileNameSetButtonVisible(i, false);
            }
            
            (void) UTIL_TimerStop(&Memory_TimerNoReportsText);
            (void) UTIL_TimerInit(&Memory_TimerNoReportsText);
        }
    }
}

static void Memory_model_init(void)
{
    Memory_Operation = MEMORY_OPERATION_NO_OPERATION;
    UTIL_TimerInit(&Memory_TimerManipulateEachFile);
}

static void Memory_model(ulong execTimeMs)
{
    const HMI_ID_t localHmiId = HMI_ID_MEMORY;
    ubyte startTimer = false;
    ubyte isExpired;
    slong listSize;
    sbyte* fileNamePtr = NULL;
    static ulong fileIndex = 0;
    sbyte tempBuffer[200U];
    
    UTIL_TimerIncrement(&Memory_TimerManipulateEachFile, execTimeMs);
    
    if(FLAG_GetAndReset(FLAG_MEMORY_COMMAND_EXPORT_FILE_LIST) != false)
    {
        debug_print("Memory_model FLAG_MEMORY_COMMAND_EXPORT_FILE_LIST");
        fileIndex = 0;
        //MessageScreen_openBar("Exporting","Initializing...", HMI_MY_ID);
        MessageScreen_openBarWithString(DYNAMIC_STRING_VARIOUS_EXPORTING, DYNAMIC_STRING_VARIOUS_INITIALIZING, localHmiId);
        (void)UTIL_TimerStart(&Memory_TimerManipulateEachFile, MEMORY_FILE_MANIPULATION_TIME_MS);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_COMMAND_ERASE_FILE_LIST) != false)
    {
        debug_print("Memory_model FLAG_MEMORY_COMMAND_ERASE_FILE_LIST");
        fileIndex = 0;
        //MessageScreen_openBar("Erasing","Initializing...", HMI_MY_ID);
        MessageScreen_openBarWithString(DYNAMIC_STRING_VARIOUS_ERASING, DYNAMIC_STRING_VARIOUS_INITIALIZING, localHmiId);
        (void)UTIL_TimerStart(&Memory_TimerManipulateEachFile, MEMORY_FILE_MANIPULATION_TIME_MS);
    }
    
    (void)UTIL_TimerIsExpired(&Memory_TimerManipulateEachFile, &isExpired);
    if(isExpired != false)
    {
        (void)UTIL_TimerStop(&Memory_TimerManipulateEachFile);
        (void)UTIL_TimerInit(&Memory_TimerManipulateEachFile);
        
        if(Memory_FileListId < 0U)
        {
            MessageScreen_openError(DYNAMIC_STRING_ERROR_MEMORY_ERROR_OPERATION, STD_ERROR_MEMORY_MODEL_OPERATION, localHmiId);
        }
        else
        {
            listSize = list_size(Memory_FileListId);
            if(listSize > 0U)
            {
                MessageScreen_setBarValue((((float)fileIndex) * 100.0f) / ((float)listSize));
                if(fileIndex < listSize)
                {
                    fileNamePtr = list_item_at(Memory_FileListId, fileIndex);
                    if(fileNamePtr != NULL)
                    {
                        ubyte tempString[32U] = {0x00U};
                        memory_set(&tempString[0], 0x00, sizeof(tempString));
                        memory_set(&tempBuffer[0], 0x00, sizeof(tempBuffer));
                        switch(Memory_Operation)
                        {
                            case MEMORY_OPERATION_ERASE:
                                graphic_get_dynamic_string_text(&tempString[0], (sizeof(tempString) - 1U), DYNAMIC_STRING_VARIOUS, DYNAMIC_STRING_VARIOUS_ERASING_FILE);
                                string_snprintf(&tempBuffer[0], sizeof(tempBuffer), "%s\n%d/%d\n%s", &tempString[0U], (fileIndex + 1U), listSize, fileNamePtr);
                                break;
                            case MEMORY_OPERATION_EXPORT:
                                graphic_get_dynamic_string_text(&tempString[0], (sizeof(tempString) - 1U), DYNAMIC_STRING_VARIOUS, DYNAMIC_STRING_VARIOUS_EXPORTING_FILE);
                                string_snprintf(&tempBuffer[0], sizeof(tempBuffer), "%s\n%d/%d\n%s", &tempString[0U], (fileIndex + 1U), listSize, fileNamePtr);
                                break;
                            default:
                                MessageScreen_openError(DYNAMIC_STRING_ERROR_MEMORY_ERROR_OPERATION, STD_ERROR_MEMORY_MODEL_OPERATION, localHmiId);
                                break;
                        }
                        MessageScreen_setText(&tempBuffer[0]);
                        switch(Memory_Operation)
                        {
                            case MEMORY_OPERATION_ERASE:
                                /* delete this file */
                                if(file_remove(fileNamePtr, ReportFile_reportLocationSave) != 0U)
                                {
                                    debug_print("[ERRO] Error deleting file: %s", fileNamePtr);
                                }
                                else
                                {
                                    debug_print("removed file: %s", fileNamePtr);
                                }
                                break;
                            case MEMORY_OPERATION_EXPORT:
                                /* TODO INSTRUMENT IDS */
                                
                                string_snprintf(&tempBuffer[0U], sizeof(tempBuffer), "SN%06d_%s", Get_InstrumentSerialNumber(), fileNamePtr);
                                /* copy this file */
                                if(file_copy(fileNamePtr, ReportFile_reportLocationSave, &tempBuffer[0U], ReportFile_reportLocationExport) < 0)
                                {
                                    debug_print("[ERRO] Error exporting file: %s", fileNamePtr);
                                }
                                else
                                {
                                    debug_print("exported file: %s with name %s", fileNamePtr, &tempBuffer[0U]);
                                }
                                break;
                            default:
                                break;
                        }
                        fileIndex++;
                        (void)UTIL_TimerStart(&Memory_TimerManipulateEachFile, MEMORY_FILE_MANIPULATION_TIME_MS);
                    }
                    else
                    {
                        MessageScreen_openError(DYNAMIC_STRING_ERROR_MEMORY_ERROR_OPERATION, STD_ERROR_MEMORY_MODEL_OPERATION_FILE_PTR_NULL, localHmiId);
                    }
                }
                else
                {
                    /* fileIndex == listSize */
                    fileIndex = 0;
                    (void)UTIL_TimerStop(&Memory_TimerManipulateEachFile);
                    (void)UTIL_TimerInit(&Memory_TimerManipulateEachFile);

                    switch(Memory_Operation)
                    {
                        case MEMORY_OPERATION_ERASE:
                            debug_print("erase complete");
                            MessageScreen_setTextWithString(DYNAMIC_STRING_VARIOUS_DELETE_COMPLETE);
                            MessageScreen_setOkButtonEnable(true);
                            MessageScreen_SetForceRedraw(true, localHmiId); /* because the delete must force the re-draw*/
                            FLAG_Set(FLAG_MEMORY_EVENT_ERASE_ALL);
                            break;
                        case MEMORY_OPERATION_EXPORT:
                            debug_print("export complete");
                            MessageScreen_setTextWithString(DYNAMIC_STRING_VARIOUS_EXPORT_COMPLETE);
                            MessageScreen_setOkButtonEnable(true);
                            FLAG_Set(FLAG_MEMORY_EVENT_EXPORT_ALL);
                            break;
                        default:
                            MessageScreen_openError(DYNAMIC_STRING_ERROR_MEMORY_ERROR_OPERATION, STD_ERROR_MEMORY_MODEL_OPERATION, localHmiId);
                            break;
                    }
                }
            }
            else
            {
                MessageScreen_openError(DYNAMIC_STRING_ERROR_MEMORY_ERROR_OPERATION, STD_ERROR_MEMORY_MODEL_OPERATION_FILE_LIST_WRONG_DIMENSION, localHmiId);
            }
        }
    }
     
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_COMMAND_CREATE_FILE_LIST) != false)
    {
        Memory_CreateFileList();
        
        FLAG_Set(FLAG_MEMORY_SCREEN_EVENT_FILE_LIST_CREATED);
    }
}

static ulong Memory_GetIndexByOrder(ulong index, ulong maxSize, ubyte revert)
{
    ulong returnValue = index;
    if(index < maxSize)
    {
        if(revert != false)
        {
            returnValue = (maxSize - 1U) - index;
        }
    }
    return returnValue;
}

static void Memory_CreateFileList(void)
{
    ubyte toAdd;
    ulong listId = file_list_get(REPORT_FILE_NAME_EXTENTION, REPORT_FILE_LOCATION_DEFAULT_SAVE);
    /*debug_print("listId: %d", listId);*/
    ulong listSize = list_size(listId);
    ulong listFilteredSize = 0;
    /*debug_print("list size: %d", listSize);*/
    debug_print("Memory_CreateFileList");
    sbyte* filter = NULL;
    /* select the filter */
    if((memoryScreen_filterReport >= 1U) && (memoryScreen_filterReport < (REPORT_NAME_CODE_MAX_NUMBER + 1U)))
    {
        REPORT_NAME_CODE_t reportType = memoryScreen_filterReport - 1U;
        filter = (sbyte*)ReportFile_reportNameCodes[reportType];
        /* debug_print("filter: %s", filter); */
    }
    /* list exists delete for a new use  */
    if(Memory_FileListId >= 0) 
    {
        list_delete(Memory_FileListId);
        debug_print("Memory_FileListId deleted");
        Memory_FileListId = -1;
    }
    /* list doesn't exist create */
    if(Memory_FileListId < 0) 
    {
        Memory_FileListId = list_create();
    }
    /* roll the entire file list and select the file that matches the filter */
    for(int i = 0; i < listSize; i++)
    {
        /* in that case it is a list of strings */
        ulong newIndex = Memory_GetIndexByOrder(i, listSize, true);
        sbyte* str = list_item_at(listId, newIndex);
                    
        if(filter != NULL)
        {                
            if(UTIL_string_contain_regular(str, string_length(str), filter, string_length(filter)) != false)
            {
                toAdd = true; /* filter and match */
            }
            else
            {
                toAdd = false; /* filter and it doesn't match*/
            }
        }
        else
        {
            toAdd = true; /* no filter add all the items */
        }
        
        if(toAdd != false)
        {
            /* debug_print("ADD"); */
            if(Memory_FileListId < 0) /* security check */
            {
                /* Create list if not yet created */
                Memory_FileListId = list_create();
            }
            /* length of string to copy */
            int fileNameLength = string_length(str);
            /* debug_print("fileNameLength %d of string %s", fileNameLength, str); */
            /* alloc the space */
            sbyte* strToAppend = memory_alloc(fileNameLength + 1U);
            /* copy from the entire list to the filtered list */
            memory_copy(strToAppend, str, fileNameLength);
            strToAppend[fileNameLength] = '\0';
            //debug_print("strToAppend: %s", strToAppend);
            ubyte appended = list_append(Memory_FileListId, strToAppend);
            //debug_print("File name #%d: added %d", i, appended);
        }
    }

    list_delete(listId); /* delete entire original file list */

    listFilteredSize = list_size(Memory_FileListId);

    debug_print("Filtered %d entry on %d", listFilteredSize, listSize);
}

/* Public functions ----------------------------------------------------------*/

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void memory_init()
{
    Memory_InitVariable();
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void memory()
{
    Memory_NormalRoutine++;
    Memory_ManageButtons(Execution_Normal);
    Memory_ManageScreen(Execution_Normal);
}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void memory_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void memory_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void memory_thread()
{
    ulong execTime = MEMORY_THREAD_SLEEP_MS;
    
    Memory_model_init();
    
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
        Memory_ThreadRoutine++; /* Only for debug */
        
        Memory_model(execTime);
    }
}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void memory_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void memory_shutdown()
//{
//}
