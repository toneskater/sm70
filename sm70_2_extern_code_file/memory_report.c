/*
 * memory_report.c
 *
 *  Created on: 06/11/2019
 *      Author: Andrea Tonello
 */
#define MEMORY_REPORT_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "message_screen.h"
#include "logger.h"
#include "serial_com.h"
#include "eepm_data.h"

/* Private typedef -----------------------------------------------------------*/
#define HMI_MY_ID HMI_ID_MEMORY_REPORT

typedef enum MEMORY_REPORT_PRINT_STATE_e
{
    MEMORY_REPORT_PRINT_STATE_READY = 0,
    MEMORY_REPORT_PRINT_STATE_SETUP,
    MEMORY_REPORT_PRINT_STATE_PRINTING_WARMUP,
    MEMORY_REPORT_PRINT_STATE_PRINTING,
    MEMORY_REPORT_PRINT_STATE_PRINTING_CLOSING,
    MEMORY_REPORT_PRINT_STATE_PRINT_FINISHED,
    MEMORY_REPORT_PRINT_STATE_ERROR,
    MEMORY_REPORT_PRINT_STATE_MAX_NUMBER
}MEMORY_REPORT_PRINT_STATE_t;

typedef enum MEMORY_REPORT_SERIAL_EXPORT_STATE_e
{
    MEMORY_REPORT_SERIAL_EXPORT_STATE_READY = 0,
    MEMORY_REPORT_SERIAL_EXPORT_STATE_EXPORTING,
    MEMORY_REPORT_SERIAL_EXPORT_STATE_EXPORT_FINISHED,
    MEMORY_REPORT_SERIAL_EXPORT_STATE_ERROR,
    MEMORY_REPORT_SERIAL_EXPORT_STATE_MAX_NUMBER
}MEMORY_REPORT_SERIAL_EXPORT_STATE_t;

typedef enum MEMORY_REPORT_PRINT_ERROR_e
{
    MEMORY_REPORT_PRINT_ERROR_NO_ERROR = 0,
    MEMORY_REPORT_PRINT_ERROR_PAPER_OUT,
    MEMORY_REPORT_PRINT_ERROR_MAX_NUMBER,
}MEMORY_REPORT_PRINT_ERROR_t;

/* Private define ------------------------------------------------------------*/
#define MEMORY_REPORT_TEXT_CHARS_MAX 1600U
#define MEMORY_REPORT_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE 20U

//#define MEMORY_REPORT_PRINT_ON_SERIAL_TERMINAL

#define MEMORY_REPORT_PRINT_MAX_TRY_COMMAND  10U
#define MEMORY_REPORT_PRINT_MAX_TIMEOUT      10U
#define MEMORY_REPORT_PRINT_TIMEOUT_MS       500U

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static slong MemoryReport_printSerialComDefault = MEMORY_REPORT_PRINT_SERIAL_PORT;
static slong MemoryReport_serialExportComDefault = MEMORY_REPORT_EXPORT_SERIAL_PORT;

static Timer_t MemoryReport_printTimerWarmUp;
static sbyte MemoryReport_Buffer[1024U];
static ulong MemoryReport_StartLine;

static ulong MemoryReport_LineOnScreen = 20;
static ulong MemoryReport_DeltaLineFast = 10;
static ulong MemoryReport_TotalLine;

static ubyte MemoryReport_printStartCommand = false;
static MEMORY_REPORT_PRINT_STATE_t MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_READY;


static const ubyte MemoryReport_PrintSetupMessage[3] = {0x1D, 0x42, 0x03};
static const ubyte MemoryReport_PrintStatusMessage[2] = {0x1B, 0x76};
static const ubyte MemoryReport_Print3LineEmptyMessage[3] = {'\n', '\n', '\n'};
static ubyte MemoryReport_printSerialComStatus;

static ubyte MemoryReport_serialExportStartCommand = false;
static MEMORY_REPORT_SERIAL_EXPORT_STATE_t MemoryReport_serialExportState = MEMORY_REPORT_PRINT_STATE_READY;


static sbyte MemoryReport_MemoryScreen_pageActivePageNumber[MEMORY_REPORT_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE] = {0x00U};

static SERIAL_COM_t serialComExport;
static ubyte MemoryReport_SerialExportTxBuffer[256];

static SERIAL_COM_t serialComPrint;
static ubyte MemoryReport_SerialPrintTxBuffer[256];
static ubyte MemoryReport_SerialPrintRxBuffer[256];
/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t MemoryReport_logFlag(FLAG_t flag);
static STD_RETURN_t MemoryReport_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t MemoryReport_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t MemoryReport_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t MemoryReport_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static void MemoryReport_model_init(void);
static void MemoryReport_model(ulong execTimeMs);

static void MemoryReport_InitVariable(void);
static void MemoryReport_ManageButtons(ulong execTimeMs);
static void MemoryReport_DrawReportLine(ulong startLine, ulong numerOfLine, ulong numberOfLineMax);
static void MemoryReport_UpDownButtons(void);
static void MemoryReport_ManageScreen(ulong execTimeMs);
static STD_RETURN_t MemoryReport_openReport(void);
static ulong MemoryReport_printStartFct(void);
static STD_RETURN_t MemoryReport_printStateMachine(ulong execTimeMs, MEMORY_REPORT_PRINT_STATE_t* printState, MEMORY_REPORT_PRINT_ERROR_t* printError);
static ulong MemoryReport_serialExportStartFct(void);
static STD_RETURN_t MemoryReport_serialExportStateMachine(ulong execTimeMs, MEMORY_REPORT_SERIAL_EXPORT_STATE_t* exportState);

static SERIAL_COM_RETURN_t MemoryReport_SerialPrintRxCallbackFct(ubyte* txMessage, ulong txMessageLength);
/* static SERIAL_COM_RETURN_t MemoryReport_SerialPrintTxCallbackFct(ubyte* txMessage, ulong txMessageLength); */
/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t MemoryReport_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t MemoryReport_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t MemoryReport_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t MemoryReport_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t MemoryReport_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static void MemoryReport_InitVariable(void)
{
    MemoryReport_FileNameToOpen = NULL;
    MemoryReport_FileStringList = -1;
    
    SerialCom_init(
        &serialComExport, 
        /* _timeoutResponseMs   */ 500U,
        /* _maxNumberOfTimeout  */ 10U,
        /* _maxTryTxCommand     */ 10U,
        /* _txPrepareFct        */ NULL,
        /* _txCallbackFct       */ NULL,
        /* _rxValidateFct       */ NULL,
        /* _rxCallbackFct       */ NULL,
        /* _messageTxBufferPtr  */ &MemoryReport_SerialExportTxBuffer[0],
        /* _messageTxBufferSize */ sizeof(MemoryReport_SerialExportTxBuffer),
        /* _messageRxBufferPtr  */ NULL,
        /* _messageRxBufferSize */ 0U,
        /* _executionTime       */ Execution_Normal
    );
    
    SerialCom_init(
        &serialComPrint, 
        /* _timeoutResponseMs   */ MEMORY_REPORT_PRINT_TIMEOUT_MS,
        /* _maxNumberOfTimeout  */ MEMORY_REPORT_PRINT_MAX_TIMEOUT,
        /* _maxTryTxCommand     */ MEMORY_REPORT_PRINT_MAX_TRY_COMMAND,
        /* _txPrepareFct        */ NULL,
        /* _txCallbackFct       */ NULL /*MemoryReport_SerialPrintTxCallbackFct*/,
        /* _rxValidateFct       */ NULL,
        /* _rxCallbackFct       */ MemoryReport_SerialPrintRxCallbackFct,
        /* _messageTxBufferPtr  */ &MemoryReport_SerialPrintTxBuffer[0],
        /* _messageTxBufferSize */ sizeof(MemoryReport_SerialExportTxBuffer),
        /* _messageRxBufferPtr  */ &MemoryReport_SerialPrintRxBuffer[0],
        /* _messageRxBufferSize */ sizeof(MemoryReport_SerialPrintRxBuffer),
        /* _executionTime       */ Execution_Normal
    );
}

static void MemoryReport_ManageButtons(ulong execTimeMs)
{
    if(memory_reportScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MemoryReport_logFlag(FLAG_MEMORY_REPORT_SCREEN_BUTTON_BACK);
    }
    if(memory_reportScreen_eraseButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MemoryReport_logFlag(FLAG_MEMORY_REPORT_SCREEN_BUTTON_ERASE);
    }
    if(memory_reportScreen_printButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MemoryReport_logFlag(FLAG_MEMORY_REPORT_SCREEN_BUTTON_PRINT);
    }
    if(memory_reportScreen_serialExportButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MemoryReport_logFlag(FLAG_MEMORY_REPORT_SCREEN_BUTTON_SERIAL_EXPORT);
    }
    if(memory_reportScreen_usbExportButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MemoryReport_logFlag(FLAG_MEMORY_REPORT_SCREEN_BUTTON_USB_EXPORT);
    }
    if(memory_reportScreen_upButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MemoryReport_logFlag(FLAG_MEMORY_REPORT_SCREEN_BUTTON_UP);
    }
    if(memory_reportScreen_downButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MemoryReport_logFlag(FLAG_MEMORY_REPORT_SCREEN_BUTTON_DOWN);
    }
    if(memory_reportScreen_upFastButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MemoryReport_logFlag(FLAG_MEMORY_REPORT_SCREEN_BUTTON_UP_FAST);
    }
    if(memory_reportScreen_downFastButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MemoryReport_logFlag(FLAG_MEMORY_REPORT_SCREEN_BUTTON_DOWN_FAST);
    }
}

static void MemoryReport_DrawReportLine(ulong startLine, ulong numerOfLine, ulong numberOfLineMax)
{
    ulong i;
    ulong j;
    ulong chars;
    sbyte* strPtr = NULL;
    ushort* strScreenPtr = &memory_reportScreen_report_Text_1;
    ulong strScreenPtrIndex = 0;
    /* debug_print("startLine %d - numerOfLine %d - numberOfLineMax %d", startLine, numerOfLine, numberOfLineMax); */
    
    if(MemoryReport_FileStringList >= 0U)
    {
        /* debug_print("****************************************"); */
        memory_set(&memory_reportScreen_report_Text_1, 0x00U, MEMORY_REPORT_TEXT_CHARS_MAX * sizeof(ushort));
        for(i = 0U; (i < numerOfLine) && ((startLine + i) < numberOfLineMax) && (strScreenPtrIndex < MEMORY_REPORT_TEXT_CHARS_MAX); i++)
        {
            strPtr = list_item_at(MemoryReport_FileStringList, (startLine + i));
            /* debug_print("%4d|%s", ((startLine + i) + 1U), strPtr); */
            
            /* string_length will give the chars number without the terminator*/
            j = 0;
            while((j < string_length(strPtr)) && (strScreenPtrIndex < MEMORY_REPORT_TEXT_CHARS_MAX))
            {
                strScreenPtr[strScreenPtrIndex] = strPtr[j];
                strScreenPtrIndex++;
                j++;
            }
            /* add \n */
            if(strScreenPtrIndex < MEMORY_REPORT_TEXT_CHARS_MAX)
            {
                strScreenPtr[strScreenPtrIndex] = '\n';
                strScreenPtrIndex++;
            }
        }
        /* add string terminator */
        if(strScreenPtrIndex < MEMORY_REPORT_TEXT_CHARS_MAX)
        {
            strScreenPtr[strScreenPtrIndex] = '\0';
            strScreenPtrIndex++;
        }
        /* debug_print("strScreenPtrIndex %d", strScreenPtrIndex); */
        
        string_snprintf(&MemoryReport_MemoryScreen_pageActivePageNumber[0], sizeof(MemoryReport_MemoryScreen_pageActivePageNumber), "%4d - %4d / %4d", (startLine + 1U), (startLine + numerOfLine), numberOfLineMax);
        UTIL_BufferFromSbyteToUShort(
                                    &MemoryReport_MemoryScreen_pageActivePageNumber[0],
                                    20U,
                                    &memoryScreen_startLineLineNumber_1,
                                    20U
        );
    }
}

static void MemoryReport_UpDownButtons(void)
{
    if(MemoryReport_StartLine > 0)
    {
        memory_reportScreen_upButton_enable = true;
        memory_reportScreen_upFastButton_enable = true;   
    }
    else
    {
        memory_reportScreen_upButton_enable = false;
        memory_reportScreen_upFastButton_enable = false; 
    }
    
    if((MemoryReport_StartLine + MemoryReport_LineOnScreen) < MemoryReport_TotalLine)
    {
        memory_reportScreen_downButton_enable = true;
        memory_reportScreen_downFastButton_enable = true;   
    }
    else
    {
        memory_reportScreen_downButton_enable = false;
        memory_reportScreen_downFastButton_enable = false; 
    }
}

static void MemoryReport_ManageScreen(ulong execTimeMs)
{
    if(MessageScreen_isEnteredHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(HMI_MY_ID) != false)
        {
            ;
        }
        else
        {
            ubyte eepromUbyteValue = false;
            UTIL_BufferFromSbyteToUShort(
                                            MemoryReport_FileNameToOpen, 
                                            string_length(MemoryReport_FileNameToOpen), 
                                            &memory_reportScreen_reportTitle_Text_1, 
                                            40U
            );
            memory_reportScreen_backButton_enable = true;
            memory_reportScreen_report_Text_1 = '\0';
            memory_reportScreen_upButton_enable = false;
            memory_reportScreen_upFastButton_enable = false;
            memory_reportScreen_downButton_enable = false;
            memory_reportScreen_downFastButton_enable = false;
            memoryScreen_startLineLineNumber_1 = '\0';
            
            if(STD_RETURN_OK != EEPM_GetId(EEPM_ID_SYST_PRINTER_OPTION, &eepromUbyteValue, EEPM_VALUE_TYPE_UBYTE, 1))
            {
                debug_print("ERROR READ EEPM_ID_SYST_PRINTER_OPTION"); 
            }
            memoryReport_print_visible = (eepromUbyteValue != 0x00) ? true : false;
            
            MemoryReport_StartLine = 0;
            
            FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_COMMAND_OPEN_REPORT);
        }
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_EVENT_REPORT_OPENED) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_EVENT_REPORT_OPENED"); */
        if(MemoryReport_FileStringList >= 0)
        {
            MemoryReport_TotalLine = list_size(MemoryReport_FileStringList);
            
            /* debug_print("file string list size: %d", MemoryReport_TotalLine); */
            
            MemoryReport_DrawReportLine(MemoryReport_StartLine, MemoryReport_LineOnScreen, MemoryReport_TotalLine);
            
            MemoryReport_UpDownButtons();
        }
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_BACK) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_BUTTON_BACK"); */
        Memory_SetOperation(MEMORY_OPERATION_SINGLE_REPORT_VIEW);
        HMI_ChangeHmi(HMI_ID_MEMORY, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_ERASE) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_BUTTON_ERASE"); */
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_COMMAND_ERASE_SINGLE_REPORT);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_ERASED) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_ERASED"); */
        Memory_SetOperation(MEMORY_OPERATION_SINGLE_REPORT_ERASED);
        HMI_ChangeHmi(HMI_ID_MEMORY, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_PRINT) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_BUTTON_PRINT"); */
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_COMMAND_SINGLE_REPORT_PRINT);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_START_PRINTING) != false)
    {
        memory_reportScreen_backButton_enable = false;
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_START_PRINTING"); */
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_START_SERIAL_EXPORTING) != false)
    {
        memory_reportScreen_backButton_enable = false;
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_START_SERIAL_EXPORTING"); */
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_PRINTED) != false)
    {
        memory_reportScreen_backButton_enable = true;
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_PRINTED"); */
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_PRINTED_ERROR) != false)
    {
        memory_reportScreen_backButton_enable = true;
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_PRINTED_ERROR"); */
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_SERIAL_EXPORT) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_BUTTON_SERIAL_EXPORT"); */
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_COMMAND_SINGLE_REPORT_SERIAL_EXPORT);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_SERIAL_EXPORTED) != false)
    {
        memory_reportScreen_backButton_enable = true;
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_SERIAL_EXPORTED"); */
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_SERIAL_EXPORTED_ERROR) != false)
    {
        memory_reportScreen_backButton_enable = true;
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_SERIAL_EXPORTED_ERROR"); */
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_USB_EXPORT) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_BUTTON_USB_EXPORT"); */
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_COMMAND_SINGLE_REPORT_USB_EXPORT);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_USB_EXPORTED) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_USB_EXPORTED"); */
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_UP) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_BUTTON_UP"); */
        
        if(MemoryReport_StartLine > 0U)
        {
            MemoryReport_StartLine -= 1U;
        }
        
        MemoryReport_DrawReportLine(MemoryReport_StartLine, MemoryReport_LineOnScreen, MemoryReport_TotalLine);
        
        MemoryReport_UpDownButtons();
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_UP_FAST) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_BUTTON_UP_FAST"); */
        
        ulong deltaReal = MemoryReport_DeltaLineFast;
        if(MemoryReport_StartLine < deltaReal)
        {
            deltaReal = MemoryReport_StartLine;
        }
        MemoryReport_StartLine -= deltaReal;
        
        MemoryReport_DrawReportLine(MemoryReport_StartLine, MemoryReport_LineOnScreen, MemoryReport_TotalLine);
        
        MemoryReport_UpDownButtons();
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_DOWN) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_BUTTON_DOWN"); */
        
        if((MemoryReport_StartLine + MemoryReport_LineOnScreen) < MemoryReport_TotalLine)
        {
            MemoryReport_StartLine += 1U;
        }
        
        MemoryReport_DrawReportLine(MemoryReport_StartLine, MemoryReport_LineOnScreen, MemoryReport_TotalLine);
        
        MemoryReport_UpDownButtons();
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_DOWN_FAST) != false)
    {
        /* debug_print("MemoryReport_ManageScreen: FLAG_MEMORY_REPORT_SCREEN_BUTTON_DOWN"); */
        
        if((MemoryReport_StartLine + MemoryReport_LineOnScreen) < MemoryReport_TotalLine)
        {
            ulong deltaReal = MemoryReport_DeltaLineFast;
            if((MemoryReport_StartLine + deltaReal + MemoryReport_LineOnScreen) >= MemoryReport_TotalLine)
            {
                deltaReal = (MemoryReport_TotalLine - MemoryReport_StartLine) - MemoryReport_LineOnScreen;
            }
            MemoryReport_StartLine += deltaReal;
        }
        
        MemoryReport_DrawReportLine(MemoryReport_StartLine, MemoryReport_LineOnScreen, MemoryReport_TotalLine);
        
        MemoryReport_UpDownButtons();
    }
    
    if(MessageScreen_isExitedHmi(HMI_MY_ID) != false)
    {        
        if(MessageScreen_isExitedHmiFromPopup(HMI_MY_ID) != false)
        {

        }
        else
        {
            memory_reportScreen_reportTitle_Text_1 = '\0';
            memory_reportScreen_report_Text_1 = '\0';
            memoryScreen_startLineLineNumber_1 = '\0';
            memory_reportScreen_upButton_enable = false;
            memory_reportScreen_upFastButton_enable = false;
            memory_reportScreen_downButton_enable = false;
            memory_reportScreen_downFastButton_enable = false;
            
            MemoryReport_FileNameToOpen = NULL;
        }
    }
}


static void MemoryReport_model_init(void)
{
    UTIL_TimerInit(&MemoryReport_printTimerWarmUp);
}

static STD_RETURN_t MemoryReport_openReport(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int returnChar;
    int line = 0;
    sbyte* strPtrToList = NULL;
    int appended;
   
    int fd = file_open(&MemoryReport_FileNameToOpen[0], ReportFile_reportLocationSave);
    
    if(fd < 0)
    {
        returnValue = STD_RETURN_ERROR_IMPOSSIBLE_TO_OPER_REPORT;
    }
    else
    {
        /* delete list if for some reason it exists */
        if(MemoryReport_FileStringList >= 0) 
        {
            list_delete(MemoryReport_FileStringList);
            /* debug_print("MemoryReport_openReport delete list MemoryReport_FileStringList"); */
            MemoryReport_FileStringList = -1;
        }
        /* create list */
        MemoryReport_FileStringList = list_create();
        
        if(MemoryReport_FileStringList >= 0)
        {
            /* read file contentents row by row and create a list with an entry for each row */
            do
            {
                returnChar = file_read_line(fd, MemoryReport_Buffer, sizeof(MemoryReport_Buffer));
                
                /* debug_print("returnChar: %d %d", returnChar, string_length(MemoryReport_Buffer)); */
                
                /* something read, otherwise EOF */
                if(returnChar > 0)
                {
                    strPtrToList = memory_alloc(returnChar); /* this value include the string terminator */
                    
                    if(strPtrToList != NULL)
                    {
                        memory_copy(strPtrToList, &MemoryReport_Buffer[0], returnChar);
                        
                        appended = list_append(MemoryReport_FileStringList, strPtrToList);
                        
                        if(appended != 0U)
                        {
                            /* error appending */
                            returnValue = STD_RETURN_ERROR_IMPOSSIBLE_TO_OPER_REPORT_FILE_APPENDING_ERROR;
                        }
                        
                        line++;
                    }
                    else
                    {
                        /* debug_print("MemoryReport_openReport memory allocation failed"); */
                        returnChar = 0;
                        returnValue = STD_RETURN_ERROR_IMPOSSIBLE_TO_OPER_REPORT_MEMORY_ALLOC_FAILED;
                    }
                }
                else
                {
                    if(returnChar == 0U)
                    {
                        ; /* EOF */
                    }
                    else
                    {
                        returnValue = STD_RETURN_ERROR_IMPOSSIBLE_TO_OPER_REPORT_FILE_READ_LINE_ERROR;
                    }
                }
            }while((returnChar > 0) && (returnValue == STD_RETURN_OK));
            
            if(returnValue == STD_RETURN_OK)
            {
                if(file_close(fd))
                {
                    returnValue = STD_RETURN_ERROR_REPORT_NOT_CLOSED;
                }
                else
                {
                    returnValue = STD_RETURN_OK;
                }
            }
        }
        else
        {
            returnValue = STD_RETURN_ERROR_IMPOSSIBLE_TO_OPER_REPORT_IMPOSSIBLE_TO_CREATE_A_LIST;
        }
    }

    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] MemoryReport_openReport: %d", returnValue);
        file_close(fd);
        list_delete(MemoryReport_FileStringList);
    }
    return returnValue;
}

static ulong MemoryReport_printStartFct(void)
{
    ulong returnValue = false;
    if(MemoryReport_FileStringList >= 0)
    {
        if(MemoryReport_printState == MEMORY_REPORT_PRINT_STATE_READY)
        {
            MemoryReport_printStartCommand = true;
            returnValue = true;
        }
    }
    return returnValue;
}

static SERIAL_COM_RETURN_t MemoryReport_SerialPrintRxCallbackFct(ubyte* txMessage, ulong txMessageLength)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if(txMessageLength >= 1U)
    {
        MemoryReport_printSerialComStatus = txMessage[0U];
    }
    return returnValue;
}

/*
static SERIAL_COM_RETURN_t MemoryReport_SerialPrintTxCallbackFct(ubyte* txMessage, ulong txMessageLength)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    ubyte temp[512] = {0x00U};
    ulong i = 0;
    ulong index = 0;
    index += string_snprintf(&temp[index], sizeof(temp), "%d - ", txMessageLength);
    while(i < txMessageLength)
    {
        index += string_snprintf(&temp[index], sizeof(temp) - index, "%02X ", txMessage[i]);
        i++;
    }
    debug_print("%s", temp);
    
    return returnValue;
}
*/

static STD_RETURN_t MemoryReport_printStateMachine(ulong execTimeMs, MEMORY_REPORT_PRINT_STATE_t* printState, MEMORY_REPORT_PRINT_ERROR_t* printError)
{
    static ubyte getStatus = false;
    static ulong lineMaxNumber = 0;
    static ulong lineIndex = 0;
    ubyte isExpired;
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SERIAL_COM_RETURN_t sendReturn;
    
    (void)UTIL_TimerIncrement(&MemoryReport_printTimerWarmUp, execTimeMs);
     
    if((printState != NULL) && (printError != NULL))
    {
        /* receive check */
        SerialCom_receive(&serialComPrint);
    
        switch(MemoryReport_printState)
        {
            case MEMORY_REPORT_PRINT_STATE_READY:
                if(MemoryReport_printStartCommand != false)
                {
                    MemoryReport_printStartCommand = false;
                    /* open serial port */
                    SERIAL_COM_RETURN_t ret = SerialCom_open(
                                                        &serialComPrint, 
                                                        MemoryReport_printSerialComDefault,
                                                        SERIAL_PORT_BAUDRATE_9600,
                                                        SERIAL_PORT_DATA_BITS_8,
                                                        SERIAL_PORT_PARITY_NONE,
                                                        SERIAL_PORT_STOP_BITS_1
                    );
                    debug_print("SerialCom_open = %d", ret);
                    if(ret != SERIAL_COM_RETURN_OK)
                    {
                        MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_ERROR;
                    }
                    else
                    {
                        MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_SETUP;
                        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_START_PRINTING);
                        MessageScreen_openBarWithString(DYNAMIC_STRING_VARIOUS_PRINT_SETUP, DYNAMIC_STRING_VARIOUS_EMPTY, HMI_MY_ID);
                        //MessageScreen_openBar("Print setup", "", HMI_MY_ID);
                    }
                }
                break;
            case MEMORY_REPORT_PRINT_STATE_SETUP:
                /* debug_print("MemoryReport_printStateMachine: MEMORY_REPORT_PRINT_STATE_SETUP"); */
                sendReturn = SerialCom_sendMessage(&serialComPrint, (ubyte*)&MemoryReport_PrintSetupMessage[0], sizeof(MemoryReport_PrintSetupMessage), SERIAL_COM_NO_RESPONSE);
                
                switch(sendReturn)
                {
                    case SERIAL_COM_RETURN_OK:
                        getStatus = false;
                        MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_PRINTING_WARMUP;
                        MessageScreen_setTitleWithString(DYNAMIC_STRING_VARIOUS_PRINT_WARM_UP);
                        //MessageScreen_setTitle("Print warm-up");
                        lineMaxNumber = list_size(MemoryReport_FileStringList);
                        lineIndex = 0;
                        /* debug_print("MemoryReport_printStateMachine: ready to print %d lines", lineMaxNumber); */
                        UTIL_TimerStart(&MemoryReport_printTimerWarmUp, 200U);
                        break;
                    case SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY:
                        MessageScreen_openError(DYNAMIC_STRING_ERROR_TX_TOO_BUSY, STD_ERROR_TX_TOO_BUSY, HMI_MY_ID);
                        MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_ERROR;
                        break;
                    default:
                        break;
                }
                break;
            case MEMORY_REPORT_PRINT_STATE_PRINTING_WARMUP:
                (void)UTIL_TimerIsExpired(&MemoryReport_printTimerWarmUp, &isExpired);
                if(isExpired != false)
                {
                    UTIL_TimerStop(&MemoryReport_printTimerWarmUp);
                    UTIL_TimerInit(&MemoryReport_printTimerWarmUp);
                    MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_PRINTING;
                    MessageScreen_setTitleWithString(DYNAMIC_STRING_VARIOUS_PRINTING_REPORT);
                    //MessageScreen_setTitle("Printing report");
                }
                break;
            case MEMORY_REPORT_PRINT_STATE_PRINTING:
#ifndef MEMORY_REPORT_PRINT_ON_SERIAL_TERMINAL
                if(getStatus != false)
                {
                    /* send command to get status */
                    sendReturn = SerialCom_sendMessage(&serialComPrint, (ubyte*)&MemoryReport_PrintStatusMessage[0], sizeof(MemoryReport_PrintStatusMessage), 1U);
                    
                    switch(sendReturn)
                    {
                        case SERIAL_COM_RETURN_OK:
                            if((MemoryReport_printSerialComStatus & 0x10) != 0x00)
                            {
                                /* Action in progress, stay in this state */
                            }
                            else
                            {
                                /* Printer ready */
                                getStatus = false;
                                lineIndex++;
                                if(lineIndex >= lineMaxNumber)
                                {
                                    MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_PRINTING_CLOSING;
                                }
                            }
                            break;
                        case SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY:
                            MessageScreen_openError(DYNAMIC_STRING_ERROR_TX_TOO_BUSY, STD_ERROR_TX_TOO_BUSY, HMI_MY_ID);
                            MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_ERROR;
                            break;
                        case SERIAL_COM_RETURN_TIMEOUT:
                            MessageScreen_openError(DYNAMIC_STRING_ERROR_TIMEOUT_PRINTER, STD_ERROR_TIMEOUT_PRINTER, HMI_MY_ID);
                            MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_ERROR;
                            break;
                        default:
                            break;
                    }
                }
                else
#endif /* MEMORY_REPORT_PRINT_ON_SERIAL_TERMINAL */
                {
                    /* send a line to print */
                    sbyte* strPtr;
                    sbyte patternTemp[3];
                    string_snprintf(&patternTemp[0], sizeof(patternTemp), "@%s", ReportFile_reportGetTag(REPORT_TAG_FIVE_MINUTES_DATA));
                    
                    ubyte exit = false;
                    do
                    {
                        strPtr = list_item_at(MemoryReport_FileStringList, lineIndex);
                        if(UTIL_string_contain_regular(strPtr, string_length(strPtr), patternTemp, sizeof(patternTemp)) != false)
                        {
                            lineIndex += 3;
                        }
                        else
                        {
                            exit = true;
                        }
                    }while((lineIndex < lineMaxNumber) && (exit == false));
                    
                    if(lineIndex < lineMaxNumber)
                    {
                        strPtr = list_item_at(MemoryReport_FileStringList, lineIndex);
                        ulong lineChar = string_length(strPtr);                        
                        sbyte bufferTemp[42] = {0x00};
                        int i;
                        for(i = 0; (i < 40U) && (i < lineChar); i++)
                        {
                            bufferTemp[i] = strPtr[i];
                        }
                        if((i == 40U) && (lineChar > 40))
                        {
                            /* signal on printer the problem */
                            bufferTemp[i - 3] = '.';
                            bufferTemp[i - 2] = '.';
                            bufferTemp[i - 1] = '.';
                        }
                        bufferTemp[i++    ] = '\n';
#ifdef MEMORY_REPORT_PRINT_ON_SERIAL_TERMINAL
                        bufferTemp[i++] = '\r';
#endif
                        MessageScreen_setBarValue((((float)lineIndex) / ((float)lineMaxNumber)) * 100.0f);
                        MessageScreen_setText(strPtr);
                        
                        sendReturn = SerialCom_sendMessage(
                                                &serialComPrint, 
                                                (ubyte*)&bufferTemp[0], 
                                                i, 
                                                SERIAL_COM_NO_RESPONSE
                        );
                        
                        switch(sendReturn)
                        {
                            case SERIAL_COM_RETURN_OK:
                                getStatus = true;
#ifdef MEMORY_REPORT_PRINT_ON_SERIAL_TERMINAL
                                lineIndex++;
                                if(lineIndex >= lineMaxNumber)
                                {
                                    MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_PRINTING_CLOSING;
                                }
#else                                
                                MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_PRINTING;
#endif
                                break;
                           case SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY:
                                MessageScreen_openError(DYNAMIC_STRING_ERROR_TX_TOO_BUSY, STD_ERROR_TX_TOO_BUSY, HMI_MY_ID);
                                MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_ERROR;   
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_PRINTING_CLOSING;
                    }
                }
                break;
            case MEMORY_REPORT_PRINT_STATE_PRINTING_CLOSING:
                debug_print("MEMORY_REPORT_PRINT_STATE_PRINTING_CLOSING");
                
                sendReturn = SerialCom_sendMessage(
                                        &serialComPrint, 
                                        (ubyte*)&MemoryReport_Print3LineEmptyMessage[0], 
                                        sizeof(MemoryReport_Print3LineEmptyMessage), 
                                        SERIAL_COM_NO_RESPONSE
                );
                
                switch(sendReturn)
                {
                    case SERIAL_COM_RETURN_OK:
                        MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_PRINT_FINISHED;
                        break;
                   case SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY:
                        MessageScreen_openError(DYNAMIC_STRING_ERROR_TX_TOO_BUSY, STD_ERROR_TX_TOO_BUSY, HMI_MY_ID);
                        MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_ERROR;   
                        break;
                    default:
                        break;
                }
                break;
            case MEMORY_REPORT_PRINT_STATE_PRINT_FINISHED:
                MessageScreen_openInfo(DYNAMIC_STRING_INFO_PRINT_SUCCESSFULLY, HMI_MY_ID);
                debug_print("MemoryReport_printStateMachine: MEMORY_REPORT_PRINT_STATE_PRINT_FINISHED");
                MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_READY;
                SerialCom_close(&serialComPrint);
                break;
            case MEMORY_REPORT_PRINT_STATE_ERROR:
                debug_print("MemoryReport_printStateMachine: MEMORY_REPORT_PRINT_STATE_ERROR");
                MemoryReport_printState = MEMORY_REPORT_PRINT_STATE_READY;
                SerialCom_close(&serialComPrint);
                break;
            default:
                break;
        }
        *printState = MemoryReport_printState;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

static ulong MemoryReport_serialExportStartFct(void)
{
    ulong returnValue = false;
    if(MemoryReport_FileStringList >= 0)
    {
        if(MemoryReport_serialExportState == MEMORY_REPORT_SERIAL_EXPORT_STATE_READY)
        {
            MemoryReport_serialExportStartCommand = true;
            returnValue = true;
        }
    }
    return returnValue;
}

static STD_RETURN_t MemoryReport_serialExportStateMachine(ulong execTimeMs, MEMORY_REPORT_SERIAL_EXPORT_STATE_t* exportState)
{
    static ulong serialExportLineMaxNumber = 0;
    static ulong serialExportLineIndex = 0;
    ubyte isExpired;
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SERIAL_COM_RETURN_t sendReturn;
    sbyte* strPtr = NULL;
    sbyte bufferTemp[200U] = {0x00U};
    if(exportState != NULL)
    {    
        switch(MemoryReport_serialExportState)
        {
            case MEMORY_REPORT_SERIAL_EXPORT_STATE_READY:
                if(MemoryReport_serialExportStartCommand != false)
                {
                    MemoryReport_serialExportStartCommand = false;
                    /* open serial port */
                    SERIAL_COM_RETURN_t ret = SerialCom_open(
                                                        &serialComExport, 
                                                        MemoryReport_serialExportComDefault,
                                                        SERIAL_PORT_BAUDRATE_9600,
                                                        SERIAL_PORT_DATA_BITS_8,
                                                        SERIAL_PORT_PARITY_NONE,
                                                        SERIAL_PORT_STOP_BITS_1
                    );
                    debug_print("SerialCom_open = %d", ret);
                    if(ret != SERIAL_COM_RETURN_OK)
                    {
                        MemoryReport_serialExportState = MEMORY_REPORT_SERIAL_EXPORT_STATE_ERROR;
                    }
                    else
                    {
                        MemoryReport_serialExportState = MEMORY_REPORT_SERIAL_EXPORT_STATE_EXPORTING;
                        serialExportLineMaxNumber = list_size(MemoryReport_FileStringList);
                        serialExportLineIndex = 0;
                        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_START_SERIAL_EXPORTING);
                        MessageScreen_openBarWithString(DYNAMIC_STRING_VARIOUS_EXPORTING_REPORT, DYNAMIC_STRING_VARIOUS_EMPTY, HMI_MY_ID);
                        //MessageScreen_openBar("Exporting report", "", HMI_MY_ID);
                    }
                }
                break;
            case MEMORY_REPORT_SERIAL_EXPORT_STATE_EXPORTING:
                /* send a line to print */
                strPtr = list_item_at(MemoryReport_FileStringList, serialExportLineIndex);
    
                MessageScreen_setBarValue((((float)serialExportLineIndex) / ((float)serialExportLineMaxNumber)) * 100.0f);
                MessageScreen_setText(strPtr);
                
                int length = string_snprintf(&bufferTemp[0], sizeof(bufferTemp), "%s\n\r\0", strPtr);
    
                sendReturn = SerialCom_sendMessage(&serialComExport, (ubyte*)&bufferTemp[0], length, SERIAL_COM_NO_RESPONSE);

                switch(sendReturn)
                {
                    case SERIAL_COM_RETURN_OK:                            
                        serialExportLineIndex++;
                        if(serialExportLineIndex >= serialExportLineMaxNumber)
                        {
                            /* debug_print("MemoryReport_serialExportState = MEMORY_REPORT_SERIAL_EXPORT_STATE_EXPORT_FINISHED;"); */
                            MemoryReport_serialExportState = MEMORY_REPORT_SERIAL_EXPORT_STATE_EXPORT_FINISHED;
                        }
                        break;
                    case SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY:
                        MessageScreen_openError(DYNAMIC_STRING_ERROR_TX_TOO_BUSY, STD_ERROR_TX_TOO_BUSY, HMI_MY_ID);
                        MemoryReport_serialExportState = MEMORY_REPORT_SERIAL_EXPORT_STATE_ERROR;   
                        break;
                    default:
                        MessageScreen_openError(DYNAMIC_STRING_ERROR_UNKNOWN, STD_ERROR_UNKNOWN, HMI_MY_ID);
                        MemoryReport_serialExportState = MEMORY_REPORT_SERIAL_EXPORT_STATE_ERROR; 
                        break;
                }
                break;
            case MEMORY_REPORT_SERIAL_EXPORT_STATE_EXPORT_FINISHED:
                MessageScreen_openInfo(DYNAMIC_STRING_INFO_SERIAL_EXPORT_OK, HMI_MY_ID);
                debug_print("MemoryReport_serialExportStateMachine: MEMORY_REPORT_SERAIL_EXPORT_STATE_EXPORT_FINISHED");
                MemoryReport_serialExportState = MEMORY_REPORT_SERIAL_EXPORT_STATE_READY;
                SerialCom_close(&serialComExport);
                break;
            case MEMORY_REPORT_SERIAL_EXPORT_STATE_ERROR:
            	debug_print("MemoryReport_serialExportStateMachine: MEMORY_REPORT_SERAIL_EXPORT_STATE_EXPORT_FINISHED");
                MemoryReport_serialExportState = MEMORY_REPORT_SERIAL_EXPORT_STATE_READY;
                SerialCom_close(&serialComExport);
                break;
            default:
                break;
        }
        *exportState = MemoryReport_serialExportState;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

static void MemoryReport_model(ulong execTimeMs)
{    
    STD_RETURN_t returnValue = STD_RETURN_OK;
    MEMORY_REPORT_PRINT_STATE_t printReturn;
    MEMORY_REPORT_PRINT_ERROR_t printError;
    MEMORY_REPORT_SERIAL_EXPORT_STATE_t exportReturn;
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_COMMAND_OPEN_REPORT) != false)
    {
        returnValue = MemoryReport_openReport();
        
        if(returnValue == STD_RETURN_OK)
        {
            FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_REPORT_OPENED);
        }
        else
        {
            FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_REPORT_NOT_OPENED);
        }
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_COMMAND_ERASE_SINGLE_REPORT) != false)
    {    
        if(MemoryReport_FileNameToOpen != NULL)
        {
            if(file_remove(&MemoryReport_FileNameToOpen[0U], ReportFile_reportLocationSave) != 0U)
            {
                MessageScreen_openError(DYNAMIC_STRING_ERROR_IMPOSSIBLE_TO_REMOVE_FILE, STD_ERROR_IMPOSSIBLE_TO_REMOVE_FILE, HMI_MY_ID);
                /* debug_print("MemoryReport_model: file not removed"); */
                returnValue = STD_RETURN_ERROR_TO_REMOVE_FILE;
            }
            else
            {
                MessageScreen_openInfo(DYNAMIC_STRING_INFO_FILE_REMOVED, HMI_MY_ID);
                /* debug_print("MemoryReport_model: file removed"); */
                FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_ERASED);
                returnValue = STD_RETURN_OK;
            }
        }
        MemoryReport_FileNameToOpen = NULL;
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_COMMAND_SINGLE_REPORT_PRINT) != false)
    {
        if(MemoryReport_printStartFct() != false)
        {
            /* Print started */
        }
        else
        {
            /* Printer busy */
        }
    }
    
    returnValue = MemoryReport_printStateMachine(execTimeMs, &printReturn, &printError);
    if(returnValue == STD_RETURN_OK)
    {
        switch(printReturn)
        {
            case MEMORY_REPORT_PRINT_STATE_PRINT_FINISHED:
                FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_PRINTED);
                break;
            case MEMORY_REPORT_PRINT_STATE_ERROR:
                debug_print("[ERRO] MemoryReport_model: error print");
                FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_PRINTED_ERROR);
                break;
            default:
                break;
        }
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_COMMAND_SINGLE_REPORT_SERIAL_EXPORT) != false)
    {
        if(MemoryReport_serialExportStartFct() != false)
        {
            /* Print started */
        }
        else
        {
            /* Printer busy */
            MessageScreen_openWarning(DYNAMIC_STRING_WARNING_PRINTER_BUSY, HMI_MY_ID);
        }
    }
    
    returnValue = MemoryReport_serialExportStateMachine(execTimeMs, &exportReturn);
    if(returnValue == STD_RETURN_OK)
    {
        switch(exportReturn)
        {
            case MEMORY_REPORT_SERIAL_EXPORT_STATE_EXPORT_FINISHED:
                debug_print("MEMORY_REPORT_SERIAL_EXPORT_STATE_EXPORT_FINISHED");
                FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_SERIAL_EXPORTED);
                break;
            case MEMORY_REPORT_SERIAL_EXPORT_STATE_ERROR:
                debug_print("[ERRO] MemoryReport_model: error print");
                FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_SERIAL_EXPORTED_ERROR);
                break;
            default:
                break;
        }
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_COMMAND_SINGLE_REPORT_USB_EXPORT) != false)
    {
        if(file_copy((sbyte*)&MemoryReport_FileNameToOpen[0], ClientFile_clientLocationSave, (sbyte*)&MemoryReport_FileNameToOpen[0], ClientFile_clientLocationExport) < 0)
        {
            debug_print("error on copy");
            MessageScreen_openError(DYNAMIC_STRING_ERROR_FILE_COPY_ERROR, STD_ERROR_FILE_COPY_ERROR, HMI_MY_ID);
        }
        else
        {
            MessageScreen_openInfo(DYNAMIC_STRING_INFO_FILE_COPIED, HMI_MY_ID);
            debug_print("file copied");
            FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_EVENT_SINGLE_REPORT_USB_EXPORTED);
        }
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] MemoryReport_model: %d", returnValue);
    }
}

/* Public functions ----------------------------------------------------------*/

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void memory_report_init()
{
    MemoryReport_InitVariable();
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void memory_report()
{
    MemoryReport_NormalRoutine++;
    MemoryReport_ManageButtons(Execution_Normal);
    MemoryReport_ManageScreen(Execution_Normal);
}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void memory_report_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void memory_report_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void memory_report_thread()
{
    ulong execTime = MEMORY_REPORT_THREAD_SLEEP_MS;
    
    MemoryReport_model_init();
    
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
        MemoryReport_ThreadRoutine++; /* Only for debug */
        
        MemoryReport_model(execTime);
    }
}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void memory_report_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void memory_report_shutdown()
//{
//}
