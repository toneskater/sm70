/*
 * dateTime.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define DATETIME_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"

/* Private define ------------------------------------------------------------*/
#define HMI_MY_ID HMI_ID_DATETIME

/* #define DATETIME_ENABLE_DEBUG_COUNTERS */
#define DATETIME_NORMAL_ENABLE
/* #define DATETIME_FAST_ENABLE */
/* #define DATETIME_LOW_PRIO_ENABLE */
/* #define DATETIME_THREAD_ENABLE */

#define DATETIME_THREAD_SLEEP_MS 50U

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t DateTime_logFlag(FLAG_t flag);
static STD_RETURN_t DateTime_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t DateTime_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t DateTime_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t DateTime_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t DateTime_initVariable(void);
#ifdef DATETIME_NORMAL_ENABLE
static STD_RETURN_t DateTime_manageButtons(ulong execTimeMs);
static STD_RETURN_t DateTime_manageScreen(ulong execTimeMs);
#endif /* #ifdef DATETIME_NORMAL_ENABLE */
#ifdef DATETIME_THREAD_ENABLE
static STD_RETURN_t DateTime_modelInit(void);
static STD_RETURN_t DateTime_model(ulong execTimeMs);
#endif /* #ifdef DATETIME_THREAD_ENABLE */

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t DateTime_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t DateTime_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t DateTime_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t DateTime_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t DateTime_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t DateTime_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] DateTime_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef DATETIME_NORMAL_ENABLE
static STD_RETURN_t DateTime_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(dateTime_backButton_released != false) /* These flag is reset by GUI */
    {
        dateTime_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)DateTime_logFlag(FLAG_DATETIME_BUTTON_BACK);
    }
    if(dateTime_setDateButton_released != false) /* These flag is reset by GUI */
    {
        dateTime_setDateButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)DateTime_logFlag(FLAG_DATETIME_BUTTON_SET_DATE);
    }
    if(dateTime_setTimeButton_released != false) /* These flag is reset by GUI */
    {
        dateTime_setTimeButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)DateTime_logFlag(FLAG_DATETIME_BUTTON_SET_TIME);
    }
    
    if(dateTime_dateYear_eventAccepted != false) /* These flag is reset by GUI */
    {
        dateTime_dateYear_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)DateTime_logUnsignedValue("Date Year: ", dateTime_dateYear_value);
        FLAG_Set(FLAG_DATETIME_SCREEN_YEAR_CHANGED);
    }
    if(dateTime_dateMonth_eventAccepted != false) /* These flag is reset by GUI */
    {
        dateTime_dateMonth_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)DateTime_logUnsignedValue("Date Month: ", dateTime_dateMonth_value);
        FLAG_Set(FLAG_DATETIME_SCREEN_MONTH_CHANGED);
    }
    if(dateTime_dateDay_eventAccepted != false) /* These flag is reset by GUI */
    {
        dateTime_dateDay_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)DateTime_logUnsignedValue("Date Day: ", dateTime_dateDay_value);
        FLAG_Set(FLAG_DATETIME_SCREEN_DAY_CHANGED);
    }
    
    if(dateTime_timeHour_eventAccepted != false) /* These flag is reset by GUI */
    {
        dateTime_timeHour_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)DateTime_logUnsignedValue("Time Hour: ", dateTime_timeHour_value);
        FLAG_Set(FLAG_DATETIME_SCREEN_HOUR_CHANGED);
    }
    if(dateTime_timeMinute_eventAccepted != false) /* These flag is reset by GUI */
    {
        dateTime_timeMinute_eventAccepted = false;
        BUZM_BUTTON_SOUND();
        (void)DateTime_logUnsignedValue("Time Minute: ", dateTime_timeMinute_value);
        FLAG_Set(FLAG_DATETIME_SCREEN_MINUTE_CHANGED);
    }
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] DateTime_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef DATETIME_NORMAL_ENABLE */

#ifdef DATETIME_NORMAL_ENABLE
static STD_RETURN_t DateTime_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    const ubyte maxDayMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    /* START CODE */
    if(MessageScreen_isEnteredHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            dateTime_dateYear_value = datarioAnno;
            dateTime_dateMonth_value = datarioMese;
            dateTime_dateDay_value = datarioGiorno;
            dateTime_timeHour_value = datarioOre;
            dateTime_timeMinute_value = datarioMinuti;
            dateTime_dateDay_rangeMax = maxDayMonth[dateTime_dateMonth_value - 1U];
        }
    }
    
    if(FLAG_GetAndReset(FLAG_DATETIME_BUTTON_BACK) != false)
    {
        HMI_ChangeHmi(HMI_ID_SETUP, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_DATETIME_SCREEN_YEAR_CHANGED) != false)
    {
        debug_print("FLAG_DATETIME_SCREEN_YEAR_CHANGED: %d", dateTime_dateYear_value);
    }
    if(FLAG_GetAndReset(FLAG_DATETIME_SCREEN_MONTH_CHANGED) != false)
    {
        debug_print("FLAG_DATETIME_SCREEN_MONTH_CHANGED: %d", dateTime_dateMonth_value);
        
        dateTime_dateDay_rangeMax = maxDayMonth[dateTime_dateMonth_value - 1U];
        if((dateTime_dateYear_value % 4U) == 0U)
        {
            dateTime_dateDay_rangeMax += 1U;
        }
    }
    if(FLAG_GetAndReset(FLAG_DATETIME_SCREEN_DAY_CHANGED) != false)
    {
        debug_print("FLAG_DATETIME_SCREEN_DAY_CHANGED: %d", dateTime_dateDay_value);
    }
    if(FLAG_GetAndReset(FLAG_DATETIME_SCREEN_HOUR_CHANGED) != false)
    {
        debug_print("FLAG_DATETIME_SCREEN_HOUR_CHANGED: %d", dateTime_timeHour_value);
    }
    if(FLAG_GetAndReset(FLAG_DATETIME_SCREEN_MINUTE_CHANGED) != false)
    {
        debug_print("FLAG_DATETIME_SCREEN_MINUTE_CHANGED: %d", dateTime_timeMinute_value);
    }
    
    if(FLAG_GetAndReset(FLAG_DATETIME_BUTTON_SET_DATE) != false)
    {
        debug_print("FLAG_DATETIME_BUTTON_SET_DATE");
        const unsigned short year = dateTime_dateYear_value;
        const unsigned char month = dateTime_dateMonth_value;
        const unsigned char day = dateTime_dateDay_value;
        const unsigned char hour = datarioOre;
        const unsigned char min = datarioMinuti;
        const unsigned char sec = datarioSecondi;
        int error = system_clock_write(&year, &month, &day, &hour, &min, &sec);
        debug_print("system_clock_write %d", error);
    }
    if(FLAG_GetAndReset(FLAG_DATETIME_BUTTON_SET_TIME) != false)
    {
        debug_print("FLAG_DATETIME_BUTTON_SET_TIME");
        const unsigned short year = datarioAnno;
        const unsigned char month = datarioMese;
        const unsigned char day = datarioGiorno;
        const unsigned char hour = dateTime_timeHour_value;
        const unsigned char min = dateTime_timeMinute_value;
        const unsigned char sec = 0;
        int error = system_clock_write(&year, &month, &day, &hour, &min, &sec);
        debug_print("system_clock_write %d", error);
    }
    
    if(MessageScreen_isExitedHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] DateTime_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef DATETIME_THREAD_ENABLE
static STD_RETURN_t DateTime_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] DateTime_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef DATETIME_THREAD_ENABLE */

#ifdef DATETIME_THREAD_ENABLE
static STD_RETURN_t DateTime_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] DateTime_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef DATETIME_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void dateTime_init()
{
    (void)DateTime_initVariable();
}

#ifdef DATETIME_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void dateTime()
{
#ifdef DATETIME_ENABLE_DEBUG_COUNTERS
    DateTime_NormalRoutine++;
#endif
    (void)DateTime_manageButtons(Execution_Normal);
    (void)DateTime_manageScreen(Execution_Normal);
}
#endif /* #ifdef DATETIME_NORMAL_ENABLE */

#ifdef DATETIME_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void dateTime_fast()
{
#ifdef DATETIME_ENABLE_DEBUG_COUNTERS
    DateTime_FastRoutine++;
#endif
}
#endif /* #ifdef DATETIME_FAST_ENABLE */

#ifdef DATETIME_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void dateTime_low_priority()
{
#ifdef DATETIME_ENABLE_DEBUG_COUNTERS
    DateTime_LowPrioRoutine++;
#endif
}
#endif /* #ifdef DATETIME_LOW_PRIO_ENABLE */

#ifdef DATETIME_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void dateTime_thread()
{
    ulong execTime = DATETIME_THREAD_SLEEP_MS;
    
    (void)DateTime_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef DATETIME_ENABLE_DEBUG_COUNTERS
        DateTime_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)DateTime_model(execTime);
    }
}
#endif /* #ifdef DATETIME_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void dateTime_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void dateTime_shutdown()
//{
//}
