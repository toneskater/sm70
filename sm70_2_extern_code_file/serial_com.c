/*
 * serial_com.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "serial_com.h"

/* Private define ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
SERIAL_COM_RETURN_t SerialCom_init(SERIAL_COM_t* serial, ulong _timeoutResponseMs, ulong _maxNumberOfTimeout, ulong _maxTryTxCommand, SerialCom_txPrepareFct _txPrepareFct, SerialCom_txCallbackFct _txCallbackFct, SerialCom_rxValidateFct _rxValidateFct, SerialCom_rxCallbackFct _rxCallbackFct, ubyte* _messageTxBufferPtr, ulong _messageTxBufferSize, ubyte* _messageRxBufferPtr, ulong _messageRxBufferSize, ulong _executionTime)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    
    if(serial != NULL)
    {
        serial->validConfigurationTx = false;
        serial->validConfigurationRx = false;
        
        if((_executionTime > 0U) && (_timeoutResponseMs > 0U) && (_maxNumberOfTimeout > 0U)
        )
        {
            /* configuration */
            serial->idComm = -1; /* !!!!!!!!!SIGNED!!!!!!!*/
            serial->timeoutResponseMs = _timeoutResponseMs;
            serial->maxNumberOfTimeout = _maxNumberOfTimeout;
            serial->maxTryTxCommand = _maxTryTxCommand;
            
            serial->txPrepareFct = _txPrepareFct;
            serial->rxValidateFct = _rxValidateFct;
            serial->rxCallbackFct = _rxCallbackFct;
            serial->txCallbackFct = _txCallbackFct;
            serial->messageTxBufferPtr = _messageTxBufferPtr;
            serial->messageTxBufferSize = _messageTxBufferSize;
            
            serial->messageRxBufferPtr = _messageRxBufferPtr;
            serial->messageRxBufferSize = _messageRxBufferSize;
            
            serial->executionTime = _executionTime; 
            
            /* Internal variables */
            serial->messageRxLength = 0U;
            serial->messageTxLength = 0U;
            serial->messageArrivedFlag = false;
            serial->responseLength = 0U;
            serial->state = SERIAL_COM_STATE_READY;
            serial->retryTx = 0U;
            serial->timeoutNumberCounter = 0U;
            serial->timeoutMsCounter = 0U;
            
            if(
                (serial->messageTxBufferPtr != NULL) && (serial->messageTxBufferSize > 0U)
            )
            {
                debug_print("SERIAL_COM_t 0x%08X valid tx configuration", serial);
                serial->validConfigurationTx = true;
            }
            else
            {
                debug_print("SERIAL_COM_t 0x%08X INVALID tx configuration", serial);
            }
            
            if(
                (serial->timeoutResponseMs > 0U) && (serial->maxNumberOfTimeout > 0U) &&
                (serial->messageRxBufferPtr != NULL) && (serial->messageRxBufferSize > 0U) &&
                (serial->rxCallbackFct != NULL)
            )
            {
                debug_print("SERIAL_COM_t 0x%08X valid rx configuration", serial);
                serial->validConfigurationRx = true;
            }
            else
            {
                debug_print("SERIAL_COM_t 0x%08X INVALID rx configuration", serial);
            }
        }
        else
        {
            debug_print("SERIAL_COM_t 0x%08X INVALID SERIAL_COM_RETURN_ERROR_PARAM", serial);
            returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
        }   
    }
    else
    {
        debug_print("SERIAL_COM_t 0x%08X INVALID SERIAL_COM_RETURN_ERROR_PARAM", serial);
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_open(SERIAL_COM_t* serial, _SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stop)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if((serial != NULL) && (serial->validConfigurationTx != false)) /* at least the possibility to send */
    {
        if(serial->idComm >= 0)
        {
            debug_print("[WARN] SerialCom_Open %d ALREADY OPEN", serial->idComm);
        }
        else
        {
            serial->idComm = serial_open( 
                        port,
                        baudRate,
                        dataBits,
                        parity,
                        stop
            );
        }

        if(serial->idComm < 0)
        {
            debug_print("[WARN] SerialCom_Open %d NOT OPEN", serial->idComm);
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            debug_print("[INFO] SerialCom_Open %d OPEN", serial->idComm);
            returnValue = SERIAL_COM_RETURN_OK;
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_close(SERIAL_COM_t* serial)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if(serial != NULL)
    {
        if(serial->idComm < 0)
        {
            debug_print("[WARN] SerialCom_close %d ALREADY CLOSED", serial->idComm);
        }
        else
        {
            if(serial_close(serial->idComm) < 0)
            {
                debug_print("[WARN] SerialCom_close %d NOT CLOSED", serial->idComm);
            }
            else
            {
                debug_print("[INFO] SerialCom_close %d CLOSED", serial->idComm);
            }
            serial->idComm = -1;
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_receive(SERIAL_COM_t* serial)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    int nBytesReceived = 0U;
    if((serial != NULL) && (serial->validConfigurationRx != false) && (serial->validConfigurationTx != false))
    {
        if(serial->responseLength > 0U)
        {
            nBytesReceived = serial_bytes_available(serial->idComm);
            /* debug_print("serialid %d responseLength %d nBytesReceived %d", serial->idComm, serial->responseLength, nBytesReceived); */
            if(nBytesReceived >= serial->responseLength)
            {
                if(nBytesReceived <= serial->messageRxBufferSize)
                {
                    serial_read(serial->idComm, serial->messageRxBufferPtr, serial->responseLength);
                    serial->messageRxLength = serial->responseLength;
                    serial->responseLength = 0U; /* Nothing to receive */
                    serial->messageArrivedFlag = true; /* message arrived */
                    returnValue = SERIAL_COM_RETURN_OK;
                }
                else
                {
                    returnValue = SERIAL_COM_RETURN_BUFFER_RX_NOT_BIG_ENOUGTH;
                }   
            }
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_sendMessage(SERIAL_COM_t* serial, ubyte* message, ulong messageLength, ubyte _responseLength)
{    
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    ubyte* msgTxPtr = message;
    ulong msgTxLength = messageLength;
    ubyte purgeVariable;
    if(
        (serial != NULL) && (serial->validConfigurationTx != false) && 
        (
            ((_responseLength > 0U) && (serial->validConfigurationRx != false)) ||
            (_responseLength == 0U)
        )
    ) 
    {
        if(serial->idComm < 0)
        {
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            if((messageLength <= serial->messageTxBufferSize) && (_responseLength <= serial->messageRxBufferSize))
            {
                switch(serial->state)
                {
                    case SERIAL_COM_STATE_READY:
                        serial->messageArrivedFlag = false; /* clear the flag before send */
                        if(serial->txPrepareFct != NULL) /* prepare function to be used */
                        {
                            /* prepare function*/
                            returnValue = serial->txPrepareFct(message, messageLength, serial->messageTxBufferPtr, &(serial->messageTxLength), serial->messageTxBufferSize);
                            /* change the message ptr, because in prepare, the message is moved to the tx bufferPtr */
                            msgTxPtr = serial->messageTxBufferPtr;
                            msgTxLength = serial->messageTxLength;
                        }
                        /* if prepare fct is ok, or no prepare function has been called */
                        if(returnValue == SERIAL_COM_RETURN_OK)
                        {
                            if(serial_send(serial->idComm, msgTxPtr, msgTxLength) > 0)
                            {
                                /* message sent */
                                if(serial->txCallbackFct != NULL)
                                {
                                    /* I don't care about this function return */
                                    (void)serial->txCallbackFct(msgTxPtr, msgTxLength);
                                }
                                
                                serial->responseLength = _responseLength;
                                if(serial->responseLength > 0U) /* need to wait a response */
                                {
                                    /* Flag to check present, go to TEST_SEND_MESSAGE_STATE_WAIT_RESPONSE */
                                    serial->state = SERIAL_COM_STATE_WAIT_RESPONSE;
                                    serial->retryTx = 0U;
                                    serial->timeoutMsCounter = 0U;
                                    returnValue = SERIAL_COM_RETURN_BUSY;
                                }
                                else /* no response to wait */
                                {
                                    serial->state = SERIAL_COM_STATE_READY;
                                    serial->retryTx = 0U;
                                    returnValue = SERIAL_COM_RETURN_OK;
                                }
                            }
                            else
                            {
                                /* peripheral tx busy */
                                serial->retryTx++;
                                if(serial->maxTryTxCommand != 0U) /* Check for retryTx*/
                                {
                                    if(serial->retryTx >= serial->maxTryTxCommand)
                                    {
                                        /* Tx threshold reached */
                                        serial->retryTx = 0U;
                                        serial->state = SERIAL_COM_STATE_READY;
                                        returnValue = SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY;
                                    }
                                    else
                                    {
                                        /* Tx threashold not reached, retry */
                                        serial->state = SERIAL_COM_STATE_READY;
                                        returnValue = SERIAL_COM_RETURN_BUSY;
                                    }
                                }
                                else
                                {
                                    /* No check for retry tx */
                                    serial->state = SERIAL_COM_STATE_READY;
                                    returnValue = SERIAL_COM_RETURN_TX_BUSY;
                                }
                            }
                        }
                        break;
                    case SERIAL_COM_STATE_WAIT_RESPONSE:
                        if(serial->timeoutResponseMs > 0U)
                        {   
                            /* There is a timeout */
                            serial->timeoutMsCounter += serial->executionTime;
                            if(serial->timeoutMsCounter < serial->timeoutResponseMs)
                            {
                                /* check for flag */
                                if(serial->messageArrivedFlag != false)
                                {
                                    serial->messageArrivedFlag = false;
                                    
                                    if(serial->rxValidateFct != NULL)
                                    {
                                        returnValue = serial->rxValidateFct(serial->messageRxBufferPtr, &(serial->messageRxLength));
                                    }
                                    
                                    /* after validation is ok, or maybe no validation function has been called */
                                    if(returnValue == SERIAL_COM_RETURN_OK)
                                    {
                                        if(serial->rxCallbackFct != NULL)
                                        {
                                            returnValue = serial->rxCallbackFct(serial->messageRxBufferPtr, serial->messageRxLength);
                                        }
                                    }
                                    serial->state = SERIAL_COM_STATE_READY;                                
                                    serial->timeoutNumberCounter = 0;
                                }
                                else
                                {
                                    /* Nothing, stay here and wait */
                                    returnValue = SERIAL_COM_RETURN_BUSY;
                                }
                            }
                            else
                            {
                                /* time out reached for the response */
                                serial->timeoutNumberCounter++;
                                /* debug_print("timeoutNumberCounter: %d", timeoutNumberCounter); */
                                if(serial->timeoutNumberCounter >= serial->maxNumberOfTimeout)
                                {
                                    serial->responseLength = 0U;
                                    /* Purge rx buffer if there is something */
                                    while(serial_bytes_available(serial->idComm) > 0)
                                    {
                                        serial_read(serial->idComm, &purgeVariable, 1U);
                                    }
                                    returnValue = SERIAL_COM_RETURN_TIMEOUT;
                                    serial->state = SERIAL_COM_STATE_READY;
                                    serial->timeoutNumberCounter = 0;
                                }
                                else
                                {
                                    returnValue = SERIAL_COM_RETURN_BUSY;
                                    serial->state = SERIAL_COM_STATE_READY;
                                }
                            }
                        }
                        else
                        {
                            /* Infinite timeout until flag goes up */
                            /* check for flag */
                            if(serial->messageArrivedFlag != false)
                            {
                                serial->messageArrivedFlag = false;
                                if(serial->rxValidateFct != NULL)
                                {
                                    returnValue = serial->rxValidateFct(serial->messageRxBufferPtr, &(serial->messageRxLength));
                                }
                                
                                /* after validation is ok, or maybe no validation function has been called */
                                if(returnValue == SERIAL_COM_RETURN_OK)
                                {
                                    if(serial->rxCallbackFct != NULL)
                                    {
                                        returnValue = serial->rxCallbackFct(serial->messageRxBufferPtr, serial->messageRxLength);
                                    }
                                }
                                serial->state = SERIAL_COM_STATE_READY;
                            }
                            else
                            {
                                /* Nothing, stay here forever and wait */
                                returnValue = SERIAL_COM_RETURN_BUSY;
                            }
                        }
                        break;
                    default:
                        returnValue = SERIAL_COM_RETURN_ERROR_AUTOMA;
                        serial->state = SERIAL_COM_STATE_READY;
                        break;
                }/* end switch */
            }
            else
            {
                returnValue = SERIAL_COM_RETURN_SERIAL_BUFFER_SIZE_WRONG;
            }              
        }
    }
    else
    {
        debug_print("SerialCom_sendMessage: SERIAL_COM_RETURN_ERROR_PARAM");
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }
    
    switch(returnValue)
    {
        case SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY:
        case SERIAL_COM_RETURN_TX_BUSY:
        case SERIAL_COM_RETURN_TIMEOUT:
        case SERIAL_COM_RETURN_ERROR_AUTOMA:
        case SERIAL_COM_RETURN_PREPARE_ERROR:
        case SERIAL_COM_RETURN_VALIDATION_ERROR:
        case SERIAL_COM_RETURN_VALIDATION_MESSAGE_INVALID:
        case SERIAL_COM_RETURN_CALLBACK_ERROR:
            //debug_print("[ERRO] SerialCom_sendMessage: %d", returnValue);
            break;
        default:
            break;
    }

    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_initXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial, ulong _timeoutResponseMs, ulong _maxNumberOfTimeout, ulong _maxTryTxCommand, SerialCom_txCallbackFct _txCallbackFct, SerialCom_rxCallbackFct _rxCallbackFct, SerialCom_errorCallbackFct _errorCallbackFct, ubyte* _messageTxBufferPtr, ulong _messageTxBufferSize, ubyte* _messageRxBufferPtr, ulong _messageRxBufferSize, ulong _executionTime)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    
    if(serial != NULL)
    {
        serial->validConfigurationTx = false;
        serial->validConfigurationRx = false;
        
        if((_executionTime > 0U) && (_timeoutResponseMs > 0U) && (_maxNumberOfTimeout > 0U)
        )
        {
            /* configuration */
            serial->idComm = -1; /* !!!!!!!!!SIGNED!!!!!!!*/
            serial->timeoutResponseMs = _timeoutResponseMs;
            serial->maxNumberOfTimeout = _maxNumberOfTimeout;
            serial->maxTryTxCommand = _maxTryTxCommand;
            
            serial->rxCallbackFct = _rxCallbackFct;
            serial->txCallbackFct = _txCallbackFct;
            serial->errorCallbackFct = _errorCallbackFct;
            
            serial->messageTxBufferPtr = _messageTxBufferPtr;
            serial->messageTxBufferSize = _messageTxBufferSize;
            
            serial->messageRxBufferPtr = _messageRxBufferPtr;
            serial->messageRxBufferSize = _messageRxBufferSize;
            
            serial->executionTime = _executionTime; 
            
            /* Internal variables */
            serial->messageRxLength = 0U;
            serial->messageTxLength = 0U;
            serial->messageArrivedFlag = false;
            serial->responseToWait = false;
            serial->state = SERIAL_COM_STATE_READY;
            serial->retryTx = 0U;
            serial->timeoutNumberCounter = 0U;
            serial->timeoutMsCounter = 0U;
            serial->lengthFaultCounter = 0U;
            serial->checksumFaultCounter = 0U;
            serial->errorDetected = false;
            serial->errorDetectedLength = 0U;
            serial->messageStarted = false;
            serial->escapeCharacterDetected = false;
            serial->nextByteOfMessage = 0u;
            serial->checksum = 0U;
            
            if(
                (serial->messageTxBufferPtr != NULL) && (serial->messageTxBufferSize > 0U)
            )
            {
                debug_print("SERIAL_COM_XBEE_PROTOCOL_t 0x%08X valid tx configuration", serial);
                serial->validConfigurationTx = true;
            }
            else
            {
                debug_print("SERIAL_COM_XBEE_PROTOCOL_t 0x%08X INVALID tx configuration", serial);
            }
            
            if(
                (serial->timeoutResponseMs > 0U) && (serial->maxNumberOfTimeout > 0U) &&
                (serial->messageRxBufferPtr != NULL) && (serial->messageRxBufferSize > 0U) &&
                (serial->rxCallbackFct != NULL)
            )
            {
                debug_print("SERIAL_COM_XBEE_PROTOCOL_t 0x%08X valid rx configuration", serial);
                serial->validConfigurationRx = true;
            }
            else
            {
                debug_print("SERIAL_COM_XBEE_PROTOCOL_t 0x%08X INVALID rx configuration", serial);
            }
        }
        else
        {
            debug_print("SERIAL_COM_XBEE_PROTOCOL_t 0x%08X INVALID SERIAL_COM_RETURN_ERROR_PARAM", serial);
            returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
        }   
    }
    else
    {
        debug_print("SERIAL_COM_XBEE_PROTOCOL_t 0x%08X INVALID SERIAL_COM_RETURN_ERROR_PARAM", serial);
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_openXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial, _SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stop)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if((serial != NULL) && (serial->validConfigurationTx != false)) /* at least the possibility to send */
    {
        serial->idComm = serial_open( 
                            port,
                            baudRate,
                            dataBits,
                            parity,
                            stop
        );
        if(serial->idComm < 0U)
        {
            debug_print("[WARN] SerialCom_openXbeeProtocol %d NOT OPEN", serial->idComm);
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            debug_print("[INFO] SerialCom_openXbeeProtocol %d OPEN", serial->idComm);
            returnValue = SERIAL_COM_RETURN_OK;
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_closeXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if((serial != NULL) && (serial->idComm >= 0))
    {
        if(serial_close(serial->idComm) < 0)
        {
            debug_print("[WARN] SerialCom_closeXbeeProtocol %d NOT CLOSED", serial->idComm);
        }
        else
        {
            debug_print("[INFO] SerialCom_closeXbeeProtocol %d CLOSED", serial->idComm);
        }
        serial->idComm = -1;
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_receiveXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    ubyte ByteReceived;
    if((serial != NULL) && (serial->validConfigurationRx != false) && (serial->validConfigurationTx != false))
    {
        while(serial_bytes_available(serial->idComm) > 0)
        {
            serial_read(serial->idComm, &ByteReceived, 1U);
            
            if(ByteReceived != XBEM_START_CHARACTER)
            {//Other character
              if(serial->messageStarted != false)
              {
                if(ByteReceived != XBEM_ESCAPE_CHARACTER)
                {
                  if(serial->escapeCharacterDetected != false)
                  {
                    serial->escapeCharacterDetected = false;
                    ByteReceived ^= XBEM_ESCAPE_XOR_CHARACTER;
                  }

                  if(serial->messageRxLength < serial->messageRxBufferSize)
                  {
                      serial->messageRxBufferPtr[serial->messageRxLength++] = ByteReceived;
                      if((serial->messageRxLength > 3U) && (serial->messageRxLength < serial->nextByteOfMessage))
                      {
                        serial->checksum += ByteReceived;
                      }
                  }
                  else
                  {
                    serial->lengthFaultCounter++;
                    serial->errorDetected = true;
                    serial->errorDetectedLength = serial->messageRxLength;
                    serial->messageStarted = false;
                    if(serial->errorCallbackFct != NULL)
                    {
                        serial->errorCallbackFct(true, false);
                    }
                  }
                }
                else
                {
                  serial->escapeCharacterDetected = true;
                }

                if(serial->messageRxLength == 3)
                {
                  serial->nextByteOfMessage = (ushort)(serial->messageRxBufferPtr[2U] + 4U);
                  if(serial->nextByteOfMessage > serial->messageRxBufferSize)
                  {
                    serial->lengthFaultCounter++;
                    serial->errorDetected = true;
                    serial->errorDetectedLength = serial->messageRxLength;
                    serial->messageStarted = false;
                    if(serial->errorCallbackFct != NULL)
                    {
                        serial->errorCallbackFct(true, false);
                    }
                  }
                }
                else if(serial->messageRxLength == serial->nextByteOfMessage)
                {
                  serial->checksum = 0xFF - serial->checksum;
                  if(serial->checksum == serial->messageRxBufferPtr[serial->nextByteOfMessage - 1])
                  {//message ok validate
                    if(serial->rxCallbackFct != NULL)
                    {
                        serial->rxCallbackFct(&serial->messageRxBufferPtr[0U], serial->nextByteOfMessage);
                    } 
                    serial->messageArrivedFlag = true;
                  }
                  else
                  {//message ko
                    serial->checksumFaultCounter++;
                    serial->errorDetected = true;
                    serial->errorDetectedLength = serial->messageRxLength;
                    if(serial->errorCallbackFct != NULL)
                    {
                        serial->errorCallbackFct(false, true);
                    }
                  }
                  serial->messageStarted = false;
                }
              }
            }
            else
            {//Start Characters
              serial->messageStarted = true;
              serial->nextByteOfMessage = 0;
              serial->messageRxBufferPtr[0U] = XBEM_START_CHARACTER;
              serial->messageRxLength = 1;
              serial->checksum = 0;
              serial->errorDetected = false;
            }
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_sendMessageXbeeProtocol(SERIAL_COM_XBEE_PROTOCOL_t* serial, ubyte* message, ulong messageLength, ubyte _responseToWait)
{    
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    ubyte purgeVariable;
    ulong i;
    ubyte checksum;
    if(
        (serial != NULL) && (serial->validConfigurationTx != false) && 
        (
            ((_responseToWait != false) && (serial->validConfigurationRx != false)) ||
            (_responseToWait == false)
        )
    ) 
    {
        if(serial->idComm < 0)
        {
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            if(messageLength <= serial->messageTxBufferSize)
            {
                switch(serial->state)
                {
                    case SERIAL_COM_STATE_READY:
                        serial->messageArrivedFlag = false; /* clear the flag before send */
                        /* START PREPARE MESSAGE TO BE SENT, ADD CHECKSUM and other stuff */
                        i = 0;
                        serial->messageTxLength = 0;
                        while((i < messageLength) && (serial->messageTxLength < serial->messageTxBufferSize))
                        {
                            if ((i != 0) && ((message[i] == XBEM_START_CHARACTER) || (message[i] == XBEM_ESCAPE_CHARACTER) || (message[i] == XBEM_SPECIAL_CHARACTER_0x11) || (message[i] == XBEM_SPECIAL_CHARACTER_0x13)))
                            {
                                serial->messageTxBufferPtr[serial->messageTxLength++] = XBEM_ESCAPE_CHARACTER;
                                serial->messageTxBufferPtr[serial->messageTxLength++] = (message[i] ^ XBEM_ESCAPE_XOR_CHARACTER);
                            }
                            else
                            {
                              serial->messageTxBufferPtr[serial->messageTxLength++] = message[i];
                            }
                            i++;
                        }
                        /* END PREPARE MESSAGE TO BE SENT, ADD CHECKSUM and other stuff */
                        /* if prepare fct is ok, or no prepare function has been called */
                        if(returnValue == SERIAL_COM_RETURN_OK)
                        {
                            if(serial_send(serial->idComm, &serial->messageTxBufferPtr[0], serial->messageTxLength) > 0)
                            {
                                /* message sent */
                                if(serial->txCallbackFct != NULL)
                                {
                                    /* I don't care about this function return */
                                    (void)serial->txCallbackFct(message, messageLength/*&serial->messageTxBufferPtr[0], serial->messageTxLength*/);
                                }
                                
                                serial->responseToWait = _responseToWait;
                                if(serial->responseToWait != false) /* need to wait a response */
                                {
                                    /* Flag to check present, go to TEST_SEND_MESSAGE_STATE_WAIT_RESPONSE */
                                    serial->state = SERIAL_COM_STATE_WAIT_RESPONSE;
                                    serial->retryTx = 0U;
                                    serial->timeoutMsCounter = 0U;
                                    returnValue = SERIAL_COM_RETURN_BUSY;
                                }
                                else /* no response to wait */
                                {
                                    serial->state = SERIAL_COM_STATE_READY;
                                    serial->retryTx = 0U;
                                    returnValue = SERIAL_COM_RETURN_OK;
                                }
                            }
                            else
                            {
                                /* peripheral tx busy */
                                serial->retryTx++;
                                if(serial->maxTryTxCommand != 0U) /* Check for retryTx*/
                                {
                                    if(serial->retryTx >= serial->maxTryTxCommand)
                                    {
                                        /* Tx threshold reached */
                                        serial->retryTx = 0U;
                                        serial->state = SERIAL_COM_STATE_READY;
                                        returnValue = SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY;
                                    }
                                    else
                                    {
                                        /* Tx threashold not reached, retry */
                                        serial->state = SERIAL_COM_STATE_READY;
                                        returnValue = SERIAL_COM_RETURN_BUSY;
                                    }
                                }
                                else
                                {
                                    /* No check for retry tx */
                                    serial->state = SERIAL_COM_STATE_READY;
                                    returnValue = SERIAL_COM_RETURN_TX_BUSY;
                                }
                            }
                        }
                        break;
                    case SERIAL_COM_STATE_WAIT_RESPONSE:
                        if(serial->timeoutResponseMs > 0U)
                        {   
                            /* There is a timeout */
                            serial->timeoutMsCounter += serial->executionTime;
                            if(serial->timeoutMsCounter < serial->timeoutResponseMs)
                            {
                                /* check for flag */
                                if(serial->messageArrivedFlag != false)
                                {
                                    serial->messageArrivedFlag = false;
                                    serial->state = SERIAL_COM_STATE_READY;                                
                                    serial->timeoutNumberCounter = 0;
                                }
                                else
                                {
                                    /* Nothing, stay here and wait */
                                    returnValue = SERIAL_COM_RETURN_BUSY;
                                }
                            }
                            else
                            {
                                /* time out reached for the response */
                                serial->timeoutNumberCounter++;
                                /* debug_print("timeoutNumberCounter: %d", timeoutNumberCounter); */
                                if(serial->timeoutNumberCounter >= serial->maxNumberOfTimeout)
                                {
                                    returnValue = SERIAL_COM_RETURN_TIMEOUT;
                                    serial->state = SERIAL_COM_STATE_READY;
                                    serial->timeoutNumberCounter = 0;
                                }
                                else
                                {
                                    returnValue = SERIAL_COM_RETURN_BUSY;
                                    serial->state = SERIAL_COM_STATE_READY;
                                }
                            }
                        }
                        else
                        {
                            /* Infinite timeout until flag goes up */
                            /* check for flag */
                            if(serial->messageArrivedFlag != false)
                            {
                                serial->messageArrivedFlag = false;
                                serial->state = SERIAL_COM_STATE_READY;
                            }
                            else
                            {
                                /* Nothing, stay here forever and wait */
                                returnValue = SERIAL_COM_RETURN_BUSY;
                            }
                        }
                        break;
                    default:
                        returnValue = SERIAL_COM_RETURN_ERROR_AUTOMA;
                        serial->state = SERIAL_COM_STATE_READY;
                        break;
                }/* end switch */
            }
            else
            {
                returnValue = SERIAL_COM_RETURN_SERIAL_BUFFER_SIZE_WRONG;
            }              
        }
    }
    else
    {
        debug_print("SerialCom_sendMessage: SERIAL_COM_RETURN_ERROR_PARAM");
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }
    
    switch(returnValue)
    {
        case SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY:
        case SERIAL_COM_RETURN_TX_BUSY:
        case SERIAL_COM_RETURN_TIMEOUT:
        case SERIAL_COM_RETURN_ERROR_AUTOMA:
        case SERIAL_COM_RETURN_PREPARE_ERROR:
        case SERIAL_COM_RETURN_VALIDATION_ERROR:
        case SERIAL_COM_RETURN_VALIDATION_MESSAGE_INVALID:
        case SERIAL_COM_RETURN_CALLBACK_ERROR:
            //debug_print("[ERRO] SerialCom_sendMessage: %d", returnValue);
            break;
        default:
            break;
    }

    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_initModbus(SERIAL_COM_MODBUS_t* _serial, ulong _timeoutResponseMs, ulong _maxNumberOfTimeout, ulong _maxTryTxCommand, SerialCom_txCallbackFct _txCallbackFct, SerialCom_rxCallbackFct _rxCallbackFct, ubyte* _messageTxBufferPtr, ulong _messageTxBufferSize, ubyte* _messageRxBufferPtr, ulong _messageRxBufferSize, ulong _executionTime, ulong _timerSilence)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    
    if(_serial != NULL)
    {
        _serial->validConfiguration = false;
        
        if((_executionTime > 0U) && (_timeoutResponseMs > 0U) && (_maxNumberOfTimeout > 0U) && (_timerSilence > 0U) && (_messageRxBufferPtr != NULL) && (_messageRxBufferSize > 0U) && (_messageTxBufferPtr != NULL) && (_messageTxBufferSize > 0U) && (_rxCallbackFct != NULL))
        {
            /* configuration */
            _serial->idComm = -1; /* !!!!!!!!!SIGNED!!!!!!!*/
            _serial->timeoutResponseMs = _timeoutResponseMs;
            _serial->maxNumberOfTimeout = _maxNumberOfTimeout;
            _serial->maxTryTxCommand = _maxTryTxCommand;
            
            _serial->rxCallbackFct = _rxCallbackFct;
            _serial->txCallbackFct = _txCallbackFct;
            _serial->messageTxBufferPtr = _messageTxBufferPtr;
            _serial->messageTxBufferSize = _messageTxBufferSize;
            
            _serial->messageRxBufferPtr = _messageRxBufferPtr;
            _serial->messageRxBufferSize = _messageRxBufferSize;
            
            _serial->executionTime = _executionTime; 
            _serial->timerSilenceMs = _timerSilence;
            
            /* Internal variables */
            _serial->messageRxLength = 0U;
            _serial->messageTxLength = 0U;
            _serial->messageArrivedFlag = false;
            _serial->state = SERIAL_COM_STATE_READY;
            _serial->retryTx = 0U;
            _serial->timeoutNumberCounter = 0U;
            _serial->timeoutMsCounter = 0U;
            _serial->checksumFaultCounter = 0U;
            _serial->txMessageCounter = 0U;
            _serial->rxMessageCounter = 0U;
            _serial->rxLengthFaultCounter = 0U;
            _serial->txBusy = 0U;
            _serial->rxDiscardedDueToTimeout = 0U;
            _serial->rxCompleteCounter = 0U;
            _serial->firstFrameReceived = false;
            _serial->inTimeout = false;
            
            debug_print("SERIAL_COM_MODBUS_t 0x%08X valid configuration", _serial);
            _serial->validConfiguration = true;
        }
        else
        {
            debug_print("SERIAL_COM_MODBUS_t 0x%08X INVALID SERIAL_COM_RETURN_ERROR_PARAM", _serial);
            returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
        }
    }
    else
    {
        debug_print("SERIAL_COM_MODBUS_t 0x%08X INVALID SERIAL_COM_RETURN_ERROR_PARAM", _serial);
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != SERIAL_COM_RETURN_OK)
    {
        debug_print("[ERRO] SerialCom_initModbus %d", returnValue);
    }
    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_openModbus(SERIAL_COM_MODBUS_t* serial, _SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stop)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if((serial != NULL) && (serial->validConfiguration != false)) /* at least the possibility to send */
    {
        serial->idComm = serial_open( 
                            port,
                            baudRate,
                            dataBits,
                            parity,
                            stop
        );
        if(serial->idComm < 0)
        {
            debug_print("[WARN] SerialCom_openModbus %d NOT OPEN", serial->idComm);
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            debug_print("[INFO] SerialCom_openModbus %d OPEN", serial->idComm);
            returnValue = SERIAL_COM_RETURN_OK;
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    } 

    if(returnValue != SERIAL_COM_RETURN_OK)
    {
        debug_print("[ERRO] SerialCom_openModbus %d", returnValue);
    }
   
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_closeModbus(SERIAL_COM_MODBUS_t* serial)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if((serial != NULL) && (serial->idComm >= 0))
    {
        if(serial_close(serial->idComm) < 0)
        {
            debug_print("[WARN] SerialCom_closeModbus %d NOT CLOSED", serial->idComm);
        }
        else
        {
            debug_print("[INFO] SerialCom_closeModbus %d CLOSED", serial->idComm);
        }
        serial->idComm = -1;
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }    
    
    if(returnValue != SERIAL_COM_RETURN_OK)
    {
        debug_print("[ERRO] SerialCom_closeModbus %d", returnValue);
    }
    
    return returnValue;
}

static ushort Serial_ModbusCrcCalculator(ubyte* data, ushort length)
{
    ushort j;
    ushort reg_crc = 0xFFFF;
    while(length--)
    {
        reg_crc ^= *data++;
        for(j=0;j<8;j++)
        {
            if(reg_crc & 0x0001U)
            { /* LSB(b0)=1 */
                reg_crc = (reg_crc>>1U) ^ 0xA001U;
            }
            else
            {
                reg_crc = reg_crc >> 1;
            }
        }
    }
    return reg_crc;
}

SERIAL_COM_RETURN_t SerialCom_receiveModbus(SERIAL_COM_MODBUS_t* serial)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    int nBytesReceived = 0U;
    ushort CrcCalculated;
    ushort CrcMessage;
    
    if((serial != NULL) && (serial->validConfiguration != false))
    {
        if(serial->idComm < 0)
        {
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            /* Check for bytes RX */
            while(serial_bytes_available(serial->idComm) > 0)
            {
                nBytesReceived = serial_bytes_available(serial->idComm);
                if((serial->messageRxLength + nBytesReceived) <= serial->messageRxBufferSize)
                {
                    serial_read(serial->idComm, &serial->messageRxBufferPtr[serial->messageRxLength], nBytesReceived);
                    serial->messageRxLength += nBytesReceived;
                    if(serial->firstFrameReceived == false)
                    {
                        serial->firstFrameReceived = true;
                        //debug_print("FF");
                    }
                    /* Refresh timer silence for rx complete */
                    serial->rxCompleteCounter = serial->timerSilenceMs;
                }
                else
                {
                    returnValue = SERIAL_COM_RETURN_BUFFER_RX_NOT_BIG_ENOUGTH;
                } 
            }
                
            if(serial->firstFrameReceived != false)
            {
                if(serial->rxCompleteCounter > 0U)
                {
                    /* Decrease counter */
                    if(serial->rxCompleteCounter >= serial->executionTime)
                    {
                        serial->rxCompleteCounter -= serial->executionTime;
                    }
                    else
                    {
                        serial->rxCompleteCounter = 0U;
                    }
                }
                else
                {                       
                    /* Calculate CRC for Modbus return OK if the message is ok, an error Otherwise 
                    Reduce the rxMessageSize of 2 for the modbus crc16
                    */
                    if(serial->messageRxLength > 2U)
                    {
                        if(serial->rxMessageCounter < 0xFFFFFFFFU)
                        {
                            serial->rxMessageCounter++;
                        }
                        CrcCalculated = Serial_ModbusCrcCalculator(serial->messageRxBufferPtr, serial->messageRxLength - 2U);
                        CrcMessage = (serial->messageRxBufferPtr[serial->messageRxLength - 1] << 8U) + (serial->messageRxBufferPtr[serial->messageRxLength - 2] << 0U);                        
                        if(CrcCalculated != CrcMessage)
                        {
                            if(serial->checksumFaultCounter < 0xFFFFFFFFU)
                            {
                                serial->checksumFaultCounter++;
                            }
                            returnValue = SERIAL_COM_RETURN_VALIDATION_MESSAGE_INVALID;
                        }
                        else
                        {
                            serial->messageRxLength = serial->messageRxLength - 2U;
                            if(serial->rxCallbackFct != NULL)
                            {
                                returnValue = serial->rxCallbackFct(serial->messageRxBufferPtr, serial->messageRxLength);
                            }
                            
                            if(serial->inTimeout != false)
                            {
                                debug_print("Message discarded for timeout flag");
                                if(serial->rxDiscardedDueToTimeout < 0xFFFFFFFFU)
                                {
                                    serial->rxDiscardedDueToTimeout++;
                                }
                            }
                            else
                            {
                                serial->messageArrivedFlag = true;
                            }
                        }
                    }
                    else
                    {
                        if(serial->rxLengthFaultCounter < 0xFFFFFFFFU)
                        {
                            serial->rxLengthFaultCounter++;
                        }
                    }
                    serial->messageRxLength = 0U;
                    serial->firstFrameReceived = false; 
                }
            }                                                                                                                                       
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }  
    
    if(returnValue != SERIAL_COM_RETURN_OK)
    {
        debug_print("[ERRO] SerialCom_receiveModbus %d", returnValue);
    }
    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_sendMessageModbus(SERIAL_COM_MODBUS_t* serial, ubyte* message, ulong messageLength)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if((serial != NULL) && (serial->validConfiguration != false) && (message != NULL) && (messageLength > 0U)) 
    {
        if(serial->idComm < 0)
        {
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            /* CRC 16 is 2 byte */
            if((messageLength + 2U) <= serial->messageTxBufferSize)
            {
                switch(serial->state)
                {
                    case SERIAL_COM_STATE_READY:
                        serial->messageArrivedFlag = false; /* clear the flag before send */
                        ushort CrcCalculated = Serial_ModbusCrcCalculator(message, messageLength);
                        memory_copy(serial->messageTxBufferPtr, message, messageLength);
                        serial->messageTxBufferPtr[messageLength + 0U] = (CrcCalculated & 0x00FFU); /* CRC to calculate lo */
                        serial->messageTxBufferPtr[messageLength + 1U] = ((CrcCalculated & 0xFF00U) >> 8); /* CRC to calculate hi */
                        serial->messageTxLength = (messageLength + 2U);
                            
                        if(serial_send(serial->idComm, serial->messageTxBufferPtr, serial->messageTxLength) > 0)
                        {
                            /* I sent a message, if in timeout, remove the timeout flag in order to receive a response */
                            if(serial->inTimeout != FALSE)
                            {
                                debug_print("RESET timeout flag");
                                serial->inTimeout = FALSE;
                            }
                            if(serial->txMessageCounter < 0xFFFFFFFFU)
                            {
                                serial->txMessageCounter++;
                            }
                            /* message sent */
                            /* I don't care about this function return */
                            (void)serial->txCallbackFct(serial->messageTxBufferPtr, serial->messageTxLength);
                            serial->state = SERIAL_COM_STATE_WAIT_RESPONSE;
                            serial->timeoutMsCounter = 0U;
                            returnValue = SERIAL_COM_RETURN_BUSY;
                        }
                        else
                        {
                            if(serial->txBusy < 0xFFFFFFFFU)
                            {
                                serial->txBusy++;
                            }
                            //debug_print("serial_send < = 0");
                            /* peripheral tx busy */
                            serial->retryTx++;
                            if(serial->maxTryTxCommand != 0U) /* Check for retryTx*/
                            {
                                if(serial->retryTx >= serial->maxTryTxCommand)
                                {
                                    /* Tx threshold reached */
                                    serial->retryTx = 0U;
                                    serial->state = SERIAL_COM_STATE_READY;
                                    returnValue = SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY;
                                }
                                else
                                {
                                    /* Tx threashold not reached, retry */
                                    serial->state = SERIAL_COM_STATE_READY;
                                    returnValue = SERIAL_COM_RETURN_BUSY;
                                }
                            }
                            else
                            {
                                /* No check for retry tx */
                                serial->state = SERIAL_COM_STATE_READY;
                                returnValue = SERIAL_COM_RETURN_TX_BUSY;
                            }
                        }
                        break;
                    case SERIAL_COM_STATE_WAIT_RESPONSE:
                        serial->timeoutMsCounter += serial->executionTime;
                        if(serial->timeoutMsCounter < serial->timeoutResponseMs)
                        {
                            /* check for flag */
                            if(serial->messageArrivedFlag != false)
                            {
                                serial->messageArrivedFlag = false;
                                serial->state = SERIAL_COM_STATE_READY;                                
                                serial->timeoutNumberCounter = 0;
                            }
                            else
                            {
                                /* Nothing, stay here and wait */
                                returnValue = SERIAL_COM_RETURN_BUSY;
                            }
                        }
                        else
                        {
                            /* time out reached for the response */
                            serial->timeoutNumberCounter++;
                            /* debug_print("timeoutNumberCounter: %d", timeoutNumberCounter); */
                            if(serial->timeoutNumberCounter >= serial->maxNumberOfTimeout)
                            {
                                returnValue = SERIAL_COM_RETURN_TIMEOUT;
                                serial->state = SERIAL_COM_STATE_READY;
                                serial->timeoutNumberCounter = 0;
                            }
                            else
                            {
                                returnValue = SERIAL_COM_RETURN_BUSY;
                                serial->state = SERIAL_COM_STATE_READY;
                            }
                        }
                    break;
                    default:
                        returnValue = SERIAL_COM_RETURN_ERROR_AUTOMA;
                        serial->state = SERIAL_COM_STATE_READY;
                        break;
                }/* End switch */
            }
            else
            {
                returnValue = SERIAL_COM_RETURN_SERIAL_BUFFER_SIZE_WRONG;
            } 
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    } 
    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_SetTimeoutModbus(SERIAL_COM_MODBUS_t* serial)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if(serial != NULL)
    {
        serial->state = SERIAL_COM_STATE_READY;
        serial->retryTx = 0U;
        serial->timeoutNumberCounter = 0U;
        serial->timeoutMsCounter = 0U;
        serial->inTimeout = TRUE;
        debug_print("SET timeout flag");
        returnValue = SERIAL_COM_RETURN_OK;
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_initIsocheck(
    SERIAL_COM_ISOCHECK_t* _serial, 
    ulong _timeoutResponseMs, 
    ulong _maxNumberOfTimeout, 
    ulong _maxTryTxCommand, 
    SerialCom_rxCallbackFct _rxCallbackFct, 
    ubyte* _messageTxBufferPtr, 
    ulong _messageTxBufferSize, 
    ubyte* _messageRxBufferPtr, 
    ulong _messageRxBufferSize, 
    ulong _executionTime, 
    ulong _timerSilence
)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    
    if(_serial != NULL)
    {
        _serial->validConfiguration = false;
        
        if(
            (_executionTime > 0U) && 
            (_timeoutResponseMs > 0U) && 
            (_maxNumberOfTimeout > 0U) && 
            (_timerSilence > 0U) && 
            (_messageRxBufferPtr != NULL) && 
            (_messageRxBufferSize > 0U) && 
            (_messageTxBufferPtr != NULL) && 
            (_messageTxBufferSize > 0U) && 
            (_rxCallbackFct != NULL) &&
            (_maxNumberOfTimeout > 0U)
        )
        {
            /* configuration */
            _serial->idComm = -1; /* !!!!!!!!!SIGNED!!!!!!!*/
            _serial->timeoutResponseMs = _timeoutResponseMs;
            _serial->maxNumberOfTimeout = _maxNumberOfTimeout;
            _serial->maxTryTxCommand = _maxTryTxCommand;
        
            _serial->rxCallbackFct = _rxCallbackFct;
            _serial->messageTxBufferPtr = _messageTxBufferPtr;
            _serial->messageTxBufferSize = _messageTxBufferSize;
            
            _serial->messageRxBufferPtr = _messageRxBufferPtr;
            _serial->messageRxBufferSize = _messageRxBufferSize;
            
            _serial->timerSilenceMs = _timerSilence;
            _serial->executionTime = _executionTime; 
            
            /* Internal variables */
            _serial->messageRxLength = 0U;
            _serial->messageTxLength = 0U;
            _serial->messageArrivedFlag = false;
            _serial->state = SERIAL_COM_STATE_READY;
            _serial->retryTx = 0U;
            _serial->timeoutNumberCounter = 0U;
            _serial->timeoutMsCounter = 0U;
            _serial->integrityFaultCounter = 0U;
            _serial->firstFrameReceived = false; 
            _serial->rxCompleteCounter = 0U;
            
            _serial->txDone = false;
            /*
            typedef struct SERIAL_COM_ISOCHECK_s
            {
                
                slong idComm; 
                ulong timeoutResponseMs;
                ulong maxNumberOfTimeout;
                ulong maxTryTxCommand;
            
                SerialCom_rxCallbackFct rxCallbackFct;
            
                ubyte* messageTxBufferPtr;
                ulong messageTxLength;
                ulong messageTxBufferSize;
                ubyte* messageRxBufferPtr;
                ulong messageRxLength;
                ulong messageRxBufferSize;
                ulong executionTime; 
                ulong timerSilenceMs;
            }SERIAL_COM_ISOCHECK_t;
            */
            _serial->validConfiguration = true; 
        }
        else
        {
            debug_print("SERIAL_COM_ISOCHECK_t 0x%08X INVALID SERIAL_COM_RETURN_ERROR_PARAM 2", _serial);
            returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
        }   
    }
    else
    {
        debug_print("SERIAL_COM_ISOCHECK_t 0x%08X INVALID SERIAL_COM_RETURN_ERROR_PARAM 1", _serial);
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_receiveIsocheck(SERIAL_COM_ISOCHECK_t* _serial)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    int nBytesReceived = 0U;
    
    
    if((_serial != NULL) && (_serial->validConfiguration != false))
    {
        if(_serial->idComm < 0)
        {
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            /* Check for bytes RX */
            while(serial_bytes_available(_serial->idComm) > 0)
            {
                if(_serial->txDone != false) /* Byte requested */
                {
                    nBytesReceived = serial_bytes_available(_serial->idComm);
                    
                    if((_serial->messageRxLength + nBytesReceived) <= _serial->messageRxBufferSize)
                    {
                        serial_read(_serial->idComm, &_serial->messageRxBufferPtr[_serial->messageRxLength], nBytesReceived);
                        _serial->messageRxLength += nBytesReceived;
                        if(_serial->firstFrameReceived == false)
                        {
                            _serial->firstFrameReceived = true;
                            //debug_print("FF");
                        }
                        /* Refresh timer silence for rx complete */
                        _serial->rxCompleteCounter = _serial->timerSilenceMs;
                        //debug_print("REF");
                    }
                    else
                    {
                        returnValue = SERIAL_COM_RETURN_BUFFER_RX_NOT_BIG_ENOUGTH;
                    } 
                }
                else
                {
                    /* Byte not requested, maybe is the line with noise */
                    ubyte discardByte;
                    serial_read(_serial->idComm, &discardByte, 1U);
                    debug_print("SERIAL_COM_ISOCHECK_t byte discarded %02X", discardByte);
                }
            }
                
            if(_serial->firstFrameReceived != false)
            {
                //debug_print("RXCC %d", _serial->rxCompleteCounter);
                if(_serial->rxCompleteCounter > 0U)
                {
                    /* Decrease counter */
                    if(_serial->rxCompleteCounter >= _serial->executionTime)
                    {
                        _serial->rxCompleteCounter -= _serial->executionTime;
                    }
                    else
                    {
                        _serial->rxCompleteCounter = 0U;
                    }
                }
                else
                {   
                    _serial->txDone = false;
                    if(_serial->rxCallbackFct != NULL)
                    {
                        returnValue = _serial->rxCallbackFct(_serial->messageRxBufferPtr, _serial->messageRxLength);
                        if(returnValue == STD_RETURN_OK)
                        {
                            _serial->messageArrivedFlag = true;
                        }
                    }
                    _serial->messageRxLength = 0U;
                    _serial->firstFrameReceived = false; 
                }
            }                                                                                                                                       
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }  
    
    if(returnValue != SERIAL_COM_RETURN_OK)
    {
        debug_print("[ERRO] SerialCom_receiveIsocheck %d", returnValue);
    }
    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_closeIsocheck(SERIAL_COM_ISOCHECK_t* _serial)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if(_serial != NULL)
    {
        if(_serial->idComm < 0)
        {
            debug_print("[WARN] SerialCom_closeIsocheck %d ALREADY CLOSED", _serial->idComm);
        }
        else
        {
            if(serial_close(_serial->idComm) < 0)
            {
                debug_print("[WARN] SerialCom_closeIsocheck %d NOT CLOSED", _serial->idComm);
            }
            else
            {
                debug_print("[INFO] SerialCom_closeIsocheck %d CLOSED", _serial->idComm);
            }
            _serial->idComm = -1;
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_openIsocheck(SERIAL_COM_ISOCHECK_t* _serial, _SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stop)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if((_serial != NULL) && (_serial->validConfiguration != false)) /* at least the possibility to send */
    {
        if(_serial->idComm >= 0)
        {
            debug_print("[WARN] SerialCom_openIsocheck %d ALREADY OPEN", _serial->idComm);
        }
        else
        {
            _serial->idComm = serial_open( 
                        port,
                        baudRate,
                        dataBits,
                        parity,
                        stop
            );
        }

        if(_serial->idComm < 0)
        {
            debug_print("[WARN] SerialCom_openIsocheck %d NOT OPEN", _serial->idComm);
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            debug_print("[INFO] SerialCom_openIsocheck %d OPEN", _serial->idComm);
            returnValue = SERIAL_COM_RETURN_OK;
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    }    
    return returnValue;
}

SERIAL_COM_RETURN_t SerialCom_sendMessageIsocheck(SERIAL_COM_ISOCHECK_t* _serial, ubyte* message, ulong messageLength)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    if((_serial != NULL) && (_serial->validConfiguration != false) && (message != NULL) && (messageLength > 0U)) 
    {
        if(_serial->idComm < 0)
        {
            returnValue = SERIAL_COM_SERIAL_PORT_NOT_OPEN;
        }
        else
        {
            switch(_serial->state)
            {
                case SERIAL_COM_STATE_READY:
                    _serial->messageArrivedFlag = false; /* clear the flag before send */
                        
                    if(serial_send(_serial->idComm, message, messageLength) > 0)
                    {
                        /* message sent */
                        //debug_print("SENT");
                        _serial->txDone = true;
                        _serial->state = SERIAL_COM_STATE_WAIT_RESPONSE;
                        _serial->timeoutMsCounter = 0U;
                        returnValue = SERIAL_COM_RETURN_BUSY;
                    }
                    else
                    {
                        //debug_print("serial_send < = 0");
                        /* peripheral tx busy */
                        _serial->retryTx++;
                        if(_serial->maxTryTxCommand != 0U) /* Check for retryTx*/
                        {
                            if(_serial->retryTx >= _serial->maxTryTxCommand)
                            {
                                /* Tx threshold reached */
                                _serial->retryTx = 0U;
                                _serial->state = SERIAL_COM_STATE_READY;
                                returnValue = SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY;
                            }
                            else
                            {
                                /* Tx threashold not reached, retry */
                                _serial->state = SERIAL_COM_STATE_READY;
                                returnValue = SERIAL_COM_RETURN_BUSY;
                            }
                        }
                        else
                        {
                            /* No check for retry tx */
                            _serial->state = SERIAL_COM_STATE_READY;
                            returnValue = SERIAL_COM_RETURN_TX_BUSY;
                        }
                    }
                    break;
                case SERIAL_COM_STATE_WAIT_RESPONSE:
                    //debug_print("WAIT %d %d %d", _serial->timeoutMsCounter, _serial->timeoutResponseMs, _serial->messageArrivedFlag);
                    _serial->timeoutMsCounter += _serial->executionTime;
                    if(_serial->timeoutMsCounter < _serial->timeoutResponseMs)
                    {
                        /* check for flag */
                        if(_serial->messageArrivedFlag != false)
                        {
                            //debug_print("ARRIVED");
                            _serial->messageArrivedFlag = false;
                            _serial->state = SERIAL_COM_STATE_READY;                                
                            _serial->timeoutNumberCounter = 0;
                        }
                        else
                        {
                            /* Nothing, stay here and wait */
                            returnValue = SERIAL_COM_RETURN_BUSY;
                        }
                    }
                    else
                    {
                        /* time out reached for the response */
                        _serial->timeoutNumberCounter++;
                        //debug_print("timeoutNumberCounter: %d", _serial->timeoutNumberCounter);
                        if(_serial->timeoutNumberCounter >= _serial->maxNumberOfTimeout)
                        {
                            returnValue = SERIAL_COM_RETURN_TIMEOUT;
                            _serial->state = SERIAL_COM_STATE_READY;
                            _serial->timeoutNumberCounter = 0;
                        }
                        else
                        {
                            returnValue = SERIAL_COM_RETURN_BUSY;
                            _serial->state = SERIAL_COM_STATE_READY;
                        }
                    }
                break;
                default:
                    returnValue = SERIAL_COM_RETURN_ERROR_AUTOMA;
                    _serial->state = SERIAL_COM_STATE_READY;
                    break;
            }/* End switch */
        }
    }
    else
    {
        returnValue = SERIAL_COM_RETURN_ERROR_PARAM;
    } 
    
    return returnValue;
}

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
//void serial_com_init()
//{
//}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
//void serial_com()
//{
//}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void serial_com_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void serial_com_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
//void serial_com_thread()
//{
//}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void serial_com_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void serial_com_shutdown()
//{
//}
