/*
 * network.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define NETWORK_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "network.h"
#include "serial_com.h"
#include "flag.h"
#include "message_screen.h"
#include "eepm_data.h"

/* Private define ------------------------------------------------------------*/
//#define NETWORK_DEBUG

/* #define NETWORK_ENABLE_DEBUG_COUNTERS */
/* #define NETWORK_NORMAL_ENABLE */
/* #define NETWORK_FAST_ENABLE */
/* #define NETWORK_LOW_PRIO_ENABLE */
#define NETWORK_THREAD_ENABLE

#define NETWORK_THREAD_SLEEP_MS  5U
#define NETWORK_TIMER_SILENCE_MS 5U

#define NETWORK_GR_PORT_DEFAULT NETWORK_SERIAL_PORT

#define NETWORK_GR_TX_BUFFER 512
#define NETWORK_GR_RX_BUFFER 512

#define NETWORK_ID_STRING_SIZE 7

#define NETWORK_GR_NODE_ID 0x01U
#define NETWORK_FUNCTION_NOT_STANDARD 0x60U

#define NETWORK_POSITION_NODEID_BYTE 0U
#define NETWORK_POSITION_FUNCTION_BYTE 1U
#define NETWORK_POSITION_COMMAND_BYTE 2U
#define NETWORK_POSITION_FIRST_DATA_BYTE 3U

#define NETWORK_INIT_BYTE_NUMBER NETWORK_POSITION_FIRST_DATA_BYTE

#define NETWORK_KEEP_ALIVE_TIMER_MS 4000U
#define NETWORK_GET_ALL_DATA_TIMER_MS 1000U
#define NETWORK_CPU_LOAD_TIMER_MS 1000U
#define NETWORK_REFRESH_PUMP_SETPOINT_TIMER_MS 2000U

#define NETWORK_TIMEOUT_MS 4000U

#define NETWORK_INVALID_16BIT_VALUE 0xFFFFU
#define NETWORK_INVALID_24BIT_VALUE 0xFFFFFFU
#define NETWORK_INVALID_32BIT_VALUE 0xFFFFFFFFU

#define NETWORK_GET_ERROR_REFRESH_TIMER_MS 500U
#define NETWORK_GET_INVALID_SIGNAL_CODE_REFRESH_TIMER_MS 500U
#define NETWORK_PENDING_16BIT_VALUE 0xFFFE
#define NETWORK_PENDING_24BIT_VALUE 0xFFFFFE
#define NETWORK_PENDING_32BIT_VALUE 0xFFFFFFFEU
#define NETWORK_GET_MEMORY_DATA_BUFFER_MAX_SIZE 256

#define NETWORK_REFRESH_PUMP_PERCENTAGE_READ_TIMER_MS 250U




/* #define MSB_FIRST */

/* NETWORK_INIT_BYTE_NUMBER is 3U */
#define NETWORK_BUFFER_0x00_RX_SIZE 4U
#define NETWORK_BUFFER_0x01_RX_SIZE 12U
#define NETWORK_BUFFER_0x02_RX_SIZE 19U
#define NETWORK_BUFFER_0x03_RX_SIZE 4U
#define NETWORK_BUFFER_0x04_RX_SIZE 6U
#define NETWORK_BUFFER_0x05_RX_SIZE 6U
#define NETWORK_BUFFER_0x06_RX_SIZE 10U
#define NETWORK_BUFFER_0x07_RX_SIZE (4U + EEPM_MAX_WRITE_BUFFER_SIZE)
#define NETWORK_BUFFER_0x08_RX_SIZE 4U
#define NETWORK_BUFFER_0x09_RX_SIZE 4U
#define NETWORK_BUFFER_0x0A_RX_SIZE 4U
#define NETWORK_BUFFER_0x0B_RX_SIZE 6U
#define NETWORK_BUFFER_0x0C_RX_SIZE 5U
#define NETWORK_BUFFER_0x0D_RX_SIZE 6U
#define NETWORK_BUFFER_0x0E_RX_SIZE 4U
#define NETWORK_BUFFER_0x0F_RX_SIZE 21U
#define NETWORK_BUFFER_0x10_RX_SIZE 4U
#define NETWORK_BUFFER_0x11_RX_SIZE 10U
#define NETWORK_BUFFER_0x12_RX_SIZE (4U + (((EEPM_ID_MAX_NUMBER - 1U) / 8U) + 1U))
#define NETWORK_BUFFER_0x13_RX_SIZE 4U

#define SYSTEM_MESSAGE_RX_BUFFER_LENGTH 32U
#define SYSTEM_MESSAGE_TX_BUFFER_LENGTH 32U

/* Private typedef -----------------------------------------------------------*/
typedef enum NETWORK_SERVICE_STATUS_e
{
    NETWORK_SERVICE_READY = 0,           /* Service in idle */
    NETWORK_SERVICE_TX_WAITING,          /* Message inserted in queue, waiting scheduler to send */
    NETWORK_SERVICE_TX_SENT_WAITING_RX,  /* Message sent by scheduler, waiting rx */
    NETWORK_SERVICE_RX_ARRIVED,          /* Rx arrived, waiting for a Get Message Response */
    NETWORK_SERVICE_MAX_NUMBER
}NETWORK_SERVICE_STATUS_t;

typedef struct NETWORK_RX_ENTRY_s
{
    ulong sizePtr;
    ubyte* ptr;
}NETWORK_RX_ENTRY_t;

typedef enum NETWORK_TIMER_e
{
	NETWORK_TIMER_0x02_GET_ALL_DATA = 0,
	NETWORK_TIMER_0x0D_GET_CPU_LOAD,
	NETWORK_TIMER_TIMEOUT_NETWORK,
	NETWORK_TIMER_0x0B_GET_PUMP_PERCENTAGE,
	NETWORK_TIMER_MAX_NUMBER
}NETWORK_TIMER_t;

typedef enum NETM_EEPROM_FIXED_ID_e
{
    NETM_EEPROM_FIXED_ID_LAST_CALIBRATION_DATE = 0x8000,
    NETM_EEPROM_FIXED_ID_SECOND_LAST_CALIBRATION_DATE,
    NETM_EEPROM_FIXED_ID_FIRST_CALIBRATION_DATE,
    NETM_EEPROM_FIXED_ID_KAPPA_METER,
    NETM_EEPROM_FIXED_ID_METER_TEMPERATURE_OFFSET,
    NETM_EEPROM_FIXED_ID_EXTERNAL_TEMPERATURE_OFFSET,
    NETM_EEPROM_FIXED_ID_EXTERNAL_PRESSURE_OFFSET,
    NETM_EEPROM_FIXED_ID_VACUUM_OFFSET,
    NETM_EEPROM_FIXED_ID_VACUUM_ZERO_MV,
    NETM_EEPROM_FIXED_ID_VACUUM_END_OF_SCALE_MV,
    NETM_EEPROM_FIXED_ID_VACUUM_CALIBRATION_PUMP_MAX_VOID_MMH2O,

    NETM_EEPROM_FIXED_ID_MAX_NUMBER
}NETM_EEPROM_FIXED_ID_t;

typedef union Network_Flags_u
{
    ubyte data[2];
    struct 
    {
#ifdef MSB_FIRST
        /* Flag 0 */
        ubyte FlagPumpRunning : 1;
        ubyte FlagPumpRegulation : 1;
        ubyte MeterPressureMounted : 1;
        ubyte MeterTemperatureMounted : 1;
        ubyte ExternTemperatureMounted : 1;
        ubyte ExternPressureMounted : 1;
        ubyte InverterOrPwm : 1;
        ubyte Warning : 1;
#else
        ubyte Warning : 1;
        ubyte InverterOrPwm : 1;
        ubyte ExternPressureMounted : 1;
        ubyte ExternTemperatureMounted : 1;
        ubyte MeterTemperatureMounted : 1;
        ubyte MeterPressureMounted : 1;
        ubyte FlagPumpRegulation : 1;
        ubyte FlagPumpRunning : 1;
#endif /* MSB_FIRST */
#ifdef MSB_FIRST
        /* Flag 1 */
        ubyte ErrorGr : 1;
        ubyte ExternalPower : 1;
        ubyte BatteryVoltageCritical : 1;
        ubyte FilterTemperatureMounted : 1;
        ubyte StorageTemperatureMounted : 1;
        ubyte unused2 : 1;
        ubyte unused1 : 1;
        ubyte unused0 : 1;
#else
        ubyte unused0 : 1;
        ubyte unused1 : 1;
        ubyte unused2 : 1;
        ubyte StorageTemperatureMounted : 1;
        ubyte FilterTemperatureMounted : 1;
        ubyte BatteryVoltageCritical : 1;
        ubyte ExternalPower : 1;
        ubyte Error : 1;
#endif /* MSB_FIRST */
    }flags_t;
}Network_Flags_t;

#define HMI_MY_ID HMI_ID_SPLASH

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static slong Network_grPortDefault;
static SERIAL_COM_MODBUS_t Network_serialComGr;

static ubyte System_MessageRxBuffer[SYSTEM_MESSAGE_RX_BUFFER_LENGTH];
static ulong System_MessageRxBufferLength;
static ubyte System_MessageTxBuffer[SYSTEM_MESSAGE_TX_BUFFER_LENGTH];
static ulong System_MessageTxBufferLength;

static ubyte Network_Buffer0x00Rx[NETWORK_BUFFER_0x00_RX_SIZE];
static ubyte Network_Buffer0x01Rx[NETWORK_BUFFER_0x01_RX_SIZE];
static ubyte Network_Buffer0x02Rx[NETWORK_BUFFER_0x02_RX_SIZE];
static ubyte Network_Buffer0x03Rx[NETWORK_BUFFER_0x03_RX_SIZE];
static ubyte Network_Buffer0x04Rx[NETWORK_BUFFER_0x04_RX_SIZE];
static ubyte Network_Buffer0x05Rx[NETWORK_BUFFER_0x05_RX_SIZE];
static ubyte Network_Buffer0x06Rx[NETWORK_BUFFER_0x06_RX_SIZE];
static ubyte Network_Buffer0x07Rx[NETWORK_BUFFER_0x07_RX_SIZE];
static ubyte Network_Buffer0x08Rx[NETWORK_BUFFER_0x08_RX_SIZE];
static ubyte Network_Buffer0x09Rx[NETWORK_BUFFER_0x09_RX_SIZE];
static ubyte Network_Buffer0x0ARx[NETWORK_BUFFER_0x0A_RX_SIZE];
static ubyte Network_Buffer0x0BRx[NETWORK_BUFFER_0x0B_RX_SIZE];
static ubyte Network_Buffer0x0CRx[NETWORK_BUFFER_0x0C_RX_SIZE];
static ubyte Network_Buffer0x0DRx[NETWORK_BUFFER_0x0D_RX_SIZE];
static ubyte Network_Buffer0x0ERx[NETWORK_BUFFER_0x0E_RX_SIZE];
static ubyte Network_Buffer0x0FRx[NETWORK_BUFFER_0x0F_RX_SIZE];
static ubyte Network_Buffer0x10Rx[NETWORK_BUFFER_0x10_RX_SIZE];
static ubyte Network_Buffer0x11Rx[NETWORK_BUFFER_0x11_RX_SIZE];
static ubyte Network_Buffer0x12Rx[NETWORK_BUFFER_0x12_RX_SIZE];
static ubyte Network_Buffer0x13Rx[NETWORK_BUFFER_0x13_RX_SIZE];

static ubyte* Network_ServicePayloadTxPtr[NETWORK_COMMAND_MAX_NUMBER];
static ulong Network_ServicePayloadTxPtrLength[NETWORK_COMMAND_MAX_NUMBER];
static ulong Network_ServiceRxMessageLength[NETWORK_COMMAND_MAX_NUMBER];
static NETWORK_SERVICE_STATUS_t Network_ServiceStatus[NETWORK_COMMAND_MAX_NUMBER];

static const NETWORK_RX_ENTRY_t Network_RxMessageEntry[NETWORK_COMMAND_MAX_NUMBER] = 
{
/* NETWORK_COMMAND_KEEP_ALIVE                */ {NETWORK_BUFFER_0x00_RX_SIZE, &Network_Buffer0x00Rx[0U]},
/* NETWORK_COMMAND_GET_EEPROM_VERSION        */ {NETWORK_BUFFER_0x01_RX_SIZE, &Network_Buffer0x01Rx[0U]},
/* NETWORK_COMMAND_GET_ALL_DATA              */ {NETWORK_BUFFER_0x02_RX_SIZE, &Network_Buffer0x02Rx[0U]},
/* NETWORK_COMMAND_SET_PUMP                  */ {NETWORK_BUFFER_0x03_RX_SIZE, &Network_Buffer0x03Rx[0U]},
/* NETWORK_COMMAND_GET_ERROR_CODE            */ {NETWORK_BUFFER_0x04_RX_SIZE, &Network_Buffer0x04Rx[0U]},
/* NETWORK_COMMAND_GET_WARNING_CODE          */ {NETWORK_BUFFER_0x05_RX_SIZE, &Network_Buffer0x05Rx[0U]},
/* NETWORK_COMMAND_CALIBRATION_DATE          */ {NETWORK_BUFFER_0x06_RX_SIZE, &Network_Buffer0x06Rx[0U]},
/* NETWORK_COMMAND_GET_MEMORY_DATA           */ {NETWORK_BUFFER_0x07_RX_SIZE, &Network_Buffer0x07Rx[0U]},
/* NETWORK_COMMAND_SET_MEMORY_DATA           */ {NETWORK_BUFFER_0x08_RX_SIZE, &Network_Buffer0x08Rx[0U]},
/* NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING */ {NETWORK_BUFFER_0x09_RX_SIZE, &Network_Buffer0x09Rx[0U]},
/* NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER */ {NETWORK_BUFFER_0x0A_RX_SIZE, &Network_Buffer0x0ARx[0U]},
/* NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER */ {NETWORK_BUFFER_0x0B_RX_SIZE, &Network_Buffer0x0BRx[0U]},
/* NETWORK_COMMAND_CLEAR_EEPROM              */ {NETWORK_BUFFER_0x0C_RX_SIZE, &Network_Buffer0x0CRx[0U]},
/* NETWORK_COMMAND_CPU_LOAD                  */ {NETWORK_BUFFER_0x0D_RX_SIZE, &Network_Buffer0x0DRx[0U]},
/* NETWORK_COMMAND_KILL_ME                   */ {NETWORK_BUFFER_0x0E_RX_SIZE, &Network_Buffer0x0ERx[0U]},
/* NETWORK_COMMAND_GET_DEBUG_DATA            */ {NETWORK_BUFFER_0x0F_RX_SIZE, &Network_Buffer0x0FRx[0U]},
/* NETWORK_COMMAND_RESET                     */ {NETWORK_BUFFER_0x10_RX_SIZE, &Network_Buffer0x10Rx[0U]},
/* NETWORK_COMMAND_VOID                      */ {NETWORK_BUFFER_0x11_RX_SIZE, &Network_Buffer0x11Rx[0U]},
/* NETM_COMMAND_MEMORY_ID_IN_DEFAULT         */ {NETWORK_BUFFER_0x12_RX_SIZE, &Network_Buffer0x12Rx[0U]},
/* NETWORK_COMMAND_SAVE_PUMP_PERCENTAGE_MIN  */ {NETWORK_BUFFER_0x13_RX_SIZE, &Network_Buffer0x13Rx[0U]},
};

static NETWORK_COMMAND_t Network_CommandPriorityList[] = 
{
    NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING,
    NETWORK_COMMAND_GET_ALL_DATA,
    NETWORK_COMMAND_GET_ERROR_CODE,
    NETWORK_COMMAND_GET_WARNING_CODE,
    NETWORK_COMMAND_SET_PUMP,
    NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER,
    NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER,
    NETWORK_COMMAND_GET_MEMORY_DATA,
    NETWORK_COMMAND_GET_EEPROM_VERSION,
    NETWORK_COMMAND_SET_MEMORY_DATA,
    NETWORK_COMMAND_CALIBRATION_DATE,
    NETWORK_COMMAND_VOID,
    //NETWORK_COMMAND_RESET,
    NETWORK_COMMAND_KEEP_ALIVE,
    //NETWORK_COMMAND_CLEAR_EEPROM,
    //NETWORK_COMMAND_KILL_ME,
    NETWORK_COMMAND_GET_DEBUG_DATA,
    NETWORK_COMMAND_CPU_LOAD,
    NETWORK_COMMAND_MEMORY_ID_IN_DEFAULT,
    NETWORK_COMMAND_SAVE_PUMP_PERCENTAGE_MIN,
};

#define NETWORK_COMMAND_PRIORITY_LIST_SIZE (sizeof(Network_CommandPriorityList) / sizeof(NETWORK_COMMAND_t))

typedef enum NETWORK_DEBUGDATA_e
{
	NETWORK_DEBUGDATA_INVC = 0U,
	NETWORK_DEBUGDATA_SENC,
	NETWORK_DEBUGDATA_NETC_SERIAL_COUNTER,
	NETWORK_DEBUGDATA_INVC_SERIAL_COUNTER,
	NETWORK_DEBUGDATA_ALL_CRC_FAULT_COUNTER,
	
	NETWORK_DEBUGDATA_MAX_NUMBER
}NETWORK_DEBUGDATA_t;

/* ****************************************************/
/* EEPROM                                             */
/* ****************************************************/
static const ubyte Network_IdStringDefault[NETWORK_ID_STRING_SIZE] = {0x20,0x20,0x20,0x20,0x20,0x20,0x20};
static ubyte Network_IdString[NETWORK_ID_STRING_SIZE];
static ubyte Network_IdStringInEeprom[NETWORK_ID_STRING_SIZE];
static ulong Network_CrcFaultCounter;
static NETWORK_STATUS_t Network_Status;

static ubyte Network_newDataMessageForWarning;
static ubyte Network_newDataMessageForError;

/* ****************************************************************************/
/* TX Command 0x03 */
static ulong Network_RawLitersMinuteSetPoint;
/* TX Command 0x0A */
static ushort Network_RawPumpPercentageSetPoint;
static ushort Network_RawPumpPercentageMin;

/* ****************************************************************************/
/* RX Command 0x01 */
static ulong Network_EepromVersion;
static ulong Network_EepromIdNumber;

/* RX Command 0x02 */
static Network_Flags_t Network_Flags;
static ushort Network_RawExternTemperatureSensor; /* offset -100.00�C a +100.00�C 20001 valori 15 bit*/
static ushort Network_RawExternDigitalBarometer;
static ushort Network_RawMeterPressure;           /* 12 bit */
static ushort Network_RawMeterTemperature; /* offset -100.00�C a +100.00�C 20001 valori 15 bit*/
static ushort Network_RawLitersMinuteRead;        /* 0.00 a 65.00 -> 13 bit MAX 81.91 L/min */
static ulong Network_RawLitersTotalFromSamplingStart; /* */
static ushort Network_RawFilterTemperature; /* offset -100.00�C a +100.00�C 20001 valori 15 bit*/
static ushort Network_RawStorageTemperature; /* offset -100.00�C a +100.00�C 20001 valori 15 bit*/
/* RX Command 0x04 */
static ushort Network_ErrorCodeRaw;
/* RX Command 0x05 */
static ushort Network_WarningCodeRaw;
/* RX Command 0x06 */

/* RX Command 0x0B */
static ushort Network_PumpPercentageReadRaw;

static ushort Network_CpuLoadRaw;

/* DEBUG DATA */
static ushort Network_RawPumpPwm;								
static ulong Network_RawImpulseTotalFromSamplingStart;			

static ushort Network_RawAdcBaro;
static ushort Network_RawAdcExtTemp;
static ushort Network_RawAdcMetTemp;
static ushort Network_RawAdcVacuum;
static ushort Network_RawAdcBatt;
static ubyte Network_RawBatteryVoltage;

static ulong Network_RawNetcTxCounter;
static ulong Network_RawNetcRxCounter;
static ulong Network_RawNetcCrcFaultCounter;
static ulong Network_RawNetcLenFaultCounter;

static ulong Network_RawInvcTxCounter;
static ulong Network_RawInvcRxCounter;
static ulong Network_RawInvcCrcFaultCounter;
static ulong Network_RawInvcLenFaultCounter;

static ushort Network_RawCrcNetcCounter;
static ushort Network_RawCrcInvcCounter;

static ushort Network_RawTestVoidLitersLost;
static ushort Network_RawTestVoidSecondsToTheEnd;
static ushort Network_RawTestVoidMeterPressure;

/* ****************************************************************************/
static ubyte Network_GrTxBuffer[NETWORK_GR_TX_BUFFER];
static ubyte Network_GrRxBuffer[NETWORK_GR_RX_BUFFER];

static ubyte Network_messageTx[512];

static NETWORK_STATE_t Network_State;
static ubyte Network_SendCommandFlags[((NETWORK_COMMAND_MAX_NUMBER - 1U) / 8U) + 1U];

static ulong Network_TimeoutCounter;
static ulong Network_TxTooBusyCounter;
static ulong Network_TxMessageCounter;
static ulong Network_RxMessageCounter;

static ubyte Network_commandOnGoing;
static NETWORK_COMMAND_t Network_CommandToSend;

static Timer_t Network_Timers[NETWORK_TIMER_MAX_NUMBER];

static ubyte Network_GetMemoryDataStatus; 
static EEPM_ID_t System_EepromIdToDownload;
static SYSTEM_STATE_t System_State;

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static SERIAL_COM_RETURN_t Network_GrTxPrepareFct(ubyte* originalMessagePtr, ulong originalMessageSize, ubyte* txMessagePtr, ulong* txMessageSize, ulong txMessageBufferSize);
static SERIAL_COM_RETURN_t Network_GrRxValidateFct(ubyte* rxMessagePtr, ulong* rxMessageSize);
static SERIAL_COM_RETURN_t Network_GrTxCallbackFct(ubyte* message, ulong messageLength);
static SERIAL_COM_RETURN_t Network_GrRxCallbackFct(ubyte* message, ulong messageLength);
static STD_RETURN_t Network_initVariable(void);
static void Netc_increaseChecksumFaultCounter(void);

static void Network_InitTimers(void);
static void Network_IncrementTimers(ulong execTimeMs);
static void Network_StartTimer(NETWORK_TIMER_t timer, ulong timerMs);
static ubyte Network_TimerIsExpired(NETWORK_TIMER_t timer);
static void Network_TimerStop(NETWORK_TIMER_t timer);
static void Network_TimerStopAll(void);
static ulong Network_TimerGetRemainingTimeMs(NETWORK_TIMER_t timer);

#ifdef NETWORK_THREAD_ENABLE
static STD_RETURN_t Network_modelInit(void);
static STD_RETURN_t Network_model(ulong execTimeMs);
#endif /* #ifdef NETWORK_THREAD_ENABLE */

/* Private functions ---------------------------------------------------------*/
static ubyte Network_TimerIsExpired(NETWORK_TIMER_t timer)
{
    ubyte isExpired = FALSE;
    if(timer < NETWORK_TIMER_MAX_NUMBER)
    {
        (void)UTIL_TimerIsExpired(&Network_Timers[timer], &isExpired);
    }
    return isExpired;
}

static ulong Network_TimerGetRemainingTimeMs(NETWORK_TIMER_t timer)
{
    ulong remainingTime = 0U;
    if(timer < NETWORK_TIMER_MAX_NUMBER)
    {
        (void)UTIL_TimerGetRemainingTimeMs(&Network_Timers[timer], &remainingTime);
    }
    return remainingTime;
} 

static void Network_InitTimers(void)
{
    ulong i;
    for(i = 0; i < NETWORK_TIMER_MAX_NUMBER; i++)
    {
        (void)UTIL_TimerInit(&Network_Timers[i]);
    }
}

static void Network_IncrementTimers(ulong execTimeMs)
{
    ulong i;
    for(i = 0; i < NETWORK_TIMER_MAX_NUMBER; i++)
    {
        (void)UTIL_TimerIncrement(&Network_Timers[i], execTimeMs);
    }
}

static void Network_StartTimer(NETWORK_TIMER_t timer, ulong timerMs)
{
    if(timer < NETWORK_TIMER_MAX_NUMBER)
    {
        (void)UTIL_TimerStart(&Network_Timers[timer], timerMs);
    }
}

static void Network_TimerStop(NETWORK_TIMER_t timer)
{
    if(timer < NETWORK_TIMER_MAX_NUMBER)
    {
        (void)UTIL_TimerStop(&Network_Timers[timer]);
        (void)UTIL_TimerInit(&Network_Timers[timer]);
    }
}

static void Network_TimerStopAll(void)
{
    NETWORK_TIMER_t timer;
    for(timer = (NETWORK_TIMER_t)0U; timer < NETWORK_TIMER_MAX_NUMBER; timer++)
    {
        Network_TimerStop(timer);
    }
}

static STD_RETURN_t Network_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ulong i;
    /* START CODE */    
    memory_copy(&Network_IdString[0], &Network_IdStringDefault[0], sizeof(Network_IdString)); /* all spaces */
    memory_copy(&Network_IdStringInEeprom[0], &Network_IdStringDefault[0], sizeof(Network_IdStringInEeprom)); /* all spaces */
    
    /* ********************************************************************************************/
    /* TX Command 0x03 */
    Network_RawLitersMinuteSetPoint = 0.0;
    /* TX Command 0x0A */
    Network_RawPumpPercentageSetPoint = 0.0;
    Network_RawPumpPercentageMin = 0;
    /* ********************************************************************************************/
    /* RX Command 0x01 */
    Network_EepromVersion = NETWORK_PENDING_32BIT_VALUE;
    Network_EepromIdNumber = NETWORK_PENDING_32BIT_VALUE;
    /* RX Command 0x02 */
    /*flags*/memory_set(&Network_Flags.data[0U], 0x00U, sizeof(Network_Flags));
    /*16bit*/Network_RawMeterPressure = NETWORK_PENDING_16BIT_VALUE;
    /*16bit*/Network_RawMeterTemperature = NETWORK_PENDING_16BIT_VALUE;
    /*16bit*/Network_RawLitersMinuteRead = NETWORK_PENDING_16BIT_VALUE;
    /*16bit*/Network_RawExternTemperatureSensor = NETWORK_PENDING_16BIT_VALUE;
    /*16bit*/Network_RawExternDigitalBarometer = NETWORK_PENDING_16BIT_VALUE;
    /*24bit*/Network_RawLitersTotalFromSamplingStart = NETWORK_PENDING_24BIT_VALUE;
    /*16bit*/Network_RawFilterTemperature = NETWORK_PENDING_16BIT_VALUE;
    /*16bit*/Network_RawStorageTemperature = NETWORK_PENDING_16BIT_VALUE;
    /* RX Command 0x04 */
    /*16bit*/Network_ErrorCodeRaw = NETWORK_PENDING_16BIT_VALUE;
    /* RX Command 0x05 */
    /*16bit*/Network_WarningCodeRaw = NETWORK_PENDING_16BIT_VALUE;
    /* RX Command 0x06 */
    
    /* RX Command 0x0B */
    /*16bit*/Network_PumpPercentageReadRaw = NETWORK_PENDING_16BIT_VALUE;

    /* RX Command 0x0D */
    /* 16bit */Network_CpuLoadRaw = NETWORK_PENDING_16BIT_VALUE;
    
    /* 16bit */ Network_RawTestVoidLitersLost = NETWORK_PENDING_16BIT_VALUE;
    /* 16bit */ Network_RawTestVoidSecondsToTheEnd = NETWORK_PENDING_16BIT_VALUE;
    /* 16bit */ Network_RawTestVoidMeterPressure = NETWORK_PENDING_16BIT_VALUE;
    

    /* ********************************************************************************************/
    Network_grPortDefault = NETWORK_GR_PORT_DEFAULT;
    memory_set(&Network_SendCommandFlags[0], 0x00, (((NETWORK_COMMAND_MAX_NUMBER - 1U) / 8U) + 1U));
    
    Network_TimeoutCounter = 0;
    Network_TxTooBusyCounter = 0;
    Network_TxMessageCounter = 0;
    Network_RxMessageCounter = 0;
    
    Network_InitTimers();    
        
    returnValue = SerialCom_initModbus(
        &Network_serialComGr, 
        /* _timeoutResponseMs   */ 250U,
        /* _maxNumberOfTimeout  */ 1U,
        /* _maxTryTxCommand     */ 10U,
        /* _txCallbackFct       */ Network_GrTxCallbackFct,
        /* _rxCallbackFct       */ Network_GrRxCallbackFct,
        /* _messageTxBufferPtr  */ &Network_GrTxBuffer[0],
        /* _messageTxBufferSize */ sizeof(Network_GrTxBuffer),
        /* _messageRxBufferPtr  */ &Network_GrRxBuffer[0],
        /* _messageRxBufferSize */ sizeof(Network_GrRxBuffer),
        /* _executionTime       */ NETWORK_THREAD_SLEEP_MS,
        /* _timerSilence        */ NETWORK_TIMER_SILENCE_MS
    );
    
    Network_State = NETWORK_STATE_INIT;
    
    for(i = 0; i < NETWORK_COMMAND_MAX_NUMBER; i++)
    {
        Network_ServiceStatus[i] = NETWORK_SERVICE_READY;
        Network_ServiceRxMessageLength[i] = 0U;
        Network_ServicePayloadTxPtr[i] = NULL;
        Network_ServicePayloadTxPtrLength[i] = 0U;
    }
    
    Network_Status = NETWORK_STATUS_NEVER_STARTED;
    
    /* DEBUG_DATA */
    Network_RawAdcBaro = 0;
    Network_RawAdcExtTemp = 0;
    Network_RawAdcMetTemp = 0;
    Network_RawAdcVacuum = 0;
    Network_RawAdcBatt = 0;
    Network_RawBatteryVoltage = 0;
    Network_RawNetcTxCounter = 0;
    Network_RawNetcRxCounter = 0;
    Network_RawNetcCrcFaultCounter = 0;
    Network_RawNetcLenFaultCounter = 0;
    Network_RawInvcTxCounter = 0;
    Network_RawInvcRxCounter = 0;
    Network_RawInvcCrcFaultCounter = 0;
    Network_RawInvcLenFaultCounter = 0;
    Network_RawCrcNetcCounter = 0;
    Network_RawCrcInvcCounter = 0;
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Network_initVariable %d", returnValue);
    }
    else
    {
        debug_print("Network_initVariable %d", returnValue);
    }
    return returnValue;
}

#define SYST_WARNING_SET_THIS_SAMPLE_BYTE_SIZE (((SYST_WARNING_MAX_NUMBER - 1U) / 8U) + 1U)

static ubyte Syst_WarningPresent[SYST_WARNING_SET_THIS_SAMPLE_BYTE_SIZE];

STD_RETURN_t Network_AddPendingWarning(SYST_WARNING_t warning)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(warning < SYST_WARNING_MAX_NUMBER)
    {
        debug_print("Network_AddPendingWarning warning %d", warning);
        UTIL_SetFlag(&Syst_WarningPresent[0U], true, warning, SYST_WARNING_MAX_NUMBER);
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Network_AddPendingWarning %d", returnValue);
    }
    return returnValue;
}

//#define NETWORK_DEBUG_TX
static SERIAL_COM_RETURN_t Network_GrTxCallbackFct(ubyte* message, ulong messageLength)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    ubyte temp[512] = {0x00U};
    ulong i = 0;
    ulong index = 0;
#ifdef NETWORK_DEBUG_TX
    index += string_snprintf(&temp[index], sizeof(temp), "TX %03d - ", (messageLength - 2U));
    /*
    while(i < (messageLength - 2))
    {
        index += string_snprintf(&temp[index], sizeof(temp) - index, "%02X ", message[i]);
        i++;
    }
    */
    debug_print("%s", temp);
#endif /* #ifdef NETWORK_DEBUG */ 

    if(messageLength > NETWORK_POSITION_COMMAND_BYTE)
    {
        if(Network_ServiceStatus[message[NETWORK_POSITION_COMMAND_BYTE]] == NETWORK_SERVICE_TX_WAITING) 
        {
            Network_ServiceStatus[message[NETWORK_POSITION_COMMAND_BYTE]] = NETWORK_SERVICE_TX_SENT_WAITING_RX;
            //debug_print("Command %d state %d", message[NETWORK_POSITION_COMMAND_BYTE], Network_ServiceStatus[message[NETWORK_POSITION_COMMAND_BYTE]]);
        }
    }
   
    return returnValue;
}

static void printBufferInHex(ubyte* message, ulong messageLength)
{
    ubyte temp[512] = {0x00U};
    ulong i = 0;
    ulong index = 0;
    index += string_snprintf(&temp[index], sizeof(temp), "%03d - ", messageLength);
    while(i < messageLength)
    {
        index += string_snprintf(&temp[index], sizeof(temp) - index, "%02X ", message[i]);
        i++;
    }
    debug_print("PRINT BUF HEX: %s", temp);
}

STD_RETURN_t NETWORK_InvalidateMessageAndData(NETWORK_COMMAND_t messageType)
{
    switch(messageType)
    {
        case NETWORK_COMMAND_VOID:
            debug_print("INVALIDATE NETWORK_COMMAND_VOID");
            /* 16bit */ Network_RawTestVoidLitersLost = 0U;
            /* 16bit */ Network_RawTestVoidSecondsToTheEnd = 0U;
            /* 16bit */ Network_RawTestVoidMeterPressure = 0U;
            Network_ServiceStatus[NETWORK_COMMAND_VOID] = NETWORK_SERVICE_READY;
            break;
        default:
            break;
    }
    return STD_RETURN_OK;
}

STD_RETURN_t NETWORK_SendMessageRequest(NETWORK_COMMAND_t messageType, ubyte* messageTxPayload, ulong messageTxLengthPayload, ubyte* messageRx, ulong* messageRxLength)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(messageType < NETWORK_COMMAND_MAX_NUMBER)
    {
        /* Check status of service, only one message a time */
        switch(Network_ServiceStatus[messageType])
        {
            case NETWORK_SERVICE_READY:
                Network_ServiceStatus[messageType] = NETWORK_SERVICE_TX_WAITING;
                /* Copy the data inside scheduler */
                Network_ServicePayloadTxPtr[messageType] = messageTxPayload;
                Network_ServicePayloadTxPtrLength[messageType] = messageTxLengthPayload;
                /* Set the flag to true, to say to the scheduler to send the message */
                UTIL_SetFlag(&Network_SendCommandFlags[0], true, messageType, NETWORK_COMMAND_MAX_NUMBER);
                /* Service busy for a new request of the same message */
                returnValue = STD_RETURN_BUSY;
                break;
            case NETWORK_SERVICE_TX_WAITING: /* This state is managed by scheduler */
                returnValue = STD_RETURN_BUSY;
                break;
            case NETWORK_SERVICE_TX_SENT_WAITING_RX: /* This state is managed by scheduler */
                returnValue = STD_RETURN_BUSY;
                break;
            case NETWORK_SERVICE_RX_ARRIVED:
                /* Copy the data outside */
                if((messageRx != NULL) && (messageRxLength != NULL))
                {
                    memory_copy(messageRx, Network_RxMessageEntry[messageType].ptr, Network_ServiceRxMessageLength[messageType]);
                    *messageRxLength = Network_ServiceRxMessageLength[messageType];
                    //memory_free(Network_ServicePayloadRxPtr[messageType]);
                }
                /* Service ready again */
                Network_ServiceStatus[messageType] = NETWORK_SERVICE_READY;
                returnValue = STD_RETURN_OK;
                break;
            default:
                returnValue = STD_RETURN_ERROR_IMPOSSIBLE_SITUATION;
                break;
        }
        //debug_print("NETWORK_SendMessageRequest %d state %d", messageType, Network_ServiceStatus[messageType]);
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

//#define NETWORK_DEBUG_RX

static SERIAL_COM_RETURN_t Network_GrRxCallbackFct(ubyte* message, ulong messageLength)
{
    SERIAL_COM_RETURN_t returnValue = SERIAL_COM_RETURN_OK;
    ubyte temp[512] = {0x00U};
    ulong i = 0;
    ulong index = 0;
    ulong ulongValue;
    ushort ushortValue;
    ubyte byteValue;
    float floatValue;
    STD_RETURN_t returnType;
    NETWORK_COMMAND_t commandResponseArrived;
#ifdef NETWORK_DEBUG_RX
    //if(message[NETWORK_POSITION_COMMAND_BYTE] == NETWORK_COMMAND_GET_EEPROM_VERSION)
    {
        index += string_snprintf(&temp[index], sizeof(temp), "RX %03d - ", messageLength);
        /*while(i < messageLength)
        {
            index += string_snprintf(&temp[index], sizeof(temp) - index, "%02X ", message[i]);
            i++;
        }*/
        debug_print("%s", temp);
    }
#endif /* NETWORK_DEBUG */    
    Network_RxMessageCounter++;
    
    if(messageLength <= NETWORK_POSITION_COMMAND_BYTE)
    {
        /* Length not enougth to understand the message */
        debug_print("Message too short");
    }
    else
    {
        commandResponseArrived = message[NETWORK_POSITION_COMMAND_BYTE];
        
        /* If my function 0x60 and a function that I really have are ok*/
        if((message[NETWORK_POSITION_FUNCTION_BYTE] == NETWORK_FUNCTION_NOT_STANDARD) && (commandResponseArrived < NETWORK_COMMAND_MAX_NUMBER))
        {
            /* If somebody ask for a message, I copy the rx data */
            if(Network_ServiceStatus[commandResponseArrived] == NETWORK_SERVICE_TX_SENT_WAITING_RX) 
            {
                //debug_print("Command %d state %d", commandResponseArrived, Network_ServiceStatus[commandResponseArrived]);
                NETWORK_RX_ENTRY_t rxEntry = Network_RxMessageEntry[commandResponseArrived];
                /* Copy data inside the buffer, but check before do a mess */
                if((messageLength > 0U) && (messageLength <= rxEntry.sizePtr))
                {
                    memory_copy(rxEntry.ptr, message, messageLength);
                    Network_ServiceRxMessageLength[commandResponseArrived] = messageLength;
                }
                Network_ServiceStatus[commandResponseArrived] = NETWORK_SERVICE_RX_ARRIVED;
                //debug_print("Command %d state %d", commandResponseArrived, Network_ServiceStatus[commandResponseArrived]);
            }
        }
    
        switch(commandResponseArrived)
        {
            case NETWORK_COMMAND_KEEP_ALIVE:
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_KEEP_ALIVE: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        debug_print("NETWORK_COMMAND_KEEP_ALIVE: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        /* Nothing */
                    }
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_KEEP_ALIVE length error");
                }
                break;
            case NETWORK_COMMAND_GET_EEPROM_VERSION:
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        Network_EepromVersion = NETWORK_PENDING_32BIT_VALUE;
                        Network_EepromIdNumber = NETWORK_PENDING_32BIT_VALUE;
                        debug_print("error NETWORK_COMMAND_GET_EEPROM_VERSION: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 5U)
                        {
                            Network_EepromVersion = 
                                    (message[NETWORK_POSITION_FIRST_DATA_BYTE + 1U] << 24U) +
                                    (message[NETWORK_POSITION_FIRST_DATA_BYTE + 2U] << 16U) +
                                    (message[NETWORK_POSITION_FIRST_DATA_BYTE + 3U] <<  8U) +
                                    (message[NETWORK_POSITION_FIRST_DATA_BYTE + 4U] <<  0U);
                            Network_EepromIdNumber =
                                    (message[NETWORK_POSITION_FIRST_DATA_BYTE + 5U] << 24U) +
                                    (message[NETWORK_POSITION_FIRST_DATA_BYTE + 6U] << 16U) +
                                    (message[NETWORK_POSITION_FIRST_DATA_BYTE + 7U] <<  8U) +
                                    (message[NETWORK_POSITION_FIRST_DATA_BYTE + 8U] <<  0U);
                            #ifdef NETWORK_DEBUG
                            debug_print("eepromVersionFrom GR is %d - id numbers is %d", Network_EepromVersion, Network_EepromIdNumber);
                            #endif
                        }
                        else
                        {
                            debug_print("error NETWORK_COMMAND_GET_EEPROM_VERSION length error");
                        }
                    }  
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_GET_EEPROM_VERSION length error");
                }
                break;
            case NETWORK_COMMAND_GET_ALL_DATA: /* 0x02 */
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_GET_ALL_DATA: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 20U)
                        {
                            /* flags */
                            memory_copy(&Network_Flags.data[0U], &message[NETWORK_POSITION_FIRST_DATA_BYTE + 1U], sizeof(Network_Flags));           
                #ifdef NETWORK_DEBUG
                            debug_print("Network_Flags.data %02X %02X", Network_Flags.data[0], Network_Flags.data[1]);
                            debug_print("FlagPumpRunning           %d", Network_Flags.flags_t.FlagPumpRunning);
                            debug_print("FlagPumpRegulation        %d", Network_Flags.flags_t.FlagPumpRegulation);
                            debug_print("MeterPressureMounted      %d", Network_Flags.flags_t.MeterPressureMounted);
                            debug_print("MeterTemperatureMounted   %d", Network_Flags.flags_t.MeterTemperatureMounted);
                            debug_print("ExternTemperatureMounted  %d", Network_Flags.flags_t.ExternTemperatureMounted);
                            debug_print("ExternPressureMounted     %d", Network_Flags.flags_t.ExternPressureMounted);
                            debug_print("InverterOrPwm             %d", Network_Flags.flags_t.InverterOrPwm);
                            debug_print("Warning                   %d", Network_Flags.flags_t.Warning);
                            debug_print("Error                     %d", Network_Flags.flags_t.Error);
                            debug_print("ExternalPower             %d", Network_Flags.flags_t.ExternalPower);
                            debug_print("BatteryVoltageCritical    %d", Network_Flags.flags_t.BatteryVoltageCritical);
                            debug_print("StorageTemperatureMounted %d", Network_Flags.flags_t.StorageTemperatureMounted);
                            debug_print("FilterTemperatureMounted  %d", Network_Flags.flags_t.FilterTemperatureMounted);
                #endif /* #ifdef NETWORK_DEBUG */
                            
                            Network_newDataMessageForWarning = true;
                            Network_newDataMessageForError = true;
                            
                            //USHORT
                            Network_RawMeterPressure = 
                                (message[NETWORK_POSITION_FIRST_DATA_BYTE + 3U] << 8U) +
                                (message[NETWORK_POSITION_FIRST_DATA_BYTE + 4U] << 0U);
                            //FLOAT
                            Network_RawMeterTemperature = 
                                (message[NETWORK_POSITION_FIRST_DATA_BYTE + 5U] << 8U) +
                                (message[NETWORK_POSITION_FIRST_DATA_BYTE + 6U] << 0U);
                            //FLOAT    
                            Network_RawLitersMinuteRead =     
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 7U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 8U] << 0U);
                            //FLOAT
                            Network_RawLitersTotalFromSamplingStart = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 9U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 10U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 11U] <<  0U);
                            //FLOAT
                            Network_RawExternTemperatureSensor = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 12U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 13U] << 0U);         
                            //USHORT
                            Network_RawExternDigitalBarometer = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 14U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 15U] << 0U); 
                            //USHORT
                            Network_RawFilterTemperature = 
                                                (message[NETWORK_POSITION_FIRST_DATA_BYTE + 16U] << 8U) +
                                                (message[NETWORK_POSITION_FIRST_DATA_BYTE + 17U] << 0U);
                            //USHORT
                            Network_RawStorageTemperature = 
                                                (message[NETWORK_POSITION_FIRST_DATA_BYTE + 18U] << 8U) +
                                                (message[NETWORK_POSITION_FIRST_DATA_BYTE + 19U] << 0U);
         
                            FLAG_Set(FLAG_TEST_FLOW_MODEL_EVENT_NEW_NETWORK_MESSAGE);
                            FLAG_Set(FLAG_ENVIRONMENTAL_MODEL_EVENT_NEW_NETWORK_DATA_MESSAGE);
                            FLAG_Set(FLAG_DUCT_MODEL_EVENT_NEW_NETWORK_DATA_MESSAGE);
                            FLAG_Set(FLAG_PM10_MODEL_EVENT_NEW_NETWORK_DATA_MESSAGE);
                            FLAG_Set(FLAG_SRB_MODEL_EVENT_NEW_NETWORK_DATA_MESSAGE);
                #ifdef NETWORK_DEBUG
                            returnType = NETWORK_GetMeterPressure(&floatValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("Meter Pres  Invalid");
                            }
                            else
                            {
                                debug_print("Meter Pres  Valid %.2f mmH2O", floatValue);
                            }
                            returnType = NETWORK_GetMeterTemperature(&floatValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("Meter Temp  Invalid");
                            }
                            else
                            {
                                debug_print("Meter Temp  Valid %.2f �C", floatValue);
                            }
                            returnType = NETWORK_GetLitersMinuteRead(&floatValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("L/min Read  Invalid");
                            }
                            else
                            {
                                debug_print("L/min Read  Valid %.2f L/min", floatValue);
                            }
                            returnType = NETWORK_GetLitersTotalFromSamplingStart(&floatValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("L/min Tot   Invalid");
                            }
                            else
                            {
                                debug_print("L/min Tot   Valid %.2f L", floatValue);
                            }
                            returnType = NETWORK_GetExternTemperatureSensor(&floatValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("Ext Temp    Invalid");
                            }
                            else
                            {
                                debug_print("Ext Temp    Valid %.2f �C", floatValue);
                            }
                            returnType = NETWORK_GetExternDigitalBarometer(&floatValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("Ext Press   Invalid");
                            }
                            else
                            {
                                debug_print("Ext Press   Valid %.2f mBar", floatValue);
                            }
                            returnType = NETWORK_GetFilterTemperature(&floatValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("Fil Temp    Invalid");
                            }
                            else
                            {
                                debug_print("Fil Temp    Valid %.2f �C", floatValue);
                            }
                            returnType = NETWORK_GetStorageTemperature(&floatValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("Sto Temp    Invalid");
                            }
                            else
                            {
                                debug_print("Sto Temp    Valid %.2f �C", floatValue);
                            }
                #endif /* NETWORK_DEBUG */
                        }
                        else
                        {
                            debug_print("error NETWORK_COMMAND_GET_ALL_DATA length error");
                        }
                    }
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_GET_ALL_DATA length error");
                }
                break;
            case NETWORK_COMMAND_SET_PUMP: /* 0x03 */
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_SET_PUMP: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        //debug_print("NETWORK_COMMAND_SET_PUMP: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        /* Nothing */
                    }
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_SET_PUMP length error");
                }
                break;
            case NETWORK_COMMAND_GET_ERROR_CODE: /* 0x04 */
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_GET_ERROR_CODE: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 3U)
                        {
                            //USHORT
                            Network_ErrorCodeRaw = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 1U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 2U] << 0U);
                            debug_print("Error detected... error code %04X", Network_ErrorCodeRaw);
                #ifdef NETWORK_DEBUG
                            returnType = NETWORK_GetErrorCodeRaw(&ushortValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("ErrorCodeRaw   Invalid");
                            }
                            else
                            {
                                debug_print("ErrorCodeRaw   Valid %d", ushortValue);
                            }
                            returnType = NETWORK_GetErrorCode((SYST_ERROR_t*)&byteValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("ErrorCode   Invalid");
                            }
                            else
                            {
                                debug_print("ErrorCode   Valid %d", byteValue);
                            }
                            
                            returnType = NETWORK_GetErrorSubCode(&byteValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("ErrorSubCode   Invalid");
                            }
                            else
                            {
                                debug_print("ErrorSubCode   Valid %d", byteValue);
                            }
                #endif /* #ifdef NETWORK_DEBUG */
                        }
                        else
                        {
                            debug_print("error NETWORK_COMMAND_GET_ERROR_CODE length error");
                        }
                    }
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_GET_ERROR_CODE length error");
                }
                break;
            case NETWORK_COMMAND_GET_WARNING_CODE: /* 0x05 */
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_GET_WARNING_CODE: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 3U)
                        {
                            //USHORT
                            Network_WarningCodeRaw = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 1U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 2U] << 0U);
                            debug_print("Warning detected... warning code %04X", Network_WarningCodeRaw);
                            Network_AddPendingWarning((SYST_WARNING_t)((Network_WarningCodeRaw & 0xFF00U) >> 8U));
                #ifdef NETWORK_DEBUG
                            returnType = NETWORK_GetWarningCodeRaw(&ushortValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("WarningCodeRaw   Invalid");
                            }
                            else
                            {
                                debug_print("WarningCodeRaw   Valid %d", ushortValue);
                            }
                            returnType = NETWORK_GetWarningCode((SYST_WARNING_t*)&byteValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("WarningCode   Invalid");
                            }
                            else
                            {
                                debug_print("WarningCode   Valid %d", byteValue);
                            }
                            
                            returnType = NETWORK_GetWarningSubCode(&byteValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("WarningSubCode   Invalid");
                            }
                            else
                            {
                                debug_print("WarningSubCode   Valid %d", byteValue);
                            }
                #endif /* #ifdef NETWORK_DEBUG */
                        }
                        else
                        {
                            debug_print("error NETWORK_COMMAND_GET_WARNING_CODE length error");
                        }
                    }
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_GET_WARNING_CODE length error");
                }
                break;
            case NETWORK_COMMAND_CALIBRATION_DATE: /* 0x06 */
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_CALIBRATION_DATE: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 7U)
                        {
                            /* Use the calibration date received by GR */
                            #ifdef NETWORK_DEBUG
                            debug_print("Calibration Data %02X%02X/%02X/%02X - %02X:%02X" , 
                                                                message[NETWORK_POSITION_FIRST_DATA_BYTE + 1U], 
                                                                message[NETWORK_POSITION_FIRST_DATA_BYTE + 2U], 
                                                                message[NETWORK_POSITION_FIRST_DATA_BYTE + 3U], 
                                                                message[NETWORK_POSITION_FIRST_DATA_BYTE + 4U], 
                                                                message[NETWORK_POSITION_FIRST_DATA_BYTE + 5U], 
                                                                message[NETWORK_POSITION_FIRST_DATA_BYTE + 6U]);
                            #endif
                        }
                    }
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_CALIBRATION_DATE length error");
                }
                break;
            case NETWORK_COMMAND_GET_MEMORY_DATA: /* 0x07 */
                if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                {
                    debug_print("error NETWORK_COMMAND_GET_MEMORY_DATA: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                }
                else
                {
                    //debug_print("NETWORK_COMMAND_GET_MEMORY_DATA: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    /* Nothing */
                }
                break;
            case NETWORK_COMMAND_SET_MEMORY_DATA:
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_SET_MEMORY_DATA: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        //debug_print("NETWORK_COMMAND_SET_MEMORY_DATA: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        /* Nothing */
                    }
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_SET_MEMORY_DATA length error");
                }
                break;
            case NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING:
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        //debug_print("NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        /* Nothing */
                    }
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING length error");
                }
                break;
            case NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER:                
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        //debug_print("NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        /* Nothing */
                    }
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER length error");
                }
                break;
            case NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER:              
                if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                {
                    if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                    {
                        debug_print("error NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                    }
                    else
                    {
                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 3U)
                        {
                            //USHORT
                            Network_PumpPercentageReadRaw = 
                                     (message[NETWORK_POSITION_FIRST_DATA_BYTE + 1U] << 8U) +
                                     (message[NETWORK_POSITION_FIRST_DATA_BYTE + 2U] << 0U);
                            #ifdef NETWORK_DEBUG
                            returnType = NETWORK_GetPumpPercentage(&floatValue);
                            if(returnType == STD_RETURN_INVALID)
                            {
                                debug_print("Pump Percentage  Invalid");
                            }
                            else
                            {
                                debug_print("Pump Percentage  Valid %.2f%%", floatValue);
                            }
                            #endif
                        }
                        else
                        {
                            debug_print("error NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER length error");
                        }
                    }  
                }
                else
                {
                    debug_print("error NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER length error");
                }
                break;
                case NETWORK_COMMAND_CLEAR_EEPROM:
                    if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                    {
                        if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                        {
                            debug_print("error NETWORK_COMMAND_CLEAR_EEPROM: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        }
                        else
                        {
                            //??????
                        }
                    }
                    else
                    {
                        debug_print("error NETWORK_COMMAND_CLEAR_EEPROM length error");
                    }
                    break;
                case NETWORK_COMMAND_CPU_LOAD:
                    if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                    {
                        if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                        {
                            debug_print("error NETWORK_COMMAND_CPU_LOAD: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        }
                        else
                        {
                            if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 3U)
                            {
                                //USHORT
                                Network_CpuLoadRaw = 
                                         (message[NETWORK_POSITION_FIRST_DATA_BYTE + 1U] << 8U) +
                                         (message[NETWORK_POSITION_FIRST_DATA_BYTE + 2U] << 0U);
                                #ifdef NETWORK_DEBUG
                                returnType = NETWORK_GetCpuLoad(&floatValue);
                                if(returnType == STD_RETURN_INVALID)
                                {
                                    debug_print("Cpu Load  Invalid");
                                }
                                else
                                {
                                    debug_print("Cpu Load  Valid %.2f%%", floatValue);
                                }
                                #endif
                            }
                            else
                            {
                                debug_print("error NETWORK_COMMAND_CPU_LOAD length error");
                            }
                        }  
                    }
                    else
                    {
                        debug_print("error NETWORK_COMMAND_CPU_LOAD length error");
                    }
                    break;
                case NETWORK_COMMAND_KILL_ME:
                    if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                    {
                        if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                        {
                            debug_print("error NETWORK_COMMAND_KILL_ME: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        }
                        else
                        {
                            //??????
                        }
                    }
                    else
                    {
                        debug_print("error NETWORK_COMMAND_KILL_ME length error");
                    }
                    break;
                case NETWORK_COMMAND_GET_DEBUG_DATA:
                    if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                    {
                        if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                        {
                            debug_print("error NETWORK_COMMAND_GET_DEBUG_DATA: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        }
                        else
                        {
                            if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 2U)
                            {
                                switch(message[NETWORK_POSITION_FIRST_DATA_BYTE + 1U])
                                {
                                    case NETWORK_DEBUGDATA_INVC:
                                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 8U)
                                        {
                                            Network_RawPumpPwm = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 2U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 3U] << 0U);
                                            Network_RawImpulseTotalFromSamplingStart = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 4U] << 24U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 5U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 6U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 7U] <<  0U);
                                        }
                                        break;
                                    case NETWORK_DEBUGDATA_SENC:
                                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 13U)
                                        {
                                            Network_RawAdcBaro = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  2U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  3U] << 0U);
                                            Network_RawAdcExtTemp = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  4U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  5U] << 0U);
                                            Network_RawAdcMetTemp = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  6U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  7U] << 0U);
                                            Network_RawAdcVacuum = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  8U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  9U] << 0U);
                                            Network_RawAdcBatt = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 10U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 11U] << 0U);
                                            Network_RawBatteryVoltage = message[NETWORK_POSITION_FIRST_DATA_BYTE + 12U];
                                        }
                                        break;
                                    case NETWORK_DEBUGDATA_NETC_SERIAL_COUNTER:
                                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 18U)
                                        {
                                            Network_RawNetcTxCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  2U] << 24U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  3U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  4U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  5U] <<  0U);
                                            Network_RawNetcRxCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  6U] << 24U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  7U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  8U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  9U] <<  0U);
                                            Network_RawNetcCrcFaultCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 10U] << 24U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 11U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 12U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 13U] <<  0U);
                                            Network_RawNetcLenFaultCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 14U] << 24U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 15U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 16U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 17U] <<  0U);
                                        }
                                        break;
                                    case NETWORK_DEBUGDATA_INVC_SERIAL_COUNTER:
                                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 18U)
                                        {
                                            Network_RawInvcTxCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  2U] << 24U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  3U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  4U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  5U] <<  0U);
                                            Network_RawInvcRxCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  6U] << 24U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  7U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  8U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  9U] <<  0U);
                                            Network_RawInvcCrcFaultCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 10U] << 24U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 11U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 12U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 13U] <<  0U);
                                            Network_RawInvcLenFaultCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 14U] << 24U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 15U] << 16U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 16U] <<  8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE + 17U] <<  0U);
                                        }
                                        break;
                                    case NETWORK_DEBUGDATA_ALL_CRC_FAULT_COUNTER:
                                        if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 6U)
                                        {
                                            Network_RawCrcNetcCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  2U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  3U] << 0U);
                                            Network_RawCrcInvcCounter = 
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  4U] << 8U) +
                                                 (message[NETWORK_POSITION_FIRST_DATA_BYTE +  5U] << 0U);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    else
                    {
                        debug_print("error NETWORK_COMMAND_GET_DEBUG_DATA length error");
                    }
                    break;
                case NETWORK_COMMAND_RESET:
                    if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                    {
                        if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                        {
                            debug_print("error NETWORK_COMMAND_RESET: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        }
                        else
                        {
                            //??????
                        }
                    }
                    else
                    {
                        debug_print("error NETWORK_COMMAND_RESET length error");
                    }
                    break;
                case NETWORK_COMMAND_VOID:
                    debug_print("NETWORK_COMMAND_VOID RX");
                    //UTIL_PrintBufferOnHex(message, messageLength);
                    if((messageLength - NETWORK_INIT_BYTE_NUMBER) >= 1U)
                    {
                        if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                        {
                            debug_print("error NETWORK_COMMAND_VOID: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        }
                        else
                        {
                            if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 7U)
                            {
                                Network_RawTestVoidLitersLost = 
                                     (message[NETWORK_POSITION_FIRST_DATA_BYTE +  1U] << 8U) +
                                     (message[NETWORK_POSITION_FIRST_DATA_BYTE +  2U] << 0U);
                                Network_RawTestVoidSecondsToTheEnd = 
                                     (message[NETWORK_POSITION_FIRST_DATA_BYTE +  3U] << 8U) +
                                     (message[NETWORK_POSITION_FIRST_DATA_BYTE +  4U] << 0U);
                                Network_RawTestVoidMeterPressure = 
                                     (message[NETWORK_POSITION_FIRST_DATA_BYTE +  5U] << 8U) +
                                     (message[NETWORK_POSITION_FIRST_DATA_BYTE +  6U] << 0U);
                                     
                                FLAG_Set(FLAG_TEST_LEAK_MODEL_EVENT_NEW_NETWORK_MESSAGE);
                            }
                        }
                    }
                    else
                    {
                        debug_print("error NETWORK_COMMAND_VOID length error");
                    }
                    break;
                case NETWORK_COMMAND_MEMORY_ID_IN_DEFAULT:
                    debug_print("NETWORK_COMMAND_MEMORY_ID_IN_DEFAULT RX");
                    break;
                case NETWORK_COMMAND_SAVE_PUMP_PERCENTAGE_MIN:
                    if((messageLength - NETWORK_INIT_BYTE_NUMBER) == 1U)
                    {
                        if(message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U] != NETM_MEMORY_RETURN_OK)
                        {
                            debug_print("error NETWORK_COMMAND_SAVE_PUMP_PERCENTAGE_MIN: %d", message[NETWORK_POSITION_FIRST_DATA_BYTE + 0U]);
                        }
                        else
                        {
                            debug_print("NETWORK_COMMAND_SAVE_PUMP_PERCENTAGE_MIN RX OK");
                        }
                    }
                    else
                    {
                        debug_print("error NETWORK_COMMAND_SAVE_PUMP_PERCENTAGE_MIN length error");
                    }
                    break;
            default:
                debug_print("Network_GrRxCallbackFct command unknown %d", temp);
                break;
        }   
    }
    return returnValue;
}

#ifdef NETWORK_THREAD_ENABLE
static STD_RETURN_t Network_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    returnValue = Network_initVariable();
    debug_print("Network ONLINE");
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Network_modelInit %d", returnValue);
    }
    else
    {
        debug_print("Network_modelInit OK");
    }    
    return returnValue;
}
#endif /* #ifdef NETWORK_THREAD_ENABLE */

#ifdef NETWORK_THREAD_ENABLE
static STD_RETURN_t System_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ulong i;
    
    for(i = 0; i < SYSTEM_MESSAGE_RX_BUFFER_LENGTH; i++)
    {
        System_MessageRxBuffer[i] = 0x00;
    }
    System_MessageRxBufferLength = 0U;
    
    for(i = 0; i < SYSTEM_MESSAGE_TX_BUFFER_LENGTH; i++)
    {
        System_MessageTxBuffer[i] = 0x00;
    }
    System_MessageTxBufferLength = 0U;
    
    System_State = SYSTEM_STATE_INIT;
    
    return returnValue;
}

static void System_GoToRunning(void)
{
    Network_StartTimer(NETWORK_TIMER_0x02_GET_ALL_DATA, NETWORK_GET_ALL_DATA_TIMER_MS);
    //Network_StartTimer(NETWORK_TIMER_0x0D_GET_CPU_LOAD, NETWORK_CPU_LOAD_TIMER_MS);
    System_State = SYSTEM_STATE_RUNNING;
}

static STD_RETURN_t System_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    STD_RETURN_t messageReturn;
    ulong eepromIdSize;
    static SYSTEM_STATE_t System_LastState = SYSTEM_STATE_INIT;
    
    if(System_State != System_LastState)
    {
        debug_print("SYS ST from %d to %d", System_LastState, System_State);
        System_LastState = System_State;
    }
    
    switch(System_State)
    {
        case SYSTEM_STATE_INIT:
            if(Network_State == NETWORK_STATE_RUNNING)
            {
                System_State = SYSTEM_STATE_GET_EEPROM_VERSION;
            }
            else
            {
                if(Network_State == NETWORK_STATE_ERROR)
                {
                    debug_print("System detect network in error, system go to error state");
                    System_State = SYSTEM_STATE_ERROR;
                }
            }
            break;
        case SYSTEM_STATE_GET_EEPROM_VERSION:
            messageReturn = NETWORK_SendMessageRequest(NETWORK_COMMAND_GET_EEPROM_VERSION, NULL, 0U ,&System_MessageRxBuffer[0], &System_MessageRxBufferLength);
            if(STD_RETURN_OK == messageReturn)
            {
                /* Message has been sent and an rx has been received*/
                printBufferInHex(&System_MessageRxBuffer[0], System_MessageRxBufferLength);
                Network_EepromVersion = (System_MessageRxBuffer[4U] << 24U) + (System_MessageRxBuffer[5U] << 16U) + (System_MessageRxBuffer[6U] << 8U) + (System_MessageRxBuffer[7U] << 0U);
                Network_EepromIdNumber = (System_MessageRxBuffer[8U] << 24U) + (System_MessageRxBuffer[9U] << 16U) + (System_MessageRxBuffer[10U] << 8U) + (System_MessageRxBuffer[11U] << 0U);
                if((Network_EepromVersion != EEPM_GetEepromVersion()) || (Network_EepromIdNumber != EEPM_ID_MAX_NUMBER))
                {
                    /* Eeprom version different between smart and GR, useless to download a different eeprom */
                    System_State = SYSTEM_STATE_CHECK_BEFORE_RUNNING;
                }
                else
                {
                    System_EepromIdToDownload = (EEPM_ID_t)0;
                    System_State = SYSTEM_STATE_DOWNLOAD_EEPROM;
                }
            }
            else
            {
                //debug_print("messageReturn %d", messageReturn);
            }
            break;
        case SYSTEM_STATE_DOWNLOAD_EEPROM:
            System_MessageTxBufferLength = 0U;
            System_MessageTxBuffer[System_MessageTxBufferLength++] = (System_EepromIdToDownload & 0xFF00U) >> 8U;
            System_MessageTxBuffer[System_MessageTxBufferLength++] = (System_EepromIdToDownload & 0x00FFU) >> 0U;  
            messageReturn = NETWORK_SendMessageRequest(NETWORK_COMMAND_GET_MEMORY_DATA, &System_MessageTxBuffer[0U], System_MessageTxBufferLength, &System_MessageRxBuffer[0], &System_MessageRxBufferLength);
            if(STD_RETURN_OK == messageReturn)
            {
                /* Message has been sent and an rx has been received*/
                //printBufferInHex(&System_MessageRxBuffer[0], System_MessageRxBufferLength);
                switch(System_MessageRxBuffer[3U]) /* Command status */
                {
                    case NETM_MEMORY_RETURN_OK:
                        eepromIdSize = EEPM_GetIdSizeInBytes(System_EepromIdToDownload);
                        //debug_print("eepromIdSize %d datasizeRx %d", eepromIdSize, (System_MessageRxBufferLength - (NETWORK_INIT_BYTE_NUMBER + 1U)));
                        if((System_MessageRxBufferLength - (NETWORK_INIT_BYTE_NUMBER + 1U)) != eepromIdSize)
                        {
                            debug_print("SYSTEM_STATE_DOWNLOAD_EEPROM Eeprom disaligned");
                            System_State = SYSTEM_STATE_ERROR;
                        }
                        else
                        {
                            memory_copy(EEPM_EepromValues[System_EepromIdToDownload].RamAddress, &System_MessageRxBuffer[4U], eepromIdSize);
                            System_EepromIdToDownload++;
                            if(System_EepromIdToDownload >= EEPM_ID_MAX_NUMBER)
                            {
                               System_State = SYSTEM_STATE_CHECK_BEFORE_RUNNING;
                            }
                        }
                        break;
                    case NETM_MEMORY_RETURN_BUSY:
                        debug_print("SYSTEM_STATE_DOWNLOAD_EEPROM Gr Memory Busy");
                        break;
                    case NETM_MEMORY_RETURN_ID_NOT_IN_LIST:
                        debug_print("SYSTEM_STATE_DOWNLOAD_EEPROM Id not in list ERROR");
                        System_State = SYSTEM_STATE_ERROR;
                        break;
                    case NETM_MEMORY_RETURN_GENERAL_ERROR:
                        debug_print("SYSTEM_STATE_DOWNLOAD_EEPROM General error from eeprom GR");
                        System_State = SYSTEM_STATE_ERROR;
                        break;
                    case NETM_MEMORY_RETURN_WRONG_LENGTH:
                        debug_print("SYSTEM_STATE_DOWNLOAD_EEPROM Command sent wrongly length");
                        System_State = SYSTEM_STATE_ERROR;
                        break;
                    case NETM_MEMORY_RETURN_PARAM_ERROR:
                        debug_print("SYSTEM_STATE_DOWNLOAD_EEPROM Command sent wrongly param");
                        System_State = SYSTEM_STATE_ERROR;
                        break;
                }
            }
            break;
        case SYSTEM_STATE_CHECK_BEFORE_RUNNING:
            debug_print("NETWORK_STATE_CHECK_BEFORE_RUNNING");
            ubyte continueToNextState = false;
            debug_print("Network_EepromVersion %d EEPM_GetEepromVersion %d", Network_EepromVersion, EEPM_GetEepromVersion());
            debug_print("Network_EepromIdNumber %d EEPM_ID_MAX_NUMBER %d", Network_EepromIdNumber, EEPM_ID_MAX_NUMBER);
            if((Network_EepromVersion != EEPM_GetEepromVersion()) || (Network_EepromIdNumber != EEPM_ID_MAX_NUMBER))
            {
                if(Network_EepromVersion != EEPM_GetEepromVersion())
                {
                    MessageScreen_openError(DYNAMIC_STRING_ERROR_EEPROM_VERSION_DIFFERENT, STD_ERROR_EEPROM_VERSION_DIFFERENT, HMI_MY_ID);
                    (void)HMI_GoToRebootPage(HMI_MY_ID);
                    System_State = SYSTEM_STATE_REQUEST_REBOOT;
                }
                if(Network_EepromIdNumber != EEPM_ID_MAX_NUMBER)
                {
                    MessageScreen_openError(DYNAMIC_STRING_ERROR_EEPROM_IDS_NUMBER_DIFFERENT, STD_ERROR_EEPROM_ID_NUMBER_DIFFERENT, HMI_MY_ID);
                    (void)HMI_GoToRebootPage(HMI_MY_ID);
                    System_State = SYSTEM_STATE_REQUEST_REBOOT;
                }
            }
            else
            {
                (void)EEPM_GetId(EEPM_ID_EEPM_PRODUCT_SERIAL_NUMBER, &Network_IdStringInEeprom[0], EEPM_VALUE_TYPE_UBYTE, 7U);
                
                if(memory_compare(&Network_IdString[0], &Network_IdStringDefault[0], sizeof(Network_IdString)) != 0)
                {
                    /* different this is not the first run */
                    if(memory_compare(&Network_IdString[0], &Network_IdStringInEeprom[0], sizeof(Network_IdString)) != 0)
                    {
                        /* Serial arrived in message is different from the previous one, reset the board after a message */
                        MessageScreen_openWarning(DYNAMIC_STRING_WARNING_DIFFERENT_SERIAL_ID_DETECTED, HMI_MY_ID);
                        (void)HMI_GoToRebootPage(HMI_MY_ID);
                        System_State = SYSTEM_STATE_REQUEST_REBOOT;
                    }
                    else
                    {
                        /* Serial arrived is equal to the previous one continue */
                        continueToNextState = true;
                    }
                }
                else
                {
                    /* this is the first run, continue */
                    memory_copy(&Network_IdString[0], &Network_IdStringInEeprom[0], sizeof(Network_IdString));
                    continueToNextState = true;
                }
            }
            
            if(continueToNextState != false)
            {
                System_State = SYSTEM_STATE_SET_PUMP_TO_ZERO;
            }
            break;
        case SYSTEM_STATE_SET_PUMP_TO_ZERO:
            System_MessageTxBufferLength = 0U;
            System_MessageTxBuffer[System_MessageTxBufferLength++] = 0U;
            System_MessageTxBuffer[System_MessageTxBufferLength++] = 0U;  
            messageReturn = NETWORK_SendMessageRequest(NETWORK_COMMAND_SET_PUMP, &System_MessageTxBuffer[0U], System_MessageTxBufferLength, &System_MessageRxBuffer[0], &System_MessageRxBufferLength);
            if(STD_RETURN_OK == messageReturn)
            {
                /* Message has been sent and an rx has been received*/
                switch(System_MessageRxBuffer[3U]) /* Command status */
                {
                    case NETM_MEMORY_RETURN_OK:
                        System_State = SYSTEM_STATE_SET_PUMP_PERCENTAGE_TO_ZERO;
                        break;
                    case NETM_MEMORY_RETURN_BUSY:
                        debug_print("SYSTEM_STATE_SET_PUMP_TO_ZERO Busy");
                        break;
                    default:
                        debug_print("SYSTEM_STATE_SET_PUMP_TO_ZERO Error %02X", System_MessageRxBuffer[3U]);
                        break;
                }
            }
            break;
        case SYSTEM_STATE_SET_PUMP_PERCENTAGE_TO_ZERO:
            System_MessageTxBufferLength = 0U;
            System_MessageTxBuffer[System_MessageTxBufferLength++] = 0U;
            System_MessageTxBuffer[System_MessageTxBufferLength++] = 0U;  
            messageReturn = NETWORK_SendMessageRequest(NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER, &System_MessageTxBuffer[0U], System_MessageTxBufferLength, &System_MessageRxBuffer[0], &System_MessageRxBufferLength);
            if(STD_RETURN_OK == messageReturn)
            {
                /* Message has been sent and an rx has been received*/
                switch(System_MessageRxBuffer[3U]) /* Command status */
                {
                    case NETM_MEMORY_RETURN_OK:
                        System_State = SYSTEM_STATE_RESET_COUNTERS;
                        break;
                    case NETM_MEMORY_RETURN_BUSY:
                        debug_print("SYSTEM_STATE_SET_PUMP_PERCENTAGE_TO_ZERO Busy");
                        break;
                    default:
                        debug_print("SYSTEM_STATE_SET_PUMP_PERCENTAGE_TO_ZERO Error %02X", System_MessageRxBuffer[3U]);
                        break;
                }
            }
            break;
        case SYSTEM_STATE_RESET_COUNTERS:
            messageReturn = NETWORK_SendMessageRequest(NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING, NULL, 0, &System_MessageRxBuffer[0], &System_MessageRxBufferLength);
            if(STD_RETURN_OK == messageReturn)
            {
                /* Message has been sent and an rx has been received*/
                switch(System_MessageRxBuffer[3U]) /* Command status */
                {
                    case NETM_MEMORY_RETURN_OK:
                        System_GoToRunning();
                        break;
                    case NETM_MEMORY_RETURN_BUSY:
                        debug_print("SYSTEM_STATE_RESET_COUNTERS Busy");
                        break;
                    default:
                        debug_print("SYSTEM_STATE_RESET_COUNTERS Error %02X", System_MessageRxBuffer[3U]);
                        break;
                }
            }
            break;
        case SYSTEM_STATE_RUNNING:
            if(Network_Status != NETWORK_STATUS_ONLINE)
            {
                debug_print("System detected network NOT online, reboot system state machine");
                System_State = SYSTEM_STATE_INIT;
            }
            break;
        case SYSTEM_STATE_REQUEST_REBOOT:
            if(HMI_GetHmiIdOnScreen() == HMI_ID_SPLASH) /* I reentered from popup*/
            {
                PLC_Reboot = true;
            }
            break;
        case SYSTEM_STATE_ERROR:
        
            break;
        default:
            break;
    }
    
    return returnValue;
}

static STD_RETURN_t Network_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SERIAL_COM_RETURN_t ret;
    static ulong indexTx = 0;
    ubyte commandSelected = false;
    ulong ulongValue;
    ubyte isExpired;
    ubyte isRunning;
    ubyte signalValue;
    ulong eepromIdSize;
    ubyte continueToNextState;
    STD_RETURN_t testReturn;
    ulong i;
    /* START CODE */

    static NETWORK_STATE_t Network_LastState = NETWORK_STATE_INIT;
    
    if(Network_State != Network_LastState)
    {
        debug_print("NET ST from %d to %d", Network_LastState, Network_State);
        Network_LastState = Network_State;
    }

    SerialCom_receiveModbus(&Network_serialComGr);
    
    Network_IncrementTimers(execTimeMs);
    
    switch(Network_State)
    {
        case NETWORK_STATE_INIT:
            Network_State = NETWORK_STATE_OPEN_PORT;
            break;
        case NETWORK_STATE_OPEN_PORT:
            ret = SerialCom_openModbus(
                                    &Network_serialComGr, 
                                    Network_grPortDefault,
                                    SERIAL_PORT_BAUDRATE_9600,
                                    SERIAL_PORT_DATA_BITS_8,
                                    SERIAL_PORT_PARITY_NONE,
                                    SERIAL_PORT_STOP_BITS_1
            );
            debug_print("SerialCom_openModbus = %d", ret);
            if(ret != SERIAL_COM_RETURN_OK)
            {
                Network_State = NETWORK_STATE_ERROR;
            }
            else
            {
                Network_State = NETWORK_STATE_RUNNING;
                Network_commandOnGoing = false;
                Network_newDataMessageForWarning = false;
                Network_newDataMessageForError = false;
                for(i = 0; i < SYST_WARNING_SET_THIS_SAMPLE_BYTE_SIZE; i++)
                {
                  //Syst_WarningSetDuringThisSample[i] = 0x00U;
                  Syst_WarningPresent[i] = 0x00;
                } 
            }
            break;
        case NETWORK_STATE_RUNNING: 
            if(Network_TimerIsExpired(NETWORK_TIMER_0x02_GET_ALL_DATA) != false)
            {
               UTIL_SetFlag(&Network_SendCommandFlags[0], true, NETWORK_COMMAND_GET_ALL_DATA, NETWORK_COMMAND_MAX_NUMBER);
               Network_StartTimer(NETWORK_TIMER_0x02_GET_ALL_DATA, NETWORK_GET_ALL_DATA_TIMER_MS);
            }
            
            if(Network_TimerIsExpired(NETWORK_TIMER_0x0D_GET_CPU_LOAD) != false)
            {
               UTIL_SetFlag(&Network_SendCommandFlags[0], true, NETWORK_COMMAND_CPU_LOAD, NETWORK_COMMAND_MAX_NUMBER);
               Network_StartTimer(NETWORK_TIMER_0x0D_GET_CPU_LOAD, NETWORK_CPU_LOAD_TIMER_MS);
            }
            
            (void)NETWORK_GetErrorFlag(&signalValue);
            if(
                (signalValue != false) && 
                (UTIL_GetFlag(&Network_SendCommandFlags[0], NETWORK_COMMAND_GET_ERROR_CODE, NETWORK_COMMAND_MAX_NUMBER) == false) &&
                (Network_newDataMessageForError != false)
            )
            {
                Network_newDataMessageForError = false;
                UTIL_SetFlag(&Network_SendCommandFlags[0], true, NETWORK_COMMAND_GET_ERROR_CODE, NETWORK_COMMAND_MAX_NUMBER);
                debug_print("Error detected... read request");
            }
            
            (void)NETWORK_GetWarningFlag(&signalValue);
            if(
                (signalValue != false) && 
                (UTIL_GetFlag(&Network_SendCommandFlags[0], NETWORK_COMMAND_GET_WARNING_CODE, NETWORK_COMMAND_MAX_NUMBER) == false) &&
                (Network_newDataMessageForWarning != false)
            )
            {
                Network_newDataMessageForWarning = false;
                UTIL_SetFlag(&Network_SendCommandFlags[0], true, NETWORK_COMMAND_GET_WARNING_CODE, NETWORK_COMMAND_MAX_NUMBER);
                debug_print("Warning detected... read request");
            }
            
            /* if a command is not ongoing, select a message to send*/
            if(Network_commandOnGoing == false)
            {
                commandSelected = false;
                /* scan all the flags to check if there is a message to send */
                for(int i = 0; (i < (NETWORK_COMMAND_PRIORITY_LIST_SIZE)) && (commandSelected == false);i++)
                {
                    if(UTIL_GetFlag(&Network_SendCommandFlags[0], Network_CommandPriorityList[i], NETWORK_COMMAND_MAX_NUMBER) != false)
                    {
                        Network_CommandToSend = Network_CommandPriorityList[i];
                        commandSelected = true;
                    }
                }

                /* if a command has been selected create it*/
                if(commandSelected == true)
                {
                    indexTx = 0U;
                    Network_messageTx[indexTx++] = NETWORK_GR_NODE_ID;
                    Network_messageTx[indexTx++] = NETWORK_FUNCTION_NOT_STANDARD;
                    Network_messageTx[indexTx++] = Network_CommandToSend;
                    
                    if(Network_CommandToSend < NETWORK_COMMAND_MAX_NUMBER)
                    {
                        /* Check if the message is a requested message */
                        if(Network_ServiceStatus[Network_CommandToSend] == NETWORK_SERVICE_TX_WAITING)
                        {
                            if((Network_ServicePayloadTxPtr[Network_CommandToSend] != NULL) && (Network_ServicePayloadTxPtrLength[Network_CommandToSend] > 0U))
                            {
                                /* payload to add */
                                for(i = 0; i < Network_ServicePayloadTxPtrLength[Network_CommandToSend]; i++)
                                {
                                    Network_messageTx[indexTx++] = (Network_ServicePayloadTxPtr[Network_CommandToSend])[i];
                                }
                            }
                            Network_commandOnGoing = true;
                        }
                        else
                        {
                            switch(Network_CommandToSend)
                            {
                                case NETWORK_COMMAND_KEEP_ALIVE:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_GET_EEPROM_VERSION:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_GET_ALL_DATA:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_SET_PUMP:                            
                                    Network_messageTx[indexTx++] = (Network_RawLitersMinuteSetPoint & 0x0000FF00U) >> 8U;
                                    Network_messageTx[indexTx++] = (Network_RawLitersMinuteSetPoint & 0x000000FFU) >> 0U;
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_GET_ERROR_CODE:
                                    debug_print("Error detected... read on going");
                                    Network_messageTx[indexTx++] = 0x00U; /* Not the same warning is possible in the same sampling */
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_GET_WARNING_CODE:
                                    debug_print("Warning detected... read on going");
                                    Network_messageTx[indexTx++] = 0x00U; /* Not the same warning is possible in the same sampling */
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_CALIBRATION_DATE:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_GET_MEMORY_DATA:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_SET_MEMORY_DATA:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER:
                                    Network_messageTx[indexTx++] = (Network_RawPumpPercentageSetPoint & 0x0000FF00U) >> 8U;
                                    Network_messageTx[indexTx++] = (Network_RawPumpPercentageSetPoint & 0x000000FFU) >> 0U;
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER:
                                    Network_commandOnGoing = true;
                                case NETWORK_COMMAND_CLEAR_EEPROM:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_CPU_LOAD:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_KILL_ME:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_GET_DEBUG_DATA:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_RESET:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_VOID:
                                    Network_commandOnGoing = true;
                                    break; 
                                case NETWORK_COMMAND_MEMORY_ID_IN_DEFAULT:
                                    Network_commandOnGoing = true;
                                    break;
                                case NETWORK_COMMAND_SAVE_PUMP_PERCENTAGE_MIN:
                                    Network_messageTx[indexTx++] = (Network_RawPumpPercentageMin & 0x0000FF00U) >> 8U;
                                    Network_messageTx[indexTx++] = (Network_RawPumpPercentageMin & 0x000000FFU) >> 0U;
                                    Network_commandOnGoing = true;
                                    break;
                            }
                        }
                    }
                }
            }

            if(Network_commandOnGoing != false)
            {
                ret = SerialCom_sendMessageModbus(&Network_serialComGr, Network_messageTx, indexTx);
                switch(ret)
                {
                    case SERIAL_COM_RETURN_OK: /* Return ok arrive only if the previous message tx has received a tx*/
                        UTIL_SetFlag(&Network_SendCommandFlags[0], false, Network_CommandToSend, NETWORK_COMMAND_MAX_NUMBER);
                        Network_TxMessageCounter++;
                        Network_StartTimer(NETWORK_TIMER_TIMEOUT_NETWORK, NETWORK_TIMEOUT_MS); /* Reset timer on each tx ok */
                        if(Network_Status != NETWORK_STATUS_ONLINE)
                        {
                            Network_Status = NETWORK_STATUS_ONLINE;
                            debug_print("Network ONLINE");
                        }
                        Network_commandOnGoing = false;
                        break;
                    case SERIAL_COM_RETURN_TIMEOUT:
                        Network_TimeoutCounter++;
                        //debug_print("Tx Timeout - Time to network timeout %d ms", Network_TimerGetRemainingTimeMs(NETWORK_TIMER_TIMEOUT_NETWORK));
                        /* Retry */
                        break;
                    case SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY:
                        Network_TxTooBusyCounter++;
                        //debug_print("SERIAL_COM_RETURN_ERROR_TX_TOO_BUSY");
                        /* Retry */
                        break;
                    default:
                        //debug_print("ret %d", ret);
                        break;
                }
            }
            
            if(Network_TimerIsExpired(NETWORK_TIMER_TIMEOUT_NETWORK) != false)
            {
                if(Network_Status == NETWORK_STATUS_ONLINE)
                {
                    Network_Status = NETWORK_STATUS_OFFLINE;
                    debug_print("Network OFFLINE");
                    Network_commandOnGoing = false; /* Stop to send actual message */
                    /* Clear message flags */
                    memory_set(&Network_SendCommandFlags[0], 0x00U, sizeof(Network_SendCommandFlags));
                    /* Clear also services state if some service is in not NETWORK_SERVICE_READY, it this way the caller will restart the service without problem*/
                    for(i = 0; i < NETWORK_COMMAND_MAX_NUMBER; i++)
                    {
                        Network_ServiceStatus[i] = NETWORK_SERVICE_READY;
                        Network_ServiceRxMessageLength[i] = 0U;
                        Network_ServicePayloadTxPtr[i] = NULL;
                        Network_ServicePayloadTxPtrLength[i] = 0U;
                    }
                    /* set the serial struct in timeout and reset its state */
                    SerialCom_SetTimeoutModbus(&Network_serialComGr);
                    /* Stop timers */
                    Network_TimerStopAll();
                }
            }
            break;
        case NETWORK_STATE_ERROR:
            break;
        default:
            break;
    }
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        //debug_print("[ERRO] Network_model %d", returnValue);
    }
    
    return returnValue;
}
#endif /* #ifdef NETWORK_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
STD_RETURN_t SYSTEM_GetEepromDownloadPercentage(float* percentage)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    if(percentage != NULL)
    {
        *percentage = (((float)(System_EepromIdToDownload * 100U)) / EEPM_ID_MAX_NUMBER);
        returnValue = STD_RETURN_OK;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_RestoreDataNewSampling(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    UTIL_SetFlag(&Network_SendCommandFlags[0], true, NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING, NETWORK_COMMAND_MAX_NUMBER);
    return returnValue;
}

/* Liter/minute ES: 30,23 l/min */
STD_RETURN_t NETWORK_SetPumpSetpoint(float pumpSetpoint)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ushort setPointMin;
    ushort setPointMax;
    float setPointMinFloat;
    float setPointMaxFloat;
    
    returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MIN, &setPointMin, EEPM_VALUE_TYPE_USHORT, 1);
    if(STD_RETURN_OK == returnValue)
    {
        returnValue = EEPM_GetId(EEPM_ID_MOTC_PUMP_SETPOINT_MAX, &setPointMax, EEPM_VALUE_TYPE_USHORT, 1);
    }
    
    if(STD_RETURN_OK == returnValue)
    {
        setPointMinFloat = ((float)(setPointMin)) / 100.0f;
        setPointMaxFloat = ((float)(setPointMax)) / 100.0f;
        if((pumpSetpoint == 0.0f) || ((pumpSetpoint >= setPointMinFloat) && (pumpSetpoint <= setPointMaxFloat)))
        {
            Network_RawLitersMinuteSetPoint = (ulong)(pumpSetpoint * 100.0f);
            UTIL_SetFlag(&Network_SendCommandFlags[0U], true, NETWORK_COMMAND_SET_PUMP, NETWORK_COMMAND_MAX_NUMBER);
        }
        else
        {
            returnValue = STD_RETURN_ERROR_PARAM;
        }
    }

    return returnValue;
}

/* Percentage power from 0,00% to 100.00% -> 0 to 10000 */
STD_RETURN_t NETWORK_SetPumpPercentageSetpoint(float pumpPercentageSetpoint)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((pumpPercentageSetpoint >= 0.0f) && (pumpPercentageSetpoint <= 100.0f))
    {
        Network_RawPumpPercentageSetPoint = (ushort)(pumpPercentageSetpoint * 100.0f);
        UTIL_SetFlag(&Network_SendCommandFlags[0], true, NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER, NETWORK_COMMAND_MAX_NUMBER);
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* Percentage power from 0,00% to 100.00% -> 0 to 10000 */
STD_RETURN_t NETWORK_SetPumpPercentageMin(float percentageMin)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((percentageMin >= 0.0f) && (percentageMin <= 100.0f))
    {
        Network_RawPumpPercentageMin = (ushort)(percentageMin * 100);
        UTIL_SetFlag(&Network_SendCommandFlags[0], true, NETWORK_COMMAND_SAVE_PUMP_PERCENTAGE_MIN, NETWORK_COMMAND_MAX_NUMBER);
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* RX Command 0x0B */
STD_RETURN_t NETWORK_StartGetPumpPercentage(void)
{
    ubyte isRunning = false;
    (void)UTIL_TimerIsRunning(&Network_Timers[NETWORK_TIMER_0x0B_GET_PUMP_PERCENTAGE], &isRunning);
    if(isRunning != false)
    {
        ; /* Nothing, keep going */
    }
    else
    {
        (void)UTIL_TimerStart(&Network_Timers[NETWORK_TIMER_0x0B_GET_PUMP_PERCENTAGE], NETWORK_REFRESH_PUMP_PERCENTAGE_READ_TIMER_MS);
    }
}

STD_RETURN_t NETWORK_StopGetPumpPercentage(void)
{
    Network_TimerStop(NETWORK_TIMER_0x0B_GET_PUMP_PERCENTAGE);
}

/* 0 = -100,00�C to 20000 = +100,00�C */
STD_RETURN_t NETWORK_GetExternTemperatureSensor(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawExternTemperatureSensor != NETWORK_INVALID_16BIT_VALUE)
        {
            /* 0 = -100,00�C to 20000 = +100,00�C */
             *floatValue = ((((float)(Network_RawExternTemperatureSensor)) - 10000.0) / 100.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* mbar 500,0 to 1200,0 */
STD_RETURN_t NETWORK_GetExternDigitalBarometer(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawExternDigitalBarometer != NETWORK_INVALID_16BIT_VALUE)
        {
             *floatValue = ((float)(Network_RawExternDigitalBarometer)) / 10.0f;
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* mbar 0,0 to 200,0 */
STD_RETURN_t NETWORK_GetMeterPressure(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawMeterPressure != NETWORK_INVALID_16BIT_VALUE)
        {
             *floatValue = ((float)(Network_RawMeterPressure)) / 10.0f;
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* 0 = -100,00�C to 20000 = +100,00�C */
STD_RETURN_t NETWORK_GetMeterTemperature(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawMeterTemperature != NETWORK_INVALID_16BIT_VALUE)
        {
            /* 0 = -100,00�C to 20000 = +100,00�C */
             *floatValue = ((((float)(Network_RawMeterTemperature)) - 10000.0) / 100.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetFilterTemperature(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawFilterTemperature != NETWORK_INVALID_16BIT_VALUE)
        {
            /* 0 = -100,00�C to 20000 = +100,00�C */
             *floatValue = ((((float)(Network_RawFilterTemperature)) - 10000.0) / 100.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetStorageTemperature(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawStorageTemperature != NETWORK_INVALID_16BIT_VALUE)
        {
            /* 0 = -100,00�C to 20000 = +100,00�C */
             *floatValue = ((((float)(Network_RawStorageTemperature)) - 10000.0) / 100.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* Liter/minute ES: 30,23 l/min */
STD_RETURN_t NETWORK_GetLitersMinuteRead(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawLitersMinuteRead != NETWORK_INVALID_16BIT_VALUE)
        {
             *floatValue = (((float)(Network_RawLitersMinuteRead)) / 100.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* Liter/minute ES: 30,23 l/min */
STD_RETURN_t NETWORK_GetLitersTotalFromSamplingStart(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawLitersTotalFromSamplingStart != NETWORK_INVALID_24BIT_VALUE)
        {
             *floatValue = (((float)(Network_RawLitersTotalFromSamplingStart)) / 100.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetPumpRunning(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.FlagPumpRunning;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetPumpRegulation(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.FlagPumpRegulation;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetFilterTemperatureMounted(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.FilterTemperatureMounted;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetStorageTemperatureMounted(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.StorageTemperatureMounted;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}


STD_RETURN_t NETWORK_GetMeterPressureMounted(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.MeterPressureMounted;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetMeterTemperatureMounted(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.MeterTemperatureMounted;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetExternTemperatureMounted(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.ExternTemperatureMounted;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetExternPressureMounted(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.ExternPressureMounted;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetInverterOrPwm(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.InverterOrPwm;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetWarningFlag(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.Warning;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetErrorFlag(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.Error;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetExternalPower(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.ExternalPower;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetBatteryVoltageCritical(ubyte* flagValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(flagValue != NULL)
    {
        *flagValue = Network_Flags.flags_t.BatteryVoltageCritical;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetIdString(sbyte* idStringBufferDest, ulong idStringBufferDestSize, ulong* idStringLength)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((idStringBufferDest != NULL) && (idStringBufferDestSize > 0U) && (idStringLength != NULL))
    {
        memory_copy(idStringBufferDest, &Network_IdString[0U], NETWORK_ID_STRING_SIZE);
        *idStringLength = NETWORK_ID_STRING_SIZE;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetErrorCodeAndSubCode(SYST_ERROR_t* errorCode, ubyte* errorSubCode, ubyte clear)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((errorCode != NULL) && (errorSubCode != NULL))
    {
        if((Network_ErrorCodeRaw != NETWORK_INVALID_16BIT_VALUE) && (Network_ErrorCodeRaw != NETWORK_PENDING_16BIT_VALUE))
        {
             *errorCode = ((Network_ErrorCodeRaw & 0xFF00U) >> 8U);
             *errorSubCode = ((Network_ErrorCodeRaw & 0x00FFU) >> 0U);
             if(clear != false)
             {
                Network_ErrorCodeRaw = 0U;
             }
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetErrorCodeRaw(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        if(Network_ErrorCodeRaw != NETWORK_INVALID_16BIT_VALUE)
        {
             *ushortValue = Network_ErrorCodeRaw;
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetErrorCode(SYST_ERROR_t* byteValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(byteValue != NULL)
    {
        if(Network_ErrorCodeRaw != NETWORK_INVALID_16BIT_VALUE)
        {
             *byteValue = ((Network_ErrorCodeRaw & 0xFF00U) >> 8U);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetErrorSubCode(ubyte* byteValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(byteValue != NULL)
    {
        if(Network_ErrorCodeRaw != NETWORK_INVALID_16BIT_VALUE)
        {
             *byteValue = ((Network_ErrorCodeRaw & 0x00FFU) >> 0U);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetWarningCodeAndSubCode(SYST_WARNING_t* warningCode, ubyte* warningSubCode, ubyte clear)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((warningCode != NULL) && (warningSubCode != NULL))
    {
        if((Network_WarningCodeRaw != NETWORK_INVALID_16BIT_VALUE) && (Network_WarningCodeRaw != NETWORK_PENDING_16BIT_VALUE))
        {
             *warningCode = ((Network_WarningCodeRaw & 0xFF00U) >> 8U);
             *warningSubCode = ((Network_WarningCodeRaw & 0x00FFU) >> 0U);
             if(clear != false)
             {
                Network_WarningCodeRaw = 0U;
             }
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetWarningCodeRaw(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        if(Network_WarningCodeRaw != NETWORK_INVALID_16BIT_VALUE)
        {
             *ushortValue = Network_WarningCodeRaw;
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_ResetWarning(void)
{
    ulong i;
    debug_print("NETWORK_ResetWarning");
    for(i = 0; i < sizeof(Syst_WarningPresent); i++)
    {
        Syst_WarningPresent[i] = 0x00;
    }
    return STD_RETURN_OK;
}

STD_RETURN_t NETWORK_GetWarningCode(SYST_WARNING_t* byteValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SYST_WARNING_t warningToReturn = SYST_WARNING_NONE;
    ulong i;
    if(byteValue != NULL)
    {
        for(i = 0; (i < SYST_WARNING_MAX_NUMBER) && (warningToReturn == SYST_WARNING_NONE); i++)
        {
            if(UTIL_GetFlag(&Syst_WarningPresent[0U], i, SYST_WARNING_MAX_NUMBER) != false)
            {
                UTIL_SetFlag(&Syst_WarningPresent[0U], false, i, SYST_WARNING_MAX_NUMBER);
                warningToReturn = i;
                debug_print("NETWORK_GetWarningCode returned %d", warningToReturn);
            }
        }
        *byteValue = warningToReturn;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

ubyte NETWORK_IsWarningPresent(void)
{
    ushort i;
    ubyte present = false;
    
    for(i = 0; (i < SYST_WARNING_MAX_NUMBER) && (present == false); i++)
    {
        if(UTIL_GetFlag(&Syst_WarningPresent[0U], i, SYST_WARNING_MAX_NUMBER) != false)
        {
            present = true;
        }
    }
  return present;
}

STD_RETURN_t NETWORK_GetWarningSubCode(ubyte* byteValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(byteValue != NULL)
    {
        if(Network_WarningCodeRaw != NETWORK_INVALID_16BIT_VALUE)
        {
             *byteValue = ((Network_WarningCodeRaw & 0x00FFU) >> 0U);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

ubyte NETWORK_IsInNoCommunicationState(void)
{
    ubyte returnValue = false;
    if(Network_Status == NETWORK_STATUS_OFFLINE)
    {
        returnValue = true;
    }
    return returnValue;
}

SYSTEM_STATE_t SYSTEM_GetState(void)
{
    return System_State;
}

/* 0.00% to 100.00% */
STD_RETURN_t NETWORK_GetPumpPercentage(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_PumpPercentageReadRaw != NETWORK_INVALID_16BIT_VALUE)
        {
             *floatValue = (((float)(Network_PumpPercentageReadRaw)) / 100.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* 0.00% to 100.00% */
STD_RETURN_t NETWORK_GetCpuLoad(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_CpuLoadRaw != NETWORK_INVALID_16BIT_VALUE)
        {
             *floatValue = (((float)(Network_CpuLoadRaw)) / 100.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* DEBUG DATA */
STD_RETURN_t NETWORK_GetPumpPwm(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        *ushortValue = Network_RawPumpPwm;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}
								
STD_RETURN_t NETWORK_GetImpulseTotalFromSamplingStart(ulong* ulongValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ulongValue != NULL)
    {
        *ulongValue = Network_RawImpulseTotalFromSamplingStart;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}			

STD_RETURN_t NETWORK_GetAdcBaro(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        *ushortValue = Network_RawAdcBaro;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetAdcExtTemp(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        *ushortValue = Network_RawAdcExtTemp;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetAdcMetTemp(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        *ushortValue = Network_RawAdcMetTemp;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetAdcVacuum(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        *ushortValue = Network_RawAdcVacuum;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetAdcBatt(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        *ushortValue = Network_RawAdcBatt;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetBatteryVoltage(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        *floatValue = (((float)(Network_RawBatteryVoltage)) / 10.0);
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetNetcTxCounter(ulong* ulongValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ulongValue != NULL)
    {
        *ulongValue = Network_RawNetcTxCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}	

STD_RETURN_t NETWORK_GetNetcRxCounter(ulong* ulongValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ulongValue != NULL)
    {
        *ulongValue = Network_RawNetcRxCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}	

STD_RETURN_t NETWORK_GetNetcCrcFaultCounter(ulong* ulongValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ulongValue != NULL)
    {
        *ulongValue = Network_RawNetcCrcFaultCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}	

STD_RETURN_t NETWORK_GetNetcLenFaultCounter(ulong* ulongValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ulongValue != NULL)
    {
        *ulongValue = Network_RawNetcLenFaultCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}	

STD_RETURN_t NETWORK_GetInvcTxCounter(ulong* ulongValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ulongValue != NULL)
    {
        *ulongValue = Network_RawInvcTxCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}	

STD_RETURN_t NETWORK_GetInvcRxCounter(ulong* ulongValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ulongValue != NULL)
    {
        *ulongValue = Network_RawInvcRxCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}	

STD_RETURN_t NETWORK_GetInvcCrcFaultCounter(ulong* ulongValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ulongValue != NULL)
    {
        *ulongValue = Network_RawInvcCrcFaultCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}	

STD_RETURN_t NETWORK_GetInvcLenFaultCounter(ulong* ulongValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ulongValue != NULL)
    {
        *ulongValue = Network_RawInvcLenFaultCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}	

STD_RETURN_t NETWORK_GetCrcNetcCounter(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        *ushortValue = Network_RawCrcNetcCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetCrcInvcCounter(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(ushortValue != NULL)
    {
        *ushortValue = Network_RawCrcInvcCounter;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetTestVoidLitersLost(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawTestVoidLitersLost != NETWORK_INVALID_16BIT_VALUE)
        {
             *floatValue = (((float)(Network_RawTestVoidLitersLost)) / 100.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetTestVoidMeterPressure(float* floatValue)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(floatValue != NULL)
    {
        if(Network_RawTestVoidMeterPressure != NETWORK_INVALID_16BIT_VALUE)
        {
             *floatValue = (((float)(Network_RawTestVoidMeterPressure)) / 10.0);
        }
        else
        {
            returnValue = STD_RETURN_INVALID;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t NETWORK_GetTestVoidSecondsToTheEnd(ushort* ushortValue)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    if(ushortValue != NULL)
    {
        switch(Network_RawTestVoidSecondsToTheEnd)
        {
            case NETWORK_INVALID_16BIT_VALUE:
                returnValue = STD_RETURN_INVALID;
                break;
            case NETWORK_PENDING_16BIT_VALUE:
                returnValue = STD_RETURN_PENDING;
                break;
            default:
                *ushortValue = Network_RawTestVoidSecondsToTheEnd;
                returnValue = STD_RETURN_OK;
                break;
        }
    }
    return returnValue;
}

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void network_init()
{
    
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
//void network()
//{
//}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void network_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void network_low_priority()
//{
//}

#ifdef NETWORK_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void network_thread()
{
    debug_print("network_thread enter");
    ulong execTime = NETWORK_THREAD_SLEEP_MS;
    
    (void)Network_modelInit();
    (void)System_modelInit();
    /* End init thread code */
    
    while(THREAD_CLOSING() != true)
    {
        /* START DO NOT REMOVE */
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef NETWORK_ENABLE_DEBUG_COUNTERS
        Network_ThreadRoutine++; /* Only for debug */
#endif
        (void)Network_model(execTime);
        (void)System_model(execTime);
    }
    debug_print("network_thread exit");
}
#endif /* #ifdef NETWORK_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
void network_close()
{
    SerialCom_closeModbus(&Network_serialComGr);
}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void network_shutdown()
//{
//}
