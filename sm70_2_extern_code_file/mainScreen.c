/*
 * main.c
 *
 *  Created on: 16/10/2019
 *      Author: Andrea Tonello
 */
#define MAIN_SCREEN_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"

/* Private define ------------------------------------------------------------*/
/* #define MAIN_ENABLE_THREAD */

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static Timer_t ManageWarningImageTimer;

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
#ifdef MAIN_ENABLE_THREAD
static void Main_screen_model_init(void);
static void Main_screen_model(ulong execTimeMs);
#endif /* MAIN_ENABLE_THREAD */

static void Main_InitVariables(void);
static void Main_ManageScreenButtons(ulong execTimeMs);
static void Main_ManageScreen(ulong execTimeMs);

static STD_RETURN_t MainScreen_logFlag(FLAG_t flag);
static STD_RETURN_t MainScreen_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t MainScreen_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t MainScreen_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t MainScreen_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t MainScreen_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t MainScreen_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t MainScreen_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t MainScreen_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t MainScreen_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static void Main_InitVariables(void)
{
    mainScreen_progressBar_bitVisible = false;
    mainScreen_progressBar_word = FLOAT_TO_WORD(0.0f);
    mainScreen_progressBar_MaximumValue = FLOAT_TO_WORD(100.0f);
    warningImage_visible = false;
    UTIL_TimerInit(&ManageWarningImageTimer);
}

static void Main_ManageScreenButtons(ulong execTimeMs)
{
    if(mainScreen_setupButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MainScreen_logFlag(FLAG_MAIN_SCREEN_BUTTON_SETUP);
    }
    if(mainScreen_clientButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MainScreen_logFlag(FLAG_MAIN_SCREEN_BUTTON_CLIENT);
    }
    if(mainScreen_memoryButton_released != false) /* These flag is reset by GUI */
    { 
        BUZM_BUTTON_SOUND();
        (void)MainScreen_logFlag(FLAG_MAIN_SCREEN_BUTTON_MEMORY);
    }
    if(mainScreen_oppButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MainScreen_logFlag(FLAG_MAIN_SCREEN_BUTTON_OOP);
    }
    if(mainScreen_infoButton_released != false) /* These flag is reset by GUI */
    { 
        BUZM_BUTTON_SOUND();
        (void)MainScreen_logFlag(FLAG_MAIN_SCREEN_BUTTON_INFO);
    }
}

void ManageDisconnetionOnScreen(void)
{
    const HMI_ID_t localHmiId = HMI_ID_MAIN;
    static SYSTEM_STATE_t lastSystemState = SYSTEM_STATE_INIT;
    SYSTEM_STATE_t systemState = SYSTEM_GetState();
    
    if(systemState != SYSTEM_STATE_RUNNING)
    {
        if(systemState == SYSTEM_STATE_DOWNLOAD_EEPROM)
        {
            mainScreen_noCommunicationString_visible = false;
            mainScreen_progressBar_bitVisible = true;
            float percentageDownload;
            if(STD_RETURN_OK == SYSTEM_GetEepromDownloadPercentage(&percentageDownload))
            {
                mainScreen_progressBar_word = FLOAT_TO_WORD(percentageDownload);
            }
        }
        else
        {
            mainScreen_noCommunicationString_visible = true;
            mainScreen_progressBar_bitVisible = false;
            /* if previously it was in running and now it is in main screen */
            if((lastSystemState == SYSTEM_STATE_RUNNING) && ((HMI_GetHmiIdOnScreen() == localHmiId) || (HMI_GetHmiIdOnScreen() == HMI_ID_SETUP)))
            {
                BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
            }
        }
        
    }
    else
    {
        mainScreen_noCommunicationString_visible = false;
        mainScreen_progressBar_bitVisible = false;
    }
    lastSystemState = systemState;
}

static void ManageWarning(ulong execTimeMs)
{
    ubyte isRunning;
    ubyte isExpired;
    
    UTIL_TimerIncrement(&ManageWarningImageTimer, execTimeMs);
    
    if(NETWORK_IsWarningPresent() != false)
    {
        //debug_print("Warning present");
        UTIL_TimerIsRunning(&ManageWarningImageTimer, &isRunning);
        if(isRunning != false)
        {
            UTIL_TimerIsExpired(&ManageWarningImageTimer, &isExpired);
            if(isExpired != false)
            {
                UTIL_TimerStop(&ManageWarningImageTimer);
                UTIL_TimerInit(&ManageWarningImageTimer);
                if(warningImage_visible != false)
                {
                    /* Immagine � visibile */
                    warningImage_visible = false;
                    UTIL_TimerStart(&ManageWarningImageTimer, 500U);
                }
                else
                {
                    /* Immagine � non visibile */
                    warningImage_visible = true;
                    UTIL_TimerStart(&ManageWarningImageTimer, 1000U);
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            warningImage_visible = true;
            UTIL_TimerStart(&ManageWarningImageTimer, 1000U);
        }
    }
    else
    {
        UTIL_TimerStop(&ManageWarningImageTimer);
        UTIL_TimerInit(&ManageWarningImageTimer);
        warningImage_visible = false;
    }
}

static STD_RETURN_t Main_RestoreDataForNewSampling(void)
{
    return NETWORK_RestoreDataNewSampling(); /* set to 0 the counters on GR */
}

static void Main_ManageScreen(ulong execTimeMs)
{
    SYSTEM_STATE_t systemState = SYSTEM_GetState();  
    const HMI_ID_t localHmiId = HMI_ID_MAIN;
    
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {

        }
        else
        {
            /* Normal code of entering */
            mainScreen_progressBar_bitVisible = false;
            mainScreen_progressBar_word = FLOAT_TO_WORD(0.0f);
            mainScreen_progressBar_MaximumValue = FLOAT_TO_WORD(100.0f);
            //Main_RestoreDataForNewSampling(); /* set to 0 the counters on GR */
        }
    }
    
    ManageDateTimeOnScreen();
    
    ManageDisconnetionOnScreen();
    
    //ManageWarning(execTimeMs);
    
    if(FLAG_GetAndReset(FLAG_MAIN_SCREEN_BUTTON_SETUP) != false)
    {
        (void)HMI_ChangeHmi(HMI_ID_SETUP, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_MAIN_SCREEN_BUTTON_CLIENT) != false)
    {
        (void)HMI_ChangeHmi(HMI_ID_CLIENT, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_MAIN_SCREEN_BUTTON_MEMORY) != false)
    {
        (void)HMI_ChangeHmi(HMI_ID_MEMORY, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_MAIN_SCREEN_BUTTON_OOP) != false)
    {
        if(systemState != SYSTEM_STATE_RUNNING)
        {
            MessageScreen_openError(DYNAMIC_STRING_ERROR_NO_INTERNAL_COMMUNICATION, STD_ERROR_NO_COMMUNICATION, localHmiId);
        }
        else
        {
            (void)HMI_ChangeHmi(HMI_ID_OPERATING_PROGRAM, localHmiId);
        }
    }
    
    if(FLAG_GetAndReset(FLAG_MAIN_SCREEN_BUTTON_INFO) != false)
    {
        (void)HMI_ChangeHmi(HMI_ID_INFO, localHmiId);
    }
    
    if(MessageScreen_isExitedHmi(localHmiId))
    {        
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {

        }
        else
        {
            /* Normal code of exit */
            
        }
    }
}

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void mainScreen_init()
{
    Main_InitVariables();
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void mainScreen()
{
    Main_NormalRoutine++; /* Only for debug */
    Main_ManageScreenButtons(Execution_Normal);
    Main_ManageScreen(Execution_Normal);
}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void mainScreen_fast()
//{
//    Main_FastRoutine++; /* Only for debug */    
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void mainScreen_low_priority()
//{
//    Main_LowPriorityRoutine++; /* Only for debug */ 
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
#ifdef MAIN_ENABLE_THREAD
static void Main_screen_model_init(void)
{

}

static void Main_screen_model(ulong execTimeMs)
{

}

void mainScreen_thread()
{
    ulong execTime = MAIN_SCREEN_THREAD_SLEEP_MS;
    /* Init thread code */
    Main_screen_model_init();

    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
        main_ThreadRoutine++; /* Only for debug */

        Main_screen_model(execTime);
    }
}
#endif
/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void mainScreen_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void mainScreen_shutdown()
//{
//}
