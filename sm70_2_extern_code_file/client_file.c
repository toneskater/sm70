/*
 * client_file.c
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */
#define CLIENT_FILE_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "client_file.h"
#include "libcsv.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define CLIENT_FILE_CSV_CLOSE_OK               1U

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/

STD_RETURN_t ClientFile_SaveFile(sbyte* fileName, _FILE_PATH destFileSystem, ulong clientMaxNumber)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int csv_fd;
    int rows;
    int i;
    /* Check parameters */
    if(
        (fileName != NULL) && 
        ( 
            (destFileSystem == LOCAL_FILE_SYSTEM) ||
            (destFileSystem == USB_1_FILE_SYSTEM) ||
            (destFileSystem == USB_2_FILE_SYSTEM) ||
            (destFileSystem == USB_3_FILE_SYSTEM) ||
            (destFileSystem == USB_4_FILE_SYSTEM)
        )
    )
    {
        int dim;
        char* str_path ;
        char* item;
        
        dim = file_get_path_size(destFileSystem);
        if(dim < 0U)
        {
            returnValue = STD_RETURN_IMPOSSIBLE_TO_GET_FILE_PATH;
        }
        else
        {
            /* Dimension file to open. */
            dim += string_length(fileName);
            str_path = memory_alloc(dim + 1U);
            /* Create the final csv path to open */
            if (file_get_path(str_path, (dim+1), destFileSystem) < 0U) 
            {
                returnValue = STD_RETURN_IMPOSSIBLE_TO_CREATE_FILE_PATH;
            }
            else
            {
                string_concatenate(str_path , fileName);
                /* Open the csv file */
                csv_fd = csv_open(str_path,';', READ_WRITE);
                if (csv_fd < 0) 
                {
                    returnValue = STD_RETURN_IMPOSSIBLE_TO_OPEN_FILE_FOR_SAVE;
                }
                else
                {
                    /* Write the content of the array as is */
                    for(i = 0; i < clientMaxNumber; i++) /* Rows contains also the header */
                    {
                        csv_write_item(csv_fd, (i + 1U), 0U, &ClientsFile_Clients[i].name[0]);
                        csv_write_item(csv_fd, (i + 1U), 1U, &ClientsFile_Clients[i].address[0]);
                        csv_write_item(csv_fd, (i + 1U), 2U, &ClientsFile_Clients[i].CAP[0]);
                        csv_write_item(csv_fd, (i + 1U), 3U, &ClientsFile_Clients[i].city[0]);
                        csv_write_item(csv_fd, (i + 1U), 4U, &ClientsFile_Clients[i].district[0]);
                        csv_write_item(csv_fd, (i + 1U), 5U, &ClientsFile_Clients[i].plantNumber[0]);
                        csv_write_item(csv_fd, (i + 1U), 6U, &ClientsFile_Clients[i].note[0]);   
                    }
                    
                    if(csv_close(csv_fd) != CLIENT_FILE_CSV_CLOSE_OK)
                    {
                        returnValue = STD_RETURN_IMPOSSIBLE_TO_CLOSE_FILE_AFTER_SAVE;
                    }
                    else
                    {
                        returnValue = STD_RETURN_OK;
                    }
                }
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t ClientFile_OpenAndReadOrCreateFile(sbyte* fileName, _FILE_PATH destFileSystem, char* headerToWrite, ulong clientMaxNumber)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int csv_fd;
    int rows;
    /* Check parameters */
    if(
        (fileName != NULL) && 
        ( 
            (destFileSystem == LOCAL_FILE_SYSTEM) ||
            (destFileSystem == USB_1_FILE_SYSTEM) ||
            (destFileSystem == USB_2_FILE_SYSTEM) ||
            (destFileSystem == USB_3_FILE_SYSTEM) ||
            (destFileSystem == USB_4_FILE_SYSTEM)
        ) &&
        (headerToWrite != NULL)
    )
    {
        int dim;
        char* str_path ;
        char* item;
        
        dim = file_get_path_size(destFileSystem);
        if(dim < 0U)
        {
            returnValue = STD_RETURN_IMPOSSIBLE_TO_GET_FILE_PATH;
        }
        else
        {
            /* Dimension file to open. */
            dim += string_length(fileName);
            str_path = memory_alloc(dim + 1U);
            /* Create the final csv path to open */
            if (file_get_path(str_path, (dim+1), destFileSystem) < 0U) 
            {
                returnValue = STD_RETURN_IMPOSSIBLE_TO_CREATE_FILE_PATH;
            }
            else
            {
                string_concatenate(str_path , fileName);
                /* Open the csv file */
                csv_fd = csv_open(str_path,';', READ_WRITE);
                if (csv_fd < 0) 
                {
                    /* debug_print("impossible to open file %s - %d", str_path, Test_csv_fd); */
                    csv_fd = csv_creat(str_path, ';');
                    if (csv_fd < 0) 
                    {
                        /*debug_print("impossible to create file %s - %d", str_path, Test_csv_fd);*/
                        returnValue = STD_RETURN_IMPOSSIBLE_TO_CREATE_FILE;
                    }
                    else
                    {
                        /* File created, write header and also the cells empty for each possible client */
                        int endI = 0;
                        int startI = 0;
                        int headerWritten = 0;
                        ubyte finish = false;
                        do
                        {
                            /* Search next separator */
                            debug_print("endI %d %c startI %d %c", endI, headerToWrite[endI], startI, headerToWrite[startI]);
                            if((headerToWrite[endI] == ';') || (headerToWrite[endI] == '\0'))
                            {
                                if(headerToWrite[endI] == '\0')
                                {
                                    finish = true;
                                }
                                if(startI < endI)
                                {
                                    /* something to write */
                                    item = memory_alloc((endI - startI) + 1U);
                                    debug_print("allocated: %d", (endI - startI) + 1U);
                                    int j;
                                    /* copy the content */
                                    for(j = 0; j < (endI - startI); j++)
                                    {
                                        item[j] = headerToWrite[startI + j];
                                        debug_print("copy: %c", headerToWrite[startI + j]);
                                    }
                                    item[j] = '\0';
                                    debug_print("to write: %s", item);
                                    csv_write_item(csv_fd, 0, headerWritten, item);
                                    
                                    memory_free(item);
                                    
                                    headerWritten++;
                                    
                                    endI++;
                                    
                                    startI = endI; 
                                    
                                    debug_print("headerWritten: %d", headerWritten);
                                }
                                else
                                {
                                    /* nothing between se*/
                                    endI++;
                                    startI++;
                                }
                            }
                            else
                            {
                                endI++;
                            }
                        }while(finish == false);

                        /* write down also the cell for all the clients, but empty */
                        sbyte valueToWrite[1] = {'\0'};
                        int j, k;
                        
                        for(j = 0; j < clientMaxNumber; j++)
                        {
                            for(k = 0; k < headerWritten; k++)
                            {
                                csv_write_item(csv_fd, (j + 1U), k, &valueToWrite[0]);
                            }
                        }

                        Client_SetClientsTotalNumber(0U);
                        Client_SetSelectedOne(0U);
                        
                        if(csv_close(csv_fd) != CLIENT_FILE_CSV_CLOSE_OK)
                        {
                            returnValue = STD_RETURN_IMPOSSIBLE_TO_CLOSE_FILE_AFTER_CREATION;
                        }
                        else
                        {
                            returnValue = STD_RETURN_OK;
                        }
                    } /* End of file creation */
                }
                else
                {
                    /* Read the content and load the clients array */
                    rows = csv_row_count(csv_fd);
                    //debug_print("Clients file row %d", rows);
                    int i = 0;
                    int sizeItem;
                    int availableClient = 0;
                    for(i = 0; (i < (rows - 1)) && (i < clientMaxNumber); i++) /* Rows contains also the header */
                    {
                        if(
                            (csv_item_size(csv_fd, (i + 1), 0U) > 0) || 
                            (csv_item_size(csv_fd, (i + 1), 1U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 2U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 3U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 4U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 5U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 6U) > 0)
                        )
                        {
                            /* In a row there is something written, it is a valide client */
                            //debug_print("Load client entry %d", i);
                            availableClient++;
                            csv_read_item(csv_fd, (i + 1U), 0U, &ClientsFile_Clients[i].name[0], CLIENT_FILE_CLIENT_NAME_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 1U, &ClientsFile_Clients[i].address[0], CLIENT_FILE_CLIENT_ADDRESS_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 2U, &ClientsFile_Clients[i].CAP[0], CLIENT_FILE_CLIENT_CAP_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 3U, &ClientsFile_Clients[i].city[0], CLIENT_FILE_CLIENT_CITY_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 4U, &ClientsFile_Clients[i].district[0], CLIENT_FILE_CLIENT_DISTRICT_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 5U, &ClientsFile_Clients[i].plantNumber[0], CLIENT_FILE_CLIENT_PLANT_NUMBER_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 6U, &ClientsFile_Clients[i].note[0], CLIENT_FILE_CLIENT_NOTE_LENTGH);   
                        }
                        else
                        {
                            //debug_print("Entry %d empty", i);
                        }
                    }
                    //debug_print("Total clients %d", availableClient);                   
                    Client_SetClientsTotalNumber(availableClient);
                    Client_SetSelectedOne(0U);
                    
                    if(csv_close(csv_fd) != CLIENT_FILE_CSV_CLOSE_OK)
                    {
                        returnValue = STD_RETURN_IMPOSSIBLE_TO_CLOSE_FILE_AFTER_READ;
                    }
                    else
                    {
                        returnValue = STD_RETURN_OK;
                    }
                }
            }
            
            memory_free(str_path);
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
//void client_file_init()
//{
//}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
//void client_file()
//{
//}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void client_file_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void client_file_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
//void client_file_thread()
//{
//}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void client_file_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void client_file_shutdown()
//{
//}
