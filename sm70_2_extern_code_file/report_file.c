/*
 * report_file.c
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */
#define REPORT_FILE_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "network.h"

/* Private define ------------------------------------------------------------*/
#define ReportFile_writeLine(...) \
{ \
    string_snprintf(&tempBuffer[0], sizeof(tempBuffer), __VA_ARGS__); \
    file_write_line(fd, &tempBuffer[0]); \
} \

#define REPORT_FILE_TEMP_BUFFER_SIZE           512U

#define REPORT_FILE_TIME_STAMP_LENGTH    20U       

#define REPORT_FILE_OPERATOR_STRING_LENGTH 20

#define REPORT_FILE_TEMP_BUFFER_DYNAMIC_STRING 50U

/* Private typedef -----------------------------------------------------------*/
typedef struct REPORT_FILE_SamplingSRBFinalData_s
{
    float lifeteckSampledVolume;
    ulong samplingTimeHours;
    ulong samplingTimeMinutes;
    ulong samplingTimeSeconds;
    float lifeteckSamplingFlowMedia;
    float temperatureMeterCounterMedia;
    float lifeteckNormalizedVolumeLiter;
    float ductTemperatureMedia;
    float staticPressureAbsolute;
    float speedMedia;
    float flowRateMedia;
    float flowRateNormalizedMedia;
    float volume_ld_t_cont;
    float volume_ld_tmedia_cont;
    float volume_tot_ld_plus_lifeteck;
    float volume_ld_t_norm;
    float volume_tot_t_norm;
    float isokineticLevel;
    SAMPLING_ERROR_t error;
    SAMPLING_STOP_CAUSE_t stopCause;
}REPORT_FILE_SamplingSRBFinalData_t;

typedef struct REPORT_FILE_StopCauseAndError_s
{
    SAMPLING_ERROR_t error;
    SAMPLING_STOP_CAUSE_t stopCause;
}REPORT_FILE_StopCauseAndError_t;

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static sbyte ReportFile_reportName[REPORT_FILE_REPORT_NAME_LENGTH];
static sbyte ReportFile_TimeStampBuffer[REPORT_FILE_TIME_STAMP_LENGTH];

static sbyte ReportFile_DynamicStringLoaderBuffer_0[REPORT_FILE_TEMP_BUFFER_DYNAMIC_STRING];
static sbyte ReportFile_DynamicStringLoaderBuffer_1[REPORT_FILE_TEMP_BUFFER_DYNAMIC_STRING];
static sbyte ReportFile_DynamicStringLoaderBuffer_2[REPORT_FILE_TEMP_BUFFER_DYNAMIC_STRING];

static const sbyte* ReportFile_InvalidString = "INVALID STR";

/* Public variables ----------------------------------------------------------*/
const sbyte* ReportFile_reportDuctShapes[SAMPLING_DUCT_SHAPE_MAX_NUMBER] = {"CIRCLE", "RECTANGULAR"};
const sbyte* ReportFile_reportMeasureUnit[SAMPLING_MEASURE_UNIT_MAX_NUMBER] = {"mmH2O", "pa"};

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t ReportFile_openFile(int* fd);
static STD_RETURN_t ReportFile_closeFile(int* fd);
static sbyte* ReportFile_getDuctShape(SAMPLING_DUCT_SHAPE_t shape);
static ulong ReportFile_getNozzleDiameter(SAMPLING_NOZZLE_DIAMETER_t nozzleDiameter);
static sbyte* ReportFile_getMeasureUnit(SAMPLING_MEASURE_UNIT_t measure_unit);
static STD_RETURN_t ReportFile_getFinalData(REPORT_FILE_SamplingSRBFinalData_t* finalData);
static STD_RETURN_t ReportFile_getSamplingData(REMOTE_SRB_SamplingData_t* samplingData);
static sbyte* ReportFile_GetTimeStampString(void);
static STD_RETURN_t ReportFile_getStopCauseAndError(REPORT_FILE_StopCauseAndError_t* stopCauseAndError);

static STD_RETURN_t ReportFile_LoadDynamicString(DYNAMIC_STRING_t dynStr, ushort strNumber, sbyte* buffer, ulong bufferSize);
static sbyte* ReportFile_GetDynamicString_0(DYNAMIC_STRING_t dynStr, ushort strNumber);
static sbyte* ReportFile_GetDynamicString_1(DYNAMIC_STRING_t dynStr, ushort strNumber);
static sbyte* ReportFile_GetDynamicString_2(DYNAMIC_STRING_t dynStr, ushort strNumber);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t ReportFile_LoadDynamicString(DYNAMIC_STRING_t dynStr, ushort strNumber, sbyte* buffer, ulong bufferSize)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte load = false;
    switch(dynStr)
    {
        case DYNAMIC_STRING_REPORT_COMMON:
            if(strNumber < DYNAMIC_STRING_REPORT_COMMON_MAX_NUMBER)
            {
                load = true;
            }
            break;
        case DYNAMIC_STRING_REPORT_ENVIRONMENTAL:
            if(strNumber < DYNAMIC_STRING_REPORT_ENVIRONMENTAL_MAX_NUMBER)
            {
                load = true;
            }
            break;
        case DYNAMIC_STRING_REPORT_DUCT:
            if(strNumber < DYNAMIC_STRING_REPORT_DUCT_MAX_NUMBER)
            {
                load = true;
            }
            break;
        case DYNAMIC_STRING_REPORT_REMOTE_SRB:
            if(strNumber < DYNAMIC_STRING_REPORT_REMOTE_SRB_MAX_NUMBER)
            {
                load = true;
            }
            else
            {
                load = false;
            }
            break;
        case DYNAMIC_STRING_REPORT_PM10:
            if(strNumber < DYNAMIC_STRING_REPORT_PM10_MAX_NUMBER)
            {
                load = true;
            }
            else
            {
                load = false;
            }
            break;
        case DYNAMIC_STRING_REPORT_REMOTE_TSB:
            if(strNumber < DYNAMIC_STRING_REPORT_REMOTE_TSB_MAX_NUMBER)
            {
                load = true;
            }
            else
            {
                load = false;
            }
            break;
        default:
            break;
    }
    if(load != false)
    {
        graphic_get_dynamic_string_text(buffer, (bufferSize - 1U), dynStr, strNumber);
        returnValue = STD_RETURN_OK;
    }
    else
    {
        memory_set(buffer, 0x00U, bufferSize);
        returnValue = STD_RETURN_ERROR_PARAM;
    } 
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_LoadDynamicString: %d - dynStr: %d - strNumber: %d", returnValue, dynStr, strNumber);
    }
    
    return returnValue;
}

static sbyte* ReportFile_GetDynamicString_0(DYNAMIC_STRING_t dynStr, ushort strNumber)
{
    sbyte* returnValue = (sbyte*)ReportFile_InvalidString;
    if(ReportFile_LoadDynamicString(dynStr, strNumber, &ReportFile_DynamicStringLoaderBuffer_0[0], sizeof(ReportFile_DynamicStringLoaderBuffer_0)) == STD_RETURN_OK)
    {
        returnValue = &ReportFile_DynamicStringLoaderBuffer_0[0];           
    }
    return returnValue;
}

static sbyte* ReportFile_GetDynamicString_1(DYNAMIC_STRING_t dynStr, ushort strNumber)
{
    sbyte* returnValue = (sbyte*)ReportFile_InvalidString;
    if(ReportFile_LoadDynamicString(dynStr, strNumber, &ReportFile_DynamicStringLoaderBuffer_1[0], sizeof(ReportFile_DynamicStringLoaderBuffer_1)) == STD_RETURN_OK)
    {
        returnValue = &ReportFile_DynamicStringLoaderBuffer_1[0];           
    }
    return returnValue;
}

static sbyte* ReportFile_GetDynamicString_2(DYNAMIC_STRING_t dynStr, ushort strNumber)
{
    sbyte* returnValue = (sbyte*)ReportFile_InvalidString;
    if(ReportFile_LoadDynamicString(dynStr, strNumber, &ReportFile_DynamicStringLoaderBuffer_2[0], sizeof(ReportFile_DynamicStringLoaderBuffer_2)) == STD_RETURN_OK)
    {
        returnValue = &ReportFile_DynamicStringLoaderBuffer_2[0];           
    }
    return returnValue;
}

static sbyte* ReportFile_GetTimeStampString(void)
{
    unsigned short year;
    unsigned char month;
    unsigned char day;
    unsigned char hour; 
    unsigned char min;
    unsigned char sec; 
    memory_set(&ReportFile_TimeStampBuffer[0], 0x00, sizeof(ReportFile_TimeStampBuffer));
    if(system_clock_read(&year, &month, &day, &hour, &min, &sec, NULL) < 0)
    {
        debug_print("ReportFile_GetTimeStampString system_clock_read failure");
    }
    else
    {
        string_snprintf(
            &ReportFile_TimeStampBuffer[0], 
            sizeof(ReportFile_TimeStampBuffer), 
            "%04d/%02d/%02d-%02d:%02d:%02d",
            year,
            month,
            day,
            hour,
            min,
            sec
        );
    }
    return &ReportFile_TimeStampBuffer[0];
}

static STD_RETURN_t ReportFile_getSamplingData(REMOTE_SRB_SamplingData_t* samplingData)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(samplingData != NULL)
    {
        /* TODO */
        REMOTE_SRB_SamplingData_t sd =
        {
            SAMPLING_MEASURE_UNIT_MMH20,
            1.0f,
            2.0f,
            3.0f,
            4.0f,
            5.0f,
            6.0f
        };

        memory_copy(samplingData, &sd, sizeof(REMOTE_SRB_SamplingData_t));
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_getSamplingData: %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t ReportFile_getStopCauseAndError(REPORT_FILE_StopCauseAndError_t* stopCauseAndError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    //debug_print("ReportFile_getStopCauseAndError ptr = %8X", stopCauseAndError);
    if(stopCauseAndError != NULL)
    {
        //debug_print("Sampling_Error %d Sampling_StopCause %d", Sampling_Error, Sampling_StopCause);
        stopCauseAndError->error = Sampling_Error;
        stopCauseAndError->stopCause = Sampling_StopCause;
        //debug_print("Sampling_Error %d Sampling_StopCause %d", stopCauseAndError->error, stopCauseAndError->stopCause);
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_getStopCauseAndError: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t ReportFile_getFinalData(REPORT_FILE_SamplingSRBFinalData_t* finalData)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SAMPLING_SamplingTime_t samplingTime;
    if(finalData != NULL)
    {
        /* TODO */
        ulong hours = 0;
        ulong minutes = 0;
        ulong seconds = 0;
        float totalVolume = 0.0f;

        (void)Sampling_GetSamplingTime(&samplingTime);

        if(NETWORK_GetLitersTotalFromSamplingStart(&totalVolume) == STD_RETURN_INVALID)
        {
        
        }

        /* TODO */
        REPORT_FILE_SamplingSRBFinalData_t srb_fd =
        {
            totalVolume,
            hours,
            minutes,
            seconds,
            2.0f,
            3.0f,
            4.0f,
            5.0f,
            6.0f,
            7.0f,
            8.0f,
            9.0f,
            10.0f,
            11.0f,
            12.0f,
            13.0f,
            14.0f,
            15.0f,
            Sampling_Error,
            Sampling_StopCause
        };

        memory_copy(finalData, &srb_fd, sizeof(REPORT_FILE_SamplingSRBFinalData_t));
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_getFinalData: %d", returnValue);
    }
    
    return returnValue;
}

static sbyte* ReportFile_getDuctShape(SAMPLING_DUCT_SHAPE_t shape)
{
    sbyte* returnValue = (sbyte*)ReportFile_reportDuctShapes[SAMPLING_DUCT_SHAPE_CIRCLE];
    if(shape < SAMPLING_DUCT_SHAPE_MAX_NUMBER)
    {
        returnValue = (sbyte*)ReportFile_reportDuctShapes[shape];
    }
    return returnValue;
}

static ulong ReportFile_getNozzleDiameter(SAMPLING_NOZZLE_DIAMETER_t nozzleDiameter)
{
    ulong returnValue = (ulong)Sampling_GetNozzleDiameter(nozzleDiameter);
    return returnValue;
}

static sbyte* ReportFile_getMeasureUnit(SAMPLING_MEASURE_UNIT_t measure_unit)
{
    sbyte* returnValue = (sbyte*)ReportFile_reportMeasureUnit[SAMPLING_MEASURE_UNIT_MMH20];
    if(measure_unit < SAMPLING_MEASURE_UNIT_MAX_NUMBER)
    {
        returnValue = (sbyte*)ReportFile_reportMeasureUnit[measure_unit];
    }
    return returnValue;
}

static STD_RETURN_t ReportFile_openFile(int* fd)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(fd != NULL)
    {
        *fd = file_open_append(&ReportFile_reportName[0], ReportFile_reportLocationSave);
        if(*fd < 0)
        {
            //debug_print("file NOT opened fd: %d\n try to create file", *fd);
            *fd = file_creat(&ReportFile_reportName[0], ReportFile_reportLocationSave);
            if(*fd < 0)
            {
                 debug_print("File NOT created fd: %d", *fd);
                 returnValue = STD_RETURN_ERROR_REPORT_NOT_CREATED;
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_openFile: %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t ReportFile_closeFile(int* fd)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(fd != NULL)
    {
        if(*fd < 0)
        {
            returnValue = STD_RETURN_ERROR_FILE_ALREADY_CLOSED;
        }
        else
        {
            if(file_close(*fd))
            {
                //debug_print("file NOT closed");
                returnValue = STD_RETURN_ERROR_REPORT_NOT_CLOSED;
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_closeFile: %d", returnValue);
    }
    return returnValue;
}

/* Public functions ----------------------------------------------------------*/
STD_RETURN_t ReportFile_createName(REPORT_NAME_CODE_t reportType)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(reportType < REPORT_NAME_CODE_MAX_NUMBER)
    {
        /* buffer length is only 34, 33 + 1 terminator */
        string_snprintf(&ReportFile_reportName[0],
                        sizeof(ReportFile_reportName),
                        "REPORT_%04d_%02d_%02d_%02d_%02d_%02d_%s.%s",
                        datarioAnno,
                        datarioMese,
                        datarioGiorno,
                        datarioOre,
                        datarioMinuti,
                        datarioSecondi,
                        ReportFile_reportNameCodes[reportType],
                        REPORT_FILE_NAME_EXTENTION
        );
        debug_print("ReportFile_createName %s", &ReportFile_reportName[0]);
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_createName: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t ReportFile_deleteReport(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    debug_print("ReportFile_deleteReport");
    
    if(returnValue == STD_RETURN_OK)
    {
        if(file_remove((sbyte*)&ReportFile_reportName[0], ReportFile_reportLocationSave) != 0)
        {
            /* error on remove */
            debug_print("ReportFile_deleteReport NOT OK");
        }
        else
        {
            /* file removed */
            debug_print("ReportFile_deleteReport OK");
        }
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_deleteReport: %d", returnValue);
    }
    
	return returnValue;
}

STD_RETURN_t ReportFile_writeStarReportHeader(REPORT_NAME_CODE_t reportType)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("ReportFile_writeStarReportHeader");
    
    returnValue = ReportFile_openFile(&fd);

    if(returnValue == STD_RETURN_OK)
    {
        switch(reportType)
        {
            case REPORT_NAME_CODE_ENVIRONMENTAL:
                ReportFile_writeLine("%s %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_HEADER), ReportFile_GetTimeStampString());
                break;
            case REPORT_NAME_CODE_PM10:
                ReportFile_writeLine("%s %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_HEADER), ReportFile_GetTimeStampString());
                break;
            case REPORT_NAME_CODE_DUCT:
                ReportFile_writeLine("%s %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_HEADER), ReportFile_GetTimeStampString());
                break;
            case REPORT_NAME_CODE_REMOTE_TSB:
                ReportFile_writeLine("%s %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_TSB, DYNAMIC_STRING_REPORT_REMOTE_TSB_HEADER), ReportFile_GetTimeStampString());
                break;
            case REPORT_NAME_CODE_REMOTE_SRB:
                ReportFile_writeLine("%s %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_HEADER), ReportFile_GetTimeStampString());
                break;
            default:
                returnValue = STD_RETURN_ERROR_PARAM;
                break;
        }
    }

    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeStarReportHeader: %d", returnValue);
    }
    
	return returnValue;
}

STD_RETURN_t ReportFile_writeInstrumentIds(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("ReportFile_writeInstrumentIds");
    
    returnValue = ReportFile_openFile(&fd);
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("[TODO] SN%06d", Get_InstrumentSerialNumber());
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeInstrumentIds: %d", returnValue);
    }
	return returnValue;
}

STD_RETURN_t ReportFile_writeClientId(void) /* anagrafica*/
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    ushort selectedOne = 0U;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("ReportFile_writeClientId");
    
    returnValue = ReportFile_openFile(&fd);
        
    if(returnValue == STD_RETURN_OK)
    {
        /* */
        if(Client_GetClientsTotalNumber() > 0)
        {
            selectedOne = Client_GetSelectedOne();
            if(selectedOne < CLIENT_FILE_CLIENTS_MAX_NUMBER)
            {
                
                ReportFile_writeLine("@%s %s-%s", 
                                    ReportFile_reportGetTag(REPORT_TAG_CLIENT_ID),
                                    ReportFile_GetTimeStampString(),
                                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_CLIENT_ID)
                );
                ReportFile_writeLine("%s: %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_NAME)        , ClientsFile_Clients[selectedOne].name);
                ReportFile_writeLine("%s: %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_ADDRESS)     , ClientsFile_Clients[selectedOne].address);
                ReportFile_writeLine("%s: %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_CAP)         , ClientsFile_Clients[selectedOne].CAP);
                ReportFile_writeLine("%s: %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_CITY)        , ClientsFile_Clients[selectedOne].city);
                ReportFile_writeLine("%s: %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_DISTRICT)    , ClientsFile_Clients[selectedOne].district);
                ReportFile_writeLine("%s: %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_PLANT_NUMBER), ClientsFile_Clients[selectedOne].plantNumber);
                ReportFile_writeLine("%s: %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_NOTE)        , ClientsFile_Clients[selectedOne].note);
            }
            else
            {
                returnValue = STD_RETURN_ERROR_CALL_WRITE_CLIENT_ID_SELECTED_OUT_OF_BOUNDS;
            }
        }
        else
        {
            ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_CLIENT_ID),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_CLIENT_ID)
            );
            ReportFile_writeLine("%s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_NO_CLIENT_SELECTED));                        
        }        
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeClientId: %d", returnValue);
    }
	return returnValue;
}

STD_RETURN_t ReportFile_writeOperatorId(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    ushort selectedOne = 0U;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("ReportFile_writeOperatorId");
    
    returnValue = ReportFile_openFile(&fd);
        
    if(returnValue == STD_RETURN_OK)
    {
        sbyte operator[REPORT_FILE_OPERATOR_STRING_LENGTH + 1U] = {0x00};
        (void)UTIL_BufferFromUShortToSByte(&parameter_operator_string_1, REPORT_FILE_OPERATOR_STRING_LENGTH, &operator[0U], REPORT_FILE_OPERATOR_STRING_LENGTH);
        ReportFile_writeLine("%s: %s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_OPERATOR), &operator[0U]);      
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeOperatorId: %d", returnValue);
    }
	return returnValue;
}

STD_RETURN_t ReportFile_writeFinalDataStopCauseAndError(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    REPORT_FILE_StopCauseAndError_t stopCauseAndError;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("ReportFile_writeFinalDataStopCauseAndError");
    
    returnValue = ReportFile_getStopCauseAndError(&stopCauseAndError);
		
	//debug_print("returnValue 1 %d", returnValue);	
		
	if(returnValue == STD_RETURN_OK)
    {
        //debug_print("ReportFile_writeFinalDataStopCauseAndError ReportFile_openFile");
        returnValue = ReportFile_openFile(&fd);
    }
    
    //debug_print("returnValue 2 %d", returnValue);
    
    if(returnValue == STD_RETURN_OK)
    {      
        switch(stopCauseAndError.stopCause)
        {
            case SAMPLING_STOP_CAUSE_TIME:
                ReportFile_writeLine("%s: %s", 
                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_STOP_CAUSE), 
                    ReportFile_GetDynamicString_1(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_TIME_REACHED)
                );
                break;
            case SAMPLING_STOP_CAUSE_VOLUME:
                ReportFile_writeLine("%s: %s", 
                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_STOP_CAUSE), 
                    ReportFile_GetDynamicString_1(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_VOLUME_REACHED)
                );
                break;
            case SAMPLING_STOP_CAUSE_MANUAL:
                ReportFile_writeLine("%s: %s", 
                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_STOP_CAUSE), 
                    ReportFile_GetDynamicString_1(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_MANUAL_STOP)
                );
                break;
            case SAMPLING_STOP_CAUSE_ERROR:
                ReportFile_writeLine("%s: %s", 
                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_STOP_CAUSE), 
                    ReportFile_GetDynamicString_1(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_ERROR_DETECTED)
                );
                break;
        }
        
        switch(stopCauseAndError.error)
        {
            case SAMPLING_ERROR_NONE:
                ReportFile_writeLine("%s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_COMPLETED_REGULARLY));
                break;
            case SAMPLING_ERROR_NO_COMMUNICATION:
                ReportFile_writeLine("%s %s", 
                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_STOPPED_DUE_TO), 
                    ReportFile_GetDynamicString_1(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SENSOR_FAULT)
                );
                ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_CODE), stopCauseAndError.error);
                break;
            case SAMPLING_ERROR_CLOGGED_FILTER:
                ReportFile_writeLine("%s %s", 
                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_STOPPED_DUE_TO), 
                    ReportFile_GetDynamicString_1(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_CLOGGED_FILTER)
                );
                ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_CODE), stopCauseAndError.error);
                break;
            default:
                ReportFile_writeLine("%s %s", 
                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_STOPPED_DUE_TO), 
                    ReportFile_GetDynamicString_1(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_UNKNOWN_ERROR)
                );
                ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_CODE), stopCauseAndError.error);
                break;
        }
    }
    
    //debug_print("returnValue 3 %d", returnValue);
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeFinalDataStopCauseAndError: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t ReportFile_writeDelayedStart(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeDelayedStart");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_DELAYED_START),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_DELAYED_STARTED)

        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeDelayedStart: %d", returnValue);
    }
	return returnValue;
}

STD_RETURN_t ReportFile_writeH24DelayedStart(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeH24DelayedStart");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_DELAYED_START),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_H24_DELAYED_STARTED)

        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeH24DelayedStart: %d", returnValue);
    }
	return returnValue;
}

STD_RETURN_t ReportFile_writeStart(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};

	debug_print("ReportFile_writeStart");
		
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_START),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_START_SAMP)
        );
    }
    
        if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeStart: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t ReportFile_writeStartForced(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};

	debug_print("ReportFile_writeStartForced");
		
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_START_FORCED),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_START_SAMP_FORCED)
        );
    }
    
        if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeStartForced: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t ReportFile_writeDelayedPaused(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeDelayedPaused");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_DELAYED_PAUSE),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_DELAYED_PAUSE)
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeDelayedPaused: %d", returnValue);
    }		
	return returnValue;
}

STD_RETURN_t ReportFile_writeRunningPaused(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeRunningPaused");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_RUNNING_PAUSE),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_PAUSE)
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeRunningPaused: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t ReportFile_write5MinutesData(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
    REMOTE_SRB_SamplingData_t samplingData;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_write5MinutesData");
		
	returnValue = ReportFile_getSamplingData(&samplingData);	
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-SAMPLING DATA", 
                                ReportFile_reportGetTag(REPORT_TAG_FIVE_MINUTES_DATA),
                                ReportFile_GetTimeStampString());
        ReportFile_writeLine("%s; t_cond=%.1f; l=%.2f;", ReportFile_getMeasureUnit(samplingData.measureUnit), samplingData.t_cont, samplingData.l);
        ReportFile_writeLine("p_d=%.2f; p_s=%.2f; t_c=%.2f; v=%.2f", samplingData.p_d, samplingData.p_s, samplingData.t_c, samplingData.v);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_write5MinutesData: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t ReportFile_writeDelayedResumed(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeDelayedResumed");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_DELAYED_RESUME),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_DELAYED_RESUMED)
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeDelayedResumed: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t ReportFile_writeRunningResumed(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeRunningResumed");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_RUNNING_RESUMED),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_SAMPLING_RESUMED)
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeRunningResumed: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t ReportFile_writeNozzleDiameterChanged(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeNozzleDiameterChanged");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_NOZZLE_DIAMETER_CHANGED),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_NOZZLE_CHANGED)
        );
        ReportFile_writeLine("%s MM: %s %d %s %d", 
                        ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_DIAMETER),
                        ReportFile_GetDynamicString_1(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_FROM),
                        ReportFile_getNozzleDiameter(ReportFile_lastNozzleDiameter), 
                        ReportFile_GetDynamicString_2(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_TO),
                        ReportFile_getNozzleDiameter(ReportFile_actualNozzleDiameter)
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeNozzleDiameterChanged: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t ReportFile_writeAspirationFlowChanged(float newAspirationFlow, float lastAspirationFlow)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeAspirationFlowChanged");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_ASPIRATION_FLOW_CHANGED),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_ASP_FLOW_CHANGED)
        );
        ReportFile_writeLine("%s: %s %.2f %s %.2f",
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_FLOW),
                                ReportFile_GetDynamicString_1(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_FROM),
                                lastAspirationFlow, 
                                ReportFile_GetDynamicString_2(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_TO),
                                newAspirationFlow
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeAspirationFlowChanged: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t ReportFile_writeWarning(SYST_WARNING_t warningCode, ubyte warningSubCode)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeWarning");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s %03d;%03d", 
                                ReportFile_reportGetTag(REPORT_TAG_WARNING),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_WARNING),
                                warningCode,
                                warningSubCode
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeWarning: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t ReportFile_writeError(SYST_ERROR_t errorCode, ubyte errorSubCode)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeError");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s %03d;%03d", 
                                ReportFile_reportGetTag(REPORT_TAG_ERROR),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_ERROR),
                                errorCode,
                                errorSubCode
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeError: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t ReportFile_writePausedNoPowerSupply(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writePausedNoPowerSupply");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_NO_POWER_SUPPLY),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_RUNNING_PAUSED_NO_POWER_SUPPLY)
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writePausedNoPowerSupply: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t ReportFile_writeRestartFromNoPowerSupply(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("ReportFile_writeRestartFromNoPowerSupply");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        ReportFile_writeLine("@%s %s-%s", 
                                ReportFile_reportGetTag(REPORT_TAG_POWER_SUPPLY_BACK),
                                ReportFile_GetTimeStampString(),
                                ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_COMMON, DYNAMIC_STRING_REPORT_COMMON_RUNNING_RESUMED_FROM_NO_POWER_SUPPLY)
        );
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = ReportFile_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeRestartFromNoPowerSupply: %d", returnValue);
    }
    return returnValue;
}

/* ************************************************************************************************/
/* ENVIRONMENTAL                                                                                  */
/* ************************************************************************************************/
STD_RETURN_t ReportFile_writeInitDataEnvironmental(ENVIRONMENTAL_InterfaceData_t* interfaceData)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("ReportFile_writeInitDataEnvironmental");
    
    if(interfaceData != NULL)
    {
    	returnValue = ReportFile_openFile(&fd);

		if(returnValue == STD_RETURN_OK)
		{
            ReportFile_writeLine("@%s %s-%s",
									ReportFile_reportGetTag(REPORT_TAG_INIT_DATA),
									ReportFile_GetTimeStampString(),
									ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_INIT_DATA)
            );
			switch(interfaceData->samplingType)
			{
                case SAMPLING_ISOKINETIC_SAMPLING_TIME:
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_SAMPLING_BY_TIME_MIN), interfaceData->time);
                    break;
                case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_SAMPLING_BY_VOLUME_L), interfaceData->volume);
                    break;
			}
			ReportFile_writeLine("%s L/min: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_ASPIRATION_FLOW), interfaceData->aspirationFlow);
			ReportFile_writeLine("%s L/min: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_STOP_FLOW_SAMPLING), interfaceData->aspirationFlowError);
			ReportFile_writeLine("%s: %d"        , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_START_DELAY_MIN), interfaceData->samplingStartDelayMinutes);
			ReportFile_writeLine("%s �C: %d"     , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_NORMALIZATION_TEMP), interfaceData->normalizationTemperature);
        }

		if(returnValue == STD_RETURN_OK)
		{
			returnValue = ReportFile_closeFile(&fd);
		}
		if(returnValue != STD_RETURN_OK)
		{
			debug_print("[ERRO] ReportFile_writeInitDataEnvironmental: %d", returnValue);
		}
    }
    else
    {
    	returnValue = STD_RETURN_ERROR_PARAM;
    }
	return returnValue;
}

STD_RETURN_t ReportFile_writeFinalDataEnvironmental(ENVIRONMENTAL_FinalData_t* finalData)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    
    debug_print("ReportFile_writeFinalDataEnvironmental");
    
    if(finalData != NULL)
    {
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = ReportFile_openFile(&fd);
        }
        
        if(returnValue == STD_RETURN_OK)
        {
            ReportFile_writeLine("@%s %s-%s", 
                                    ReportFile_reportGetTag(REPORT_TAG_FINAL_DATA),
                                    ReportFile_GetTimeStampString(),
                                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_FINAL_DATA)
            );
            ReportFile_writeLine("%s = %.2f"      , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_SAMPLING_LENGTH_MIN), finalData->samplingLenghtMin);
            ReportFile_writeLine("%s L/MIN = %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_AVERAGE_FLOW)       , finalData->averageFlow);
            ReportFile_writeLine("%s �C = %.2f"   , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_AVERAGE_TEMPERATURE), finalData->averageTemperature);
            ReportFile_writeLine("%s L = %.2f"    , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_ASPIRATED_LITERS)   , finalData->aspiratedLiters);
            ReportFile_writeLine("%s L = %.2f"    , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_ENVIRONMENTAL, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_NORMALIZED_LITERS)  , finalData->normalizedLiters);
        }
        
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = ReportFile_closeFile(&fd);
        }
    }
	else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeFinalDataEnvironmental: %d", returnValue);
    }	
	return returnValue;
}

/* ************************************************************************************************/
/* PM10                                                                                           */
/* ************************************************************************************************/
STD_RETURN_t ReportFile_writeInitDataPm10(PM10_InterfaceData_t* interfaceData)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("ReportFile_writeInitDataPm10");
    
    if(interfaceData != NULL)
    {
        returnValue = ReportFile_openFile(&fd);

		if(returnValue == STD_RETURN_OK)
		{
            ReportFile_writeLine("@%s %s-%s",
									ReportFile_reportGetTag(REPORT_TAG_INIT_DATA),
									ReportFile_GetTimeStampString(),
									ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_ENVIRONMENTAL_INIT_DATA)
            );
			switch(interfaceData->samplingType)
			{
                case SAMPLING_ISOKINETIC_SAMPLING_TIME:
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_SAMPLING_BY_TIME_MIN), interfaceData->time);
                    break;
                case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_SAMPLING_BY_VOLUME_L), interfaceData->volume);
                    break;
			}
			ReportFile_writeLine("%s L/min: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_ASPIRATION_FLOW), interfaceData->aspirationFlow);
			ReportFile_writeLine("%s L/min: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_STOP_FLOW_SAMPLING), interfaceData->aspirationFlowError);
			ReportFile_writeLine("%s: %d"        , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_START_DELAY_MIN), interfaceData->samplingStartDelayMinutes);
			ReportFile_writeLine("%s �C: %d"     , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_NORMALIZATION_TEMP), interfaceData->normalizationTemperature);
            if(interfaceData->h24Enable != false)
            {
                ReportFile_writeLine("%s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_H24_ENABLED));
            }
            else
            {
                ReportFile_writeLine("%s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_H24_DISABLED));
            }
            ReportFile_writeLine("%s MIN: %d"    , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_SERIAL_LOG_TIME_MIN), interfaceData->serialLogTineMin);
        }

		if(returnValue == STD_RETURN_OK)
		{
			returnValue = ReportFile_closeFile(&fd);
		}
		if(returnValue != STD_RETURN_OK)
		{
			debug_print("[ERRO] ReportFile_writeInitDataEnvironmental: %d", returnValue);
		}
    }
    else
    {
    	returnValue = STD_RETURN_ERROR_PARAM;
    }
	return returnValue;
}

STD_RETURN_t ReportFile_writeFinalDataPm10(PM10_FinalData_t* finalData)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    
    debug_print("ReportFile_writeFinalDataPm10");
    
    if(finalData != NULL)
    {
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = ReportFile_openFile(&fd);
        }
        
        if(returnValue == STD_RETURN_OK)
        {
            ReportFile_writeLine("@%s %s-%s", 
                                    ReportFile_reportGetTag(REPORT_TAG_FINAL_DATA),
                                    ReportFile_GetTimeStampString(),
                                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_FINAL_DATA)
            );
            ReportFile_writeLine("%s = %.2f"      , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_SAMPLING_LENGTH_MIN), finalData->samplingLenghtMin);
            ReportFile_writeLine("%s L/MIN = %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_AVERAGE_FLOW)       , finalData->averageFlow);
            ReportFile_writeLine("%s �C = %.2f"   , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_AVERAGE_TEMPERATURE), finalData->averageTemperature);
            ReportFile_writeLine("%s L = %.2f"    , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_ASPIRATED_LITERS)   , finalData->aspiratedLiters);
            ReportFile_writeLine("%s L = %.2f"    , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_PM10, DYNAMIC_STRING_REPORT_PM10_NORMALIZED_LITERS)  , finalData->normalizedLiters);
        }
        
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = ReportFile_closeFile(&fd);
        }
    }
	else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeFinalDataPm10: %d", returnValue);
    }	
	return returnValue;
}

/* ************************************************************************************************/
/* DUCT                                                                                           */
/* ************************************************************************************************/
STD_RETURN_t ReportFile_writeInitDataDuct(DUCT_InterfaceData_t* interfaceData)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("ReportFile_writeInitDataDuct");
    
    if(interfaceData != NULL)
    {
    	returnValue = ReportFile_openFile(&fd);

		if(returnValue == STD_RETURN_OK)
		{
			ReportFile_writeLine("@%s %s-INIT DATA",
									ReportFile_reportGetTag(REPORT_TAG_INIT_DATA),
									ReportFile_GetTimeStampString(),
                                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_INIT_DATA)
            );
			switch(interfaceData->samplingType)
			{
                case SAMPLING_ISOKINETIC_SAMPLING_TIME:
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_SAMPLING_BY_TIME_MIN), interfaceData->timeMin);
                    break;
                case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_SAMPLING_BY_VOLUME_L), interfaceData->volumeL);
                    break;
			}
			switch(interfaceData->ductShape)
			{
                case SAMPLING_DUCT_SHAPE_CIRCLE:
                    ReportFile_writeLine("%s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_SHAPE_CIRCLE));
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_DUCT_DIAMETER_CM), interfaceData->diameterCm);
                    break;
                case SAMPLING_DUCT_SHAPE_RECTANGULAR:
                    ReportFile_writeLine("%s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_SHAPE_RECTANGULAR));
                    ReportFile_writeLine("%s: %d x %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_DUCT_SIDES_CM), interfaceData->sideMajorCm, interfaceData->sideMinorCm);
                    break;
			}
			ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_START_DELAY_MIN), interfaceData->samplingStartDelayMinutes);
			ReportFile_writeLine("%s: %.3f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_COSTANT_MEASURE_TUBE), interfaceData->kCostant);
			ReportFile_writeLine("%s KG/M3: %.3f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_GAS_DENSITY), interfaceData->gasDensity);
			ReportFile_writeLine("%s %%: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_MOISTURE), interfaceData->condensePerc);
            ReportFile_writeLine("%s HPA: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_BAROMETRIC_PRESSURE), (interfaceData->barometricPressurePa/100U));
			ReportFile_writeLine("%s �C: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_NORMALIZATION_TEMPERATURE), interfaceData->normalizationTemperatureCelsius);
            ReportFile_writeLine("%s �C: %.1f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_STACK_TEMPERATURE), interfaceData->ductTemperatureCelsius);
            switch(interfaceData->measureUnit)
			{
                case SAMPLING_MEASURE_UNIT_MMH20:
                    ReportFile_writeLine("%s MMH2O: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_STATIC_PRESSURE), PA_TO_MMH2O(interfaceData->staticPressurePa));
                    ReportFile_writeLine("%s MMH2O: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_DIFFERENTIAL_PRESSURE), PA_TO_MMH2O(interfaceData->differentialPressurePa));
                    break;
                case SAMPLING_MEASURE_UNIT_PA:
                    ReportFile_writeLine("%s PA: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_STATIC_PRESSURE), interfaceData->staticPressurePa);
                    ReportFile_writeLine("%s PA: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_DIFFERENTIAL_PRESSURE), interfaceData->differentialPressurePa);
                    break;
			}
			ReportFile_writeLine("%s MM: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_NOZZLE_DIAMETER), interfaceData->nozzleDiameterMm);
			switch(interfaceData->isokinetiFlow)
			{
                case SAMPLING_ISOKINETIC_FLOW_AUTOMATIC:
                    ReportFile_writeLine("%s L/Min %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_ISOKINETIC_FLOW_AUTOMATIC), interfaceData->isoFlow);
                    break;
                case SAMPLING_ISOKINETIC_FLOW_MANUAL:
                    ReportFile_writeLine("%s L/Min %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_ISOKINETIC_FLOW_MANUAL), interfaceData->isokinetiFlowManualValueLMin);
                    break;
            }
			ReportFile_writeLine("%s: %.3f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_ISOKINETIC_WALL_EFFECT_FWA), interfaceData->fwa);
        }

		if(returnValue == STD_RETURN_OK)
		{
			returnValue = ReportFile_closeFile(&fd);
		}
		if(returnValue != STD_RETURN_OK)
		{
			debug_print("[ERRO] ReportFile_writeInitDataDuct: %d", returnValue);
		}
    }
    else
    {
    	returnValue = STD_RETURN_ERROR_PARAM;
    }
	return returnValue;
}

STD_RETURN_t ReportFile_writeFinalDataDuct(DUCT_FinalData_t* finalData)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    
    debug_print("ReportFile_writeFinalDataDuct");
    
    if(finalData != NULL)
    {
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = ReportFile_openFile(&fd);
        }
        
        if(returnValue == STD_RETURN_OK)
        {
            ReportFile_writeLine("@%s %s-%s", 
                                    ReportFile_reportGetTag(REPORT_TAG_FINAL_DATA),
                                    ReportFile_GetTimeStampString(),
                                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_FINAL_DATA)
            );
            ReportFile_writeLine("%s L = %.2f"    , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_SAMPLED_LITERS)     , finalData->aspiratedLiters);
            ReportFile_writeLine("%s MIN = %.2f"  , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_SAMPLING_LENGHT)    , finalData->samplingLenghtMin);
            ReportFile_writeLine("%s L/MIN = %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_AVERAGE_FLOW)       , finalData->averageFlowLMin);
            ReportFile_writeLine("%s �C = %.2f"   , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_AVERAGE_TEMP_DRYGAS), finalData->averageTempDryGasCelsius);
            ReportFile_writeLine("%s L = %.2f"    , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_NORMALIZED_LITERS)  , finalData->normalizedLiters);
            ReportFile_writeLine("%s �C = %.2f"   , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_AVERAGE_TEMP_STACK) , finalData->averageTemperatureStackCelsius);
            ReportFile_writeLine("%s HPA = %.2f"  , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_ABSOLUT_STATIC_PRES), finalData->averageAbsoluteStaticPressureHPa);
            ReportFile_writeLine("%s M/SEC = %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_AVERAGE_SPEED_FWA)  , finalData->averageSpeedFwa_msec);
            ReportFile_writeLine("%s M3/H = %.2f" , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_AVERAGE_F_RATE_S)   , finalData->averageFlowRateS_m3h);
            ReportFile_writeLine("%s NM3/H = %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_AVERAGE_NORM_QS)    , finalData->averageNormQs_m3h);
            ReportFile_writeLine("%s %% = %.2f"   , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_DUCT, DYNAMIC_STRING_REPORT_DUCT_ISOKINETIC_GRADE)   , finalData->isokineticGradePercentual);
        }
        
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = ReportFile_closeFile(&fd);
        }
    }
	else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeFinalDataDuct: %d", returnValue);
    }	
	return returnValue;
}

/* ************************************************************************************************/
/* SRB                                                                                            */
/* ************************************************************************************************/
STD_RETURN_t ReportFile_writeInitDataSrb(SRB_InterfaceData_t* interfaceData)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("ReportFile_writeInitDataSrb");
    
    if(interfaceData != NULL)
    {
    	returnValue = ReportFile_openFile(&fd);

		if(returnValue == STD_RETURN_OK)
		{
			ReportFile_writeLine("@%s %s-INIT DATA",
									ReportFile_reportGetTag(REPORT_TAG_INIT_DATA),
									ReportFile_GetTimeStampString(),
                                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_INIT_DATA)
            );
			switch(interfaceData->samplingType)
			{
                case SAMPLING_ISOKINETIC_SAMPLING_TIME:
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SAMPLING_BY_TIME_MIN), interfaceData->timeMin);
                    break;
                case SAMPLING_ISOKINETIC_SAMPLING_VOLUME:
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SAMPLING_BY_VOLUME_L), interfaceData->volumeL);
                    break;
			}
			switch(interfaceData->ductShape)
			{
                case SAMPLING_DUCT_SHAPE_CIRCLE:
                    ReportFile_writeLine("%s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SHAPE_CIRCLE));
                    ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_DIAMETER_CM), interfaceData->diameterCm);
                    break;
                case SAMPLING_DUCT_SHAPE_RECTANGULAR:
                    ReportFile_writeLine("%s", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SHAPE_RECTANGULAR));
                    ReportFile_writeLine("%s: %d x %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SIDES_CM), interfaceData->sideMajorCm, interfaceData->sideMinorCm);
                    break;
			}
			ReportFile_writeLine("%s: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_START_DELAY_MIN), interfaceData->samplingStartDelayMinutes);
			ReportFile_writeLine("%s: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SIDESTREAM_FLOW), interfaceData->sidestreamFlowLiterMin);
			ReportFile_writeLine("%s: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SIDESTREAM_DGM), interfaceData->sidestreamDGMCelsius);
			ReportFile_writeLine("%s: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SIDESTREAM_FLOW_REAL), interfaceData->sidestreamFlowCalculated);
			ReportFile_writeLine("%s: %.3f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_COSTANT_MEASURE_TUBE), interfaceData->kCostant);
			ReportFile_writeLine("%s KG/M3: %.3f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_GAS_DENSITY), interfaceData->gasDensity);
			ReportFile_writeLine("%s %%: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_MOISTURE), interfaceData->condensePerc);
            ReportFile_writeLine("%s HPA: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_BAROMETRIC_PRESSURE), (interfaceData->barometricPressurePa/100U));
			ReportFile_writeLine("%s �C: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_NORMALIZATION_TEMPERATURE), interfaceData->normalizationTemperatureCelsius);
            ReportFile_writeLine("%s �C: %.1f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_STACK_TEMPERATURE), interfaceData->ductTemperatureCelsius);
            switch(interfaceData->measureUnit)
			{
                case SAMPLING_MEASURE_UNIT_MMH20:
                    ReportFile_writeLine("%s MMH2O: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_STATIC_PRESSURE), PA_TO_MMH2O(interfaceData->staticPressurePa));
                    ReportFile_writeLine("%s MMH2O: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_DIFFERENTIAL_PRESSURE), PA_TO_MMH2O(interfaceData->differentialPressurePa));
                    break;
                case SAMPLING_MEASURE_UNIT_PA:
                    ReportFile_writeLine("%s PA: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_STATIC_PRESSURE), interfaceData->staticPressurePa);
                    ReportFile_writeLine("%s PA: %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_DIFFERENTIAL_PRESSURE), interfaceData->differentialPressurePa);
                    break;
			}
			ReportFile_writeLine("%s MM: %d", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_NOZZLE_DIAMETER), interfaceData->nozzleDiameterMm);
			switch(interfaceData->isokinetiFlow)
			{
                case SAMPLING_ISOKINETIC_FLOW_AUTOMATIC:
                    ReportFile_writeLine("%s L/Min %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_ISOKINETIC_FLOW_AUTOMATIC), interfaceData->isoFlow);
                    break;
                case SAMPLING_ISOKINETIC_FLOW_MANUAL:
                    ReportFile_writeLine("%s L/Min %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_ISOKINETIC_FLOW_MANUAL), interfaceData->isokinetiFlowManualValueLMin);
                    break;
            }
			ReportFile_writeLine("%s: %.3f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_ISOKINETIC_WALL_EFFECT_FWA), interfaceData->fwa);
        }

		if(returnValue == STD_RETURN_OK)
		{
			returnValue = ReportFile_closeFile(&fd);
		}
		if(returnValue != STD_RETURN_OK)
		{
			debug_print("[ERRO] ReportFile_writeInitDataSrb: %d", returnValue);
		}
    }
    else
    {
    	returnValue = STD_RETURN_ERROR_PARAM;
    }
	return returnValue;
}

STD_RETURN_t ReportFile_writeFinalDataSrb(SRB_FinalData_t* finalData)
{
STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[REPORT_FILE_TEMP_BUFFER_SIZE] = {0x00U};
    
    debug_print("ReportFile_writeFinalDataSrb");
    
    if(finalData != NULL)
    {
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = ReportFile_openFile(&fd);
        }
        
        if(returnValue == STD_RETURN_OK)
        {
            ReportFile_writeLine("@%s %s-%s", 
                                    ReportFile_reportGetTag(REPORT_TAG_FINAL_DATA),
                                    ReportFile_GetTimeStampString(),
                                    ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_FINAL_DATA)
            );
            ReportFile_writeLine("%s L = %.2f"    , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SAMPLED_LITERS)     , finalData->aspiratedLiters);
            ReportFile_writeLine("%s MIN = %.2f"  , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_SAMPLING_LENGHT)    , finalData->samplingLenghtMin);
            ReportFile_writeLine("%s L/MIN = %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_AVERAGE_FLOW)       , finalData->averageFlowLMin);
            ReportFile_writeLine("%s �C = %.2f"   , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_AVERAGE_TEMP_DRYGAS), finalData->averageTempDryGasCelsius);
            ReportFile_writeLine("%s L = %.2f"    , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_NORMALIZED_LITERS)  , finalData->normalizedLiters);
            ReportFile_writeLine("%s �C = %.2f"   , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_AVERAGE_TEMP_STACK) , finalData->averageTemperatureStackCelsius);
            ReportFile_writeLine("%s HPA = %.2f"  , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_ABSOLUT_STATIC_PRES), finalData->averageAbsoluteStaticPressureHPa);
            ReportFile_writeLine("%s M/SEC = %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_AVERAGE_SPEED_FWA)  , finalData->averageSpeedFwa_msec);
            ReportFile_writeLine("%s M3/H = %.2f" , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_AVERAGE_F_RATE_S)   , finalData->averageFlowRateS_m3h);
            ReportFile_writeLine("%s NM3/H = %.2f", ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_AVERAGE_NORM_QS)    , finalData->averageNormQs_m3h);
            ReportFile_writeLine("%s %% = %.2f"   , ReportFile_GetDynamicString_0(DYNAMIC_STRING_REPORT_REMOTE_SRB, DYNAMIC_STRING_REPORT_REMOTE_SRB_ISOKINETIC_GRADE)   , finalData->isokineticGradePercentual);
        }
        
        if(returnValue == STD_RETURN_OK)
        {
            returnValue = ReportFile_closeFile(&fd);
        }
    }
	else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] ReportFile_writeFinalDataSrb: %d", returnValue);
    }	
	return returnValue;
}

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
//void report_file_init()
//{
//}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
//void report_file()
//{
//}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void report_file_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void report_file_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
//void report_file_thread()
//{
//}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void report_file_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void report_file_shutdown()
//{
//}
