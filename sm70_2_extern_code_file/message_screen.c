/*
 * message_screen.c
 *
 *  Created on: 06/11/2019
 *      Author: Andrea Tonello
 */
#define MESSAGE_SCREEN_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "message_screen.h"
#include "logger.h"

/* Private define ------------------------------------------------------------*/
#define MESSAGE_SCREEN_MESSAGE_TEXT_SIZE 500U
#define MESSAGE_SCREEN_TITLE_TEXT_SIZE 20U
#define MESSAGE_SCREEN_NO_TEXT "No text"
/* #define MESSAGE_SCREEN_ENABLE_THREAD */

#define HMI_ID_FLAGS_SIZE (((HMI_ID_MAX_NUMBER - 1U) / 8U) + 1U)

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static ubyte MessageScreen_exitByPopup;
static HMI_ID_t MessageScreen_hmi_id_source;
static char MessageScreen_hmi_page_source;
static ubyte MessageScreen_forceRedrawOnReenterSource;
static ubyte Hmi_HMIIDVariablesLoaded;

static ulong HMI_CheckScreenCounter;
static ubyte HMI_EnterFlags[HMI_ID_FLAGS_SIZE];
static ubyte HMI_ExitFlags[HMI_ID_FLAGS_SIZE];
static HMI_ID_t HMI_LastHmiId;
static ushort HMI_idMapping[HMI_ID_MAX_NUMBER];
static const sbyte* HMI_idScreenNames[HMI_ID_MAX_NUMBER] =
{
	/* HMI_ID_SPLASH,                            */ "Splash",
	/* HMI_ID_MAIN,                              */ "Main",
	/* HMI_ID_TEST_FLOW,                         */ "Test Flow",
	/* HMI_ID_MEMORY_REPORT,                     */ "Memory report",
	/* HMI_ID_ENVIRONMENTAL_SETUP,               */ "Environemntal Setup",
	/* HMI_ID_PM10_SETUP_1,                      */ "PM10 Setup 1",
	/* HMI_ID_PM10_DATA_1,                       */ "PM10 Data 1",	
	/* HMI_ID_PM10_WAITING,                      */ "PM10 Waiting",	
	/* HMI_ID_PM10_GRAPH,                        */ "PM10 Graph",	
	/* HMI_ID_TEST,                              */ "Test",
	/* HMI_ID_MESSAGE_SCREEN,                    */ "Message",
	/* HMI_ID_MEMORY,                            */ "Memory",
	/* HMI_ID_CLIENT,                            */ "Client",
	/* HMI_ID_OPERATING_PROGRAM,                 */ "Operating Program",
	/* HMI_ID_INFO,                              */ "Info",
	/* HMI_ID_TEST_TEMPERATURE,                  */ "Test temperature",
    /* HMI_ID_TEST_LEAK,                         */ "Test leak",
    /* HMI_ID_ENVIRONMENTAL_DATA,                */ "Environmental Data",
    /* HMI_ID_DATETIME,                          */ "Date Time",
    /* HMI_ID_SETUP,                             */ "Setup",
    /* HMI_ID_LANGUAGE,                          */ "Language",                 
    /* HMI_ID_PARAMETER,                         */ "Parameter",
    /* HMI_ID_ENVIRONMENTAL_WAITING              */ "Environmental Waiting",
    /* HMI_ID_ENVIRONMENTAL_GRAPH                */ "Environmental Graph",
    /* HMI_ID_DUCT_SETUP_1,                      */ "Duct Setup 1",
    /* HMI_ID_DUCT_SETUP_2,                      */ "Duct Setup 2",
    /* HMI_ID_DUCT_SETUP_3,                      */ "Duct Setup 3",
    /* HMI_ID_DUCT_DATA,                         */ "Duct Data",
    /* HMI_ID_DUCT_GRAPH,                        */ "Duct Graph",
    /* HMI_ID_DUCT_WAITING,                      */ "Duct Waiting",
    /* HMI_ID_SRB_SETUP_1,                       */ "SRB Setup 1",
    /* HMI_ID_SRB_SETUP_2,                       */ "SRB Setup 2",
    /* HMI_ID_SRB_SETUP_3,                       */ "SRB Setup 3",
    /* HMI_ID_SRB_DATA,                          */ "SRB Data",
    /* HMI_ID_SRB_GRAPH,                         */ "SRB Graph",
    /* HMI_ID_SRB_WAITING,                       */ "SRB Waiting",
    /* HMI_ID_TEST_MIN                           */ "Test min motor"
};

static ubyte MessageScreen_dynamicMessage[512U];
static ubyte MessageScreen_titleTextTemp[64U];

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t MessageScreen_logFlag(FLAG_t flag);
static STD_RETURN_t MessageScreen_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t MessageScreen_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t MessageScreen_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t MessageScreen_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t HMI_ChangeHmiAndPage(HMI_ID_t id_dest, ushort page_dest, HMI_ID_t hmi_id_source, ushort page_source);

static void MessageScreen_LoadHMIIDVariables(void);

static void MessageScreen_manageButtons(ulong execTimeMs);
static void MessageScreen_manageScreen(ulong execTimeMs);

static STD_RETURN_t HMI_CheckEnterExitScreen(ulong execTimeMs);
static STD_RETURN_t HMI_AssignIdMapping(HMI_ID_t hmi_id, ulong easId);

#ifdef MESSAGE_SCREEN_ENABLE_THREAD
static void MessageScreen_modelInit(void);
static void MessageScreen_model(ulong execTimeMs);
#endif

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t MessageScreen_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t MessageScreen_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t MessageScreen_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t MessageScreen_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t MessageScreen_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static void MessageScreen_manageButtons(ulong execTimeMs)
{
    if(message_screen_okButton_released != false) /* These flag is reset by GUI */
    {
        BUZM_BUTTON_SOUND();
        (void)MessageScreen_logFlag(FLAG_MESSAGE_SCREEN_BUTTON_OK);
    }
}

static STD_RETURN_t HMI_AssignIdMapping(HMI_ID_t hmi_id, ulong easId)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte alreadyPresent = false;
    HMI_ID_t hmi_id_found = HMI_ID_MAX_NUMBER;
    ulong i = 0;
    if(hmi_id < HMI_ID_MAX_NUMBER)
    {
        debug_print("easId %3d - hmi_id %3d - %s", easId, hmi_id, HMI_idScreenNames[hmi_id]);
        for(i < 0; (i < HMI_CheckScreenCounter) && (alreadyPresent == false); i++)
        {
            if(HMI_idMapping[i] == easId)
            {
                alreadyPresent = true;
                hmi_id_found = (HMI_ID_t)i;
            }
        }
        
        if(alreadyPresent != false)
        {
            debug_print("[WARN] Entering the %d Eas id screen %d already used in %d-%s screen mapping", HMI_CheckScreenCounter, easId, hmi_id_found, HMI_idScreenNames[hmi_id_found]);
        }
        else
        {
            HMI_idMapping[hmi_id] = easId;
            HMI_CheckScreenCounter++;
        }
    }
    else
    {
        debug_print("ERROR: easId %3d - hmi_id %3d", easId, hmi_id);
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

static STD_RETURN_t HMI_CheckEnterExitScreen(ulong execTimeMs)
{   
    STD_RETURN_t returnValue = STD_RETURN_OK;
    HMI_ID_t idActual = HMI_ID_MAX_NUMBER;
    ulong i;
    if(Hmi_HMIIDVariablesLoaded != false)
    {
        for(i = 0U; (idActual == HMI_ID_MAX_NUMBER) && (i < HMI_ID_MAX_NUMBER); i++)
        {
            if(HMI_idMapping[i] == HMI_Current_ID)
            {
                idActual = i;
            }
        }
        
        if(idActual < HMI_ID_MAX_NUMBER)
        {
            if(idActual != HMI_LastHmiId)
            {
                /* Enter */
                UTIL_SetFlag(&HMI_EnterFlags[0U], true, idActual, HMI_ID_MAX_NUMBER);
                
                if(HMI_LastHmiId < HMI_ID_MAX_NUMBER)
                {
                    /* Exit - avoid to do this during first run */
                    UTIL_SetFlag(&HMI_ExitFlags[0U], true, HMI_LastHmiId, HMI_ID_MAX_NUMBER);
                    
                    debug_print("Screen Transition %d-%s ----> %d-%s", HMI_LastHmiId, HMI_idScreenNames[HMI_LastHmiId], idActual, HMI_idScreenNames[idActual]);
                }
                else
                {
                    debug_print("Screen First run ----> %d-%s", idActual, HMI_idScreenNames[idActual]);
                }
            }
            else
            {
                /* no transition all bits down */
                memory_set(&HMI_EnterFlags[0U], 0x00U, HMI_ID_FLAGS_SIZE);
                memory_set(&HMI_ExitFlags[0U], 0x00U, HMI_ID_FLAGS_SIZE);
            }
            HMI_LastHmiId = idActual;
        }
        else
        {
            debug_print("[ERRO] HMI_Current_ID %d not managed in HMI_CheckEnterExitScreen", HMI_Current_ID);
            returnValue = STD_RETURN_ERROR_PARAM;
        }
    }
    else
    {
        debug_print("[WARN] HMI_CheckEnterExitScreen: hmi id variables not yet loaded!");
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] HMI_CheckEnterExitScreen");
    }
    
    return returnValue;
}

static void MessageScreen_LoadHMIIDVariables(void)
{
    /* Load in ladder during flgPrimoCiclo all the symbol hmi into ushort variable symbols to load into this array */
    if(LoadHMIID_Vars != false)
    {
        LoadHMIID_Vars = false;
        (void)HMI_AssignIdMapping(HMI_ID_SPLASH								, splash_screen_hmi_id						);
        (void)HMI_AssignIdMapping(HMI_ID_MAIN                               , main_screen_hmi_id                        );
        (void)HMI_AssignIdMapping(HMI_ID_TEST_FLOW							, testFlow_screen_hmi_id					);
        (void)HMI_AssignIdMapping(HMI_ID_MEMORY_REPORT						, memoryReport_screen_hmi_id				);
        (void)HMI_AssignIdMapping(HMI_ID_ENVIRONMENTAL_SETUP                , environmental_setup_screen_hmi_id     	);
        
        (void)HMI_AssignIdMapping(HMI_ID_PM10_SETUP_1                       , pm10_setup_1_screen_hmi_id      			);
        (void)HMI_AssignIdMapping(HMI_ID_PM10_DATA_1                        , pm10_data_1_screen_hmi_id                 );
        (void)HMI_AssignIdMapping(HMI_ID_PM10_WAITING                       , pm10_waiting_screen_hmi_id                );
        (void)HMI_AssignIdMapping(HMI_ID_PM10_GRAPH                         , pm10_graph_screen_hmi_id                  );

        (void)HMI_AssignIdMapping(HMI_ID_TEST								, test_screen_hmi_id						);
        (void)HMI_AssignIdMapping(HMI_ID_MESSAGE_SCREEN						, message_screen_hmi_id						);

        (void)HMI_AssignIdMapping(HMI_ID_MEMORY								, memory_screen_hmi_id						);
        (void)HMI_AssignIdMapping(HMI_ID_CLIENT								, client_screen_hmi_id						);
        (void)HMI_AssignIdMapping(HMI_ID_OPERATING_PROGRAM					, opp_screen_hmi_id							);
        (void)HMI_AssignIdMapping(HMI_ID_INFO								, info_screen_hmi_id						);
        (void)HMI_AssignIdMapping(HMI_ID_TEST_TEMPERATURE   				, test_temp_screen_hmi_id					);
        (void)HMI_AssignIdMapping(HMI_ID_TEST_LEAK							, test_leak_screen_hmi_id					);
        (void)HMI_AssignIdMapping(HMI_ID_ENVIRONMENTAL_DATA                 , environmental_data_screen_hmi_id          );
		(void)HMI_AssignIdMapping(HMI_ID_DATETIME                           , dateTime_screen_hmi_id                    );
		(void)HMI_AssignIdMapping(HMI_ID_SETUP                              , setup_screen_hmi_id                       );
		(void)HMI_AssignIdMapping(HMI_ID_LANGUAGE                           , language_screen_hmi_id                    );
		(void)HMI_AssignIdMapping(HMI_ID_PARAMETER                          , parameter_screen_hmi_id                   );
		(void)HMI_AssignIdMapping(HMI_ID_ENVIRONMENTAL_WAITING              , environmental_waiting_screen_hmi_id       );
		(void)HMI_AssignIdMapping(HMI_ID_ENVIRONMENTAL_GRAPH                , environmental_graph_screen_hmi_id         );
		
		(void)HMI_AssignIdMapping(HMI_ID_DUCT_SETUP_1                       , duct_setup_1_screen_hmi_id                );
		(void)HMI_AssignIdMapping(HMI_ID_DUCT_SETUP_2                       , duct_setup_2_screen_hmi_id                );
		(void)HMI_AssignIdMapping(HMI_ID_DUCT_SETUP_3                       , duct_setup_3_screen_hmi_id                );
		(void)HMI_AssignIdMapping(HMI_ID_DUCT_DATA                          , duct_data_1_screen_hmi_id                 );
		(void)HMI_AssignIdMapping(HMI_ID_DUCT_GRAPH                         , duct_graph_1_screen_hmi_id                );
		(void)HMI_AssignIdMapping(HMI_ID_DUCT_WAITING                       , duct_waiting_screen_hmi_id                );
		
		(void)HMI_AssignIdMapping(HMI_ID_SRB_SETUP_1                        , srb_setup_1_screen_hmi_id                 );
		(void)HMI_AssignIdMapping(HMI_ID_SRB_SETUP_2                        , srb_setup_2_screen_hmi_id                 );
		(void)HMI_AssignIdMapping(HMI_ID_SRB_SETUP_3                        , srb_setup_3_screen_hmi_id                 );
		(void)HMI_AssignIdMapping(HMI_ID_SRB_DATA                           , srb_data_screen_hmi_id                    );
		(void)HMI_AssignIdMapping(HMI_ID_SRB_GRAPH                          , srb_graph_screen_hmi_id                   );
		(void)HMI_AssignIdMapping(HMI_ID_SRB_WAITING                        , srb_waiting_screen_hmi_id                 );
		
		(void)HMI_AssignIdMapping(HMI_ID_TEST_MIN                           , test_min_screen_hmi_id                    );
		
        if(HMI_CheckScreenCounter != HMI_ID_MAX_NUMBER)
        {
            debug_print("[ERRO] HMI screen disaligned");
        }
        else
        {
            debug_print("[INFO] HMI screen loaded ");
            Hmi_HMIIDVariablesLoaded = true;
        }
    }
}

static void MessageScreen_manageScreen(ulong execTimeMs)
{
    MessageScreen_LoadHMIIDVariables();

    if(FLAG_GetAndReset(FLAG_MESSAGE_SCREEN_BUTTON_OK) != false)
    {
        debug_print("return to hmi id %d-%s and hmi page %d", MessageScreen_hmi_id_source, HMI_idScreenNames[MessageScreen_hmi_id_source], MessageScreen_hmi_page_source);
        graphic_change_HMI(HMI_idMapping[MessageScreen_hmi_id_source], MessageScreen_hmi_page_source - 1U);
    }
}

#ifdef MESSAGE_SCREEN_ENABLE_THREAD
static void MessageScreen_modelInit(void)
{
 
}
  
static void MessageScreen_model(ulong execTimeMs)
{
    
}
#endif /* MESSAGE_SCREEN_ENABLE_THREAD */

static STD_RETURN_t HMI_ChangeHmiAndPage(HMI_ID_t id_dest, ushort page_dest, HMI_ID_t hmi_id_source, ushort page_source)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((id_dest < HMI_ID_MAX_NUMBER) && (page_dest > 0) && (page_dest <= 5))
    {
        /* a popup is entering, prioirty to show it, but save the new source */
        /* maybe the second condition is useless */
        if(MessageScreen_exitByPopup != false)
        {
            debug_print("change_hmi_and_page called by %d-%s %d but a popup must be shown %d, set new source %d-%s %d", hmi_id_source, HMI_idScreenNames[hmi_id_source], page_source, MessageScreen_exitByPopup, id_dest, HMI_idScreenNames[id_dest], page_dest);
            /* somebody call the change page when a popup is open, change the source of call to */
            MessageScreen_hmi_id_source = id_dest;
            MessageScreen_hmi_page_source = page_dest;
            /* remove the flag exitByPopup, in this way the new page can create itself */
            MessageScreen_exitByPopup = false;
            
            if(MessageScreen_forceRedrawOnReenterSource != false) /* page changed, remove the force redraw, it will be automatically redrawn*/
            {
                MessageScreen_forceRedrawOnReenterSource = false;
            }
        }
        else
        {
            if((id_dest == hmi_id_source) && (page_dest == page_source))
            {
                debug_print("[WARN] change_hmi_and_page requested to the same page %d-%s %d", id_dest, HMI_idScreenNames[id_dest], page_dest);
            }
            else
            {
                debug_print("change_hmi_and_page from %d-%s to %d-%s", hmi_id_source, HMI_idScreenNames[hmi_id_source], id_dest, HMI_idScreenNames[id_dest]);
                graphic_change_HMI(HMI_idMapping[id_dest], page_dest - 1U);
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* Public functions ----------------------------------------------------------*/
STD_RETURN_t HMI_ChangeHmi(HMI_ID_t id_dest, HMI_ID_t hmi_id_source)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(id_dest < HMI_ID_MAX_NUMBER)
    {
        returnValue = HMI_ChangeHmiAndPage(id_dest, 1U, hmi_id_source, 1U);
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t HMI_GoToRebootPage(HMI_ID_t hmi_id_source)
{
	return HMI_ChangeHmiAndPage(HMI_ID_SPLASH, 2U, hmi_id_source, 1U);
}

void MessageScreen_openBar(sbyte* titleText, sbyte* messageText, HMI_ID_t hmi_id_source)
{
    ulong titleSize = 0U;
    ulong messageTextLength = 0U;
    
    sbyte message_screen_bufferText[MESSAGE_SCREEN_MESSAGE_TEXT_SIZE];
    sbyte message_screen_titleText[MESSAGE_SCREEN_TITLE_TEXT_SIZE];
    
    MessageScreen_forceRedrawOnReenterSource = false; /* TODO pass by parameter */
    
    if(HMI_ID_MESSAGE_SCREEN != hmi_id_source) /* in case a popup arise over a popup, impossible to exit*/
    {
        MessageScreen_hmi_id_source = hmi_id_source;
        MessageScreen_hmi_page_source = 1U;
    }
    else
    {
        debug_print("POP-UP ON POP_UP");
    }
    
    debug_print("Pop-up Bar called by hmi id %d-%s and hmi page %d", hmi_id_source, HMI_idScreenNames[hmi_id_source], MessageScreen_hmi_page_source);

    /* clear buffers */
    memory_set(&message_screen_bufferText[0U], 0x00U, sizeof(message_screen_bufferText));
    memory_set(&message_screen_titleText[0U], 0x00U, sizeof(message_screen_titleText));
    
    /* check if there is text */
    if(messageText == NULL)
    {
        /* entering with no text*/
        string_snprintf(&message_screen_bufferText[0], sizeof(message_screen_bufferText), MESSAGE_SCREEN_NO_TEXT);
    }
    else
    {
        messageTextLength = string_length(messageText);
        if(messageTextLength > MESSAGE_SCREEN_MESSAGE_TEXT_SIZE)
        {
            messageTextLength = MESSAGE_SCREEN_MESSAGE_TEXT_SIZE;
        }
        /* copy text into buffer */
        memory_copy(&message_screen_bufferText[0], &messageText[0], messageTextLength);
    }
    
    if(titleText == NULL)
    {
        /* entering with no text*/
        string_snprintf(&message_screen_titleText[0], sizeof(message_screen_titleText), MESSAGE_SCREEN_NO_TEXT);
    }
    else
    {
        titleSize = string_length(titleText);
        if(titleSize > MESSAGE_SCREEN_TITLE_TEXT_SIZE)
        {
            titleSize = MESSAGE_SCREEN_TITLE_TEXT_SIZE;
        }
        /* copy text into buffer */
        memory_copy(&message_screen_titleText[0], &titleText[0], titleSize);
    }
        
    message_screen_title_background = YELLOW;
    message_screen_title_foreground = BLUE;
    
    message_screen_progressBar_maximumValue = FLOAT_TO_WORD(100.0f);
    message_screen_progressBar_textAndWord = FLOAT_TO_WORD(0.0f);
    
    message_screen_progressBar_visible = true;
    
    message_screen_okButton_enable = false;
    
    (void)UTIL_BufferFromSbyteToUShort(&message_screen_titleText[0], titleSize, (ushort*)&message_screen_title_text_1, MESSAGE_SCREEN_TITLE_TEXT_SIZE);
    (void)UTIL_BufferFromSbyteToUShort(&message_screen_bufferText[0], messageTextLength, (ushort*)&message_screen_message_text_1, MESSAGE_SCREEN_MESSAGE_TEXT_SIZE);
    
    MessageScreen_exitByPopup = true;
    /* start the ladder command to enter in this screen */
    if(HMI_ID_MESSAGE_SCREEN != hmi_id_source)
    {
        debug_print("graphic_change_HMI starting");
        graphic_change_HMI(HMI_idMapping[HMI_ID_MESSAGE_SCREEN], 0U); /* MUST BE 0 not 1 */
        debug_print("graphic_change_HMI finish");
    }
}

void MessageScreen_openBarWithString(DYNAMIC_STRING_VARIOUS_t idVariousTitle, DYNAMIC_STRING_VARIOUS_t idVariousMessage, HMI_ID_t hmi_id_source)
{
    memory_set(&MessageScreen_titleTextTemp[0U], 0x00U, sizeof(MessageScreen_titleTextTemp));    
    if(idVariousTitle < DYNAMIC_STRING_INFO_MAX_NUMBER)
    {
        graphic_get_dynamic_string_text(&MessageScreen_titleTextTemp[0], (sizeof(MessageScreen_titleTextTemp) - 1U), DYNAMIC_STRING_VARIOUS, idVariousTitle);
    }
    
    memory_set(&MessageScreen_dynamicMessage[0U], 0x00U, sizeof(MessageScreen_dynamicMessage));    
    if(idVariousMessage < DYNAMIC_STRING_INFO_MAX_NUMBER)
    {
        graphic_get_dynamic_string_text(&MessageScreen_dynamicMessage[0], (sizeof(MessageScreen_dynamicMessage) - 1U), DYNAMIC_STRING_VARIOUS, idVariousMessage);
    }
    debug_print("MessageScreen_openBarWithString: %s %s", &MessageScreen_titleTextTemp[0], &MessageScreen_dynamicMessage[0]);
    
    MessageScreen_openBar(&MessageScreen_titleTextTemp[0], &MessageScreen_dynamicMessage[0U], hmi_id_source);
}

void MessageScreen_setOkButtonEnable(ubyte enable)
{
    message_screen_okButton_enable = enable;
}

void MessageScreen_setBarValue(float value)
{
    if((value >= 0.0f) && (value <= 100.0f))
    {
        message_screen_progressBar_textAndWord = FLOAT_TO_WORD(value);
    }
}

void MessageScreen_setTitle(sbyte* messageText)
{
    ulong titleSize = 0U;
    sbyte message_screen_titleText[MESSAGE_SCREEN_TITLE_TEXT_SIZE];
    if(messageText == NULL)
    {
        /* entering with no text*/
        string_snprintf(&message_screen_titleText[0], sizeof(message_screen_titleText), MESSAGE_SCREEN_NO_TEXT);
    }
    else
    {
        titleSize = string_length(messageText);
        if(titleSize > MESSAGE_SCREEN_TITLE_TEXT_SIZE)
        {
            titleSize = MESSAGE_SCREEN_TITLE_TEXT_SIZE;
        }
        /* copy text into buffer */
        memory_copy(&message_screen_titleText[0], &messageText[0], titleSize);
    }
    (void)UTIL_BufferFromSbyteToUShort(&message_screen_titleText[0], titleSize, (ushort*)&message_screen_title_text_1, MESSAGE_SCREEN_TITLE_TEXT_SIZE);
}

void MessageScreen_setTitleWithString(DYNAMIC_STRING_VARIOUS_t idVarious)
{
    memory_set(&MessageScreen_titleTextTemp[0U], 0x00U, sizeof(MessageScreen_titleTextTemp));    
    if(idVarious < DYNAMIC_STRING_INFO_MAX_NUMBER)
    {
        graphic_get_dynamic_string_text(&MessageScreen_titleTextTemp[0], (sizeof(MessageScreen_titleTextTemp) - 1U), DYNAMIC_STRING_VARIOUS, idVarious);
    }
    debug_print("MessageScreen_setTitleWithString: %s", &MessageScreen_titleTextTemp[0]);
    MessageScreen_setTitle(&MessageScreen_titleTextTemp[0U]);
}

void MessageScreen_setText(sbyte* messageText)
{
    ulong messageTextLength;
    sbyte message_screen_bufferText[MESSAGE_SCREEN_MESSAGE_TEXT_SIZE];
    if(messageText == NULL)
    {
        /* entering with no text*/
        string_snprintf(&message_screen_bufferText[0], sizeof(message_screen_bufferText), MESSAGE_SCREEN_NO_TEXT);
    }
    else
    {
        messageTextLength = string_length(messageText);
        if(messageTextLength > MESSAGE_SCREEN_MESSAGE_TEXT_SIZE)
        {
            messageTextLength = MESSAGE_SCREEN_MESSAGE_TEXT_SIZE;
        }
        /* copy text into buffer */
        memory_copy(&message_screen_bufferText[0], &messageText[0], messageTextLength);
    }
    (void)UTIL_BufferFromSbyteToUShort(&message_screen_bufferText[0], messageTextLength, (ushort*)&message_screen_message_text_1, MESSAGE_SCREEN_MESSAGE_TEXT_SIZE);
}

void MessageScreen_setTextWithString(DYNAMIC_STRING_VARIOUS_t idVarious)
{
    memory_set(&MessageScreen_dynamicMessage[0U], 0x00U, sizeof(MessageScreen_dynamicMessage));    
    if(idVarious < DYNAMIC_STRING_INFO_MAX_NUMBER)
    {
        graphic_get_dynamic_string_text(&MessageScreen_dynamicMessage[0], (sizeof(MessageScreen_dynamicMessage) - 1U), DYNAMIC_STRING_VARIOUS, idVarious);
    }
    debug_print("MessageScreen_setTextWithString: %s", &MessageScreen_dynamicMessage[0]);
    MessageScreen_setText(&MessageScreen_dynamicMessage[0U]);
}

void MessageScreen_SetForceRedraw(ubyte force, HMI_ID_t hmi_id)
{
    if(hmi_id == MessageScreen_hmi_id_source)
    {
        debug_print("Force re-draw %d set by %d-%s", force, hmi_id, HMI_idScreenNames[hmi_id]);
        MessageScreen_forceRedrawOnReenterSource = force;
    }
}

void MessageScreen_open(MESSAGE_SCREEN_TYPE_t type, sbyte* messageText, STD_ERROR_t errorCode, HMI_ID_t hmi_id_source)
{    
    ulong titleSize = 0U;
    ulong messageTextLength = 0U;
    
    sbyte message_screen_bufferText[MESSAGE_SCREEN_MESSAGE_TEXT_SIZE];
    sbyte message_screen_titleText[MESSAGE_SCREEN_TITLE_TEXT_SIZE];
    
    MessageScreen_forceRedrawOnReenterSource = false; /* TODO pass by parameter */

    if(HMI_ID_MESSAGE_SCREEN != hmi_id_source) /* in case a popup arise over a popup, impossible to exit*/
    {
        MessageScreen_hmi_id_source = hmi_id_source;
        MessageScreen_hmi_page_source = 1U;
    }
    else
    {
        debug_print("POP-UP ON POP_UP");
    }
    
    debug_print("Pop-up called by hmi id %d-%s and hmi page %d", hmi_id_source, HMI_idScreenNames[hmi_id_source], MessageScreen_hmi_page_source);
    
    /* clear buffers */
    memory_set(&message_screen_bufferText[0U], 0x00U, sizeof(message_screen_bufferText));
    memory_set(&message_screen_titleText[0U], 0x00U, sizeof(message_screen_titleText));
    
    /* check if there is text */
    if(messageText == NULL)
    {
        /* entering with no text*/
        string_snprintf(&message_screen_bufferText[0], sizeof(message_screen_bufferText), MESSAGE_SCREEN_NO_TEXT);
    }
    else
    {
        messageTextLength = string_length(messageText);
        if(messageTextLength > MESSAGE_SCREEN_MESSAGE_TEXT_SIZE)
        {
            messageTextLength = MESSAGE_SCREEN_MESSAGE_TEXT_SIZE;
        }
        /* copy text into buffer */
        memory_copy(&message_screen_bufferText[0], &messageText[0], messageTextLength);
    }
    
    if(type < MESSAGE_SCREEN_TYPE_MAX_NUMBER)
    {
        switch(type)
        {
            case MESSAGE_SCREEN_TYPE_INFO:
                message_screen_title_background = GREEN;
                message_screen_title_foreground = BLACK;
                graphic_get_dynamic_string_text(&MessageScreen_titleTextTemp[0], (sizeof(MessageScreen_titleTextTemp) - 1U), DYNAMIC_STRING_POPUP_TYPE, POPUP_TYPE_STRING_INFO);
                titleSize = string_snprintf(&message_screen_titleText[0], sizeof(message_screen_titleText), "%s", &MessageScreen_titleTextTemp[0U]);
                break;
            case MESSAGE_SCREEN_TYPE_ERROR:
                message_screen_title_background = RED;
                message_screen_title_foreground = BLACK;
                graphic_get_dynamic_string_text(&MessageScreen_titleTextTemp[0], (sizeof(MessageScreen_titleTextTemp) - 1U), DYNAMIC_STRING_POPUP_TYPE, POPUP_TYPE_STRING_ERROR);
                titleSize = string_snprintf(&message_screen_titleText[0], sizeof(message_screen_titleText), "%s #%d", &MessageScreen_titleTextTemp[0U], errorCode);
                break;
            case MESSAGE_SCREEN_TYPE_WARNING:
                message_screen_title_background = 136 /* ORANGE */;
                message_screen_title_foreground = BLACK;
                graphic_get_dynamic_string_text(&MessageScreen_titleTextTemp[0], (sizeof(MessageScreen_titleTextTemp) - 1U), DYNAMIC_STRING_POPUP_TYPE, POPUP_TYPE_STRING_WARNING);
                titleSize = string_snprintf(&message_screen_titleText[0], sizeof(message_screen_titleText), "%s", &MessageScreen_titleTextTemp[0U]);
                break;
            default:
                message_screen_title_background = GRAY;
                message_screen_title_foreground = BLACK;
                graphic_get_dynamic_string_text(&MessageScreen_titleTextTemp[0], (sizeof(MessageScreen_titleTextTemp) - 1U), DYNAMIC_STRING_POPUP_TYPE, POPUP_TYPE_STRING_UNKNOWN);
                titleSize = string_snprintf(&message_screen_titleText[0], sizeof(message_screen_titleText), "%s", &MessageScreen_titleTextTemp[0U]);
                break;
        }
    }
    
    message_screen_okButton_enable = true;
    message_screen_progressBar_visible = false;
    
    (void)UTIL_BufferFromSbyteToUShort(&message_screen_titleText[0], titleSize, (ushort*)&message_screen_title_text_1, MESSAGE_SCREEN_TITLE_TEXT_SIZE);
    (void)UTIL_BufferFromSbyteToUShort(&message_screen_bufferText[0], messageTextLength, (ushort*)&message_screen_message_text_1, MESSAGE_SCREEN_MESSAGE_TEXT_SIZE);
    
    MessageScreen_exitByPopup = true;
    /* start the ladder command to enter in this screen */
    
    if(HMI_ID_MESSAGE_SCREEN != hmi_id_source)
    {
        debug_print("graphic_change_HMI starting");
        graphic_change_HMI(HMI_idMapping[HMI_ID_MESSAGE_SCREEN], 0U); /* MUST BE 0 not 1 */
        debug_print("graphic_change_HMI finish");
    }
}

void MessageScreen_openInfo(DYNAMIC_STRING_INFO_t idInfo, HMI_ID_t hmi_id_source)
{
    memory_set(&MessageScreen_dynamicMessage[0U], 0x00U, sizeof(MessageScreen_dynamicMessage));    
    if(idInfo < DYNAMIC_STRING_INFO_MAX_NUMBER)
    {
        graphic_get_dynamic_string_text(&MessageScreen_dynamicMessage[0], (sizeof(MessageScreen_dynamicMessage) - 1U), DYNAMIC_STRING_INFO, idInfo);
    }
    debug_print("MessageScreen_openInfo: %s", &MessageScreen_dynamicMessage[0]);
    MessageScreen_open(MESSAGE_SCREEN_TYPE_INFO, &MessageScreen_dynamicMessage[0], 0U, hmi_id_source);
}

void MessageScreen_openWarning(DYNAMIC_STRING_WARNING_t idWarning, HMI_ID_t hmi_id_source)
{
    memory_set(&MessageScreen_dynamicMessage[0U], 0x00U, sizeof(MessageScreen_dynamicMessage));
    if(idWarning < DYNAMIC_STRING_WARNING_MAX_NUMBER)
    {
        graphic_get_dynamic_string_text(&MessageScreen_dynamicMessage[0], (sizeof(MessageScreen_dynamicMessage) - 1U), DYNAMIC_STRING_WARNING, idWarning);
    }
    debug_print("MessageScreen_openWarning: %s", &MessageScreen_dynamicMessage[0]);
    MessageScreen_open(MESSAGE_SCREEN_TYPE_WARNING, &MessageScreen_dynamicMessage[0], 0U, hmi_id_source);
}

void MessageScreen_openError(DYNAMIC_STRING_ERROR_t idError, ulong errorCode, HMI_ID_t hmi_id_source)
{  
    memory_set(&MessageScreen_dynamicMessage[0U], 0x00U, sizeof(MessageScreen_dynamicMessage));
    if(idError < DYNAMIC_STRING_ERROR_MAX_NUMBER)
    {
        graphic_get_dynamic_string_text(&MessageScreen_dynamicMessage[0], (sizeof(MessageScreen_dynamicMessage) - 1U), DYNAMIC_STRING_ERROR, idError);
    }
    debug_print("MessageScreen_openError: %s", &MessageScreen_dynamicMessage[0]);
    MessageScreen_open(MESSAGE_SCREEN_TYPE_ERROR, &MessageScreen_dynamicMessage[0], errorCode, hmi_id_source);
}

/* call this function in every exit page to avoid the clear of some variables */
ubyte MessageScreen_isExitedHmiFromPopup(HMI_ID_t hmi_id)
{
    debug_print("%d-%s _isExitedHmiFromPopup %d", hmi_id, HMI_idScreenNames[hmi_id], MessageScreen_exitByPopup);
    return MessageScreen_exitByPopup;
}

/* call this function in every enter page, to avoid recreation of the page */
ubyte MessageScreen_isEnteredHmiFromPopup(HMI_ID_t hmi_id)
{
    debug_print("%d-%s _isEnteredHmiFromPopup %d", hmi_id, HMI_idScreenNames[hmi_id], MessageScreen_exitByPopup);
    ubyte returnValue = MessageScreen_exitByPopup;
    MessageScreen_exitByPopup = false;
    if(hmi_id == MessageScreen_hmi_id_source)
    {
        debug_print("Check force redraw for %d-%s.. %d", hmi_id, HMI_idScreenNames[hmi_id], MessageScreen_forceRedrawOnReenterSource);
        if(MessageScreen_forceRedrawOnReenterSource != false)
        {
            MessageScreen_forceRedrawOnReenterSource = false;
            returnValue = false; /* force the re-draw on reenter page */
        }
    }
    return returnValue;
}

ubyte MessageScreen_isEnteredHmi(HMI_ID_t hmi_id)
{
    return UTIL_GetFlag(&HMI_EnterFlags[0U], hmi_id, HMI_ID_MAX_NUMBER);
}

ubyte MessageScreen_isExitedHmi(HMI_ID_t hmi_id)
{
    return UTIL_GetFlag(&HMI_ExitFlags[0U], hmi_id, HMI_ID_MAX_NUMBER);
}

HMI_ID_t HMI_GetHmiIdOnScreen(void)
{
	return HMI_LastHmiId;
}

STD_RETURN_t MessageScreen_ManageErrorPopup(SAMPLING_ERROR_t error, HMI_ID_t destHmi, HMI_ID_t sourceHmi)
{
    debug_print("Srb_ManageErrorPopupOnScreen error %d", error);
    if(error != SAMPLING_ERROR_NONE)
    {
        switch(error)
        {
            case SAMPLING_ERROR_NO_COMMUNICATION:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_NO_INTERNAL_COM, error, sourceHmi);
                break;
            case SAMPLING_ERROR_CLOGGED_FILTER:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_LOW_FLOW, error, sourceHmi);
                break;
            case SAMPLING_ERROR_RETURNED_ERROR_FROM_GR:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_SAMPLING_STOPPED_DUE_TO_ERROR, error, sourceHmi);
                break;
            case SAMPLING_ERROR_NO_COMMUNICATION_SRB_INIT_1_TIMEOUT:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_SRB_INIT_1_TIMEOUT, error, sourceHmi);
                break;
            case SAMPLING_ERROR_NO_COMMUNICATION_SRB_INIT_1_TX_BUSY:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_SRB_INIT_1_TX_TOOT_BUSY, error, sourceHmi);
                break;
            case SAMPLING_ERROR_NO_COMMUNICATION_SRB_INIT_2_TIMEOUT:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_SRB_INIT_2_TIMEOUT, error, sourceHmi);
                break;
            case SAMPLING_ERROR_NO_COMMUNICATION_SRB_INIT_2_TX_BUSY:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_SRB_INIT_2_TX_TOOT_BUSY, error, sourceHmi);
                break;
            case SAMPLING_ERROR_NO_COMMUNICATION_SRB_DATA_TIMEOUT:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_SRB_DATA_TIMEOUT, error, sourceHmi);
                break;
            case SAMPLING_ERROR_NO_COMMUNICATION_SRB_DATA_TX_BUSY:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_SRB_DATA_TX_TOOT_BUSY, error, sourceHmi);
                break;
            case SAMPLING_ERROR_PUMP_NO_INPULSE:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_PUMP_NO_INPULSE, error, sourceHmi);
                break;
            case SAMPLING_ERROR_PUMP_NO_REGULATION_CAPABILITIES:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_PUMP_NO_REGULATION_CAPABILITIES, error, sourceHmi);
                break;
            case SAMPLING_ERROR_MEMORY_CORRUPTION:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_MEMORY_CORRUPTION, error, sourceHmi);
                break;
            case SAMPLING_ERROR_PROGRAM_ERROR:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_PROGRAM_ERROR, error, sourceHmi);
                break;
            case SAMPLING_ERROR_INVERTER_NO_COM:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_INVERTER_NO_COM, error, sourceHmi);
                break;
            case SAMPLING_ERROR_UNKNOWN:
            default:
                MessageScreen_openError(DYNAMIC_STRING_ERROR_UNKNOWN, error, sourceHmi);
                break;
        }
        (void)HMI_ChangeHmi(destHmi, sourceHmi);
    }
    return STD_RETURN_OK;
}

STD_RETURN_t MessageScreen_ManageWarningPopup(SYST_WARNING_t warningCode, ubyte warningSubCode, HMI_ID_t sourceHmi)
{
    debug_print("warning code %d warning sub code %d - source %d", warningCode, warningSubCode, sourceHmi);
	switch(warningCode)
	{
		  case SYST_WARNING_SENSOR_BAROMETRIC:
			MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SENSOR_BAROMETRIC, sourceHmi);
			break;
		  case SYST_WARNING_SENSOR_VACUUM:
			MessageScreen_openWarning(DYNAMIC_STRING_WARNING_SENSOR_VACUUM, sourceHmi);
			break;
		  case SYST_WARNING_SENSOR_METER_TEMPERATURE:
			MessageScreen_openWarning(DYNAMIC_STRING_WARNING_METER_TEMPERATURE, sourceHmi);
			break;
		  case SYST_WARNING_SENSOR_EXTERNAL_TEMPERATURE:
			MessageScreen_openWarning(DYNAMIC_STRING_WARNING_EXTERNAL_TEMPERATURE, sourceHmi);
			break;
		  case SYST_WARNING_SENSOR_FILTER_TEMPERATURE:
			MessageScreen_openWarning(DYNAMIC_STRING_WARNING_FILTER_TEMPERATURE, sourceHmi);
			break;
		  case SYST_WARNING_SENSOR_STORAGE_TEMPERATURE:
			MessageScreen_openWarning(DYNAMIC_STRING_WARNING_STORAGE_TEMPERATURE, sourceHmi);
			break; 
		  case SYST_WARNING_PUMP_NO_KEEP_ALIVE:
			MessageScreen_openWarning(DYNAMIC_STRING_WARNING_PUMP_NO_KEEP_ALIVE, sourceHmi);
			break;
		  case SYST_WARNING_MEMORY_CRC_ON_READ:
			MessageScreen_openWarning(DYNAMIC_STRING_WARNING_MEMORY_CRC_ON_READ, sourceHmi);
			break;
		  default:
			MessageScreen_openWarning(DYNAMIC_STRING_WARNING_NOT_MANAGED, sourceHmi);
			break;
	}
	return STD_RETURN_OK;
}

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void message_screen_init()
{
    MessageScreen_exitByPopup = false;
    memory_set(&HMI_idMapping[0], HMI_ID_MAX_NUMBER, sizeof(HMI_idMapping));
    
    HMI_CheckScreenCounter = 0U;
    memory_set(&HMI_EnterFlags[0], 0x00U, HMI_ID_FLAGS_SIZE);
    memory_set(&HMI_ExitFlags[0], 0x00U, HMI_ID_FLAGS_SIZE);
    HMI_LastHmiId = HMI_ID_MAX_NUMBER;
    
    MessageScreen_forceRedrawOnReenterSource = false;
    Hmi_HMIIDVariablesLoaded = false;
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void message_screen()
{
    Message_Screen_NormalRoutine++;
    (void)HMI_CheckEnterExitScreen(Execution_Normal);
    MessageScreen_manageButtons(Execution_Normal);
    MessageScreen_manageScreen(Execution_Normal);
}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void message_screen_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void message_screen_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
#ifdef MESSAGE_SCREEN_ENABLE_THREAD
void message_screen_thread()
{
    ulong execTime = MESSAGE_SCREEN_THREAD_SLEEP_MS;
    
    MessageScreen_modelInit();    
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
        Message_Screen_ThreadRoutine++; /* Only for debug */
        
        MessageScreen_model(execTime);
    }
}
#endif /* MESSAGE_SCREEN_ENABLE_THREAD */
/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void message_screen_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void message_screen_shutdown()
//{
//}
