#ifndef MESSAGE_SCREEN_H
#define MESSAGE_SCREEN_H

/*
 * message_screen.h
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */
/* Includes ------------------------------------------------------------------*/
#include "typedef.h"

/* Public define ------------------------------------------------------------*/
typedef enum MESSAGE_SCREEN_TYPE_e
{
    MESSAGE_SCREEN_TYPE_INFO = 0,
    MESSAGE_SCREEN_TYPE_ERROR,
    MESSAGE_SCREEN_TYPE_WARNING, 
    MESSAGE_SCREEN_TYPE_MAX_NUMBER
}MESSAGE_SCREEN_TYPE_t;

typedef enum HMI_ID_e
{
	/* 00 */HMI_ID_SPLASH = 0,
	/* 01 */HMI_ID_MAIN,
	/* 02 */HMI_ID_TEST_FLOW,
	/* 03 */HMI_ID_MEMORY_REPORT,
	/* 04 */HMI_ID_ENVIRONMENTAL_SETUP,
	/* 05 */HMI_ID_PM10_SETUP_1,
    /* 06 */HMI_ID_PM10_DATA_1,
    /* 07 */HMI_ID_PM10_WAITING,
    /* 08 */HMI_ID_PM10_GRAPH,
	/* 09 */HMI_ID_TEST,
	/* 10 */HMI_ID_MESSAGE_SCREEN,
	/* 11 */HMI_ID_MEMORY,
	/* 12 */HMI_ID_CLIENT,
	/* 13 */HMI_ID_OPERATING_PROGRAM,
	/* 14 */HMI_ID_INFO,
	/* 15 */HMI_ID_TEST_TEMPERATURE,
	/* 16 */HMI_ID_TEST_LEAK,
	/* 17 */HMI_ID_ENVIRONMENTAL_DATA,
    /* 18 */HMI_ID_DATETIME,
    /* 19 */HMI_ID_SETUP,
    /* 20 */HMI_ID_LANGUAGE,
    /* 21 */HMI_ID_PARAMETER,
    /* 22 */HMI_ID_ENVIRONMENTAL_WAITING,
    /* 23 */HMI_ID_ENVIRONMENTAL_GRAPH,
    /* 24 */HMI_ID_DUCT_SETUP_1,
    /* 25 */HMI_ID_DUCT_SETUP_2,
    /* 26 */HMI_ID_DUCT_SETUP_3,
    /* 27 */HMI_ID_DUCT_DATA,
    /* 28 */HMI_ID_DUCT_GRAPH,
    /* 29 */HMI_ID_DUCT_WAITING,
    /* 30 */HMI_ID_SRB_SETUP_1,
    /* 31 */HMI_ID_SRB_SETUP_2,
    /* 32 */HMI_ID_SRB_SETUP_3,
    /* 33 */HMI_ID_SRB_DATA,
    /* 34 */HMI_ID_SRB_GRAPH,
    /* 35 */HMI_ID_SRB_WAITING,
    /* 35 */HMI_ID_TEST_MIN,
	HMI_ID_MAX_NUMBER
}HMI_ID_t;

/* Public variables ----------------------------------------------------------*/

/* Public function prototypes -----------------------------------------------*/
//void MessageScreen_open(MESSAGE_SCREEN_TYPE_t type, sbyte* messageText, STD_ERROR_t errorCode, HMI_ID_t hmi_id_source);
void MessageScreen_openInfo(DYNAMIC_STRING_INFO_t idInfo, HMI_ID_t hmi_id_source);
void MessageScreen_openWarning(DYNAMIC_STRING_WARNING_t idWarning, HMI_ID_t hmi_id_source);
void MessageScreen_openError(DYNAMIC_STRING_ERROR_t idError, ulong errorCode, HMI_ID_t hmi_id_source);

ubyte MessageScreen_isExitedHmiFromPopup(HMI_ID_t hmi_id);
ubyte MessageScreen_isEnteredHmiFromPopup(HMI_ID_t hmi_id);
ubyte MessageScreen_isEnteredHmi(HMI_ID_t hmi_id);
ubyte MessageScreen_isExitedHmi(HMI_ID_t hmi_id);

STD_RETURN_t HMI_ChangeHmi(HMI_ID_t id, HMI_ID_t hmi_id_source);
STD_RETURN_t HMI_GoToRebootPage(HMI_ID_t hmi_id_source);
HMI_ID_t HMI_GetHmiIdOnScreen(void);

//void MessageScreen_openBar(sbyte* titleText, sbyte* messageText, HMI_ID_t hmi_id_source);
void MessageScreen_setBarValue(float value);
void MessageScreen_setText(sbyte* messageText);
//void MessageScreen_setTitle(sbyte* messageText);
void MessageScreen_setOkButtonEnable(ubyte enable);
void MessageScreen_SetForceRedraw(ubyte force, HMI_ID_t hmi_id);

void MessageScreen_setTitleWithString(DYNAMIC_STRING_VARIOUS_t idVarious);
void MessageScreen_setTextWithString(DYNAMIC_STRING_VARIOUS_t idVarious);
void MessageScreen_openBarWithString(DYNAMIC_STRING_VARIOUS_t idVariousTitle, DYNAMIC_STRING_VARIOUS_t idVariousMessage, HMI_ID_t hmi_id_source);

STD_RETURN_t MessageScreen_ManageErrorPopup(SAMPLING_ERROR_t error, HMI_ID_t destHmi, HMI_ID_t sourceHmi);
STD_RETURN_t MessageScreen_ManageWarningPopup(SYST_WARNING_t warningCode, ubyte warningSubCode, HMI_ID_t sourceHmi);

#endif // MESSAGE_SCREEN_H 
