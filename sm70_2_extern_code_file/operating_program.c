/*
 * operating_program.c
 *
 *  Created on: 06/11/2019
 *      Author: Andrea Tonello
 */
#define OPERATING_PROGRAM_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "client_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"
#include "eepm_data.h"

/* Private define ------------------------------------------------------------*/
#define OPERATING_PROGRAM_NAME_VISIBLE_TIMER_MS       1000U
#define OPERATING_PROGRAM_NAME_NOT_VISIBLE_TIMER_MS   500U

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static Timer_t OperatingProgram_NameTimer;
static ubyte OperatingProgram_clientNameBlinkState;

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t OperatingProgram_logFlag(FLAG_t flag);
static STD_RETURN_t OperatingProgram_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t OperatingProgram_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t OperatingProgram_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t OperatingProgram_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static void OperatingProgram_InitVariables(void);

static void OperatingProgram_ManageButtons(ulong execTimeMs);
static void OperatingProgram_ManageScreen(ulong execTimeMs);

static void OperatingProgram_modelInit(void);
static void OperatingProgram_model(ulong execTimeMs);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t OperatingProgram_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t OperatingProgram_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t OperatingProgram_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t OperatingProgram_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t OperatingProgram_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static void OperatingProgram_ManageButtons(ulong execTimeMs)
{
    if(oopScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        oopScreen_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)OperatingProgram_logFlag(FLAG_OOP_SCREEN_BUTTON_BACK);
    }
    if(oopScreen_oop_environmentalButton_released != false) /* These flag is reset by GUI */
    {
        oopScreen_oop_environmentalButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)OperatingProgram_logFlag(FLAG_OOP_SCREEN_BUTTON_ENVIRONMENTAL);
    }
    if(oopScreen_oop_pm10Button_released != false) /* These flag is reset by GUI */
    { 
        oopScreen_oop_pm10Button_released = false;
        BUZM_BUTTON_SOUND();
        (void)OperatingProgram_logFlag(FLAG_OOP_SCREEN_BUTTON_PM10);
    }
    if(oopScreen_oop_ductButton_released != false) /* These flag is reset by GUI */
    {
        oopScreen_oop_ductButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)OperatingProgram_logFlag(FLAG_OOP_SCREEN_BUTTON_DUCT);
    }
    if(oopScreen_oop_remoteSRBButton_released != false) /* These flag is reset by GUI */
    {
        oopScreen_oop_remoteSRBButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)OperatingProgram_logFlag(FLAG_OOP_SCREEN_BUTTON_REMOTE_SRB); 
    }
    if(oopScreen_oop_clientTouch_released != false) /* These flag is reset by GUI */
    {
        oopScreen_oop_clientTouch_released = false;
        BUZM_BUTTON_SOUND();
        (void)OperatingProgram_logFlag(FLAG_OOP_SCREEN_TOUCH_CLIENT);
    }
}

static void OperatingProgram_ManageScreen(ulong execTimeMs)
{    
    const HMI_ID_t localHmiId = HMI_ID_OPERATING_PROGRAM;
    ubyte isRunning = false;
    ubyte isExpired = false;
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SYSTEM_STATE_t systemState = SYSTEM_GetState();
    
    (void) UTIL_TimerIncrement(&OperatingProgram_NameTimer, execTimeMs);
    
    (void) UTIL_TimerIsExpired(&OperatingProgram_NameTimer, &isExpired);
    if(isExpired != false)
    {
        (void) UTIL_TimerStop(&OperatingProgram_NameTimer);
        if(OperatingProgram_clientNameBlinkState != false)
        {
            OperatingProgram_clientNameBlinkState = false;
            /* Now set not visible */
            oopScreen_clientNameVisible = false;
            (void) UTIL_TimerStart(&OperatingProgram_NameTimer, OPERATING_PROGRAM_NAME_NOT_VISIBLE_TIMER_MS);
        }
        else
        {
            OperatingProgram_clientNameBlinkState = true;
            /* Now set visible */
            oopScreen_clientNameVisible = true;
            (void) UTIL_TimerStart(&OperatingProgram_NameTimer, OPERATING_PROGRAM_NAME_VISIBLE_TIMER_MS);
        }
    }
    
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ;
        }
        else
        {            
            ubyte ubyteValue = 0x00;

            if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_SYST_PROGRAM_ENVIRONMENTAL, &ubyteValue, EEPM_VALUE_TYPE_UBYTE, 1))
            {
                if(ubyteValue != 0x00)
                {
                    op_environmentalButton_enabled = true;
                }
                else
                {
                    op_environmentalButton_enabled = false;
                }
            }
            
            if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_SYST_PROGRAM_PM10, &ubyteValue, EEPM_VALUE_TYPE_UBYTE, 1))
            {
                if(ubyteValue != 0x00)
                {
                    op_pm10Button_enabled = true;
                }
                else
                {
                    op_pm10Button_enabled = false;
                }
            }
            
            if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_SYST_PROGRAM_DUCT, &ubyteValue, EEPM_VALUE_TYPE_UBYTE, 1))
            {
                if(ubyteValue != 0x00)
                {
                    op_ductButton_enabled = true;
                }
                else
                {
                    op_ductButton_enabled = false;
                }
            }
            
            if(STD_RETURN_OK == EEPM_GetId(EEPM_ID_SYST_PROGRAM_SRB_REMOTE, &ubyteValue, EEPM_VALUE_TYPE_UBYTE, 1))
            {
                if(ubyteValue != 0x00)
                {
                    op_remoteSrbButton_enabled = true;
                }
                else
                {
                    op_remoteSrbButton_enabled = false;
                }
            }
        
            /* Check clients file exists */
            oopScreen_clientNameVisible = false;
            oopScreen_clientName_1 = '\0';
            (void) UTIL_TimerIsRunning(&OperatingProgram_NameTimer, &isRunning);
            if(isRunning != false)
            {
                (void) UTIL_TimerStop(&OperatingProgram_NameTimer);
            }
            FLAG_Set(FLAG_OOP_SCREEN_COMMAND_LOAD_CLIENTS); /* Send the command to open the client file in the thread */
        }
    }
    
    if(FLAG_GetAndReset(FLAG_EVENT_CLIENT_FILE_OPENED_FOR_OOP) != FALSE)
    {
        oopScreen_clientNameVisible = true;
        if(Client_GetClientsTotalNumber() > 0U)
        {
            /* Use the selected client for report */
            oopScreen_clientNameColor = WHITE;
            Client_SetSelectedOne(clientScreen_selectedClientRetention);
            (void)UTIL_BufferFromSbyteToUShort(&ClientsFile_Clients[Client_GetSelectedOne()].name[0]          , CLIENT_FILE_CLIENT_NAME_LENTGH           , &oopScreen_clientName_1         , CLIENT_FILE_CLIENT_NAME_LENTGH);
        }
        else
        {
            oopScreen_clientNameColor = RED;
            sbyte tempStringBuffer[CLIENT_FILE_CLIENT_NAME_LENTGH] = {0};
            graphic_get_dynamic_string_text(&tempStringBuffer[0], (sizeof(tempStringBuffer) - 1U), DYNAMIC_STRING_VARIOUS, DYNAMIC_STRING_VARIOUS_NO_CLIENT_CONFIGURATED);
            (void)UTIL_BufferFromSbyteToUShort(&tempStringBuffer[0], CLIENT_FILE_CLIENT_NAME_LENTGH, &oopScreen_clientName_1, CLIENT_FILE_CLIENT_NAME_LENTGH);
            /* Start timer for blink */
            (void) UTIL_TimerStart(&OperatingProgram_NameTimer, OPERATING_PROGRAM_NAME_VISIBLE_TIMER_MS);
            OperatingProgram_clientNameBlinkState = true;
            oopScreen_clientNameVisible = true;
        }
    }  

    if(FLAG_GetAndReset(FLAG_OOP_SCREEN_BUTTON_BACK) != FALSE)
    {
        (void)HMI_ChangeHmi(HMI_ID_MAIN, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_OOP_SCREEN_BUTTON_ENVIRONMENTAL) != FALSE)
    {
        (void)HMI_ChangeHmi(HMI_ID_ENVIRONMENTAL_SETUP, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_OOP_SCREEN_BUTTON_PM10) != FALSE)
    {
        (void)HMI_ChangeHmi(HMI_ID_PM10_SETUP_1, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_OOP_SCREEN_BUTTON_DUCT) != FALSE)
    {
        (void)HMI_ChangeHmi(HMI_ID_DUCT_SETUP_1, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_OOP_SCREEN_BUTTON_REMOTE_SRB) != FALSE)
    {
        (void)HMI_ChangeHmi(HMI_ID_SRB_SETUP_1, localHmiId);
    }
    
    if(FLAG_GetAndReset(FLAG_OOP_SCREEN_TOUCH_CLIENT) != FALSE)
    {
        (void)HMI_ChangeHmi(HMI_ID_CLIENT, localHmiId);
    }
    
    if(HMI_GetHmiIdOnScreen() == localHmiId)
    {
        if(systemState != SYSTEM_STATE_RUNNING)
        {
            BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
            MessageScreen_openError(DYNAMIC_STRING_ERROR_NO_INTERNAL_COMMUNICATION, STD_ERROR_NO_COMMUNICATION, localHmiId);
            (void)HMI_ChangeHmi(HMI_ID_MAIN, localHmiId);
            (void) UTIL_TimerStop(&OperatingProgram_NameTimer);
        }
    }
    
    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        /* On exit screen */
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ;
        }
        else
        {
            oopScreen_clientName_1 = '\0';
            oopScreen_clientNameVisible = false;
            (void) UTIL_TimerStop(&OperatingProgram_NameTimer);
        }
    }
}

static void OperatingProgram_modelInit(void)
{

}

static void OperatingProgram_model(ulong execTimeMs)
{
    STD_RETURN_t threadReturn;
    if(FLAG_GetAndReset(FLAG_OOP_SCREEN_COMMAND_LOAD_CLIENTS) != FALSE)
    {
        //debug_print("FLAG_OOP_SCREEN_COMMAND_LOAD_CLIENTS");
        threadReturn = ClientFile_OpenAndReadOrCreateFile((sbyte*)&ClientFile_FileName[0], ClientFile_clientLocationSave, (sbyte*)&ClientFile_FileHeader[0], CLIENT_FILE_CLIENTS_MAX_NUMBER);
        switch(threadReturn)
        {
            case STD_RETURN_OK:
               FLAG_Set(FLAG_EVENT_CLIENT_FILE_OPENED_FOR_OOP);
               break;
            default:
                debug_print("ERROR: %d", threadReturn);
                break;
        }
    }
}

static void OperatingProgram_InitVariables(void)
{
    OperatingProgram_clientNameBlinkState = false;
    UTIL_TimerInit(&OperatingProgram_NameTimer);
}

/* Public functions ----------------------------------------------------------*/

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void operating_program_init()
{
    OperatingProgram_InitVariables();
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void operating_program()
{
    OperatingProgramm_NormalRoutine++;
    OperatingProgram_ManageButtons(Execution_Normal);
    OperatingProgram_ManageScreen(Execution_Normal);
}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void operating_program_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void operating_program_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void operating_program_thread()
{
    ulong execTime = OPERATING_PROGRAM_THREAD_SLEEP_MS;
    
    OperatingProgram_modelInit();
    
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
        OperatingProgramm_ThreadRoutine++; /* Only for debug */
        
        OperatingProgram_model(execTime);
    }
}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void operating_program_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void operating_program_shutdown()
//{
//}
