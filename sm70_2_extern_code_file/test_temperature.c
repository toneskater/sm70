/*
 * test_temperature.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define TEST_TEMPERATURE_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"

/* Private define ------------------------------------------------------------*/
/* #define TEST_TEMPERATURE_ENABLE_DEBUG_COUNTERS */
#define TEST_TEMPERATURE_NORMAL_ENABLE
/* #define TEST_TEMPERATURE_FAST_ENABLE */
/* #define TEST_TEMPERATURE_LOW_PRIO_ENABLE */
/* #define TEST_TEMPERATURE_THREAD_ENABLE */

#define TEST_TEMPERATURE_THREAD_SLEEP_MS 50U

#define TEST_TEMPERATURE_VALID_VALUE_COLOR DARKGREEN
#define TEST_TEMPERATURE_INVALID_VALUE_COLOR RED
#define TEST_TEMPERATURE_NOT_MOUNTED_COLOR DARKGRAY

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t TestTemperature_logFlag(FLAG_t flag);
static STD_RETURN_t TestTemperature_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t TestTemperature_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t TestTemperature_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t TestTemperature_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t TestTemperature_initVariable(void);
#ifdef TEST_TEMPERATURE_NORMAL_ENABLE
static STD_RETURN_t TestTemperature_manageButtons(ulong execTimeMs);
static STD_RETURN_t TestTemperature_manageScreen(ulong execTimeMs);
#endif /* #ifdef TEST_TEMPERATURE_NORMAL_ENABLE */
#ifdef TEST_TEMPERATURE_THREAD_ENABLE
static STD_RETURN_t TestTemperature_modelInit(void);
static STD_RETURN_t TestTemperature_model(ulong execTimeMs);
#endif /* #ifdef TEST_TEMPERATURE_THREAD_ENABLE */

static STD_RETURN_t TestTemperature_initDataOnScreen(void);
static STD_RETURN_t TestTemperature_showDataOnScreen(void);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t TestTemperature_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t TestTemperature_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t TestTemperature_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t TestTemperature_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
}

static STD_RETURN_t TestTemperature_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t TestTemperature_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */

    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestTemperature_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef TEST_TEMPERATURE_NORMAL_ENABLE
static STD_RETURN_t TestTemperature_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(test_tempScreen_backButton_released != false) /* These flag is reset by GUI */
    {
    	test_tempScreen_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestTemperature_logFlag(FLAG_TEST_TEMPERATURE_BUTTON_BACK);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestTemperature_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_TEMPERATURE_NORMAL_ENABLE */

#ifdef TEST_TEMPERATURE_NORMAL_ENABLE
static STD_RETURN_t TestTemperature_initDataOnScreen(void)
{
    testTemperature_meterPressureValue = FLOAT_TO_WORD(0.0f);
    testTemperature_meterPressureColor = TEST_TEMPERATURE_NOT_MOUNTED_COLOR;

    testTemperature_meterTemperatureValue = FLOAT_TO_WORD(0.0f);
    testTemperature_meterTemperatureColor = TEST_TEMPERATURE_NOT_MOUNTED_COLOR;

    testTemperature_externalTemperatureValue = FLOAT_TO_WORD(0.0f);
    testTemperature_externalTemperatureColor = TEST_TEMPERATURE_NOT_MOUNTED_COLOR;

    testTemperature_externalPressureValue = FLOAT_TO_WORD(0.0f);
    testTemperature_externalPressureColor = TEST_TEMPERATURE_NOT_MOUNTED_COLOR;
    return STD_RETURN_OK;
}

static STD_RETURN_t TestTemperature_showDataOnScreen(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    ubyte meterTemperatureMounted = false;
    ubyte meterPressureMounted = false;
    ubyte externalTemperatureMounted = false;
    ubyte externalPressureMounted = false;
    
    float meterTemperatureValue = 0.0f;
    float meterPressureValue = 0.0f;
    float externalTemperatureValue = 0.0f;
    float externalPressureValue = 0.0f;
    
    STD_RETURN_t meterTemperatureReturnValue = STD_RETURN_OK;
    STD_RETURN_t meterPressureReturnValue = STD_RETURN_OK;
    STD_RETURN_t externalTemperatureReturnValue = STD_RETURN_OK;
    STD_RETURN_t externalPressureReturnValue = STD_RETURN_OK;
    
    (void)NETWORK_GetMeterTemperatureMounted(&meterTemperatureMounted);
    (void)NETWORK_GetMeterPressureMounted(&meterPressureMounted);
    (void)NETWORK_GetExternTemperatureMounted(&externalTemperatureMounted);
    (void)NETWORK_GetExternPressureMounted(&externalPressureMounted);
    
    meterTemperatureReturnValue = NETWORK_GetMeterTemperature(&meterTemperatureValue);
    meterPressureReturnValue = NETWORK_GetMeterPressure(&meterPressureValue);
    externalTemperatureReturnValue = NETWORK_GetExternTemperatureSensor(&externalTemperatureValue);
    externalPressureReturnValue = NETWORK_GetExternDigitalBarometer(&externalPressureValue);
    
    if(meterTemperatureMounted != false)
    {
        if(meterTemperatureReturnValue == STD_RETURN_OK)
        {
            testTemperature_meterTemperatureColor = TEST_TEMPERATURE_VALID_VALUE_COLOR;
        }
        else
        {
            testTemperature_meterTemperatureColor = TEST_TEMPERATURE_INVALID_VALUE_COLOR;
        }
    }
    else
    {
        testTemperature_meterTemperatureColor = TEST_TEMPERATURE_NOT_MOUNTED_COLOR;
    }
    testTemperature_meterTemperatureValue = FLOAT_TO_WORD(meterTemperatureValue);
    
    if(meterPressureMounted != false)
    {
        if(meterPressureReturnValue == STD_RETURN_OK)
        {
            testTemperature_meterPressureColor = TEST_TEMPERATURE_VALID_VALUE_COLOR;
        }
        else
        {
            testTemperature_meterPressureColor = TEST_TEMPERATURE_INVALID_VALUE_COLOR;
        }
    }
    else
    {
        testTemperature_meterPressureColor = TEST_TEMPERATURE_NOT_MOUNTED_COLOR;
    }
    testTemperature_meterPressureValue = FLOAT_TO_WORD(meterPressureValue);
    
    if(externalTemperatureMounted != false)
    {
        if(externalTemperatureReturnValue == STD_RETURN_OK)
        {
            testTemperature_externalTemperatureColor = TEST_TEMPERATURE_VALID_VALUE_COLOR;
        }
        else
        {
            testTemperature_externalTemperatureColor = TEST_TEMPERATURE_INVALID_VALUE_COLOR;
        }
    }
    else
    {
        testTemperature_externalTemperatureColor = TEST_TEMPERATURE_NOT_MOUNTED_COLOR;
    }
    testTemperature_externalTemperatureValue = FLOAT_TO_WORD(externalTemperatureValue);
    
    if(externalPressureMounted != false)
    {
        if(externalPressureReturnValue == STD_RETURN_OK)
        {
            testTemperature_externalPressureColor = TEST_TEMPERATURE_VALID_VALUE_COLOR;
        }
        else
        {
            testTemperature_externalPressureColor = TEST_TEMPERATURE_INVALID_VALUE_COLOR;
        }
    }
    else
    {
        testTemperature_externalPressureColor = TEST_TEMPERATURE_NOT_MOUNTED_COLOR;
    }
    testTemperature_externalPressureValue = FLOAT_TO_WORD(externalPressureValue);
    
    return returnValue;
}

static STD_RETURN_t TestTemperature_manageScreen(ulong execTimeMs)
{
    const HMI_ID_t localHmiId = HMI_ID_TEST_TEMPERATURE;
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SYSTEM_STATE_t systemState = SYSTEM_GetState();
    /* START CODE */
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            TestTemperature_initDataOnScreen();
        }
    }
    
    if(HMI_GetHmiIdOnScreen() == localHmiId)
    {
        if(systemState != SYSTEM_STATE_RUNNING)
        {
            BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
            MessageScreen_openError(DYNAMIC_STRING_ERROR_NO_INTERNAL_COMMUNICATION, STD_ERROR_NO_COMMUNICATION, HMI_GetHmiIdOnScreen());
            (void)HMI_ChangeHmi(HMI_ID_MAIN, localHmiId);
        }
        else
        {
            TestTemperature_showDataOnScreen();
        }
    }
        
    if(FLAG_GetAndReset(FLAG_TEST_TEMPERATURE_BUTTON_BACK) != false)
    {
        (void)HMI_ChangeHmi(HMI_ID_TEST, HMI_GetHmiIdOnScreen()); 
    }

    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
            TestTemperature_initDataOnScreen();
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestTemperature_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef TEST_TEMPERATURE_THREAD_ENABLE
static STD_RETURN_t TestTemperature_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */

    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestTemperature_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_TEMPERATURE_THREAD_ENABLE */

#ifdef TEST_TEMPERATURE_THREAD_ENABLE
static STD_RETURN_t TestTemperature_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */

    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestTemperature_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_TEMPERATURE_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void test_temperature_init()
{
    (void)TestTemperature_initVariable();
}

#ifdef TEST_TEMPERATURE_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void test_temperature()
{
#ifdef TEST_TEMPERATURE_ENABLE_DEBUG_COUNTERS
    TestTemperature_NormalRoutine++;
#endif
    (void)TestTemperature_manageButtons(Execution_Normal);
    (void)TestTemperature_manageScreen(Execution_Normal);
}
#endif /* #ifdef TEST_TEMPERATURE_NORMAL_ENABLE */

#ifdef TEST_TEMPERATURE_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void test_temperature_fast()
{
#ifdef TEST_TEMPERATURE_ENABLE_DEBUG_COUNTERS
    TestTemperature_FastRoutine++;
#endif
}
#endif /* #ifdef TEST_TEMPERATURE_FAST_ENABLE */

#ifdef TEST_TEMPERATURE_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void test_temperature_low_priority()
{
#ifdef TEST_TEMPERATURE_ENABLE_DEBUG_COUNTERS
    TestTemperature_LowPrioRoutine++;
#endif
}
#endif /* #ifdef TEST_TEMPERATURE_LOW_PRIO_ENABLE */

#ifdef TEST_TEMPERATURE_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void test_temperature_thread()
{
    ulong execTime = TEST_TEMPERATURE_THREAD_SLEEP_MS;

    (void)TestTemperature_modelInit();

    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef TEST_TEMPERATURE_ENABLE_DEBUG_COUNTERS
        TestTemperature_ThreadRoutine++; /* Only for debug */
#endif

        (void)TestTemperature_model(execTime);
    }
}
#endif /* #ifdef TEST_TEMPERATURE_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void test_temperature_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void test_temperature_shutdown()
//{
//}
