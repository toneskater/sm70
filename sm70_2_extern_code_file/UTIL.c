/*
 * util.c
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */
#define UTIL_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "UTIL.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Public define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
void UTIL_IncreaseAddDaysToDate(ushort* year, ubyte* month, ubyte* day, ulong dayToAdd)
{
    const ubyte maxDayMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    debug_print("Passed Date %04d/%02d/%02d - Days %d", *year, *month, *day, dayToAdd);
	if((year != NULL) && (month != NULL) && (day != NULL) && (dayToAdd > 0U))
	{
		if((*month >= 1) && (*month <= 12U))
		{
            while(dayToAdd > 0)
            {
                (*day)++;
                if( 
                    ( /* Not leap year */
                        (((*year) % 4U) != 0U) && 
                        ((*day) > maxDayMonth[(*month) - 1U])
                    )
                    ||
                    ( /* Leap year not february */
                        (((*year) % 4U) == 0U) && 
                        ((*month) != 2U) &&
                        ((*day) > maxDayMonth[(*month) - 1U])
                    )
                    ||
                    ( /* Leap year and february */
                        (((*year) % 4U) == 0U) && 
                        ((*month) == 2U) &&
                        ((*day) > 29U)
                    )
                )
                {
                    *day = 1;
                    (*month)++;
                    if((*month) > 12U)
                    {
                        *month = 1;
                        (*year)++;
                    }
                }
                dayToAdd--;
            }
		}
	}
	debug_print("Calculated Date: %04d/%02d/%02d", *year, *month, *day);
}

ubyte UTIL_DateIsOverAnotherDate(ushort yearRef, ubyte monthRef, ubyte dayRef, ushort yearActual, ubyte monthActual, ubyte dayActual)
{
    debug_print("------------------------------", yearRef, monthRef, dayRef);
    debug_print("Reference Date: %04d/%02d/%02d", yearRef, monthRef, dayRef);
    debug_print("Actual Date   : %04d/%02d/%02d", yearActual, monthActual, dayActual);
    debug_print("------------------------------", yearRef, monthRef, dayRef);
	ubyte returnValue = false;
    
    if(yearActual < yearRef)
    {
        returnValue = false;
    }
    else
    {
        if(monthActual < monthRef)
        {
            returnValue = false;
        }
        else
        {
            if(dayActual < dayRef)
            {
                returnValue = false;
            }
            else
            {
                returnValue = true;
            }
        }
    }
	return returnValue;
}

STD_RETURN_t UTIL_PrintBufferOnHex(ubyte* buffer, ulong bufferSize)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    #define SIZE 513U
    ubyte temp[SIZE] = {0x00U};
    ulong i = 0;
    ulong index = 0;
    index += string_snprintf(&temp[index], sizeof(temp), "%03d - ", bufferSize);
    while((i < bufferSize) && (index < (SIZE - 3U)))
    {
        index += string_snprintf(&temp[index], sizeof(temp) - index, "%02X ", buffer[i]);
        i++;
    }
    debug_print("%s", temp);
    return returnValue;
}

void UTIL_SetFlag(ubyte* flags, ubyte flagValue, ulong flagPosition, ulong maxSize)
{
  if(flagPosition < maxSize)
  {
    if(flagValue != false)
    {
      flags[flagPosition / 8] |=  (1 << (7 - (flagPosition % 8)));
    }
    else
    {
      flags[flagPosition / 8] &=  (~(1 << (7 - (flagPosition % 8))));
    }
  }
  else
  {
    debug_print("[WARN] UTIL_SetFlag: flagPosition %d maxSize %d", flagPosition, maxSize);
  }
}

ubyte UTIL_GetFlag(ubyte* flags, ulong flagPosition, ulong maxSize)
{
  ubyte returnValue = false;
  
  if(flagPosition < maxSize)
  {
    if((flags[flagPosition / 8] & (1 << (7 - (flagPosition % 8)))) != 0x00)
    {
      returnValue = true;
    }
  }
  else
  {
    debug_print("[WARN] UTIL_GetFlag: flagPosition %d maxSize %d", flagPosition, maxSize);
  }
  return returnValue;
}

ubyte UTIL_GetFlagAndReset(ubyte* flags, ulong flagPosition, ulong maxSize)
{
  ubyte returnValue = false;
  if(flagPosition < maxSize)
  {
    if((flags[flagPosition / 8] & (1 << (7 - (flagPosition % 8)))) != 0x00)
    {
      flags[flagPosition / 8] &=  (~(1 << (7 - (flagPosition % 8))));
      returnValue = true;
    }
  }  
  else
  {
    debug_print("[WARN] UTIL_GetFlagAndReset: flagPosition %d maxSize %d", flagPosition, maxSize);
  }
  return returnValue;
}

ulong UTIL_GetBufferStringOfUshortLength(ushort* srcPtr, ulong srcPtrSize)
{
    ulong returnValue = 0;
    if((srcPtr != NULL) && (srcPtrSize > 0))
    {
        while((srcPtr[returnValue] != '\0') && (returnValue < srcPtrSize))
        {
            returnValue++;
        }
    }
    //debug_print("UTIL_GetBufferStringOfUshortLength %d", returnValue);
    return returnValue;
}

STD_RETURN_t UTIL_BufferFromUShortToSByte(ushort* srcPtr, ulong srcPtrSize, sbyte* destPtr, ulong destPtrSize)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((srcPtr != NULL) && (srcPtrSize > 0) && (destPtr != NULL) && (destPtrSize > 0))
    {
        ushort* ptrLocal = srcPtr;
        int i = 0;
        do
        {
            destPtr[i] = (sbyte)srcPtr[i];
            i++;
        }
        while((i < srcPtrSize) && (i < destPtrSize) &&(srcPtr[i] != '\0'));
        //debug_print("UTIL_BufferFromUShortToSByte: %d %s", i, destPtr);
        for(; i < destPtrSize; i++)
        {
            destPtr[i] = '\0';
        }
        /*
        if(i < destPtrSize)
        {
            destPtr[i] = '\0';
        }*/
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_BufferFromSbyteToUShort(sbyte* srcPtr, ulong srcPtrSize, ushort* destPtr, ulong destPtrSize)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((srcPtr != NULL) && (srcPtrSize > 0) && (destPtr != NULL) && (destPtrSize > 0))
    {
        sbyte* ptrLocal = srcPtr;
        int i = 0;
        do
        {
            destPtr[i] = (ushort)(srcPtr[i] & 0x00FFU);
            i++;
        }
        while((i < srcPtrSize) && (i < destPtrSize) &&(srcPtr[i] != '\0'));
        //debug_print("UTIL_BufferFromSbyteToUShort: %d %s", i, srcPtr);
        for(; i < destPtrSize; i++)
        {
            destPtr[i] = '\0';
        }
        /*
        if(i < destPtrSize)
        {
            destPtr[i] = '\0';
        }*/
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}


STD_RETURN_t UTIL_TimerInit(Timer_t *timer)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        timer->counter = 0;
        timer->counterLimit = 0;
        timer->running = false;
        timer->expired = false;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerStart(Timer_t *timer, ulong timeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((timer != NULL) && (timeMs > 0))
    {
        timer->counter = 0;
        timer->counterLimit = timeMs;
        timer->running = true;
        timer->expired = false;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerGetRemainingTimeMs(Timer_t *timer, ulong* remainingTime)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((timer != NULL) && (remainingTime != NULL))
    {
        *remainingTime = timer->counterLimit - timer->counter;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerIsExpired(Timer_t *timer, ubyte* isExpired)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((timer != NULL) && (isExpired != NULL))
    {
        *isExpired = timer->expired;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerIsRunning(Timer_t *timer, ubyte* isRunning)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((timer != NULL) && (isRunning != NULL))
    {
        *isRunning = timer->running;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerStop(Timer_t *timer)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        timer->running = false;
        timer->counter = 0;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerPause(Timer_t *timer)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        timer->running = false;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerResume(Timer_t *timer)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        timer->running = true;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerIncrement(Timer_t *timer, ulong timeMsElapsed)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        if((timer->running != false) && (timer->expired == false)) /* if it is running and not expired, increase and checl if is expired */
        {
            timer->counter += timeMsElapsed;
            if(timer->counter >= timer->counterLimit)
            {
                //timer->running = false;
                timer->expired = true;
            } 
        }
       
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

ubyte UTIL_string_contain(sbyte* str, ulong strSize, ulong strStartIndex, sbyte* pattern, ulong patternSize)
{
    ulong index = 0;
    ulong returnValue = false;
    if((str != NULL) && (pattern != NULL))
    {
        while(((index + strStartIndex) < strSize) && (index < patternSize) && str[(index + strStartIndex)] == pattern[index])
        {
            index++;
        }
        if(index == patternSize)
        {
            returnValue = true;
        }
        else
        {
            returnValue = false;
        }
    }
    return returnValue;
}

ulong UTIL_string_contain_regular(sbyte* str, ulong str_size, sbyte* pattern, ulong pattern_size)
{
	ulong found = false;
	ulong patternIndex = 0;
	ulong stringIndex = 0;
	ulong allChar = false;

    if((str != NULL) && (pattern != NULL))
    {
        while ((stringIndex < str_size) && (patternIndex < pattern_size))
        {
            if (pattern[patternIndex] != '*')
            {
                if (allChar != false)
                {
                    if (str[stringIndex] != pattern[patternIndex])
                    {
                        stringIndex++;
                    }
                    else
                    {
                        allChar = false;
                    }
                }
                else
                {
                    if (str[stringIndex] != pattern[patternIndex])
                    {
                        stringIndex++;
                        patternIndex = 0;
                    }
                    else
                    {
                        stringIndex++;
                        patternIndex++;
                    }
                }
            }
            else
            {
                /* pattern == * */
                allChar = true;
                do
                {
                    patternIndex++;
                } while (pattern[patternIndex] == '*');
            }
        }

        if (patternIndex == pattern_size)
        {
            found = true;
        }
    }
	return found;
}

ulong UTIL_ClampULongValue(ulong value, ulong min, ulong max)
{
    if(value < min)
    {
        value = min;
    }
    else
    {
        if(value > max)
        {
            value = max;
        }
    }
    return value;
}

ulong UTIL_ClampSLongValue(slong value, slong min, slong max)
{
    if(value < min)
    {
        value = min;
    }
    else
    {
        if(value > max)
        {
            value = max;
        }
    }
    return value;
}

ushort UTIL_ClampUShortValue(ushort value, ushort min, ushort max)
{
    if(value < min)
    {
        value = min;
    }
    else
    {
        if(value > max)
        {
            value = max;
        }
    }
    return value;
}

sshort UTIL_ClampSShortValue(sshort value, sshort min, sshort max)
{
    if(value < min)
    {
        value = min;
    }
    else
    {
        if(value > max)
        {
            value = max;
        }
    }
    return value;
}

ubyte UTIL_ClampUByteValue(ubyte value, ubyte min, ubyte max)
{
    if(value < min)
    {
        value = min;
    }
    else
    {
        if(value > max)
        {
            value = max;
        }
    }
    return value;
}

sbyte UTIL_ClampSByteValue(sbyte value, sbyte min, sbyte max)
{
    if(value < min)
    {
        value = min;
    }
    else
    {
        if(value > max)
        {
            value = max;
        }
    }
    return value;
}

STD_RETURN_t UTIL_QueueInit(Queue_t* queuePtr, ulong sizeMax)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	ulong bitNumberSize = 0U;
	ulong elementsSizeBytes = 0U;
	if((queuePtr != NULL) && (sizeMax > 0U))
	{
		bitNumberSize = ((sizeMax - 1U) / 8U) + 1U;
		elementsSizeBytes = sizeMax * sizeof(void*);
		queuePtr->head = 0U;
		queuePtr->tail = 0U;
		queuePtr->size = 0U;
		queuePtr->sizeMax = sizeMax;
		queuePtr->flags = memory_alloc(bitNumberSize);
		queuePtr->elements = memory_alloc(elementsSizeBytes);
		memory_set(queuePtr->flags, 0x00U, bitNumberSize);
		memory_set(queuePtr->elements, 0x00U, elementsSizeBytes);
		debug_print("[INFO] UTIL_QueueInit Ok: queuePtr 0x%08X, bitNumberSizeBytes %d, elementsSizeBytes %d", queuePtr, bitNumberSize, elementsSizeBytes);
		returnValue = STD_RETURN_OK;
	}
	else
	{
		debug_print("[ERRO] UTIL_QueueInit param: queuePtr 0x%08X, sizeMax %d", queuePtr, sizeMax);
		returnValue = STD_RETURN_ERROR_PARAM;
	}
	return returnValue;
}

STD_RETURN_t UTIL_QueueSend(Queue_t* queuePtr, void* ptrElement)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	ubyte flag;
	if((queuePtr != NULL) && (ptrElement != NULL))
	{
		flag = UTIL_GetFlag(queuePtr->flags, queuePtr->head, queuePtr->sizeMax);
		if(flag != false)
		{
			/* Queue full */
			returnValue = STD_RETURN_QUEUE_FULL;
		}
		else
		{
			queuePtr->elements[queuePtr->head] = ptrElement;
			UTIL_SetFlag(queuePtr->flags, true, queuePtr->head, queuePtr->sizeMax);
			if(queuePtr->head < (queuePtr->sizeMax - 1U))
			{
                queuePtr->head = queuePtr->head + 1U;
			}
			else
			{
				queuePtr->head = 0U;
			}
			queuePtr->size = queuePtr->size + 1U;
			debug_print("[INFO] UTIL_QueueSend param: queuePtr 0x%08X, ptrElement 0x%08X, head %d, tail %d, size %d", queuePtr, ptrElement, queuePtr->head, queuePtr->tail, queuePtr->size);
			returnValue = STD_RETURN_OK;
		}
	}
	else
	{
		debug_print("[ERRO] UTIL_QueueSend param: queuePtr 0x%08X, ptrElement 0x%08X", queuePtr, ptrElement);
		returnValue = STD_RETURN_ERROR_PARAM;
	}
	return returnValue;
}

STD_RETURN_t UTIL_QueueReceive(Queue_t* queuePtr, void** ptrElement)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	ubyte flag;
	if((queuePtr != NULL) && (ptrElement != NULL))
	{
		flag = UTIL_GetFlag(queuePtr->flags, queuePtr->tail, queuePtr->sizeMax);
		if(flag != false)
		{
			/* Element to read */
			*ptrElement = queuePtr->elements[queuePtr->tail];
			UTIL_SetFlag(queuePtr->flags, false, queuePtr->tail, queuePtr->sizeMax);
			if(queuePtr->tail < (queuePtr->sizeMax - 1U))
			{
                queuePtr->tail = queuePtr->tail + 1U;
			}
			else
			{
				queuePtr->tail = 0U;
			}
			queuePtr->size = queuePtr->size - 1U;
			debug_print("[INFO] UTIL_QueueSend param: queuePtr 0x%08X, ptrElement 0x%08X, head %d, tail %d, size %d", queuePtr, ptrElement, queuePtr->head, queuePtr->tail, queuePtr->size);
			returnValue = STD_RETURN_OK;
		}
		else
		{
			/* Queue empty */
			returnValue = STD_RETURN_QUEUE_EMPTY;
		}
	}
	else
	{
		debug_print("[ERRO] UTIL_QueueReceive param: queuePtr 0x%08X, ptrElement 0x%08X", queuePtr, ptrElement);
		returnValue = STD_RETURN_ERROR_PARAM;
	}
	return returnValue;
}

STD_RETURN_t UTIL_QueueClear(Queue_t* queuePtr)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	ulong bitNumberSize = 0U;
	ulong elementsSizeBytes = 0U;
	if(queuePtr != NULL)
	{
		bitNumberSize = ((queuePtr->sizeMax - 1U) / 8U) + 1U;
		elementsSizeBytes = queuePtr->sizeMax * sizeof(void*);
		queuePtr->head = 0U;
		queuePtr->tail = 0U;
		queuePtr->size = 0U;
		memory_set(queuePtr->flags, 0x00U, bitNumberSize);
		memory_set(queuePtr->elements, 0x00U, elementsSizeBytes);
		returnValue = STD_RETURN_OK;
	}
	else
	{
		debug_print("[ERRO] UTIL_QueueClear param: queuePtr 0x%08X", queuePtr);
		returnValue = STD_RETURN_ERROR_PARAM;
	}
	return returnValue;
}

STD_RETURN_t UTIL_QueueSize(Queue_t* queuePtr, ulong* sizePtr)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	if((queuePtr != NULL) && (sizePtr != NULL))
	{
		*sizePtr = queuePtr->size = 0U;
		returnValue = STD_RETURN_OK;
	}
	else
	{
		debug_print("[ERRO] UTIL_QueueSize param: queuePtr 0x%08X, sizePtr 0x%08X", queuePtr, sizePtr);
		returnValue = STD_RETURN_ERROR_PARAM;
	}
	return returnValue;
}

STD_RETURN_t UTIL_QueueIsEmpty(Queue_t* queuePtr, ubyte* isEmptyPtr)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	if((queuePtr != NULL) && (isEmptyPtr != NULL))
	{
		if(queuePtr->size > 0)
		{
			*isEmptyPtr = false;
		}
		else
		{
			*isEmptyPtr = true;
		}
		returnValue = STD_RETURN_OK;
	}
	else
	{
		debug_print("[ERRO] UTIL_QueueIsEmpty param: queuePtr 0x%08X, isEmptyPtr 0x%08X", queuePtr, isEmptyPtr);
		returnValue = STD_RETURN_ERROR_PARAM;
	}
	return returnValue;
}
 
STD_RETURN_t UTIL_QueueIsFull(Queue_t* queuePtr, ubyte* isFullPtr)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	if((queuePtr != NULL) && (isFullPtr != NULL))
	{
		if(queuePtr->size < queuePtr->sizeMax)
		{
			*isFullPtr = false;
		}
		else
		{
			*isFullPtr = true;
		}
		returnValue = STD_RETURN_OK;
	}
	else
	{
		debug_print("[ERRO] UTIL_QueueIsFull param: queuePtr 0x%08X, isFullPtr 0x%08X", queuePtr, isFullPtr);
		returnValue = STD_RETURN_ERROR_PARAM;
	}
	return returnValue;
}

STD_RETURN_t UTIL_QueueDelete(Queue_t* queuePtr)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	if(queuePtr != NULL)
	{
		queuePtr->head = 0U;
		queuePtr->tail = 0U;
		queuePtr->size = 0U;
		queuePtr->sizeMax = 0U;
		memory_free(queuePtr->flags);
		debug_print("[INFO] UTIL_QueueDelete: queuePtr->flags 0x%08X", queuePtr->flags);
		memory_free(queuePtr->elements);
		debug_print("[INFO] UTIL_QueueDelete: queuePtr->elements 0x%08X", queuePtr->elements);
		debug_print("[INFO] UTIL_QueueDelete Ok: queuePtr 0x%08X", queuePtr);
		returnValue = STD_RETURN_OK;
	}
	else
	{
		debug_print("[ERRO] UTIL_QueueDelete param: queuePtr 0x%08X", queuePtr);
		returnValue = STD_RETURN_ERROR_PARAM;
	}
	return returnValue;
}

ubyte UTIL_MemoryEqualTo(ubyte* memoryPtr, ulong memoryLength, ubyte value)
{
    ubyte returnValue = true;
    ulong i;
    if(memoryPtr != NULL)
    {
        for(i = 0; (i < memoryLength) && (returnValue != false); i++)
        {
            if(memoryPtr[i] != value)
            {
                returnValue = false;
            }
        }
    }
    else
    {
        returnValue = false;
    }
    return returnValue;
}

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
//void UTIL_init()
//{ 
//}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
//void UTIL()
//{
//}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void UTIL_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void UTIL_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
//void UTIL_thread()
//{
//}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void UTIL_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void UTIL_shutdown()
//{
//}
