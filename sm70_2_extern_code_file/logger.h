#ifndef LOGGER_H
#define LOGGER_H

/*
 * logger.h
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */
/* Includes ------------------------------------------------------------------*/
#include "typedef.h"

/* Public define ------------------------------------------------------------*/
#define LOGGER_LOG_TEXT_SIZE 512U

/* Public variables ----------------------------------------------------------*/

/* Public function prototypes -----------------------------------------------*/
STD_RETURN_t Logger_logFlag(ulong flag, ulong value);
STD_RETURN_t Logger_logUnsignedValue(sbyte* variableName, ulong value);
STD_RETURN_t Logger_logFloatValue(sbyte* variableName, ulong value);
STD_RETURN_t Logger_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
STD_RETURN_t Logger_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

#endif // LOGGER_H 
