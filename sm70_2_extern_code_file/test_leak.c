/*
 * test_leak.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define TEST_LEAK_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"

/* Private define ------------------------------------------------------------*/
/* #define TEST_LEAK_ENABLE_DEBUG_COUNTERS */
#define TEST_LEAK_NORMAL_ENABLE
/* #define TEST_LEAK_FAST_ENABLE */
/* #define TEST_LEAK_LOW_PRIO_ENABLE */
#define TEST_LEAK_THREAD_ENABLE

#define TEST_LEAK_THREAD_SLEEP_MS 50U
#define TEST_LEAK_TIMER_GET_DATA_MS 500U

/* Private typedef -----------------------------------------------------------*/
typedef enum TEST_LEAK_GUI_STATE_e
{
    TEST_LEAK_GUI_STATE_INIT = 0,
    TEST_LEAK_GUI_STATE_READY,
    TEST_LEAK_GUI_STATE_RUNNING,
    /* TEST_LEAK_GUI_STATE_PAUSED, */
    TEST_LEAK_GUI_STATE_PUMP_STOPPING,
    TEST_LEAK_GUI_STATE_STOPPED,
    TEST_LEAK_GUI_STATE_MAX_NUMBER
}TEST_LEAK_GUI_STATE_t;

typedef enum TEST_LEAK_MODEL_STATE_e
{
    TEST_LEAK_MODEL_STATE_INIT = 0,
    TEST_LEAK_MODEL_STATE_READY,
    /* TEST_LEAK_MODEL_STATE_WAIT_START, */
    TEST_LEAK_MODEL_STATE_PUMP_START,
    TEST_LEAK_MODEL_STATE_RUNNING,
    /* TEST_LEAK_MODEL_STATE_PAUSED, */
    TEST_LEAK_MODEL_STATE_PUMP_STOP,
    TEST_LEAK_MODEL_STATE_STOPPED,
    TEST_LEAK_MODEL_STATE_ERROR,
    TEST_LEAK_MODEL_STATE_MAX_NUMBER
}TEST_LEAK_MODEL_STATE_t;

typedef enum MOTC_TEST_VOID_TYPE_e
{
  MOTC_TEST_VOID_TYPE_COMPENSATED = 0U,
  MOTC_TEST_VOID_TYPE_COSTANT,
  MOTC_TEST_VOID_TYPE_MAX_NUMBER
}MOTC_TEST_VOID_TYPE_t;

typedef struct TEST_LEAK_InterfaceData_s
{
    float voidSetPoint;
	MOTC_TEST_VOID_TYPE_t testType;
}TEST_LEAK_InterfaceData_t;

typedef enum MOTC_TEST_VOID_COMMAND_e
{
    MOTC_TEST_VOID_COMMAND_START = 0U,
    MOTC_TEST_VOID_COMMAND_GET_DATA,
    MOTC_TEST_VOID_COMMAND_STOP,
    MOTC_TEST_VOID_COMMAND_MAX_NUMBER
}MOTC_TEST_VOID_COMMAND_t;

static const sbyte* TestLeak_guiStateNames[TEST_LEAK_GUI_STATE_MAX_NUMBER] =
{
	/* TEST_LEAK_GUI_STATE_INIT,              */ "Init         ",
	/* TEST_LEAK_GUI_STATE_READY,             */ "Ready        ",
	/* TEST_LEAK_GUI_STATE_RUNNING,           */ "Running      ",
	/* TEST_LEAK_GUI_STATE_PUMP_STOPPING,     */ "Pump Stopping",
	/* TEST_LEAK_GUI_STATE_STOPPED,           */ "Stopped      ",
};

static const sbyte* TestLeak_modelStateNames[TEST_LEAK_MODEL_STATE_MAX_NUMBER] =
{
	/* TEST_LEAK_MODEL_STATE_INIT,            */ "Init         ",
	/* TEST_LEAK_MODEL_STATE_READY,           */ "Ready        ",
	/* TEST_LEAK_MODEL_STATE_PUMP_START,      */ "Pump Starting",
	/* TEST_LEAK_MODEL_STATE_RUNNING,         */ "Running      ",
	/* TEST_LEAK_MODEL_STATE_PUMP_STOP,       */ "Pump Stopping",
	/* TEST_LEAK_MODEL_STATE_STOPPED,         */ "Stopped      ",
	/* TEST_LEAK_MODEL_STATE_ERROR,           */ "Error        ",
};

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static TEST_LEAK_GUI_STATE_t TestLeak_GuiState;
static TEST_LEAK_MODEL_STATE_t TestLeak_ModelState;

static ubyte TestLeak_MessageTxBuffer[16U];
static ulong TestLeak_MessageTxBufferLength;
static ubyte TestLeak_MessageRxBuffer[16U];
static ulong TestLeak_MessageRxBufferLength;
static STD_RETURN_t messageReturn;

static ubyte TestLeak_requestStartSequence;
static ubyte TestLeak_requestStopSequence;
static ubyte TestLeak_requestDataSequence;  
static ubyte TestLeak_atLeastOneResponseDataSequence;
static Timer_t TestLeak_TimerData;
static ubyte TestLeak_ErrorDetectedOnSentMessage;

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t TestLeak_logFlag(FLAG_t flag);
static STD_RETURN_t TestLeak_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t TestLeak_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t TestLeak_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t TestLeak_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t TestLeak_initVariable(void);
#ifdef TEST_LEAK_NORMAL_ENABLE
static STD_RETURN_t TestLeak_manageButtons(ulong execTimeMs);
static STD_RETURN_t TestLeak_manageScreen(ulong execTimeMs);
#endif /* #ifdef TEST_LEAK_NORMAL_ENABLE */
#ifdef TEST_LEAK_THREAD_ENABLE
static STD_RETURN_t TestLeak_modelInit(void);
static STD_RETURN_t TestLeak_model(ulong execTimeMs);
#endif /* #ifdef TEST_LEAK_THREAD_ENABLE */

static STD_RETURN_t TestLeak_showDataOnCharts(void);
static STD_RETURN_t TestLeak_RestoreDataForNewSampling(void);
static STD_RETURN_t TestLeak_sendVoidSetPoint(float voidMmH2O);
static STD_RETURN_t TestLeak_modelStartFromReadyOrStopped(void);
static STD_RETURN_t TestLeak_CheckAndManageAutoTerminationCondition(void);
static STD_RETURN_t TestLeak_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError);
static STD_RETURN_t TestLeak_ErrorDetected(SAMPLING_ERROR_t* samplingError);

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t TestLeak_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t TestLeak_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t TestLeak_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t TestLeak_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t TestLeak_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t TestLeak_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    TestLeak_GuiState = TEST_LEAK_GUI_STATE_INIT;
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_initVariable %d", returnValue);
    }
    return returnValue;
}

#ifdef TEST_LEAK_NORMAL_ENABLE
static STD_RETURN_t TestLeak_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(test_leakScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        test_leakScreen_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestLeak_logFlag(FLAG_TEST_LEAK_BUTTON_BACK);
    }
    if(testLeakScreen_setPointVoid_eventAccepted != false)
    {
        testLeakScreen_setPointVoid_eventAccepted = false;
        FLAG_Set(FLAG_TEST_LEAK_SCREEN_SET_POINT_VOID_VALUE_CHANGED);
        BUZM_BUTTON_SOUND();
        (void)TestLeak_logFloatValue("Void Set Point %", testLeakScreen_setPointVoid_value);
    }  
    if(testLeakScreen_startButton_released != false) /* These flag is reset by GUI */
    {
    	testLeakScreen_startButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestLeak_logFlag(FLAG_TEST_LEAK_BUTTON_START);
    }
    if(testLeakScreen_stopButton_released != false) /* These flag is reset by GUI */
    {
    	testLeakScreen_stopButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestLeak_logFlag(FLAG_TEST_LEAK_BUTTON_STOP);
    }
    if(testLeakScreen_costantButton_released != false) /* These flag is reset by GUI */
    {
    	testLeakScreen_costantButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestLeak_logFlag(FLAG_TEST_LEAK_BUTTON_COSTANT);
    }
    if(testLeakScreen_compensatedButton_released != false) /* These flag is reset by GUI */
    {
    	testLeakScreen_compensatedButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)TestLeak_logFlag(FLAG_TEST_LEAK_BUTTON_COMPENSATED);
    }
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_manageButtons %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_LEAK_NORMAL_ENABLE */

#ifdef TEST_LEAK_NORMAL_ENABLE
static STD_RETURN_t TestLeak_showDataOnCharts(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    float LitersLost = 0.0f;
    float MeterPressure = 0.0f;
    ushort SecondsToTheEnd = 0U;
    
    if(NETWORK_GetTestVoidLitersLost(&LitersLost) == STD_RETURN_OK)
    {
        testLeakScreen_litersLost_color = WHITE;
    }
    else
    {
        testLeakScreen_litersLost_color = RED;
        returnValue = STD_RETURN_INVALID;
    }          
    testLeakScreen_litersLost_value = FLOAT_TO_WORD(LitersLost);  

    if(NETWORK_GetMeterPressure(&MeterPressure) == STD_RETURN_OK)
    {
        testLeakScreen_meterPressure_color = WHITE;
    }
    else
    {
        testLeakScreen_meterPressure_color = RED;
        returnValue = STD_RETURN_INVALID;
    }          
    testLeakScreen_meterPressure_value = FLOAT_TO_WORD(MeterPressure); 
    
    if(NETWORK_GetTestVoidSecondsToTheEnd(&SecondsToTheEnd) == STD_RETURN_OK)
    {
        testLeakScreen_secondsToTheEnd_color = WHITE;
    }
    else
    {
        testLeakScreen_secondsToTheEnd_color = RED;
        returnValue = STD_RETURN_INVALID;
    }          
    testLeakScreen_secondsToTheEnd_value = SecondsToTheEnd; 

    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestFlow_showDataOnCharts %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestLeak_manageScreenCompensatedAndCostantButton(void)
{
    switch(testLeakScreen_testType_value)
    {
        case MOTC_TEST_VOID_TYPE_COMPENSATED:
            testLeakScreen_costantButton_backgroundColor = YELLOW;
            testLeakScreen_compensatedButton_backgroundColor = DARKGREEN;
            break;
        case MOTC_TEST_VOID_TYPE_COSTANT:
            testLeakScreen_costantButton_backgroundColor = DARKGREEN;
            testLeakScreen_compensatedButton_backgroundColor = YELLOW;
            break;
    }
}

static void TestLeak_manageScreenFieldsEnable(TEST_LEAK_GUI_STATE_t stateOfArrival)
{
    debug_print("TestLeak_manageScreenFieldsEnable %d", stateOfArrival);
    switch(stateOfArrival)
    {
        case TEST_LEAK_GUI_STATE_INIT:
            test_leakScreen_backButton_enabled = true;
            testLeakScreen_setPointVoid_enable = true;
            testLeakScreen_setPointVoid_backgroundColor = WHITE;
            testLeakScreen_testType_enable = true;
            testLeakScreen_startButton_enabled = true;
            testLeakScreen_stopButton_enabled = false;
            testLeakScreen_compensatedButton_enable = true;
            testLeakScreen_costantButton_enable = true;
            break;
        case TEST_LEAK_GUI_STATE_READY:
            test_leakScreen_backButton_enabled = true;
            testLeakScreen_setPointVoid_enable = true;
            testLeakScreen_setPointVoid_backgroundColor = WHITE;
            testLeakScreen_testType_enable = true;
            testLeakScreen_startButton_enabled = true;
            testLeakScreen_stopButton_enabled = false;
            testLeakScreen_compensatedButton_enable = true;
            testLeakScreen_costantButton_enable = true;
            break;
        case TEST_LEAK_GUI_STATE_RUNNING:        
            test_leakScreen_backButton_enabled = false;
            testLeakScreen_setPointVoid_enable = false;
            testLeakScreen_setPointVoid_backgroundColor = DARKGRAY;
            testLeakScreen_testType_enable = false;
            testLeakScreen_startButton_enabled = false;
            testLeakScreen_stopButton_enabled = true;
            testLeakScreen_compensatedButton_enable = false;
            testLeakScreen_costantButton_enable = false;
            break;
        case TEST_LEAK_GUI_STATE_PUMP_STOPPING:
            test_leakScreen_backButton_enabled = false;
            testLeakScreen_setPointVoid_enable = false;
            testLeakScreen_setPointVoid_backgroundColor = DARKGRAY;
            testLeakScreen_testType_enable = false;
            testLeakScreen_startButton_enabled = false;
            testLeakScreen_stopButton_enabled = false;
            testLeakScreen_compensatedButton_enable = false;
            testLeakScreen_costantButton_enable = false;
            break;
        case TEST_LEAK_GUI_STATE_STOPPED:
            test_leakScreen_backButton_enabled = true;
            testLeakScreen_setPointVoid_enable = true;
            testLeakScreen_setPointVoid_backgroundColor = WHITE;
            testLeakScreen_testType_enable = true;
            testLeakScreen_startButton_enabled = true;
            testLeakScreen_stopButton_enabled = false;
            testLeakScreen_compensatedButton_enable = true;
            testLeakScreen_costantButton_enable = true;
            break;
    }
}

static STD_RETURN_t TestLeak_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    const HMI_ID_t localHmiId = HMI_ID_TEST_LEAK;
    /* START CODE */
    static TEST_LEAK_GUI_STATE_t TestLeak_LastGuiState = TEST_LEAK_GUI_STATE_INIT;
    ubyte costantVisible = false;
    ubyte ubyteValue;
    
    if(TestLeak_LastGuiState != TestLeak_GuiState)
    {
        debug_print("TLK GUI STATE from %d %s to %d %s", TestLeak_LastGuiState, TestLeak_guiStateNames[TestLeak_LastGuiState], TestLeak_GuiState, TestLeak_guiStateNames[TestLeak_GuiState]);
        TestLeak_LastGuiState = TestLeak_GuiState;
    }
    
    if(MessageScreen_isEnteredHmi(localHmiId) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
            switch(TestLeak_GuiState)
            {
                case TEST_LEAK_GUI_STATE_INIT:
                    TestLeak_manageScreenFieldsEnable(TEST_LEAK_GUI_STATE_INIT);                    
                    testLeakScreen_setPointVoid_value = FLOAT_TO_WORD(500.0f);
                    
                    (void)NETWORK_GetInverterOrPwm(&ubyteValue);
                    switch(ubyteValue)
                    {
                        case MOTC_MOTOR_TYPE_PWM:
                            costantVisible = true;
                            break;
                        case MOTC_MOTOR_TYPE_INVERTER:
                            costantVisible = false;
                            break;
                    }
                    
                    testLeakScreen_costantButton_visible = costantVisible;
                    testLeakScreen_compensatedButton_visible = true;
                    
                    testLeakScreen_testType_value = MOTC_TEST_VOID_TYPE_COMPENSATED;
                    TestLeak_manageScreenCompensatedAndCostantButton();
                    
                    testLeakScreen_litersLost_value = FLOAT_TO_WORD(0.0f);
                    testLeakScreen_litersLost_color = WHITE;
                    
                    testLeakScreen_secondsToTheEnd_value = 0U;
                    testLeakScreen_secondsToTheEnd_color = WHITE;
                    
                    testLeakScreen_meterPressure_value = FLOAT_TO_WORD(0.0f);
                    testLeakScreen_meterPressure_color = WHITE;
                
                    TestLeak_GuiState = TEST_LEAK_GUI_STATE_READY;
                    FLAG_Set(FLAG_TEST_LEAK_SCREEN_COMMAND_GO_TO_READY); 
                    TestLeak_manageScreenFieldsEnable(TEST_LEAK_GUI_STATE_READY);  
                    break;
                default:
                    break;
            }
        }
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_LEAK_BUTTON_COSTANT) != false)
    {
        testLeakScreen_testType_value = MOTC_TEST_VOID_TYPE_COSTANT;
        TestLeak_manageScreenCompensatedAndCostantButton();
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_LEAK_BUTTON_COMPENSATED) != false)
    {
        testLeakScreen_testType_value = MOTC_TEST_VOID_TYPE_COMPENSATED;
        TestLeak_manageScreenCompensatedAndCostantButton();
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_LEAK_BUTTON_START) != false)
    {
        TestLeak_GuiState = TEST_LEAK_GUI_STATE_RUNNING;
        TestLeak_manageScreenFieldsEnable(TEST_LEAK_GUI_STATE_RUNNING); 
        FLAG_Set(FLAG_TEST_LEAK_SCREEN_COMMAND_START); 
        
        testLeakScreen_litersLost_value = FLOAT_TO_WORD(0.0f);
        testLeakScreen_litersLost_color = WHITE;
        testLeakScreen_secondsToTheEnd_value = 0U;
        testLeakScreen_secondsToTheEnd_color = WHITE;
        testLeakScreen_meterPressure_value = FLOAT_TO_WORD(0.0f);
        testLeakScreen_meterPressure_color = WHITE;
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_LEAK_BUTTON_STOP) != false)
    {
        TestLeak_GuiState = TEST_LEAK_GUI_STATE_PUMP_STOPPING;
        TestLeak_manageScreenFieldsEnable(TEST_LEAK_GUI_STATE_PUMP_STOPPING);
        FLAG_Set(FLAG_TEST_LEAK_SCREEN_COMMAND_STOP);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_LEAK_BUTTON_BACK) != false)
    {
        TestLeak_GuiState = TEST_LEAK_GUI_STATE_INIT;
        FLAG_Set(FLAG_TEST_LEAK_SCREEN_COMMAND_BACK); 
        (void)HMI_ChangeHmi(HMI_ID_TEST, HMI_GetHmiIdOnScreen());
    }
    
    /* Test stopped due to automatic condition */
    if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED) != false)
    {         
        TestLeak_GuiState = TEST_LEAK_GUI_STATE_PUMP_STOPPING;
        TestLeak_manageScreenFieldsEnable(TEST_LEAK_GUI_STATE_PUMP_STOPPING);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_EVENT_PUMP_STOPPED) != false)
    {
        TestLeak_GuiState = TEST_LEAK_GUI_STATE_STOPPED;
        TestLeak_manageScreenFieldsEnable(TEST_LEAK_GUI_STATE_STOPPED); 
    }
    
    /* Test stopped due to error */
    if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR) != false)
    {
        TestLeak_GuiState = TEST_LEAK_GUI_STATE_INIT;
    }
    
    if(HMI_GetHmiIdOnScreen() == localHmiId)
    {
        TestLeak_showDataOnCharts();
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_SET_POINT_VOID_VALUE_CHANGED) != false)
    {
        if(WORD_TO_FLOAT(testLeakScreen_setPointVoid_value) > 0.0f)
        {
            testLeakScreen_startButton_enabled = true;
        }
        else
        {
            testLeakScreen_startButton_enabled = false;
        }
    }
    
    if(MessageScreen_isExitedHmi(localHmiId) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(localHmiId) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_manageScreen %d", returnValue);
    }
    return returnValue;
}
#endif

#ifdef TEST_LEAK_THREAD_ENABLE
static STD_RETURN_t TestLeak_ErrorDetected(SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SAMPLING_ERROR_t error = SAMPLING_ERROR_NONE;
    ubyte noComm = NETWORK_IsInNoCommunicationState();
    
    if(samplingError != NULL)
    {
        if((error == SAMPLING_ERROR_NONE) && (noComm != false))
        {
            error = SAMPLING_ERROR_NO_COMMUNICATION;
        }
        if((error == SAMPLING_ERROR_NONE) && (TestLeak_ErrorDetectedOnSentMessage != false))
        {
            TestLeak_ErrorDetectedOnSentMessage = false;
            error = SAMPLING_ERROR_RETURNED_ERROR_FROM_GR;
        }
        if((error == SAMPLING_ERROR_NONE) && (false))
        {
            ;
        }
        *samplingError = error;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_ErrorDetected: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t TestLeak_CheckAndManageAutoTerminationCondition(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    ubyte samplingTerminationCondition;
    returnValue = TestLeak_AreSamplingTerminationConditionReached(&samplingTerminationCondition, &Sampling_StopCause, &Sampling_Error);
    if(returnValue == STD_RETURN_OK)
    {
        if(samplingTerminationCondition != false)
        {
            debug_print("TEST_LEAK TERMINATION CONDITION REACHED Sampling_StopCause %d", Sampling_StopCause);

            switch(Sampling_StopCause)
            {
                case SAMPLING_STOP_CAUSE_TIME:
                    BUZM_BUTTON_SOUND(); /* manual sound */
                    MessageScreen_openInfo(DYNAMIC_STRING_INFO_TEST_STOPPED_BY_TIME_REACHED, HMI_GetHmiIdOnScreen());
                    TestLeak_ModelState = TEST_LEAK_MODEL_STATE_PUMP_STOP;
                    FLAG_Set(FLAG_TEST_LEAK_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED);
                    break;
                case SAMPLING_STOP_CAUSE_MANUAL:
                    /* manual sound not called, because manual stop is by pressing button */
                    MessageScreen_openWarning(DYNAMIC_STRING_WARNING_TEST_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen());
                    TestLeak_ModelState = TEST_LEAK_MODEL_STATE_PUMP_STOP;
                    FLAG_Set(FLAG_TEST_LEAK_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED);
                    break;
                case SAMPLING_STOP_CAUSE_ERROR:
                    BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
                    MessageScreen_openError(DYNAMIC_STRING_ERROR_TEST_BLOCKED_DUE_TO_ERROR, Sampling_Error, HMI_GetHmiIdOnScreen());
                    (void)HMI_ChangeHmi(HMI_ID_MAIN, HMI_GetHmiIdOnScreen());
                    FLAG_Set(FLAG_TEST_LEAK_SCREEN_EVENT_AUTOMATIC_STOP_CONDITION_REACHED_DUE_TO_ERROR);
                    TestLeak_ModelState = TEST_LEAK_MODEL_STATE_INIT; /* MODEL */
                    break;
                default:
                    break;
            }
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_CheckAndManageAutoTerminationCondition %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestLeak_AreSamplingTerminationConditionReached(ubyte* reached, SAMPLING_STOP_CAUSE_t* stopCause, SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK; 
    STD_RETURN_t tempReturnValue = STD_RETURN_OK; 
    ulong sampleTimeInMinutes = 0U;
    ushort SecondToTheEnd;
    ubyte messageRx = FLAG_GetAndReset(FLAG_TEST_LEAK_MODEL_EVENT_NEW_NETWORK_MESSAGE);
    
    if(messageRx != false)
    {
        debug_print("TLK NEW MEX EVENT CONSUMED");
    }
    
    if((reached != NULL) && (stopCause != NULL) && (samplingError != NULL))
    {
        returnValue = TestLeak_ErrorDetected(samplingError);
        
        if(returnValue == STD_RETURN_OK)
        {
            if(*samplingError != SAMPLING_ERROR_NONE) /* if no errors continue, otherwise stop */
            {
                *reached = true;
                *stopCause = SAMPLING_STOP_CAUSE_ERROR;
                /* samplingError already filled */
            }
            else
            {
                if(TestLeak_ModelState == TEST_LEAK_MODEL_STATE_RUNNING)
                {
                    if(messageRx != FALSE)
                    {
                        tempReturnValue = NETWORK_GetTestVoidSecondsToTheEnd(&SecondToTheEnd);
                        //debug_print("TLK NEW MEX ret %d, value %d", tempReturnValue, SecondToTheEnd);
                        if(STD_RETURN_OK == tempReturnValue)
                        {
                            if(SecondToTheEnd == 0U) /* Test finished */
                            {
                                *reached = true;
                                *stopCause = SAMPLING_STOP_CAUSE_TIME;
                                *samplingError = SAMPLING_ERROR_NONE;
                            }
                            else
                            {
                                *reached = false;
                            }
                        }
                        else
                        {
                            *reached = false;
                        }
                    }
                    else
                    {
                        *reached = false;
                    }
                }
                else
                {
                    /* not in running */
                    *reached = false;
                }
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_AreSamplingTerminationConditionReached: %d", returnValue);
    }
    
    return returnValue;
}

static STD_RETURN_t TestLeak_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    TestLeak_ModelState = TEST_LEAK_MODEL_STATE_INIT;
    TestLeak_requestStartSequence = false;
    TestLeak_requestStopSequence = false;
    TestLeak_requestDataSequence = false;
    TestLeak_ErrorDetectedOnSentMessage = false;
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_modelInit %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t TestLeak_getDataFromView(TEST_LEAK_InterfaceData_t* interfaceData)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    
    if(interfaceData != NULL)
    {
        interfaceData->voidSetPoint = WORD_TO_FLOAT(testLeakScreen_setPointVoid_value);
        interfaceData->testType = (MOTC_TEST_VOID_TYPE_t)testLeakScreen_testType_value;
        returnValue = STD_RETURN_OK;
    }
        
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_getDataFromView %d", returnValue);
    }
    return returnValue;
}

#endif /* #ifdef TEST_LEAK_THREAD_ENABLE */

#ifdef TEST_LEAK_THREAD_ENABLE
static STD_RETURN_t TestLeak_RequesteStartSequenceAutoma(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    TEST_LEAK_InterfaceData_t interfaceData;
    returnValue = TestLeak_getDataFromView(&interfaceData);
    TestLeak_MessageTxBufferLength = 0U;
    TestLeak_MessageTxBuffer[TestLeak_MessageTxBufferLength++] = MOTC_TEST_VOID_COMMAND_START;
    TestLeak_MessageTxBuffer[TestLeak_MessageTxBufferLength++] = interfaceData.testType;        
    TestLeak_MessageTxBuffer[TestLeak_MessageTxBufferLength++] = (((ushort)(interfaceData.voidSetPoint * 10.0)) & 0xFF00U) >> 8U;
    TestLeak_MessageTxBuffer[TestLeak_MessageTxBufferLength++] = (((ushort)(interfaceData.voidSetPoint * 10.0)) & 0x00FFU) >> 0U;  
    messageReturn = NETWORK_SendMessageRequest(NETWORK_COMMAND_VOID, &TestLeak_MessageTxBuffer[0U], TestLeak_MessageTxBufferLength ,&TestLeak_MessageRxBuffer[0], &TestLeak_MessageRxBufferLength);
    if(STD_RETURN_OK == messageReturn)
    {
        UTIL_PrintBufferOnHex(TestLeak_MessageRxBuffer, TestLeak_MessageRxBufferLength);
        switch(TestLeak_MessageRxBuffer[3U]) /* Command status */
        {
            case NETM_MEMORY_RETURN_OK:
                TestLeak_ModelState = TEST_LEAK_MODEL_STATE_PUMP_START;
                break;
            case NETM_MEMORY_RETURN_CONDITION_NOT_CORRECT:
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_CONDITION_NOT_CORRECT_TO_CONTINUE_THE_TEST, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
                break;
            default:
                TestLeak_ErrorDetectedOnSentMessage = true;
                break;
        }
        TestLeak_requestStartSequence = false;
    }
    else
    {
        /* Busy only busy, check network state */
        if(SYSTEM_GetState() != SYSTEM_STATE_RUNNING)
        {
            returnValue = STD_RETURN_TIMEOUT;
        }
    }
    return returnValue;
}

static STD_RETURN_t TestLeak_RequestDataSequenceAutoma(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    TestLeak_MessageTxBufferLength = 0U;
    TestLeak_MessageTxBuffer[TestLeak_MessageTxBufferLength++] = MOTC_TEST_VOID_COMMAND_GET_DATA; 
    messageReturn = NETWORK_SendMessageRequest(NETWORK_COMMAND_VOID, &TestLeak_MessageTxBuffer[0U], TestLeak_MessageTxBufferLength ,&TestLeak_MessageRxBuffer[0], &TestLeak_MessageRxBufferLength);
    if(STD_RETURN_OK == messageReturn)
    {
        UTIL_PrintBufferOnHex(TestLeak_MessageRxBuffer, TestLeak_MessageRxBufferLength);
        switch(TestLeak_MessageRxBuffer[3U]) /* Command status */
        {
            case NETM_MEMORY_RETURN_OK:
                (void)UTIL_TimerInit(&TestLeak_TimerData);
                (void)UTIL_TimerStart(&TestLeak_TimerData, TEST_LEAK_TIMER_GET_DATA_MS);
                break;
            case NETM_MEMORY_RETURN_CONDITION_NOT_CORRECT:
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_CONDITION_NOT_CORRECT_TO_CONTINUE_THE_TEST, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
                break;
            default:
                TestLeak_ModelState = TEST_LEAK_MODEL_STATE_ERROR;
                TestLeak_ErrorDetectedOnSentMessage = true;
                break;
        }
        TestLeak_requestDataSequence = false;
    }
    else
    {
        /* Busy only busy, check network state */
        if(SYSTEM_GetState() != SYSTEM_STATE_RUNNING)
        {
            returnValue = STD_RETURN_TIMEOUT;
        }
    }
    return returnValue;
}

static STD_RETURN_t TestLeak_RequestStopSequenceAutoma(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    TestLeak_MessageTxBufferLength = 0U;
    TestLeak_MessageTxBuffer[TestLeak_MessageTxBufferLength++] = MOTC_TEST_VOID_COMMAND_STOP; 
    messageReturn = NETWORK_SendMessageRequest(NETWORK_COMMAND_VOID, &TestLeak_MessageTxBuffer[0U], TestLeak_MessageTxBufferLength ,&TestLeak_MessageRxBuffer[0], &TestLeak_MessageRxBufferLength);
    if(STD_RETURN_OK == messageReturn)
    {
        UTIL_PrintBufferOnHex(TestLeak_MessageRxBuffer, TestLeak_MessageRxBufferLength);
        switch(TestLeak_MessageRxBuffer[3U]) /* Command status */
        {
            case NETM_MEMORY_RETURN_OK:
                Sampling_StopCause = SAMPLING_STOP_CAUSE_MANUAL;
                Sampling_Error = SAMPLING_ERROR_NONE;
                TestLeak_ModelState = TEST_LEAK_MODEL_STATE_PUMP_STOP;
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_TEST_STOPPED_BY_OPERATOR, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
                break;
            case NETM_MEMORY_RETURN_CONDITION_NOT_CORRECT:
                MessageScreen_openWarning(DYNAMIC_STRING_WARNING_CONDITION_NOT_CORRECT_TO_CONTINUE_THE_TEST, HMI_GetHmiIdOnScreen()); /* the stop could arrive from setup or data */
                break;
            default:
                TestLeak_ModelState = TEST_LEAK_MODEL_STATE_ERROR;
                TestLeak_ErrorDetectedOnSentMessage = true;
                break;
        }
        TestLeak_requestStopSequence = false;
    }
    else
    {
        /* Busy only busy, check network state */
        if(SYSTEM_GetState() != SYSTEM_STATE_RUNNING)
        {
            returnValue = STD_RETURN_TIMEOUT;
        }
    }
    return returnValue;
}

static STD_RETURN_t TestLeak_model(ulong execTimeMs)
{
    static ushort busyCounter = 0U;
    STD_RETURN_t returnValue = STD_RETURN_OK;
    ubyte pumpRunning = false;
    ubyte pumpRegulation = false;
    ubyte isExpired;

    /* START CODE */
    static TEST_LEAK_MODEL_STATE_t TestLeak_LastModelState = TEST_LEAK_MODEL_STATE_INIT;
    
    if(TestLeak_ModelState != TestLeak_LastModelState)
    {
        debug_print("TLK MOD STATE from %d %s to %d %s", TestLeak_LastModelState, TestLeak_modelStateNames[TestLeak_LastModelState], TestLeak_ModelState, TestLeak_modelStateNames[TestLeak_ModelState]);
        TestLeak_LastModelState = TestLeak_ModelState;
    }
    
    (void)UTIL_TimerIncrement(&TestLeak_TimerData, execTimeMs);
    
    switch(TestLeak_ModelState)
    {
        case TEST_LEAK_MODEL_STATE_INIT:
            if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_COMMAND_GO_TO_READY)  != false)
            {
                TestLeak_ModelState = TEST_LEAK_MODEL_STATE_READY;
                NETWORK_InvalidateMessageAndData(NETWORK_COMMAND_VOID);
                TestLeak_requestStartSequence = false;
                TestLeak_requestStopSequence = false;
                TestLeak_requestDataSequence = false;
                TestLeak_atLeastOneResponseDataSequence = false;
            }
            break;
        case TEST_LEAK_MODEL_STATE_READY:
            if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_COMMAND_START) != false)
            {
                NETWORK_InvalidateMessageAndData(NETWORK_COMMAND_VOID);
                TestLeak_requestStartSequence = true;
            }
            
            if(TestLeak_requestStartSequence != false)
            {
                TestLeak_RequesteStartSequenceAutoma();
            }
            else
            {
                if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_COMMAND_BACK) != false)
                {
                    TestLeak_ModelState = TEST_LEAK_MODEL_STATE_INIT;
                }
            }
            
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestLeak_CheckAndManageAutoTerminationCondition();
            }
            break;
        /*case TEST_LEAK_MODEL_STATE_WAIT_START:

            break;*/
        case TEST_LEAK_MODEL_STATE_PUMP_START:
            TestLeak_ModelState = TEST_LEAK_MODEL_STATE_RUNNING;
            TestLeak_requestDataSequence = true;
            break;
       case TEST_LEAK_MODEL_STATE_RUNNING:  
            //debug_print("RDS %d RSS %d", TestLeak_requestDataSequence, TestLeak_requestStopSequence);
            if((TestLeak_requestDataSequence == false) && (TestLeak_requestStopSequence == false))
            {
                if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_COMMAND_STOP) != FALSE)
                {
                    (void)UTIL_TimerStop(&TestLeak_TimerData);
                    (void)UTIL_TimerInit(&TestLeak_TimerData);
                    TestLeak_requestStopSequence = true;
                }
            }
            else
            {
                /* data or stop sequence called */
                if((TestLeak_requestDataSequence != false) && (TestLeak_requestStopSequence == false))
                {
                    TestLeak_RequestDataSequenceAutoma();
                }
                if((TestLeak_requestDataSequence == false) && (TestLeak_requestStopSequence != false))
                {
                    TestLeak_RequestStopSequenceAutoma();
                }
            }
            
            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestLeak_CheckAndManageAutoTerminationCondition();
            }
            break;
        /*case TEST_LEAK_MODEL_STATE_PAUSED:

            break;*/
       case TEST_LEAK_MODEL_STATE_PUMP_STOP:
            if( 
                (NETWORK_GetPumpRunning(&pumpRunning) == STD_RETURN_OK) &&
                (NETWORK_GetPumpRegulation(&pumpRegulation) == STD_RETURN_OK)
            )
            {
                if((pumpRunning == false) && (pumpRegulation == false))
                {
                    /* pump stopped and regulated */
                    TestLeak_ModelState = TEST_LEAK_MODEL_STATE_STOPPED;
                    FLAG_Set(FLAG_TEST_LEAK_SCREEN_EVENT_PUMP_STOPPED);
                }
            }
            
            if(returnValue == STD_RETURN_OK)
            {
                TestLeak_CheckAndManageAutoTerminationCondition();
            }
            break;
        case TEST_LEAK_MODEL_STATE_STOPPED:
            if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_COMMAND_START) != false)
            {
                NETWORK_InvalidateMessageAndData(NETWORK_COMMAND_VOID);
                TestLeak_requestStartSequence = true;
            }
            
            if(TestLeak_requestStartSequence != false)
            {
                TestLeak_RequesteStartSequenceAutoma();
            }
            else
            {
                if(FLAG_GetAndReset(FLAG_TEST_LEAK_SCREEN_COMMAND_BACK) != false)
                {
                    TestLeak_ModelState = TEST_LEAK_MODEL_STATE_INIT;
                }
            }     

            if(returnValue == STD_RETURN_OK)
            {
                returnValue = TestLeak_CheckAndManageAutoTerminationCondition();
            }
            break;
        case TEST_LEAK_MODEL_STATE_ERROR:

            break;
    } 
    
    (void)UTIL_TimerIsExpired(&TestLeak_TimerData, &isExpired);
    if(isExpired != false)
    {
        TestLeak_requestDataSequence = true;
        (void)UTIL_TimerInit(&TestLeak_TimerData);
        (void)UTIL_TimerStart(&TestLeak_TimerData, TEST_LEAK_TIMER_GET_DATA_MS);
    }
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] TestLeak_model %d", returnValue);
    }
    return returnValue;
}

#endif /* #ifdef TEST_LEAK_THREAD_ENABLE */

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */

void test_leak_init()
{
    (void)TestLeak_initVariable();
}

#ifdef TEST_LEAK_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void test_leak()
{
#ifdef TEST_LEAK_ENABLE_DEBUG_COUNTERS
    TestLeak_NormalRoutine++;
#endif
    (void)TestLeak_manageButtons(Execution_Normal);
    (void)TestLeak_manageScreen(Execution_Normal);
}
#endif /* #ifdef TEST_LEAK_NORMAL_ENABLE */

#ifdef TEST_LEAK_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void test_leak_fast()
{
#ifdef TEST_LEAK_ENABLE_DEBUG_COUNTERS
    TestLeak_FastRoutine++;
#endif
}
#endif /* #ifdef TEST_LEAK_FAST_ENABLE */

#ifdef TEST_LEAK_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void test_leak_low_priority()
{
#ifdef TEST_LEAK_ENABLE_DEBUG_COUNTERS
    TestLeak_LowPrioRoutine++;
#endif
}
#endif /* #ifdef TEST_LEAK_LOW_PRIO_ENABLE */

#ifdef TEST_LEAK_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void test_leak_thread()
{
    ulong execTime = TEST_LEAK_THREAD_SLEEP_MS;
    
    (void)TestLeak_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef TEST_LEAK_ENABLE_DEBUG_COUNTERS
        TestLeak_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)TestLeak_model(execTime);
    }
}
#endif /* #ifdef TEST_LEAK_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void test_leak_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void test_leak_shutdown()
//{
//}
