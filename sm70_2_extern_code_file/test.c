/*
 * test.c
 *
 *  Created on: 14/11/2019
 *      Author: Andrea Tonello
 */
#define TEST_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "report_file.h"
#include "message_screen.h"
#include "logger.h"
#include "network.h"

/* Private define ------------------------------------------------------------*/
#define HMI_MY_ID HMI_ID_TEST

/* #define TEST_ENABLE_DEBUG_COUNTERS */
#define TEST_NORMAL_ENABLE
/* #define TEST_FAST_ENABLE */
/* #define TEST_LOW_PRIO_ENABLE */
/* #define TEST_THREAD_ENABLE */

#define TEST_THREAD_SLEEP_MS 50U

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
static STD_RETURN_t Test_logFlag(FLAG_t flag);
static STD_RETURN_t Test_logUnsignedValue(sbyte* variableName, ulong value);
static STD_RETURN_t Test_logFloatValue(sbyte* variableName, ulong value);
static STD_RETURN_t Test_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength);
static STD_RETURN_t Test_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id);

static STD_RETURN_t Test_initVariable(void);
static STD_RETURN_t Test_manageButtons(ulong execTimeMs);
static STD_RETURN_t Test_manageScreen(ulong execTimeMs);
#ifdef TEST_THREAD_ENABLE
static STD_RETURN_t Test_modelInit(void);
static STD_RETURN_t Test_model(ulong execTimeMs);
#endif

/* Private functions ---------------------------------------------------------*/
static STD_RETURN_t Test_logFlag(FLAG_t flag)
{
    (void)FLAG_Set(flag);
    return Logger_logFlag(flag, true);
}

static STD_RETURN_t Test_logUnsignedValue(sbyte* variableName, ulong value)
{
    return Logger_logUnsignedValue(variableName, value);
}

static STD_RETURN_t Test_logFloatValue(sbyte* variableName, ulong value)
{
    return Logger_logFloatValue(variableName, value);
}

static STD_RETURN_t Test_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    return Logger_logString(variableName, stringScreen, stringScreenMaxLength);
} 

static STD_RETURN_t Test_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    return Logger_logComboBox(variableName, dynamic_string_id, message_id);
}

static STD_RETURN_t Test_initVariable(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Test_initVariable %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Test_manageButtons(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    if(testScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        testScreen_backButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Test_logFlag(FLAG_TEST_BUTTON_BACK);
    }
    if(testScreen_testTemperatureButton_released != false) /* These flag is reset by GUI */
    {
        testScreen_testTemperatureButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Test_logFlag(FLAG_TEST_BUTTON_TEST_TEMPERATURE);
    }
    if(testScreen_testleakButton_released != false) /* These flag is reset by GUI */
    {
        testScreen_testleakButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Test_logFlag(FLAG_TEST_BUTTON_TEST_LEAK);
    }
    if(testScreen_testflowButton_released != false) /* These flag is reset by GUI */
    {
        testScreen_testflowButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Test_logFlag(FLAG_TEST_BUTTON_TEST_FLOW);
    }
    if(testScreen_testMotorMinButton_released != false) /* These flag is reset by GUI */
    {
        testScreen_testMotorMinButton_released = false;
        BUZM_BUTTON_SOUND();
        (void)Test_logFlag(FLAG_TEST_BUTTON_TEST_MOTOR_MIN);
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Test_manageButtons %d", returnValue);
    }
    return returnValue;
}

static STD_RETURN_t Test_manageScreen(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    SYSTEM_STATE_t systemState = SYSTEM_GetState();
    HMI_ID_t actualHmiId = HMI_GetHmiIdOnScreen();
    /* START CODE */
    if(MessageScreen_isEnteredHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isEnteredHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on enter  screen */
        }
    }
    
    if(actualHmiId == HMI_MY_ID)
    {
        if(systemState != SYSTEM_STATE_RUNNING)
        {
            BUZM_startBuzzerForMsAndFrequency(BUZM_TIME_MS_ERROR_DEFAULT, BUZM_FREQUENCY_ERROR_DEFAULT);
            MessageScreen_openError(DYNAMIC_STRING_ERROR_NO_INTERNAL_COMMUNICATION, STD_ERROR_NO_COMMUNICATION, HMI_MY_ID);
            (void)HMI_ChangeHmi(HMI_ID_SETUP, HMI_MY_ID);
        }
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_BUTTON_BACK) != false)
    {
        HMI_ChangeHmi(HMI_ID_SETUP, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_BUTTON_TEST_MOTOR_MIN) != false)
    {
        debug_print("event managed: FLAG_TEST_BUTTON_TEST_MOTOR_MIN");
        HMI_ChangeHmi(HMI_ID_TEST_MIN, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_BUTTON_TEST_TEMPERATURE) != false)
    {
        debug_print("event managed: FLAG_TEST_BUTTON_TEST_TEMPERATURE");
        HMI_ChangeHmi(HMI_ID_TEST_TEMPERATURE, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_BUTTON_TEST_FLOW) != false)
    {
        debug_print("event managed: FLAG_TEST_BUTTON_TEST_FLOW");
        HMI_ChangeHmi(HMI_ID_TEST_FLOW, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_BUTTON_TEST_MOTOR_MIN) != false)
    {
        debug_print("event managed: FLAG_TEST_BUTTON_TEST_MOTOR_MIN");
        HMI_ChangeHmi(HMI_ID_TEST_MIN, HMI_MY_ID);
    }
    
    if(FLAG_GetAndReset(FLAG_TEST_BUTTON_TEST_LEAK) != false)
	{
		debug_print("event managed: FLAG_TEST_BUTTON_TEST_LEAK");
		HMI_ChangeHmi(HMI_ID_TEST_LEAK, HMI_MY_ID);
	}

    if(MessageScreen_isExitedHmi(HMI_MY_ID) != false)
    {
        if(MessageScreen_isExitedHmiFromPopup(HMI_MY_ID) != false)
        {
            ; /* MISRA*/
        }
        else
        {
            /* Code on exit screen */
        }
    }
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Test_manageScreen %d", returnValue);
    }
    return returnValue;
}

#ifdef TEST_THREAD_ENABLE
static STD_RETURN_t Test_modelInit(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Test_modelInit %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_THREAD_ENABLE */

#ifdef TEST_THREAD_ENABLE
static STD_RETURN_t Test_model(ulong execTimeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    /* START CODE */
    
    /* END CODE */
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Test_model %d", returnValue);
    }
    return returnValue;
}
#endif /* #ifdef TEST_THREAD_ENABLE */

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void test_init()
{
    (void)Test_initVariable();
}

#ifdef TEST_NORMAL_ENABLE
/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void test()
{
#ifdef TEST_ENABLE_DEBUG_COUNTERS
    Test_NormalRoutine++;
#endif
    (void)Test_manageButtons(Execution_Normal);
    (void)Test_manageScreen(Execution_Normal);
}
#endif /* #ifdef TEST_NORMAL_ENABLE */

#ifdef TEST_FAST_ENABLE
/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void test_fast()
{
#ifdef TEST_ENABLE_DEBUG_COUNTERS
    Test_FastRoutine++;
#endif
}
#endif /* #ifdef TEST_FAST_ENABLE */

#ifdef TEST_LOW_PRIO_ENABLE
/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void test_low_priority()
{
#ifdef TEST_ENABLE_DEBUG_COUNTERS
    Test_LowPrioRoutine++;
#endif    
}
#endif /* #ifdef TEST_LOW_PRIO_ENABLE */

#ifdef TEST_THREAD_ENABLE
/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
void test_thread()
{
    ulong execTime = TEST_THREAD_SLEEP_MS;
    
    (void)Test_modelInit();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
#ifdef TEST_ENABLE_DEBUG_COUNTERS
        Test_ThreadRoutine++; /* Only for debug */
#endif
        
        (void)Test_model(execTime);
    }
}
#endif /* #ifdef TEST_THREAD_ENABLE */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void test_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void test_shutdown()
//{
//}
