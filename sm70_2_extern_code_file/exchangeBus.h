#ifndef EXCHANGEBUS_H
#define EXCHANGEBUS_H
/*
 * exchangeBus.h
 *
 *  Created on: 06/11/2019
 *      Author: Andrea Tonello
 */
/* Includes ------------------------------------------------------------------*/
#include "typedef.h"

/* Private define ------------------------------------------------------------*/
#define CLIENT_FILE_CLIENT_NAME_LENTGH         40U
#define CLIENT_FILE_CLIENT_ADDRESS_LENTGH      40U
#define CLIENT_FILE_CLIENT_CAP_LENTGH          40U
#define CLIENT_FILE_CLIENT_CITY_LENTGH         40U
#define CLIENT_FILE_CLIENT_DISTRICT_LENTGH     40U
#define CLIENT_FILE_CLIENT_PLANT_NUMBER_LENTGH 40U
#define CLIENT_FILE_CLIENT_NOTE_LENTGH         200U

#define MMH2O_TO_PA_CONVERSION_NUMBER   9.80665f
#define MMH2O_TO_PA(x)                  ((float)(((float)(x)) * MMH2O_TO_PA_CONVERSION_NUMBER))
#define PA_TO_MMH2O(x)                  ((float)(((float)(x)) / MMH2O_TO_PA_CONVERSION_NUMBER))

#define KELVIN_VALUE 273.15f
#define CELSIUS_TO_KELVIN(x) (((float)(x)) + KELVIN_VALUE)
#define KELVIN_TO_CELSIUS(x) (((float)(x)) - KELVIN_VALUE)

#define SAMPLING_ISOKINETICK_SAMPLING_TIME_MAX 9999U
#define SAMPLING_ISOKINETICK_SAMPLING_TIME_MIN 1U
#define SAMPLING_ISOKINETICK_SAMPLING_VOLUME_MAX 65000U
#define SAMPLING_ISOKINETICK_SAMPLING_VOLUME_MIN 1U

#define SAMPLING_NORMALIZATION_TEMPERATURE_DEFAULT 20U
#define SAMPLING_DIAMETER_DEFAULT 100U
#define SAMPLING_FIRST_SIDE_DEFAULT 100U
#define SAMPLING_SECOND_SIDE_DEFAULT 100U

#define SAMPLING_KCOSTANT_DEFAULT 0.830f
#define SAMPLING_GAS_DENSITY_DEFAULT 1.293f
#define SAMPLING_BAROMETRIC_PRESSURE_HPA_DEFAULT 1013
#define SAMPLING_STATIC_PRESSURE_PA_DEFAULT 294.185f
#define SAMPLING_DIFFERENTIAL_PRESSURE_PA_DEFAULT 98.062f
#define SAMPLING_FWA_DEFAULT 0.995f
#define SAMPLING_CONDENSE_DEFAULT 10
#define SAMPLING_DUCT_TEMPERATURE_DEFAULT 100.0f
#define SAMPLING_ISOKINETIC_FLOW_MANUAL_VALUE_DEFAULT 1.00f

#define PIGRECO 3.1416f

#define CHARTS_GRAPH_MAX_POINTS_NUMBER 10U
#define GRAPH_MINUTE_TO_KEEP_DATA 60U
#define MODEL_CALCULATE_STATISTIC_REFRESH_COUNTER_VALUE 2U

/* Private typedef -----------------------------------------------------------*/
typedef void (*BooleanSetCallbackFct)(ubyte);
typedef ubyte (*BooleanGetCallbackFct)(void);

typedef enum GRAPH_DATA_TO_SHOW_e
{
    GRAPH_DATA_TO_SHOW_FLOW = 0,
    GRAPH_DATA_TO_SHOW_ERROR,
    GRAPH_DATA_TO_SHOW_VACUUM,
    GRAPH_DATA_TO_SHOW_METER_TEMPERATURE,
    GRAPH_DATA_TO_SHOW_MAX_NUMBER
}GRAPH_DATA_TO_SHOW_t;

typedef enum MEMORY_OPERATION_e
{
    MEMORY_OPERATION_NO_OPERATION = 0,
	MEMORY_OPERATION_ERASE,
    MEMORY_OPERATION_EXPORT,
    MEMORY_OPERATION_SINGLE_REPORT_ERASED,
    MEMORY_OPERATION_SINGLE_REPORT_VIEW
}MEMORY_OPERATION_t;

typedef enum TEST_SEND_MESSAGE_STATE_e
{
    TEST_SEND_MESSAGE_STATE_READY = 0,
    TEST_SEND_MESSAGE_STATE_WAIT_RESPONSE,
    TEST_SEND_MESSAGE_STATE_ERROR,
    TEST_SEND_MESSAGE_STATE_MAX_NUMBER
}TEST_SEND_MESSAGE_STATE_t;

typedef enum TEST_TX_RETURN_e
{
    TEST_TX_RETURN_OK = 0,
    TEST_TX_RETURN_BUSY,
    TEST_TX_RETURN_TIMEOUT,
    TEST_TX_RETURN_ERROR_TX_TOO_BUSY,
    TEST_TX_RETURN_ERROR_PARAM,
    TEST_TX_RETURN_ERROR_AUTOMA
}TEST_SEND_MESSAGE_RETURN_t;

typedef enum REPORT_TAG_e
{
    REPORT_TAG_DUMMY = 0,
    REPORT_TAG_CLIENT_ID,
    REPORT_TAG_INIT_DATA,
    REPORT_TAG_SAMPLING_START,
    REPORT_TAG_FIVE_MINUTES_DATA,
    
    REPORT_TAG_FINAL_DATA,
    REPORT_TAG_DELAYED_START,
    REPORT_TAG_START,
    REPORT_TAG_DELAYED_PAUSE,
    REPORT_TAG_RUNNING_PAUSE,
    
    REPORT_TAG_DELAYED_RESUME,
    REPORT_TAG_RUNNING_RESUMED,
    REPORT_TAG_NOZZLE_DIAMETER_CHANGED,
    REPORT_TAG_ASPIRATION_FLOW_CHANGED,
    REPORT_TAG_START_FORCED,
    
    REPORT_TAG_H24_DELAYED_START,
    REPORT_TAG_WARNING,
    REPORT_TAG_ERROR,
    REPORT_TAG_NO_POWER_SUPPLY,
    REPORT_TAG_POWER_SUPPLY_BACK,
    
    REPORT_TAG_MAX_NUMBER
}REPORT_TAG_t;

typedef enum REPORT_NAME_CODE_e
{
    REPORT_NAME_CODE_ENVIRONMENTAL = 0, 
    REPORT_NAME_CODE_PM10,          
    REPORT_NAME_CODE_DUCT,           
    REPORT_NAME_CODE_REMOTE_TSB,   
    REPORT_NAME_CODE_REMOTE_SRB,  
    REPORT_NAME_CODE_MAX_NUMBER,
}REPORT_NAME_CODE_t;

typedef struct ClientFile_Client_s
{
    sbyte name[CLIENT_FILE_CLIENT_NAME_LENTGH + 1U];
    sbyte address[CLIENT_FILE_CLIENT_ADDRESS_LENTGH + 1U];
    sbyte CAP[CLIENT_FILE_CLIENT_CAP_LENTGH + 1U];
    sbyte city[CLIENT_FILE_CLIENT_CITY_LENTGH + 1U];
    sbyte district[CLIENT_FILE_CLIENT_DISTRICT_LENTGH + 1U];
    sbyte plantNumber[CLIENT_FILE_CLIENT_PLANT_NUMBER_LENTGH + 1U];
    sbyte note[CLIENT_FILE_CLIENT_NOTE_LENTGH + 1U];
}ClientFile_Client_t;

typedef enum SAMPLING_DUCT_SHAPE_e
{
    SAMPLING_DUCT_SHAPE_CIRCLE = 0,
    SAMPLING_DUCT_SHAPE_RECTANGULAR,
    SAMPLING_DUCT_SHAPE_MAX_NUMBER
}SAMPLING_DUCT_SHAPE_t;

typedef enum SAMPLING_MEASURE_UNIT_e
{
    SAMPLING_MEASURE_UNIT_MMH20 = 0,
    SAMPLING_MEASURE_UNIT_PA,
    SAMPLING_MEASURE_UNIT_MAX_NUMBER
}SAMPLING_MEASURE_UNIT_t;

typedef enum SAMPLING_NOZZLE_DIAMETER_e
{
    SAMPLING_NOZZLE_DIAMETER_4MM = 0,
    SAMPLING_NOZZLE_DIAMETER_5MM,
    SAMPLING_NOZZLE_DIAMETER_6MM,
    SAMPLING_NOZZLE_DIAMETER_7MM,
    SAMPLING_NOZZLE_DIAMETER_8MM,
    SAMPLING_NOZZLE_DIAMETER_9MM,
    SAMPLING_NOZZLE_DIAMETER_10MM,
    SAMPLING_NOZZLE_DIAMETER_12MM,
    SAMPLING_NOZZLE_DIAMETER_14MM,
    SAMPLING_NOZZLE_DIAMETER_16MM,
	SAMPLING_NOZZLE_DIAMETER_MAX_NUMBER
}SAMPLING_NOZZLE_DIAMETER_t;

typedef struct Sampling_StatisticData_s
{
    float ErrorFlow;
    float Flow;
    float MeterTemperature;
	float Vacuum;
    float TemperatureDuctCelsius;
    float AbsoluteStaticPressurePa;
    float Speed;
    float Qv;
    float Qvn;
    float BarometricPressureMbar;
    float Condense;
    float DifferentialPressurePa;
}Sampling_StatisticData_t;	

typedef struct SAMPLING_SamplingTime_s
{
    /*   Minus          Positive
        sec  -2.147.483.648	2.147.483.647
        min  -35.791.394	35.791.394
        hour -596.523	    596.523
        day  -24.855	    24.855
        year -68	        68
        */
    slong counterSecond; /* -2147483648 sec to 2147483647, */
    ushort days;
    ubyte hours;
    ubyte minutes;
    ubyte seconds;
    ushort daysRemaining;
    ubyte hoursRemaining;
    ubyte minutesRemaining;
    ubyte secondsRemaining;
}SAMPLING_SamplingTime_t;

typedef enum SAMPLING_ISOKINETIC_SAMPLING_e
{
    SAMPLING_ISOKINETIC_SAMPLING_TIME = 0,
    SAMPLING_ISOKINETIC_SAMPLING_VOLUME
}SAMPLING_ISOKINETIC_SAMPLING_t;


typedef enum SAMPLING_ISOKINETIC_FLOW_e
{
    SAMPLING_ISOKINETIC_FLOW_AUTOMATIC = 0,
    SAMPLING_ISOKINETIC_FLOW_MANUAL
}SAMPLING_ISOKINETIC_FLOW_t;

typedef enum MOTC_MOTOR_TYPE_e
{
  MOTC_MOTOR_TYPE_PWM = 0,
  MOTC_MOTOR_TYPE_INVERTER,
  MOTC_MOTOR_TYPE_MAX_NUMBER
}MOTC_MOTOR_TYPE_t;

typedef struct REMOTE_SRB_InterfaceData_s
{
    SAMPLING_ISOKINETIC_SAMPLING_t samplingType;
    ulong time;
    ulong volume;
    ulong normalizationTemperature;
    ulong litriAspeLineaDerivata;
    ulong temperaturaContatoreLineaDerivata;
    ulong samplingStartDelayMinutes;
    SAMPLING_MEASURE_UNIT_t measureUnit;
    SAMPLING_NOZZLE_DIAMETER_t nozzleDiameter;
    SAMPLING_ISOKINETIC_FLOW_t isokinetiFlow;
    float isokinetiFlowManualValue;
}REMOTE_SRB_InterfaceData_t;

typedef struct SRB_InterfaceData_s
{
    SAMPLING_ISOKINETIC_SAMPLING_t samplingType;
    ulong timeMin;
    ulong volumeL;
    SAMPLING_DUCT_SHAPE_t ductShape;
    ulong diameterCm;
    ulong sideMajorCm;
    ulong sideMinorCm;
    ulong delay;
    float kCostant;
    float gasDensity;
    float barometricPressurePa;
    SAMPLING_MEASURE_UNIT_t measureUnit;
    float staticPressurePa;
    float differentialPressurePa;
    float fwa;
    float condensePerc;    
    ulong normalizationTemperatureCelsius;
    float ductTemperatureCelsius;
    ulong nozzleDiameterMm;
    SAMPLING_ISOKINETIC_FLOW_t isokinetiFlow;
    float isokinetiFlowManualValueLMin;
    float isoFlow;
    ulong samplingStartDelayMinutes;
    float sidestreamFlowLiterMin;
    float sidestreamDGMCelsius;
    float sidestreamFlowCalculated;
}SRB_InterfaceData_t;

typedef struct ENVIRONMENTAL_InterfaceData_s
{
    SAMPLING_ISOKINETIC_SAMPLING_t samplingType;
    ulong time;
    ulong volume;
    float aspirationFlow;
    float aspirationFlowError;
    ulong samplingStartDelayMinutes;
    ulong normalizationTemperature;
}ENVIRONMENTAL_InterfaceData_t;

typedef struct PM10_InterfaceData_s
{
    SAMPLING_ISOKINETIC_SAMPLING_t samplingType;
    ulong time;
    ulong volume;
    ulong normalizationTemperature;
    float aspirationFlow;
    float aspirationFlowError;
    ulong samplingStartDelayMinutes;
    ulong serialLogTineMin;
    ubyte h24Enable;
}PM10_InterfaceData_t;

typedef struct PM10_FinalData_s
{
    float samplingLenghtMin;
    float averageFlow;
    float averageTemperature;
    float aspiratedLiters;
    float normalizedLiters;
}PM10_FinalData_t;

typedef struct ENVIRONMENTAL_FinalData_s
{
    float samplingLenghtMin;
    float averageFlow;
    float averageTemperature;
    float aspiratedLiters;
    float normalizedLiters;
}ENVIRONMENTAL_FinalData_t;

typedef struct DUCT_FinalData_s
{
    float aspiratedLiters;
    float samplingLenghtMin;
    float averageFlowLMin;
    float averageTempDryGasCelsius;
    float normalizedLiters;
    float averageTemperatureStackCelsius;
    float averageAbsoluteStaticPressureHPa;
    float averageSpeedFwa_msec;
    float averageFlowRateS_m3h;
    float averageNormQs_m3h;
    float isokineticGradePercentual;
}DUCT_FinalData_t;

typedef struct SRB_FinalData_s
{
    float aspiratedLiters;
    float samplingLenghtMin;
    float averageFlowLMin;
    float averageTempDryGasCelsius;
    float normalizedLiters;
    float averageTemperatureStackCelsius;
    float averageAbsoluteStaticPressureHPa;
    float averageSpeedFwa_msec;
    float averageFlowRateS_m3h;
    float averageNormQs_m3h;
    float isokineticGradePercentual;
}SRB_FinalData_t;

typedef struct DUCT_InterfaceData_s
{
    SAMPLING_ISOKINETIC_SAMPLING_t samplingType;
    ulong timeMin;
    ulong volumeL;
    SAMPLING_DUCT_SHAPE_t ductShape;
    ulong diameterCm;
    ulong sideMajorCm;
    ulong sideMinorCm;
    ulong delay;
    float kCostant;
    float gasDensity;
    float barometricPressurePa;
    SAMPLING_MEASURE_UNIT_t measureUnit;
    float staticPressurePa;
    float differentialPressurePa;
    float fwa;
    ulong condensePerc;    
    ulong normalizationTemperatureCelsius;
    float ductTemperatureCelsius;
    ulong nozzleDiameterMm;
    SAMPLING_ISOKINETIC_FLOW_t isokinetiFlow;
    float isokinetiFlowManualValueLMin;
    float isoFlow;
    ulong samplingStartDelayMinutes;
}DUCT_InterfaceData_t;

typedef struct TEST_FLOW_InterfaceData_s
{
    float pumpSetPointPercentage;
}TEST_FLOW_InterfaceData_t;

typedef struct REMOTE_SRB_SetupMessage_s
{
    SAMPLING_DUCT_SHAPE_t ductType;
    ulong ductDiameter;
    ulong maxSide;
    ulong minSide;
    ulong kappa;
    ulong density;
    ulong condensate;
    float barometric;
}REMOTE_SRB_SetupMessage_t;

typedef struct REMOTE_SRB_SamplingData_s
{
    SAMPLING_MEASURE_UNIT_t measureUnit;
    float t_cont; /* temperatura contatore */
    float l;      /* litri campionati attualmente */
    float p_d;    /* pressione differenziale */
    float p_s;    /* pressione statica */
    float t_c;    /* temperatura condotto */
    float v;      /* velocit� del flusso */
}REMOTE_SRB_SamplingData_t;

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* WRITE ONLY THE VARIABLES THAT ARE EXCHANGED BETWEEN MODULES */
extern sbyte* MemoryReport_FileNameToOpen;
extern slong MemoryReport_FileStringList;

extern _FILE_PATH ReportFile_reportLocationSave;
extern _FILE_PATH ReportFile_reportLocationExport;
extern _FILE_PATH ClientFile_clientLocationSave;
extern _FILE_PATH ClientFile_clientLocationExport;

/* memory.c, memory_report.c */
#ifdef MEMORY_C /* with this define the variable could be seen only by the file.c in which before the include of this file have defined the corretc symbol*/
extern MEMORY_OPERATION_t Memory_Operation;
#endif /* MEMORY_C */

extern const sbyte* ReportFile_reportNameCodes[REPORT_NAME_CODE_MAX_NUMBER];

#ifdef CLIENT_C
extern ushort Client_ClientsTotalNumber;
extern ushort Client_SelectedOne;
#endif /* CLIENT_C */

/* TODO insert these */
extern ClientFile_Client_t ClientsFile_Clients[CLIENT_FILE_CLIENTS_MAX_NUMBER];
extern const sbyte ClientFile_FileName[];
extern const sbyte ClientFile_FileHeader[];

extern SAMPLING_STOP_CAUSE_t Sampling_StopCause;
extern SAMPLING_ERROR_t Sampling_Error;

extern REMOTE_SRB_SetupMessage_t RemoteSrb_lastSetupMessage;

extern SAMPLING_NOZZLE_DIAMETER_t ReportFile_actualNozzleDiameter;
extern SAMPLING_NOZZLE_DIAMETER_t ReportFile_lastNozzleDiameter;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
/* memory.c */
MEMORY_OPERATION_t Memory_GetOperation(void);
void Memory_SetOperation(MEMORY_OPERATION_t m);

/* client_file.c */
ushort Client_GetClientsTotalNumber(void);
void Client_SetClientsTotalNumber(ushort c);
ushort Client_GetSelectedOne(void);
void Client_SetSelectedOne(ushort s);

sbyte* ReportFile_reportGetTag(REPORT_TAG_t tag);

STD_RETURN_t Sampling_GetSamplingTime(SAMPLING_SamplingTime_t* samplingTime);
void Sampling_manageSamplingTimeForceStart(void);
void Sampling_manageSamplingTimeOnScreen(void);
void Sampling_manageSamplingRemainingTimeOnScreen(void);
void Sampling_manageSamplingTimeInit(slong startDelaySeconds);
void Sampling_manageSamplingTime(void);
ubyte Sampling_manageSamplingTimeIsInWaitStart(void);

ulong Sampling_SampleTimeInMinutes(void);
ulong Sampling_SampleVolumeInLiters(void);
void Sampling_manageIsokineticSamplingLimits(void);

float Sampling_getNormalizedLiters(float aspiratedLiters, float normalizedTemperature, float averageTemperature);

void ManageDateTimeOnScreen(void);

float Sampling_GetNozzleDiameter(SAMPLING_NOZZLE_DIAMETER_t nozzle);

float Sampling_GetFinalizedFlow(void);
float Sampling_GetFinalizedMeterTemperature(void);
float Sampling_GetFinalizedTemperatureDuctCelsius(void); 
float Sampling_GetFinalizedAbsoluteStaticPressurePa(void);
float Sampling_GetFinalizedSpeed(void);
float Sampling_GetFinalizedQv(void);
float Sampling_GetFinalizedQvn(void);
float Sampling_GetFinalizedBarometricPressureMbar(void);
float Sampling_GetFinalizedCondense(void);
float Sampling_GetFinalizedDifferentialPressurePa(void);

STD_RETURN_t Sampling_drawDataOnGraph(GRAPH_DATA_TO_SHOW_t DataToShow);
void Sampling_modelManageButtonMobileScreen(void);
void Sampling_modelInitMobileScreenPositionMoveToLeft(void);
void Sampling_modelInitMobileScreenPositionMoveToRight(void);
void Sampling_modelCalculateMobileScreenPosition(void);
void Sampling_modelInitMobileScreenPosition(void);
STD_RETURN_t Sampling_modelCalculateStatisticInit(void);

void Sampling_modelCalculateStatisticNewMinuteDetect(void);
STD_RETURN_t Sampling_modelCalculateStatisticFinalize(void);
STD_RETURN_t Sampling_modelCalculateStatisticStructInit(Sampling_StatisticData_t* sd);
STD_RETURN_t Sampling_modelCalculateStatistic(Sampling_StatisticData_t* sd);

void Sampling_ManageNozzleDiameterUpAndDownButton(void);
ulong Sampling_ManageNozzleDiameterGetColorSelected(ubyte isSelected);
float Sampling_ManageNozzleDiameterGetCalculatedFlow(SAMPLING_NOZZLE_DIAMETER_t nozzle);
void Sampling_ManageNozzleDiameterPopulate(ubyte managePlayEnable, ubyte nozzleButtonEnable, BooleanSetCallbackFct bSet);
void Sampling_ManageNozzleDiameterInit(void);
void Sampling_ManageNozzleDiameterDownButton(void);
void Sampling_ManageNozzleDiameterUpButton(void);
void Sampling_ManageNozzleDiameterSlotSelected(ulong slotSelected);
void Sampling_ManageNozzleDiameterSelectedFlow(void);

float Sampling_GetBarometricPressure(void);


ulong Get_InstrumentSerialNumber(void);

SAMPLING_ERROR_t Sampling_LowFlowErrorFct(float actualFlow, float flowLimit, ubyte runCheck);
#endif // EXCHANGEBUS_H 
