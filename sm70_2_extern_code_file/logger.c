/*
 * logger.c
 *
 *  Created on: 06/11/2019
 *      Author: Andrea Tonello
 */
#define LOGGER_C
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "buzm.h"
#include "flag.h"
#include "exchangeBus.h"
#include "logger.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define LOGGER_ENABLE_THREAD

#ifndef LOGGER_THREAD_SLEEP_MS
#define LOGGER_THREAD_SLEEP_MS 25U /* Logger must run at a double frequency of the fast screen or thread */
#endif

#define LOGGER_FILE_MAX_SIZE_BYTES (KILOBYTES_TO_BYTES(512U))

#define LOGGER_FILE_PATH USB_1_FILE_SYSTEM

#define LOGGER_TIMESTAMP_BUFFER_SIZE 24
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static ubyte logger_logCommand;
static sbyte logger_logText[LOGGER_LOG_TEXT_SIZE];
static const sbyte logger_logFileName[] = "UserLogger.log"; 
static const sbyte logger_logFileNameOld[] = "UserLoggerOld.log"; 
static sbyte Logger_TimeStampBuffer[LOGGER_TIMESTAMP_BUFFER_SIZE];

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
#ifdef LOGGER_ENABLE_THREAD
static void Logger_ModelInit(void);
static void Logger_Model(ulong execTime);
#endif /* #ifdef LOGGER_ENABLE_THREAD */

static STD_RETURN_t logger_logStringCommon(sbyte* variableName, sbyte* string, sbyte* varType);
static STD_RETURN_t Logger_LogOnFile(void);
static sbyte* Logger_GetTimeStampString(void);

/* Private functions ---------------------------------------------------------*/
static sbyte* Logger_GetTimeStampString(void)
{
    unsigned short year;
    unsigned char month;
    unsigned char day;
    unsigned char hour; 
    unsigned char min;
    unsigned char sec; 
    unsigned short millisec;
    memory_set(&Logger_TimeStampBuffer[0], 0x00, sizeof(Logger_TimeStampBuffer));
    if(system_clock_read(&year, &month, &day, &hour, &min, &sec, &millisec) < 0)
    {
        debug_print("system_clock_read failure"); /* MISRA */
    }
    else
    {
        string_snprintf(
            &Logger_TimeStampBuffer[0], 
            sizeof(Logger_TimeStampBuffer), 
            "%04d/%02d/%02d %02d:%02d:%02d:%03d",
            year,
            month,
            day,
            hour,
            min,
            sec,
            millisec
        );
    }
    return &Logger_TimeStampBuffer[0];
}

static STD_RETURN_t Logger_LogOnFile(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int log_fd;
    slong fileSize;
    if(
        (logger_logFileName != NULL) && 
        (logger_logFileNameOld != NULL) &&
        ( 
            (LOGGER_FILE_PATH == LOCAL_FILE_SYSTEM) ||
            (LOGGER_FILE_PATH == USB_1_FILE_SYSTEM) ||
            (LOGGER_FILE_PATH == USB_2_FILE_SYSTEM) ||
            (LOGGER_FILE_PATH == USB_3_FILE_SYSTEM) ||
            (LOGGER_FILE_PATH == USB_4_FILE_SYSTEM)
        )
    )
    {
        log_fd = file_open_append((sbyte*)&logger_logFileName[0], LOGGER_FILE_PATH);
        if(log_fd < 0)
        {
            log_fd = file_creat((sbyte*)&logger_logFileName[0], LOGGER_FILE_PATH);
            if(log_fd < 0)
            {
                 returnValue = STD_RETURN_ERROR_LOGGER_FILE_NOT_CREATED;
            }
        }
        
        if(returnValue == STD_RETURN_OK)
        {
            if(file_write_line(log_fd, &logger_logText[0]) > 0)
            {
                /* bytes written */
            }
            else
            {
                /* error */
                returnValue = STD_RETURN_ERROR_LOGGER_FILE_NOT_WRITTEN;
            }
        }
        
        /* check if the size reachs the limits */
        if(returnValue == STD_RETURN_OK)
        {
            fileSize = file_size((sbyte*)&logger_logFileName[0], LOGGER_FILE_PATH);
            //debug_print("LogSize %d", fileSize);
            if(fileSize > LOGGER_FILE_MAX_SIZE_BYTES)
            {  
            /*
                if(file_exists((sbyte*)&logger_logFileNameOld[0], LOGGER_FILE_PATH) 
                {
                    
                }
              */  
                if(file_copy((sbyte*)&logger_logFileName[0], LOGGER_FILE_PATH, (sbyte*)&logger_logFileNameOld[0], LOGGER_FILE_PATH) < 0)
                {
                    /* error on copy*/
                    returnValue = STD_RETURN_ERROR_LOGGER_FILE_NOT_COPIED;
                }
                else
                {
                    //debug_print("file copied");
                    if(file_remove((sbyte*)&logger_logFileName[0], LOGGER_FILE_PATH) != 0)
                    {
                        /* error on remove */
                        returnValue = STD_RETURN_ERROR_LOGGER_FILE_NOT_REMOVED;
                    }
                    else
                    {
                        /* file removed */
                        //debug_print("file removed");
                    }
                }
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Logger_LogOnFile %d", returnValue);
    }
    return returnValue;
}

#ifdef LOGGER_ENABLE_THREAD
static void Logger_ModelInit(void)
{
    memory_set(&logger_logText[0], 0x00U, sizeof(logger_logText));
    string_snprintf(&logger_logText[0],
        sizeof(logger_logText), 
        "%s IN;STARTED LOGGER - FLAG VERSION: %s", 
        Logger_GetTimeStampString(),
        FLAG_VERSION
    );
    logger_logCommand = true;
}

static void Logger_Model(ulong execTime)
{
    STD_RETURN_t returnLogger = STD_RETURN_OK;
    if(logger_logCommand != false)
    {
        returnLogger = Logger_LogOnFile();
        logger_logCommand = false;
    }
    
    if(returnLogger != STD_RETURN_OK)
    {
        debug_print("[ERRO] Logger_Model %d", returnLogger);
    }
}

#endif /* #ifdef LOGGER_ENABLE_THREAD */

static STD_RETURN_t logger_logStringCommon(sbyte* variableName, sbyte* string, sbyte* varType)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((variableName != NULL) && (string_length(variableName) > 0U) && (string != NULL))
    {
        if(logger_logCommand != false)
        {
            debug_print("[WARN] Logger_logString log in progress %s", variableName);
        }
        else
        {
            memory_set(&logger_logText[0], 0x00U, sizeof(logger_logText));
            string_snprintf(&logger_logText[0],
                sizeof(logger_logText), 
                "%s %s;%s;%s", 
                Logger_GetTimeStampString(),
                varType, 
                variableName, 
                string
            );
            logger_logCommand = true;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Logger_logString %d", returnValue);
    }
    
    return returnValue;
}

/* Public functions ----------------------------------------------------------*/
STD_RETURN_t Logger_logString(sbyte* variableName, ushort* stringScreen, ulong stringScreenMaxLength)
{
    sbyte bufferTempSbyte[LOGGER_LOG_TEXT_SIZE + 1U];
    memory_set(&bufferTempSbyte[0], 0x00U, sizeof(bufferTempSbyte));
    (void)UTIL_BufferFromUShortToSByte(stringScreen, stringScreenMaxLength, &bufferTempSbyte[0], LOGGER_LOG_TEXT_SIZE);
    //debug_print("%s %s", variableName, &bufferTempSbyte[0]);
    return logger_logStringCommon(variableName, &bufferTempSbyte[0], "ST");
} 

STD_RETURN_t Logger_logComboBox(sbyte* variableName, DYNAMIC_STRING_t dynamic_string_id, ushort message_id)
{
    sbyte bufferTempSbyte[LOGGER_LOG_TEXT_SIZE + 1U];
    memory_set(&bufferTempSbyte[0], 0x00U, sizeof(bufferTempSbyte));
    graphic_get_dynamic_string_text(&bufferTempSbyte[0], LOGGER_LOG_TEXT_SIZE, dynamic_string_id, message_id);
    return logger_logStringCommon(variableName, &bufferTempSbyte[0], "CB");
}

STD_RETURN_t Logger_logFlag(ulong flag, ulong value)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(logger_logCommand != false)
    {
        debug_print("[WARN] Logger_logFlag log in progress %d", flag);
    }
    else
    {
        memory_set(&logger_logText[0], 0x00U, sizeof(logger_logText));
        string_snprintf(&logger_logText[0],
            sizeof(logger_logText), 
            "%s FL;%d;%d", 
            Logger_GetTimeStampString(), 
            flag, 
            value
        );
        logger_logCommand = true;
    }
    return returnValue;
}

STD_RETURN_t Logger_logUnsignedValue(sbyte* variableName, ulong value)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((variableName != NULL) && (string_length(variableName) > 0U))
    {
        if(logger_logCommand != false)
        {
            debug_print("[WARN] Logger_logValue log in progress %s", variableName);
        }
        else
        {
            memory_set(&logger_logText[0], 0x00U, sizeof(logger_logText));
            string_snprintf(&logger_logText[0],
                sizeof(logger_logText), 
                "%s UV;%s;%d", 
                Logger_GetTimeStampString(), 
                variableName, 
                value
            );
            logger_logCommand = true;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Logger_logValue %d", returnValue);
    }
    
    return returnValue;
}


STD_RETURN_t Logger_logFloatValue(sbyte* variableName, ulong value)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    float floatValue = ((float)(value)) / ((float)(Fatt_corr));
    if((variableName != NULL) && (string_length(variableName) > 0U))
    {
        if(logger_logCommand != false)
        {
            debug_print("[WARN] Logger_logValue log in progress %s", variableName);
        }
        else
        {
            memory_set(&logger_logText[0], 0x00U, sizeof(logger_logText));
            string_snprintf(&logger_logText[0],
                sizeof(logger_logText), 
                "%s FV;%s;%f", 
                Logger_GetTimeStampString(), 
                variableName, 
                floatValue
            );
            logger_logCommand = true;
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Logger_logValue %d", returnValue);
    }
    
    return returnValue;
}
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void logger_init()
{

}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
//void logger()
//{
//    Logger_NormalRoutine++; /* unsigned bit word not retained */
//    /* Add code here */
//}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void logger_fast()
//{
//    Logger_FastRoutine++; /* unsigned bit word not retained */
//    /* Add code here */
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void logger_low_priority()
//{
//    Logger_LowPriorityRoutine++; /* unsigned bit word not retained */
//    /* Add code here */
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
  
#ifdef LOGGER_ENABLE_THREAD
void logger_thread()
{
    ulong execTime = LOGGER_THREAD_SLEEP_MS;
    
    Logger_ModelInit();
    
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(execTime)); /* uS */
        /* END DO NOT REMOVE */
        Logger_ThreadRoutine++; /* unsigned bit word not retained */
        
        Logger_Model(execTime);
    }
}
#endif /* #ifdef LOGGER_ENABLE_THREAD */

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void logger_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void logger_shutdown()
//{
//}
