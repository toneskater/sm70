#ifndef SYSTEM_INTERFACE_H
#define SYSTEM_INTERFACE_H

//System includes
#include "firmware_version.h"
#include "cshared.h"

#define SOFTPLC_VERSION_CHECK(major, minor, patch) ((major<<16)|(minor<<8)|(patch))

//Useful defines
#ifndef false
    #define false 0
#endif
#ifndef true
    #define true  1
#endif
#ifndef NULL
    #define NULL  0
#endif

//Bit access to words
#define BIT(address, bit)              ((address & (1<<(bit-1)))==(1<<(bit-1)))

//Macros used to convert Symbol Table Integer words to/from C unsigned short variables
#define WORD_TO_INT(address)           ((unsigned short)address)
#define INT_TO_WORD(address)           (address)

//Macros used to convert Symbol Table Float words to/from C float variables
#define WORD_TO_FLOAT(address)         ((float)((float)address/(float)Fatt_corr))
#define FLOAT_TO_WORD(address)         ((int)((float)address*(float)Fatt_corr))

//Macros used to convert Symbol Table Float words to/from C double variables
#define WORD_TO_DOUBLE(address)        ((double)((double)address/(double)Fatt_corr))
#define DOUBLE_TO_WORD(address)        ((int)((double)address*(double)Fatt_corr))

//Direct access to PLC's digital I/Os
//
//Warning: 'number' argument starts from ZERO. Here are some examples:
//IA1 read:     INPUT_GET(0)
//IB1 read:     INPUT_GET(16)
//UA1 write:    OUTPUT_SET(0)
//UC1 write:    OUTPUT_SET(32)
#define INPUT_GET(number)              (BIT(in[number/16], (number%16)+1))
#define OUTPUT_SET(number, value)      { if(!value) { out[number/16]&=~(0x01<<(number%16)); } else { out[number/16]|=(0x01<<(number%16)); } }
#define OUTPUT_GET(number)             (number<16 ? (BIT(digout,(number%16)+1)) : (number<32 ? (BIT(digout2,(number%16)+1)) : (number<48 ? (BIT(digout3,(number%16)+1)) : (number<64 ? (BIT(digout4,(number%16)+1)) : (number < 80 ? (BIT(digout5,(number%16)+1)) : (BIT(out[number/16],(number%16)+1)))))))


#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x152200

//Threads handling
#define THREAD_LOOP(obj)        \
    if(THREAD_CLOSING()) {          \
        return obj;                 \
    }                               \
    thread_usleep(100);

#define THREAD_CLOSING()        (c_thread_object_stop!=0)

#endif


#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x141100

/* File access functions
 *
 * Every function returns negative value in case of error
 *
 * Argument _FILE_PATH path must be used with the following typedef:
 * typedef enum
 * {
 *     //Common
 *     LOCAL_FILE_SYSTEM        =0,
 *     NETWORK_FILE_SYSTEM      =2,

 *     //TOP target
 *     USB_FILE_SYSTEM          =1, (USB_FRONT_FILE_SYSTEM is available since
 *                                   firmware version 0.4.0)
 *     USB_REAR_FILE_SYSTEM     =3,
 *
 *     //SMART target
 *     USB_1_FILE_SYSTEM        =1,
 *     USB_2_FILE_SYSTEM        =3,
 *     USB_3_FILE_SYSTEM        =4,
 *     USB_4_FILE_SYSTEM        =5,
 *
 * } _FILE_PATH;
 */
#define file_open          f_pnt_file_open              /* int fd file_open(char *name, _FILE_PATH path); */
#if !defined(SOFTPLC_VERSION) || (SOFTPLC_VERSION >= SOFTPLC_VERSION_CHECK(0, 4, 0))
    #define file_open_append   f_pnt_file_open_append       /* int fd file_open_append(char *name, _FILE_PATH path); */
#endif
#define file_creat         f_pnt_file_creat             /* int fd file_creat(char *name, _FILE_PATH path) */
#define file_close         f_pnt_file_close             /* int    file_close(int fd) */
#define file_remove        f_pnt_file_remove            /* int    file_remove(char *name, _FILE_PATH path) */
#define file_read_line     f_pnt_file_read_line         /* int    file_read_line(int fd, char *line, int line_max_size) */
#define file_write_line    f_pnt_file_write_line        /* int    file_write_line(int fd, char *line) */
#define file_read          f_pnt_file_read              /* int    file_read(int fd, char *buffer, int size) */
#define file_write         f_pnt_file_write             /* int    file_write(int fd, char *buffer, int size) */
#define file_copy          f_pnt_file_copy              /* int    file_copy(char *src_name, _FILE_PATH src_path, char *dest_name, _FILE_PATH dest_path) */
#define file_rename        f_pnt_file_rename            /* int    file_rename(char *old_name, char *new_name, _FILE_PATH path) */
#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x151200
    #define file_get_path      f_pnt_file_get_path          /* int    file_get_path(char *path_string, int path_size, _FILE_PATH path) - path_string will be '/' terminated */
    #define file_get_path_size f_pnt_file_get_path_size     /* int    file_get_path_size(_FILE_PATH path) */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x173800
    #define file_size          f_pnt_file_size              /* int    file_size(char *name, _FILE_PATH path); */
    #define file_exists        f_pnt_file_exists            /* int    file_exists(char *name, _FILE_PATH path); */
#endif
#if !defined(SOFTPLC_VERSION) || (SOFTPLC_VERSION >= SOFTPLC_VERSION_CHECK(0, 4, 0))
    #define file_seek_beginning f_pnt_file_seek_beginning   /* int    file_seek_beginning(int fd); */
    #define file_seek_end      f_pnt_file_seek_end          /* int    file_seek_end(int fd); */
    #define file_seek_absolute f_pnt_file_seek_absolute     /* int    file_seek_absolute(int fd, unsigned int pos); */
    #define file_seek_relative f_pnt_file_seek_relative     /* int    file_seek_relative(int fd, int offset); */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x151200
    /* File access functions
    *
    * These functions MUST be executed in low priority sections
    *
    * In "extension" argument use commas to select multiple extensions. e.g. "png, jpg, bmp"
    */
    #define file_open_select   f_pnt_file_open_select               /* int    file_open_select(char *name, int name_size, char *extension, _FILE_PATH path) */
    #define file_save_select   f_pnt_file_save_select               /* int    file_save_select(char *name, int name_size) */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x184100
    #define file_remove_multiselect f_pnt_file_remove_multiselect   /* int file_remove_multiselect(char *extension, _FILE_PATH path) */
    #define file_copy_multiselect f_pnt_file_copy_multiselect       /* int file_copy_multiselect(char *extension, _FILE_PATH sourcePath, _FILE_PATH destinationPath) */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x162000
    /* File group handling functions
    *
    * These functions MUST be executed in low priority sections
    *
    * In "extensions" argument use commas to select multiple extensions. e.g. "png, jpg, bmp"
    * Since version 0.6.0 wildcards characters '*' and '?' are allowed as well. e.g. "file?.*"
    *
    * Functions file_list_get() and file_list_get_multiselect() returns a char* list.
    * WARNING: after usage list MUST be deleted in order to avoid memory leaks
    *
    */
    #define file_list_get      f_pnt_file_list_get          /* int list_id file_list_get(char *extensions, _FILE_PATH path) */
    #define file_list_size_get f_pnt_file_list_size_get     /* int         file_list_size_get(char *extensions, _FILE_PATH path) */
    #define file_list_file_get f_pnt_file_list_file_get     /* int         file_list_file_get(int index, char *name, int name_size, char *extensions, _FILE_PATH path) */
    #define file_list_copy     f_pnt_file_list_copy         /* int         file_list_copy(_FILE_PATH src_path, _FILE_PATH dest_path, char *extensions) */
    #define file_list_remove   f_pnt_file_list_remove       /* int         file_list_remove(char *extensions, _FILE_PATH path) */
    #if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x185100
        #define file_list_get_multiselect   f_pnt_file_list_get_multiselect /* int list_id file_list_get_multiselect(char *extensions, _FILE_PATH path) */
    #endif // FIRMWARE_VERSION >= 0x185100
#endif // FIRMWARE_VERSION >= 0x162000


/* Strings manipulation functions
 *
 * Every function has the same prototype of the correspondent ANSI C function
 *
 * WARNING: comparison made by string_compare() function is case sensitive (ABC is different from abc)
 */
#define string_sprintf     f_pnt_string_sprintf         /* sprintf -  int    string_sprintf(char *s, const char *format, ...) */
#if !defined(SOFTPLC_VERSION) || (SOFTPLC_VERSION >= SOFTPLC_VERSION_CHECK(0, 4, 0))
    #define string_snprintf    f_pnt_string_snprintf    /* snprintf -  int   snprintf (char *s, unsigned int n, const char *format, ...) */
#endif
#define string_concatenate f_pnt_string_concatenate     /* strcat  -  char*  string_concatenate(char *dest_str, char *src_str) */
#define string_length      f_pnt_string_length          /* strlen  -  int    string_length(char *str) */
#define string_compare     f_pnt_string_compare         /* strcmp  -  int    string_compare(char *str_1, char *str_2) */
#define string_copy        f_pnt_string_copy            /* strcpy  -  char*  string_copy(char *dest_str, char *src_str) */
#define string_atoi        f_pnt_string_atoi            /* atoi    -  int    string_atoi(char *str) */
#define string_atof        f_pnt_string_atof            /* atof    -  double string_atof(char *str) */
#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x180200
    #define string_atol        f_pnt_string_atol            /* atol    -  long int string_atol(char *str) */
    #define string_atoll       f_pnt_string_atoll           /* atoll   -  long long int string_atoll(char *str) */
    #define string_tod         f_pnt_string_tod             /* strtod  -  double string_tod(const char *str, char **endptr) */
    #define string_told        f_pnt_string_told            /* strtold -  long double string_told(const char* str, char** endptr) */
    #define string_tof         f_pnt_string_tof             /* strtof  -  float  string_tof(const char *str, char **endptr) */
    #define string_tol         f_pnt_string_tol             /* strtol  -  long int string_tol(const char* str, char** endptr, int base) */
    #define string_toll        f_pnt_string_toll            /* strtoll -  long long int string_toll(const char* str, char** endptr, int base) */
    #define string_toul        f_pnt_string_toul            /* strtoul -  unsigned long int string_toul(const char* str, char** endptr, int base) */
    #define string_toull       f_pnt_string_toull           /* strtoull-  unsigned long long int string_toull(const char* str, char** endptr, int base) */
#endif
#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x181200
    #define string_str         f_pnt_string_str             /* strstr  -  char*  string_str(char *str1, char *str2) */
    #define string_chr         f_pnt_string_chr             /* strchr  -  char*  string_chr(char *str, int character) */
    #define string_rchr        f_pnt_string_rchr            /* strrchr -  char*  string_rchr(char *str, int character) */
    #define string_spn         f_pnt_string_spn             /* strspn  -  int    string_spn(char *str1, char *str2) */
    #define string_cspn        f_pnt_string_cspn            /* strcspn -  int    string_cspn(char *str1, char *str2) */
    #define string_tok         f_pnt_string_tok             /* strtok  -  char*  string_tok(char *str, char *delimiters) */
    #define string_pbrk        f_pnt_string_pbrk            /* strpbrk -  char*  string_pbrk(char *str1, char *str2) */
    #define string_ncpy        f_pnt_string_ncpy            /* strncpy -  char*  string_ncpy(char *dest_str, char *src_str, int num) */
    #define string_ncat        f_pnt_string_ncat            /* strncat -  char*  string_ncat(char *dest_str, char *src_str, int num) */
    #define string_ncmp        f_pnt_string_ncmp            /* strncmp -  int    string_ncmp(char *str1, char* str2, int num) */
#endif
#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x184100
    #define string_trim        f_pnt_string_trim            /* strncmp -  int    string_trim(char *str) */
#endif


/* Math functions
 *
 * Every function has the same prototype of the correspondent ANSI C function
 */
#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x180200
    #define math_div           f_pnt_math_div               /* div     -  div_t math_div(int numer, int denom) */
    #define math_ldiv          f_pnt_math_ldiv              /* ldiv    -  ldiv_t math_ldiv(long int numer, long int denom) */
    #define math_lldiv         f_pnt_math_lldiv             /* lldiv   -  lldiv_t math_lldiv(long long int numer, long long int denom) */
#endif


/* Memory manipulation functions
 *
 * Every function has the same prototype of the correspondent ANSI C function
 */
#define memory_set         f_pnt_memory_set             /* memset  -  void* memory_set(void *mem, int val, int n_bytes) */
#define memory_compare     f_pnt_memory_compare         /* memcmp  -  int   memory_compare(const void *mem_1, const void *mem_2, int n_bytes) */
#define memory_move        f_pnt_memory_move            /* memmove -  void* memory_move(void *dest_mem, const void *src_mem, int n_bytes) */
#define memory_copy        f_pnt_memory_copy            /* memcpy  -  void* memory_copy(void *dest_mem, const void *src_mem, int n_bytes) */

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x150400
    #define memory_alloc       f_pnt_memory_alloc           /* malloc  -  void* memory_alloc(int size) */
    #define memory_calloc      f_pnt_memory_calloc          /* calloc  -  void* memory_calloc(int num, int size) */
    #define memory_free        f_pnt_memory_free            /* free    -  void  memory_free(void *ptr) */
    #define memory_realloc     f_pnt_memory_realloc         /* realloc -  void* memory_realloc(void *ptr, int size) */
#endif


#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x162000
    /* List functions
    *
    * Every function returns negative value in case of error
    *
    */

    #define list_create        f_pnt_list_create            /* int id list_create(void) */
    #define list_clear         f_pnt_list_clear             /* int    list_clear(int id) */
    #define list_size          f_pnt_list_size              /* int    list_size(int id) */
    #define list_empty         f_pnt_list_empty             /* int    list_empty(int id) */
    #define list_append        f_pnt_list_append            /* int    list_append(int id, void *item) */
    #define list_item_at       f_pnt_list_item_at           /* void  *list_item_at(int id, int index) */
    #define list_item_insert   f_pnt_list_item_insert       /* int    list_item_insert(int id, void *item, int index) */
    #define list_item_replace  f_pnt_list_item_replace      /* int    list_item_replace(int id, void *item, int index) */
    #define list_item_remove   f_pnt_list_item_remove       /* int    list_item_remove(int id, int index) */
    #define list_delete        f_pnt_list_delete            /* int    list_delete(int id) */
    #define list_delete_all    f_pnt_list_delete_all        /* int    list_delete_all(void) */
#endif // FIRMWARE_VERSION >= 0x162000


#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x150800
/* Graphic objects manipulation functions
 *
 * Every function returns negative value in case of error
 *
 */

/* Screen size functions
 */
#define graphic_get_screen_width            f_pnt_graphic_get_screen_width              /* int    graphic_get_screen_width(void) */
#define graphic_get_screen_height           f_pnt_graphic_get_screen_height             /* int    graphic_get_screen_height(void) */

/* Functions applying on all objects
 */
#define graphic_objects_delete_all          f_pnt_graphic_objects_delete_all            /* int    graphic_objects_delete_all(void) */
#define graphic_objects_show_all            f_pnt_graphic_objects_show_all              /* int    graphic_objects_show_all(void) */
#define graphic_objects_hide_all            f_pnt_graphic_objects_hide_all              /* int    graphic_objects_hide_all(void) */
#define graphic_objects_number              f_pnt_graphic_objects_number                /* int    graphic_objects_number(void) */

/* Functions that can be used on every graphic object
 *
 * Main colors definitions
 */

#define BLACK              0
#define DARKBLUE           1
#define DARKGREEN          2
#define LIGHTBLUE          3
#define DARKRED            4
#define VIOLET             5
#define DARKYELLOW         6
#define LIGHTGRAY          7
#define DARKGRAY           8
#define BLUE               9
#define GREEN              10
#define CYAN               11
#define RED                12
#define PINK               13
#define YELLOW             14
#define WHITE              15
#define GRAY               24
// 16 -> 31   ... From BLACK  to WHITE (grey scale)
// 32 -> 47   ... From BLACK  to RED
// 48 -> 63   ... From BLACK  to GREEN
// 64 -> 79   ... From BLACK  to BLUE
// 80 -> 95   ... From BLUE   to CYAN
// 96 -> 111  ... From CYAN   to GREEN
// 112 -> 127 ... From GREEN  to YELLOW
// 128 -> 143 ... From YELLOW to RED
// 144 -> 159 ... From RED    to VIOLET
// 160 -> 175 ... From VIOLET to BLUE
// 176 -> 191 ... From BLACK  to VIOLET
// 192 -> 207 ... From BLACK  to CYAN
// 208 -> 225 ... From RED    to WHITE
// 226 -> 241 ... From GREEN  to WHITE
// 242 -> 255 ... From BLUE   to WHITE

/* Functions that can be used on every graphic object
 *
 *
 * typedef enum
 * {
 *     OBJECT_NONE          = 0,
 *     OBJECT_LINE          = 1,
 *     OBJECT_RECT          = 2,
 *     OBJECT_ELLIPSE       = 3,
 *     OBJECT_IMAGE         = 4,
 *     OBJECT_TEXT          = 5,
 *     OBJECT_ARC           = 6,
 *     OBJECT_ECALC         = 7,
 *     OBJECT_PATH          = 8,
 *     OBJECT_VIEWER        = 9,
 *     OBJECT_ISO_VIEWER    = 10,
 *     OBJECT_BITMAP        = 11,
 *
 *
 * } _OBJECT_TYPE;
 *
 */

#define graphic_object_get_type             f_pnt_graphic_object_get_type               /* _OBJECT_TYPE  graphic_object_get_type(int id) */
#define graphic_object_delete               f_pnt_graphic_object_delete                 /* int    graphic_object_delete(int id) */
#define graphic_object_show                 f_pnt_graphic_object_show                   /* int    graphic_object_show(int id) */
#define graphic_object_hide                 f_pnt_graphic_object_hide                   /* int    graphic_object_hide(int id) */
#define graphic_object_set_visible          f_pnt_graphic_object_set_visible            /* int    graphic_object_set_visible(int id, unsigned char visible) */
#define graphic_object_set_filled           f_pnt_graphic_object_set_filled             /* int    graphic_object_set_filled(int id, unsigned char filled) */
#define graphic_object_move                 f_pnt_graphic_object_move                   /* int    graphic_object_move(int id, int x, int y) */
#define graphic_object_set_x                f_pnt_graphic_object_set_x                  /* int    graphic_object_move(int id, int x) */
#define graphic_object_set_y                f_pnt_graphic_object_set_y                  /* int    graphic_object_move(int id, int y) */
#define graphic_object_set_main_color       f_pnt_graphic_object_set_main_color         /* int    graphic_object_set_main_color(int id, int color) */
#define graphic_object_set_secondary_color  f_pnt_graphic_object_set_secondary_color    /* int    graphic_object_set_secondary_color(int id, int color) */
#define graphic_object_set_geometry         f_pnt_graphic_object_set_geometry           /* int    graphic_object_set_geometry(int id, int x, int y, int w, int h) */
#define graphic_object_set_size             f_pnt_graphic_object_set_size               /* int    graphic_object_set_size(int id, int w, int h) */
#define graphic_object_set_width            f_pnt_graphic_object_set_width              /* int    graphic_object_set_width(int id, int w) */
#define graphic_object_set_height           f_pnt_graphic_object_set_height             /* int    graphic_object_set_height(int id, int h) */
#define graphic_object_set_tag              f_pnt_graphic_object_set_tag                /* int    graphic_object_set_tag(int id, int tag) */
#define graphic_object_get_tag              f_pnt_graphic_object_get_tag                /* int    graphic_object_get_tag(int id) */

/* Straight line object
 * - move actions refers to top-left corner of the line bounding rect
 */
#define graphic_line_create                 f_pnt_graphic_line_create                   /* int id graphic_line_create(int x1, int y1, int x2, int y2, int main_color, unsigned char visible) */
#define graphic_line_modify                 f_pnt_graphic_line_modify                   /* int    graphic_line_modify(int id, int x1, int y1, int x2, int y2) */
#define graphic_line_set_p1                 f_pnt_graphic_line_set_p1                   /* int    graphic_line_set_p1(int id, int x1, int y1) */
#define graphic_line_set_p2                 f_pnt_graphic_line_set_p2                   /* int    graphic_line_set_p2(int id, int x2, int y2) */

/* Rectangle object
 * - object position refers to its top-left corner
 *
 * - main color is filling color
 * - secondary_color is border color
 */
#define graphic_rect_create                 f_pnt_graphic_rect_create                   /* int id graphic_rect_create(int x, int y, int w, int h, int main_color, unsigned char filled, int secondary_color, unsigned char visible) */

/* Ellipse (circle) object
 * - object position refers to its center
 *
 * - main color is filling color
 * - secondary_color is border color
 */
#define graphic_ellipse_create              f_pnt_graphic_ellipse_create                /* int id graphic_ellipse_create(int center_x, int center_y, int w, int h, int main_color, unsigned char filled, int secondary_color, unsigned char visible) */

/* Image object
 * - object position refers to its top-left corner
 */
#define graphic_image_create                f_pnt_graphic_image_create                  /* int id graphic_image_create(int x, int y, int w, int h, int image_id, unsigned char scaled, unsigned char visible) */
#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x164300
    #define graphic_image_get_width             f_pnt_graphic_image_get_width               /* int    graphic_image_get_width(int image_id) */
    #define graphic_image_get_height            f_pnt_graphic_image_get_height              /* int    graphic_image_get_height(int image_id) */
    #define graphic_image_create_from_file      f_pnt_graphic_image_create_from_file        /* int id graphic_image_create_from_file(int x, int y, int w, int h, char *name, _FILE_PATH path, unsigned char scaled, unsigned char visible) */
    #define graphic_image_get_width_from_file   f_pnt_graphic_image_get_width_from_file     /* int    graphic_image_get_width_from_file(char *name, _FILE_PATH path) */
    #define graphic_image_get_height_from_file  f_pnt_graphic_image_get_height_from_file    /* int    graphic_image_get_height_from_file(char *name, _FILE_PATH path) */
#endif // FIRMWARE_VERSION >= 0x164300
#define graphic_image_modify                f_pnt_graphic_image_modify                  /* int    graphic_image_modify(int id, int image_id) */

/* Text object
 * - object position refers to its top-left corner
 *
 * - main color is text_color
 * - secondary_color is background color
 * - NULL font = default font
 *
 * In the alignment parameter, use enum below
 */
enum AlignmentFlag {
    AlignLeft          = 0x0001,
    AlignRight         = 0x0002,
    AlignHCenter       = 0x0004,
    AlignJustify       = 0x0008,
    AlignAbsolute      = 0x0010,

    AlignTop           = 0x0020,
    AlignBottom        = 0x0040,
    AlignVCenter       = 0x0080,

    AlignTopLeft       = AlignTop | AlignLeft,
    AlignTopCenter     = AlignTop | AlignHCenter,
    AlignTopRight      = AlignTop | AlignRight,
    AlignCenterLeft    = AlignVCenter | AlignLeft,
    AlignCenter        = AlignVCenter | AlignHCenter,
    AlignCenterRight   = AlignVCenter | AlignRight,
    AlignBottomLeft    = AlignBottom | AlignLeft,
    AlignBottomCenter  = AlignBottom | AlignHCenter,
    AlignBottomRight   = AlignBottom | AlignRight
};

#define graphic_text_create                 f_pnt_graphic_text_create                   /* int id graphic_text_create(int x, int y, int w, int h, char *text, int text_color, unsigned char transparency, int background_color, char *font, int point_size, unsigned char bold, unsigned char italic, int alignment, unsigned char visible) */
#define graphic_text_text_modify            f_pnt_graphic_text_text_modify              /* int    graphic_text_text_modify(int id, char *text) */
#define graphic_text_font_modify            f_pnt_graphic_text_font_modify              /* int    graphic_text_font_modify(int id, char *font, int point_size, unsigned char bold, unsigned char italic) */
#define graphic_text_alignment_modify       f_pnt_graphic_text_alignment_modify         /* int    graphic_text_alignment_modify(int id, int alignment) */
#define graphic_text_transparency_modify    f_pnt_graphic_text_transparency_modify      /* int    graphic_text_transparency_modify(int id, int transparency) */

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x162800
    #define graphic_text_dynamic_string_create  f_pnt_graphic_text_dynamic_string_create    /* int id graphic_text_dynamic_string_create(int x, int y, int w, int h, int dynamic_string_id, int message_id, int text_color, unsigned char transparency, int background_color, char *font, int point_size, unsigned char bold, unsigned char italic, int alignment, unsigned char visible) */
#endif // FIRMWARE_VERSION >= 0x162800

/* Arc object
 * - object position refers to its center
 * - angles are in degrees
 * - 0 degrees refers to X+ direction
 *
 * - main color is arc color
 */
#define graphic_arc_create                  f_pnt_graphic_arc_create                    /* int id graphic_arc_create(int center_x, int center_y, int w, int h, float start_angle, float end_angle, int main_color, unsigned char visible) */
#define graphic_arc_modify                  f_pnt_graphic_arc_modify                    /* int    graphic_arc_modify(int id, float start_angle, float end_angle) */

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x160900
    /* Calculator object
    * - object position refers to its top-left corner
    *
    */
    #define graphic_calc_create             f_pnt_graphic_calc_create                   /* int id graphic_calc_create(int x, int y, int w, int h, unsigned char visible) */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x161600
    #define graphic_path_create                 f_pnt_graphic_path_create                   /* int id graphic_path_create(int color, unsigned char filled, int border_color, unsigned char visible) */
    #define graphic_path_set_starting_point     f_pnt_graphic_path_set_starting_point       /* int    graphic_path_set_starting_point(int id, float start_x, float start_y) */
    #define graphic_path_add_line               f_pnt_graphic_path_add_line                 /* int    graphic_path_add_line(int id, float destination_x, float destination_y) */
    #define graphic_path_add_arc                f_pnt_graphic_path_add_arc                  /* int    graphic_path_add_arc(int id, float center_x, float center_y, float w, float h, float start_angle, float end_angle) */
    #define graphic_path_draw                   f_pnt_graphic_path_draw                     /* int    graphic_path_draw(int id) */
#endif // FIRMWARE_VERSION >= 0x161600

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x162300
    #define graphic_view_create                 f_pnt_graphic_view_create                   /* int id graphic_view_create(int x, int y, int w, int h, int color, unsigned char visible) */
    #define graphic_view_zoom_in                f_pnt_graphic_view_zoom_in                  /* int    graphic_view_zoom_in(int id, int level) */
    #define graphic_view_zoom_out               f_pnt_graphic_view_zoom_out                 /* int    graphic_view_zoom_out(int id, int level) */
    #define graphic_view_fit_content            f_pnt_graphic_view_fit_content              /* int    graphic_view_fit_content(int id) */
    #define graphic_view_fit_object             f_pnt_graphic_view_fit_object               /* int    graphic_view_fit_object(int id, int id_object) */
    #define graphic_view_fit_rect               f_pnt_graphic_view_fit_rect                 /* int    graphic_view_fit_rect(int id, float x, float y, float w, float h) */

    /*
    typedef enum
    {
    DIRECTION_LEFT  = 0,
    DIRECTION_RIGHT = 1,
    DIRECTION_UP    = 2,
    DIRECTION_DOWN  = 3

    } _DIRECTION_TYPE;
    */
    #define graphic_view_move                   f_pnt_graphic_view_move                     /* int    graphic_view_move(int id, int direction) */
    #define graphic_view_set_move_enable        f_pnt_graphic_view_set_move_enable          /* int    graphic_view_set_move_enable(int id, unsigned char h_enable, unsigned char v_enable) */
    #define graphic_view_clear                  f_pnt_graphic_view_clear                    /* int    graphic_view_clear(int id) */

    #define graphic_view_add_line               f_pnt_graphic_view_add_line                 /* int id graphic_view_add_line(int id_viewer, float x1, float y1, float x2, float y2, int color, unsigned char visible) */
    #define graphic_view_add_rect               f_pnt_graphic_view_add_rect                 /* int id graphic_view_add_rect(int id_viewer, float x, float y, float w, float h, int color, unsigned char filled, int border_color, unsigned char visible) */
    #define graphic_view_add_ellipse            f_pnt_graphic_view_add_ellipse              /* int id graphic_view_add_ellipse(int id_viewer, float center_x, float center_y, float w, float h, int color, unsigned char filled, int border_color, unsigned char visible) */
    #define graphic_view_add_image              f_pnt_graphic_view_add_image                /* int id graphic_view_add_image(int id_viewer, float x, float y, float w, float h, int image_id, unsigned char scaled, unsigned char visible) */
    #define graphic_view_add_text               f_pnt_graphic_view_add_text                 /* int id graphic_view_add_text(int id_viewer, float x, float y, float w, float h, char *text, int text_color, unsigned char transparency, int background_color, char *font, int point_size, unsigned char bold, unsigned char italic, int alignment, unsigned char visible) */
    #define graphic_view_add_arc                f_pnt_graphic_view_add_arc                  /* int id graphic_view_add_arc(int id_viewer, float center_x, float center_y, float w, float h, float start_angle, float end_angle, int main_color, unsigned char visible) */
    #define graphic_view_add_path               f_pnt_graphic_view_add_path                 /* int id graphic_view_add_path(int id_viewer, int color, unsigned char filled, int border_color, unsigned char visible) */

#endif // FIRMWARE_VERSION >= 0x162300

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x180901
    /* Bitmap object
    *
    * - Bitmap is refreshed only when function graphic_bitmap_update()
    *   is called. Here is an example:
    *     id = graphic_bitmap_create(...);
    *     graphic_bitmap_draw_line(id, ...);
    *     graphic_bitmap_draw_rect(id, ...);
    *     ...
    *     graphic_bitmap_update(id);
    *
    * - Bitmap resizing keep background color
    * - graphic_bitmap_set_area() and graphic_bitmap_get_area() lets
    *   only backup and restore exactly the same bitmap region.
    *
    *   This is a correct example:
    *   graphic_bitmap_get_area(id, 20, 10, 300, 200);
    *   doSomething();
    *   graphic_bitmap_set_area(id, 20, 10, 300, 200);
    *
    *   This example will not work:
    *   graphic_bitmap_get_area(id, 20, 10, 300, 200);
    *   doSomething();
    *   graphic_bitmap_set_area(id, 50, 60, 300, 200);
    *
    *   If you need to move a region into another use graphic_bitmap_copy_area()
    *   instead.
    */
    #define graphic_bitmap_create               f_pnt_graphic_bitmap_create                 /* int id graphic_bitmap_create(int x, int y, int w, int h, unsigned char visible) */
    #define graphic_bitmap_draw_background      f_pnt_graphic_bitmap_draw_background        /* int    graphic_bitmap_draw_background(int id, int background_color) */
    #define graphic_bitmap_draw_pixel           f_pnt_graphic_bitmap_draw_pixel             /* int    graphic_bitmap_draw_pixel(int id, int x, int y, int color) */
    #define graphic_bitmap_get_pixel_color      f_pnt_graphic_bitmap_get_pixel_color        /* int    graphic_bitmap_get_pixel_color(int id, int x, int y) */
    #define graphic_bitmap_set_area             f_pnt_graphic_bitmap_set_area               /* int    graphic_bitmap_set_area(int id, int x, int y, int w, int h) */
    #define graphic_bitmap_get_area             f_pnt_graphic_bitmap_get_area               /* int    graphic_bitmap_get_area(int id, int x, int y, int w, int h) */
    #define graphic_bitmap_copy_area            f_pnt_graphic_bitmap_copy_area              /* int    graphic_bitmap_copy_area(int id, int x, int y, int w, int h, int x_new, int y_new) */
    #define graphic_bitmap_draw_line            f_pnt_graphic_bitmap_draw_line              /* int    graphic_bitmap_draw_line(int id, int x1, int y1, int x2, int y2, int color, int thickness) */
    #define graphic_bitmap_draw_rect            f_pnt_graphic_bitmap_draw_rect              /* int    graphic_bitmap_draw_rect(int id, int x, int y, int w, int h, int color, unsigned char filled, int border_color, unsigned char smooth) */
    #define graphic_bitmap_draw_ellipse         f_pnt_graphic_bitmap_draw_ellipse           /* int    graphic_bitmap_draw_ellipse(int id, int center_x, int center_y, int w, int h, int color, unsigned char filled, int border_color) */
    #define graphic_bitmap_draw_arc             f_pnt_graphic_bitmap_draw_arc               /* int    graphic_bitmap_draw_arc(int id, int center_x, int center_y, int w, int h, float start_angle, float end_angle, int color, int thickness) */
    #define graphic_bitmap_draw_image           f_pnt_graphic_bitmap_draw_image             /* int    graphic_bitmap_draw_image(int id, int x, int y, int image_id) */
    #define graphic_bitmap_draw_text            f_pnt_graphic_bitmap_draw_text              /* int    graphic_bitmap_draw_text(int id, int x, int y, int w, int h, char *text, int text_color, unsigned char transparency, int background_color, char *font, int point_size, unsigned char bold, unsigned char italic, int alignment) */
    #define graphic_bitmap_update               f_pnt_graphic_bitmap_update                 /* int    graphic_bitmap_update(int id) */

    #if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x182000
        #define graphic_bitmap_draw_dynamic_string  f_pnt_graphic_bitmap_draw_dynamic_string    /* int    graphic_bitmap_draw_dynamic_string(int id, int x, int y, int w, int h, int dynamic_string_id, int message_id, int text_color, unsigned char transparency, int background_color, char *font, int point_size, unsigned char bold, unsigned char italic, int alignment) */
    #endif
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x182000
    #define graphic_get_dynamic_string_size     f_pnt_graphic_get_dynamic_string_size       /* int    graphic_get_dynamic_string_size(int dynamic_string_id, int message_id) */
    #define graphic_get_dynamic_string_text     f_pnt_graphic_get_dynamic_string_text       /* int    graphic_get_dynamic_string_text(char *text_string, int string_size, int dynamic_string_id, int message_id) */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x163100

    /*
    * typedef enum _ISO_VIEWER_MODE {
    *  ISO_VIEWER_MODE_LIST       = 0,
    *  ISO_VIEWER_MODE_PREVIEW    = 1
    *
    * }_ISO_VIEWER_MODE;
    *
    * typedef enum {
    *     ISO_TASK_1                 = 0,
    *     ISO_TASK_2                 = 1,
    *     ISO_TASK_3                 = 2
    *
    * } _ISO_TASK_ID;
    */

    #define graphic_iso_viewer_create           f_pnt_graphic_iso_viewer_create                                                 /* int id graphic_iso_viewer_create(int x, int y, int w, int h, _ISO_TASK_ID iso_task_id, _ISO_VIEWER_MODE mode) */
    #define graphic_iso_viewer_draw             f_pnt_graphic_iso_viewer_draw                                                   /* int    graphic_iso_viewer_draw(int id) */
    #define graphic_iso_viewer_background_color f_pnt_graphic_iso_viewer_background_color                                       /* int    graphic_iso_viewer_background_color(int id, int color) */
    #define graphic_iso_viewer_foreground_color f_pnt_graphic_iso_viewer_foreground_color                                       /* int    graphic_iso_viewer_foreground_color(int id, int color) */
    #define graphic_iso_viewer_font             f_pnt_graphic_iso_viewer_font                                                   /* int    graphic_iso_viewer_font(int id, char *font, int point_size, unsigned char bold, unsigned char italic) */
    #if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x172000
        #define graphic_iso_viewer_change_mode      f_pnt_graphic_iso_viewer_change_mode                                            /* int    graphic_iso_viewer_change_mode(int id, _ISO_VIEWER_MODE mode) */
    #endif // FIRMWARE_VERSION > 0x172000

    /* List properties */
    #define graphic_iso_viewer_line_number_area_background_color    f_pnt_graphic_iso_viewer_line_number_area_background_color  /* int    graphic_iso_viewer_line_number_area_background_color(int id, int color) */
    #define graphic_iso_viewer_line_number_area_foreground_color    f_pnt_graphic_iso_viewer_line_number_area_foreground_color  /* int    graphic_iso_viewer_line_number_area_foreground_color(int id, int color) */
    #define graphic_iso_viewer_status_bar_visible                   f_pnt_graphic_iso_viewer_status_bar_visible                 /* int    graphic_iso_viewer_status_bar_visible(int id, unsigned char visible) */

    /* Preview properties */
    #define graphic_iso_viewer_axes              f_pnt_graphic_iso_viewer_axes                                                  /* int    graphic_iso_viewer_axes(int id, unsigned char horizontal_axis, unsigned char vertical_axis) */
    #define graphic_iso_viewer_grid_color        f_pnt_graphic_iso_viewer_grid_color                                            /* int    graphic_iso_viewer_grid_color(int id, int color) */
    #define graphic_iso_viewer_grid_spacing      f_pnt_graphic_iso_viewer_grid_spacing                                          /* int    graphic_iso_viewer_grid_spacing(int id, float spacing) */
    #define graphic_iso_viewer_grid_lines_width  f_pnt_graphic_iso_viewer_grid_lines_width                                      /* int    graphic_iso_viewer_grid_lines_width(int id, float linesWidth) */
    #define graphic_iso_viewer_axis_path_color   f_pnt_graphic_iso_viewer_axis_path_color                                       /* int    graphic_iso_viewer_axis_path_color(int id, int color) */
    #define graphic_iso_viewer_maximum_zoom      f_pnt_graphic_iso_viewer_maximum_zoom                                          /* int    graphic_iso_viewer_maximum_zoom(int id, float maxZoom) */
    #define graphic_iso_viewer_zoom              f_pnt_graphic_iso_viewer_zoom                                                  /* int    graphic_iso_viewer_current_zoom(int id, float zoom) */
    #define graphic_iso_viewer_fit               f_pnt_graphic_iso_viewer_fit                                                   /* int    graphic_iso_viewer_fit(int id) */
    #if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x180600
        #define graphic_iso_viewer_horizontal_mirror f_pnt_graphic_iso_viewer_horizontal_mirror                                     /* int    graphic_iso_viewer_horizontal_mirror(int id, unsigned char enable) */
        #define graphic_iso_viewer_vertical_mirror   f_pnt_graphic_iso_viewer_vertical_mirror                                       /* int    graphic_iso_viewer_vertical_mirror(int id, unsigned char enable) */
    #endif
    #if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x182000
        /*
        * typedef enum {
        *  ISO_VIEWER_AXIS_PATH_MODE_REAL      = 0,
        *  ISO_VIEWER_AXIS_PATH_MODE_NONE      = 1,
        *  ISO_VIEWER_AXIS_PATH_MODE_IDEAL     = 2
        *
        * } _ISO_VIEWER_AXIS_PATH_MODE;
        */
        #define graphic_iso_viewer_axis_path_mode    f_pnt_graphic_iso_viewer_axis_path_mode                                        /* int    graphic_iso_viewer_axis_path_mode(int id, _ISO_VIEWER_AXIS_PATH_MODE mode) */
        #define graphic_iso_viewer_starting_line_color f_pnt_graphic_iso_viewer_starting_line_color                                 /* int    graphic_iso_viewer_starting_line_color(int id, int color) */

        /*
        * ISO starting line selection parameters:
        * - id_preview: iso_viewer preview object on which current starting
        *               line will be highlighted
        * - id_list: iso_viewer list object on which starting line selection
        *            will be possible
        *
        * NOTE:
        * - graphic_iso_viewer_starting_line_selection_enable() has to be
        *   called before every ISO_Start activation. Starting line will be
        *   reset after ISO file execution
        */
        #define graphic_iso_viewer_starting_line_selection_enable f_pnt_graphic_iso_viewer_starting_line_selection_enable           /* int    graphic_iso_viewer_starting_line_selection_enable(int id_preview, int id_list) */
        #define graphic_iso_viewer_starting_line_selection_disable f_pnt_graphic_iso_viewer_starting_line_selection_disable         /* int    graphic_iso_viewer_starting_line_selection_disable(int id_preview, int id_list) */
        #define iso_starting_line_selection_status                f_pnt_iso_starting_line_selection_status                          /* int    iso_starting_line_selection_status(_ISO_TASK_ID iso_task_id) */
    #endif // FIRMWARE_VERSION > 0x182000

#endif // FIRMWARE_VERSION >= 0x163100

#endif

#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x152800
    /* Change HMI function
    *
    * id_hmi must be a constant like [HMI]<hmi_name> (see symbol table)
    * id_page must be an integer between 0 (page 1) and 4 (page 5)
    *
    */
    #define graphic_change_HMI                  f_pnt_graphic_change_HMI                    /* int    graphic_change_HMI(unsigned short id_hmi, unsigned char id_page) */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x152801
    /* Network handling functions
    *
    * Every function returns negative value in case of error
    *
    */

    /*
    * Network object creation/deletion functions
    *
    * enum _NETWORK_PROTOCOL
    * {
    *     NETWORK_PROTOCOL_TCP     =0,
    *     NETWORK_PROTOCOL_UDP     =1
    * };
    */
    #define network_create                      f_pnt_network_create                        /* int id network_create(_NETWORK_PROTOCOL protocol); */
    #define network_delete                      f_pnt_network_delete                        /* int    network_delete(int id); */

    /*
    * Client handling functions
    */
    #define network_connect                     f_pnt_network_connect                        /* int    network_connect(int id, unsigned char ip_address1, unsigned char ip_address2, unsigned char ip_address3, unsigned char ip_address4, unsigned short port); */
    #define network_disconnect                  f_pnt_network_disconnect                     /* int    network_disconnect(int id); */

    /*
    * Server handling functions
    */
    #define network_listen                      f_pnt_network_listen                         /* int    network_listen(int id, unsigned short port); */

    /*
    * Common functions
    *
    * enum _NETWORK_STATE
    * {
    *     NETWORK_STATE_UNCONNECTED    =0,
    *     NETWORK_STATE_CONNECTING     =1,
    *     NETWORK_STATE_CONNECTED      =2
    * };
    */
    #define network_state                       f_pnt_network_state                          /* _NETWORK_STATE network_state(int id); */
    #define network_bytes_available             f_pnt_network_bytes_available                /* int    network_bytes_available(int id); */
    #define network_read                        f_pnt_network_read                           /* int    network_read(int id, char *data, int size); */
    #define network_write                       f_pnt_network_write                          /* int    network_write(int id, char *data, int size); */

    /*
    * General network functions
    *
    * network_ping function MUST be executed in a thread section
    */
    #define network_ping                        f_pnt_network_ping                           /* int    network_ping(unsigned char ip_address1, unsigned char ip_address2, unsigned char ip_address3, unsigned char ip_address4); */
#endif

/*
 * C sections debug tools
 *
 * This function has the same prototype of ANSI C sprintf().
 * It lets you print strings on Elco Automation Studio logger console
 *
 * Ex: debug_print("Variable value= %f", Variable);
 *
 * NOTE: debug_print() has an internal buffer of 256 characters. Exceeding
 *       characters will be ignored. Return value gives the amount of characters
 *       that would have been printed if the internal buffer would have been
 *       big enough (that is return value could be greater than 256)
 */
#define debug_print             f_pnt_debug_print           /* int debug_print(const char *format, ...) */
#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x180200
    #define debug_time_start        f_pnt_debug_time_start      /* void debug_time_start() */
    #define debug_time_elapsed_ms   f_pnt_debug_time_elapsed_ms /* float debug_time_elapsed_ms() */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x170401
    /* Keyboard handling functions
    *
    * Every function returns negative value in case of error
    *
    */

    #define keyboard_bytes_available            f_pnt_keyboard_bytes_available               /* int    keyboard_bytes_available(); */
    #define keyboard_read                       f_pnt_keyboard_read                          /* int    keyboard_read(char *data, int size); */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x172400
    /* Serial ports handling functions
    *
    * Every function returns negative value in case of error
    *
    */

    /*
    * serial_open() parameters
    *
    * port:
    * enum _SERIAL_PORT
    * {
    *     SERIAL_PORT_RS232,
    *     SERIAL_PORT_RS485_1,
    *     SERIAL_PORT_RS485_2
    * };
    *
    * baudRate:
    * enum _SERIAL_PORT_BAUDRATE
    * {
    *     SERIAL_PORT_BAUDRATE_2400,
    *     SERIAL_PORT_BAUDRATE_4800,
    *     SERIAL_PORT_BAUDRATE_9600,
    *     SERIAL_PORT_BAUDRATE_19200,
    *     SERIAL_PORT_BAUDRATE_38400,
    *     SERIAL_PORT_BAUDRATE_57600,
    *     SERIAL_PORT_BAUDRATE_115200
    * };
    *
    * dataBits:
    * enum _SERIAL_PORT_DATA_BITS
    * {
    *     SERIAL_PORT_DATA_BITS_5,
    *     SERIAL_PORT_DATA_BITS_6,
    *     SERIAL_PORT_DATA_BITS_7,
    *     SERIAL_PORT_DATA_BITS_8
    * };
    *
    * parity:
    * enum _SERIAL_PORT_PARITY
    * {
    *     SERIAL_PORT_PARITY_NONE,
    *     SERIAL_PORT_PARITY_ODD,
    *     SERIAL_PORT_PARITY_EVEN
    * };
    *
    * stopBits:
    * enum _SERIAL_PORT_STOP_BITS
    * {
    *     SERIAL_PORT_STOP_BITS_1,
    *     SERIAL_PORT_STOP_BITS_2
    * };
    */
    #define serial_open                         f_pnt_serial_open                            /* int id serial_open(_SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stopBits); */
    #define serial_open_from_name               f_pnt_serial_open_from_name                  /* int id serial_open_from_name(char *serialPortName, _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stopBits); */
    #define serial_close                        f_pnt_serial_close                           /* int    serial_close(int id); */

    #define serial_bytes_available              f_pnt_serial_bytes_available                 /* int    serial_bytes_available(int id); */
    #define serial_read                         f_pnt_serial_read                            /* int    serial_read(int id, char *data, int size); */
    #define serial_send                         f_pnt_serial_send                            /* int    serial_send(int id, char *data, int size); */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x173100
    /* Kinematics handling functions
    *
    * Every function returns negative value in case of error
    *
    */

    /*
    * cartesian_coords details:
    * cartesian_coords[0] = X cartesian logic coordinate
    * cartesian_coords[1] = Y cartesian logic coordinate
    * cartesian_coords[2] = Z cartesian logic coordinate
    *
    * robot_coords details:
    * robot_coords[0] = X axis physical coordinate
    * robot_coords[1] = Y axis physical coordinate
    * robot_coords[2] = W axis physical coordinate
    * robot_coords[3] = Z axis physical coordinate
    * robot_coords[4] = A axis physical coordinate
    * robot_coords[5] = B axis physical coordinate
    * robot_coords[6] = C axis physical coordinate
    * robot_coords[7] = D axis physical coordinate
    *
    * Example: project with anthropomorphous kinematics with following
    * configuration:
    * joint 1 = axis X
    * joint 2 = axis Z
    * joint 3 = axis Y
    *
    float robot_coords[4];
    float cartesian_coords[3];
    int ret;

    robot_coords[0] = WORD_TO_FLOAT(asse_FeedBack_X);
    robot_coords[1] = WORD_TO_FLOAT(asse_FeedBack_Y);
    robot_coords[2] = WORD_TO_FLOAT(asse_FeedBack_W);    //Not relevant
    robot_coords[3] = WORD_TO_FLOAT(asse_FeedBack_Z);

    //Robot ---> Cartesian
    ret = kinematics_to_cartesian(cartesian_coords, robot_coords);
    if(ret < 0)
    debug_print("to_cartesian error: %d", ret);
    else {
    cartesianCoordX = FLOAT_TO_WORD(cartesian_coords[0]);
    cartesianCoordY = FLOAT_TO_WORD(cartesian_coords[1]);
    cartesianCoordZ = FLOAT_TO_WORD(cartesian_coords[2]);
    }

    //Cartesian ---> Robot
    ret = kinematics_to_robot(cartesian_coords, robot_coords);
    if(ret < 0)
    debug_print("to_robot error: %d", ret);
    else {
    robotCoordX = FLOAT_TO_WORD(robot_coords[0]);
    robotCoordY = FLOAT_TO_WORD(robot_coords[1]);
    robotCoordW = FLOAT_TO_WORD(robot_coords[2]);    //Not relevant
    robotCoordX = FLOAT_TO_WORD(robot_coords[3]);
    }
    *
    */
    #define kinematics_to_robot                 f_pnt_kinematics_to_robot                    /* int kinematics_to_robot(float cartesian_coords[], float robot_coords[]); */
    #define kinematics_to_cartesian             f_pnt_kinematics_to_cartesian                /* int kinematics_to_cartesian(float cartesian_coords[], float robot_coords[]); */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x181200
    /* ISO variables handling functions
    *
    * Every function sets negative value in 'err' field in case of error
    *
    * NOTE: 'variable_index' parameter starts from 0 (e.g. 0 = $0, 1 = $1, ...)
    *       'err' parameter is optional .. pass NULL in case you don't want to check errors
    */

    #define iso_variable_read                   f_pnt_iso_variable_read                      /* double iso_variable_read(_ISO_TASK_ID iso_task, int variable_index, int *err); */
    #define iso_variable_write                  f_pnt_iso_variable_write                     /* void iso_variable_write(_ISO_TASK_ID iso_task, int variable_index, double value, int *err); */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x184100
/*
 * typedef struct _iso_file_analisys_info {
 *     unsigned int error_code;
 *     unsigned int line_number;
 *     double min_horizontal;
 *     double max_horizontal;
 *     double min_vertical;
 *     double max_vertical;
 *
 * } _iso_file_analisys_info;
 *
 * typedef enum _PLANE_DIMENSIONS {
 *     HORIZONTAL_DIMENSION = 0,
 *     VERTICAL_DIMENSION   = 1,
 *
 *     PLANE_DIMENSIONS
 * } _PLANE_DIMENSIONS;
 *
 * typedef enum _ARC_ROTATION
 * {
 *     ARC_ROTATION_CLOCKWISE     = 0,
 *     ARC_ROTATION_ANTICLOCKWISE = 1
 *
 * } _ARC_ROTATION;
 *
 * typedef int (*pntDrawLine)(double start_point[PLANE_DIMENSIONS],
 *                            double end_point[PLANE_DIMENSIONS]);
 * typedef int (*pntDrawArc)(double start_point[PLANE_DIMENSIONS],
 *                           double end_point[PLANE_DIMENSIONS],
 *                           double center[PLANE_DIMENSIONS],
 *                           _ARC_ROTATION verso);
 *
 */
#define iso_file_preview    f_pnt_iso_file_preview      /* int  iso_file_preview(_ISO_TASK_ID iso_task,
                                                                                    _iso_file_analisys_info *info,
                                                                                    pntDrawLine drawLine,
                                                                                    pntDrawArc drawArc) */
#define iso_file_load       f_pnt_iso_file_load         /* int  iso_file_load(_ISO_TASK_ID iso_task,
                                                                                    char* file_name,
                                                                                    _FILE_PATH path) */
#define iso_file_start       f_pnt_iso_file_start         /* int  iso_file_start(_ISO_TASK_ID iso_task) */
#define iso_file_stop        f_pnt_iso_file_stop          /* int  iso_file_stop(_ISO_TASK_ID iso_task) */
#define iso_file_reset       f_pnt_iso_file_reset         /* int  iso_file_reset(_ISO_TASK_ID iso_task) */

#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x184500
// typedef enum _Orientation {
//      VerticalLeft     = 0x1,
//      VerticalRight    = 0x2,
//      HorizontalTop    = 0x3,
//      HorizontalBottom = 0x4
// } _Orientation;

#define graphic_iso_editor_create                               f_pnt_graphic_iso_editor_create             /* int graphic_iso_editor_create(int x, int y, int w, int h,
                                                                                                                                                    _ISO_TASK_ID iso_task_id,
                                                                                                                                                    _Orientation buttonBarOrientation) */
#define graphic_iso_editor_visualize_title                      f_pnt_graphic_iso_editor_visualize_title    /* int graphic_iso_editor_visualize_title(int id, unsigned char visualizeTitle) */
#define graphic_iso_editor_background_color                     f_pnt_graphic_iso_editor_background_color   /* int graphic_iso_editor_background_color(int id, int color) */
#define graphic_iso_editor_foreground_color                     f_pnt_graphic_iso_editor_foreground_color   /* int graphic_iso_editor_foreground_color(int id, int color) */
#define graphic_iso_editor_font                                 f_pnt_graphic_iso_editor_font               /* int graphic_iso_editor_font(int id, char *font, int point_size, unsigned char bold, unsigned char italic) */
#define graphic_iso_editor_line_number_area_background_color    f_pnt_graphic_iso_editor_line_number_area_background_color  /* int graphic_iso_editor_line_number_area_background_color(int id, int color) */
#define graphic_iso_editor_line_number_area_foreground_color    f_pnt_graphic_iso_editor_line_number_area_foreground_color  /* int graphic_iso_editor_line_number_area_foreground_color(int id, int color) */
#endif

#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x185100
    #define graphic_iso_editor_draw                                 f_pnt_graphic_iso_editor_draw               /* int graphic_iso_editor_draw(int id) */
#endif

#if !defined(SOFTPLC_VERSION) || (SOFTPLC_VERSION >= SOFTPLC_VERSION_CHECK(0, 1, 0))

    /*
    * Set timeout options. Values are in seconds.
    *
    * void sql_set_timeout(int connectionTimeout, int loginTimeout);
    */
    #define sql_set_timeout f_sql_set_timeout

    /*
    * Connect to database; on failure returns negative value, on success returns the database id.
    *
    * enum SQL_DRIVER_TYPE {
    *   MSSQL = 0,
    *   MySQL = 1
    * }
    *
    * int sql_db_connect(SQL_DRIVER_TYPE driverType, char *hostname, int port,
    *                    char *username, char *psw, char *database);
    */
    #define sql_db_connect f_sql_db_connect

    /*
    * Disconnect from database; on failure returns negative value, on success returns 0.
    *
    * int sql_db_disconnect(unsigned int db_id);
    */
    #define sql_db_disconnect f_sql_db_disconnect

    /*
    * Executes a SQL command without no result like INSERT, UPDATE, DELETE, ALTER, DROP, etc.
    * On failure returns negative value, on success returns the number of affected rows.
    *
    * int sql_exec(unsigned int db_id, char *command);
    */
    #define sql_exec f_sql_exec

    /*
    * Executes a SQL command with results like SELECT, etc.
    * On failure returns negative value; on success returns the query id; rows
    * will be valorized with the number of selected records,
    * or -1 if it cannot be determined; columns will be valorized with
    * the number of fields in each selected record.
    *
    * int sql_query(unsigned int db_id, char *command, int *rows, int *columns);
    */
    #define sql_query f_sql_query

    /*
    * Exectue a SQL command with call to database stored procedure.
    * On failure returns negative value; on success returns the query id and
    * columns are valorized with the affected columns.
    *
    * int sql_stored_procedure(unsigned int db_id, char *command, int *columns);
    */
    #define sql_stored_procedure f_sql_stored_procedure

    /*
    * Check if the next record in the result is available.
    * On failure returns 0; on success return 1 and the record pointer of the
    * query is moved forward.
    *
    * int sql_next(unsigned int db_id, unsigned int q_id);
    */
    #define sql_next f_sql_next

    /*
    * Converts the requested field data into readable value in value.
    * On failure returns negative value, on success returns 0.
    *
    * NOTE: for stored procedure queries row must be equal to -1. Column value
    * of the current record will be selected.
    *
    * See also: sql_next()
    *
    * int sql_get_value(unsigned int db_id, unsigned int q_id, int row, int column,
    *                   char *value, int value_size);
    */
    #define sql_get_value f_sql_get_value

    /*
    * Get the requested field data type.
    * On failure returns negative value, on success returns 0.
    *
    * enum SQL_DATA_TYPE {
    *   INTEGER = 0,
    *   FLOAT = 1,
    *   STRING = 2
    * };
    *
    * NOTE: for stored procedure queries row must be equal to -1. Column value
    * type of the current record will be selected.
    *
    * See also: sql_next()
    *
    * int sql_get_value_type(unsigned int db_id, unsigned int q_id, int row, int column,
    *                        DataType *value_data_type);
    */
    #define sql_get_value_type f_sql_get_value_type

    /*
    * Remove from record the selected query.
    * On failure returns negative value, on success returns 0.
    *
    * NOTE: forgetting to delete a query will produce a resource leak.
    *
    * int sql_delete_query(unsigned int db_id, unsigned int q_id);
    */
    #define sql_delete_query f_sql_delete_query

#endif

#if !defined(SOFTPLC_VERSION) || (SOFTPLC_VERSION >= SOFTPLC_VERSION_CHECK(0, 4, 0))
    /*
    * Reads system clock
    * On failure returns negative value; on success returns 0
    * Unneeded arguments could be set to NULL
    *
    * int system_clock_read(unsigned short *year, unsigned char *month,
    *                       unsigned char *day, unsigned char *hour, unsigned char *min,
    *                       unsigned char *sec, unsigned short *millisec);
    */
    #define system_clock_read f_pnt_system_clock_read
#endif

#endif  //SYSTEM_INTERFACE_H
