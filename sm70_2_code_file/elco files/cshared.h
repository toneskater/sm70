#ifndef LIB_C_SHARED_H
#define LIB_C_SHARED_H

#define PLC_WORDS_ARRAY     26
#define PLC_IO_WORDS_MAX    30
#define PLC_PULS_TOT        5

#include "cshared_global.h"

#if defined(CSHARED_EAS_LIB)

typedef union {
    unsigned short R;
    struct {
        unsigned short sysbitplc1: 1;
        unsigned short sysbitplc2: 1;
        unsigned short sysbitplc3: 1;
        unsigned short sysbitplc4: 1;
        unsigned short sysbitplc5: 1;
        unsigned short sysbitplc6: 1;
        unsigned short sysbitplc7: 1;
        unsigned short sysbitplc8: 1;
        unsigned short sysbitplc9: 1;
        unsigned short sysbitplc10: 1;
        unsigned short sysbitplc11: 1;
        unsigned short sysbitplc12: 1;
        unsigned short sysbitplc13: 1;
        unsigned short sysbitplc14: 1;
        unsigned short sysbitplc15: 1;
        unsigned short sysbitplc16: 1;
    } B;
} plcbit;

#define PLC_BIT plcbit
#else
#define PLC_BIT unsigned short
#endif

/* Word intere non tamponate (es. Wnaaa) */
CSHARED_EXPORT extern short Wn[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word intere tamponate (es. Wmaaa) */
CSHARED_EXPORT extern short Wm[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word 'float' tamponate (es. WFmaaa) */
CSHARED_EXPORT extern int WFm[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word 'float' non tamponate (es. WFnaaa) */
CSHARED_EXPORT extern int WFn[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word 'float' di sistema (es. WFsaa) */
CSHARED_EXPORT extern int WFs[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word intere di sistema (es. WSaa) */
CSHARED_EXPORT extern short Ws[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Flag non tamponate (es. Naa1) */
CSHARED_EXPORT extern PLC_BIT nom[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Flag tamponate (es. Maa1) */
CSHARED_EXPORT extern PLC_BIT mem[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Flag di sistema (es. Saa1) */
CSHARED_EXPORT extern PLC_BIT flg[PLC_WORDS_ARRAY];
/* Uscite (es. Ua1) (lettura) */
CSHARED_EXPORT extern volatile unsigned short out[PLC_IO_WORDS_MAX];
/* Uscite (0-15) (es. Ua1) (scrittura) */
CSHARED_EXPORT extern volatile unsigned short digout;
/* Uscite (16-31) (es. Ub1) (scrittura) */
CSHARED_EXPORT extern volatile unsigned short digout2;
/* Uscite (32-47) (es. Uc1) (scrittura) */
CSHARED_EXPORT extern volatile unsigned short digout3;
/* Uscite (48-63) (es. Ud1) (scrittura) */
CSHARED_EXPORT extern volatile unsigned short digout4;
/* Uscite (64-79) (es. Ue1) (scrittura) */
CSHARED_EXPORT extern volatile unsigned short digout5;
/* Ingressi (es. Ia1) */
CSHARED_EXPORT extern volatile unsigned short in[PLC_IO_WORDS_MAX];
/* Fattore di conversione per trasformazione word 'float' in float reali */
CSHARED_EXPORT extern unsigned short Fatt_corr;
/* Tempo esecuzione task PLC (sola lettura) */
CSHARED_EXPORT extern unsigned short IRQ_ExePlc_Time;
/* Tempo esecuzione IRQ Short (sola lettura) */
CSHARED_EXPORT extern float IRQ_Short_Time_ms;
/* Gestione chiusura thread sezioni C */
CSHARED_EXPORT extern unsigned char c_thread_object_stop;
/* Tasti fisici */
CSHARED_EXPORT extern PLC_BIT plc_puls[PLC_PULS_TOT];
/* Banchi di bit temporanei */
CSHARED_EXPORT extern PLC_BIT int_mem[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Costante sempre vera */
CSHARED_EXPORT extern unsigned short cost;
/* Costante sempre falsa */
CSHARED_EXPORT extern unsigned short costn;
/* Timers */
CSHARED_EXPORT extern unsigned int temporizzatori[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];

typedef enum {
    LOCAL_FILE_SYSTEM   = 0,
    USB_FILE_SYSTEM     = 1,
    USB_FRONT_FILE_SYSTEM = 1,  // Alias
#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION >= 0x143000
    NETWORK_FILE_SYSTEM = 2,
#endif
#if !defined(FIRMWARE_VERSION) || FIRMWARE_VERSION > 0x173100
    USB_REAR_FILE_SYSTEM = 3,
#endif

    USB_1_FILE_SYSTEM = 1,
    USB_2_FILE_SYSTEM = 3,
    USB_3_FILE_SYSTEM = 4,
    USB_4_FILE_SYSTEM = 5,

    N_FILE_PATH

} _FILE_PATH;

typedef int (*pnt_file_open)(char *name, _FILE_PATH path);
typedef int (*pnt_file_creat)(char *name, _FILE_PATH path);
typedef int (*pnt_file_close)(int fd);
typedef int (*pnt_file_remove)(char *name, _FILE_PATH path);
typedef int (*pnt_file_read_line)(int fd, char *line, int maxline);
typedef int (*pnt_file_write_line)(int fd, char *line);
typedef int (*pnt_file_read)(int fd, char *buffer, int n);
typedef int (*pnt_file_write)(int fd, char *buffer, int n);
typedef int (*pnt_file_copy)(char *src_name, _FILE_PATH src_path, char *dest_name,
                             _FILE_PATH dest_path);
typedef int (*pnt_file_rename)(char *old_name, char *new_name, _FILE_PATH Path);
typedef int (*pnt_file_get_path)(char *path_string, int path_size, _FILE_PATH path);
typedef int (*pnt_file_get_path_size)(_FILE_PATH path);
typedef int (*pnt_file_open_select)(char *name, int name_size, char *extension,
                                    _FILE_PATH path);
typedef int (*pnt_file_save_select)(char *name, int name_size);
typedef int (*pnt_file_remove_multiselect)(char *extension, _FILE_PATH path);
typedef int (*pnt_file_copy_multiselect)(char *extension, _FILE_PATH sourcePath,
                                         _FILE_PATH destinationPath);

typedef int (*pnt_file_list_get)(char *extensions, _FILE_PATH path);
typedef int (*pnt_file_list_size_get)(char *extensions, _FILE_PATH path);
typedef int (*pnt_file_list_file_get)(int index, char *name, int name_size,
                                      char *extensions, _FILE_PATH path);
typedef int (*pnt_file_list_copy)(_FILE_PATH src_path, _FILE_PATH dest_path,
                                  char *extensions);
typedef int (*pnt_file_list_remove)(char *extensions, _FILE_PATH path);
typedef int (*pnt_file_list_get_multiselect)(char *extensions, _FILE_PATH path);
typedef int (*pnt_file_size)(char *name, _FILE_PATH path);
typedef int (*pnt_file_exists)(char *name, _FILE_PATH path);
typedef int (*pnt_file_seek_beginning)(int fd);
typedef int (*pnt_file_seek_end)(int fd);
typedef int (*pnt_file_seek_absolute)(int fd, unsigned int pos);
typedef int (*pnt_file_seek_relative)(int fd, int offset);

typedef int   (*pnt_string_sprintf)(char *s, const char *format, ...);
typedef int   (*pnt_string_snprintf)(char *s, unsigned int n, const char *format, ...);
typedef char *(*pnt_string_concatenate)(char *dest_str, char *src_str);
typedef int   (*pnt_string_length)(char *str);
typedef int   (*pnt_string_compare)(char *str_1, char *str_2);
typedef char *(*pnt_string_copy)(char *dest_str, char *src_str);
typedef int   (*pnt_string_atoi)(char *str);
typedef double (*pnt_string_atof)(char *str);
typedef long int (*pnt_string_atol)(char *str);
typedef long long int (*pnt_string_atoll)(char *str);
typedef double (*pnt_string_tod)(const char *str, char **endptr);
typedef long double (*pnt_string_told)(const char *str, char **endptr);
typedef float  (*pnt_string_tof)(const char *str, char **endptr);
typedef long int (*pnt_string_tol)(const char *str, char **endptr, int base);
typedef long long int (*pnt_string_toll)(const char *str, char **endptr, int base);
typedef unsigned long int (*pnt_string_toul)(const char *str, char **endptr, int base);
typedef unsigned long long int (*pnt_string_toull)(const char *str, char **endptr,
                                                   int base);
typedef char  *(*pnt_string_str)(char *str1, char *str2);
typedef char  *(*pnt_string_chr)(char *str, int character);
typedef char  *(*pnt_string_rchr)(char *str, int character);
typedef int    (*pnt_string_spn)(char *str1, char *str2);
typedef int    (*pnt_string_cspn)(char *str1, char *str2);
typedef char  *(*pnt_string_tok)(char *str, char *delimiters);
typedef char  *(*pnt_string_pbrk)(char *str1, char *str2);
typedef char  *(*pnt_string_ncpy)(char *dest_str, char *src_str, int num);
typedef char  *(*pnt_string_ncat)(char *dest_str, char *src_str, int num);
typedef int    (*pnt_string_ncmp)(char *str1, char *str2, int num);
typedef int    (*pnt_string_trim)(char *str);

typedef struct {
    int quot;
    int rem;
} _math_div_t;

typedef struct {
    long int quot;
    long int rem;
} _math_ldiv_t;

typedef struct {
    long long int quot;
    long long int rem;
} _math_lldiv_t;

typedef _math_div_t (*pnt_math_div)(int numer, int denom);
typedef _math_ldiv_t (*pnt_math_ldiv)(long int numer, long int denom);
typedef _math_lldiv_t (*pnt_math_lldiv)(long long int numer, long long int denom);

typedef void *(*pnt_memory_set)(void *mem, int val, int n_bytes);
typedef int (*pnt_memory_compare)(const void *mem_1, const void *mem_2, int n_bytes);
typedef void *(*pnt_memory_move)(void *dest_mem, const void *src_mem, int n_bytes);
typedef void *(*pnt_memory_copy)(void *dest_mem, const void *src_mem, int n_bytes);
typedef void *(*pnt_memory_alloc)(int size);
typedef void *(*pnt_memory_calloc)(int num, int size);
typedef void  (*pnt_memory_free)(void *ptr);
typedef void *(*pnt_memory_realloc)(void *ptr, int size);

typedef int (*pnt_list_create)(void);
typedef int (*pnt_list_clear)(int id);
typedef int (*pnt_list_size)(int id);
typedef int (*pnt_list_empty)(int id);
typedef int (*pnt_list_append)(int id, void *item);
typedef void *(*pnt_list_item_at)(int id, int index);
typedef int (*pnt_list_item_insert)(int id, void *item, int index);
typedef int (*pnt_list_item_replace)(int id, void *item, int index);
typedef int (*pnt_list_item_remove)(int id, int index);
typedef int (*pnt_list_delete)(int id);
typedef int (*pnt_list_delete_all)(void);

typedef int (*pnt_debug_print_string)(char *string);
typedef int (*pnt_debug_print_int)(int n);
typedef int (*pnt_debug_print_float)(float n);
typedef int (*pnt_debug_print)(const char *format, ...);
typedef void (*pnt_debug_time_start)(void);
typedef float (*pnt_debug_time_elapsed_ms)(void);

typedef int (*pnt_graphic_get_screen_width)(void);
typedef int (*pnt_graphic_get_screen_height)(void);

typedef int (*pnt_graphic_objects_delete_all)(void);
typedef int (*pnt_graphic_objects_show_all)(void);
typedef int (*pnt_graphic_objects_hide_all)(void);
typedef int (*pnt_graphic_objects_number)(void);

typedef enum {
    OBJECT_NONE         = 0,
    OBJECT_LINE         = 1,
    OBJECT_RECT         = 2,
    OBJECT_ELLIPSE      = 3,
    OBJECT_IMAGE        = 4,
    OBJECT_TEXT         = 5,
    OBJECT_ARC          = 6,
    OBJECT_ECALC        = 7,
    OBJECT_PATH         = 8,
    OBJECT_VIEWER       = 9,
    OBJECT_ISO_VIEWER   = 10,
    OBJECT_BITMAP       = 11,
    OBJECT_ISO_EDITOR   = 12

} _OBJECT_TYPE;

typedef _OBJECT_TYPE (*pnt_graphic_object_get_type)(int id);
typedef int (*pnt_graphic_object_delete)(int id);
typedef int (*pnt_graphic_object_show)(int id);
typedef int (*pnt_graphic_object_hide)(int id);
typedef int (*pnt_graphic_object_set_visible)(int id, unsigned char visible);
typedef int (*pnt_graphic_object_set_filled)(int id, unsigned char filled);
typedef int (*pnt_graphic_object_move)(int id, int x, int y);
typedef int (*pnt_graphic_object_set_x)(int id, int x);
typedef int (*pnt_graphic_object_set_y)(int id, int y);
typedef int (*pnt_graphic_object_set_main_color)(int id, int color);
typedef int (*pnt_graphic_object_set_secondary_color)(int id, int color);
typedef int (*pnt_graphic_object_set_geometry)(int id, int x, int y, int w, int h);
typedef int (*pnt_graphic_object_set_size)(int id, int w, int h);
typedef int (*pnt_graphic_object_set_width)(int id, int w);
typedef int (*pnt_graphic_object_set_height)(int id, int h);
typedef int (*pnt_graphic_object_set_tag)(int id, int tag);
typedef int (*pnt_graphic_object_get_tag)(int id);

typedef int (*pnt_graphic_line_create)(int x1, int y1, int x2, int y2, int color,
                                       unsigned char visible);
typedef int (*pnt_graphic_line_modify)(int id, int x1, int y1, int x2, int y2);
typedef int (*pnt_graphic_line_set_p1)(int id, int x1, int y1);
typedef int (*pnt_graphic_line_set_p2)(int id, int x2, int y2);

typedef int (*pnt_graphic_rect_create)(int x, int y, int w, int h, int color,
                                       unsigned char filled, int border_color, unsigned char visible);

typedef int (*pnt_graphic_ellipse_create)(int center_x, int center_y, int w, int h,
                                          int color, unsigned char filled, int border_color, unsigned char visible);

typedef int (*pnt_graphic_image_create)(int x, int y, int w, int h, int image_id,
                                        unsigned char scaled, unsigned char visible);
typedef int (*pnt_graphic_image_get_width)(int image_id);
typedef int (*pnt_graphic_image_get_height)(int image_id);
typedef int (*pnt_graphic_image_create_from_file)(int x, int y, int w, int h, char *name,
                                                  _FILE_PATH path, unsigned char scaled, unsigned char visible);
typedef int (*pnt_graphic_image_get_width_from_file)(char *name, _FILE_PATH path);
typedef int (*pnt_graphic_image_get_height_from_file)(char *name, _FILE_PATH path);
typedef int (*pnt_graphic_image_modify)(int id, int image_id);

typedef int (*pnt_graphic_text_create)(int x, int y, int w, int h, char *text,
                                       int text_color, unsigned char transparency, int background_color, char *font,
                                       int point_size, unsigned char bold, unsigned char italic, int alignment,
                                       unsigned char visible);
typedef int (*pnt_graphic_text_text_modify)(int id, char *text);
typedef int (*pnt_graphic_text_font_modify)(int id, char *font, int point_size,
                                            unsigned char bold, unsigned char italic);
typedef int (*pnt_graphic_text_alignment_modify)(int id, int alignment);
typedef int (*pnt_graphic_text_transparency_modify)(int id, int transparency);
typedef int (*pnt_graphic_text_dynamic_string_create)(int x, int y, int w, int h,
                                                      int dynamic_string_id, int message_id, int text_color, unsigned char transparency,
                                                      int background_color, char *font, int point_size, unsigned char bold,
                                                      unsigned char italic, int alignment, unsigned char visible);

typedef int (*pnt_graphic_arc_create)(int center_x, int center_y, int w, int h,
                                      float start_angle, float end_angle, int main_color, unsigned char visible);
typedef int (*pnt_graphic_arc_modify)(int id, float start_angle, float end_angle);

typedef int (*pnt_graphic_calc_create)(int x, int y, int w, int h, unsigned char visible);

typedef int (*pnt_graphic_path_create)(int color, unsigned char filled, int border_color,
                                       unsigned char visible);
typedef int (*pnt_graphic_path_set_starting_point)(int id, float start_x, float start_y);
typedef int (*pnt_graphic_path_add_line)(int id, float destination_x,
                                         float destination_y);
typedef int (*pnt_graphic_path_add_arc)(int id, float center_x, float center_y, float w,
                                        float h, float start_angle, float end_angle);
typedef int (*pnt_graphic_path_draw)(int id);

typedef int (*pnt_graphic_view_create)(int x, int y, int w, int h, int color,
                                       unsigned char visible);
typedef int (*pnt_graphic_view_zoom_in)(int id, int level);
typedef int (*pnt_graphic_view_zoom_out)(int id, int level);
typedef int (*pnt_graphic_view_fit_content)(int id);
typedef int (*pnt_graphic_view_fit_object)(int id, int id_object);
typedef int (*pnt_graphic_view_fit_rect)(int id, float x, float y, float w, float h);

typedef enum {
    DIRECTION_LEFT  = 0,
    DIRECTION_RIGHT = 1,
    DIRECTION_UP    = 2,
    DIRECTION_DOWN  = 3

} _DIRECTION_TYPE;

typedef int (*pnt_graphic_view_move)(int id, int direction);
typedef int (*pnt_graphic_view_set_move_enable)(int id, unsigned char h_enable,
                                                unsigned char v_enable);
typedef int (*pnt_graphic_view_clear)(int id);

typedef int (*pnt_graphic_view_add_line)(int id_viewer, float x1, float y1, float x2,
                                         float y2, int color, unsigned char visible);
typedef int (*pnt_graphic_view_add_rect)(int id_viewer, float x, float y, float w,
                                         float h, int color, unsigned char filled, int border_color, unsigned char visible);
typedef int (*pnt_graphic_view_add_ellipse)(int id_viewer, float center_x, float center_y,
                                            float w, float h, int color, unsigned char filled, int border_color,
                                            unsigned char visible);
typedef int (*pnt_graphic_view_add_image)(int id_viewer, float x, float y, float w,
                                          float h, int image_id, unsigned char scaled, unsigned char visible);
typedef int (*pnt_graphic_view_add_text)(int id_viewer, float x, float y, float w,
                                         float h, char *text, int text_color, unsigned char transparency, int background_color,
                                         char *font, int point_size, unsigned char bold, unsigned char italic, int alignment,
                                         unsigned char visible);
typedef int (*pnt_graphic_view_add_arc)(int id_viewer, float center_x, float center_y,
                                        float w, float h, float start_angle, float end_angle, int main_color,
                                        unsigned char visible);
typedef int (*pnt_graphic_view_add_path)(int id_viewer, int color, unsigned char filled,
                                         int border_color, unsigned char visible);

typedef int (*pnt_graphic_bitmap_create)(int x, int y, int w, int h,
                                         unsigned char visible);
typedef int (*pnt_graphic_bitmap_draw_background)(int id, int background_color);
typedef int (*pnt_graphic_bitmap_draw_pixel)(int id, int x, int y, int color);
typedef int (*pnt_graphic_bitmap_get_pixel_color)(int id, int x, int y);
typedef int (*pnt_graphic_bitmap_set_area)(int id, int x, int y, int w, int h);
typedef int (*pnt_graphic_bitmap_get_area)(int id, int x, int y, int w, int h);
typedef int (*pnt_graphic_bitmap_copy_area)(int id, int x, int y, int w, int h, int x_new,
                                            int y_new);
typedef int (*pnt_graphic_bitmap_draw_line)(int id, int x1, int y1, int x2, int y2,
                                            int color, int thickness);
typedef int (*pnt_graphic_bitmap_draw_rect)(int id, int x, int y, int w, int h, int color,
                                            unsigned char filled, int border_color, unsigned char smooth);
typedef int (*pnt_graphic_bitmap_draw_ellipse)(int id, int center_x, int center_y, int w,
                                               int h, int color, unsigned char filled, int border_color);
typedef int (*pnt_graphic_bitmap_draw_arc)(int id, int center_x, int center_y, int w,
                                           int h, float start_angle, float end_angle, int color, int thickness);
typedef int (*pnt_graphic_bitmap_draw_image)(int id, int x, int y, int image_id);
typedef int (*pnt_graphic_bitmap_draw_text)(int id, int x, int y, int w, int h,
                                            char *text, int text_color, unsigned char transparency, int background_color, char *font,
                                            int point_size, unsigned char bold, unsigned char italic, int alignment);
typedef int (*pnt_graphic_bitmap_draw_dynamic_string)(int id, int x, int y, int w, int h,
                                                      int dynamic_string_id, int message_id, int text_color, unsigned char transparency,
                                                      int background_color, char *font, int point_size, unsigned char bold,
                                                      unsigned char italic, int alignment);
typedef int (*pnt_graphic_bitmap_update)(int id);

typedef int (*pnt_graphic_get_dynamic_string_size)(int dynamic_string_id, int message_id);
typedef int (*pnt_graphic_get_dynamic_string_text)(char *text_string, int string_size,
                                                   int dynamic_string_id, int message_id);

typedef enum _ISO_VIEWER_MODE {
    ISO_VIEWER_MODE_LIST       = 0,
    ISO_VIEWER_MODE_PREVIEW    = 1

} _ISO_VIEWER_MODE;

typedef enum {
    ISO_TASK_1                 = 0,
    ISO_TASK_2                 = 1,
    ISO_TASK_3                 = 2

} _ISO_TASK_ID;

typedef enum {
    ISO_VIEWER_AXIS_PATH_MODE_REAL      = 0,
    ISO_VIEWER_AXIS_PATH_MODE_NONE      = 1,
    ISO_VIEWER_AXIS_PATH_MODE_IDEAL     = 2

} _ISO_VIEWER_AXIS_PATH_MODE;

typedef int (*pnt_graphic_iso_viewer_create)(int x, int y, int w, int h,
                                             _ISO_TASK_ID iso_task_id, _ISO_VIEWER_MODE mode);
typedef int (*pnt_graphic_iso_viewer_draw)(int id);
typedef int (*pnt_graphic_iso_viewer_background_color)(int id, int color);
typedef int (*pnt_graphic_iso_viewer_foreground_color)(int id, int color);
typedef int (*pnt_graphic_iso_viewer_font)(int id, char *font, int point_size,
                                           unsigned char bold, unsigned char italic);
typedef int (*pnt_graphic_iso_viewer_line_number_area_background_color)(int id,
                                                                        int color);
typedef int (*pnt_graphic_iso_viewer_line_number_area_foreground_color)(int id,
                                                                        int color);
typedef int (*pnt_graphic_iso_viewer_status_bar_visible)(int id, unsigned char visible);
typedef int (*pnt_graphic_iso_viewer_axes)(int id, unsigned char horizontal_axis,
                                           unsigned char vertical_axis);
typedef int (*pnt_graphic_iso_viewer_grid_color)(int id, int color);
typedef int (*pnt_graphic_iso_viewer_grid_spacing)(int id, float spacing);
typedef int (*pnt_graphic_iso_viewer_grid_lines_width)(int id, float linesWidth);
typedef int (*pnt_graphic_iso_viewer_axis_path_color)(int id, int color);
typedef int (*pnt_graphic_iso_viewer_maximum_zoom)(int id, float maxZoom);
typedef int (*pnt_graphic_iso_viewer_zoom)(int id, float zoom);
typedef int (*pnt_graphic_iso_viewer_fit)(int id);
typedef int (*pnt_graphic_iso_viewer_change_mode)(int id, _ISO_VIEWER_MODE mode);
typedef int (*pnt_graphic_iso_viewer_horizontal_mirror)(int id, unsigned char enable);
typedef int (*pnt_graphic_iso_viewer_vertical_mirror)(int id, unsigned char enable);
typedef int (*pnt_graphic_iso_viewer_axis_path_mode)(int id,
                                                     _ISO_VIEWER_AXIS_PATH_MODE mode);
typedef int (*pnt_graphic_iso_viewer_starting_line_color)(int id, int color);

typedef int (*pnt_graphic_iso_viewer_starting_line_selection_enable)(int id_preview,
                                                                     int id_list);
typedef int (*pnt_graphic_iso_viewer_starting_line_selection_disable)(int id_preview,
                                                                      int id_list);
typedef int (*pnt_iso_starting_line_selection_status)(_ISO_TASK_ID iso_task_id);

typedef int (*pnt_graphic_change_HMI)(unsigned short id_hmi, unsigned char id_page);

typedef void (*pnt_thread_usleep)(unsigned int us);

typedef enum {
    NETWORK_PROTOCOL_TCP        = 0,
    NETWORK_PROTOCOL_UDP        = 1

} _NETWORK_PROTOCOL;

typedef enum {
    NETWORK_STATE_UNCONNECTED    = 0,
    NETWORK_STATE_CONNECTING     = 1,
    NETWORK_STATE_CONNECTED      = 2

} _NETWORK_STATE;

typedef int (*pnt_network_create)(_NETWORK_PROTOCOL protocol);
typedef int (*pnt_network_delete)(int id);
typedef int (*pnt_network_connect)(int id, unsigned char ip_address1,
                                   unsigned char ip_address2, unsigned char ip_address3, unsigned char ip_address4,
                                   unsigned short port);
typedef int (*pnt_network_disconnect)(int id);
typedef int (*pnt_network_listen)(int id, unsigned short port);
typedef _NETWORK_STATE (*pnt_network_state)(int id);
typedef int (*pnt_network_bytes_available)(int id);
typedef int (*pnt_network_read)(int id, char *data, int size);
typedef int (*pnt_network_write)(int id, char *data, int size);
typedef int (*pnt_network_ping)(unsigned char ip_address1, unsigned char ip_address2,
                                unsigned char ip_address3, unsigned char ip_address4);

typedef int (*pnt_keyboard_bytes_available)(void);
typedef int (*pnt_keyboard_read)(char *data, int size);

typedef enum {
    SERIAL_PORT_RS232       = 0,
    SERIAL_PORT_RS485_1     = 1,
    SERIAL_PORT_RS485_2     = 2,

    SERIAL_PORT_SYSTEM      = 255

} _SERIAL_PORT;

#ifndef CSHARED_EAS_LIB
typedef enum {
    SERIAL_PORT_BAUDRATE_9600,
    SERIAL_PORT_BAUDRATE_19200,
    SERIAL_PORT_BAUDRATE_38400,
    SERIAL_PORT_BAUDRATE_57600,
    SERIAL_PORT_BAUDRATE_115200,
    SERIAL_PORT_BAUDRATE_2400,
    SERIAL_PORT_BAUDRATE_4800,

    SERIAL_PORT_BAUDRATE_MAX

} _SERIAL_PORT_BAUDRATE;
#else
#if (defined(FIRMWARE_VERSION) && FIRMWARE_VERSION > 0x184101)
typedef enum {
    SERIAL_PORT_BAUDRATE_9600,
    SERIAL_PORT_BAUDRATE_19200,
    SERIAL_PORT_BAUDRATE_38400,
    SERIAL_PORT_BAUDRATE_57600,
    SERIAL_PORT_BAUDRATE_115200,
    SERIAL_PORT_BAUDRATE_2400,
    SERIAL_PORT_BAUDRATE_4800,

    SERIAL_PORT_BAUDRATE_MAX

} _SERIAL_PORT_BAUDRATE;
#elif (defined(FIRMWARE_VERSION) && (FIRMWARE_VERSION == 0x184100 || FIRMWARE_VERSION == 0x184101))
typedef enum {
    SERIAL_PORT_BAUDRATE_2400,
    SERIAL_PORT_BAUDRATE_4800,
    SERIAL_PORT_BAUDRATE_9600,
    SERIAL_PORT_BAUDRATE_19200,
    SERIAL_PORT_BAUDRATE_38400,
    SERIAL_PORT_BAUDRATE_57600,
    SERIAL_PORT_BAUDRATE_115200,

    SERIAL_PORT_BAUDRATE_MAX

} _SERIAL_PORT_BAUDRATE;
#else
typedef enum {
    SERIAL_PORT_BAUDRATE_9600,
    SERIAL_PORT_BAUDRATE_19200,
    SERIAL_PORT_BAUDRATE_38400,
    SERIAL_PORT_BAUDRATE_57600,
    SERIAL_PORT_BAUDRATE_115200,

    SERIAL_PORT_BAUDRATE_MAX

} _SERIAL_PORT_BAUDRATE;
#endif
#endif

typedef enum {
    SERIAL_PORT_DATA_BITS_5,
    SERIAL_PORT_DATA_BITS_6,
    SERIAL_PORT_DATA_BITS_7,
    SERIAL_PORT_DATA_BITS_8,

    SERIAL_PORT_DATA_BITS_MAX

} _SERIAL_PORT_DATA_BITS;

typedef enum {
    SERIAL_PORT_PARITY_NONE,
    SERIAL_PORT_PARITY_ODD,
    SERIAL_PORT_PARITY_EVEN,

    SERIAL_PORT_PARITY_MAX

} _SERIAL_PORT_PARITY;

typedef enum {
    SERIAL_PORT_STOP_BITS_1,
    SERIAL_PORT_STOP_BITS_2,

    SERIAL_PORT_STOP_BITS_MAX

} _SERIAL_PORT_STOP_BITS;

typedef int (*pnt_serial_open)(_SERIAL_PORT port, _SERIAL_PORT_BAUDRATE baudRate,
                               _SERIAL_PORT_DATA_BITS dataBits, _SERIAL_PORT_PARITY parity,
                               _SERIAL_PORT_STOP_BITS stopBits);
typedef int (*pnt_serial_open_from_name)(char *serialPortName,
                                         _SERIAL_PORT_BAUDRATE baudRate, _SERIAL_PORT_DATA_BITS dataBits,
                                         _SERIAL_PORT_PARITY parity, _SERIAL_PORT_STOP_BITS stopBits);
typedef int (*pnt_serial_close)(int id);
typedef int (*pnt_serial_bytes_available)(int id);
typedef int (*pnt_serial_read)(int id, char *data, int size);
typedef int (*pnt_serial_send)(int id, char *data, int size);

typedef int (*pnt_kinematics_to_robot)(float cartesian_coords[], float robot_coords[]);
typedef int (*pnt_kinematics_to_cartesian)(float cartesian_coords[],
                                           float robot_coords[]);

typedef double (*pnt_iso_variable_read)(_ISO_TASK_ID iso_task, int variable_index,
                                        int *err);
typedef void (*pnt_iso_variable_write)(_ISO_TASK_ID iso_task, int variable_index,
                                       double value, int *err);

typedef struct _iso_file_analisys_info {
    unsigned int error_code;
    unsigned int line_number;
    double min_horizontal;
    double max_horizontal;
    double min_vertical;
    double max_vertical;

} _iso_file_analisys_info;

typedef enum _PLANE_DIMENSIONS {
    HORIZONTAL_DIMENSION = 0,
    VERTICAL_DIMENSION   = 1,

    PLANE_DIMENSIONS
} _PLANE_DIMENSIONS;

typedef enum _ARC_ROTATION {
    ARC_ROTATION_CLOCKWISE = 0,
    ARC_ROTATION_ANTICLOCKWISE = 1

} _ARC_ROTATION;

typedef int (*pntDrawLine)(double start_point[PLANE_DIMENSIONS],
                           double end_point[PLANE_DIMENSIONS]);
typedef int (*pntDrawArc)(double start_point[PLANE_DIMENSIONS],
                          double end_point[PLANE_DIMENSIONS],
                          double center[PLANE_DIMENSIONS],
                          _ARC_ROTATION verso);


typedef int (*pnt_iso_file_analisys)(_ISO_TASK_ID iso_task, _iso_file_analisys_info *info,
                                     pntDrawLine drawLine, pntDrawArc drawArc);
typedef int (*pnt_iso_file_load)(_ISO_TASK_ID iso_task, char *file_name, _FILE_PATH path);
typedef int (*pnt_iso_file_start)(_ISO_TASK_ID iso_task);
typedef int (*pnt_iso_file_stop)(_ISO_TASK_ID iso_task);
typedef int (*pnt_iso_file_reset)(_ISO_TASK_ID iso_task);

typedef int (*pnt_flying_shear_running)(int flying_shear_index);
typedef int (*pnt_flying_shear_set_slave_periodic)(int flying_shear_index,
                                                   double slave_period_start_position, double slave_period,
                                                   double master_period_start_position, double master_period);
typedef int (*pnt_flying_shear_reset_slave_periodic)(int flying_shear_index);
typedef int (*pnt_flying_shear_is_slave_periodic)(int flying_shear_index,
                                                  unsigned char *is_periodic);
typedef int (*pnt_flying_shear_set_slave_period_position)(int flying_shear_index,
                                                          double position, double ramp_units_s2);
typedef int (*pnt_flying_shear_reset_slave_period_position_correction)(
    int flying_shear_index, double ramp_units_s);
typedef int (*pnt_flying_shear_add_slave_phase)(int flying_shear_index,
                                                double slave_phase_increment, double ramp_units_s2);
typedef int (*pnt_flying_shear_reset_slave_phase)(int flying_shear_index,
                                                  double ramp_units_s2);
typedef int (*pnt_flying_shear_set_current_slave_phase)(int flying_shear_index,
                                                        double slave_phase, double ramp_units_s2);
typedef int (*pnt_flying_shear_get_current_slave_phase)(int flying_shear_index,
                                                        double *slave_phase);
typedef int (*pnt_flying_shear_get_slave_value)(int flying_shear_index,
                                                double master_period_position, double *value);
typedef int (*pnt_flying_shear_get_master_value)(int flying_shear_index,
                                                 double slave_period_position, double *value);
typedef int (*pnt_flying_shear_get_slave_value_from_absolute)(int flying_shear_index,
                                                              double master_absolute_position, double *value);
typedef int (*pnt_flying_shear_get_master_value_from_absolute)(int flying_shear_index,
                                                               double slave_absolute_position, double *value);
typedef int (*pnt_flying_shear_get_slave_period_position)(int flying_shear_index,
                                                          double slave_absolute_position, double *value);
typedef int (*pnt_flying_shear_slave_sync)(int flying_shear_index);
typedef int (*pnt_flying_shear_slave_sync_evaluate)(int flying_shear_index,
                                                    double master_period_start_position, double master_period_stop_position);
typedef int (*pnt_flying_shear_slave_sync_in_progress)(int flying_shear_index);
typedef int (*pnt_flying_shear_slave_stop_at)(int flying_shear_index);
typedef int (*pnt_flying_shear_slave_stop_at_evaluate)(int flying_shear_index,
                                                       double master_period_start_position, double master_period_end_position);
typedef int (*pnt_flying_shear_slave_stop_at_stopped)(int flying_shear_index);
typedef int (*pnt_flying_shear_slave_stop_at_in_progress)(int flying_shear_index);

typedef int (*pnt_flying_shear_cam_get_element_size)(void);
typedef int (*pnt_flying_shear_cam_init)(int flying_shear_index, void *pnt_elements,
                                         unsigned int cam_max_elements);
typedef int (*pnt_flying_shear_cam_reset)(int flying_shear_index);
typedef int (*pnt_flying_shear_cam_set_left_boundary_condition)(int flying_shear_index,
                                                                int type, double val);
typedef int (*pnt_flying_shear_cam_set_right_boundary_condition)(int flying_shear_index,
                                                                 int type, double val);
typedef int (*pnt_flying_shear_cam_add_element)(int flying_shear_index,
                                                double master_position, double slave_position, int type);
typedef int (*pnt_flying_shear_cam_evaluate)(int flying_shear_index);

typedef int (*pnt_thermoregulation_factors_get)(int profile_index, float *K, float *T,
                                                float *Ti, float *Td);
typedef int (*pnt_thermoregulation_factors_set)(int profile_index, float K, float T,
                                                float Ti, float Td);

typedef int (*pnt_motion_set_word_feedback_axis_simulation)(int axis_index,
                                                            unsigned char simulation_on);

typedef int (*pnt_shell_command_exec)(char *command, unsigned int auth_code,
                                      char **std_output, char **std_error);

typedef enum _Orientation {
    VerticalLeft = 0x1,
    VerticalRight = 0x2,
    HorizontalTop = 0x3,
    HorizontalBottom = 0x4
} _Orientation;

typedef int (*pnt_graphic_iso_editor_create)(int x, int y, int w, int h,
                                             _ISO_TASK_ID iso_task_id,
                                             _Orientation buttonBarOrientation);
typedef int (*pnt_graphic_iso_editor_draw)(int id);
typedef int (*pnt_graphic_iso_editor_visualize_title)(int id,
                                                      unsigned char visualizeTitle);
typedef int (*pnt_graphic_iso_editor_background_color)(int id, int color);
typedef int (*pnt_graphic_iso_editor_foreground_color)(int id, int color);
typedef int (*pnt_graphic_iso_editor_font)(int id, char *font, int point_size,
                                           unsigned char bold, unsigned char italic);
typedef int (*pnt_graphic_iso_editor_line_number_area_background_color)(int id,
                                                                        int color);
typedef int (*pnt_graphic_iso_editor_line_number_area_foreground_color)(int id,
                                                                        int color);


typedef enum _SQL_DRIVER_TYPE {
    MSSQL = 0,
    MySQL = 1
} SQL_DRIVER_TYPE;

typedef enum _SQL_DATA_TYPE {
    INTEGER = 0,
    FLOATING_POINT = 1,
    STRING = 2
} SQL_DATA_TYPE;

typedef void (*p_sql_set_timeout)(int connectionTimeout, int loginTimeout);
typedef int (*p_sql_db_connect)(SQL_DRIVER_TYPE driverType, char *hostname,
                                int port, char *username, char *psw, char *database);
typedef int (*p_sql_db_disconnect)(unsigned int db_id);
typedef int (*p_sql_exec)(unsigned int db_id, char *command);
typedef int (*p_sql_query)(unsigned int db_id, char *command, int *rows, int *columns);
typedef int (*p_sql_stored_procedure)(unsigned int db_id, char *command, int *columns);
typedef int (*p_sql_next)(unsigned int db_id, unsigned int q_id);
typedef int (*p_sql_get_value)(unsigned int db_id, unsigned int q_id, int row,
                               int column, char *value, int value_size);
typedef int (*p_sql_get_value_type)(unsigned int db_id, unsigned int q_id, int row,
                                    int column, SQL_DATA_TYPE *value_type);
typedef int (*p_sql_delete_query)(unsigned int db_id, unsigned int q_id);

typedef int (*pnt_system_clock_read)(unsigned short *year, unsigned char *month,
                                     unsigned char *day, unsigned char *hour,
                                     unsigned char *min, unsigned char *sec,
                                     unsigned short *millisec);


CSHARED_EXPORT extern pnt_file_open           f_pnt_file_open;
CSHARED_EXPORT extern pnt_file_open           f_pnt_file_open_append;
CSHARED_EXPORT extern pnt_file_creat          f_pnt_file_creat;
CSHARED_EXPORT extern pnt_file_close          f_pnt_file_close;
CSHARED_EXPORT extern pnt_file_remove         f_pnt_file_remove;
CSHARED_EXPORT extern pnt_file_read_line      f_pnt_file_read_line;
CSHARED_EXPORT extern pnt_file_write_line     f_pnt_file_write_line;
CSHARED_EXPORT extern pnt_file_read           f_pnt_file_read;
CSHARED_EXPORT extern pnt_file_write          f_pnt_file_write;
CSHARED_EXPORT extern pnt_file_copy           f_pnt_file_copy;
CSHARED_EXPORT extern pnt_file_rename         f_pnt_file_rename;
CSHARED_EXPORT extern pnt_file_get_path       f_pnt_file_get_path;
CSHARED_EXPORT extern pnt_file_get_path_size  f_pnt_file_get_path_size;
CSHARED_EXPORT extern pnt_file_open_select    f_pnt_file_open_select;
CSHARED_EXPORT extern pnt_file_save_select    f_pnt_file_save_select;
CSHARED_EXPORT extern pnt_file_remove_multiselect f_pnt_file_remove_multiselect;
CSHARED_EXPORT extern pnt_file_copy_multiselect f_pnt_file_copy_multiselect;
CSHARED_EXPORT extern pnt_file_list_get       f_pnt_file_list_get;
CSHARED_EXPORT extern pnt_file_list_size_get  f_pnt_file_list_size_get;
CSHARED_EXPORT extern pnt_file_list_file_get  f_pnt_file_list_file_get;
CSHARED_EXPORT extern pnt_file_list_copy      f_pnt_file_list_copy;
CSHARED_EXPORT extern pnt_file_list_remove    f_pnt_file_list_remove;
CSHARED_EXPORT extern pnt_file_list_get_multiselect f_pnt_file_list_get_multiselect;
CSHARED_EXPORT extern pnt_file_size           f_pnt_file_size;
CSHARED_EXPORT extern pnt_file_exists         f_pnt_file_exists;
CSHARED_EXPORT extern pnt_file_seek_beginning f_pnt_file_seek_beginning;
CSHARED_EXPORT extern pnt_file_seek_end       f_pnt_file_seek_end;
CSHARED_EXPORT extern pnt_file_seek_absolute  f_pnt_file_seek_absolute;
CSHARED_EXPORT extern pnt_file_seek_relative  f_pnt_file_seek_relative;

CSHARED_EXPORT extern pnt_string_sprintf      f_pnt_string_sprintf;
CSHARED_EXPORT extern pnt_string_snprintf     f_pnt_string_snprintf;
CSHARED_EXPORT extern pnt_string_concatenate  f_pnt_string_concatenate;
CSHARED_EXPORT extern pnt_string_length       f_pnt_string_length;
CSHARED_EXPORT extern pnt_string_compare      f_pnt_string_compare;
CSHARED_EXPORT extern pnt_string_copy         f_pnt_string_copy;
CSHARED_EXPORT extern pnt_string_atoi         f_pnt_string_atoi;
CSHARED_EXPORT extern pnt_string_atof         f_pnt_string_atof;
CSHARED_EXPORT extern pnt_string_atol         f_pnt_string_atol;
CSHARED_EXPORT extern pnt_string_atoll        f_pnt_string_atoll;
CSHARED_EXPORT extern pnt_string_tod          f_pnt_string_tod;
CSHARED_EXPORT extern pnt_string_told         f_pnt_string_told;
CSHARED_EXPORT extern pnt_string_tof          f_pnt_string_tof;
CSHARED_EXPORT extern pnt_string_tol          f_pnt_string_tol;
CSHARED_EXPORT extern pnt_string_toll         f_pnt_string_toll;
CSHARED_EXPORT extern pnt_string_toul         f_pnt_string_toul;
CSHARED_EXPORT extern pnt_string_toull        f_pnt_string_toull;
CSHARED_EXPORT extern pnt_string_str          f_pnt_string_str;
CSHARED_EXPORT extern pnt_string_chr          f_pnt_string_chr;
CSHARED_EXPORT extern pnt_string_rchr         f_pnt_string_rchr;
CSHARED_EXPORT extern pnt_string_spn          f_pnt_string_spn;
CSHARED_EXPORT extern pnt_string_cspn         f_pnt_string_cspn;
CSHARED_EXPORT extern pnt_string_tok          f_pnt_string_tok;
CSHARED_EXPORT extern pnt_string_pbrk         f_pnt_string_pbrk;
CSHARED_EXPORT extern pnt_string_ncpy         f_pnt_string_ncpy;
CSHARED_EXPORT extern pnt_string_ncat         f_pnt_string_ncat;
CSHARED_EXPORT extern pnt_string_ncmp         f_pnt_string_ncmp;
CSHARED_EXPORT extern pnt_string_trim         f_pnt_string_trim;

CSHARED_EXPORT extern pnt_math_div            f_pnt_math_div;
CSHARED_EXPORT extern pnt_math_ldiv           f_pnt_math_ldiv;
CSHARED_EXPORT extern pnt_math_lldiv          f_pnt_math_lldiv;

CSHARED_EXPORT extern pnt_memory_set          f_pnt_memory_set;
CSHARED_EXPORT extern pnt_memory_compare      f_pnt_memory_compare;
CSHARED_EXPORT extern pnt_memory_move         f_pnt_memory_move;
CSHARED_EXPORT extern pnt_memory_copy         f_pnt_memory_copy;
CSHARED_EXPORT extern pnt_memory_alloc        f_pnt_memory_alloc;
CSHARED_EXPORT extern pnt_memory_calloc       f_pnt_memory_calloc;
CSHARED_EXPORT extern pnt_memory_free         f_pnt_memory_free;
CSHARED_EXPORT extern pnt_memory_realloc      f_pnt_memory_realloc;

CSHARED_EXPORT extern pnt_list_create         f_pnt_list_create;
CSHARED_EXPORT extern pnt_list_clear          f_pnt_list_clear;
CSHARED_EXPORT extern pnt_list_size           f_pnt_list_size;
CSHARED_EXPORT extern pnt_list_empty          f_pnt_list_empty;
CSHARED_EXPORT extern pnt_list_append         f_pnt_list_append;
CSHARED_EXPORT extern pnt_list_item_at        f_pnt_list_item_at;
CSHARED_EXPORT extern pnt_list_item_insert    f_pnt_list_item_insert;
CSHARED_EXPORT extern pnt_list_item_replace   f_pnt_list_item_replace;
CSHARED_EXPORT extern pnt_list_item_remove    f_pnt_list_item_remove;
CSHARED_EXPORT extern pnt_list_delete         f_pnt_list_delete;
CSHARED_EXPORT extern pnt_list_delete_all     f_pnt_list_delete_all;

CSHARED_EXPORT extern pnt_debug_print_string  f_pnt_debug_print_string;
CSHARED_EXPORT extern pnt_debug_print_int     f_pnt_debug_print_int;
CSHARED_EXPORT extern pnt_debug_print_float   f_pnt_debug_print_float;
CSHARED_EXPORT extern pnt_debug_print         f_pnt_debug_print;
CSHARED_EXPORT extern pnt_debug_time_start    f_pnt_debug_time_start;
CSHARED_EXPORT extern pnt_debug_time_elapsed_ms f_pnt_debug_time_elapsed_ms;

CSHARED_EXPORT extern pnt_thread_usleep       f_pnt_thread_usleep;

CSHARED_EXPORT extern pnt_graphic_get_screen_width
f_pnt_graphic_get_screen_width;
CSHARED_EXPORT extern pnt_graphic_get_screen_width
f_pnt_graphic_get_screen_height;

CSHARED_EXPORT extern pnt_graphic_objects_delete_all
f_pnt_graphic_objects_delete_all;
CSHARED_EXPORT extern pnt_graphic_objects_show_all
f_pnt_graphic_objects_show_all;
CSHARED_EXPORT extern pnt_graphic_objects_hide_all
f_pnt_graphic_objects_hide_all;
CSHARED_EXPORT extern pnt_graphic_objects_number
f_pnt_graphic_objects_number;

CSHARED_EXPORT extern pnt_graphic_object_get_type
f_pnt_graphic_object_get_type;
CSHARED_EXPORT extern pnt_graphic_object_delete
f_pnt_graphic_object_delete;
CSHARED_EXPORT extern pnt_graphic_object_show                   f_pnt_graphic_object_show;
CSHARED_EXPORT extern pnt_graphic_object_hide                   f_pnt_graphic_object_hide;
CSHARED_EXPORT extern pnt_graphic_object_set_visible
f_pnt_graphic_object_set_visible;
CSHARED_EXPORT extern pnt_graphic_object_set_filled
f_pnt_graphic_object_set_filled;
CSHARED_EXPORT extern pnt_graphic_object_move                   f_pnt_graphic_object_move;
CSHARED_EXPORT extern pnt_graphic_object_set_x
f_pnt_graphic_object_set_x;
CSHARED_EXPORT extern pnt_graphic_object_set_y
f_pnt_graphic_object_set_y;
CSHARED_EXPORT extern pnt_graphic_object_set_main_color
f_pnt_graphic_object_set_main_color;
CSHARED_EXPORT extern pnt_graphic_object_set_secondary_color
f_pnt_graphic_object_set_secondary_color;
CSHARED_EXPORT extern pnt_graphic_object_set_geometry
f_pnt_graphic_object_set_geometry;
CSHARED_EXPORT extern pnt_graphic_object_set_size
f_pnt_graphic_object_set_size;
CSHARED_EXPORT extern pnt_graphic_object_set_width
f_pnt_graphic_object_set_width;
CSHARED_EXPORT extern pnt_graphic_object_set_height
f_pnt_graphic_object_set_height;
CSHARED_EXPORT extern pnt_graphic_object_set_tag
f_pnt_graphic_object_set_tag;
CSHARED_EXPORT extern pnt_graphic_object_get_tag
f_pnt_graphic_object_get_tag;

CSHARED_EXPORT extern pnt_graphic_line_create                   f_pnt_graphic_line_create;
CSHARED_EXPORT extern pnt_graphic_line_modify                   f_pnt_graphic_line_modify;
CSHARED_EXPORT extern pnt_graphic_line_set_p1                   f_pnt_graphic_line_set_p1;
CSHARED_EXPORT extern pnt_graphic_line_set_p2                   f_pnt_graphic_line_set_p2;

CSHARED_EXPORT extern pnt_graphic_rect_create                   f_pnt_graphic_rect_create;

CSHARED_EXPORT extern pnt_graphic_ellipse_create
f_pnt_graphic_ellipse_create;

CSHARED_EXPORT extern pnt_graphic_image_create
f_pnt_graphic_image_create;
CSHARED_EXPORT extern pnt_graphic_image_get_width
f_pnt_graphic_image_get_width;
CSHARED_EXPORT extern pnt_graphic_image_get_height
f_pnt_graphic_image_get_height;
CSHARED_EXPORT extern pnt_graphic_image_create_from_file
f_pnt_graphic_image_create_from_file;
CSHARED_EXPORT extern pnt_graphic_image_get_width_from_file
f_pnt_graphic_image_get_width_from_file;
CSHARED_EXPORT extern pnt_graphic_image_get_height_from_file
f_pnt_graphic_image_get_height_from_file;
CSHARED_EXPORT extern pnt_graphic_image_modify
f_pnt_graphic_image_modify;

CSHARED_EXPORT extern pnt_graphic_text_create                   f_pnt_graphic_text_create;
CSHARED_EXPORT extern pnt_graphic_text_text_modify
f_pnt_graphic_text_text_modify;
CSHARED_EXPORT extern pnt_graphic_text_font_modify
f_pnt_graphic_text_font_modify;
CSHARED_EXPORT extern pnt_graphic_text_alignment_modify
f_pnt_graphic_text_alignment_modify;
CSHARED_EXPORT extern pnt_graphic_text_transparency_modify
f_pnt_graphic_text_transparency_modify;
CSHARED_EXPORT extern pnt_graphic_text_dynamic_string_create
f_pnt_graphic_text_dynamic_string_create;

CSHARED_EXPORT extern pnt_graphic_arc_create                    f_pnt_graphic_arc_create;
CSHARED_EXPORT extern pnt_graphic_arc_modify                    f_pnt_graphic_arc_modify;

CSHARED_EXPORT extern pnt_graphic_calc_create                   f_pnt_graphic_calc_create;

CSHARED_EXPORT extern pnt_graphic_path_create                   f_pnt_graphic_path_create;
CSHARED_EXPORT extern pnt_graphic_path_set_starting_point
f_pnt_graphic_path_set_starting_point;
CSHARED_EXPORT extern pnt_graphic_path_add_line
f_pnt_graphic_path_add_line;
CSHARED_EXPORT extern pnt_graphic_path_add_arc
f_pnt_graphic_path_add_arc;
CSHARED_EXPORT extern pnt_graphic_path_draw                     f_pnt_graphic_path_draw;

CSHARED_EXPORT extern pnt_graphic_view_create                   f_pnt_graphic_view_create;
CSHARED_EXPORT extern pnt_graphic_view_zoom_in
f_pnt_graphic_view_zoom_in;
CSHARED_EXPORT extern pnt_graphic_view_zoom_out
f_pnt_graphic_view_zoom_out;
CSHARED_EXPORT extern pnt_graphic_view_fit_content
f_pnt_graphic_view_fit_content;
CSHARED_EXPORT extern pnt_graphic_view_fit_object
f_pnt_graphic_view_fit_object;
CSHARED_EXPORT extern pnt_graphic_view_fit_rect
f_pnt_graphic_view_fit_rect;
CSHARED_EXPORT extern pnt_graphic_view_move                     f_pnt_graphic_view_move;
CSHARED_EXPORT extern pnt_graphic_view_set_move_enable
f_pnt_graphic_view_set_move_enable;
CSHARED_EXPORT extern pnt_graphic_view_clear                    f_pnt_graphic_view_clear;

CSHARED_EXPORT extern pnt_graphic_view_add_line
f_pnt_graphic_view_add_line;
CSHARED_EXPORT extern pnt_graphic_view_add_rect
f_pnt_graphic_view_add_rect;
CSHARED_EXPORT extern pnt_graphic_view_add_ellipse
f_pnt_graphic_view_add_ellipse;
CSHARED_EXPORT extern pnt_graphic_view_add_image
f_pnt_graphic_view_add_image;
CSHARED_EXPORT extern pnt_graphic_view_add_text
f_pnt_graphic_view_add_text;
CSHARED_EXPORT extern pnt_graphic_view_add_arc
f_pnt_graphic_view_add_arc;
CSHARED_EXPORT extern pnt_graphic_view_add_path
f_pnt_graphic_view_add_path;

CSHARED_EXPORT extern pnt_graphic_bitmap_create
f_pnt_graphic_bitmap_create;
CSHARED_EXPORT extern pnt_graphic_bitmap_draw_background
f_pnt_graphic_bitmap_draw_background;
CSHARED_EXPORT extern pnt_graphic_bitmap_draw_pixel
f_pnt_graphic_bitmap_draw_pixel;
CSHARED_EXPORT extern pnt_graphic_bitmap_get_pixel_color
f_pnt_graphic_bitmap_get_pixel_color;
CSHARED_EXPORT extern pnt_graphic_bitmap_set_area
f_pnt_graphic_bitmap_set_area;
CSHARED_EXPORT extern pnt_graphic_bitmap_get_area
f_pnt_graphic_bitmap_get_area;
CSHARED_EXPORT extern pnt_graphic_bitmap_copy_area
f_pnt_graphic_bitmap_copy_area;
CSHARED_EXPORT extern pnt_graphic_bitmap_draw_line
f_pnt_graphic_bitmap_draw_line;
CSHARED_EXPORT extern pnt_graphic_bitmap_draw_rect
f_pnt_graphic_bitmap_draw_rect;
CSHARED_EXPORT extern pnt_graphic_bitmap_draw_ellipse
f_pnt_graphic_bitmap_draw_ellipse;
CSHARED_EXPORT extern pnt_graphic_bitmap_draw_arc
f_pnt_graphic_bitmap_draw_arc;
CSHARED_EXPORT extern pnt_graphic_bitmap_draw_image
f_pnt_graphic_bitmap_draw_image;
CSHARED_EXPORT extern pnt_graphic_bitmap_draw_text
f_pnt_graphic_bitmap_draw_text;
CSHARED_EXPORT extern pnt_graphic_bitmap_draw_dynamic_string
f_pnt_graphic_bitmap_draw_dynamic_string;
CSHARED_EXPORT extern pnt_graphic_bitmap_update
f_pnt_graphic_bitmap_update;

CSHARED_EXPORT extern pnt_graphic_get_dynamic_string_size
f_pnt_graphic_get_dynamic_string_size;
CSHARED_EXPORT extern pnt_graphic_get_dynamic_string_text
f_pnt_graphic_get_dynamic_string_text;

CSHARED_EXPORT extern pnt_graphic_iso_viewer_create
f_pnt_graphic_iso_viewer_create;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_draw
f_pnt_graphic_iso_viewer_draw;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_background_color
f_pnt_graphic_iso_viewer_background_color;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_foreground_color
f_pnt_graphic_iso_viewer_foreground_color;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_font
f_pnt_graphic_iso_viewer_font;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_line_number_area_background_color
f_pnt_graphic_iso_viewer_line_number_area_background_color;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_line_number_area_foreground_color
f_pnt_graphic_iso_viewer_line_number_area_foreground_color;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_status_bar_visible
f_pnt_graphic_iso_viewer_status_bar_visible;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_axes
f_pnt_graphic_iso_viewer_axes;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_grid_color
f_pnt_graphic_iso_viewer_grid_color;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_grid_spacing
f_pnt_graphic_iso_viewer_grid_spacing;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_grid_lines_width
f_pnt_graphic_iso_viewer_grid_lines_width;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_axis_path_color
f_pnt_graphic_iso_viewer_axis_path_color;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_starting_line_color
f_pnt_graphic_iso_viewer_starting_line_color;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_maximum_zoom
f_pnt_graphic_iso_viewer_maximum_zoom;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_zoom
f_pnt_graphic_iso_viewer_zoom;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_fit
f_pnt_graphic_iso_viewer_fit;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_change_mode
f_pnt_graphic_iso_viewer_change_mode;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_horizontal_mirror
f_pnt_graphic_iso_viewer_horizontal_mirror;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_vertical_mirror
f_pnt_graphic_iso_viewer_vertical_mirror;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_axis_path_mode
f_pnt_graphic_iso_viewer_axis_path_mode;

CSHARED_EXPORT extern pnt_graphic_iso_viewer_starting_line_selection_enable
f_pnt_graphic_iso_viewer_starting_line_selection_enable;
CSHARED_EXPORT extern pnt_graphic_iso_viewer_starting_line_selection_disable
f_pnt_graphic_iso_viewer_starting_line_selection_disable;
CSHARED_EXPORT extern pnt_iso_starting_line_selection_status
f_pnt_iso_starting_line_selection_status;

CSHARED_EXPORT extern pnt_graphic_change_HMI                    f_pnt_graphic_change_HMI;

CSHARED_EXPORT extern pnt_network_create                        f_pnt_network_create;
CSHARED_EXPORT extern pnt_network_delete                        f_pnt_network_delete;
CSHARED_EXPORT extern pnt_network_connect                       f_pnt_network_connect;
CSHARED_EXPORT extern pnt_network_disconnect                    f_pnt_network_disconnect;
CSHARED_EXPORT extern pnt_network_listen                        f_pnt_network_listen;
CSHARED_EXPORT extern pnt_network_state                         f_pnt_network_state;
CSHARED_EXPORT extern pnt_network_bytes_available
f_pnt_network_bytes_available;
CSHARED_EXPORT extern pnt_network_read                          f_pnt_network_read;
CSHARED_EXPORT extern pnt_network_write                         f_pnt_network_write;
CSHARED_EXPORT extern pnt_network_ping                          f_pnt_network_ping;

CSHARED_EXPORT extern pnt_keyboard_bytes_available
f_pnt_keyboard_bytes_available;
CSHARED_EXPORT extern pnt_keyboard_read                         f_pnt_keyboard_read;

CSHARED_EXPORT extern pnt_serial_open                           f_pnt_serial_open;
CSHARED_EXPORT extern pnt_serial_open_from_name
f_pnt_serial_open_from_name;
CSHARED_EXPORT extern pnt_serial_close                          f_pnt_serial_close;
CSHARED_EXPORT extern pnt_serial_bytes_available
f_pnt_serial_bytes_available;
CSHARED_EXPORT extern pnt_serial_read                           f_pnt_serial_read;
CSHARED_EXPORT extern pnt_serial_send                           f_pnt_serial_send;

CSHARED_EXPORT extern pnt_kinematics_to_robot                   f_pnt_kinematics_to_robot;
CSHARED_EXPORT extern pnt_kinematics_to_cartesian
f_pnt_kinematics_to_cartesian;

CSHARED_EXPORT extern pnt_iso_variable_read                     f_pnt_iso_variable_read;
CSHARED_EXPORT extern pnt_iso_variable_write                    f_pnt_iso_variable_write;
CSHARED_EXPORT extern pnt_iso_file_analisys                     f_pnt_iso_file_preview;
CSHARED_EXPORT extern pnt_iso_file_load                         f_pnt_iso_file_load;
CSHARED_EXPORT extern pnt_iso_file_start                        f_pnt_iso_file_start;
CSHARED_EXPORT extern pnt_iso_file_stop                         f_pnt_iso_file_stop;
CSHARED_EXPORT extern pnt_iso_file_reset                        f_pnt_iso_file_reset;

CSHARED_EXPORT extern pnt_flying_shear_running
f_pnt_flying_shear_running;
CSHARED_EXPORT extern pnt_flying_shear_set_slave_periodic
f_pnt_flying_shear_set_slave_periodic;
CSHARED_EXPORT extern pnt_flying_shear_reset_slave_periodic
f_pnt_flying_shear_reset_slave_periodic;
CSHARED_EXPORT extern pnt_flying_shear_is_slave_periodic
f_pnt_flying_shear_is_slave_periodic;
CSHARED_EXPORT extern pnt_flying_shear_set_slave_period_position
f_pnt_flying_shear_set_slave_period_position;
CSHARED_EXPORT extern pnt_flying_shear_reset_slave_period_position_correction
f_pnt_flying_shear_reset_slave_period_position_correction;
CSHARED_EXPORT extern pnt_flying_shear_add_slave_phase
f_pnt_flying_shear_add_slave_phase;
CSHARED_EXPORT extern pnt_flying_shear_reset_slave_phase
f_pnt_flying_shear_reset_slave_phase;
CSHARED_EXPORT extern pnt_flying_shear_set_current_slave_phase
f_pnt_flying_shear_set_current_slave_phase;
CSHARED_EXPORT extern pnt_flying_shear_get_current_slave_phase
f_pnt_flying_shear_get_current_slave_phase;
CSHARED_EXPORT extern pnt_flying_shear_get_slave_value
f_pnt_flying_shear_get_slave_value;
CSHARED_EXPORT extern pnt_flying_shear_get_master_value
f_pnt_flying_shear_get_master_value;
CSHARED_EXPORT extern pnt_flying_shear_get_slave_value_from_absolute
f_pnt_flying_shear_get_slave_value_from_absolute;
CSHARED_EXPORT extern pnt_flying_shear_get_master_value_from_absolute
f_pnt_flying_shear_get_master_value_from_absolute;
CSHARED_EXPORT extern pnt_flying_shear_get_slave_period_position
f_pnt_flying_shear_get_slave_period_position;
CSHARED_EXPORT extern pnt_flying_shear_slave_sync
f_pnt_flying_shear_slave_sync;
CSHARED_EXPORT extern pnt_flying_shear_slave_sync_evaluate
f_pnt_flying_shear_slave_sync_evaluate;
CSHARED_EXPORT extern pnt_flying_shear_slave_sync_in_progress
f_pnt_flying_shear_slave_sync_in_progress;
CSHARED_EXPORT extern pnt_flying_shear_slave_stop_at
f_pnt_flying_shear_slave_stop_at;
CSHARED_EXPORT extern pnt_flying_shear_slave_stop_at_evaluate
f_pnt_flying_shear_slave_stop_at_evaluate;
CSHARED_EXPORT extern pnt_flying_shear_slave_stop_at_stopped
f_pnt_flying_shear_slave_stop_at_stopped;
CSHARED_EXPORT extern pnt_flying_shear_slave_stop_at_in_progress
f_pnt_flying_shear_slave_stop_at_in_progress;

CSHARED_EXPORT extern pnt_flying_shear_cam_get_element_size
f_pnt_flying_shear_cam_get_element_size;
CSHARED_EXPORT extern pnt_flying_shear_cam_init
f_pnt_flying_shear_cam_init;
CSHARED_EXPORT extern pnt_flying_shear_cam_reset
f_pnt_flying_shear_cam_reset;
CSHARED_EXPORT extern pnt_flying_shear_cam_set_left_boundary_condition
f_pnt_flying_shear_cam_set_left_boundary_condition;
CSHARED_EXPORT extern pnt_flying_shear_cam_set_right_boundary_condition
f_pnt_flying_shear_cam_set_right_boundary_condition;
CSHARED_EXPORT extern pnt_flying_shear_cam_add_element
f_pnt_flying_shear_cam_add_element;
CSHARED_EXPORT extern pnt_flying_shear_cam_evaluate
f_pnt_flying_shear_cam_evaluate;

CSHARED_EXPORT extern pnt_thermoregulation_factors_get
f_pnt_thermoregulation_factors_get;
CSHARED_EXPORT extern pnt_thermoregulation_factors_set
f_pnt_thermoregulation_factors_set;

CSHARED_EXPORT extern pnt_motion_set_word_feedback_axis_simulation
f_pnt_motion_set_word_feedback_axis_simulation;

CSHARED_EXPORT extern pnt_shell_command_exec f_pnt_shell_command_exec;

CSHARED_EXPORT extern pnt_graphic_iso_editor_create
f_pnt_graphic_iso_editor_create;
CSHARED_EXPORT extern pnt_graphic_iso_editor_draw
f_pnt_graphic_iso_editor_draw;
CSHARED_EXPORT extern pnt_graphic_iso_editor_visualize_title
f_pnt_graphic_iso_editor_visualize_title;
CSHARED_EXPORT extern pnt_graphic_iso_editor_background_color
f_pnt_graphic_iso_editor_background_color;
CSHARED_EXPORT extern pnt_graphic_iso_editor_background_color
f_pnt_graphic_iso_editor_foreground_color;
CSHARED_EXPORT extern pnt_graphic_iso_editor_font
f_pnt_graphic_iso_editor_font;
CSHARED_EXPORT extern pnt_graphic_iso_editor_line_number_area_background_color
f_pnt_graphic_iso_editor_line_number_area_background_color;
CSHARED_EXPORT extern pnt_graphic_iso_editor_line_number_area_foreground_color
f_pnt_graphic_iso_editor_line_number_area_foreground_color;

// SQL database interface
CSHARED_EXPORT extern p_sql_set_timeout f_sql_set_timeout;
CSHARED_EXPORT extern p_sql_db_connect f_sql_db_connect;
CSHARED_EXPORT extern p_sql_db_disconnect f_sql_db_disconnect;
CSHARED_EXPORT extern p_sql_exec f_sql_exec;
CSHARED_EXPORT extern p_sql_query f_sql_query;
CSHARED_EXPORT extern p_sql_stored_procedure f_sql_stored_procedure;
CSHARED_EXPORT extern p_sql_next f_sql_next;
CSHARED_EXPORT extern p_sql_get_value f_sql_get_value;
CSHARED_EXPORT extern p_sql_get_value_type f_sql_get_value_type;
CSHARED_EXPORT extern p_sql_delete_query f_sql_delete_query;

CSHARED_EXPORT extern pnt_system_clock_read f_pnt_system_clock_read;

//Private functions
#define debug_print_string      f_pnt_debug_print_string    /* int debug_print_string(char *string) */
#define debug_print_int         f_pnt_debug_print_int       /* int debug_print_int(int n) */
#define debug_print_float       f_pnt_debug_print_float     /* int debug_print_float(float n) */

#define thread_usleep           f_pnt_thread_usleep

#define flying_shear_running                                f_pnt_flying_shear_running                          /* int flying_shear_running(int flying_shear_index); */
#define flying_shear_set_slave_periodic                     f_pnt_flying_shear_set_slave_periodic               /* int flying_shear_set_slave_periodic(int flying_shear_index, double slave_period_start_position, double slave_period, double master_period_start_position, double master_period) */
#define flying_shear_reset_slave_periodic                   f_pnt_flying_shear_reset_slave_periodic             /* int flying_shear_reset_slave_periodic(int flying_shear_index) */
#define flying_shear_is_slave_periodic                      f_pnt_flying_shear_is_slave_periodic                /* int flying_shear_is_slave_periodic(int flying_shear_index, unsigned char *is_periodic) */
#define flying_shear_set_slave_period_position              f_pnt_flying_shear_set_slave_period_position        /* int flying_shear_set_slave_period_position(int flying_shear_index, double position, double ramp_units_s2) */
#define flying_shear_reset_slave_period_position_correction f_pnt_flying_shear_reset_slave_period_position_correction   /* int flying_shear_reset_slave_period_position_correction(int flying_shear_index, double ramp_units_s2) */
#define flying_shear_add_slave_phase                        f_pnt_flying_shear_add_slave_phase                  /* int flying_shear_add_slave_phase(int flying_shear_index, double slave_phase_increment, double ramp_units_s2) */
#define flying_shear_reset_slave_phase                      f_pnt_flying_shear_reset_slave_phase                /* int flying_shear_reset_slave_phase(int flying_shear_index, double ramp_units_s2) */
#define flying_shear_set_current_slave_phase                f_pnt_flying_shear_set_current_slave_phase          /* int flying_shear_set_current_slave_phase(int flying_shear_index, double slave_phase, double ramp_units_s2) */
#define flying_shear_get_current_slave_phase                f_pnt_flying_shear_get_current_slave_phase          /* int flying_shear_get_current_slave_phase(int flying_shear_index, double *slave_phase) */
#define flying_shear_get_slave_value                        f_pnt_flying_shear_get_slave_value                  /* int flying_shear_get_slave_value(int flying_shear_index, double master_period_position, double *value); */
#define flying_shear_get_master_value                       f_pnt_flying_shear_get_master_value                 /* int flying_shear_get_master_value(int flying_shear_index, double slave_period_position, double *value); */
#define flying_shear_get_slave_value_from_absolute          f_pnt_flying_shear_get_slave_value_from_absolute    /* int flying_shear_get_slave_value_from_absolute(int flying_shear_index, double master_absolute_position, double *value); */
#define flying_shear_get_master_value_from_absolute         f_pnt_flying_shear_get_master_value_from_absolute   /* int flying_shear_get_master_value_from_absolute(int flying_shear_index, double slave_absolute_position, double *value); */
#define flying_shear_get_slave_period_position              f_pnt_flying_shear_get_slave_period_position        /* int flying_shear_get_slave_period_position(int flying_shear_index, double slave_absolute_position, double *value); */
#define flying_shear_slave_sync                             f_pnt_flying_shear_slave_sync                       /* int flying_shear_slave_sync(int flying_shear_index); */
#define flying_shear_slave_sync_evaluate                    f_pnt_flying_shear_slave_sync_evaluate              /* int flying_shear_slave_sync_evaluate(int flying_shear_index, double master_period_start_position, double master_period_stop_position); */
#define flying_shear_slave_sync_in_progress                 f_pnt_flying_shear_slave_sync_in_progress           /* int flying_shear_slave_sync_in_progress(int flying_shear_index); */
#define flying_shear_slave_stop_at                          f_pnt_flying_shear_slave_stop_at                    /* int flying_shear_slave_stop_at(int flying_shear_index); */
#define flying_shear_slave_stop_at_evaluate                 f_pnt_flying_shear_slave_stop_at_evaluate           /* int flying_shear_slave_stop_at_evaluate(int flying_shear_index, double master_period_start_position, double master_period_end_position); */
#define flying_shear_slave_stop_at_stopped                  f_pnt_flying_shear_slave_stop_at_stopped            /* int flying_shear_slave_stop_at_stopped(int flying_shear_index); */
#define flying_shear_slave_stop_at_in_progress              f_pnt_flying_shear_slave_stop_at_in_progress        /* int flying_shear_slave_stop_at_in_progress(int flying_shear_index); */

//Flying shear cam private functions
#define flying_shear_cam_get_element_size                   f_pnt_flying_shear_cam_get_element_size             /* int flying_shear_cam_get_element_size(void); */
#define flying_shear_cam_init                               f_pnt_flying_shear_cam_init                         /* int flying_shear_cam_init(int flying_shear_index, void *pnt_elements, unsigned int cam_max_elements); */
#define flying_shear_cam_reset                              f_pnt_flying_shear_cam_reset                        /* int flying_shear_cam_reset(int flying_shear_index); */
#define flying_shear_cam_set_left_boundary_condition        f_pnt_flying_shear_cam_set_left_boundary_condition  /* int flying_shear_cam_set_left_boundary_condition(int flying_shear_index, int type, double val); */
#define flying_shear_cam_set_right_boundary_condition       f_pnt_flying_shear_cam_set_right_boundary_condition /* int flying_shear_cam_set_right_boundary_condition(int flying_shear_index, int type, double val); */
#define flying_shear_cam_add_element                        f_pnt_flying_shear_cam_add_element                  /* int flying_shear_cam_add_element(int flying_shear_index, double master_position, double slave_position, int type); */
#define flying_shear_cam_evaluate                           f_pnt_flying_shear_cam_evaluate                     /* int flying_shear_cam_evaluate(int flying_shear_index); */

//Thermoregulation private function
#define thermoregulation_factors_get                        f_pnt_thermoregulation_factors_get                  /* int thermoregulation_factors_get(int profile_index, float *K, float *T, float *Ti, float *Td); */
#define thermoregulation_factors_set                        f_pnt_thermoregulation_factors_set                  /* int thermoregulation_factors_set(int profile_index, float K, float T, float Ti, float Td); */

//Setting of simulation of axis with word feedback
#define motion_set_word_feedback_axis_simulation            f_pnt_motion_set_word_feedback_axis_simulation      /* int motion_set_word_feedback_axis_simulation(int axis_index, unsigned char simulation_on); */

/* Shell command execution
 * std_output and std_error argument are dynamically allocated inside the function.
 * It is the duty of the calling application to free them when they are no longer needed.
 * "auth_code" parameter has to be given to the right value in order to let the
 * specified command be executed.
 */
#define shell_command_exec                                  f_pnt_shell_command_exec                            /* int shell_command_exec(char *cmd, unsigned int auth_code, char **std_output, char **std_error); */

#endif //LIB_C_SHARED_H
