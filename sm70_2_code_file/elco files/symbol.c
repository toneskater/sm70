/*
 * symbol.c
 *
 *  Created on: 30 ott 2019
 *      Author: Andrea
 */

#include "cshared.h"

/* Word intere non tamponate (es. Wnaaa) */
CSHARED_EXPORT unsigned short Wn[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word intere tamponate (es. Wmaaa) */
CSHARED_EXPORT unsigned short Wm[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word 'float' tamponate (es. WFmaaa) */
CSHARED_EXPORT int WFm[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word 'float' non tamponate (es. WFnaaa) */
CSHARED_EXPORT int WFn[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word 'float' di sistema (es. WFsaa) */
CSHARED_EXPORT int WFs[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Word intere di sistema (es. WSaa) */
CSHARED_EXPORT unsigned short Ws[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Flag non tamponate (es. Naa1) */
CSHARED_EXPORT PLC_BIT nom[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Flag tamponate (es. Maa1) */
CSHARED_EXPORT PLC_BIT mem[PLC_WORDS_ARRAY][PLC_WORDS_ARRAY];
/* Flag di sistema (es. Saa1) */
CSHARED_EXPORT PLC_BIT flg[PLC_WORDS_ARRAY];
