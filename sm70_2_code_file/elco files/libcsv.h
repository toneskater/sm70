#ifndef LIBCSV_H
#define LIBCSV_H

#ifdef __cplusplus
extern "C" {
#endif

#include "libcsv_global.h"

#define LIB_CSV_VERSION_MAJOR            3
#define LIB_CSV_VERSION_MINOR            0
#define LIB_CSV_VERSION_REVISION         1

#define ESTR_STRINGFY(x)                              #x
#define ESTR_TOSTRING(x)                 ESTR_STRINGFY(x)

#define LIB_CSV_VERSION                 ESTR_TOSTRING(LIB_CSV_VERSION_MAJOR) \
    "." ESTR_TOSTRING(LIB_CSV_VERSION_MINOR)  \
    "." ESTR_TOSTRING(LIB_CSV_VERSION_REVISION)

#define LIB_CSV_VERSION_CODE                  ((LIB_CSV_VERSION_MAJOR << 16) | \
                                               (LIB_CSV_VERSION_MINOR << 8) | \
                                               LIB_CSV_VERSION_REVISION)

#define LIB_CSV_VERSION_IS(maj,min)           ((maj == LIB_CSV_VERSION_MAJOR)  \
                                               && (min == LIB_CSV_VERSION_MINOR))

#define READ_ONLY   0x0001
#define WRITE_ONLY  0x0002
#define READ_WRITE  0x0004

CSV_EXPORT int csv_open(char *path, char separatore, int flags);
CSV_EXPORT int csv_creat(char *path, char separatore);
CSV_EXPORT int csv_close(int fd);
CSV_EXPORT int csv_row_count(int fd);
CSV_EXPORT int csv_column_count(int fd);
CSV_EXPORT int csv_read_item(int fd, unsigned int row, unsigned int column, char *item,
                             unsigned int max_item_size);
CSV_EXPORT int csv_write_item(int fd, unsigned int row, unsigned int column, char *item);
CSV_EXPORT int csv_item_size(int fd, unsigned int row, unsigned int column);

#ifdef __cplusplus
}
#endif

#endif //LIBCSV_H
