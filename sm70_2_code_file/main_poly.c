/*
 * main_poly.c
 *
 *  Created on: 7 nov 2019
 *      Author: Andrea
 */

extern void BUZM(void);
extern void client(void);
extern void mainScreen(void);
extern void memory_report(void);
extern void memory(void);
extern void operating_program(void);
extern void remote_srb(void);
extern void sampling_terminated(void);
extern void message_screen(void);
extern void logger(void);
extern void duct(void);
extern void environmental(void);
extern void eol(void);
extern void info(void);
extern void memory_erase_export_progress(void);
extern void pm10(void);
extern void remote_tsb(void);
extern void test(void);
extern void test_flow(void);
extern void test_leak(void);
extern void test_table_load(void);
extern void test_temperature(void);

extern void memory_low_priority(void);

void normalTask(void)
{
  BUZM();
  client();
  mainScreen();
  memory_report();
  memory();
  operating_program();
  remote_srb();
  sampling_terminated();
  message_screen();
  logger();
  duct();
  environmental();
  eol();
  info();
  memory_erase_export_progress();
  pm10();
  remote_tsb();
  test();
  test_flow();
  test_leak();
  test_table_load();
  test_temperature();
}

void lowPrioTask(void)
{
  memory_low_priority();
}

void fastTask(void)
{

}
