/*
* EepromIds.cs
*
*  AUTOGENERATED on: 08/06/2020 18:04:08
*      Author: Andrea Tonello
*/

using System.Collections.Generic;

namespace GrLifetekSimulator
{
    class EepromIds
    {
        public enum EEPM_ID_t
        {
            EEPM_BOOT = 0,
            EEPM_SW_VERSION_NUMBER,
            EEPM_HW_VERSION_NUMBER,
            EEPM_PRODUCT_SERIAL_NUMBER,
            EEPM_TOTAL_LITERS,
            SENC_BATTERY_POWER_OFF_TIME_SEC,
            MOTC_PUMP_SETPOINT_MIN,
            MOTC_PUMP_SETPOINT_MAX,
            MOTC_REGULATION_TIME_SEC,
            SENC_EXTERNAL_PRESSURE_MIN,
            SENC_EXTERNAL_PRESSURE_MAX,
            MOTC_INPULSE_PRESENCE_TIMER_SEC,
            MOTC_MIN_PWM_VALUE,
            SENC_METER_TEMPERATURE_OFFSET,
            SENC_EXTERNAL_TEMPERATURE_OFFSET,
            SENC_EXTERNAL_PRESSURE_OFFSET,
            CPUM_CPU_MAX_LOAD,
            SENC_EXTERNAL_PRESSURE_SENSOR_TYPE,
            SENC_METER_TEMPERATURE_SENSOR_TYPE,
            SENC_METER_TEMPERATURE_ADC_MV_MIN,
            SENC_METER_TEMPERATURE_ADC_MV_MAX,
            SENC_EXTERNAL_TEMPERATURE_SENSOR_TYPE,
            SENC_EXTERNAL_TEMPERATURE_ADC_MV_MIN,
            SENC_EXTERNAL_TEMPERATURE_ADC_MV_MAX,
            SENC_VACUUM_ADC_MV_MIN,
            SENC_VACUUM_ADC_MV_MAX,
            SENC_VACUUM_OFFSET,
            MOTC_PUMP_KEEP_ALIVE_TIME_SEC,
            MOTC_KAPPA_METER,
            SENC_VACUUM_SENSOR_MOUNTED,
            SENC_METER_TEMPERATURE_SENSOR_MOUNTED,
            SENC_EXTERNAL_TEMPERATURE_SENSOR_MOUNTED,
            SENC_EXTERNAL_PRESSURE_SENSOR_MOUNTED,
            MOTC_MOTOR_TYPE_MOUNTED,
            MOTC_TOTAL_METER_IMPULSE_HI,
            MOTC_TOTAL_METER_IMPULSE_LOW,
            MOTC_TOTAL_METER_IMPULSE_SAVE_TIME_SECONDS,
            MOTC_PID_P,
            MOTC_MAX_PWM_VALUE_OUTPUT_STEP_AUTO,
            MOTC_MAX_PWM_VALUE_OUTPUT_STEP_MANUAL,
            MOTC_STEP_FIRST_VALUE_LITERS,
            MOTC_STEP_FIRST_MULTIPLIER,
            MOTC_STEP_SECOND_VALUE_LITERS,
            MOTC_STEP_SECOND_MULTIPLIER,
            MOTC_MAX_PWM_VALUE,
            SENC_BATTERY_CRITICAL_POWER_OFF_THRESHOLD,
            SENC_BATTERY_CRITICAL_POWER_OFF_TIME_DELAY,
            MOTC_TOTAL_PUMP_IMPULSE_HI,
            MOTC_TOTAL_PUMP_IMPULSE_LOW,
            MOTC_TOTAL_PUMP_RUNNING_SECONDS,
            MOTC_TOTAL_PUMP_RUNNING_SECONDS_POWER_LEVEL_01,
            MOTC_TOTAL_PUMP_RUNNING_SECONDS_POWER_LEVEL_02,
            MOTC_TOTAL_PUMP_RUNNING_SECONDS_POWER_LEVEL_03,
            MOTC_TOTAL_PUMP_RUNNING_SECONDS_POWER_LEVEL_04,
            MOTC_TOTAL_PUMP_RUNNING_SECONDS_POWER_LEVEL_05,
            MOTC_TOTAL_PUMP_POWER_LEVEL_01_MAX,
            MOTC_TOTAL_PUMP_POWER_LEVEL_02_MAX,
            MOTC_TOTAL_PUMP_POWER_LEVEL_03_MAX,
            MOTC_TOTAL_PUMP_POWER_LEVEL_04_MAX,
            MOTC_TOTAL_PUMP_POWER_LEVEL_05_MAX,
            SYST_DATETIME_OF_LAST_CALIBRATION,
            SYST_DATETIME_OF_SECOND_LAST_CALIBRATION,
            SYST_DATETIME_OF_FIRST_CALIBRATION,
            SENC_VACUUM_CALIBRATION_PUMP_MAX_VOID_MMH2O,
            SENC_VACUUM_ZERO_MV,
            SENC_VACUUM_END_OF_SCALE_MV,
            SYST_SYSTEM_IN_CALIBRATION,
            MOTC_MAX_INVERTER_FREQUENCY_VALUE,
            MOTC_VOID_TEST_TIME_SEC,
            MOTC_PID_I,
            MOTC_PID_I_EPSILON,
            MOTC_FLOW_FILTERED_TIME_CALCULATION_SEC,
            MOTC_LONGER_METER_SPEED_CORRECTION_SEC,
            MOTC_LONGER_METER_SPEED_CORRECTION_FACTOR,
            MOTC_LONGER_METER_SPEED_CORRECTION_TIME_AUTO,
            SYST_DAYS_TO_NEXT_CALIBRATION,
            SYST_PROGRAM_ENVIRONMENTAL,
            SYST_PROGRAM_PM10,
            SYST_PROGRAM_DUCT,
            SYST_PROGRAM_TSB_REMOTE,
            SYST_PROGRAM_SRB_REMOTE,
            SENC_FILTER_TEMPERATURE_SENSOR_TYPE,
            SENC_FILTER_TEMPERATURE_ADC_MV_MIN,
            SENC_FILTER_TEMPERATURE_ADC_MV_MAX,
            SENC_FILTER_TEMPERATURE_OFFSET,
            SENC_FILTER_TEMPERATURE_SENSOR_MOUNTED,
            MOTC_PUMP_REGULATION_HYSTERESIS_PERCENTAGE,
            SYST_PRINTER_OPTION,
            EEPM_ID_MAX_NUMBER
        };

        static private uint eepm_eeprom_version = 1;

        static private uint eepm_boot_rom_default = 0xFFFFFFFF;
        static private byte[] eepm_sw_version_number_rom_default = {0x00, 0x00, 0x00, 0x01};
        static private byte[] eepm_hw_version_number_rom_default = {0x00, 0x00, 0x00, 0x01};
        static private byte[] eepm_product_serial_number_rom_default = {0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20};
        static private uint eepm_total_liters_rom_default = 0;
        static private ushort senc_battery_power_off_time_sec_rom_default = 300;
        static private ushort motc_pump_setpoint_min_rom_default = 25;
        static private ushort motc_pump_setpoint_max_rom_default = 6500;
        static private ushort motc_regulation_time_sec_rom_default = 300;
        static private ushort senc_external_pressure_min_rom_default = 5000;
        static private ushort senc_external_pressure_max_rom_default = 12000;
        static private ushort motc_inpulse_presence_timer_sec_rom_default = 150;
        static private ushort motc_min_pwm_value_rom_default = 120;
        static private short senc_meter_temperature_offset_rom_default = 0;
        static private short senc_external_temperature_offset_rom_default = 0;
        static private short senc_external_pressure_offset_rom_default = 0;
        static private ushort cpum_cpu_max_load_rom_default = 0;
        static private byte senc_external_pressure_sensor_type_rom_default = 0;
        static private byte senc_meter_temperature_sensor_type_rom_default = 0;
        static private ushort senc_meter_temperature_adc_mv_min_rom_default = 500;
        static private ushort senc_meter_temperature_adc_mv_max_rom_default = 2500;
        static private byte senc_external_temperature_sensor_type_rom_default = 0;
        static private ushort senc_external_temperature_adc_mv_min_rom_default = 500;
        static private ushort senc_external_temperature_adc_mv_max_rom_default = 2500;
        static private ushort senc_vacuum_adc_mv_min_rom_default = 500;
        static private ushort senc_vacuum_adc_mv_max_rom_default = 3250;
        static private short senc_vacuum_offset_rom_default = 0;
        static private ushort motc_pump_keep_alive_time_sec_rom_default = 30;
        static private uint motc_kappa_meter_rom_default = 14426;
        static private byte senc_vacuum_sensor_mounted_rom_default = 0;
        static private byte senc_meter_temperature_sensor_mounted_rom_default = 0;
        static private byte senc_external_temperature_sensor_mounted_rom_default = 0;
        static private byte senc_external_pressure_sensor_mounted_rom_default = 0;
        static private byte motc_motor_type_mounted_rom_default = 0;
        static private uint motc_total_meter_impulse_hi_rom_default = 0;
        static private uint motc_total_meter_impulse_low_rom_default = 0;
        static private ushort motc_total_meter_impulse_save_time_seconds_rom_default = 600;
        static private ushort motc_pid_p_rom_default = 100;
        static private ushort motc_max_pwm_value_output_step_auto_rom_default = 20;
        static private ushort motc_max_pwm_value_output_step_manual_rom_default = 50;
        static private byte motc_step_first_value_liters_rom_default = 5;
        static private byte motc_step_first_multiplier_rom_default = 2;
        static private byte motc_step_second_value_liters_rom_default = 10;
        static private byte motc_step_second_multiplier_rom_default = 3;
        static private ushort motc_max_pwm_value_rom_default = 4000;
        static private ushort senc_battery_critical_power_off_threshold_rom_default = 60;
        static private ushort senc_battery_critical_power_off_time_delay_rom_default = 5000;
        static private uint motc_total_pump_impulse_hi_rom_default = 0;
        static private uint motc_total_pump_impulse_low_rom_default = 0;
        static private uint motc_total_pump_running_seconds_rom_default = 0;
        static private uint motc_total_pump_running_seconds_power_level_01_rom_default = 0;
        static private uint motc_total_pump_running_seconds_power_level_02_rom_default = 0;
        static private uint motc_total_pump_running_seconds_power_level_03_rom_default = 0;
        static private uint motc_total_pump_running_seconds_power_level_04_rom_default = 0;
        static private uint motc_total_pump_running_seconds_power_level_05_rom_default = 0;
        static private byte motc_total_pump_power_level_01_max_rom_default = 20;
        static private byte motc_total_pump_power_level_02_max_rom_default = 40;
        static private byte motc_total_pump_power_level_03_max_rom_default = 60;
        static private byte motc_total_pump_power_level_04_max_rom_default = 80;
        static private byte motc_total_pump_power_level_05_max_rom_default = 100;
        static private byte[] syst_datetime_of_last_calibration_rom_default = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        static private byte[] syst_datetime_of_second_last_calibration_rom_default = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        static private byte[] syst_datetime_of_first_calibration_rom_default = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        static private ushort senc_vacuum_calibration_pump_max_void_mmh2o_rom_default = 0;
        static private ushort senc_vacuum_zero_mv_rom_default = 1000;
        static private ushort senc_vacuum_end_of_scale_mv_rom_default = 3000;
        static private byte syst_system_in_calibration_rom_default = 0;
        static private ushort motc_max_inverter_frequency_value_rom_default = 6000;
        static private ushort motc_void_test_time_sec_rom_default = 60;
        static private ushort motc_pid_i_rom_default = 10;
        static private ushort motc_pid_i_epsilon_rom_default = 10;
        static private byte motc_flow_filtered_time_calculation_sec_rom_default = 3;
        static private byte motc_longer_meter_speed_correction_sec_rom_default = 20;
        static private byte motc_longer_meter_speed_correction_factor_rom_default = 50;
        static private byte motc_longer_meter_speed_correction_time_auto_rom_default = 0;
        static private ushort syst_days_to_next_calibration_rom_default = 365;
        static private byte syst_program_environmental_rom_default = 0;
        static private byte syst_program_pm10_rom_default = 0;
        static private byte syst_program_duct_rom_default = 0;
        static private byte syst_program_tsb_remote_rom_default = 0;
        static private byte syst_program_srb_remote_rom_default = 0;
        static private byte senc_filter_temperature_sensor_type_rom_default = 0;
        static private ushort senc_filter_temperature_adc_mv_min_rom_default = 500;
        static private ushort senc_filter_temperature_adc_mv_max_rom_default = 2500;
        static private short senc_filter_temperature_offset_rom_default = 0;
        static private byte senc_filter_temperature_sensor_mounted_rom_default = 0;
        static private byte motc_pump_regulation_hysteresis_percentage_rom_default = 5;
        static private byte syst_printer_option_rom_default = 0;

        internal static void createEepromEntries(List<EepromEntry> eepromEntries)
        {
            eepromEntries.Add(new EepromEntry(eepm_boot_rom_default));
            eepromEntries.Add(new EepromEntry(eepm_sw_version_number_rom_default));
            eepromEntries.Add(new EepromEntry(eepm_hw_version_number_rom_default));
            eepromEntries.Add(new EepromEntry(eepm_product_serial_number_rom_default));
            eepromEntries.Add(new EepromEntry(eepm_total_liters_rom_default));
            eepromEntries.Add(new EepromEntry(senc_battery_power_off_time_sec_rom_default));
            eepromEntries.Add(new EepromEntry(motc_pump_setpoint_min_rom_default));
            eepromEntries.Add(new EepromEntry(motc_pump_setpoint_max_rom_default));
            eepromEntries.Add(new EepromEntry(motc_regulation_time_sec_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_pressure_min_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_pressure_max_rom_default));
            eepromEntries.Add(new EepromEntry(motc_inpulse_presence_timer_sec_rom_default));
            eepromEntries.Add(new EepromEntry(motc_min_pwm_value_rom_default));
            eepromEntries.Add(new EepromEntry(senc_meter_temperature_offset_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_temperature_offset_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_pressure_offset_rom_default));
            eepromEntries.Add(new EepromEntry(cpum_cpu_max_load_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_pressure_sensor_type_rom_default));
            eepromEntries.Add(new EepromEntry(senc_meter_temperature_sensor_type_rom_default));
            eepromEntries.Add(new EepromEntry(senc_meter_temperature_adc_mv_min_rom_default));
            eepromEntries.Add(new EepromEntry(senc_meter_temperature_adc_mv_max_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_temperature_sensor_type_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_temperature_adc_mv_min_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_temperature_adc_mv_max_rom_default));
            eepromEntries.Add(new EepromEntry(senc_vacuum_adc_mv_min_rom_default));
            eepromEntries.Add(new EepromEntry(senc_vacuum_adc_mv_max_rom_default));
            eepromEntries.Add(new EepromEntry(senc_vacuum_offset_rom_default));
            eepromEntries.Add(new EepromEntry(motc_pump_keep_alive_time_sec_rom_default));
            eepromEntries.Add(new EepromEntry(motc_kappa_meter_rom_default));
            eepromEntries.Add(new EepromEntry(senc_vacuum_sensor_mounted_rom_default));
            eepromEntries.Add(new EepromEntry(senc_meter_temperature_sensor_mounted_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_temperature_sensor_mounted_rom_default));
            eepromEntries.Add(new EepromEntry(senc_external_pressure_sensor_mounted_rom_default));
            eepromEntries.Add(new EepromEntry(motc_motor_type_mounted_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_meter_impulse_hi_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_meter_impulse_low_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_meter_impulse_save_time_seconds_rom_default));
            eepromEntries.Add(new EepromEntry(motc_pid_p_rom_default));
            eepromEntries.Add(new EepromEntry(motc_max_pwm_value_output_step_auto_rom_default));
            eepromEntries.Add(new EepromEntry(motc_max_pwm_value_output_step_manual_rom_default));
            eepromEntries.Add(new EepromEntry(motc_step_first_value_liters_rom_default));
            eepromEntries.Add(new EepromEntry(motc_step_first_multiplier_rom_default));
            eepromEntries.Add(new EepromEntry(motc_step_second_value_liters_rom_default));
            eepromEntries.Add(new EepromEntry(motc_step_second_multiplier_rom_default));
            eepromEntries.Add(new EepromEntry(motc_max_pwm_value_rom_default));
            eepromEntries.Add(new EepromEntry(senc_battery_critical_power_off_threshold_rom_default));
            eepromEntries.Add(new EepromEntry(senc_battery_critical_power_off_time_delay_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_impulse_hi_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_impulse_low_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_running_seconds_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_running_seconds_power_level_01_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_running_seconds_power_level_02_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_running_seconds_power_level_03_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_running_seconds_power_level_04_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_running_seconds_power_level_05_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_power_level_01_max_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_power_level_02_max_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_power_level_03_max_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_power_level_04_max_rom_default));
            eepromEntries.Add(new EepromEntry(motc_total_pump_power_level_05_max_rom_default));
            eepromEntries.Add(new EepromEntry(syst_datetime_of_last_calibration_rom_default));
            eepromEntries.Add(new EepromEntry(syst_datetime_of_second_last_calibration_rom_default));
            eepromEntries.Add(new EepromEntry(syst_datetime_of_first_calibration_rom_default));
            eepromEntries.Add(new EepromEntry(senc_vacuum_calibration_pump_max_void_mmh2o_rom_default));
            eepromEntries.Add(new EepromEntry(senc_vacuum_zero_mv_rom_default));
            eepromEntries.Add(new EepromEntry(senc_vacuum_end_of_scale_mv_rom_default));
            eepromEntries.Add(new EepromEntry(syst_system_in_calibration_rom_default));
            eepromEntries.Add(new EepromEntry(motc_max_inverter_frequency_value_rom_default));
            eepromEntries.Add(new EepromEntry(motc_void_test_time_sec_rom_default));
            eepromEntries.Add(new EepromEntry(motc_pid_i_rom_default));
            eepromEntries.Add(new EepromEntry(motc_pid_i_epsilon_rom_default));
            eepromEntries.Add(new EepromEntry(motc_flow_filtered_time_calculation_sec_rom_default));
            eepromEntries.Add(new EepromEntry(motc_longer_meter_speed_correction_sec_rom_default));
            eepromEntries.Add(new EepromEntry(motc_longer_meter_speed_correction_factor_rom_default));
            eepromEntries.Add(new EepromEntry(motc_longer_meter_speed_correction_time_auto_rom_default));
            eepromEntries.Add(new EepromEntry(syst_days_to_next_calibration_rom_default));
            eepromEntries.Add(new EepromEntry(syst_program_environmental_rom_default));
            eepromEntries.Add(new EepromEntry(syst_program_pm10_rom_default));
            eepromEntries.Add(new EepromEntry(syst_program_duct_rom_default));
            eepromEntries.Add(new EepromEntry(syst_program_tsb_remote_rom_default));
            eepromEntries.Add(new EepromEntry(syst_program_srb_remote_rom_default));
            eepromEntries.Add(new EepromEntry(senc_filter_temperature_sensor_type_rom_default));
            eepromEntries.Add(new EepromEntry(senc_filter_temperature_adc_mv_min_rom_default));
            eepromEntries.Add(new EepromEntry(senc_filter_temperature_adc_mv_max_rom_default));
            eepromEntries.Add(new EepromEntry(senc_filter_temperature_offset_rom_default));
            eepromEntries.Add(new EepromEntry(senc_filter_temperature_sensor_mounted_rom_default));
            eepromEntries.Add(new EepromEntry(motc_pump_regulation_hysteresis_percentage_rom_default));
            eepromEntries.Add(new EepromEntry(syst_printer_option_rom_default));
        }

        internal static uint getVersion()
        {
            return eepm_eeprom_version;
        }

    }
}
