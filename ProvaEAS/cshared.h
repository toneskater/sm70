/*
 * cshared.h
 *
 *  Created on: 30 ott 2019
 *      Author: Andrea
 */

#ifndef CSHARED_H_
#define CSHARED_H_

typedef enum
{
     LOCAL_FILE_SYSTEM        =0,
     USB_1_FILE_SYSTEM          =1,
     USB_2_FILE_SYSTEM      =2,
     USB_3_FILE_SYSTEM        =3,
     USB_4_FILE_SYSTEM        =4,
     NETWORK_FILE_SYSTEM      =5,
     USB_REAR_FILE_SYSTEM     =6,

} _FILE_PATH;

#define c_thread_object_stop 0

#endif /* CSHARED_H_ */
