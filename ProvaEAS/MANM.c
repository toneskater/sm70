/*
 * MANM.c
 *
 *  Created on: 16/10/2019
 *      Author: Andrea Tonello
 */
/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"
#include "util.h"
#include "libcsv.h"
#include "buzm.h"
#include <math.h>
#include "flag.h"

/* Private define ------------------------------------------------------------*/
#define MANM_DEMO

#define MANM_5_MINUTES_TO_MS            30000U /* 5 minutes in ms */ /* TODO NOW 30secs */
#define MMH2O_TO_PA_CONVERSION_NUMBER   9.80665f
#define MMH2O_TO_PA(x)                  ((float)(((float)(x)) * MMH2O_TO_PA_CONVERSION_NUMBER))
#define PA_TO_MMH2O(x)                  ((float)(((float)(x)) / MMH2O_TO_PA_CONVERSION_NUMBER))

#define MANM_THREAD_SLEEP_MS            50U

#define MANM_TEMP_BUFFER_SIZE           512U

#define MANM_CLIENT_NAME_LENTGH         40U
#define MANM_CLIENT_ADDRESS_LENTGH      40U
#define MANM_CLIENT_CAP_LENTGH          40U
#define MANM_CLIENT_CITY_LENTGH         40U
#define MANM_CLIENT_DISTRICT_LENTGH     40U
#define MANM_CLIENT_PLANT_NUMBER_LENTGH 40U
#define MANM_CLIENT_NOTE_LENTGH         200U
#define MANM_CLIENTS_MAX_NUMBER         5U

#define MANM_CSV_CLOSE_OK               1U

#define MANM_OOP_SCREEN_NAME_VISIBLE_TIMER_MS       1000U
#define MANM_OOP_SCREEN_NAME_NOT_VISIBLE_TIMER_MS   500U

#define MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_MANUAL_MIN_VALUE 0.30f

#define MANM_CLIENT_SCREEN_TOP_TEXT_VISIBLE_TIMER_MS        1000U
#define MANM_CLIENT_SCREEN_TOP_TEXT_NOT_VISIBLE_TIMER_MS    500U

#define MANM_BUTTON_SOUND() (BUZM_startBuzzerForMsAndFrequency(200U, BUZM_SOUND_DEFAULT_FREQUENCY))

#define MANM_OOP_REMOTESRB_SCREEN_CONNECT_TO_SRB_TIMEOUT_MS 1000U
#ifdef MANM_DEMO
#define MANM_OOP_REMOTESRB_SCREEN_TIMER_FIRST_DATA_DEMO_MS 1000U
#define MANM_OOP_REMOTESRB_SCREEN_TIMER_DATA_DEMO_MS        5000U
#endif 
/* report name length is only 34, 33 + 1 terminator */
#define MANM_REPORT_NAME_LENGTH 34U 

#define MANM_REPORT_FILE_NAME_EXTENTION "txt"
#define MANM_REPORT_TYPE_POSITION_IN_STRING_NAME 27U

#define MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER 5U

#define MANM_MEMORY_SCREEN_NO_REPORTS_TEXT_NOT_VISIBLE_TIMER_MS 500U
#define MANM_MEMORY_SCREEN_NO_REPORTS_TEXT_VISIBLE_TIMER_MS     1500U


#define MANM_REPORT_LOCATION_DEFAULT_SAVE   USB_1_FILE_SYSTEM /* TODO */
#define MANM_REPORT_LOCATION_DEFAULT_EXPORT USB_2_FILE_SYSTEM /* TODO */

#define MANM_CLIENT_LOCATION_DEFAULT_SAVE   USB_1_FILE_SYSTEM /* TODO */
#define MANM_CLIENT_LOCATION_DEFAULT_EXPORT USB_2_FILE_SYSTEM /* TODO */
/* Private typedef -----------------------------------------------------------*/
typedef enum MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_e
{
    MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_INIT = 0,
    MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_READY,
    MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_RUNNING,
    MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_PAUSED,
    MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_STOPPED,
    MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_MAX_NUMBER
}MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_t;

typedef enum MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_e
{
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_SELECT = 0,
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_2MM,
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_4MM,
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_6MM,
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_8MM,
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_10MM,
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_12MM,
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_14MM,
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_16MM,
}MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_t;

typedef enum MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_e
{
    MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_AUTOMATIC = 0,
    MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_MANUAL
}MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_t;

typedef enum MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_e
{
    MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_MMH20 = 0,
    MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_PA,
    MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_NO_UNIT,
    MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_MAX_NUMBER
}MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_t;

typedef enum MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_e
{
    MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_TIME = 0,
    MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_VOLUME
}MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_t;

typedef struct MANM_Client_s
{
    sbyte name[MANM_CLIENT_NAME_LENTGH + 1U];
    sbyte address[MANM_CLIENT_ADDRESS_LENTGH + 1U];
    sbyte CAP[MANM_CLIENT_CAP_LENTGH + 1U];
    sbyte city[MANM_CLIENT_CITY_LENTGH + 1U];
    sbyte district[MANM_CLIENT_DISTRICT_LENTGH + 1U];
    sbyte plantNumber[MANM_CLIENT_PLANT_NUMBER_LENTGH + 1U];
    sbyte note[MANM_CLIENT_NOTE_LENTGH + 1U];
}MANM_Client_t;

typedef struct MANM_OOPREMOTESRB_InterfaceData_s
{
    MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_t samplingType;
    ulong time;
    ulong volume;
    ulong normalizationTemperature;
    ulong litriAspeLineaDerivata;
    ulong temperaturaContatoreLineaDerivata;
    ulong samplingStartDelayMinutes;
    MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_t measureUnit;
    MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_t nozzleDiameter;
    MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_t isokinetiFlow;
    float isokinetiFlowManualValue;
}MANM_OOPREMOTESRB_InterfaceData_t;

typedef enum MANM_DUCT_SHAPE_e
{
    MANM_DUCT_SHAPE_CIRCLE = 0,
    MANM_DUCT_SHAPE_RECTANGULAR,
    MANM_DUCT_SHAPE_UNKNOWN,
    MANM_DUCT_SHAPE_MAX_NUMBER
}MANM_DUCT_SHAPE_t;

typedef enum MANM_SAMPLING_STOP_CAUSE_e
{
    MANM_SAMPLING_STOP_CAUSE_TIME = 0,
    MANM_SAMPLING_STOP_CAUSE_VOLUME,
    MANM_SAMPLING_STOP_CAUSE_MANUAL,
    MANM_SAMPLING_STOP_CAUSE_ERROR,
    MANM_SAMPLING_STOP_CAUSE_MAX_NUMBER
}MANM_SAMPLING_STOP_CAUSE_t;

typedef struct MANM_SRB_SetupMessage_s
{
    MANM_DUCT_SHAPE_t ductType;
    ulong ductDiameter;
    ulong maxSide;
    ulong minSide;
    ulong kappa;
    ulong density;
    ulong condensate;
    float barometric;
}MANM_SRB_SetupMessage_t;

typedef struct MANM_SRB_DataMessage_s
{
    ulong temp_tc;
    ulong delta_p;
    ulong p_static;
}MANM_SRB_DataMessage_t;

typedef enum MANM_CLIENT_SCREEN_TOP_TEXT_e
{
    MANM_CLIENT_SCREEN_TOP_TEXT_EMPTY = 0,
    MANM_CLIENT_SCREEN_TOP_TEXT_NO_CLIENT,
    MANM_CLIENT_SCREEN_TOP_TEXT_NEW_CLIENT,
    MANM_CLIENT_SCREEN_TOP_TEXT_EDIT_CLIENT,
    MANM_CLIENT_SCREEN_TOP_TEXT_MAX_NUMBER
}MANM_CLIENT_SCREEN_TOP_TEXT_t;

typedef enum MANM_SRB_STATE_e
{
    MANM_SRB_STATE_INIT = 0,
    MANM_SRB_STATE_CONNECTING_SRB,
    MANM_SRB_STATE_CONNECTED_READY,
    MANM_SRB_STATE_WAIT_START,
    MANM_SRB_STATE_RUNNING,
    MANM_SRB_STATE_PAUSED,
    MANM_SRB_STATE_STOPPED,
    MANM_SRB_STATE_ERROR,
    MANM_SRB_STATE_MAX_NUMBER
}MANM_SRB_STATE_t;

typedef struct MANM_SamplingTime_s
{
    ulong days;
    ulong hours;
    ulong minutes;
    ulong seconds;
    ubyte minus;
    ulong startDelaySeconds;
}MANM_SamplingTime_t;

typedef enum MANM_REPORT_NAME_CODE_e
{
    MANM_REPORT_NAME_CODE_ENVIRONMENTAL = 0, 
    MANM_REPORT_NAME_CODE_PM10,          
    MANM_REPORT_NAME_CODE_DUCT,           
    MANM_REPORT_NAME_CODE_REMOTE_TSB,   
    MANM_REPORT_NAME_CODE_REMOTE_SRB,  
    MANM_REPORT_NAME_CODE_MAX_NUMBER,
}MANM_REPORT_NAME_CODE_t;

typedef enum MANM_SAMPLING_ERROR_e
{
    MANM_SAMPLING_ERROR_NONE = 0,
    MANM_SAMPLING_ERROR_MAX_NUMBER
}MANM_SAMPLING_ERROR_t;

typedef struct MANM_SamplingSRBFinalData_s
{
    float lifeteckSampledVolume;
    ulong samplingTimeHours;
    ulong samplingTimeMinutes;
    ulong samplingTimeSeconds;
    float lifeteckSamplingFlowMedia;
    float temperatureMeterCounterMedia;
    float lifeteckNormalizedVolumeLiter;
    float ductTemperatureMedia;
    float staticPressureAbsolute;
    float speedMedia;
    float flowRateMedia;
    float flowRateNormalizedMedia;
    float volume_ld_t_cont;
    float volume_ld_tmedia_cont;
    float volume_tot_ld_plus_lifeteck;
    float volume_ld_t_norm;
    float volume_tot_t_norm;
    float isokineticLevel;
    MANM_SAMPLING_ERROR_t error;
    MANM_SAMPLING_STOP_CAUSE_t stopCause;
}MANM_SamplingSRBFinalData_t;

typedef enum MANM_MEMORY_SCREEN_FILTER_REPORT_e
{
	MANM_MEMORY_SCREEN_FILTER_REPORT_ALL = 0,
	MANM_MEMORY_SCREEN_FILTER_REPORT_ENVIRONMENTAL,
	MANM_MEMORY_SCREEN_FILTER_REPORT_PM10,
	MANM_MEMORY_SCREEN_FILTER_REPORT_DUCT,
	MANM_MEMORY_SCREEN_FILTER_REPORT_REMOTE_TSB,
	MANM_MEMORY_SCREEN_FILTER_REPORT_REMOTE_RSB,
	MANM_MEMORY_SCREEN_FILTER_REPORT_MAX_NUMBER
}MANM_MEMORY_SCREEN_FILTER_REPORT_t;

typedef enum MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_e
{
	MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_INIT = 0,
    MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_ERASING,
    MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_EXPORTING,
    MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_ERASE_COMPLETE,
    MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_EXPORT_COMPLETE,
    MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_MAX_NUMBER
}MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_t;

typedef enum MANM_MEMORY_OPERATION_e
{
    MANM_MEMORY_OPERATION_NO_OPERATION = 0,
	MANM_MEMORY_OPERATION_ERASE,
    MANM_MEMORY_OPERATION_EXPORT,
    MANM_MEMORY_OPERATION_SINGLE_REPORT_ERASED,
    MANM_MEMORY_OPERATION_SINGLE_REPORT_VIEW
}MANM_MEMORY_OPERATION_t;

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static MANM_Client_t MANM_Clients[MANM_CLIENTS_MAX_NUMBER];
static ubyte Manm_Clients_loaded = false;
static ubyte Manm_Clients_Total_Number = 0;
static ubyte Manm_Clients_SelectedOne = 0;
static const sbyte Manm_Clients_FileName[] = "Clients.csv";
static const sbyte Manm_Clients_FileHeader[] = "NAME;ADDRESS;CAP;CITY;DISTRICT;PLANT NUMBER;NOTE";
static ubyte Manm_Clients_PreviousSelectedOneForNewOperation = 0;
static ubyte Manm_Clients_NewOperationOnGoing = false;
static ubyte Manm_Clients_EditOperationOnGoing = false;
/*static int Manm_Clients_FileDescriptor;*/

static Timer_t Manm_ClientScreen_TopTextTimer;
static ubyte Manm_ClientScreen_clientNameBlinkState;

static Timer_t Manm_OopScreen_NameTimer;
static ubyte Manm_OopScreen_clientNameBlinkState;
/* OOP REMOTE SBR */
static MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_t Manm_OopRemoteSbrSampleState;
static Timer_t Manm_oopRemoteSrb_timerConnect;
#ifdef MANM_DEMO
static Timer_t Manm_oopRemoteSrb_timerDataDemo;
#endif 
static Timer_t Manm_oopRemoteSrb_timer5minutes;
static MANM_SRB_DataMessage_t Manm_oopRemoteSrbScreen_lastDataMessage;
static MANM_SRB_SetupMessage_t Manm_oopRemoteSrbScreen_lastSetupMessage;
static MANM_SamplingTime_t Manm_SamplingTime;
static MANM_SRB_STATE_t Manm_SRB_StateMachine;
static sbyte Manm_reportName[MANM_REPORT_NAME_LENGTH];
static _FILE_PATH Manm_reportLocationSave;
static _FILE_PATH Manm_reportLocationExport;
static _FILE_PATH Manm_clientLocationSave;
static _FILE_PATH Manm_clientLocationExport;

static const sbyte* MANM_report_name_codes[MANM_REPORT_NAME_CODE_MAX_NUMBER] = {"EN", "PM", "DC", "TS", "SR"};
static MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_t Manm_report_actualNozzleDiameter;
static MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_t Manm_report_lastNozzleDiameter;

static MANM_SAMPLING_STOP_CAUSE_t Manm_SamplingStopCause;
static MANM_SAMPLING_ERROR_t Manm_SamplingError;

typedef enum MANM_REPORT_TAG_e
{
    MANM_REPORT_TAG_DUMMY = 0,
    MANM_REPORT_TAG_CLIENT_ID,
    MANM_REPORT_TAG_INIT_DATA,
    MANM_REPORT_TAG_SAMPLING_START,
    MANM_REPORT_TAG_FIVE_MINUTES_DATA,
    
    MANM_REPORT_TAG_FINAL_DATA,
    MANM_REPORT_TAG_DELAYED_START,
    MANM_REPORT_TAG_START,
    MANM_REPORT_TAG_DELAYED_PAUSE,
    MANM_REPORT_TAG_RUNNING_PAUSE,
    
    MANM_REPORT_TAG_DELAYED_RESUME,
    MANM_REPORT_TAG_RUNNING_RESUMED,
    MANM_REPORT_TAG_NOZZLE_DIAMETER_CHANGED,
    
    MANM_REPORT_TAG_MAX_NUMBER
}MANM_REPORT_TAG_t;

static const sbyte* MANM_report_tags[MANM_REPORT_TAG_MAX_NUMBER] = 
{
    "XX", "X1", "12", "24", "14",
    "13", "X2", "X3", "X4", "X5",
    "X6", "X6", "X7"
};

static const sbyte* MANM_report_duct_shapes[MANM_DUCT_SHAPE_MAX_NUMBER] = {"CIRCLE", "RECTANGULAR", "UNKNOWN"};

static const sbyte* MANM_report_measure_unit[MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_MAX_NUMBER] = {"mmH2O", "pa", "no unit"};

static Timer_t Manm_SamplingTerminatedScreen_TimeInScreen;

static Timer_t Manm_MemoryEraseExportProgressScreen_TimeInScreen;
static Timer_t Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile;
static MANM_MEMORY_OPERATION_t Manm_MemoryOperation;

static Timer_t Manm_MemoryScreen_TimerNoReportsText;

static sbyte* Manm_MemoryReport_FileNameToOpen;
/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
/* INIT */
void Manm_InitVariables(void);
void Manm_InitVariablesMain(void);
void Manm_InitVariablesClient(void);
void Manm_InitVariablesOop(void);
void Manm_InitVariableOopRemoteSbr(void);
void Manm_InitVariableSamplingTerminated(void);
void Manm_InitVariableMemory(void);
void Manm_InitVariableMemoryReport(void);
void Manm_InitVariableMemoryEraseExportProgress(void);
/* BUTTONS */
void Manm_ManageButtons(void);
void Manm_ManageClientButtons(void);
void Manm_ManageOopButtons(void);
void Manm_ManageMainScreenButtons(void);
void Manm_ManageOopRemoteSRBButtons(void);
void Manm_ManageSamplingTerminatedButtons(void);
void Manm_ManageMemoryScreenButtons(void);
void Manm_ManageMemoryEraseExportProgressButtons(void);
void Manm_ManageMemoryReportButtons(void);

void Manm_ManageScreens(void);
void Manm_ManageSplashScreen(void);

void Manm_ManageMainScreen(void);

void Manm_ManageTestScreen(void);
void Manm_ManageTestFlowScreen(void);
void Manm_ManageTestLeakScreen(void);
void Manm_ManageTestTableLoadScreen(void);
void Manm_ManageTestTemperatureScreen(void);
/* CLIENT SCREEN */
void Manm_ManageClientScreen(void);
STD_RETURN_t Manm_Client_OpenAndReadOrCreateFile(sbyte* fileName, _FILE_PATH destPath, char* headerToWrite/*, int* csv_fd*/);
STD_RETURN_t Manm_Client_SaveFile(sbyte* fileName, _FILE_PATH destFileSystem);
void Manm_ManageNewButton(void);
void Manm_ManageClientScreenNextAndPreviousButton(void);
void Manm_Client_ClearClientFields(void);
STD_RETURN_t Manm_Client_ShowClientFields(ubyte clientNumber);
void Manm_ManageTopTextString(void);

void Manm_ManageMemoryScreen(void);
void Manm_ManageMemoryEraseExportProgressScreen(void);
void Manm_ManageMemoryReportScreen(void);


/* OOP SCREEN */
void Manm_ManageOopScreen(void);
void Manm_ManageOopEnvironmentalScreen(void);
void Manm_ManageOopPm10Screen(void);
void Manm_ManageOopDuctScreen(void);
void Manm_ManageOopRemoteTSBScreen(void);

void Manm_ManageOopRemoteSRBScreen(void);
void Manm_ManageOopRemoteSRBScreen_manageIsokineticSamplingLimits(void);
void Manm_OopRemoteSbr_PopulateFlowDataOnView(ubyte init);
STD_RETURN_t Manm_OopRemoteSbr_GetFlow(float* flow);
void Manm_OopRemoteSRBScreen_manageTableFlowSelection(void);
STD_RETURN_t Manm_OopRemoteSRBScreen_GetSamplingTime(MANM_SamplingTime_t* samplingTime);
void Manm_OopRemoteSRBScreen_manageSamplingTimeOnScreen(void);
STD_RETURN_t Manm_OopRemoteSRBScreen_GetDataFromView(MANM_OOPREMOTESRB_InterfaceData_t* interfaceData);

/* SAMPLING TERMINATED */
void Manm_ManageSamplingTerminatedScreen(void);

void Manm_ManageEolScreen(void);
void Manm_ManageInfoScreen(void);

/* MODEL */
void Manm_main_screen_model_init(void);
void Manm_main_screen_model(ulong execTimeMs);

void Manm_oop_screen_model_init(void);
void Manm_oop_screen_model(ulong execTimeMs);

void Manm_memory_screen_model_init(void);
void Manm_memory_screen_model(ulong execTimeMs);

void Manm_oopRemoteSrb_screen_model_init(void);
void Manm_oopRemoteSrb_screen_model(ulong execTimeMs);
void Manm_oopRemoteSrb_screen_model_manageSamplingTime(ubyte initFlag, ulong startDelaySeconds);
ulong Manm_oopRemoteSrb_screen_model_manageSamplingTimeGetStartDelayRemained(void);
sbyte* Manm_report_getTag(MANM_REPORT_TAG_t tag);
STD_RETURN_t Manm_report_createName(MANM_REPORT_NAME_CODE_t reportType);
STD_RETURN_t Manm_report_writeInstrumentIds(void);
STD_RETURN_t Manm_report_writeClientId(void); /* anagrafica*/
STD_RETURN_t Manm_report_writeInitData(void);
STD_RETURN_t Manm_report_writeDelayedStart(void);
STD_RETURN_t Manm_report_writeStart(void);
STD_RETURN_t Manm_report_writeDelayedPaused(void);
STD_RETURN_t Manm_report_writeFinalData(void);
STD_RETURN_t Manm_report_writeRunningPaused(void);
STD_RETURN_t Manm_report_write5MinutesData(void);
STD_RETURN_t Manm_report_writeDelayedResumed(void);
STD_RETURN_t Manm_report_writeRunningResumed(void);
STD_RETURN_t Manm_report_writeNozzleDiameterChanged(void);
STD_RETURN_t Manm_ReportGetStoCauseAndError(MANM_SAMPLING_STOP_CAUSE_t* stopCause, MANM_SAMPLING_ERROR_t* reportError);
STD_RETURN_t Manm_AreSamplingTerminationConditionReached(ubyte* reached, MANM_SAMPLING_STOP_CAUSE_t* stopCause, MANM_SAMPLING_ERROR_t* samplingError);

void Manm_samplingTerminated_screen_enterScreen(ubyte manualEnter);
void Manm_samplingTerminated_screen_model_init(void);
void Manm_samplingTerminated_screen_model(ulong execTimeMs);

void Manm_memory_erase_export_progress_screen_model_init(void);
void Manm_memory_erase_export_progress_screen_model(ulong execTimeMs);

void Manm_memory_report_screen_model_init(void);
void Manm_memory_report_screen_model(ulong execTimeMs);

/* Private functions ---------------------------------------------------------*/
void Manm_InitVariables(void)
{
    Manm_InitVariablesMain();
    Manm_InitVariablesClient();
    Manm_InitVariablesOop();
    Manm_InitVariableOopRemoteSbr();
    Manm_InitVariableSamplingTerminated();
    Manm_InitVariableMemory();
    Manm_InitVariableMemoryEraseExportProgress();
    Manm_InitVariableMemoryReport();
    
    Manm_reportLocationSave = MANM_REPORT_LOCATION_DEFAULT_SAVE;
    Manm_reportLocationExport = MANM_REPORT_LOCATION_DEFAULT_EXPORT;
}

void Manm_InitVariablesMain(void)
{
    mainScreen_eolButton_bitVisible = true;
}

void Manm_InitVariablesClient(void)
{
    ulong i;
    for(i = 0; i < MANM_CLIENTS_MAX_NUMBER; i++)
    {
        memory_set(&MANM_Clients[0], 0x00U, sizeof(MANM_Clients)); 
        /*        
        MANM_Clients[i].address
        MANM_Clients[i].CAP
        MANM_Clients[i].city
        MANM_Clients[i].district
        MANM_Clients[i].plantNumber
        MANM_Clients[i].note
        */
    }
    Manm_Clients_loaded = false;
    Manm_Clients_Total_Number = 0;
    Manm_Clients_SelectedOne = 0;
    /*Manm_Clients_FileDescriptor = -1;*/
    Manm_Clients_PreviousSelectedOneForNewOperation = 0;
    Manm_Clients_NewOperationOnGoing = false;
    Manm_Clients_EditOperationOnGoing = false;
    clientScreen_topText = MANM_CLIENT_SCREEN_TOP_TEXT_EMPTY;

    Manm_ClientScreen_clientNameBlinkState = false;
    UTIL_TimerInit(&Manm_ClientScreen_TopTextTimer);
    
    Manm_clientLocationSave = MANM_CLIENT_LOCATION_DEFAULT_SAVE;
    Manm_clientLocationExport = MANM_CLIENT_LOCATION_DEFAULT_EXPORT;
}

void Manm_InitVariablesOop(void)
{
    Manm_OopScreen_clientNameBlinkState = false;
    UTIL_TimerInit(&Manm_OopScreen_NameTimer);
}

void Manm_InitVariableOopRemoteSbr(void)
{
    Manm_OopRemoteSbrSampleState = MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_INIT;
    Manm_SamplingTime.days = 0;
    Manm_SamplingTime.hours = 0;
    Manm_SamplingTime.minutes = 0;
    Manm_SamplingTime.seconds = 0;
    Manm_SamplingTime.minus = false;
    Manm_SamplingTime.startDelaySeconds = 0;
    

}

void Manm_InitVariableSamplingTerminated(void)
{
    samplingTerminatedScreen_message_1 = '\0';
}

void Manm_InitVariableMemory(void)
{
    UTIL_TimerInit(&Manm_MemoryScreen_TimerNoReportsText);
}

void Manm_InitVariableMemoryEraseExportProgress(void)
{
    Manm_MemoryOperation = MANM_MEMORY_OPERATION_NO_OPERATION;
}

void Manm_InitVariableMemoryReport(void)
{
    Manm_MemoryReport_FileNameToOpen = NULL;
}

void Manm_ManageButtons(void)
{
    /* Manage the button released flag from GUI, to manage commands */
    Manm_ManageMainScreenButtons(); 
    Manm_ManageClientButtons();
    Manm_ManageOopButtons();
    Manm_ManageOopRemoteSRBButtons();
    Manm_ManageSamplingTerminatedButtons();
    Manm_ManageMemoryScreenButtons();
    Manm_ManageMemoryEraseExportProgressButtons();
    Manm_ManageMemoryReportButtons();
}

/* **********************************/
void Manm_ManageScreens(void)
{
    Manm_ManageSplashScreen();
    Manm_ManageMainScreen();
    Manm_ManageTestScreen();
    Manm_ManageTestFlowScreen();
    Manm_ManageTestLeakScreen();
    Manm_ManageTestTableLoadScreen();
    Manm_ManageTestTemperatureScreen();
    Manm_ManageClientScreen();
    Manm_ManageMemoryScreen();
    Manm_ManageMemoryReportScreen();
    Manm_ManageOopScreen();
    Manm_ManageOopEnvironmentalScreen();
    Manm_ManageOopPm10Screen();
    Manm_ManageOopDuctScreen();
    Manm_ManageOopRemoteTSBScreen();
    Manm_ManageOopRemoteSRBScreen();
    Manm_ManageEolScreen();
    Manm_ManageInfoScreen();
    Manm_ManageSamplingTerminatedScreen();
    Manm_ManageMemoryEraseExportProgressScreen();
}

/* **********************************/
void Manm_ManageSplashScreen(void)
{

}

/* **********************************/
void Manm_ManageMainScreenButtons(void)
{
    if(mainScreen_testButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MAIN_SCREEN_BUTTON_TEST);
    }
    if(mainScreen_clientButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MAIN_SCREEN_BUTTON_CLIENT);
    }
    if(mainScreen_memoryButton_released != false) /* These flag is reset by GUI */
    { 
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MAIN_SCREEN_BUTTON_MEMORY);
    }
    if(mainScreen_oppButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MAIN_SCREEN_BUTTON_OOP);
    }
    if(mainScreen_infoButton_released != false) /* These flag is reset by GUI */
    { 
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MAIN_SCREEN_BUTTON_INFO);
    }
    if(mainScreen_eolButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MAIN_SCREEN_BUTTON_EOL);
    }
}

void Manm_ManageMainScreen(void)
{
    
}

/* **********************************/
void Manm_ManageTestScreen(void)
{

}

/* **********************************/
void Manm_ManageTestFlowScreen(void)
{

}

/* **********************************/
void Manm_ManageTestLeakScreen(void)
{

}

/* **********************************/
void Manm_ManageTestTableLoadScreen(void)
{

}

/* **********************************/
void Manm_ManageTestTemperatureScreen(void)
{

}

/* **********************************/
void Manm_ManageClientButtons(void)
{
    if(clientScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_BACK);
    }
    if(clientScreen_previousButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_PREVIOUS);
    }
    if(clientScreen_nextButton_released != false) /* These flag is reset by GUI */
    { 
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_NEXT);
    }
    if(clientScreen_deleteButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_DELETE);
    }
    if(clientScreen_saveButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_SAVE);
    }
    if(clientScreen_editButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_EDIT); 
    }
    if(clientScreen_newButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_NEW);
    }
    if(clientScreen_removeFileButton_released != false)
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_REMOVE_FILE);
    }
    if(clientScreen_copyFileButton_released != false)
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_COPY_FILE);
    }
}

void Manm_ManageClientScreen(void)
{
    ubyte isRunning;
    ubyte isExpired;
    
    (void) UTIL_TimerIncrement(&Manm_ClientScreen_TopTextTimer, Execution_Normal);
    
    (void) UTIL_TimerIsExpired(&Manm_ClientScreen_TopTextTimer, &isExpired);
    if(isExpired != false)
    {
        (void) UTIL_TimerStop(&Manm_ClientScreen_TopTextTimer);
        if(Manm_ClientScreen_clientNameBlinkState != false)
        {
            Manm_ClientScreen_clientNameBlinkState = false;
            /* Now set not visible */
            clientScreen_topText = MANM_CLIENT_SCREEN_TOP_TEXT_EMPTY;
            (void) UTIL_TimerStart(&Manm_ClientScreen_TopTextTimer, MANM_CLIENT_SCREEN_TOP_TEXT_NOT_VISIBLE_TIMER_MS);
        }
        else
        {
            Manm_ClientScreen_clientNameBlinkState = true;
            /* Now set visible */
            clientScreen_topText = MANM_CLIENT_SCREEN_TOP_TEXT_NO_CLIENT;
            (void) UTIL_TimerStart(&Manm_ClientScreen_TopTextTimer, MANM_CLIENT_SCREEN_TOP_TEXT_VISIBLE_TIMER_MS);
        }
    }
    
    if(clientScreen_page1_entered != false)
    {
        clientScreen_page1_entered = false;
        /* On enter screen */
        Manm_Client_ClearClientFields();
        Manm_InitVariablesClient();
        clientScreen_topText = MANM_CLIENT_SCREEN_TOP_TEXT_EMPTY;
        /* Check clients file exists */
        FLAG_Set(FLAG_COMMAND_OPEN_CLIENT_FILE); /* Send the command to open the client file in the thread */
        clientScreen_enableClientFields = false;
        (void) UTIL_TimerIsRunning(&Manm_ClientScreen_TopTextTimer, &isRunning);
        if(isRunning != false)
        {
            /* Already Started, stop */
            (void) UTIL_TimerStop(&Manm_ClientScreen_TopTextTimer);
        }
    }
    
    clientScreen_totalClientNumber = Manm_Clients_Total_Number;
    if(Manm_Clients_Total_Number > 0)
    {
        clientScreen_selectedClient = Manm_Clients_SelectedOne + 1;
    }
    else
    {
        clientScreen_selectedClient = 0;
    }
    
    /* *********************************************************************/
    if(FLAG_Get(FLAG_EVENT_CLIENT_FILE_OPENED) != FALSE)
    {
        debug_print("FLAG_EVENT_CLIENT_FILE_OPENED");
    }
    if(FLAG_Get(FLAG_EVENT_CLIENT_FILE_SAVED) != FALSE)
    {
        debug_print("FLAG_EVENT_CLIENT_FILE_SAVED");
    }
    if(
        (FLAG_GetAndReset(FLAG_EVENT_CLIENT_FILE_OPENED) != FALSE) ||
        (FLAG_GetAndReset(FLAG_EVENT_CLIENT_FILE_SAVED) != FALSE)
    )
    {        
        clientScreen_totalClientNumber = Manm_Clients_Total_Number;
        
        Manm_Client_ClearClientFields();
        
        if(Manm_Clients_Total_Number > 0)
        {
            Manm_Clients_SelectedOne = clientScreen_selectedClientRetention;
            Manm_Client_ShowClientFields(Manm_Clients_SelectedOne);
        }
        
        /* Only when the file is opened and hopefully loaded, check */
        if(Manm_Clients_Total_Number > 0U)
        {
            /* There are clients */
            /* delete button enabled */
            clientScreen_deleteButton_enabled = true;
            /* save button disabled */
            clientScreen_saveButton_enabled = false;
            /* edit button enabled */
            clientScreen_editButton_enabled = true;
        }
        else
        {
            /* delete button disable */
            clientScreen_deleteButton_enabled = false;
            /* save button disabled */
            clientScreen_saveButton_enabled = false;
            /* edit button disabled */
            clientScreen_editButton_enabled = false;
        } 
        
        Manm_ManageTopTextString();
        Manm_ManageNewButton();
        Manm_ManageClientScreenNextAndPreviousButton();
    }
    
    /* Once the file is loaded manage the buttons */
    if(Manm_Clients_loaded != false)
    {
        /* previus button disabled */
        
    }
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_NEXT) != FALSE)
    {
        debug_print("FLAG_CLIENT_SCREEN_BUTTON_NEXT");
        if(Manm_Clients_SelectedOne < (Manm_Clients_Total_Number - 1U))
        {
            Manm_Clients_SelectedOne++;
            clientScreen_selectedClientRetention = Manm_Clients_SelectedOne;
            Manm_Client_ClearClientFields();
            Manm_Client_ShowClientFields(Manm_Clients_SelectedOne);
        }
        
        Manm_ManageClientScreenNextAndPreviousButton();
    }   
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_PREVIOUS) != FALSE)
    {
        debug_print("FLAG_CLIENT_SCREEN_BUTTON_PREVIOUS");
        if(Manm_Clients_SelectedOne > 0U)
        {
            Manm_Clients_SelectedOne--;
            clientScreen_selectedClientRetention = Manm_Clients_SelectedOne;
            Manm_Client_ClearClientFields();
            Manm_Client_ShowClientFields(Manm_Clients_SelectedOne);
        }
        
        Manm_ManageClientScreenNextAndPreviousButton();
    }
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_NEW) != FALSE)
    {
        debug_print("FLAG_CLIENT_SCREEN_BUTTON_NEW");
        
        (void) UTIL_TimerIsRunning(&Manm_ClientScreen_TopTextTimer, &isRunning);
        if(isRunning != false)
        {
            /* Already Started, stop */
            (void) UTIL_TimerStop(&Manm_ClientScreen_TopTextTimer);
        }
        
        /* enable save button */
        clientScreen_saveButton_enabled = true;
        /* enable clients fields */
        clientScreen_enableClientFields = true;
        /* new button disabled, i'm adding a new clients  */
        clientScreen_newButton_enabled = false;
        /* block previus and next button */
        clientScreen_previousButton_enabled = false;
        clientScreen_nextButton_enabled = false;
        /* clear client fields in order to write down new stuff */
        Manm_Client_ClearClientFields();
        /* In case I don't finalize with save operation */
        Manm_Clients_PreviousSelectedOneForNewOperation = Manm_Clients_SelectedOne;
        Manm_Clients_NewOperationOnGoing = true;
        
        Manm_Clients_SelectedOne = Manm_Clients_Total_Number;
        clientScreen_selectedClientRetention = Manm_Clients_SelectedOne;
        
        Manm_Clients_Total_Number++;
        
        /* enable also the delete button in order to delete before saving */
        clientScreen_deleteButton_enabled = true;
        
        clientScreen_topText = MANM_CLIENT_SCREEN_TOP_TEXT_NEW_CLIENT;
    }
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_DELETE) != FALSE)
    {
        if(Manm_Clients_EditOperationOnGoing != false)
        {
            debug_print("FLAG_CLIENT_SCREEN_BUTTON_DELETE EDIT ONGOING");
            
            Manm_Clients_EditOperationOnGoing = false;

            /* Re-show the non edit fields */
            Manm_Client_ShowClientFields(Manm_Clients_SelectedOne);
        }
        else
        {
            if(Manm_Clients_NewOperationOnGoing != false)
            {
                debug_print("FLAG_CLIENT_SCREEN_BUTTON_DELETE NEW ONGOING");
                /* nothing finalized, show the last shown client */
                Manm_Clients_Total_Number--;
                Manm_Clients_SelectedOne = Manm_Clients_PreviousSelectedOneForNewOperation;
                clientScreen_selectedClientRetention = Manm_Clients_SelectedOne;
                
                if(Manm_Clients_Total_Number > 0)
                {
                    clientScreen_deleteButton_enabled = true;
                }
                else
                {
                    clientScreen_deleteButton_enabled = false;
                }
                
                Manm_Clients_NewOperationOnGoing = false;
                
                Manm_Client_ShowClientFields(Manm_Clients_SelectedOne);
            }
            else
            {
                debug_print("FLAG_CLIENT_SCREEN_BUTTON_DELETE");
                /* Delete from array and then save the file */
                int i = 0;
                int traslated = false;
                
                /* Set client entry as total empy writing 0 everywhere */
                memory_set((ubyte*)&MANM_Clients[Manm_Clients_SelectedOne], 0x00U, sizeof(MANM_Client_t));
                /* if there are clients after, traslate the next clients */
                for(i = Manm_Clients_SelectedOne; i < (Manm_Clients_Total_Number - 1U); i++)
                {
                    memory_copy((ubyte*)&MANM_Clients[i], (ubyte*)&MANM_Clients[i + 1U], sizeof(MANM_Client_t));
                    traslated = true;
                }
                /* If I have moved the clients, remove the latest one */
                if(traslated != false)
                {
                    memory_set((ubyte*)&MANM_Clients[i], 0x00U, sizeof(MANM_Client_t));
                }
                
                /* Manage the selectedOne */
                if(Manm_Clients_SelectedOne == (Manm_Clients_Total_Number - 1U))
                {
                    /* If i had selected the last one */
                    if(Manm_Clients_Total_Number > 1U)
                    {
                        /* And there were at least two clients */
                        Manm_Clients_SelectedOne--;
                    }
                }
                clientScreen_selectedClientRetention = Manm_Clients_SelectedOne;
                
                Manm_Clients_Total_Number--;
                
                FLAG_Set(FLAG_COMMAND_SAVE_CLIENT_FILE);
            }
        }
        /* disable clients fields */
        clientScreen_enableClientFields = false;
        /* disable save button */
        clientScreen_saveButton_enabled = false;
        
        Manm_ManageTopTextString();
        Manm_ManageNewButton();
        Manm_ManageClientScreenNextAndPreviousButton();
    }
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_EDIT) != FALSE)
    {
        debug_print("FLAG_CLIENT_SCREEN_BUTTON_EDIT");
        /* enable save button */
        clientScreen_saveButton_enabled = true;
        /* enable clients fields */
        clientScreen_enableClientFields = true;
        /* new button disabled, i'm adding a new clients  */
        clientScreen_newButton_enabled = false;
        /* block previus and next button */
        clientScreen_previousButton_enabled = false;
        clientScreen_nextButton_enabled = false;
        
        Manm_Clients_EditOperationOnGoing = true;
        
        clientScreen_topText = MANM_CLIENT_SCREEN_TOP_TEXT_EDIT_CLIENT;
    }
    

    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_SAVE) != FALSE)
    {        
        if(
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_name_1       , MANM_CLIENT_NAME_LENTGH           ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_address_1    , MANM_CLIENT_ADDRESS_LENTGH        ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_CAP_1        , MANM_CLIENT_CAP_LENTGH            ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_city_1       , MANM_CLIENT_CITY_LENTGH           ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_district_1   , MANM_CLIENT_DISTRICT_LENTGH       ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_plantNumber_1, MANM_CLIENT_PLANT_NUMBER_LENTGH   ) > 0U) ||
            (UTIL_GetBufferStringOfUshortLength((ushort*)&clientScreen_note_1       , MANM_CLIENT_NOTE_LENTGH           ) > 0U)
        )
        {
            debug_print("FLAG_CLIENT_SCREEN_BUTTON_SAVE");
            /* If at least one of the fields are written, save */
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_name_1         , MANM_CLIENT_NAME_LENTGH           , &MANM_Clients[Manm_Clients_SelectedOne].name[0]          , MANM_CLIENT_NAME_LENTGH);      
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_address_1      , MANM_CLIENT_ADDRESS_LENTGH        , &MANM_Clients[Manm_Clients_SelectedOne].address[0]       , MANM_CLIENT_ADDRESS_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_CAP_1          , MANM_CLIENT_CAP_LENTGH            , &MANM_Clients[Manm_Clients_SelectedOne].CAP[0]           , MANM_CLIENT_CAP_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_city_1         , MANM_CLIENT_CITY_LENTGH           , &MANM_Clients[Manm_Clients_SelectedOne].city[0]          , MANM_CLIENT_CITY_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_district_1     , MANM_CLIENT_DISTRICT_LENTGH       , &MANM_Clients[Manm_Clients_SelectedOne].district[0]      , MANM_CLIENT_DISTRICT_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_plantNumber_1  , MANM_CLIENT_PLANT_NUMBER_LENTGH   , &MANM_Clients[Manm_Clients_SelectedOne].plantNumber[0]   , MANM_CLIENT_PLANT_NUMBER_LENTGH);
            (void)UTIL_BufferFromUShortToSByte(&clientScreen_note_1         , MANM_CLIENT_NOTE_LENTGH           , &MANM_Clients[Manm_Clients_SelectedOne].note[0]          , MANM_CLIENT_NOTE_LENTGH);
         
            FLAG_Set(FLAG_COMMAND_SAVE_CLIENT_FILE);
        
            /* disable save button */
            clientScreen_saveButton_enabled = false;
            /* disable clients fields */
            clientScreen_enableClientFields = false;
            /* new button disabled, i'm adding a new clients  */
            clientScreen_newButton_enabled = true;
            
            Manm_Clients_EditOperationOnGoing = false;
            Manm_Clients_NewOperationOnGoing = false;
        }
        else
        {
            /* No fields written discard */
            debug_print("FLAG_CLIENT_SCREEN_BUTTON_SAVE No fields written discard");
            /* Manage making the discard */
            
            /* disable clients fields */
            clientScreen_enableClientFields = false;
            clientScreen_deleteButton_enabled = false;
            
            if(
                (Manm_Clients_NewOperationOnGoing != false) || 
                (Manm_Clients_EditOperationOnGoing != false)
            )
            {
                /* also if i'm editing and all the fields during editing are set to empy, discard. Maybe I could delete the entry */
                FLAG_Set(FLAG_CLIENT_SCREEN_BUTTON_DELETE);
            }
        }
    }
    
    if(clientScreen_page1_exit != false)
    {
        clientScreen_page1_exit = false;
        /* On exit screen */
        Manm_Client_ClearClientFields();
        
        /* Same code of delete button when new operation is on goind */
        Manm_Clients_Total_Number--;
        if(Manm_Clients_NewOperationOnGoing != false)
        {
            Manm_Clients_SelectedOne = Manm_Clients_PreviousSelectedOneForNewOperation;
            clientScreen_selectedClientRetention = Manm_Clients_SelectedOne;
            Manm_Clients_NewOperationOnGoing = false;
        }
        if(Manm_Clients_EditOperationOnGoing != false)
        {
            Manm_Clients_EditOperationOnGoing = false;
        }
        clientScreen_enableClientFields = false;
        clientScreen_topText = MANM_CLIENT_SCREEN_TOP_TEXT_EMPTY;
        (void) UTIL_TimerStop(&Manm_ClientScreen_TopTextTimer);
    }
}

STD_RETURN_t Manm_Client_ShowClientFields(ubyte clientNumber)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((clientNumber >= 0) && (clientNumber < MANM_CLIENTS_MAX_NUMBER))
    {
        (void)UTIL_BufferFromSbyteToUShort(&MANM_Clients[clientNumber].name[0]          , MANM_CLIENT_NAME_LENTGH           , &clientScreen_name_1         , MANM_CLIENT_NAME_LENTGH);      
        (void)UTIL_BufferFromSbyteToUShort(&MANM_Clients[clientNumber].address[0]       , MANM_CLIENT_ADDRESS_LENTGH        , &clientScreen_address_1      , MANM_CLIENT_ADDRESS_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&MANM_Clients[clientNumber].CAP[0]           , MANM_CLIENT_CAP_LENTGH            , &clientScreen_CAP_1          , MANM_CLIENT_CAP_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&MANM_Clients[clientNumber].city[0]          , MANM_CLIENT_CITY_LENTGH           , &clientScreen_city_1         , MANM_CLIENT_CITY_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&MANM_Clients[clientNumber].district[0]      , MANM_CLIENT_DISTRICT_LENTGH       , &clientScreen_district_1     , MANM_CLIENT_DISTRICT_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&MANM_Clients[clientNumber].plantNumber[0]   , MANM_CLIENT_PLANT_NUMBER_LENTGH   , &clientScreen_plantNumber_1  , MANM_CLIENT_PLANT_NUMBER_LENTGH);
        (void)UTIL_BufferFromSbyteToUShort(&MANM_Clients[clientNumber].note[0]          , MANM_CLIENT_NOTE_LENTGH           , &clientScreen_note_1         , MANM_CLIENT_NOTE_LENTGH);
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

void Manm_ManageTopTextString(void)
{
    (void) UTIL_TimerStop(&Manm_ClientScreen_TopTextTimer);
    if(Manm_Clients_Total_Number > 0U)
    {
        clientScreen_topText = MANM_CLIENT_SCREEN_TOP_TEXT_EMPTY;
    }
    else
    {
        clientScreen_topText = MANM_CLIENT_SCREEN_TOP_TEXT_NO_CLIENT;
        (void) UTIL_TimerStart(&Manm_ClientScreen_TopTextTimer, MANM_CLIENT_SCREEN_TOP_TEXT_VISIBLE_TIMER_MS);
    }  
}

void Manm_ManageNewButton(void)
{
    if(Manm_Clients_Total_Number < MANM_CLIENTS_MAX_NUMBER)
    {
        /* new button enabled */
        clientScreen_newButton_enabled = true;
    }
    else
    {
         /* new button disabled */
        clientScreen_newButton_enabled = false;
    } 
}

void Manm_ManageClientScreenNextAndPreviousButton(void)
{
    if(Manm_Clients_Total_Number == 0)
    {
        /* No clients */
        clientScreen_previousButton_enabled = false;
        clientScreen_nextButton_enabled = false;
    }
    else
    {
        if(Manm_Clients_SelectedOne == 0)
        {
            clientScreen_previousButton_enabled = false; 
        }
        else
        {
            clientScreen_previousButton_enabled = true;
        }

        if(Manm_Clients_SelectedOne == (Manm_Clients_Total_Number - 1U))
        {
            clientScreen_nextButton_enabled = false;
        }
        else
        {
            clientScreen_nextButton_enabled = true;
        }
    }
}

void Manm_Client_ClearClientFields(void)
{
    /* clear the clients fields */
    clientScreen_name_1 = '\0';
    clientScreen_address_1 = '\0';
    clientScreen_CAP_1 = '\0';
    clientScreen_city_1 = '\0';
    clientScreen_district_1 = '\0';
    clientScreen_plantNumber_1 = '\0';
    clientScreen_note_1 = '\0';
} 

STD_RETURN_t Manm_Client_SaveFile(sbyte* fileName, _FILE_PATH destFileSystem)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int csv_fd;
    int rows;
    int i;
    /* Check parameters */
    if(
        (fileName != NULL) && 
        ( 
            (destFileSystem == LOCAL_FILE_SYSTEM) ||
            (destFileSystem == USB_1_FILE_SYSTEM) ||
            (destFileSystem == USB_2_FILE_SYSTEM) ||
            (destFileSystem == USB_3_FILE_SYSTEM) ||
            (destFileSystem == USB_4_FILE_SYSTEM)
        )
    )
    {
        int dim;
        char* str_path ;
        char* item;
        
        dim = file_get_path_size(destFileSystem);
        if(dim < 0U)
        {
            returnValue = STD_RETURN_IMPOSSIBLE_TO_GET_FILE_PATH;
        }
        else
        {
            /* Dimension file to open. */
            dim += string_length(fileName);
            str_path = memory_alloc(dim + 1U);
            /* Create the final csv path to open */
            if (file_get_path(str_path, (dim+1), destFileSystem) < 0U) 
            {
                returnValue = STD_RETURN_IMPOSSIBLE_TO_CREATE_FILE_PATH;
            }
            else
            {
                string_concatenate(str_path , fileName);
                /* Open the csv file */
                csv_fd = csv_open(str_path,';', READ_WRITE);
                if (csv_fd < 0) 
                {
                    returnValue = STD_RETURN_IMPOSSIBLE_TO_OPEN_FILE_FOR_SAVE;
                }
                else
                {
                    /* Write the content of the array as is */
                    for(i = 0; i < MANM_CLIENTS_MAX_NUMBER; i++) /* Rows contains also the header */
                    {
                        csv_write_item(csv_fd, (i + 1U), 0U, &MANM_Clients[i].name[0]);
                        csv_write_item(csv_fd, (i + 1U), 1U, &MANM_Clients[i].address[0]);
                        csv_write_item(csv_fd, (i + 1U), 2U, &MANM_Clients[i].CAP[0]);
                        csv_write_item(csv_fd, (i + 1U), 3U, &MANM_Clients[i].city[0]);
                        csv_write_item(csv_fd, (i + 1U), 4U, &MANM_Clients[i].district[0]);
                        csv_write_item(csv_fd, (i + 1U), 5U, &MANM_Clients[i].plantNumber[0]);
                        csv_write_item(csv_fd, (i + 1U), 6U, &MANM_Clients[i].note[0]);   
                    }
                    
                    if(csv_close(csv_fd) != MANM_CSV_CLOSE_OK)
                    {
                        returnValue = STD_RETURN_IMPOSSIBLE_TO_CLOSE_FILE_AFTER_SAVE;
                    }
                    else
                    {
                        returnValue = STD_RETURN_OK;
                    }
                }
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t Manm_Client_OpenAndReadOrCreateFile(sbyte* fileName, _FILE_PATH destFileSystem, char* headerToWrite/*, int* csv_fd*/)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int csv_fd;
    int rows;
    /* Check parameters */
    if(
        (fileName != NULL) && 
        ( 
            (destFileSystem == LOCAL_FILE_SYSTEM) ||
            (destFileSystem == USB_1_FILE_SYSTEM) ||
            (destFileSystem == USB_2_FILE_SYSTEM) ||
            (destFileSystem == USB_3_FILE_SYSTEM) ||
            (destFileSystem == USB_4_FILE_SYSTEM)
        ) &&
        (headerToWrite != NULL)
    )
    {
        int dim;
        char* str_path ;
        char* item;
        
        dim = file_get_path_size(destFileSystem);
        if(dim < 0U)
        {
            returnValue = STD_RETURN_IMPOSSIBLE_TO_GET_FILE_PATH;
        }
        else
        {
            /* Dimension file to open. */
            dim += string_length(fileName);
            str_path = memory_alloc(dim + 1U);
            /* Create the final csv path to open */
            if (file_get_path(str_path, (dim+1), destFileSystem) < 0U) 
            {
                returnValue = STD_RETURN_IMPOSSIBLE_TO_CREATE_FILE_PATH;
            }
            else
            {
                string_concatenate(str_path , fileName);
                /* Open the csv file */
                csv_fd = csv_open(str_path,';', READ_WRITE);
                if (csv_fd < 0) 
                {
                    /* debug_print("impossible to open file %s - %d", str_path, Test_csv_fd); */
                    csv_fd = csv_creat(str_path, ';');
                    if (csv_fd < 0) 
                    {
                        /*debug_print("impossible to create file %s - %d", str_path, Test_csv_fd);*/
                        returnValue = STD_RETURN_IMPOSSIBLE_TO_CREATE_FILE;
                    }
                    else
                    {
                        /* File created, write header and also the cells empty for each possible client */
                        int endI = 0;
                        int startI = 0;
                        int headerWritten = 0;
                        ubyte finish = false;
                        do
                        {
                            /* Search next separator */
                            debug_print("endI %d %c startI %d %c", endI, headerToWrite[endI], startI, headerToWrite[startI]);
                            if((headerToWrite[endI] == ';') || (headerToWrite[endI] == '\0'))
                            {
                                if(headerToWrite[endI] == '\0')
                                {
                                    finish = true;
                                }
                                if(startI < endI)
                                {
                                    /* something to write */
                                    item = memory_alloc((endI - startI) + 1U);
                                    debug_print("allocated: %d", (endI - startI) + 1U);
                                    int j;
                                    /* copy the content */
                                    for(j = 0; j < (endI - startI); j++)
                                    {
                                        item[j] = headerToWrite[startI + j];
                                        debug_print("copy: %c", headerToWrite[startI + j]);
                                    }
                                    item[j] = '\0';
                                    debug_print("to write: %s", item);
                                    csv_write_item(csv_fd, 0, headerWritten, item);
                                    
                                    memory_free(item);
                                    
                                    headerWritten++;
                                    
                                    endI++;
                                    
                                    startI = endI; 
                                    
                                    debug_print("headerWritten: %d", headerWritten);
                                }
                                else
                                {
                                    /* nothing between se*/
                                    endI++;
                                    startI++;
                                }
                            }
                            else
                            {
                                endI++;
                            }
                        }while(finish == false);

                        /* write down also the cell for all the clients, but empty */
                        sbyte valueToWrite[1] = {'\0'};
                        int j, k;
                        
                        for(j = 0; j < MANM_CLIENTS_MAX_NUMBER; j++)
                        {
                            for(k = 0; k < headerWritten; k++)
                            {
                                csv_write_item(csv_fd, (j + 1U), k, &valueToWrite[0]);
                            }
                        }

                        Manm_Clients_loaded = true;
                        Manm_Clients_Total_Number = 0;
                        Manm_Clients_SelectedOne = 0;
                        
                        if(csv_close(csv_fd) != MANM_CSV_CLOSE_OK)
                        {
                            returnValue = STD_RETURN_IMPOSSIBLE_TO_CLOSE_FILE_AFTER_CREATION;
                        }
                        else
                        {
                            returnValue = STD_RETURN_OK;
                        }
                    } /* End of file creation */
                }
                else
                {
                    /* Read the content and load the clients array */
                    rows = csv_row_count(csv_fd);
                    debug_print("Clients file row %d", rows);
                    int i = 0;
                    int sizeItem;
                    int availableClient = 0;
                    for(i = 0; (i < (rows - 1)) && (i < MANM_CLIENTS_MAX_NUMBER); i++) /* Rows contains also the header */
                    {
                        if(
                            (csv_item_size(csv_fd, (i + 1), 0U) > 0) || 
                            (csv_item_size(csv_fd, (i + 1), 1U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 2U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 3U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 4U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 5U) > 0) ||
                            (csv_item_size(csv_fd, (i + 1), 6U) > 0)
                        )
                        {
                            /* In a row there is something written, it is a valide client */
                            debug_print("Load client entry %d", i);
                            availableClient++;
                            csv_read_item(csv_fd, (i + 1U), 0U, &MANM_Clients[i].name[0], MANM_CLIENT_NAME_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 1U, &MANM_Clients[i].address[0], MANM_CLIENT_ADDRESS_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 2U, &MANM_Clients[i].CAP[0], MANM_CLIENT_CAP_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 3U, &MANM_Clients[i].city[0], MANM_CLIENT_CITY_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 4U, &MANM_Clients[i].district[0], MANM_CLIENT_DISTRICT_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 5U, &MANM_Clients[i].plantNumber[0], MANM_CLIENT_PLANT_NUMBER_LENTGH);
                            csv_read_item(csv_fd, (i + 1U), 6U, &MANM_Clients[i].note[0], MANM_CLIENT_NOTE_LENTGH);   
                        }
                        else
                        {
                            debug_print("Entry %d empty", i);
                        }
                    }
                    debug_print("Total clients %d", availableClient);
                    Manm_Clients_Total_Number = availableClient;       
                    Manm_Clients_loaded = true;
                    Manm_Clients_SelectedOne = 0;
                    
                    if(csv_close(csv_fd) != MANM_CSV_CLOSE_OK)
                    {
                        returnValue = STD_RETURN_IMPOSSIBLE_TO_CLOSE_FILE_AFTER_READ;
                    }
                    else
                    {
                        returnValue = STD_RETURN_OK;
                    }
                }
            }
            
            memory_free(str_path);
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

/* **********************************/
void Manm_ManageMemoryScreenButtons(void)
{
    if(memoryScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_BACK);
    }
    if(memoryScreen_eraseAllButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_ERASE_ALL);
    }
    if(memoryScreen_exportAllButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_EXPORT_ALL);
    }
    if(memoryScreen_fileName_00_button_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_00);
    }
    if(memoryScreen_fileName_01_button_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_01);
    }
    if(memoryScreen_fileName_02_button_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_02);
    }
    if(memoryScreen_fileName_03_button_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_03);
    }
    if(memoryScreen_fileName_04_button_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_04);
    }
    if(memoryScreen_filterReport_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_COMBOBOX_FILTER_REPORT);
    }
    if(memoryScreen_previousButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_PREVIOUS);
    }
    if(memoryScreen_nextButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_SCREEN_BUTTON_NEXT);
    }
}

void Manm_MemoryScreen_FileNameSetForeGround(ulong id, ushort color)
{
    switch(id)
    {
        case 0U:
            memoryScreen_fileName_00_foregroundColor = color;
            break;
        case 1U:
            memoryScreen_fileName_01_foregroundColor = color;
            break;
        case 2U:
            memoryScreen_fileName_02_foregroundColor = color;
            break;
        case 3U:
            memoryScreen_fileName_03_foregroundColor = color;
            break;
        case 4U:
            memoryScreen_fileName_04_foregroundColor = color;
            break;
        default:
            break;
    }
}

void Manm_MemoryScreen_FileNameSetButtonVisible(ulong id, ubyte visible)
{
    switch(id)
    {
        case 0U:
            memoryScreen_fileName_00_button_visible = visible;
            break;
        case 1U:
            memoryScreen_fileName_01_button_visible = visible;
            break;
        case 2U:
            memoryScreen_fileName_02_button_visible = visible;
            break;
        case 3U:
            memoryScreen_fileName_03_button_visible = visible;
            break;
        case 4U:
            memoryScreen_fileName_04_button_visible = visible;
            break;
        default:
            break;
    }
}

void Manm_MemoryScreen_FileNameClearText(ulong id)
{
    switch(id)
    {
        case 0U:
            memoryScreen_fileName_00_text_1 = '\0';
            break;
        case 1U:
            memoryScreen_fileName_01_text_1 = '\0';
            break;
        case 2U:
            memoryScreen_fileName_02_text_1 = '\0';
            break;
        case 3U:
            memoryScreen_fileName_03_text_1 = '\0';
            break;
        case 4U:
            memoryScreen_fileName_04_text_1 = '\0';
            break;
        default:
            break;
    }
}

void Manm_MemoryScreen_FileNameSetText(ulong id, sbyte* text, ulong textLength)
{
    if(text != NULL)
    {
        /* debug_print("TEXT %d=%s", textLength, text); */
        switch(id)
        {
            case 0U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_00_text_1, 40U);
                break;
            case 1U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_01_text_1, 40U);
                break;
            case 2U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_02_text_1, 40U);
                break;
            case 3U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_03_text_1, 40U);
                break;
            case 4U:
                UTIL_BufferFromSbyteToUShort(text, textLength, &memoryScreen_fileName_04_text_1, 40U);
                break;
            default:
                break;
        }
    }
}

slong Manm_MemoryScreen_FileListId; /* KEEP IT SIGNED */
#define MANM_MEMORY_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE 20U
static ulong Manm_MemoryScreen_firstPageActive;
static sbyte Manm_MemoryScreen_pageActivePageNumber[MANM_MEMORY_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE] = {0x00U};

void Manm_MemoryScreenDrawData(void)
{
    ulong listSize;
    ulong i;
    ulong lastPageActive;
    
    /* File list created write on display */
    if(Manm_MemoryScreen_FileListId >= 0)
    {
        /* File list exists write on screen */
        listSize = list_size(Manm_MemoryScreen_FileListId);
        
        if(listSize > 0U)
        {
            memoryScreen_eraseAllButton_enabled = true;
            memoryScreen_exportAllButton_enabled = true;

            memoryScreen_NoReportsText_visible = false;
            (void) UTIL_TimerStop(&Manm_MemoryScreen_TimerNoReportsText);
            (void) UTIL_TimerInit(&Manm_MemoryScreen_TimerNoReportsText);
        }
        else
        {
            memoryScreen_eraseAllButton_enabled = false;
            memoryScreen_exportAllButton_enabled = false;
            
            memoryScreen_NoReportsText_visible = true;
            /* Now set visible */
            (void) UTIL_TimerStart(&Manm_MemoryScreen_TimerNoReportsText, MANM_MEMORY_SCREEN_NO_REPORTS_TEXT_VISIBLE_TIMER_MS);
        }
        
        if(listSize > 0U)
        {
            if(listSize > (Manm_MemoryScreen_firstPageActive + (MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER - 1U)))
            {
                lastPageActive = Manm_MemoryScreen_firstPageActive + (MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER - 1U);
            }
            else
            {
                lastPageActive = listSize;
            }
            
            string_sprintf(&Manm_MemoryScreen_pageActivePageNumber[0], "%03d - %03d / %03d", Manm_MemoryScreen_firstPageActive, lastPageActive, listSize);
        }
        else
        {
            string_sprintf(&Manm_MemoryScreen_pageActivePageNumber[0], "--- - --- / ---");
        }
        UTIL_BufferFromSbyteToUShort(
                                    &Manm_MemoryScreen_pageActivePageNumber[0],
                                    MANM_MEMORY_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE,
                                    &memoryScreen_pageActivePageNumber_1,
                                    MANM_MEMORY_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE
        );
        
        /* manage enable of previous and next button */
        if(listSize > 0U)
        {
            if(Manm_MemoryScreen_firstPageActive > MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER)
            {
                memoryScreen_previousButton_enabled = true;
            }
            else
            {
                memoryScreen_previousButton_enabled = false;
            }
            
            if(listSize > (Manm_MemoryScreen_firstPageActive + (MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER - 1U)))
            {
                memoryScreen_nextButton_enabled = true;
            }
            else
            {
                memoryScreen_nextButton_enabled = false;
            }
        }
        else
        {
            memoryScreen_previousButton_enabled = false;
            memoryScreen_nextButton_enabled = false;
        }
        
        i = 0;
        if(listSize > 0U)
        {
            for(; ((i + (Manm_MemoryScreen_firstPageActive - 1U)) < listSize) && (i < MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER); i++)
            {
                sbyte* str = list_item_at(Manm_MemoryScreen_FileListId, (i + (Manm_MemoryScreen_firstPageActive - 1U)));
                /* debug_print("str len: %d", string_length(str)); */
                Manm_MemoryScreen_FileNameSetText(i, str, string_length(str));
                Manm_MemoryScreen_FileNameSetForeGround(i, BLACK);
                Manm_MemoryScreen_FileNameSetButtonVisible(i, true);
            }
        }

        /* in case i < MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER, set to invisible the other buttons */
        for(; i < MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER; i++)
        {
            Manm_MemoryScreen_FileNameClearText(i);
            Manm_MemoryScreen_FileNameSetForeGround(i, BLACK);
            Manm_MemoryScreen_FileNameSetButtonVisible(i, false);
        }
    }
    else
    {
        /* ERROR???? */
        debug_print("List not created but called the event? What the fuck?");
    }
}

void Manm_MemoryScreen_OpenMemoryReport(ulong buttonId)
{
    if(buttonId < MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER)
    {
        if(Manm_MemoryScreen_FileListId >= 0)
        {
            Manm_MemoryReport_FileNameToOpen = list_item_at(Manm_MemoryScreen_FileListId, (Manm_MemoryScreen_firstPageActive - 1U) + buttonId);
            debug_print("Apertura file %s", Manm_MemoryReport_FileNameToOpen);
            memoryReportScreen_enterCommand = true;
        }
        else
        {
            debug_print("La lista non esiste");
        }
    }
    else
    {
        debug_print("button id inesistente");
    }
}    

void Manm_ManageMemoryScreen(void)
{
    ulong i;
    ubyte isRunning;
    ubyte isExpired;
    
    (void) UTIL_TimerIncrement(&Manm_MemoryScreen_TimerNoReportsText, Execution_Normal);
    
    (void) UTIL_TimerIsExpired(&Manm_MemoryScreen_TimerNoReportsText, &isExpired);
    if(isExpired != false)
    {
        (void) UTIL_TimerStop(&Manm_MemoryScreen_TimerNoReportsText);
        (void) UTIL_TimerInit(&Manm_MemoryScreen_TimerNoReportsText);
        if(memoryScreen_NoReportsText_visible != false)
        {
            memoryScreen_NoReportsText_visible = false;
            (void) UTIL_TimerStart(&Manm_MemoryScreen_TimerNoReportsText, MANM_MEMORY_SCREEN_NO_REPORTS_TEXT_NOT_VISIBLE_TIMER_MS);
        }
        else
        {
            memoryScreen_NoReportsText_visible = true;
            /* Now set visible */
            (void) UTIL_TimerStart(&Manm_MemoryScreen_TimerNoReportsText, MANM_MEMORY_SCREEN_NO_REPORTS_TEXT_VISIBLE_TIMER_MS);
        }
    }
    
    if(memoryScreen_page1_entered != false)
    {
        memoryScreen_page1_entered = false;
        /* Code */
        memoryScreen_NoReportsText_visible = false;
        
        switch(Manm_MemoryOperation)
        {
            case MANM_MEMORY_OPERATION_NO_OPERATION: /* entering from the main menu */
            case MANM_MEMORY_OPERATION_ERASE:
            case MANM_MEMORY_OPERATION_SINGLE_REPORT_ERASED:
                memoryScreen_filterReport = MANM_MEMORY_SCREEN_FILTER_REPORT_ALL;
                memoryScreen_eraseAllButton_enabled = false;
                memoryScreen_exportAllButton_enabled = false;
                for(i = 0; i < MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER; i++)
                {
                    Manm_MemoryScreen_FileNameClearText(i);
                    Manm_MemoryScreen_FileNameSetForeGround(i, BLACK);
                    Manm_MemoryScreen_FileNameSetButtonVisible(i, false);
                }
                
                string_sprintf(&Manm_MemoryScreen_pageActivePageNumber[0], "--- - --- / ---");
                UTIL_BufferFromSbyteToUShort(
                                            &Manm_MemoryScreen_pageActivePageNumber[0],
                                            MANM_MEMORY_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE,
                                            &memoryScreen_pageActivePageNumber_1,
                                            MANM_MEMORY_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE
                );
                
                memoryScreen_previousButton_enabled = false;
                memoryScreen_nextButton_enabled = false;
                
                FLAG_Set(FLAG_MEMORY_SCREEN_COMMAND_CLEAR_FILE_LIST);
                break;
            case MANM_MEMORY_OPERATION_EXPORT:
                Manm_MemoryScreenDrawData();
                break;
            case MANM_MEMORY_OPERATION_SINGLE_REPORT_VIEW:
                Manm_MemoryScreenDrawData();
                break;
            default:
                break;
        }
        Manm_MemoryOperation = MANM_MEMORY_OPERATION_NO_OPERATION;
        
        FLAG_Set(FLAG_MEMORY_SCREEN_COMMAND_CREATE_FILE_LIST);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_00) != false)
    {
        Manm_MemoryScreen_OpenMemoryReport(0U);
    }
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_01) != false)
    {
        Manm_MemoryScreen_OpenMemoryReport(1U);
    }
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_02) != false)
    {
        Manm_MemoryScreen_OpenMemoryReport(2U);
    }
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_03) != false)
    {
        Manm_MemoryScreen_OpenMemoryReport(3U);
    }
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_FILENAME_04) != false)
    {
        Manm_MemoryScreen_OpenMemoryReport(4U);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_COMBOBOX_FILTER_REPORT) != false)
    {
        FLAG_Set(FLAG_MEMORY_SCREEN_COMMAND_CREATE_FILE_LIST);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_EVENT_FILE_LIST_CREATED) != false)
    {
        Manm_MemoryScreen_firstPageActive = 1;
        
        Manm_MemoryScreenDrawData();
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_NEXT) != false)
    {
        Manm_MemoryScreen_firstPageActive += MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER;
        
        Manm_MemoryScreenDrawData();
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_PREVIOUS) != false)
    {
        Manm_MemoryScreen_firstPageActive -= MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER;
        
        Manm_MemoryScreenDrawData();
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_EXPORT_ALL) != false)
    {
        /* The button change automatically to the memoryEraseExportProgressScreen */
        Manm_MemoryOperation = MANM_MEMORY_OPERATION_EXPORT;
        memoryEraseExport_enterCommand = true;
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_ERASE_ALL) != false)
    {
        /* The button change automatically to the memoryEraseExportProgressScreen */
        Manm_MemoryOperation = MANM_MEMORY_OPERATION_ERASE;
        memoryEraseExport_enterCommand = true;
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_BUTTON_BACK) != false)
    {
        FLAG_Set(FLAG_MEMORY_SCREEN_COMMAND_CLEAR_FILE_LIST);
    }
    
    if(memoryScreen_page1_exit != false)
    {
        memoryScreen_page1_exit = false;
        
        if(Manm_MemoryOperation != MANM_MEMORY_OPERATION_EXPORT)
        {
            memoryScreen_filterReport = MANM_MEMORY_SCREEN_FILTER_REPORT_ALL;
            
            string_sprintf(&Manm_MemoryScreen_pageActivePageNumber[0], "--- - --- / ---");
            UTIL_BufferFromSbyteToUShort(
                                        &Manm_MemoryScreen_pageActivePageNumber[0],
                                        MANM_MEMORY_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE,
                                        &memoryScreen_pageActivePageNumber_1,
                                        MANM_MEMORY_SCREEN_PAGE_ACTIVE_PAGE_NUMBER_STRING_SIZE
            );
        }
        
        memoryScreen_eraseAllButton_enabled = false;
        memoryScreen_exportAllButton_enabled = false;
        for(i = 0; i < MANM_MEMORY_SCREEN_FILE_NAME_MAX_NUMBER; i++)
        {
            Manm_MemoryScreen_FileNameClearText(i);
            Manm_MemoryScreen_FileNameSetForeGround(i, BLACK);
            Manm_MemoryScreen_FileNameSetButtonVisible(i, false);
        }
        
        (void) UTIL_TimerStop(&Manm_MemoryScreen_TimerNoReportsText);
        (void) UTIL_TimerInit(&Manm_MemoryScreen_TimerNoReportsText);
    }
}

/* **********************************/
void Manm_ManageMemoryEraseExportProgressButtons(void)
{
    if(memoryEraseExportProgressScreen_continueButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_BUTTON_CONTINUE);
    }
}

STD_RETURN_t Manm_ManageMemoryEraseExportProgressScreen_SetProgressBar(float percentage)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((percentage >= 0.0f) && (percentage <= 100.0f))
    {
        memoryEraseExportProgressScreen_progressBar_MaximumValue = FLOAT_TO_WORD(100);
        memoryEraseExportProgressScreen_progressBar_textAndWord = FLOAT_TO_WORD(percentage);
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t Manm_ManageMemoryEraseExportProgressScreen_SetMessage(sbyte* str, MANM_MEMORY_OPERATION_t op, ulong actualFile, ulong fileNumber)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    sbyte bufferTemp[100U] = {0x00U};
    if(str != NULL)
    {
        switch(op)
        {
            case MANM_MEMORY_OPERATION_ERASE:
                string_sprintf(&bufferTemp[0], "Erasing file\n%3d / %3d\n%s", actualFile, fileNumber, str);
                break;
            case MANM_MEMORY_OPERATION_EXPORT:
                string_sprintf(&bufferTemp[0], "Exporting file\n%3d / %3d\n%s", actualFile, fileNumber, str);
                break;
            default:
                break;
        }
        
        (void)UTIL_BufferFromSbyteToUShort(
                                    &bufferTemp[0],
                                    100U,
                                    &memoryEraseExportProgressScreen_message_1,
                                    100U
        );
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }

    return returnValue;
}

void Manm_ManageMemoryEraseExportProgressScreen(void)
{
    if(memoryEraseExportProgressScreen_page1_entered != false)
    {
        memoryEraseExportProgressScreen_page1_entered = false;
        memoryEraseExportProgressScreen_message_1 = '\0';
        memoryEraseExportProgressScreen_continueButton_enabled = false;
        switch(Manm_MemoryOperation)
        {
            case MANM_MEMORY_OPERATION_ERASE:
                memoryEraseExportProgressScreen_operation = MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_ERASING;
                /* Start the erase command to the model */
                FLAG_Set(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_COMMAND_ERASE_ALL);
                break;
            case MANM_MEMORY_OPERATION_EXPORT:
                memoryEraseExportProgressScreen_operation = MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_EXPORTING;
                /* Start the export command to the model */
                FLAG_Set(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_COMMAND_EXPORT_ALL);
                break;
        }
    }
    
    /* Exported completed */
    if(FLAG_GetAndReset(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_EVENT_EXPORT_ALL) != false)
    {
        memoryEraseExportProgressScreen_operation = MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_EXPORT_COMPLETE;
        memoryEraseExportProgressScreen_message_1 = '\0';
        memoryEraseExportProgressScreen_continueButton_enabled = true;
    }
    
    /* Erased completed */
    if(FLAG_GetAndReset(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_EVENT_ERASE_ALL) != false)
    {
        memoryEraseExportProgressScreen_operation = MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_ERASE_COMPLETE;
        memoryEraseExportProgressScreen_message_1 = '\0';
        memoryEraseExportProgressScreen_continueButton_enabled = true;
    }

    if(FLAG_GetAndReset(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_BUTTON_CONTINUE) != false)
    {
        FLAG_Set(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_COMMAND_EXIT_SCREEN);
    }
    
    if(memoryEraseExportProgressScreen_page1_exit != false)
    {
        memoryEraseExportProgressScreen_page1_exit = false;
        memoryEraseExportProgressScreen_message_1 = '\0';
        memoryEraseExportProgressScreen_continueButton_enabled = false;
        memoryEraseExportProgressScreen_operation = MANM_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_INIT;
    }
}

/* **********************************/
void Manm_ManageMemoryReportButtons(void)
{
    if(memory_reportScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_BUTTON_BACK);
    }
    if(memory_reportScreen_eraseButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_BUTTON_ERASE);
    }
    if(memory_reportScreen_printButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_BUTTON_PRINT);
    }
    if(memory_reportScreen_serialExportButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_BUTTON_SERIAL_EXPORT);
    }
    if(memory_reportScreen_usbExportButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_BUTTON_USB_EXPORT);
    }
    if(memory_reportScreen_upButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_BUTTON_UP);
    }
    if(memory_reportScreen_downButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_MEMORY_REPORT_SCREEN_BUTTON_DOWN);
    }
}

void Manm_ManageMemoryReportScreen(void)
{
    if(memoryReportScreen_page1_entered != false)
    {
        memoryReportScreen_page1_entered = false;
        UTIL_BufferFromSbyteToUShort(
                                        Manm_MemoryReport_FileNameToOpen, 
                                        string_length(Manm_MemoryReport_FileNameToOpen), 
                                        &memory_reportScreen_reportTitle_Text_1, 
                                        40U
        );
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_BACK) != false)
    {
        Manm_MemoryOperation = MANM_MEMORY_OPERATION_SINGLE_REPORT_VIEW;
        memoryReportScreen_goBack = true;
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_ERASE) != false)
    {
    
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_PRINT) != false)
    {
    
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_SERIAL_EXPORT) != false)
    {
    
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_USB_EXPORT) != false)
    {
    
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_UP) != false)
    {
    
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_REPORT_SCREEN_BUTTON_DOWN) != false)
    {
    
    }
    
    if(memoryReportScreen_page1_exit != false)
    {
        memoryReportScreen_page1_exit = false;
        memory_reportScreen_reportTitle_Text_1 = '\0';
        Manm_MemoryReport_FileNameToOpen = NULL;
    }
}

/* **********************************/
void Manm_ManageOopButtons(void)
{
    if(oopScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_SCREEN_BUTTON_BACK);
    }
    if(oopScreen_oop_environmentalButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_SCREEN_BUTTON_ENVIRONMENTAL);
    }
    if(oopScreen_oop_pm10Button_released != false) /* These flag is reset by GUI */
    { 
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_SCREEN_BUTTON_PM10);
    }
    if(oopScreen_oop_ductButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_SCREEN_BUTTON_DUCT);
    }
    if(oopScreen_oop_remoteTSBButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_SCREEN_BUTTON_REMOTE_TSB);
    }
    if(oopScreen_oop_remoteSRBButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_SCREEN_BUTTON_REMOTE_SRB); 
    }
    if(oopScreen_oop_clientTouch_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_SCREEN_TOUCH_CLIENT);
    }
}

void Manm_ManageOopScreen(void)
{    
    ubyte isRunning;
    ubyte isExpired;
    
    (void) UTIL_TimerIncrement(&Manm_OopScreen_NameTimer, Execution_Normal);
    
    (void) UTIL_TimerIsExpired(&Manm_OopScreen_NameTimer, &isExpired);
    if(isExpired != false)
    {
        (void) UTIL_TimerStop(&Manm_OopScreen_NameTimer);
        if(Manm_OopScreen_clientNameBlinkState != false)
        {
            Manm_OopScreen_clientNameBlinkState = false;
            /* Now set not visible */
            oopScreen_clientNameVisible = false;
            (void) UTIL_TimerStart(&Manm_OopScreen_NameTimer, MANM_OOP_SCREEN_NAME_NOT_VISIBLE_TIMER_MS);
        }
        else
        {
            Manm_OopScreen_clientNameBlinkState = true;
            /* Now set visible */
            oopScreen_clientNameVisible = true;
            (void) UTIL_TimerStart(&Manm_OopScreen_NameTimer, MANM_OOP_SCREEN_NAME_VISIBLE_TIMER_MS);
        }
    }
    
    if(oopScreen_page1_entered != false)
    {
        oopScreen_page1_entered = false;
        /* Check clients file exists */
        oopScreen_clientNameVisible = false;
        oopScreen_clientName_1 = '\0';
        (void) UTIL_TimerIsRunning(&Manm_OopScreen_NameTimer, &isRunning);
        if(isRunning != false)
        {
            (void) UTIL_TimerStop(&Manm_OopScreen_NameTimer);
        }
        FLAG_Set(FLAG_OOP_SCREEN_COMMAND_LOAD_CLIENTS); /* Send the command to open the client file in the thread */
    }
    
    if(FLAG_GetAndReset(FLAG_EVENT_CLIENT_FILE_OPENED_FOR_OOP) != FALSE)
    {
        oopScreen_clientNameVisible = true;
        if(Manm_Clients_Total_Number > 0)
        {
            /* Use the selected client for report */
            oopScreen_clientNameColor = BLUE;
            Manm_Clients_SelectedOne = clientScreen_selectedClientRetention;
            (void)UTIL_BufferFromSbyteToUShort(&MANM_Clients[Manm_Clients_SelectedOne].name[0]          , MANM_CLIENT_NAME_LENTGH           , &oopScreen_clientName_1         , MANM_CLIENT_NAME_LENTGH);
        }
        else
        {
            oopScreen_clientNameColor = RED;
            (void)UTIL_BufferFromSbyteToUShort("No client configurated", MANM_CLIENT_NAME_LENTGH, &oopScreen_clientName_1, MANM_CLIENT_NAME_LENTGH);
            /* Start timer for blink */
            (void) UTIL_TimerStart(&Manm_OopScreen_NameTimer, MANM_OOP_SCREEN_NAME_VISIBLE_TIMER_MS);
            Manm_OopScreen_clientNameBlinkState = true;
            oopScreen_clientNameVisible = true;
        }
    }    
    
    if(oopScreen_page1_exit != false)
    {
        oopScreen_page1_exit = false;
        /* On exit screen */
        oopScreen_clientName_1 = '\0';
        oopScreen_clientNameVisible = false;
        (void) UTIL_TimerStop(&Manm_OopScreen_NameTimer);
    }
}

/* **********************************/
void Manm_ManageOopEnvironmentalScreen(void)
{
    
}

/* **********************************/
void Manm_ManageOopPm10Screen(void)
{

}

/* **********************************/
void Manm_ManageOopDuctScreen(void)
{

}

/* **********************************/
void Manm_ManageOopRemoteTSBScreen(void)
{

}

/* **********************************/
void Manm_ManageOopRemoteSRBButtons(void)
{
    if(oop_remoteSRBScreen_backButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_BUTTON_BACK);
    }
    if(oop_remoteSRBScreen_startButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_BUTTON_START);
    }
    if(oop_remoteSRBScreen_stopButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_BUTTON_STOP);
    }
    if(oop_remoteSRBScreen_pauseButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_BUTTON_PAUSE);
    }
    if(oop_remoteSRBScreen_showChartButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_BUTTON_SHOW_CHART);
    }
    if(oop_remoteSRBScreen_isokineticSamplingComboBox_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_COMBOBOX_ISOKINETIC_SAMPLING);
    }
    if(oop_remoteSRBScreen_measureUnitComboBox_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_COMBOBOX_MEASURE_UNIT);
    }
    if(oop_remoteSRBScreen_nozzleDiameterComboBox_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_COMBOBOX_NOZZLE_DIAMETER);
    }
    if(oop_remoteSRBScreen_isokineticFlowComboBox_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_COMBOBOX_ISOKINETIC_FLOW);
    }
    if(oop_remoteSRBScreen_isokineticSamplingValue_eventAccepted != false) /* user must set the value to false */
    {
        oop_remoteSRBScreen_isokineticSamplingValue_eventAccepted = false;
        MANM_BUTTON_SOUND();
    }
    if(oop_remoteSRBScreen_normalizationTemperatureValue_eventAccepted != false)
    {
        oop_remoteSRBScreen_normalizationTemperatureValue_eventAccepted = false;
        MANM_BUTTON_SOUND();
    }
    if(oop_remoteSRBScreen_litriAspirazioneLineaDerivataValue_eventAccepted != false)
    {
        oop_remoteSRBScreen_litriAspirazioneLineaDerivataValue_eventAccepted = false;
        MANM_BUTTON_SOUND();
    }
    if(oop_remoteSRBScreen_temperaturaContatoreLineaDerivataValue_eventAccepted != false)
    {
        oop_remoteSRBScreen_temperaturaContatoreLineaDerivataValue_eventAccepted = false;
        MANM_BUTTON_SOUND();
    }
    if(oop_remoteSRBScreen_startDelayValue_eventAccepted != false)
    {
        oop_remoteSRBScreen_startDelayValue_eventAccepted = false;
        MANM_BUTTON_SOUND();
    }
    if(oop_remoteSRBScreen_isokineticFlowManualValue_eventAccepted != false)
    {
        oop_remoteSRBScreen_isokineticFlowManualValue_eventAccepted = false;
        MANM_BUTTON_SOUND();
    }
}

void Manm_ManageOopRemoteSRBScreen_manageIsokineticSamplingLimits(void)
{
    switch(oop_remoteSRBScreen_isokineticSampling)
    {
        case MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_TIME:
            oop_remoteSRBScreen_isokineticSamplingValue_rangeMin = 2U; /* min */
            oop_remoteSRBScreen_isokineticSamplingValue_rangeMax = 9999U;
            oop_remoteSRBScreen_isokineticSamplingValue = oop_remoteSRBScreen_isokineticSamplingValue_rangeMin;
            break;
        case MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_VOLUME:
            oop_remoteSRBScreen_isokineticSamplingValue_rangeMin = 1U; /* min */
            oop_remoteSRBScreen_isokineticSamplingValue_rangeMax = 65000U;
            oop_remoteSRBScreen_isokineticSamplingValue = oop_remoteSRBScreen_isokineticSamplingValue_rangeMin;
            break;
        default:
            break;
    }
}

STD_RETURN_t Manm_OopRemoteSbr_GetFlow(float* flow)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
#ifdef MANM_DEMO
    static float value = 1.15f;
    static float delta = 0.01f;
#endif
    if(flow != NULL)
    {
#ifdef MANM_DEMO
        if(value > 1.25f)
        {
            delta = -0.01f;
        }
        else
        {
            if(value < 1.15f)
            {
                delta = +0.01f;
            }
        }
        *flow = value;
        value = value + delta;
#else
        *flow = ; /* TODO */
#endif
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

void Manm_OopRemoteSbr_PopulateFlowDataOnView(ubyte init)
{
    float flow2mm_value = 0.0f;
    float flow4mm_value = 0.0f;
    float flow6mm_value = 0.0f;
    float flow8mm_value = 0.0f;
    float flow10mm_value = 0.0f;
    float flow12mm_value = 0.0f;
    float flow14mm_value = 0.0f;
    float flow16mm_value = 0.0f;
    float flow = 0.0f;;
    
    if(init == false)
    {   
        (void)Manm_OopRemoteSbr_GetFlow(&flow);
    }
    
    /* flow x (diameter / 2)^2 */
    flow2mm_value = flow * pow((2.0 / 2.0), 2);
    flow4mm_value = flow * pow((4.0 / 2.0), 2);
    flow6mm_value = flow * pow((6.0 / 2.0), 2);
    flow8mm_value = flow * pow((8.0 / 2.0), 2);
    flow10mm_value = flow * pow((10.0 / 2.0), 2);
    flow12mm_value = flow * pow((12.0 / 2.0), 2);
    flow14mm_value = flow * pow((14.0 / 2.0), 2);
    flow16mm_value = flow * pow((16.0 / 2.0), 2);
    
    oop_remoteSRBScreen_flow2mm_value = FLOAT_TO_WORD(flow2mm_value);
    oop_remoteSRBScreen_flow4mm_value = FLOAT_TO_WORD(flow4mm_value);
    oop_remoteSRBScreen_flow6mm_value = FLOAT_TO_WORD(flow6mm_value);
    oop_remoteSRBScreen_flow8mm_value = FLOAT_TO_WORD(flow8mm_value);
    oop_remoteSRBScreen_flow10mm_value = FLOAT_TO_WORD(flow10mm_value);
    oop_remoteSRBScreen_flow12mm_value = FLOAT_TO_WORD(flow12mm_value);
    oop_remoteSRBScreen_flow14mm_value = FLOAT_TO_WORD(flow14mm_value);
    oop_remoteSRBScreen_flow16mm_value = FLOAT_TO_WORD(flow16mm_value);
}

STD_RETURN_t Manm_OopRemoteSRBScreen_GetDataFromView(MANM_OOPREMOTESRB_InterfaceData_t* interfaceData)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    if(interfaceData != NULL)
    {
        interfaceData->samplingType = oop_remoteSRBScreen_isokineticSampling;
        interfaceData->time = oop_remoteSRBScreen_isokineticSamplingValue;
        interfaceData->volume = oop_remoteSRBScreen_isokineticSamplingValue;
        interfaceData->normalizationTemperature = oop_remoteSRBScreen_normalizationTemperatureValue;
        interfaceData->litriAspeLineaDerivata = oop_remoteSRBScreen_litriAspirazioneLineaDerivataValue;
        interfaceData->temperaturaContatoreLineaDerivata = oop_remoteSRBScreen_temperaturaContatoreLineaDerivataValue;
        interfaceData->samplingStartDelayMinutes = oop_remoteSRBScreen_startDelayValue;
        interfaceData->measureUnit = oop_remoteSRBScreen_measureUnit;
        interfaceData->nozzleDiameter = oop_remoteSRBScreen_nozzleDiameter;
        interfaceData->isokinetiFlow = oop_remoteSRBScreen_isokineticFlow;
        interfaceData->isokinetiFlowManualValue = WORD_TO_FLOAT(oop_remoteSRBScreen_isokineticFlowManualValue);
        returnValue = STD_RETURN_OK;
    }
    return returnValue;
}

void Manm_OopRemoteSRBScreen_manageTableFlowSelection(void)
{
    /*
#define BLACK              0
#define DARKBLUE           1
#define DARKGREEN          2
#define LIGHTBLUE          3
#define DARKRED            4
#define VIOLET             5
#define DARKYELLOW         6
#define LIGHTGRAY          7
#define DARKGRAY           8
#define BLUE               9
#define GREEN              10
#define CYAN               11
#define RED                12
#define PINK               13
#define YELLOW             14
#define WHITE              15
#define GRAY               24
*/
    const ushort selectedColor = DARKGREEN;
    const ushort notSelectedColor = BLACK;
    const ushort notUsedColor = DARKGRAY;
    if(oop_remoteSRBScreen_isokineticFlow == MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_AUTOMATIC)
    {
        switch(oop_remoteSRBScreen_nozzleDiameter)
        {
            case MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_2MM:
                oop_remoteSRBScreen_nozzle02mm_color = selectedColor;
                oop_remoteSRBScreen_nozzle04mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle06mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle08mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle10mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle12mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle14mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle16mm_color = notSelectedColor;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_4MM:
                oop_remoteSRBScreen_nozzle02mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle04mm_color = selectedColor;
                oop_remoteSRBScreen_nozzle06mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle08mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle10mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle12mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle14mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle16mm_color = notSelectedColor;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_6MM:
                oop_remoteSRBScreen_nozzle02mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle04mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle06mm_color = selectedColor;
                oop_remoteSRBScreen_nozzle08mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle10mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle12mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle14mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle16mm_color = notSelectedColor;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_8MM:
                oop_remoteSRBScreen_nozzle02mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle04mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle06mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle08mm_color = selectedColor;
                oop_remoteSRBScreen_nozzle10mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle12mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle14mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle16mm_color = notSelectedColor;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_10MM:
                oop_remoteSRBScreen_nozzle02mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle04mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle06mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle08mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle10mm_color = selectedColor;
                oop_remoteSRBScreen_nozzle12mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle14mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle16mm_color = notSelectedColor;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_12MM:
                oop_remoteSRBScreen_nozzle02mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle04mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle06mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle08mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle10mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle12mm_color = selectedColor;
                oop_remoteSRBScreen_nozzle14mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle16mm_color = notSelectedColor;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_14MM:
                oop_remoteSRBScreen_nozzle02mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle04mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle06mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle08mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle10mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle12mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle14mm_color = selectedColor;
                oop_remoteSRBScreen_nozzle16mm_color = notSelectedColor;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_16MM:
                oop_remoteSRBScreen_nozzle02mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle04mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle06mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle08mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle10mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle12mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle14mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle16mm_color = selectedColor;
                break;
            default:
                oop_remoteSRBScreen_nozzle02mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle04mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle06mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle08mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle10mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle12mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle14mm_color = notSelectedColor;
                oop_remoteSRBScreen_nozzle16mm_color = notSelectedColor;
                break;
        }
        oop_remoteSRBScreen_isokineticFlowManualValue_color = notUsedColor;
    }
    else
    {
        /* Manual flow */
        oop_remoteSRBScreen_nozzle02mm_color = notUsedColor;
        oop_remoteSRBScreen_nozzle04mm_color = notUsedColor;
        oop_remoteSRBScreen_nozzle06mm_color = notUsedColor;
        oop_remoteSRBScreen_nozzle08mm_color = notUsedColor;
        oop_remoteSRBScreen_nozzle10mm_color = notUsedColor;
        oop_remoteSRBScreen_nozzle12mm_color = notUsedColor;
        oop_remoteSRBScreen_nozzle14mm_color = notUsedColor;
        oop_remoteSRBScreen_nozzle16mm_color = notUsedColor;
        oop_remoteSRBScreen_isokineticFlowManualValue_color = selectedColor;
    }
}

STD_RETURN_t Manm_OopRemoteSRBScreen_GetSamplingTime(MANM_SamplingTime_t* samplingTime)
{
    STD_RETURN_t returnValue = STD_RETURN_ERROR_PARAM;
    if(samplingTime != NULL)
    {
        samplingTime->days = Manm_SamplingTime.days;
        samplingTime->hours = Manm_SamplingTime.hours;
        samplingTime->minutes = Manm_SamplingTime.minutes;
        samplingTime->seconds = Manm_SamplingTime.seconds;
        samplingTime->minus = Manm_SamplingTime.minus;
        samplingTime->startDelaySeconds = Manm_SamplingTime.startDelaySeconds;
        returnValue = STD_RETURN_OK;
    }
}

void Manm_OopRemoteSRBScreen_manageSamplingTimeOnScreen(void)
{
    MANM_SamplingTime_t samplingTime;
    sbyte samplingTimeStringChar[11U];
    sbyte signChar = ' ';
    
    (void)Manm_OopRemoteSRBScreen_GetSamplingTime(&samplingTime);

    if(samplingTime.minus != false)
    {
        signChar = '-';
    }
    
    /* Format string */
    string_sprintf(
                    &samplingTimeStringChar[0], 
                    "%c%3d:%02d:%02d",
                    signChar,
                    ((samplingTime.days * 24) + samplingTime.hours),  
                    samplingTime.minutes,
                    samplingTime.seconds
    );
    /* show on oop_remoteSRBScreen_samplingTimeString_1 */
    (void)UTIL_BufferFromSbyteToUShort(
                    &samplingTimeStringChar[0], 
                    11U, 
                    (ushort*)&oop_remoteSRBScreen_samplingTimeString_1, 
                    11U
    );
}

void Manm_ManageOopRemoteSRBScreen(void)
{    
    oop_remoteSRBScreen_isokineticSamplingLimits = oop_remoteSRBScreen_isokineticSampling;

    if(oop_remoteSRBScreen_page1_entered != false)
    {
        oop_remoteSRBScreen_page1_entered = false;
        
        Manm_OopRemoteSbr_PopulateFlowDataOnView(true);
        
        switch(Manm_OopRemoteSbrSampleState)
        {
            case MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_INIT: /* entered first time */
                /* Send command to connect to SBR and wait event to go in ready */
                FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_CONNECT_TO_SRB); 
                oop_remoteSRBScreen_isokineticSampling = MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_TIME;
                Manm_ManageOopRemoteSRBScreen_manageIsokineticSamplingLimits();
                oop_remoteSRBScreen_nozzleDiameter = MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_SELECT;
                oop_remoteSRBScreen_isokineticFlow = MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_AUTOMATIC; 
                oop_remoteSRBScreen_isokineticFlowManualValue = FLOAT_TO_WORD(MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_MANUAL_MIN_VALUE);
                oop_remoteSRBScreen_dataFields_enable = false;
                oop_remoteSRBScreen_nozzleDiameter_enable = false;
                oop_remoteSRBScreen_backButton_enable = true;
                oop_remoteSRBScreen_startButton_enabled = false;
                oop_remoteSRBScreen_stopButton_enabled = false;
                oop_remoteSRBScreen_pauseButton_enabled = false;
                oop_remoteSRBScreen_showChartButton_enabled = false;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_READY: /* not yet started */
                oop_remoteSRBScreen_dataFields_enable = true;
                oop_remoteSRBScreen_nozzleDiameter_enable = true;
                oop_remoteSRBScreen_backButton_enable = true;
                oop_remoteSRBScreen_startButton_enabled = false;
                oop_remoteSRBScreen_stopButton_enabled = false;
                oop_remoteSRBScreen_pauseButton_enabled = false;
                oop_remoteSRBScreen_showChartButton_enabled = false;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_RUNNING: /* running */
                oop_remoteSRBScreen_dataFields_enable = false;
                oop_remoteSRBScreen_nozzleDiameter_enable = false;
                oop_remoteSRBScreen_backButton_enable = false;
                oop_remoteSRBScreen_startButton_enabled = false;
                oop_remoteSRBScreen_stopButton_enabled = true;
                oop_remoteSRBScreen_pauseButton_enabled = true;
                oop_remoteSRBScreen_showChartButton_enabled = true;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_PAUSED: /* paused */
                oop_remoteSRBScreen_dataFields_enable = false;
                oop_remoteSRBScreen_nozzleDiameter_enable = true;
                oop_remoteSRBScreen_backButton_enable = false;
                oop_remoteSRBScreen_startButton_enabled = true;
                oop_remoteSRBScreen_stopButton_enabled = true;
                oop_remoteSRBScreen_pauseButton_enabled = false;
                oop_remoteSRBScreen_showChartButton_enabled = true;
                break;
            case MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_STOPPED: /* stopped */
                oop_remoteSRBScreen_dataFields_enable = true;
                oop_remoteSRBScreen_nozzleDiameter_enable = true;
                oop_remoteSRBScreen_backButton_enable = true;
                oop_remoteSRBScreen_startButton_enabled = true;
                oop_remoteSRBScreen_stopButton_enabled = false;
                oop_remoteSRBScreen_pauseButton_enabled = false;
                oop_remoteSRBScreen_showChartButton_enabled = true;
                break;
        }
    }
    
    Manm_OopRemoteSRBScreen_manageSamplingTimeOnScreen();
    
    Manm_OopRemoteSRBScreen_manageTableFlowSelection();    

    /* *******************************************************/
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMBOBOX_ISOKINETIC_SAMPLING) != false)
    {
        Manm_ManageOopRemoteSRBScreen_manageIsokineticSamplingLimits();
    }

    if(oop_remoteSRBScreen_isokineticFlow == MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_FLOW_MANUAL)
    {
        oop_remoteSRBScreen_isokineticFlowManualValue_visible = true;
    }
    else
    {
        oop_remoteSRBScreen_isokineticFlowManualValue_visible = false;
    }
    
    /* When change nozzle diameter combo box check if diameter is different from select enable start button */
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMBOBOX_NOZZLE_DIAMETER) != false)
    {
        if(oop_remoteSRBScreen_nozzleDiameter != MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_SELECT)
        {
            oop_remoteSRBScreen_startButton_enabled = true;
        }
        else
        {
            oop_remoteSRBScreen_startButton_enabled = false;
        }
    }

    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_BUTTON_START) != false)
    {
        Manm_OopRemoteSbrSampleState = MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_RUNNING;
        /* Send start command */
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_START); 
        /* disable all the edit fieds include nozzle diameter, and back button, set only stop, pause and chart button enabled */
        oop_remoteSRBScreen_dataFields_enable = false;
        oop_remoteSRBScreen_nozzleDiameter_enable = false;
        oop_remoteSRBScreen_backButton_enable = false;
        oop_remoteSRBScreen_startButton_enabled = false;
        oop_remoteSRBScreen_stopButton_enabled = true;
        oop_remoteSRBScreen_pauseButton_enabled = true;
        oop_remoteSRBScreen_showChartButton_enabled = true;
    }
    
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_BUTTON_STOP) != false)
    {
        Manm_OopRemoteSbrSampleState = MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_STOPPED;
        /* Send stop command */
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_STOP); 
        /* enable all the edit fieds include nozzle diameter, and back button, set only stop, pause and chart button disabled */
        oop_remoteSRBScreen_dataFields_enable = true;
        oop_remoteSRBScreen_nozzleDiameter_enable = true;
        oop_remoteSRBScreen_backButton_enable = true;
        oop_remoteSRBScreen_startButton_enabled = true;
        oop_remoteSRBScreen_stopButton_enabled = false;
        oop_remoteSRBScreen_pauseButton_enabled = false;
        oop_remoteSRBScreen_showChartButton_enabled = true;
    }
    
    /* Send pause command */
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_BUTTON_PAUSE) != false)
    {
        Manm_OopRemoteSbrSampleState = MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_PAUSED;
        
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_PAUSE); 
        /* disable all the edit fieds, and back button, set only stop, start and chart button enabled, set nozzle diameter enabled */
        oop_remoteSRBScreen_dataFields_enable = false;
        oop_remoteSRBScreen_nozzleDiameter_enable = true;
        oop_remoteSRBScreen_backButton_enable = false;
        oop_remoteSRBScreen_startButton_enabled = true;
        oop_remoteSRBScreen_stopButton_enabled = true;
        oop_remoteSRBScreen_pauseButton_enabled = false;
        oop_remoteSRBScreen_showChartButton_enabled = true;
    }
    
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_BUTTON_BACK) != false)
    {
        /* If I press back re init the GUI */
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB);
        Manm_OopRemoteSbrSampleState = MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_INIT;
        /* */
    }
    
    /* ******************************************************************/
    /* Manage srb events                                                */
    /* FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_CONNECTION_OK,       */
    /* FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_CONNECTION_TIMEOUT,  */
    /* FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_CONNECTION_LOST,     */
    /* FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_DATA_ARRIVED,     */
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_CONNECTION_OK) != false)
    {
        /* enable fields from init state */
        oop_remoteSRBScreen_dataFields_enable = true;
        oop_remoteSRBScreen_nozzleDiameter_enable = true;
        oop_remoteSRBScreen_backButton_enable = true;
        oop_remoteSRBScreen_startButton_enabled = false;
        oop_remoteSRBScreen_stopButton_enabled = false;
        oop_remoteSRBScreen_pauseButton_enabled = false;
        oop_remoteSRBScreen_showChartButton_enabled = false;
    }
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_CONNECTION_TIMEOUT) != false)
    {
        /* TODO */
    }
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_CONNECTION_LOST) != false)
    {
        /* TODO */
    }
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_DATA_ARRIVED) != false)
    {
        Manm_OopRemoteSbr_PopulateFlowDataOnView(false);
    }

    /* Sampling stopped due to automatic condition, time, volume, or error */
    if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_AUTOMATIC_STOP_CONDITION_REACHED) != false)
    {         
        Manm_OopRemoteSbrSampleState = MANM_OOP_REMOTE_SBRSCREEN_SAMPLE_STATE_STOPPED;

        /* enable all the edit fieds include nozzle diameter, and back button, set only stop, pause and chart button disabled */
        oop_remoteSRBScreen_dataFields_enable = true;
        oop_remoteSRBScreen_nozzleDiameter_enable = true;
        oop_remoteSRBScreen_backButton_enable = true;
        oop_remoteSRBScreen_startButton_enabled = true;
        oop_remoteSRBScreen_stopButton_enabled = false;
        oop_remoteSRBScreen_pauseButton_enabled = false;
        oop_remoteSRBScreen_showChartButton_enabled = true;
    }
    /* *******************************************************/
    
    if(oop_remoteSRBScreen_page1_exit != false)
    {
        oop_remoteSRBScreen_page1_exit = false;
        
        Manm_OopRemoteSbr_PopulateFlowDataOnView(true);
    }
}

/* **********************************/
void Manm_ManageSamplingTerminatedButtons(void)
{
    if(samplingTerminatedScreen_continueButton_released != false) /* These flag is reset by GUI */
    {
        MANM_BUTTON_SOUND();
        FLAG_Set(FLAG_SAMPLING_TERMINATED_SCREEN_BUTTON_CONTINUE);
    }
}

void Manm_ManageSamplingTerminatedScreen(void)
{
    const sbyte* timeReachedMessage     = "Time Reached";
    const sbyte* volumeReachedMessage   = "Volume Reached";
    const sbyte* manualStopMessage      = "Manual Stop";
    
    sbyte bufferMessage[200U] = {0x00U};
    
    MANM_SAMPLING_ERROR_t reportError = MANM_SAMPLING_ERROR_NONE;
    MANM_SAMPLING_STOP_CAUSE_t stopCause;
    
    if(samplingTerminatedScreen_page1_entered != false)
    {
        samplingTerminatedScreen_page1_entered = false;

        /* Get cause of stop, get error cause */
        (void)Manm_ReportGetStoCauseAndError(&stopCause, &reportError);

        switch(stopCause)
        {
            case MANM_SAMPLING_STOP_CAUSE_TIME:
                samplingTerminatedScreen_message_color = DARKGREEN;
                UTIL_BufferFromSbyteToUShort(
                                    (sbyte*)timeReachedMessage, 
                                    string_length((sbyte*)timeReachedMessage), 
                                    (ushort*)&samplingTerminatedScreen_message_1,
                                    200U 
                                    );
                break;
            case MANM_SAMPLING_STOP_CAUSE_VOLUME:
                samplingTerminatedScreen_message_color = DARKGREEN;
                UTIL_BufferFromSbyteToUShort(
                                    (sbyte*)volumeReachedMessage, 
                                    string_length((sbyte*)volumeReachedMessage), 
                                    (ushort*)&samplingTerminatedScreen_message_1,
                                    200U 
                                    );
                break;
            case MANM_SAMPLING_STOP_CAUSE_MANUAL:
                samplingTerminatedScreen_message_color = 136; /* Orange I hope */
                UTIL_BufferFromSbyteToUShort(
                                    (sbyte*)manualStopMessage, 
                                    string_length((sbyte*)manualStopMessage), 
                                    (ushort*)&samplingTerminatedScreen_message_1,
                                    200U 
                                    );
                break;
            case MANM_SAMPLING_STOP_CAUSE_ERROR:
                samplingTerminatedScreen_message_color = RED; 
                string_sprintf(&bufferMessage[0], "Error Stop\nError Code: #%04X", reportError);
                UTIL_BufferFromSbyteToUShort(
                                    (sbyte*)&bufferMessage[0], 
                                    200U, 
                                    (ushort*)&samplingTerminatedScreen_message_1,
                                    200U 
                                    );
                break;
            default:
                break;
        }
    }
    
    if(samplingTerminatedScreen_page1_exit != false)
    {
        samplingTerminatedScreen_page1_exit = false;
        samplingTerminatedScreen_message_1 = '\0';
        
    }
}

/* **********************************/
void Manm_ManageEolScreen(void)
{

}

/* **********************************/
void Manm_ManageInfoScreen(void)
{

}

/* Public functions ----------------------------------------------------------*/
/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
void MANM_init()
{
    Manm_InitVariables();
}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
void MANM()
{
    Manm_NormalRoutine++; /* Only for debug */
    Manm_ManageButtons();
    Manm_ManageScreens();
}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
void MANM_fast()
{
    Manm_FastRoutine++; /* Only for debug */
    
}

void Manm_MemoryCreateList(void)
{
    ubyte toAdd;
    ulong listId = file_list_get(MANM_REPORT_FILE_NAME_EXTENTION, MANM_REPORT_LOCATION_DEFAULT_SAVE);
    /*debug_print("listId: %d", listId);*/
    ulong listSize = list_size(listId);
    ulong listFilteredSize = 0;
    /*debug_print("list size: %d", listSize);*/

    sbyte* filter = NULL;
    /* select the filter */
    if((memoryScreen_filterReport >= 1U) && (memoryScreen_filterReport < (MANM_REPORT_NAME_CODE_MAX_NUMBER + 1U)))
    {
        MANM_REPORT_NAME_CODE_t reportType = memoryScreen_filterReport - 1U;
        filter = (sbyte*)MANM_report_name_codes[reportType];
        /* debug_print("filter: %s", filter); */
    }
    /* list exists delete for a new use  */
    if(Manm_MemoryScreen_FileListId >= 0) 
    {
        list_delete(Manm_MemoryScreen_FileListId);
        debug_print("Manm_MemoryScreen_FileListId deleted in create list");
        Manm_MemoryScreen_FileListId = -1;
    }
    /* list doesn't exist create */
    if(Manm_MemoryScreen_FileListId < 0) 
    {
        Manm_MemoryScreen_FileListId = list_create();
        /* debug_print("Manm_MemoryScreen_FileListId: %d", Manm_MemoryScreen_FileListId); */
    }
    /* roll the entire file list and select the file that matches the filter */
    for(int i = 0; i < listSize; i++)
    {
        /* in that case it is a list of strings */
        sbyte* str = list_item_at(listId, i);
                    
        /* debug_print("File name #%d: %s", i, str); */
        toAdd = true;
        if(filter != NULL)
        {                
            if(UTIL_string_contain(str, string_length(str), MANM_REPORT_TYPE_POSITION_IN_STRING_NAME, filter, string_length(filter)) != false)
            {
                toAdd = true; /* filter and match */
            }
            else
            {
                toAdd = false; /* filter and it doesn't match*/
            }
        }
        else
        {
            toAdd = true; /* no filter add all the items */
        }
        
        if(toAdd != false)
        {
            /* debug_print("ADD"); */
            if(Manm_MemoryScreen_FileListId < 0) /* security check */
            {
                /* Create list if not yet created */
                Manm_MemoryScreen_FileListId = list_create();
                /* debug_print("Manm_MemoryScreen_FileListId: %d", Manm_MemoryScreen_FileListId); */
            }
            /* length of string to copy */
            int fileNameLength = string_length(str);
            /* debug_print("fileNameLength %d of string %s", fileNameLength, str); */
            /* alloc the space */
            sbyte* strToAppend = memory_alloc(fileNameLength + 1U);
            /* copy from the entire list to the filtered list */
            memory_copy(strToAppend, str, fileNameLength);
            strToAppend[fileNameLength] = '\0';
            //debug_print("strToAppend: %s", strToAppend);
            ubyte appended = list_append(Manm_MemoryScreen_FileListId, strToAppend);
            //debug_print("File name #%d: added %d", i, appended);
        }
    }

    list_delete(listId); /* delete entire original file list */

    listFilteredSize = list_size(Manm_MemoryScreen_FileListId);

    debug_print("Filtered %d entry on %d", listFilteredSize, listSize);
}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
void MANM_low_priority()
{
    Manm_LowPriorityRoutine++; /* Only for debug */
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_COMMAND_CLEAR_FILE_LIST) != false)
    {
        if(Manm_MemoryScreen_FileListId >= 0) /* list exists delete for a new use  */
        {
            list_delete(Manm_MemoryScreen_FileListId);
            debug_print("Manm_MemoryScreen_FileListId deleted in low prio");
            Manm_MemoryScreen_FileListId = -1;
        }
        else
        {
            debug_print("Manm_MemoryScreen_FileListId nothing to delete");
        }
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_SCREEN_COMMAND_CREATE_FILE_LIST) != false)
    {
        Manm_MemoryCreateList();
        
        FLAG_Set(FLAG_MEMORY_SCREEN_EVENT_FILE_LIST_CREATED);
    }
}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
#define MS_TO_US(x) ((x) * 1000)
void Manm_main_screen_model_init(void)
{

}

void Manm_main_screen_model(ulong execTimeMs)
{
    STD_RETURN_t threadReturn;
    if(FLAG_GetAndReset(FLAG_COMMAND_OPEN_CLIENT_FILE) != FALSE)
    {
        debug_print("FLAG_COMMAND_OPEN_CLIENT_FILE");
        threadReturn = Manm_Client_OpenAndReadOrCreateFile((sbyte*)&Manm_Clients_FileName[0], Manm_clientLocationSave, (sbyte*)&Manm_Clients_FileHeader[0]/*, &Manm_Clients_FileDescriptor*/);
        switch(threadReturn)
        {
            case STD_RETURN_OK:
               FLAG_Set(FLAG_EVENT_CLIENT_FILE_OPENED);
               break;
            default:
                debug_print("ERROR: %d", threadReturn);
                break;
        }
    }
    
    if(FLAG_GetAndReset(FLAG_COMMAND_SAVE_CLIENT_FILE) != FALSE)
    {
        debug_print("FLAG_COMMAND_SAVE_CLIENT_FILE");
        threadReturn = Manm_Client_SaveFile((sbyte*)&Manm_Clients_FileName[0], Manm_clientLocationSave);
        switch(threadReturn)
        {
            case STD_RETURN_OK:
               FLAG_Set(FLAG_EVENT_CLIENT_FILE_SAVED);
               break;
            default:
                debug_print("ERROR: %d", threadReturn);
                break;
        }
    }
    
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_REMOVE_FILE) != FALSE)
    {        
        if(file_remove((sbyte*)&Manm_Clients_FileName[0],Manm_clientLocationSave) != 0)
        {
            debug_print("file not removed");
        }
        else
        {
            debug_print("file removed");
        }
        
    }
    if(FLAG_GetAndReset(FLAG_CLIENT_SCREEN_BUTTON_COPY_FILE) != FALSE)
    {
        if(file_copy((sbyte*)&Manm_Clients_FileName[0], Manm_clientLocationSave, (sbyte*)&Manm_Clients_FileName[0], Manm_clientLocationExport) < 0)
        {
            debug_print("error on copy");
        }
        else
        {
            debug_print("file copied");
        }
    }
}

void Manm_oop_screen_model_init(void)
{

}

void Manm_oop_screen_model(ulong execTimeMs)
{
    STD_RETURN_t threadReturn;
    if(FLAG_GetAndReset(FLAG_OOP_SCREEN_COMMAND_LOAD_CLIENTS) != FALSE)
    {
        debug_print("FLAG_OOP_SCREEN_COMMAND_LOAD_CLIENTS");
        threadReturn = Manm_Client_OpenAndReadOrCreateFile((sbyte*)&Manm_Clients_FileName[0], Manm_clientLocationSave, (sbyte*)&Manm_Clients_FileHeader[0]/*, &Manm_Clients_FileDescriptor*/);
        switch(threadReturn)
        {
            case STD_RETURN_OK:
               FLAG_Set(FLAG_EVENT_CLIENT_FILE_OPENED_FOR_OOP);
               break;
            default:
                debug_print("ERROR: %d", threadReturn);
                break;
        }
    }
}

void Manm_oopRemoteSrb_screen_model_init(void)
{
    (void)UTIL_TimerInit(&Manm_oopRemoteSrb_timerConnect);
#ifdef MANM_DEMO
    (void)UTIL_TimerInit(&Manm_oopRemoteSrb_timerDataDemo);
#endif 
    (void)UTIL_TimerInit(&Manm_oopRemoteSrb_timer5minutes);

    Manm_SRB_StateMachine = MANM_SRB_STATE_INIT;
}

ulong Manm_oopRemoteSrb_screen_model_manageSamplingTimeGetStartDelayRemained(void)
{
    return Manm_SamplingTime.startDelaySeconds;
}

void Manm_oopRemoteSrb_screen_model_manageSamplingTime(ubyte initFlag, ulong startDelaySeconds)
{
    static ulong lastSec = 0;

    if(initFlag != false)
    {
        /* init the sample time struct */
        Manm_SamplingTime.days = 0;
        Manm_SamplingTime.hours = 0;
        Manm_SamplingTime.minutes = 0;
        Manm_SamplingTime.seconds = 0;
        Manm_SamplingTime.minus = false;
        Manm_SamplingTime.startDelaySeconds = startDelaySeconds;
        lastSec = datarioSecondi;
    }
    else
    {
        if(datarioSecondi != lastSec)
        {
            lastSec = datarioSecondi;
            /* second has changed */
            
            if(Manm_SamplingTime.startDelaySeconds > 0)
            {
                /* decrease the delaySeconds until they reach 0, then count normally */
                
                Manm_SamplingTime.seconds = (Manm_SamplingTime.startDelaySeconds % 60U); /* sec */
                
                Manm_SamplingTime.minutes = (Manm_SamplingTime.startDelaySeconds / 60U) % 60U; /* 60 sec in 1 min */
                
                Manm_SamplingTime.hours = (Manm_SamplingTime.startDelaySeconds / 3600U) % 24U; /* 3600 sec in 1 hour */
                
                Manm_SamplingTime.days = (Manm_SamplingTime.startDelaySeconds / 86400U); /* 86400U sec in 1 day */
            
                Manm_SamplingTime.minus = true;
                
                Manm_SamplingTime.startDelaySeconds--;
            }
            else
            {
                /* delay terminated start to count normally */
                Manm_SamplingTime.seconds++;
                Manm_SamplingTime.minus = false;
                
                if((Manm_SamplingTime.seconds % 60) == 0)
                {
                    Manm_SamplingTime.seconds = 0;
                    /* +1 min */
                    Manm_SamplingTime.minutes++;
                    if((Manm_SamplingTime.minutes % 60) == 0)
                    {
                        Manm_SamplingTime.minutes = 0;
                        /* + 1h */
                        Manm_SamplingTime.hours++;
                        if((Manm_SamplingTime.hours % 24) == 0)
                        {
                            Manm_SamplingTime.hours = 0;
                            Manm_SamplingTime.days++;
                        }
                    }
                }
            }
        }
    }
}

STD_RETURN_t Manm_report_createName(MANM_REPORT_NAME_CODE_t reportType)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(reportType < MANM_REPORT_NAME_CODE_MAX_NUMBER)
    {
        /* buffer length is only 34, 33 + 1 terminator */
        string_sprintf(&Manm_reportName[0], 
                        "REPORT_%04d_%02d_%02d_%02d_%02d_%02d_%s.%s",
                        datarioAnno,
                        datarioMese,
                        datarioGiorno,
                        datarioOre,
                        datarioMinuti,
                        datarioSecondi,
                        MANM_report_name_codes[reportType],
                        MANM_REPORT_FILE_NAME_EXTENTION
        );
        debug_print("Manm_report_createName %s", &Manm_reportName[0]);
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_createName: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t Manm_report_openFile(int* fd)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(fd != NULL)
    {
        *fd = file_open_append(&Manm_reportName[0], Manm_reportLocationSave);
        if(*fd < 0)
        {
            //debug_print("file NOT opened fd: %d\n try to create file", fd);
            *fd = file_creat(&Manm_reportName[0], Manm_reportLocationSave);
            if(*fd < 0)
            {
                 //debug_print("file NOT created fd: %d", fd);
                 returnValue = STD_RETURN_ERROR_REPORT_NOT_CREATED;
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_openFile: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t Manm_report_closeFile(int* fd)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(fd != NULL)
    {
        if(*fd < 0)
        {
            returnValue = STD_RETURN_ERROR_FILE_ALREADY_CLOSED;
        }
        else
        {
            if(file_close(*fd))
            {
                //debug_print("file NOT closed");
                returnValue = STD_RETURN_ERROR_REPORT_NOT_CLOSED;
            }
        }
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_closeFile: %d", returnValue);
    }
    return returnValue;
}

sbyte* Manm_report_getTag(MANM_REPORT_TAG_t tag)
{
    sbyte* returnValue = (sbyte*)MANM_report_tags[MANM_REPORT_TAG_DUMMY];
    if(tag < MANM_REPORT_TAG_MAX_NUMBER)
    {
        returnValue = (sbyte*)MANM_report_tags[tag];
    }
    return returnValue;
}

sbyte* Manm_report_getDuctShape(MANM_DUCT_SHAPE_t shape)
{
    sbyte* returnValue = (sbyte*)MANM_report_duct_shapes[MANM_DUCT_SHAPE_UNKNOWN];
    if(shape < MANM_DUCT_SHAPE_MAX_NUMBER)
    {
        returnValue = (sbyte*)MANM_report_duct_shapes[shape];
    }
    return returnValue;
}

ulong Manm_report_getNozzleDiameter(MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_t nozzleDiameter)
{
    ulong returnValue = 0U;
    if(nozzleDiameter != MANM_OOP_REMOTE_SBRSCREEN_NOZZLE_DIAMETER_SELECT)
    {
        returnValue = nozzleDiameter * 2U; /* 2mm each increment */
    }
    return returnValue;
}

sbyte* Manm_report_getMeasureUnit(MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_t measure_unit)
{
    sbyte* returnValue = (sbyte*)MANM_report_measure_unit[MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_NO_UNIT];
    if(measure_unit < MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_MAX_NUMBER)
    {
        returnValue = (sbyte*)MANM_report_measure_unit[measure_unit];
    }
    return returnValue;
}

#define Manm_reportWriteLine(...) \
{ \
    string_sprintf(&tempBuffer[0], __VA_ARGS__); \
    file_write_line(fd, &tempBuffer[0]); \
} \

STD_RETURN_t Manm_report_writeInstrumentIds(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("Manm_report_writeInstrumentIds");
    
    returnValue = Manm_report_openFile(&fd);
    
    if(returnValue == STD_RETURN_OK)
    {
        string_sprintf(&tempBuffer[0], "[TODO] *LIFETECK 55XPR R2.1 S/N 55328*");
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeInstrumentIds: %d", returnValue);
    }
	return returnValue;
}

STD_RETURN_t Manm_report_writeClientId(void) /* anagrafica*/
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("Manm_report_writeClientId");
    
    returnValue = Manm_report_openFile(&fd);
        
    if(returnValue == STD_RETURN_OK)
    {
        /* */
        if(Manm_Clients_Total_Number > 0)
        {
            if((Manm_Clients_SelectedOne >= 0) && (Manm_Clients_SelectedOne < MANM_CLIENTS_MAX_NUMBER))
            {
                /* TODO Manm_reportWriteLine use this define */
                string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - CLIENT ID", 
                                Manm_report_getTag(MANM_REPORT_TAG_CLIENT_ID),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
                file_write_line(fd, &tempBuffer[0]);
                string_sprintf(&tempBuffer[0], "Name: %s", MANM_Clients[Manm_Clients_SelectedOne].name);
                file_write_line(fd, &tempBuffer[0]);
                string_sprintf(&tempBuffer[0], "Address: %s", MANM_Clients[Manm_Clients_SelectedOne].address);
                file_write_line(fd, &tempBuffer[0]);
                string_sprintf(&tempBuffer[0], "CAP: %s", MANM_Clients[Manm_Clients_SelectedOne].CAP);
                file_write_line(fd, &tempBuffer[0]);
                string_sprintf(&tempBuffer[0], "City: %s", MANM_Clients[Manm_Clients_SelectedOne].city);
                file_write_line(fd, &tempBuffer[0]);
                string_sprintf(&tempBuffer[0], "District: %s", MANM_Clients[Manm_Clients_SelectedOne].district);
                file_write_line(fd, &tempBuffer[0]);
                string_sprintf(&tempBuffer[0], "Plant Number: %s", MANM_Clients[Manm_Clients_SelectedOne].plantNumber);
                file_write_line(fd, &tempBuffer[0]);
                string_sprintf(&tempBuffer[0], "Note: %s", MANM_Clients[Manm_Clients_SelectedOne].note);
                file_write_line(fd, &tempBuffer[0]);
            }
            else
            {
                returnValue = STD_RETURN_ERROR_CALL_WRITE_CLIENT_ID_SELECTED_OUT_OF_BOUNDS;
            }
        }
        else
        {
            returnValue = STD_RETURN_ERROR_CALL_WRITE_CLIENT_ID_NO_CLIENTS;
        }        
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeClientId: %d", returnValue);
    }
	return returnValue;
}

STD_RETURN_t Manm_report_writeInitData(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
    MANM_OOPREMOTESRB_InterfaceData_t interfaceData;
    sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
    debug_print("Manm_report_writeInitData");
    
    returnValue = Manm_OopRemoteSRBScreen_GetDataFromView(&interfaceData);
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        /* TODO Manm_reportWriteLine use this define */
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - CLIENT ID", 
                                Manm_report_getTag(MANM_REPORT_TAG_INIT_DATA),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "SAMPLING TIME MIN: %d", ((Manm_SamplingTime.days * 1440) + (Manm_SamplingTime.hours * 60) + Manm_SamplingTime.minutes));
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "DUCT SHAPE: %s", Manm_report_getDuctShape(Manm_oopRemoteSrbScreen_lastSetupMessage.ductType));
        file_write_line(fd, &tempBuffer[0]);
        switch(Manm_oopRemoteSrbScreen_lastSetupMessage.ductType)
        {
            case MANM_DUCT_SHAPE_CIRCLE:
                string_sprintf(&tempBuffer[0], "DUCT DIAMETER CM: %d", Manm_oopRemoteSrbScreen_lastSetupMessage.ductDiameter);
                break;
            case MANM_DUCT_SHAPE_RECTANGULAR:
                string_sprintf(&tempBuffer[0], "DUCT SIDES CM: %d x %d", Manm_oopRemoteSrbScreen_lastSetupMessage.maxSide, Manm_oopRemoteSrbScreen_lastSetupMessage.minSide);
                break;
            default:
                string_sprintf(&tempBuffer[0], "DUCT SIZE UNKNOWN");
                break;
        }
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "START DELAY MIN: %d", interfaceData.samplingStartDelayMinutes);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] DUCT CONSTANT: %d", Manm_oopRemoteSrbScreen_lastSetupMessage.kappa);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] DENSITY Kg/m3: %d", Manm_oopRemoteSrbScreen_lastSetupMessage.density);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] CONDENSATE %%: %d", Manm_oopRemoteSrbScreen_lastSetupMessage.condensate);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] BAROMETRIC HPA: %0.2f", Manm_oopRemoteSrbScreen_lastSetupMessage.barometric);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "NORMALIZATION TEMPERATURE �C: %d", interfaceData.normalizationTemperature);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "NOZZLE DIAMETER MM: %d", Manm_report_getNozzleDiameter(interfaceData.nozzleDiameter));
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] EFFETTO PARETE FWA: %0.2f", 1.0f);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] LITRI ASPIRATI L.DERIVATA L: %d", interfaceData.litriAspeLineaDerivata);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] TEMP. CONTATORE L.DERIVATA �C: %d", interfaceData.temperaturaContatoreLineaDerivata);
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeInitData: %d", returnValue);
    }
	return returnValue;
}

STD_RETURN_t Manm_report_writeDelayedStart(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("Manm_report_writeDelayedStart");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        /* TODO Manm_reportWriteLine use this define */
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - DELAYED START STARTED", 
                                Manm_report_getTag(MANM_REPORT_TAG_DELAYED_START),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeDelayedStart: %d", returnValue);
    }
	return returnValue;
}

STD_RETURN_t Manm_report_writeStart(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    int fd;
	sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};

	debug_print("Manm_report_writeStart");
		
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        /* TODO Manm_reportWriteLine use this define */
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - START REMOTE SAMPLING", 
                                Manm_report_getTag(MANM_REPORT_TAG_START),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
    }
    
        if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeStart: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t Manm_report_writeDelayedPaused(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("Manm_report_writeDelayedPaused");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        /* TODO Manm_reportWriteLine use this define */
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - DELAYED PAUSE", 
                                Manm_report_getTag(MANM_REPORT_TAG_DELAYED_PAUSE),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeDelayedPaused: %d", returnValue);
    }		
	return returnValue;
}

STD_RETURN_t Manm_SRB_GetFinalData(MANM_SamplingSRBFinalData_t* finalData)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(finalData != NULL)
    {
        /* TODO */
        ulong hours = 0;
        ulong minutes = 0;
        ulong seconds = 0;
        
        if(Manm_SamplingTime.startDelaySeconds == 0)
        {
            /* Sampling time started */
            hours = (Manm_SamplingTime.days * 24U) + Manm_SamplingTime.hours;
            minutes = Manm_SamplingTime.minutes;
            seconds = Manm_SamplingTime.seconds;
        }
        /* TODO */
        MANM_SamplingSRBFinalData_t srb_fd = 
        {
            1.0f,
            hours,
            minutes,
            seconds,
            2.0f,
            3.0f,
            4.0f,
            5.0f,
            6.0f,
            7.0f,
            8.0f,
            9.0f,
            10.0f,
            11.0f,
            12.0f,
            13.0f,
            14.0f,
            15.0f,
            Manm_SamplingError,
            Manm_SamplingStopCause
        };
        
        memory_copy(finalData, &srb_fd, sizeof(MANM_SamplingSRBFinalData_t));
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_SRB_GetFinalData: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t Manm_report_writeFinalData(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
	MANM_SamplingSRBFinalData_t finalData;
		
	debug_print("Manm_report_writeFinalData");
	
	returnValue = Manm_SRB_GetFinalData(&finalData);
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - FINAL DATA", 
                                Manm_report_getTag(MANM_REPORT_TAG_FINAL_DATA),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
        
        string_sprintf(&tempBuffer[0], "[TODO] VOLUME ASPIRATO LIFETEK L = %.2f", finalData.lifeteckSampledVolume);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "SAMPLING TOTAL TIME = %.2f", ((float)((finalData.samplingTimeHours * 60U) + finalData.samplingTimeMinutes)) + (((float)finalData.samplingTimeSeconds) / 60.0f));
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] FLUSSO MEDIO LIFETEK l/min = %.2f", finalData.lifeteckSamplingFlowMedia);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] TEMP. MEDIA CONTATORE �C = %.2f", finalData.temperatureMeterCounterMedia);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] VOLUME NORM. LIFETEK l = %.2f", finalData.lifeteckNormalizedVolumeLiter);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] TEMP. MEDIA CONDOTTO �C = %.2f", finalData.ductTemperatureMedia);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] PRESS.STATICA ASSOLUTA hPa = %.2f", finalData.staticPressureAbsolute);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] VELOCITA MEDIA m/sec = %.2f", finalData.speedMedia);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] PORTATA MEDIA m3/h = %.2f", finalData.flowRateMedia);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] PORTATA MEDIA NORM. Nm3/h = %.2f", finalData.flowRateNormalizedMedia);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] VOLUME LD ( T.CONT LD) l = %.2f", finalData.volume_ld_t_cont);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] VOLUME LD(T.MEDIA CONT.) l = %.2f", finalData.volume_ld_tmedia_cont);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] VOLUME TOT.(L.D.+LIFTEK) l = %.2f", finalData.volume_tot_ld_plus_lifeteck);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] VOLUME LD (T. NORM.) l = %.2f", finalData.volume_ld_t_norm);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] VOLUME TOT. (T. NORM.) l = %.2f", finalData.volume_tot_t_norm);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "[TODO] GRADO ISOCINETISMO %% = %.2f", finalData.isokineticLevel);
        file_write_line(fd, &tempBuffer[0]);
        
        switch(finalData.stopCause)
        {
            case MANM_SAMPLING_STOP_CAUSE_TIME:
                string_sprintf(&tempBuffer[0], "SAMPLING STOP CAUSE: TIME REACHED");
                break;
            case MANM_SAMPLING_STOP_CAUSE_VOLUME:
                string_sprintf(&tempBuffer[0], "SAMPLING STOP CAUSE: VOLUME REACHED");
                break;
            case MANM_SAMPLING_STOP_CAUSE_MANUAL:
                string_sprintf(&tempBuffer[0], "SAMPLING STOP CAUSE: MANUAL STOP CALLED");
                break;
            case MANM_SAMPLING_STOP_CAUSE_ERROR:
                string_sprintf(&tempBuffer[0], "SAMPLING STOP CAUSE: ERROR DETECTED");
                break;
        }
        file_write_line(fd, &tempBuffer[0]);
        
        switch(finalData.error)
        {
            case MANM_SAMPLING_ERROR_NONE:
                string_sprintf(&tempBuffer[0], "SAMPLING COMPLETED REGULARLY");
                break;
            default:
                string_sprintf(&tempBuffer[0], "SAMPLING STOPPED DUE TO UNKNOWN ERROR");
                break;
        }
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
	if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeFinalData: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t Manm_report_writeRunningPaused(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("Manm_report_writeRunningPaused");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        /* TODO Manm_reportWriteLine use this define */
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - SAMPLING PAUSE", 
                                Manm_report_getTag(MANM_REPORT_TAG_RUNNING_PAUSE),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeRunningPaused: %d", returnValue);
    }	
	return returnValue;
}

typedef struct MANM_SamplingData_s
{
    MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_t measureUnit;
    float t_cont; /* temperatura contatore */
    float l;      /* litri campionati attualmente */
    float p_d;    /* pressione differenziale */
    float p_s;    /* pressione statica */
    float t_c;    /* temperatura condotto */
    float v;      /* velocit� del flusso */
}MANM_SamplingData_t;

STD_RETURN_t Manm_GetSamplingData(MANM_SamplingData_t* samplingData)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(samplingData != NULL)
    {
        /* TODO */
        MANM_SamplingData_t sd = 
        {
            MANM_OOP_REMOTE_SBRSCREEN_MEASURE_UNIT_MMH20,
            1.0f,
            2.0f,
            3.0f,
            4.0f,
            5.0f,
            6.0f
        };
        
        memory_copy(samplingData, &sd, sizeof(MANM_SamplingData_t));
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_GetSamplingData: %d", returnValue);
    }
    return returnValue;
}

STD_RETURN_t Manm_report_write5MinutesData(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
    MANM_SamplingData_t samplingData;
	sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("Manm_report_write5MinutesData");
		
	returnValue = Manm_GetSamplingData(&samplingData);	
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        /* TODO Manm_reportWriteLine use this define */
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - SAMPLING DATA", 
                                Manm_report_getTag(MANM_REPORT_TAG_FIVE_MINUTES_DATA),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0],"%s; t_cond = %.1f; l = %.2f;", Manm_report_getMeasureUnit(samplingData.measureUnit), samplingData.t_cont, samplingData.l);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0],"p_d = %.2f; p_s = %.2f; t_c = %.2f; v = %.2f", samplingData.p_d, samplingData.p_s, samplingData.t_c, samplingData.v);
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_write5MinutesData: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t Manm_report_writeDelayedResumed(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("Manm_report_writeDelayedResumed");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        /* TODO Manm_reportWriteLine use this define */
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - DELAYED START RESUMED", 
                                Manm_report_getTag(MANM_REPORT_TAG_DELAYED_RESUME),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeDelayedResumed: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t Manm_report_writeRunningResumed(void)
{
	STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("Manm_report_writeRunningResumed");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        /* TODO Manm_reportWriteLine use this define */
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - SAMPLING RESUMED", 
                                Manm_report_getTag(MANM_REPORT_TAG_RUNNING_RESUMED),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeRunningResumed: %d", returnValue);
    }	
	return returnValue;
}

STD_RETURN_t Manm_oopRemoteSrb_screen_model_startFromReadyOrStopped(void)
{
    MANM_OOPREMOTESRB_InterfaceData_t interfaceData;
    STD_RETURN_t reportReturn = Manm_report_createName(MANM_REPORT_NAME_CODE_REMOTE_SRB);

    reportReturn = Manm_OopRemoteSRBScreen_GetDataFromView(&interfaceData);

    if(reportReturn == STD_RETURN_OK)
    {
        Manm_report_writeInstrumentIds();
    }
    if(reportReturn == STD_RETURN_OK)
    {
        Manm_report_writeClientId(); /* anagrafica*/
    }
    if(reportReturn == STD_RETURN_OK)
    {
        Manm_report_writeInitData();
    }
    if(reportReturn == STD_RETURN_OK)
    {
        if(MINS_TO_SECS(interfaceData.samplingStartDelayMinutes) > 0U)
        {
            if(reportReturn == STD_RETURN_OK)
            {
                reportReturn = Manm_report_writeDelayedStart();
            }
            Manm_oopRemoteSrb_screen_model_manageSamplingTime(true, MINS_TO_SECS(interfaceData.samplingStartDelayMinutes));
            Manm_SRB_StateMachine = MANM_SRB_STATE_WAIT_START;
        }
        else
        {
            if(reportReturn == STD_RETURN_OK)
            {
                reportReturn = Manm_report_writeStart();
            }
            Manm_oopRemoteSrb_screen_model_manageSamplingTime(true, 0U);
            (void)UTIL_TimerStart(&Manm_oopRemoteSrb_timer5minutes, MANM_5_MINUTES_TO_MS);
            Manm_SRB_StateMachine = MANM_SRB_STATE_RUNNING;
        }
        /* Take trace of nozzle diameter because in pause it could be changed*/
        Manm_report_actualNozzleDiameter = interfaceData.nozzleDiameter;
        Manm_report_lastNozzleDiameter = Manm_report_actualNozzleDiameter;
    }
    if(reportReturn != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_oopRemoteSrb_screen_model_startFromReadyOrStopped: %d", reportReturn);
    }
    return reportReturn;
}

STD_RETURN_t Manm_report_writeNozzleDiameterChanged(void)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
	int fd;
	sbyte tempBuffer[MANM_TEMP_BUFFER_SIZE] = {0x00U};
	debug_print("Manm_report_writeNozzleDiameterChanged");
		
	if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_openFile(&fd);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        /* TODO Manm_reportWriteLine use this define */
        string_sprintf(&tempBuffer[0], 
                                "@%s %02d/%02d/%04d - %02d:%02d:%02d - NOZZLE DIAMETER CHANGED", 
                                Manm_report_getTag(MANM_REPORT_TAG_NOZZLE_DIAMETER_CHANGED),
                                datarioGiorno, datarioMese, datarioAnno, datarioOre, datarioMinuti, datarioSecondi);
        file_write_line(fd, &tempBuffer[0]);
        string_sprintf(&tempBuffer[0], "NOZZLE DIAMETER MM: FROM %d TO %d", 
                        Manm_report_getNozzleDiameter(Manm_report_lastNozzleDiameter), 
                        Manm_report_getNozzleDiameter(Manm_report_actualNozzleDiameter)
        );
        file_write_line(fd, &tempBuffer[0]);
    }
    
    if(returnValue == STD_RETURN_OK)
    {
        returnValue = Manm_report_closeFile(&fd);
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_report_writeNozzleDiameterChanged: %d", returnValue);
    }
    return returnValue;
}

ulong Manm_SampleTimeInMinutes(void)
{
    return (Manm_SamplingTime.minutes + (Manm_SamplingTime.hours * 60U) + (Manm_SamplingTime.days * 1440U));
}

ulong Manm_SampleVolumeInLiters(void)
{
    /* TODO implements */
    return (ulong)0U;
}

STD_RETURN_t Manm_ReportGetStoCauseAndError(MANM_SAMPLING_STOP_CAUSE_t* stopCause, MANM_SAMPLING_ERROR_t* reportError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    
    if((stopCause != NULL) && (reportError != NULL))
    {
        *stopCause = Manm_SamplingStopCause;
        *reportError = Manm_SamplingError;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_ReportGetStoCauseAndError: %d", returnValue);
    }
    
    return returnValue;
}

STD_RETURN_t Manm_AreSamplingTerminationConditionReached(ubyte* reached, MANM_SAMPLING_STOP_CAUSE_t* stopCause, MANM_SAMPLING_ERROR_t* samplingError)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    MANM_OOPREMOTESRB_InterfaceData_t interfaceData; 
    if((reached != NULL) && (stopCause != NULL))
    {
        returnValue = Manm_OopRemoteSRBScreen_GetDataFromView(&interfaceData);
        
        if(returnValue == STD_RETURN_OK)
        {
            switch(interfaceData.samplingType)
            {
                case MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_TIME:
                    if(Manm_SampleTimeInMinutes() >= interfaceData.time)
                    {
                        *reached = true;
                        *stopCause = MANM_SAMPLING_STOP_CAUSE_TIME;
                        *samplingError = MANM_SAMPLING_ERROR_NONE;
                    }
                    else
                    {
                        *reached = false;
                    }
                    break;
                case MANM_OOP_REMOTE_SBRSCREEN_ISOKINETIC_SAMPLING_VOLUME:
                    if(Manm_SampleVolumeInLiters() >= interfaceData.volume)
                    {
                        *reached = true;
                        *stopCause = MANM_SAMPLING_STOP_CAUSE_VOLUME;
                        *samplingError = MANM_SAMPLING_ERROR_NONE;
                    }
                    else
                    {
                        *reached = false;
                    }
                    break;
                default:
                    returnValue = STD_RETURN_ERROR_SAMPLING_TYPE_UNKNOWN;
                    break;
            }
        }
        
        /* Manage ERRORs HERE */
        /* TODO */
        //Manm_SamplingError = MANM_SAMPLING_ERROR_NONE;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    
    if(returnValue != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_AreSamplingTerminationConditionReached: %d", returnValue);
    }
    
    return returnValue;
}

void Manm_oopRemoteSrb_screen_model(ulong execTimeMs)
{
    STD_RETURN_t reportReturn = STD_RETURN_OK;
    ubyte isExpired; 
    ubyte samplingTerminationCondition;
    
    (void)UTIL_TimerIncrement(&Manm_oopRemoteSrb_timerConnect, execTimeMs);
#ifdef MANM_DEMO
    (void)UTIL_TimerIncrement(&Manm_oopRemoteSrb_timerDataDemo, execTimeMs);
#endif 
    (void)UTIL_TimerIncrement(&Manm_oopRemoteSrb_timer5minutes, execTimeMs);   
    
#ifdef MANM_DEMO
    (void)UTIL_TimerIsExpired(&Manm_oopRemoteSrb_timerDataDemo, &isExpired);
    if(isExpired != false)
    {
        (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timerDataDemo);
        /* TODO setup message to set in the buffer */
        /* Manm_oopRemoteSrbScreen_lastDataMessage */
        FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_DATA_ARRIVED);
        (void)UTIL_TimerStart(&Manm_oopRemoteSrb_timerDataDemo, MANM_OOP_REMOTESRB_SCREEN_TIMER_DATA_DEMO_MS);
    }
#endif

    switch(Manm_SRB_StateMachine)
    {
        case MANM_SRB_STATE_INIT:
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_CONNECT_TO_SRB) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_CONNECT_TO_SRB in MANM_SRB_STATE_INIT");
                (void)UTIL_TimerStart(&Manm_oopRemoteSrb_timerConnect, MANM_OOP_REMOTESRB_SCREEN_CONNECT_TO_SRB_TIMEOUT_MS);
                Manm_SRB_StateMachine = MANM_SRB_STATE_CONNECTING_SRB;
            }
            break;
            /* ******************************************************/
        case MANM_SRB_STATE_CONNECTING_SRB:
#ifdef MANM_DEMO
            (void)UTIL_TimerIsExpired(&Manm_oopRemoteSrb_timerConnect, &isExpired);
            if(isExpired != false)
            {
                (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timerConnect);
                
                /* create fake setup message */
                MANM_SRB_SetupMessage_t setupMessageFromSRB = 
                {
                    /* MANM_DUCT_SHAPE_t ductType;  */ MANM_DUCT_SHAPE_CIRCLE,
                    /* ulong ductDiameter;          */ 1,
                    /* ulong maxSide;               */ 2,
                    /* ulong minSide;               */ 3,
                    /* ulong kappa;                 */ 4,
                    /* ulong density;               */ 5,
                    /* ulong condensate;            */ 6,
                    /* float barometric;            */ 7.0f
                };
                /* setup message to set in the buffer */
                memory_copy(
                    (ubyte*)&Manm_oopRemoteSrbScreen_lastSetupMessage, 
                    (ubyte*)&setupMessageFromSRB, 
                    sizeof(MANM_SRB_SetupMessage_t)
                );
                
                /* Manm_oopRemoteSrbScreen_lastSetupMessage */
                FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_CONNECTION_OK);
                Manm_SRB_StateMachine = MANM_SRB_STATE_CONNECTED_READY;
                (void)UTIL_TimerStart(&Manm_oopRemoteSrb_timerDataDemo, MANM_OOP_REMOTESRB_SCREEN_TIMER_FIRST_DATA_DEMO_MS);
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_CONNECTION_OK");
            }
#else
            /* TODO MANAGE THE REAL SRB */
#endif /* #ifdef MANM_DEMO */           
            break;
            /* ******************************************************/
        case MANM_SRB_STATE_CONNECTED_READY:
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB in MANM_SRB_STATE_CONNECTED_READY");
#ifdef MANM_DEMO
                (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timerDataDemo);
#else
        
#endif /* #ifdef MANM_DEMO */
                Manm_oopRemoteSrb_screen_model_manageSamplingTime(true, 0U);
                Manm_SRB_StateMachine = MANM_SRB_STATE_INIT;
            }
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_START) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_START in MANM_SRB_STATE_CONNECTED_READY");
                
                reportReturn = Manm_oopRemoteSrb_screen_model_startFromReadyOrStopped();
            }
            break;
            /* ******************************************************/
        case MANM_SRB_STATE_WAIT_START:
            Manm_oopRemoteSrb_screen_model_manageSamplingTime(false, 0); /* manage timer, with true should be called previously */
        
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB in MANM_SRB_STATE_WAIT_START");
#ifdef MANM_DEMO
                (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timerDataDemo);
#else
        
#endif /* #ifdef MANM_DEMO */
                Manm_oopRemoteSrb_screen_model_manageSamplingTime(true, 0U);
                Manm_SRB_StateMachine = MANM_SRB_STATE_INIT;
            }
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_PAUSE) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_PAUSE in MANM_SRB_STATE_WAIT_START");
                reportReturn = Manm_report_writeDelayedPaused();
                Manm_SRB_StateMachine = MANM_SRB_STATE_PAUSED;
            }
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_STOP) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_STOP in MANM_SRB_STATE_WAIT_START");
                Manm_SamplingStopCause = MANM_SAMPLING_STOP_CAUSE_MANUAL;
                Manm_SamplingError = MANM_SAMPLING_ERROR_NONE;
                reportReturn = Manm_report_writeFinalData();
                Manm_SRB_StateMachine = MANM_SRB_STATE_STOPPED;
                Manm_samplingTerminated_screen_enterScreen(true);
            }
            
            if(Manm_oopRemoteSrb_screen_model_manageSamplingTimeGetStartDelayRemained() > 0)
            {
                ; /* Nothing wait */
            }
            else
            {
                reportReturn = Manm_report_writeStart();
                
                (void)UTIL_TimerStart(&Manm_oopRemoteSrb_timer5minutes, MANM_5_MINUTES_TO_MS);
                
                Manm_SRB_StateMachine = MANM_SRB_STATE_RUNNING;
            }            
            break;
        case MANM_SRB_STATE_RUNNING:
            Manm_oopRemoteSrb_screen_model_manageSamplingTime(false, 0); /* manage timer, with true should be called previously */
        
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB in MANM_SRB_STATE_RUNNING");
#ifdef MANM_DEMO
                (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timerDataDemo);
#else
        
#endif /* #ifdef MANM_DEMO */
                Manm_oopRemoteSrb_screen_model_manageSamplingTime(true, 0U);
                Manm_SRB_StateMachine = MANM_SRB_STATE_INIT;
            }
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_PAUSE) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_PAUSE in MANM_SRB_STATE_RUNNING");
                reportReturn = Manm_report_writeRunningPaused();
                (void)UTIL_TimerPause(&Manm_oopRemoteSrb_timer5minutes);
                Manm_SRB_StateMachine = MANM_SRB_STATE_PAUSED;
            }
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_STOP) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_STOP in MANM_SRB_STATE_RUNNING");
                Manm_SamplingStopCause = MANM_SAMPLING_STOP_CAUSE_MANUAL;
                Manm_SamplingError = MANM_SAMPLING_ERROR_NONE;
                reportReturn = Manm_report_writeFinalData();
                (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timer5minutes);
                Manm_SRB_StateMachine = MANM_SRB_STATE_STOPPED;
                Manm_samplingTerminated_screen_enterScreen(true);
            }
            /* manage 5 minutes data */
            (void)UTIL_TimerIsExpired(&Manm_oopRemoteSrb_timer5minutes, &isExpired);
            if(isExpired != false)
            {
                (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timer5minutes);                
                Manm_report_write5MinutesData();
                (void)UTIL_TimerStart(&Manm_oopRemoteSrb_timer5minutes, MANM_5_MINUTES_TO_MS);
            }
            
            reportReturn = Manm_AreSamplingTerminationConditionReached(&samplingTerminationCondition, &Manm_SamplingStopCause, &Manm_SamplingError);
            if(reportReturn == STD_RETURN_OK)
            {
                if(samplingTerminationCondition != false)
                {
                    debug_print("TERMINATION CONDITION REACHED in MANM_SRB_STATE_RUNNING");
                    reportReturn = Manm_report_writeFinalData();
                    (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timer5minutes);
                    FLAG_Set(FLAG_OOP_REMOTESRB_SCREEN_EVENT_SRB_AUTOMATIC_STOP_CONDITION_REACHED);
                    Manm_SRB_StateMachine = MANM_SRB_STATE_STOPPED;
                    Manm_samplingTerminated_screen_enterScreen(false);
                }
            }
            break;
            /* ******************************************************/
        case MANM_SRB_STATE_PAUSED:
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB in MANM_SRB_STATE_PAUSED");
#ifdef MANM_DEMO
                (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timerDataDemo);
#else
        
#endif /* #ifdef MANM_DEMO */
                Manm_oopRemoteSrb_screen_model_manageSamplingTime(true, 0U);
                Manm_SRB_StateMachine = MANM_SRB_STATE_INIT;
            }
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_START) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_START in MANM_SRB_STATE_PAUSED");

                /* Check nozzle change, it could occurs only here, write only if a start is called */
                MANM_OOPREMOTESRB_InterfaceData_t interfaceData;
                reportReturn = Manm_OopRemoteSRBScreen_GetDataFromView(&interfaceData);
                
                if(reportReturn == STD_RETURN_OK)
                {
                    Manm_report_actualNozzleDiameter = interfaceData.nozzleDiameter;
                    if(Manm_report_actualNozzleDiameter != Manm_report_lastNozzleDiameter)
                    {
                        reportReturn = Manm_report_writeNozzleDiameterChanged();
                    }
                    Manm_report_lastNozzleDiameter = Manm_report_actualNozzleDiameter; /* set new actual nozzle */
                }

                if(reportReturn == STD_RETURN_OK)
                {
                    if(Manm_oopRemoteSrb_screen_model_manageSamplingTimeGetStartDelayRemained() > 0)
                    {
                        reportReturn = Manm_report_writeDelayedResumed();
                        Manm_SRB_StateMachine = MANM_SRB_STATE_WAIT_START;
                    }
                    else
                    {
                        reportReturn = Manm_report_writeRunningResumed();
                        (void)UTIL_TimerResume(&Manm_oopRemoteSrb_timer5minutes);
                        Manm_SRB_StateMachine = MANM_SRB_STATE_RUNNING;
                    } 
                }
            }
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_STOP) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_STOP in MANM_SRB_STATE_PAUSED");
                Manm_SamplingStopCause = MANM_SAMPLING_STOP_CAUSE_MANUAL;
                Manm_SamplingError = MANM_SAMPLING_ERROR_NONE;
                reportReturn = Manm_report_writeFinalData();
                (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timer5minutes);
                Manm_SRB_StateMachine = MANM_SRB_STATE_STOPPED;
                Manm_samplingTerminated_screen_enterScreen(true);
            }
            break;
            /* ******************************************************/
        case MANM_SRB_STATE_STOPPED:
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_DISCONNECT_FROM_SRB in MANM_SRB_STATE_STOPPED");
#ifdef MANM_DEMO
                (void)UTIL_TimerStop(&Manm_oopRemoteSrb_timerDataDemo);
#else
        
#endif /* #ifdef MANM_DEMO */
                Manm_oopRemoteSrb_screen_model_manageSamplingTime(true, 0U);
                Manm_SRB_StateMachine = MANM_SRB_STATE_INIT;
            }
            if(FLAG_GetAndReset(FLAG_OOP_REMOTESRB_SCREEN_COMMAND_START) != FALSE)
            {
                debug_print("FLAG_OOP_REMOTESRB_SCREEN_COMMAND_START in MANM_SRB_STATE_STOPPED");
                
                reportReturn = Manm_oopRemoteSrb_screen_model_startFromReadyOrStopped();
            }
            break;
            /* ******************************************************/
        case MANM_SRB_STATE_ERROR:
        
            break;
        default:
            break;
    }
    if(reportReturn != STD_RETURN_OK)
    {
        debug_print("[ERRO] Manm_oopRemoteSrb_screen_model: %d", reportReturn);
    }
}

void Manm_samplingTerminated_screen_enterScreen(ubyte manualEnter)
{
    samplingTerminatedScreen_enterCommand = true;
    if(manualEnter != false)
    {
        UTIL_TimerStart(&Manm_SamplingTerminatedScreen_TimeInScreen, 5000U);
    }
}

void Manm_samplingTerminated_screen_model_init(void)
{
    UTIL_TimerInit(&Manm_SamplingTerminatedScreen_TimeInScreen);
}

void Manm_samplingTerminated_screen_model(ulong execTimeMs)
{
    ubyte isExpired;
    
    UTIL_TimerIncrement(&Manm_SamplingTerminatedScreen_TimeInScreen, execTimeMs);

    /* manual stop */
    (void)UTIL_TimerIsExpired(&Manm_SamplingTerminatedScreen_TimeInScreen, &isExpired);
    if(isExpired != false)
    {
        (void)UTIL_TimerStop(&Manm_SamplingTerminatedScreen_TimeInScreen);
        (void)UTIL_TimerInit(&Manm_SamplingTerminatedScreen_TimeInScreen);
        samplingTerminatedScreen_goBackToSourcePageCommand = true;
        /* TODO DOESN'T WORK */
        //graphic_change_HMI(Manm_samplingTerminatedScreen_SourceHMI_ID, Manm_samplingTerminatedScreen_SourceHMI_Page);
    }

    if(FLAG_GetAndReset(FLAG_SAMPLING_TERMINATED_SCREEN_BUTTON_CONTINUE) != FALSE)
    {
        (void)UTIL_TimerStop(&Manm_SamplingTerminatedScreen_TimeInScreen);
        (void)UTIL_TimerInit(&Manm_SamplingTerminatedScreen_TimeInScreen);
        samplingTerminatedScreen_goBackToSourcePageCommand = true;
        /* TODO DOESN'T WORK */
        //graphic_change_HMI(Manm_samplingTerminatedScreen_SourceHMI_ID, Manm_samplingTerminatedScreen_SourceHMI_Page);
    }
}

void Manm_memory_screen_model_init(void)
{

}

void Manm_memory_screen_model(ulong execTimeMs)
{

}

void Manm_memory_erase_export_progress_screen_model_init(void)
{
    UTIL_TimerInit(&Manm_MemoryEraseExportProgressScreen_TimeInScreen);
    UTIL_TimerInit(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile);
}

#define MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_MANIPULATION_TIME_MS 100U

void Manm_memory_erase_export_progress_screen_model(ulong execTimeMs)
{
    ubyte isExpired;
    ulong listSize;
    sbyte* fileNamePtr = NULL;
    static ulong fileIndex = 0;
    
    UTIL_TimerIncrement(&Manm_MemoryEraseExportProgressScreen_TimeInScreen, execTimeMs);
    UTIL_TimerIncrement(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile, execTimeMs);
    
    (void)UTIL_TimerIsExpired(&Manm_MemoryEraseExportProgressScreen_TimeInScreen, &isExpired);
    if(isExpired != false)
    {
        debug_print("Manm_memory_erase_export_progress_screen_model TIMER EXPIRED");
        (void)UTIL_TimerStop(&Manm_MemoryEraseExportProgressScreen_TimeInScreen);
        (void)UTIL_TimerInit(&Manm_MemoryEraseExportProgressScreen_TimeInScreen);
        memoryEraseExportProgressScreen_goBack = true;
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_COMMAND_EXPORT_ALL) != false)
    {
        debug_print("Manm_memory_erase_export_progress_screen_model COMMAND_EXPORT_ALL");
        fileIndex = 0;
        (void)UTIL_TimerStart(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile, MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_MANIPULATION_TIME_MS);
    }
    
    if(FLAG_GetAndReset(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_COMMAND_ERASE_ALL) != false)
    {
        debug_print("Manm_memory_erase_export_progress_screen_model COMMAND_ERASE_ALL");
        fileIndex = 0;
        (void)UTIL_TimerStart(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile, MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_MANIPULATION_TIME_MS);
    }

    if(FLAG_GetAndReset(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_COMMAND_EXIT_SCREEN) != false)
    {
        debug_print("Manm_memory_erase_export_progress_screen_model COMMAND_EXIT_SCREEN");
        (void)UTIL_TimerStop(&Manm_MemoryEraseExportProgressScreen_TimeInScreen);
        (void)UTIL_TimerInit(&Manm_MemoryEraseExportProgressScreen_TimeInScreen);
        (void)UTIL_TimerStop(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile);
        (void)UTIL_TimerInit(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile);
        memoryEraseExportProgressScreen_goBack = true;
    }
    
    (void)UTIL_TimerIsExpired(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile, &isExpired);
    if(isExpired != false)
    {
        (void)UTIL_TimerStop(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile);
        (void)UTIL_TimerInit(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile);
        
        if(Manm_MemoryScreen_FileListId >= 0)
        {
            listSize = list_size(Manm_MemoryScreen_FileListId);
            /* List exists*/
            (void)Manm_ManageMemoryEraseExportProgressScreen_SetProgressBar((((float)fileIndex) * 100.0f) / ((float)listSize));
            
            if(fileIndex < listSize)
            {
                fileNamePtr = list_item_at(Manm_MemoryScreen_FileListId, fileIndex);
                
                (void)Manm_ManageMemoryEraseExportProgressScreen_SetMessage(fileNamePtr, Manm_MemoryOperation, (fileIndex + 1U), listSize);
                
                switch(Manm_MemoryOperation)
                {
                    case MANM_MEMORY_OPERATION_ERASE:
                        /* delete this file */
                        if(file_remove(fileNamePtr, Manm_reportLocationSave) != 0U)
                        {
                            debug_print("[ERRO] Error deleting file: %s", fileNamePtr);
                        }
                        else
                        {
                            debug_print("removed file: %s", fileNamePtr);
                        }
                        debug_print("deleted file: %s", fileNamePtr);
                        break;
                    case MANM_MEMORY_OPERATION_EXPORT:
                        /* copy this file */
                        if(file_copy(fileNamePtr, Manm_reportLocationSave, fileNamePtr, Manm_reportLocationExport) < 0)
                        {
                            debug_print("[ERRO] Error exporting file: %s", fileNamePtr);
                        }
                        else
                        {
                            debug_print("exported file: %s", fileNamePtr);
                        }
                        break;
                    default:
                        break;
                }
                
                fileIndex++;
                
                (void)UTIL_TimerStart(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile, MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_MANIPULATION_TIME_MS);
            }
            else
            {
                fileIndex = 0;
                (void)UTIL_TimerStop(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile);
                (void)UTIL_TimerInit(&Manm_MemoryEraseExportProgressScreen_TimerManipulateEachFile);
                
                
                switch(Manm_MemoryOperation)
                {
                    case MANM_MEMORY_OPERATION_ERASE:
                        debug_print("erase complete");
                        FLAG_Set(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_EVENT_ERASE_ALL);
                        break;
                    case MANM_MEMORY_OPERATION_EXPORT:
                        debug_print("export complete");
                        FLAG_Set(FLAG_MEMORY_ERASE_EXPORT_PROGRESS_SCREEN_EVENT_EXPORT_ALL);
                        break;
                    default:
                        break;
                }
                
                (void)UTIL_TimerStart(&Manm_MemoryEraseExportProgressScreen_TimeInScreen, 5000U);
            }
        }
        else
        {
            debug_print("Manm_memory_erase_export_progress_screen_model LIST DOESN'T EXISTS");
        }
    }
}

void Manm_memory_report_screen_model_init(void)
{

}

void Manm_memory_report_screen_model(ulong execTimeMs)
{

}

void MANM_thread()
{
    float execTime;
    /* Init thread code */
    Manm_main_screen_model_init();
    Manm_oop_screen_model_init();
    Manm_oopRemoteSrb_screen_model_init();
    Manm_samplingTerminated_screen_model_init();
    Manm_memory_screen_model_init();
    Manm_memory_erase_export_progress_screen_model_init();
    Manm_memory_report_screen_model_init();
    
    /* End init thread code */
    while(true)
    {
        /* START DO NOT REMOVE */
        if(THREAD_CLOSING())
        {        
            return;
        }
        thread_usleep(MS_TO_US(MANM_THREAD_SLEEP_MS)); /* uS */
        /* END DO NOT REMOVE */
        Manm_ThreadRoutine++; /* Only for debug */
        
        execTime = MANM_THREAD_SLEEP_MS; /* TODO manage with debug_time_elapsed_ms debug_time_start or manage timer internally in a different way */
        
        Manm_main_screen_model(execTime);
        Manm_oop_screen_model(execTime);
        Manm_oopRemoteSrb_screen_model(execTime);
        Manm_samplingTerminated_screen_model(execTime);
        Manm_memory_screen_model(execTime);
        Manm_memory_erase_export_progress_screen_model(execTime);
        Manm_memory_report_screen_model(execTime);
    }
}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void MANM_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void MANM_shutdown()
//{
//}
