/*
 * util.h
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */

#ifndef UTIL_H_
#define UTIL_H_

/* Includes ------------------------------------------------------------------*/
#include "typedef.h"

/* Public define ------------------------------------------------------------*/
typedef struct Timer_s
{
    ulong counter;
    ulong counterLimit;
    ubyte running;
    ubyte expired;
}Timer_t;

#define MINS_TO_SECS(x) ((x) * 60U)
/* Public variables ----------------------------------------------------------*/

/* Public function prototypes -----------------------------------------------*/
void UTIL_SetFlag(ubyte* flags, ubyte flagValue, ulong flagPosition, ulong maxSize);
ubyte UTIL_GetFlag(ubyte* flags, ulong flagPosition, ulong maxSize);   
ubyte UTIL_GetFlagAndReset(ubyte* flags, ulong flagPosition, ulong maxSize);

ulong UTIL_GetBufferStringOfUshortLength(ushort* srcPtr, ulong srcPtrSize);
STD_RETURN_t UTIL_BufferFromUShortToSByte(ushort* srcPtr, ulong srcPtrSize, sbyte* destPtr, ulong destPtrSize);
STD_RETURN_t UTIL_BufferFromSbyteToUShort(sbyte* srcPtr, ulong srcPtrSize, ushort* destPtr, ulong destPtrSize);


STD_RETURN_t UTIL_TimerInit(Timer_t *timer);
STD_RETURN_t UTIL_TimerStart(Timer_t *timer, ulong timeMs);
STD_RETURN_t UTIL_TimerIsExpired(Timer_t *timer, ubyte* isExpired);
STD_RETURN_t UTIL_TimerIsRunning(Timer_t *timer, ubyte* isRunning);
STD_RETURN_t UTIL_TimerStop(Timer_t *timer);
STD_RETURN_t UTIL_TimerPause(Timer_t *timer);
STD_RETURN_t UTIL_TimerResume(Timer_t *timer);
STD_RETURN_t UTIL_TimerIncrement(Timer_t *timer, ulong timeMsElapsed);

ubyte UTIL_string_contain(sbyte* str, ulong strSize, ulong strStartIndex, sbyte* pattern, ulong patternSize);

#ifdef __cplusplus
}
#endif

#endif /* UTIL_H_ */