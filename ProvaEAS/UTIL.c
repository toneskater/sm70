

/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "UTIL.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Public define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
void UTIL_SetFlag(ubyte* flags, ubyte flagValue, ulong flagPosition, ulong maxSize)
{
  if(flagPosition < maxSize)
  {
    if(flagValue != false)
    {
      flags[flagPosition / 8] |=  (1 << (7 - (flagPosition % 8)));
    }
    else
    {
      flags[flagPosition / 8] &=  (~(1 << (7 - (flagPosition % 8))));
    }
  }
}

ubyte UTIL_GetFlag(ubyte* flags, ulong flagPosition, ulong maxSize)
{
  ubyte returnValue = false;
  
  if(flagPosition < maxSize)
  {
    if((flags[flagPosition / 8] & (1 << (7 - (flagPosition % 8)))) != 0x00)
    {
      returnValue = true;
    }
  }
  return returnValue;
}

ubyte UTIL_GetFlagAndReset(ubyte* flags, ulong flagPosition, ulong maxSize)
{
  ubyte returnValue = false;
  if(flagPosition < maxSize)
  {
    if((flags[flagPosition / 8] & (1 << (7 - (flagPosition % 8)))) != 0x00)
    {
      flags[flagPosition / 8] &=  (~(1 << (7 - (flagPosition % 8))));
      returnValue = true;
    }
  }
  return returnValue;
}

ulong UTIL_GetBufferStringOfUshortLength(ushort* srcPtr, ulong srcPtrSize)
{
    ulong returnValue = 0;
    if((srcPtr != NULL) && (srcPtrSize > 0))
    {
        while((srcPtr[returnValue] != '\0') && (returnValue < srcPtrSize))
        {
            returnValue++;
        }
    }
    //debug_print("UTIL_GetBufferStringOfUshortLength %d", returnValue);
    return returnValue;
}

STD_RETURN_t UTIL_BufferFromUShortToSByte(ushort* srcPtr, ulong srcPtrSize, sbyte* destPtr, ulong destPtrSize)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((srcPtr != NULL) && (srcPtrSize > 0) && (destPtr != NULL) && (destPtrSize > 0))
    {
        ushort* ptrLocal = srcPtr;
        int i = 0;
        do
        {
            destPtr[i] = (sbyte)srcPtr[i];
            i++;
        }
        while((i < srcPtrSize) && (i < destPtrSize) &&(srcPtr[i] != '\0'));
        //debug_print("UTIL_BufferFromUShortToSByte: %d %s", i, destPtr);
        for(; i < destPtrSize; i++)
        {
            destPtr[i] = '\0';
        }
        /*
        if(i < destPtrSize)
        {
            destPtr[i] = '\0';
        }*/
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_BufferFromSbyteToUShort(sbyte* srcPtr, ulong srcPtrSize, ushort* destPtr, ulong destPtrSize)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((srcPtr != NULL) && (srcPtrSize > 0) && (destPtr != NULL) && (destPtrSize > 0))
    {
        sbyte* ptrLocal = srcPtr;
        int i = 0;
        do
        {
            destPtr[i] = (ushort)(srcPtr[i] & 0x00FFU);
            i++;
        }
        while((i < srcPtrSize) && (i < destPtrSize) &&(srcPtr[i] != '\0'));
        //debug_print("UTIL_BufferFromSbyteToUShort: %d %s", i, srcPtr);
        for(; i < destPtrSize; i++)
        {
            destPtr[i] = '\0';
        }
        /*
        if(i < destPtrSize)
        {
            destPtr[i] = '\0';
        }*/
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}


STD_RETURN_t UTIL_TimerInit(Timer_t *timer)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        timer->counter = 0;
        timer->counterLimit = 0;
        timer->running = false;
        timer->expired = false;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerStart(Timer_t *timer, ulong timeMs)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((timer != NULL) && (timeMs > 0))
    {
        timer->counter = 0;
        timer->counterLimit = timeMs;
        timer->running = true;
        timer->expired = false;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerIsExpired(Timer_t *timer, ubyte* isExpired)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((timer != NULL) && (isExpired != NULL))
    {
        *isExpired = timer->expired;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerIsRunning(Timer_t *timer, ubyte* isRunning)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if((timer != NULL) && (isRunning != NULL))
    {
        *isRunning = timer->running;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerStop(Timer_t *timer)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        timer->running = false;
        timer->counter = 0;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerPause(Timer_t *timer)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        timer->running = false;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerResume(Timer_t *timer)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        timer->running = true;
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

STD_RETURN_t UTIL_TimerIncrement(Timer_t *timer, ulong timeMsElapsed)
{
    STD_RETURN_t returnValue = STD_RETURN_OK;
    if(timer != NULL)
    {
        if((timer->running != false) && (timer->expired == false)) /* if it is running and not expired, increase and checl if is expired */
        {
            timer->counter += timeMsElapsed;
            if(timer->counter >= timer->counterLimit)
            {
                //timer->running = false;
                timer->expired = true;
            } 
        }
       
        returnValue = STD_RETURN_OK;
    }
    else
    {
        returnValue = STD_RETURN_ERROR_PARAM;
    }
    return returnValue;
}

ubyte UTIL_string_contain(sbyte* str, ulong strSize, ulong strStartIndex, sbyte* pattern, ulong patternSize)
{
    ulong index = 0;
    ulong returnValue = false;
    if((str != NULL) && (pattern != NULL))
    {
        while(((index + strStartIndex) < strSize) && (index < patternSize) && str[(index + strStartIndex)] == pattern[index])
        {
            index++;
        }
        if(index == patternSize)
        {
            returnValue = true;
        }
        else
        {
            returnValue = false;
        }
    }
    return returnValue;
}

/*
 * Initialization.
 * This function will be executed during PLC's start up, before the first
 * execution cycle. 
 * Its name must be <section_name> followed by the string "_init". 
 */
//void UTIL_init()
//{
//}

/*
 * Normal routine.
 * This function will be executed in PLC's normal routine. 
 * Its name must be <section_name>. 
 */ 
//void UTIL()
//{
//}

/*
 * Fast routine.
 * This function will be executed during PLC's fast routine. 
 * Its name must be <section_name> followed by the string "_fast". 
 */
//void UTIL_fast()
//{
//}

/*
 * Low priority routine.
 * This function will be executed during PLC's low priority routine. 
 * Its name must be <section_name> followed by the string "_low_priority".
 */
//void UTIL_low_priority()
//{
//}

/*
 * Thread routine.
 * This function will be executed in a separate PLC's thread, allowing 
 * writing blocking code. Each blocking loop must call THREAD_LOOP() macro
 * in order to work.
 * For example:
   while (true) {
       THREAD_LOOP()
       i++;
   }
 * Its name must be <section_name> followed by the string "_thread". 
 */
//void UTIL_thread()
//{
//}

/*
 * Close.
 * This function will be executed during user's application closing event 
 * (i.e. when downloading the project to the controller). 
 * Its name must be <section_name> followed by the string "_close". 
 */
//void UTIL_close()
//{
//}

/*
 * Shutdown.
 * This function will be executed during PLC's shutdown. 
 * Its name must be <section_name> followed by the string "_shutdown". 
 * NOTE: THIS FUNCTION WILL BE EXECUTED BEFORE FIRMWARE'S SHUTDOWN SAVING 
 * EVENT, THEREFORE SUBTRACTING TIME TO THE LATTER; IF THIS FUNCTION TAKES 
 * TOO MUCH TIME EXECUTING, FIRMWARE MIGHT NOT BE ABLE TO SAVE ALL USER'S 
 * RETAINED SYMBOLS, RESULTING IN DATA LOSS.
 * USE THIS FUNCTION ONLY WITH BASIC AND FAST OPERATIONS, I.E. NOT SAVING 
 * ON FILE SYSTEM: WE STRONGLY RECOMMEND ASSIGNING VARIABLES' VALUES TO 
 * PLC'S RETAINED SYMBOLS, WHICH WILL BE AUTOMATICALLY SAVED DURING SHUTDOWN.
 */
//void UTIL_shutdown()
//{
//}
