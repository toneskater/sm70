/*
 * buzm.h
 *
 *  Created on: 19/set/2019
 *      Author: Andrea Tonello
 */
#ifndef BUZM_H
#define BUZM_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <system_interface.h>
#include <symbols.h>
#include "typedef.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define BUZM_SOUND_TICK_MS 50U
#define BUZM_SOUND_FREQUENCY_MIN 100U
#define BUZM_SOUND_FREQUENCY_MAX 14000U
#define BUZM_SOUND_DEFAULT_FREQUENCY 3000U

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
void BUZM_startBuzzer(void);
void BUZM_startBuzzerForMs(ulong timeMs);
void BUZM_startBuzzerForMsAndFrequency(ulong timeMs, ushort frequency);
void BUZM_stopBuzzer(void);

/* Public function prototypes -----------------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif // BUZM_H 
