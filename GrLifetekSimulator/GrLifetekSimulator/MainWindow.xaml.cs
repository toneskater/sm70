﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace GrLifetekSimulator
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int messageByteToReceive = 3;
        static SerialPort _serialPort = null;

        static bool resetLitriTotaliDaAvvio = false;

        static float pressioneContatore = 0;
        static float temperaturaContatore = 20;
        static float litriMinutoLettura = 0;
        static float litriTotaliDaAvvio = 0;
        static float temperaturaEsterno = 20;
        static float barometroDigitale = 1013;

        static float temperaturaFiltro = 20;
        static float temperaturaStorage = 20;


        static byte flagsByte0 = 0;
        static bool pompaRunningFlag = false;
        static bool pompaInRegolazioneFlag = false;
        static bool pressioneContatoreMontato = true;
        static bool temperaturaContatoreMontato = true;
        static bool temperaturaEsternoMontato = true;
        static bool pressioneEsternoMontato = true;
        static byte inverterOrPwmPump = 1;

        static bool temperaturaFiltroMontato = false;
        static bool temperaturaStorageMontato = false;
        static bool temperaturaFiltroValido = true;
        static bool temperaturaStorageValido = true;

        static byte flagsByte1 = 0;

        static ushort tempWarningCode;
        static ushort warningCode;
        static ushort errorCode;
        static ushort tempErrorCode;

        static bool powerSupplyPresent;

        static ushort TemperaturaEsternoRaw = 0;
        static bool temperaturaEsternoValid = false;
        static ushort BarometroDigitaleRaw = 0;
        static bool barometroDigitaleValid = false;
        static ushort PressioneContatoreRaw = 0;
        static bool pressioneContatoreValid = false;
        static ushort TemperaturaContatoreRaw = 0;
        static bool TemperaturaContatoreValid = false;
        static ushort litriMinutoLetturaRaw = 0;
        static bool litriMinutoLetturaValid = false;

        static ushort TemperaturaFiltroRaw = 0;
        static ushort TemperaturaStorageRaw = 0;

        static uint litriTotaliDaAvvioRaw = 0;

        static bool SendErrorChecksum = false;

        static uint delayReplyTimeMs = 0;
        static bool txEnable = true;
        
        static ushort pumpPercentagePowerRaw = 0;
        static float pumpPercentagePower = 0;

        static ushort cpuLoadRaw = 0;
        static float cpuLoad = 0;

        static int pompaRunningLastSecond;
        static float litriMinSetpoint = 0;

        private bool eepromIsInit;

        static Eeprom eeprom;

        const byte XBEM_START_CHARACTER = 0x7E;
        const byte XBEM_ESCAPE_CHARACTER = 0x7D;
        const byte XBEM_ESCAPE_XOR_CHARACTER = 0x20;
        const byte XBEM_SPECIAL_CHARACTER_0x11 = 0x11;
        const byte XBEM_SPECIAL_CHARACTER_0x13 = 0x13;
        const byte XBEM_PAYLOAD_SIZE_TX_RX = 2 + 128;
        const byte XBEM_INPUT_BUFFER_LENGTH = (XBEM_PAYLOAD_SIZE_TX_RX + 16);

        const int NETWORK_POSITION_START_BYTE = 0;
        const int NETWORK_POSITION_MESSAGE_LENGTH_HI_BYTE = 1;
        const int NETWORK_POSITION_MESSAGE_LENGTH_LO_BYTE = 2;
        const int NETWORK_POSITION_NODEID_BYTE = 0;
        const int NETM_POSITION_FUNCTION_BYTE = 1;
        const int NETWORK_POSITION_COMMAND_BYTE = 2;
        const int NETM_POSITION_FIRST_DATA_BYTE = 3;
        const int NETM_INIT_BYTES = 3;

        enum MOTC_TEST_VOID_COMMAND_t
        {
            MOTC_TEST_VOID_COMMAND_START = 0,
            MOTC_TEST_VOID_COMMAND_GET_DATA,
            MOTC_TEST_VOID_COMMAND_STOP,
            MOTC_TEST_VOID_COMMAND_MAX_NUMBER
        };

        public enum NETM_MEMORY_RETURN_t
        {
            NETM_MEMORY_RETURN_OK = 0,
            NETM_MEMORY_RETURN_BUSY,
            STD_RETURN_CONDITION_NOT_CORRECT,
            STD_RETURN_ERROR_PARAM,
            NETM_MEMORY_RETURN_ID_NOT_IN_LIST,
            NETM_MEMORY_RETURN_GENERAL_ERROR,
            NETM_MEMORY_RETURN_WRONG_LENGTH,
            NETM_MEMORY_RETURN_PARAM_ERROR,
            NETM_MEMORY_RETURN_MAX_NUMBER
        };
        enum MOTC_TEST_VOID_TYPE_t
        {
            MOTC_TEST_VOID_TYPE_COMPENSATED = 0,
            MOTC_TEST_VOID_TYPE_COSTANT,
            MOTC_TEST_VOID_TYPE_MAX_NUMBER
        };



        const byte FUNCTION_NOT_STANDARD = 0x60;

        const int NETWORK_NODE_ID = 1;

        static bool runningSetData = false;
        static long startSetData = 0;
        static long msRequiredToSetData = 0;

        const long MS_REQUIRED_TO_WRITE_A_BYTE = 10;

        static Thread readThread = null;
        const string defaultComFileName = "defaultComPort.txt";
        MyQueue txQueue;

        enum NETWORK_COMMAND_t
        {
            /* 0x00 */ NETWORK_COMMAND_KEEP_ALIVE = 0,
            /* 0x01 */ NETWORK_COMMAND_GET_EEPROM_VERSION,
            /* 0x02 */ NETWORK_COMMAND_GET_ALL_DATA,
            /* 0x03 */ NETWORK_COMMAND_SET_PUMP,
            /* 0x04 */ NETWORK_COMMAND_GET_ERROR_CODE,
            /* 0x05 */ NETWORK_COMMAND_GET_WARNING_CODE,
            /* 0x06 */ NETWORK_COMMAND_CALIBRATION_DATE,
            /* 0x07 */ NETWORK_COMMAND_GET_MEMORY_DATA,
            /* 0x08 */ NETWORK_COMMAND_SET_MEMORY_DATA,
            /* 0x09 */ NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING,
            /* 0x0A */ NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER,
            /* 0x0B */ NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER,
            /* 0x0C */ NETWORK_COMMAND_CLEAR_EEPROM,
            /* 0x0D */ NETWORK_COMMAND_CPU_LOAD,
            /* 0x0E */ NETWORK_COMMAND_KILL_ME,
            /* 0x0F */ NETWORK_COMMAND_GET_DEBUG_DATA,
            /* 0x10 */ NETWORK_COMMAND_RESET,
            /* 0x11 */ NETWORK_COMMAND_VOID,
            NETWORK_COMMAND_MAX_NUMBER
        };

        bool programEnvEnable = false;
        bool programP10Enable = false;
        bool programDucEnable = false;
        bool programTsbEnable = false;
        bool programSrbEnable = false;

        public MainWindow()
        {
            InitializeComponent();

            txQueue = new MyQueue();

            temperaturaEsternoTextBox.Text = "" + temperaturaEsterno;
            barometroTextBox.Text = "" + barometroDigitale;
            pressioneContatoreTextBox.Text = "" + pressioneContatore;
            litriMinutoLetturaTextBox.Text = "" + litriMinutoLettura;
            litriMinutoLetturaTextBox.Background = Brushes.White;
            litriDaAvvioTextBox.Text = "" + litriTotaliDaAvvio;
            temperaturaContatoreTextBox.Text = "" + temperaturaContatore;
            delayReplyTimeMsTextBox.Text = "" + delayReplyTimeMs;



            temperaturaFiltroTextBox.Text = "" + temperaturaFiltro;
            temperaturaFiltroTextBox.Background = Brushes.White;
            temperaturaStorageTextBox.Text = "" + temperaturaStorage;
            temperaturaStorageTextBox.Background = Brushes.White;

            temperaturaFiltroMontato = false;
            MontatoTemperaturaFiltroCheckBox.IsChecked = temperaturaFiltroMontato;

            temperaturaStorageMontato = false;
            MontatoTemperaturaStorageCheckBox.IsChecked = temperaturaStorageMontato;

            temperaturaFiltroValido = true;
            ValidoTemperaturaFiltroCheckBox.IsChecked = temperaturaFiltroValido;

            temperaturaStorageValido = true;
            ValidoTemperaturaStorageCheckBox.IsChecked = temperaturaStorageValido;

            pompaRunningLastSecond = 0;
            PompaInRegolazioneCheckBox.IsEnabled = false;
            PompaInRegolazioneCheckBox.IsChecked = false;

            inverterOrPwmPumpComboBox.Items.Add("Pwm");
            inverterOrPwmPumpComboBox.Items.Add("Inverter");
            inverterOrPwmPumpComboBox.SelectedIndex = 1;
            inverterOrPwmPump = (byte)inverterOrPwmPumpComboBox.SelectedIndex;

            eeprom = new Eeprom();

            byte[] swVersionSimulator = new byte[4];
            swVersionSimulator[0] = 0x00 + 0xA0;
            swVersionSimulator[1] = 0x01;
            swVersionSimulator[2] = 0x00;
            swVersionSimulator[3] = 0x09;

            String idStringString = "";
            byte[] idString = eeprom.getDataFromId(EepromIds.EEPM_ID_t.EEPM_PRODUCT_SERIAL_NUMBER);
            for(int i = 0; i < idString.Length; i++)
            {
                idStringString += "" + ((char)(idString[i]));
            }
            serialNumberTextBox.Text = idStringString;
            eeprom.setDataFromId(EepromIds.EEPM_ID_t.EEPM_PRODUCT_SERIAL_NUMBER,idString);

            eeprom.setDataFromId(EepromIds.EEPM_ID_t.EEPM_SW_VERSION_NUMBER, swVersionSimulator);

            String lastComPort = "";
            if(File.Exists(defaultComFileName) != false)
            {
                string[] lines = System.IO.File.ReadAllLines(defaultComFileName);
                if(lines.Length > 0)
                {
                    lastComPort = lines[0];
                }
            }

            int defIndex = 0;

            string[] ports = SerialPort.GetPortNames();
            for(int i = 0; i < ports.Length; i++)
            {
                if(lastComPort.Equals(ports[i]) != false)
                {
                    defIndex = i;
                }
                serialPortComboBox.Items.Add(ports[i]);
            }
            if(serialPortComboBox.Items.Count > 0)
            {
                if(defIndex < serialPortComboBox.Items.Count)
                {
                    serialPortComboBox.SelectedIndex = defIndex;
                }
                else
                {
                    serialPortComboBox.SelectedIndex = 0;
                }
            }

            powerSupplyPresent = true;
            powerSupplyPresentCheckBox.IsChecked = powerSupplyPresent;
            warningCode = 0;
            errorCode = 0;

            eepromIsInit = true;
            eepromInitCheckBox.IsChecked = eepromIsInit;

            pressioneContatoreTextBox.Background = Brushes.White;
            temperaturaContatoreTextBox.Background = Brushes.White;
            temperaturaEsternoTextBox.Background = Brushes.White;
            barometroTextBox.Background = Brushes.White;

            warningCodeTextBox.Background = Brushes.White;
            warningSubCodeTextBox.Background = Brushes.White;
            errorCodeTextBox.Background = Brushes.White;
            errorSubCodeTextBox.Background = Brushes.White;

            TestVoidGetLiterLostFromStart = 0.0;
            testVoidLitersLostTextBox.Text = TestVoidGetLiterLostFromStart.ToString("F2");
            testVoidLitersLostTextBox.Background = Brushes.White;

            byte[] lastCalibration = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_LAST_CALIBRATION);
            byte[] secondLastCalibration = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_SECOND_LAST_CALIBRATION);
            byte[] firstCalibration = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_FIRST_CALIBRATION);
            DateTime today = DateTime.Now;
            if (ArrayIsZero(lastCalibration) != false)
            {
                lastCalibration[0] = (byte)((((today.Year / 1000) % 10) << 4) + (((today.Year / 100) % 10) << 0));
                lastCalibration[1] = (byte)((((today.Year / 10) % 10) << 4) + (((today.Year / 1) % 10) << 0));
                lastCalibration[2] = (byte)((((today.Month / 10) % 10) << 4) + (((today.Month / 1) % 10) << 0));
                lastCalibration[3] = (byte)((((today.Day / 10) % 10) << 4) + (((today.Day / 1) % 10) << 0));
                lastCalibration[4] = (byte)((((today.Hour / 10) % 10) << 4) + (((today.Hour / 1) % 10) << 0));
                lastCalibration[5] = (byte)((((today.Minute / 10) % 10) << 4) + (((today.Minute / 1) % 10) << 0));
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_LAST_CALIBRATION, lastCalibration);
                lastCalibrationDateTimePicker.SelectedDate = today;
            }
            else
            {
                int year = (((lastCalibration[0] & 0xF0) >> 4) * 1000) + (((lastCalibration[0] & 0x0F) >> 0) * 100) + (((lastCalibration[1] & 0xF0) >> 4) * 10) + (((lastCalibration[1] & 0x0F) >> 0) * 1);
                int month = (((lastCalibration[2] & 0xF0) >> 4) * 10) + (((lastCalibration[2] & 0x0F) >> 0) * 1);
                int day = (((lastCalibration[3] & 0xF0) >> 4) * 10) + (((lastCalibration[3] & 0x0F) >> 0) * 1);
                int hour = (((lastCalibration[4] & 0xF0) >> 4) * 10) + (((lastCalibration[4] & 0x0F) >> 0) * 1);
                int minute = (((lastCalibration[5] & 0xF0) >> 4) * 10) + (((lastCalibration[5] & 0x0F) >> 0) * 1);
                DateTime dateTimeEeprom = new DateTime(year, month, day, hour, minute, 0);
                lastCalibrationDateTimePicker.SelectedDate = dateTimeEeprom;
            }

            if (ArrayIsZero(firstCalibration) != false)
            {
                firstCalibration[0] = (byte)((((today.Year / 1000) % 10) << 4) + (((today.Year / 100) % 10) << 0));
                firstCalibration[1] = (byte)((((today.Year / 10) % 10) << 4) + (((today.Year / 1) % 10) << 0));
                firstCalibration[2] = (byte)((((today.Month / 10) % 10) << 4) + (((today.Month / 1) % 10) << 0));
                firstCalibration[3] = (byte)((((today.Day / 10) % 10) << 4) + (((today.Day / 1) % 10) << 0));
                firstCalibration[4] = (byte)((((today.Hour / 10) % 10) << 4) + (((today.Hour / 1) % 10) << 0));
                firstCalibration[5] = (byte)((((today.Minute / 10) % 10) << 4) + (((today.Minute / 1) % 10) << 0));
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_FIRST_CALIBRATION, firstCalibration);
                firstCalibrationDateTimePicker.SelectedDate = today;
            }
            else
            {
                int year = (((firstCalibration[0] & 0xF0) >> 4) * 1000) + (((firstCalibration[0] & 0x0F) >> 0) * 100) + (((firstCalibration[1] & 0xF0) >> 4) * 10) + (((firstCalibration[1] & 0x0F) >> 0) * 1);
                int month = (((firstCalibration[2] & 0xF0) >> 4) * 10) + (((firstCalibration[2] & 0x0F) >> 0) * 1);
                int day = (((firstCalibration[3] & 0xF0) >> 4) * 10) + (((firstCalibration[3] & 0x0F) >> 0) * 1);
                int hour = (((firstCalibration[4] & 0xF0) >> 4) * 10) + (((firstCalibration[4] & 0x0F) >> 0) * 1);
                int minute = (((firstCalibration[5] & 0xF0) >> 4) * 10) + (((firstCalibration[5] & 0x0F) >> 0) * 1);
                DateTime dateTimeEeprom = new DateTime(year, month, day, hour, minute, 0);
                firstCalibrationDateTimePicker.SelectedDate = dateTimeEeprom;
            }

            if (ArrayIsZero(secondLastCalibration) != false)
            {

            }
            else
            {
                int year = (((secondLastCalibration[0] & 0xF0) >> 4) * 1000) + (((secondLastCalibration[0] & 0x0F) >> 0) * 100) + (((secondLastCalibration[1] & 0xF0) >> 4) * 10) + (((secondLastCalibration[1] & 0x0F) >> 0) * 1);
                int month = (((secondLastCalibration[2] & 0xF0) >> 4) * 10) + (((secondLastCalibration[2] & 0x0F) >> 0) * 1);
                int day = (((secondLastCalibration[3] & 0xF0) >> 4) * 10) + (((secondLastCalibration[3] & 0x0F) >> 0) * 1);
                int hour = (((secondLastCalibration[4] & 0xF0) >> 4) * 10) + (((secondLastCalibration[4] & 0x0F) >> 0) * 1);
                int minute = (((secondLastCalibration[5] & 0xF0) >> 4) * 10) + (((secondLastCalibration[5] & 0x0F) >> 0) * 1);
                DateTime dateTimeEeprom = new DateTime(year, month, day, hour, minute, 0);
                secondLastCalibrationDateTimePicker.SelectedDate = dateTimeEeprom;
            }

            byte[] programEnv = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_ENVIRONMENTAL);
            byte[] programP10 = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_PM10);
            byte[] programDuc = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_DUCT);
            byte[] programTsb = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_TSB_REMOTE);
            byte[] programSrb = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_SRB_REMOTE);

            bool programEnvEnable = (programEnv[0] != 0x00);
            bool programP10Enable = (programP10[0] != 0x00);
            bool programDucEnable = (programDuc[0] != 0x00);
            bool programTsbEnable = (programTsb[0] != 0x00);
            bool programSrbEnable = (programSrb[0] != 0x00);

            programEnvironmentalCheckBox.IsChecked = programEnvEnable;
            programPm10CheckBox.IsChecked = programP10Enable;
            programDuctCheckBox.IsChecked = programDucEnable;
            programTsbRemoteCheckBox.IsChecked = programTsbEnable;
            programSrbRemoteCheckBox.IsChecked = programSrbEnable;
        }

        bool ArrayIsZero(byte[] array)
        {
            bool returnValue = true;
            for(int i = 0; i < array.Length; i++)
            {
                if(array[i] != 0)
                {
                    returnValue = false;
                }
            }
            return returnValue;
        }

        byte[] allDataCommand = Enumerable.Repeat((byte)0x00, 17).ToArray();
        byte[] flagsCommand = Enumerable.Repeat((byte)0x00, 2).ToArray();

        public static void ConvertDataToRawData()
        {
            flagsByte0 = 0;
            if (pompaRunningFlag != false)
            {
                flagsByte0 = (byte)(flagsByte0 + (0x80));
            }

            if (pompaInRegolazioneFlag != false) /* solo se la pompa sta andando può essere in regolazione */
            {
                flagsByte0 = (byte)(flagsByte0 + (0x40));
            }

            if (pressioneContatoreMontato != false)
            {
                flagsByte0 = (byte)(flagsByte0 + (0x20));
            }
            if (temperaturaContatoreMontato != false)
            {
                flagsByte0 = (byte)(flagsByte0 + (0x10));
            }
            if (temperaturaEsternoMontato != false)
            {
                flagsByte0 = (byte)(flagsByte0 + (0x08));
            }
            if (pressioneEsternoMontato != false)
            {
                flagsByte0 = (byte)(flagsByte0 + (0x04));
            }
            if (inverterOrPwmPump != 0)
            {
                flagsByte0 = (byte)(flagsByte0 + (0x02));
            }
            if (warningCode != 0x00)
            {
                flagsByte0 = (byte)(flagsByte0 + (0x01));
            }

            flagsByte1 = 0;
            if (errorCode != 0x00)
            {
                flagsByte1 = (byte)(flagsByte1 + (0x80));
            }
            if (powerSupplyPresent != false)
            {
                flagsByte1 = (byte)(flagsByte1 + (0x40));
            }
            //Battery voltage critical 0x20
            if (temperaturaFiltroMontato != false)
            {
                flagsByte1 = (byte)(flagsByte1 + (0x10));
            }
            if (temperaturaStorageMontato != false)
            {
                flagsByte1 = (byte)(flagsByte1 + (0x08));
            }

            if (pressioneContatoreValid != false && pressioneContatoreMontato != false)
            {
                PressioneContatoreRaw = ushort.Parse((Math.Round(pressioneContatore, 2) * 10).ToString());
            }
            else
            {
                PressioneContatoreRaw = 0xFFFF;
            }

            if (TemperaturaContatoreValid  != false && temperaturaContatoreMontato != false)
            {
                TemperaturaContatoreRaw = (ushort)(int.Parse((temperaturaContatore * 100).ToString()) + 10000); /* offset di 10000 = -100.00°C*/
            }
            else
            {
                TemperaturaContatoreRaw = 0xFFFF;
            }

            if (litriMinutoLetturaValid != false)
            {
                litriMinutoLetturaRaw = ushort.Parse((Math.Round(litriMinutoLettura, 2) * 100).ToString());
            }
            else
            {
                litriMinutoLetturaRaw = 0xFFFF;
            }

            if (temperaturaEsternoValid != false && temperaturaEsternoMontato != false)
            {
                TemperaturaEsternoRaw = (ushort)(int.Parse((temperaturaEsterno * 100).ToString()) + 10000); /* offset di 10000 = -100.00°C*/
            }
            else
            {
                TemperaturaEsternoRaw = 0xFFFF;
            }

            if (barometroDigitaleValid != false && pressioneEsternoMontato != false)
            {
                BarometroDigitaleRaw = ushort.Parse((Math.Round(barometroDigitale, 2) * 10).ToString()); 
            }
            else
            {
                BarometroDigitaleRaw = 0xFFFF;
            }

            if (temperaturaFiltroValido != false && temperaturaFiltroMontato != false)
            {
                TemperaturaFiltroRaw = (ushort)(int.Parse((temperaturaFiltro * 100).ToString()) + 10000); /* offset di 10000 = -100.00°C*/
            }
            else
            {
                TemperaturaFiltroRaw = 0xFFFF;
            }

            if (temperaturaStorageValido != false && temperaturaStorageMontato != false)
            {
                TemperaturaStorageRaw = (ushort)(int.Parse((temperaturaStorage * 100).ToString()) + 10000); /* offset di 10000 = -100.00°C*/
            }
            else
            {
                TemperaturaStorageRaw = 0xFFFF;
            }

            litriTotaliDaAvvioRaw = uint.Parse((Math.Round(litriTotaliDaAvvio, 2) * 100).ToString());
            if(litriTotaliDaAvvioRaw >= 0xFFFFFE) /* clamp values */
            {
                litriTotaliDaAvvioRaw = 0xFFFFFE;
            }
        }

        // Compute the MODBUS RTU CRC
        UInt16 ModRTU_CRC(byte[] buf, int len)
        {
            UInt16 crc = 0xFFFF;

            for (int pos = 0; pos < len; pos++)
            {
                crc ^= (UInt16)buf[pos];          // XOR byte into least sig. byte of crc

                for (int i = 8; i != 0; i--)
                {    // Loop over each bit
                    if ((crc & 0x0001) != 0)
                    {      // If the LSB is set
                        crc >>= 1;                    // Shift right and XOR 0xA001
                        crc ^= 0xA001;
                    }
                    else                            // Else LSB is not set
                        crc >>= 1;                    // Just shift right
                }
            }
            // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
            return crc;
        }

        int timerSilence = 5;
        bool firstFrameReceived = false;
        List<byte> listByteRx = new List<byte>();
        Random random = new Random((int)DateTime.Now.Ticks);

        public void Read()
        {
            byte[] rxBuffer = Enumerable.Repeat((byte)0x00, 1024).ToArray();
            while (true)
            {
                Thread.Sleep(1);
                simulateEepromWriteTime();
                ConvertDataToRawData();
                CreateAllDataCommandMessage();
                readDataFromSerialAndManage();
                simulatePump();
                tests();
                Write();
            }
        }

        private void tests()
        {
            leakTest();
        }

        const int testLeadDurationSec = 10;

        private void leakTest()
        {
            if (TestVoidStartRequest != false)
            {
                TestVoidStartRequest = false;
                TestVoidState = MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_INIT;
                TestVoidIsRunning = true;
            }

            if(TestVoidIsRunning != false)
            {
                TestVoidActualTick = DateTime.Now.Ticks;
                switch (TestVoidState)
                {
                    case MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_INIT:
                        TestVoidActualVoid = 0.0;
                        TestVoidGetSecondsToTheEnd = testLeadDurationSec;
                        TestVoidState = MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_REACH_VOID_SETPOINT;
                        break;
                    case MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_REACH_VOID_SETPOINT:
                        if (TestVoidActualVoid < pumpVoidSetPoint)
                        {
                            TestVoidActualVoid += 1;
                            pressioneContatore = (float)TestVoidActualVoid;
                            this.Dispatcher.Invoke(() =>
                            {
                                pressioneContatoreTextBox.Text = pressioneContatore.ToString("F2");
                            });
                            
                        }
                        else
                        {
                            TestVoidState = MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_VACUUM_REACHED;
                        }
                        break;
                    case MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_VACUUM_REACHED:
                        TestVoidState = MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_STABLE;
                        TestVoidStartTick = TestVoidActualTick;
                        break;
                    case MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_STABLE:
                        TestVoidSecondsPassed = ((TestVoidActualTick - TestVoidStartTick) / TimeSpan.TicksPerSecond);
                        if(TestVoidSecondsPassed < testLeadDurationSec)
                        {
                            TestVoidGetSecondsToTheEnd = (ushort)(testLeadDurationSec - TestVoidSecondsPassed);
                        }
                        else
                        {
                            TestVoidState = MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_FINISH;
                        }
                        break;
                    case MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_FINISH:
                        TestVoidIsRunning = false;
                        TestVoidGetSecondsToTheEnd = 0;
                        pressioneContatore = 0;
                        this.Dispatcher.Invoke(() =>
                        {
                            pressioneContatoreTextBox.Text = pressioneContatore.ToString("F2");
                        });
                        break;
                    default:
                        break;
                }
                this.Dispatcher.Invoke(() =>
                {
                    secondsToTheEndTextBox.Text = TestVoidGetSecondsToTheEnd.ToString();
                });
            }

            this.Dispatcher.Invoke(() =>
            {
                testVoidRunningTextBox.Text = (TestVoidIsRunning != false) ? "RUNNING" : "NOT RUNNING";
            });

            if (TestVoidStopRequest != false)
            {
                TestVoidStopRequest = false;
                TestVoidState = MOTC_TEST_VOID_STATE_t.MOTC_TEST_VOID_STATE_FINISH;
            }
        }

        enum MOTC_TEST_VOID_STATE_t
        {
            MOTC_TEST_VOID_STATE_INIT = 0,
            MOTC_TEST_VOID_STATE_REACH_VOID_SETPOINT,
            MOTC_TEST_VOID_STATE_VACUUM_REACHED,
            MOTC_TEST_VOID_STATE_STABLE,
            MOTC_TEST_VOID_STATE_FINISH,
            MOTC_TEST_VOID_STATE_ERROR,
            MOTC_TEST_VOID_STATE_MAX_NUMBER
        };

        private long TestVoidSecondsPassed;
        private long TestVoidStartTick;
        private long TestVoidActualTick;
        private double TestVoidActualVoid;
        private MOTC_TEST_VOID_STATE_t TestVoidState;
        private bool TestVoidStartRequest = false;
        private bool TestVoidStopRequest = false;
        private bool TestVoidIsRunning = false;
        private double TestVoidGetLiterLostFromStart;
        private ushort TestVoidGetSecondsToTheEnd;
        private double pumpVoidSetPoint;
        private MOTC_TEST_VOID_TYPE_t testType;

        private void simulateEepromWriteTime()
        {
            if (runningSetData != false)
            {
                if (((DateTime.Now.Ticks - startSetData) / TimeSpan.TicksPerMillisecond) >= msRequiredToSetData)
                {
                    runningSetData = false;
                }
            }
        }

        private void readDataFromSerialAndManage()
        {
            while (_serialPort.BytesToRead > 0)
            {
                byte[] byteArrived = new byte[1];
                _serialPort.Read(byteArrived, 0, 1);
                listByteRx.Add(byteArrived[0]);
                timerSilence = 4;
                firstFrameReceived = true;
            }

            if (firstFrameReceived != false)
            {
                if (timerSilence > 1)
                {
                    timerSilence--;
                }
                else
                {
                    /* Timer silence terminated, use the  bytes */
                    byte[] receivedBytes = listByteRx.ToArray();
                    if (receivedBytes.Length >= 2)
                    {
                        rxReceivedFct();
                        int calculatedCRC = ModRTU_CRC(receivedBytes, (int)(receivedBytes.Length - 2U));
                        int messageCRC = receivedBytes[receivedBytes.Length - 1U];
                        messageCRC = messageCRC << 8;
                        messageCRC += receivedBytes[receivedBytes.Length - 2U];

                        if (calculatedCRC == messageCRC)
                        {
                            newMessageArrivedFct(receivedBytes, receivedBytes.Take((int)(receivedBytes.Length - 2U)).ToArray());
                        }
                        else
                        {
                            ChecksumFault++;
                            ErrorDetectedLength = NumberOfByteReceived;
                            checksumFaultDetectedFct();
                        }
                    }
                    else
                    {
                        rxInvalidLengthReceivedFct();
                    }

                    firstFrameReceived = false;
                    listByteRx.Clear();
                }
            }
        }

        private void simulatePump()
        {
            double delta = 0.5;
            double deltaLiter = ((float)(random.Next(1, 5)) / 10.0);

            if (resetLitriTotaliDaAvvio != false)
            {
                resetLitriTotaliDaAvvio = false;
                litriTotaliDaAvvio = 0;
            }

            if (pompaRunningLastSecond != DateTime.Now.Second)
            {
                pompaRunningLastSecond = DateTime.Now.Second;
                if (litriMinutoLetturaValid != false)
                {
                    litriTotaliDaAvvio += litriMinutoLettura / 60;
                }

                try
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        litriDaAvvioTextBox.Text = litriTotaliDaAvvio.ToString();// your code here.
                    });
                }
                catch (Exception)
                {

                }
            }

            /* on pump power off */
            if(powerSupplyPresent == false)
            {
                litriMinSetpoint = 0;
            }

            if (litriMinSetpoint == 0)
            {
                deltaLiter = 0;
            }
            if (true != false)
            {
                if (litriMinutoLettura < (litriMinSetpoint - delta))
                {

                    litriMinutoLettura += 0.05f;
                    try
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            litriMinutoLetturaTextBox.Text = litriMinutoLettura.ToString();// your code here.
                            pompaInRegolazioneFlag = true;
                            PompaInRegolazioneCheckBox.IsChecked = true;
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                else if (litriMinutoLettura > (litriMinSetpoint + delta))
                {
                    litriMinutoLettura -= 0.05f;
                    try
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            litriMinutoLetturaTextBox.Text = litriMinutoLettura.ToString();// your code here.
                            pompaInRegolazioneFlag = true;
                            PompaInRegolazioneCheckBox.IsChecked = true;
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    double sign = random.NextDouble();
                    if (sign >= 0.5)
                    {
                        litriMinutoLettura = litriMinSetpoint + (float)deltaLiter;
                    }
                    else
                    {
                        litriMinutoLettura = litriMinSetpoint - (float)deltaLiter;
                    }
                    if (litriMinutoLettura < 0)
                    {
                        litriMinutoLettura = 0;
                    }
                    try
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            litriMinutoLetturaTextBox.Text = litriMinutoLettura.ToString();// your code here.
                            pompaInRegolazioneFlag = false;
                            PompaInRegolazioneCheckBox.IsChecked = false;
                        });
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

        private void CreateAllDataCommandMessage()
        {
            /* Pressione contatore */
            allDataCommand[0] = (byte)((PressioneContatoreRaw & 0x0000FF00) >> 8);
            allDataCommand[1] = (byte)((PressioneContatoreRaw & 0x000000FF) >> 0);

            /* Temperatura contatore */
            allDataCommand[2] = (byte)((TemperaturaContatoreRaw & 0x0000FF00) >> 8);
            allDataCommand[3] = (byte)((TemperaturaContatoreRaw & 0x000000FF) >> 0);

            /* Litri minuti lettura */
            allDataCommand[4] = (byte)((litriMinutoLetturaRaw & 0x0000FF00) >> 8);
            allDataCommand[5] = (byte)((litriMinutoLetturaRaw & 0x000000FF) >> 0);
            /* Litri totali da avvio */
            allDataCommand[6] = (byte)((litriTotaliDaAvvioRaw & 0x00FF0000) >> 16);
            allDataCommand[7] = (byte)((litriTotaliDaAvvioRaw & 0x0000FF00) >> 8);
            allDataCommand[8] = (byte)((litriTotaliDaAvvioRaw & 0x000000FF) >> 0);
            /* Sensore temperatura esterno */
            allDataCommand[9] = (byte)((TemperaturaEsternoRaw & 0x0000FF00) >> 8);
            allDataCommand[10] = (byte)((TemperaturaEsternoRaw & 0x000000FF) >> 0);
            /* Sensore barometrico digitale */
            allDataCommand[11] = (byte)((BarometroDigitaleRaw & 0x0000FF00) >> 8);
            allDataCommand[12] = (byte)((BarometroDigitaleRaw & 0x000000FF) >> 0);
            /* Sensore barometrico digitale */
            allDataCommand[13] = (byte)((TemperaturaFiltroRaw & 0x0000FF00) >> 8);
            allDataCommand[14] = (byte)((TemperaturaFiltroRaw & 0x000000FF) >> 0);
            /* Sensore barometrico digitale */
            allDataCommand[15] = (byte)((TemperaturaStorageRaw & 0x0000FF00) >> 8);
            allDataCommand[16] = (byte)((TemperaturaStorageRaw & 0x000000FF) >> 0);

            flagsCommand[0] = flagsByte0;
            flagsCommand[1] = flagsByte1;

            String str = "";
            for (int i = 0; i < allDataCommand.Length; i++)
            {
                str += allDataCommand[i].ToString("X2") + " ";
            }

            str += " - ";
            for (int i = 0; i < flagsCommand.Length; i++)
            {
                str += flagsCommand[i].ToString("X2") + " ";
            }

            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    GrDataHexTextBox.Text = str;// your code here.
                });
            }
            catch (Exception)
            {

            }
        }

        byte[] InputBuffer = new byte[2048];
        int NumberOfByteReceived = 0;
        int ErrorDetectedLength = 0;
        long ChecksumFault = 0;
        long TxCounter = 0;
        long RxCounter = 0;
        long RxInvalidLengthCounter = 0;

        List<byte> byteArrivedList = new List<byte>();

        private void checksumFaultDetectedFct()
        {
            ChecksumFault++;
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    ChecksumErrorCounterTextBox.Text = "" + ChecksumFault;
                });
            }
            catch (Exception)
            {

            }
        }

        private void txSentFct()
        {
            TxCounter++;
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    TxCounterTextBox.Text = "" + TxCounter;
                });
            }
            catch (Exception)
            {

            }
        }

        private void rxReceivedFct()
        {
            RxCounter++;
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    RxCounterTextBox.Text = "" + RxCounter;
                });
            }
            catch (Exception)
            {

            }
        }

        private void rxInvalidLengthReceivedFct()
        {
            RxInvalidLengthCounter++;
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    RxInvalidLengthCounterTextBox.Text = "" + RxInvalidLengthCounter;
                });
            }
            catch (Exception)
            {

            }
        }

        private void sendMessageFct(byte[] payload)
        {
            if (txEnable != false)
            {
                txQueue.enqueueTxMessage(payload, true);
            }
        }

        public void Write()
        {
            int i;
            if (txQueue.sizeQueueTxMessage() > 0)
            {
                byte[] messageToSend = txQueue.dequeueTxMessage();

                if (messageToSend != null)
                {
                    String timeStamp = DateTime.Now.ToString("HH:mm:ss:fff");

                    _serialPort.Write(messageToSend, 0, messageToSend.Length);
                    txSentFct();
                    String strToSend = timeStamp + " - " + messageToSend.Length + " - ";
                    String strPayload = timeStamp + " - " + (messageToSend.Length - 2) + " - ";

                    for (i = 0; i < messageToSend.Length; i++)
                    {
                        strToSend += messageToSend[i].ToString("X2") + " ";
                    }

                    for (i = 0; i < (messageToSend.Length - 2U); i++)
                    {
                        strPayload += messageToSend[i].ToString("X2") + " ";
                    }

                    try
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            RawDataHexTxTextBox.Text = strToSend;
                            PayloadTxTextBox.Text = strPayload;
                        });
                    }
                    catch (Exception)
                    {

                    }
                    //logOnFileTx(strOriginal);
                }
            }
        }

        private byte[] CalculateChecksumAndLengthXbeeProtocol(byte[] inputBuffer)
        {
            List<byte> buffer = new List<byte>();
            int i = 0;
            while (i < inputBuffer.Length)
            {
                buffer.Add(inputBuffer[i++]);
            }
            int crcCalculated = ModRTU_CRC(inputBuffer, inputBuffer.Length);
            if(SendErrorChecksum != false)
            {
                crcCalculated = (crcCalculated & 0xFFFF) + 1;
            }
            buffer.Add((byte)(crcCalculated & 0x00FF));
            buffer.Add((byte)((crcCalculated & 0xFF00) >> 8));
            return buffer.ToArray();
        }

        private void newMessageArrivedFct(byte[] originalInputBuffer, byte[] InputBuffer)
        {
            List<byte> listByte = new List<byte>();
            String timeStamp = DateTime.Now.ToString("HH:mm:ss:fff");
            String strOriginal = timeStamp + " - " + originalInputBuffer.Length + " - ";
            String strPayload = timeStamp + " - " + InputBuffer.Length + " - ";
            EepromIds.EEPM_ID_t eepm_id;
            int dataLength;
            byte[] dataBuffer = null;
            int i;
            for (i = 0; i < originalInputBuffer.Length; i++)
            {
                strOriginal += originalInputBuffer[i].ToString("X2") + " ";
            }
            for (i = 0; i < InputBuffer.Length; i++)
            {
                strPayload += InputBuffer[i].ToString("X2") + " ";
            }
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    RawDataHexRxTextBox.Text = strOriginal;
                    PayloadRxTextBox.Text = strPayload;
                });
            }
            catch (Exception)
            {

            }

            if(delayReplyTimeMs > 0)
            {
                Thread.Sleep((int)delayReplyTimeMs);
            }

            if(InputBuffer[NETWORK_POSITION_NODEID_BYTE] == 0x01) /* ID GR */
            {
                bool sendResponse = false;
                listByte.Clear();
                listByte.Add((byte)NETWORK_NODE_ID);
                listByte.Add((byte)FUNCTION_NOT_STANDARD);
                listByte.Add(InputBuffer[NETWORK_POSITION_COMMAND_BYTE]);
                if (InputBuffer[NETM_POSITION_FUNCTION_BYTE] != FUNCTION_NOT_STANDARD)
                {
                    ; /* Not managed */
                }
                else
                {
                    switch (InputBuffer[NETWORK_POSITION_COMMAND_BYTE])
                    {
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_KEEP_ALIVE: /* keep alive*/
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 0U)
                            {
                                if (Netm_IsEepromInitialized() != false)
                                {
                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                }
                                else
                                {
                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_BUSY);
                                }
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_GET_EEPROM_VERSION:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 0U)
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                listByte.Add((byte)((eeprom.getVersion() & 0xFF000000U) >> 24));
                                listByte.Add((byte)((eeprom.getVersion() & 0x00FF0000U) >> 16));
                                listByte.Add((byte)((eeprom.getVersion() & 0x0000FF00U) >> 8));
                                listByte.Add((byte)((eeprom.getVersion() & 0x000000FFU) >> 0));
                                listByte.Add((byte)((((int)(EepromIds.EEPM_ID_t.EEPM_ID_MAX_NUMBER)) & 0xFF000000U) >> 24));
                                listByte.Add((byte)((((int)(EepromIds.EEPM_ID_t.EEPM_ID_MAX_NUMBER)) & 0x00FF0000U) >> 16));
                                listByte.Add((byte)((((int)(EepromIds.EEPM_ID_t.EEPM_ID_MAX_NUMBER)) & 0x0000FF00U) >> 8));
                                listByte.Add((byte)((((int)(EepromIds.EEPM_ID_t.EEPM_ID_MAX_NUMBER)) & 0x000000FFU) >> 0));
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_GET_ALL_DATA:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 0U)
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                for (i = 0; i < flagsCommand.Length; i++)
                                {
                                    listByte.Add((byte)flagsCommand[i]);
                                }
                                for (i = 0; i < allDataCommand.Length; i++)
                                {
                                    listByte.Add((byte)allDataCommand[i]);
                                }
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_SET_PUMP:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 2U)
                            {
                                uint var = (uint)((InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 0U] << 8) + InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 1U]);
                                if(setPumpSetPoint(((float)(var)) / 100) != false)
                                {
                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                }
                                else
                                {
                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_PARAM_ERROR);
                                }
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_GET_ERROR_CODE:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 1U)
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                listByte.Add((byte)((errorCode & 0xFF00U) >> 8));
                                listByte.Add((byte)((errorCode & 0x00FFU) >> 0));
                                errorCode = 0;
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_GET_WARNING_CODE:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 1U)
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                listByte.Add((byte)((warningCode & 0xFF00U) >> 8));
                                listByte.Add((byte)((warningCode & 0x00FFU) >> 0));
                                warningCode = 0;
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_CALIBRATION_DATE:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) >= 1U)
                            {
                                switch(InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 0])
                                {
                                    case 0x00:
                                        if ((InputBuffer.Length - NETM_INIT_BYTES) == 7U)
                                        {
                                            
                                            byte[] Netm_DatetimeCalibration = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_FIRST_CALIBRATION);
                                            for(i = 0; i < Netm_DatetimeCalibration.Length; i++)
                                            {
                                                if(Netm_DatetimeCalibration[i] != 0x00)
                                                {
                                                    break;
                                                }
                                            }
                                            if(i == Netm_DatetimeCalibration.Length)
                                            {
                                                /* First calibration date is empty */
                                                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_FIRST_CALIBRATION, InputBuffer.Skip(NETM_INIT_BYTES).Take(6).ToArray());
                                            }
                                            else
                                            {
                                                /* First calibration already written */
                                            }
                                            Netm_DatetimeCalibration = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_LAST_CALIBRATION);
                                            eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_SECOND_LAST_CALIBRATION, Netm_DatetimeCalibration);
                                            eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_LAST_CALIBRATION, InputBuffer.Skip(NETM_INIT_BYTES).Take(6).ToArray());
                                            listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                        }
                                        else
                                        {
                                            listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                                        }
                                        break;
                                    case 0x01:
                                        if ((InputBuffer.Length - NETM_INIT_BYTES) == 2U)
                                        {
                                            switch (InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 1])
                                            {
                                                case 0x00:
                                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                                    dataBuffer = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_LAST_CALIBRATION);
                                                    for (i = 0; i < dataBuffer.Length; i++)
                                                    {
                                                        listByte.Add((byte)(dataBuffer[i]));
                                                    }
                                                    break;
                                                case 0x01:
                                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                                    dataBuffer = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_SECOND_LAST_CALIBRATION);
                                                    for (i = 0; i < dataBuffer.Length; i++)
                                                    {
                                                        listByte.Add((byte)(dataBuffer[i]));
                                                    }
                                                    break;
                                                case 0x02:
                                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                                    dataBuffer = eeprom.getDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_FIRST_CALIBRATION);
                                                    for (i = 0; i < dataBuffer.Length; i++)
                                                    {
                                                        listByte.Add((byte)(dataBuffer[i]));
                                                    }
                                                    break;
                                                default:
                                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_PARAM_ERROR);
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                                        }
                                        break;
                                    default:
                                        listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_PARAM_ERROR);
                                        break;
                                }
                            }
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_GET_MEMORY_DATA:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 2U)
                            {
                                if (runningSetData != false)
                                {
                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_BUSY);
                                }
                                else
                                {
                                    eepm_id = (EepromIds.EEPM_ID_t)(InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 0] + InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 1]);
                                    if (eepm_id < EepromIds.EEPM_ID_t.EEPM_ID_MAX_NUMBER)
                                    {
                                        listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                        dataBuffer = eeprom.getDataFromId(eepm_id);
                                        for (i = 0; i < dataBuffer.Length; i++)
                                        {
                                            listByte.Add((byte)(dataBuffer[i]));
                                        }
                                    }
                                    else
                                    {
                                        /* Id not in list */
                                        listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_ID_NOT_IN_LIST);
                                    }
                                }
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_SET_MEMORY_DATA:
                            if (runningSetData != false)
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_BUSY);
                            }
                            else
                            {
                                if ((InputBuffer.Length - NETM_INIT_BYTES) >= 2U)
                                {
                                    eepm_id = (EepromIds.EEPM_ID_t)(InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 0] + InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 1]);
                                    dataLength = (InputBuffer.Length - NETM_INIT_BYTES) - 2;
                                    if ((eepm_id < EepromIds.EEPM_ID_t.EEPM_ID_MAX_NUMBER) && (dataLength > 0) && (dataLength == eeprom.getDataLengthFromId(eepm_id)))
                                    {
                                        eeprom.setDataFromId(eepm_id, InputBuffer.Skip(NETM_INIT_BYTES + 2).Take(dataLength).ToArray());
                                        listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                        runningSetData = true;
                                        startSetData = DateTime.Now.Ticks;
                                        msRequiredToSetData = dataLength * MS_REQUIRED_TO_WRITE_A_BYTE;
                                    }
                                    else
                                    {
                                        listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_ID_NOT_IN_LIST);
                                    }
                                }
                                else
                                {
                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                                }
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_RESTORE_DATA_NEW_SAMPLING:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 0U)
                            {
                                resetLitriTotaliDaAvvio = true;
                                ChecksumFault = 0;
                                TxCounter = -1;
                                RxCounter = 0;
                                RxInvalidLengthCounter = 0;
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            } 
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_SET_PUMP_PERCENTAGE_POWER:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 2U)
                            {
                                pumpPercentagePowerRaw = (ushort)((InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 0U] << 8) + InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 1U]);
                                pumpPercentagePower = (float)(((float)(pumpPercentagePowerRaw)) / 100.0f);
                                if (pumpPercentagePower >= 0 && pumpPercentagePower <= 100)
                                {
                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                    dataBuffer = eeprom.getDataFromId(EepromIds.EEPM_ID_t.MOTC_PUMP_SETPOINT_MAX);
                                    float setpointMax = (float)(((float)(BitConverter.ToUInt16(dataBuffer, 0))) / 100.0f);
                                    litriMinSetpoint = (pumpPercentagePower * setpointMax) / 100.0f;
                                    if (litriMinSetpoint == 0)
                                    {
                                        /* turnoff pump */
                                        stopPompa();
                                    }
                                    else
                                    {
                                        /* turnon pump */
                                        if (pompaRunningFlag == false)
                                        {
                                            startPompa();
                                        }
                                    }
                                }
                                else
                                {
                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_PARAM_ERROR);
                                }
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_GET_PUMP_PERCENTAGE_POWER:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 0U)
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                listByte.Add((byte)((pumpPercentagePowerRaw & 0xFF00U) >> 8));
                                listByte.Add((byte)((pumpPercentagePowerRaw & 0x00FFU) >> 0));
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_CLEAR_EEPROM:
                            //throw new NotImplementedException("NETWORK_COMMAND_CALIBRATION_DATE Not implemented in simulator");
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_CPU_LOAD:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) == 0U)
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                listByte.Add((byte)((cpuLoadRaw & 0xFF00U) >> 8));
                                listByte.Add((byte)((cpuLoadRaw & 0x00FFU) >> 0));
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_KILL_ME:
                            //throw new NotImplementedException("NETWORK_COMMAND_CALIBRATION_DATE Not implemented in simulator");
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_GET_DEBUG_DATA:
                            //throw new NotImplementedException("NETWORK_COMMAND_CALIBRATION_DATE Not implemented in simulator");
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_RESET:
                            //throw new NotImplementedException("NETWORK_COMMAND_CALIBRATION_DATE Not implemented in simulator");
                            break;
                        case (byte)NETWORK_COMMAND_t.NETWORK_COMMAND_VOID:
                            if ((InputBuffer.Length - NETM_INIT_BYTES) >= 1U)
                            {
                                switch((MOTC_TEST_VOID_COMMAND_t)InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 0U])
                                {
                                    case MOTC_TEST_VOID_COMMAND_t.MOTC_TEST_VOID_COMMAND_START:
                                        if((InputBuffer.Length - NETM_INIT_BYTES) == 4U)
                                        {
                                            testType = (MOTC_TEST_VOID_TYPE_t)InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 0U];
                                            double localPumpVoidSetPoint = ((double)((InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 2U] << 8) + InputBuffer[NETM_POSITION_FIRST_DATA_BYTE + 3U])) / 10.0;
                                            switch(MOTC_TestVoidStart(localPumpVoidSetPoint, testType))
                                            {
                                                case NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK:
                                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                                    break;
                                                case NETM_MEMORY_RETURN_t.STD_RETURN_CONDITION_NOT_CORRECT:
                                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.STD_RETURN_CONDITION_NOT_CORRECT);
                                                    break;
                                                case NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_PARAM_ERROR:
                                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_PARAM_ERROR);
                                                    break;
                                                default:
                                                    listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_GENERAL_ERROR);
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                                        }
                                        break;
                                    case MOTC_TEST_VOID_COMMAND_t.MOTC_TEST_VOID_COMMAND_GET_DATA:
                                        if ((InputBuffer.Length - NETM_INIT_BYTES) == 1U)
                                        {
                                            listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);

                                            ushort literLostRaw = (ushort)(MOTC_TestVoidGetLiterLostFromStart() * 100.0);
                                            ushort secondToTheEnd = MOTC_TestVoidGetSecondsToTheEnd();
                                            ushort meterPressure = (ushort)(MOTC_TestVoidGetPressioneContatore() * 10.0);

                                            listByte.Add((byte)((literLostRaw & 0xFF00U) >> 8));
                                            listByte.Add((byte)((literLostRaw & 0x00FFU) >> 0));

                                            listByte.Add((byte)((secondToTheEnd & 0xFF00U) >> 8));
                                            listByte.Add((byte)((secondToTheEnd & 0x00FFU) >> 0));

                                            listByte.Add((byte)((meterPressure & 0xFF00U) >> 8));
                                            listByte.Add((byte)((meterPressure & 0x00FFU) >> 0));
                                        }
                                        else
                                        {
                                            listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                                        }
                                        break;
                                    case MOTC_TEST_VOID_COMMAND_t.MOTC_TEST_VOID_COMMAND_STOP:
                                        if ((InputBuffer.Length - NETM_INIT_BYTES) == 1U)
                                        {
                                            if(MOTC_TestVoidStop() != NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK)
                                            {
                                                listByte.Add((byte)NETM_MEMORY_RETURN_t.STD_RETURN_CONDITION_NOT_CORRECT);
                                            }
                                            else
                                            {
                                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK);
                                            }
                                        }
                                        else
                                        {
                                            listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                listByte.Add((byte)NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_WRONG_LENGTH);
                            }
                            sendResponse = true;
                            break;
                        default:
                            break;
                    }
                    if (sendResponse != false)
                    {
                        sendMessageFct(CalculateChecksumAndLengthXbeeProtocol(listByte.ToArray()));
                    }
                }
            }
        }

        private double MOTC_TestVoidGetPressioneContatore()
        {
            return pressioneContatore;
        }

        private ushort MOTC_TestVoidGetSecondsToTheEnd()
        {
            return TestVoidGetSecondsToTheEnd;
        }        

        private double MOTC_TestVoidGetLiterLostFromStart()
        {
            return TestVoidGetLiterLostFromStart;
        }

        private NETM_MEMORY_RETURN_t MOTC_TestVoidStop()
        {
            NETM_MEMORY_RETURN_t returnValue = NETM_MEMORY_RETURN_t.STD_RETURN_CONDITION_NOT_CORRECT;
            if (TestVoidIsRunning != false)
            {
                TestVoidStopRequest = true;
                returnValue = NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK;
            }
            return returnValue;
        }

        private NETM_MEMORY_RETURN_t MOTC_TestVoidStart(double localPumpVoidSetPoint, MOTC_TEST_VOID_TYPE_t testType)
        {
            NETM_MEMORY_RETURN_t returnValue = NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK;
            if ((localPumpVoidSetPoint >= 0) && (localPumpVoidSetPoint <= 760.0) && (testType < MOTC_TEST_VOID_TYPE_t.MOTC_TEST_VOID_TYPE_MAX_NUMBER))
            {
                if (TestVoidIsRunning != false)
                {
                    returnValue = NETM_MEMORY_RETURN_t.STD_RETURN_CONDITION_NOT_CORRECT;
                }
                else
                {
                    TestVoidStartRequest = true;
                    pumpVoidSetPoint = localPumpVoidSetPoint;
                    TestVoidGetSecondsToTheEnd = 60;
                    TestVoidGetLiterLostFromStart = 0.0f;
                    returnValue = NETM_MEMORY_RETURN_t.NETM_MEMORY_RETURN_OK;
                }
            }
            return returnValue;
        }

        private bool setPumpSetPoint(float v)
        {
            byte[] dataBufferSetPointLitersMinuteMaxValueEeprom = eeprom.getDataFromId(EepromIds.EEPM_ID_t.MOTC_PUMP_SETPOINT_MAX);
            float Invc_SetPointLitersMinuteMaxValueEeprom = (float)(((float)(BitConverter.ToUInt16(dataBufferSetPointLitersMinuteMaxValueEeprom, 0))) / 100.0f);
            byte[] dataBufferSetPointLitersMinuteMinValueEeprom = eeprom.getDataFromId(EepromIds.EEPM_ID_t.MOTC_PUMP_SETPOINT_MIN);
            float Invc_SetPointLitersMinuteMinValueEeprom = (float)(((float)(BitConverter.ToUInt16(dataBufferSetPointLitersMinuteMinValueEeprom, 0))) / 100.0f);

            bool returnValue = false;
            if(
                  ((v >= Invc_SetPointLitersMinuteMinValueEeprom) &&
                  (v <= Invc_SetPointLitersMinuteMaxValueEeprom))
                  ||
                  (v == 0U)
            )
            {
                litriMinSetpoint = v;
                if (litriMinSetpoint == 0)
                {
                    /* turnoff pump */
                    stopPompa();
                }
                else
                {
                    /* turnon pump */
                    if (pompaRunningFlag == false)
                    {
                        startPompa();
                    }
                }
                returnValue = true;
            }
            return returnValue;
        }

        private bool Netm_IsEepromInitialized()
        {
            return eepromIsInit;
        }

        private void startPompa()
        {
            pompaRunningFlag = true;
            pompaRunningLastSecond = DateTime.Now.Second;
            //litriTotaliDaAvvio = 0;
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    litriDaAvvioTextBox.Text = litriTotaliDaAvvio.ToString();// your code here.
                    PompaRunningCheckBox.IsChecked = true;
                    PompaRunningCheckBox.IsEnabled = true;
                    PompaInRegolazioneCheckBox.IsEnabled = true;
                    PompaInRegolazioneCheckBox.IsChecked = true;
                });
            }
            catch (Exception)
            {

            }
            pompaInRegolazioneFlag = true;
        }

        private void stopPompa()
        {
            pompaRunningFlag = false;
            pompaInRegolazioneFlag = true;
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    PompaRunningCheckBox.IsChecked = false;
                    PompaRunningCheckBox.IsEnabled = true;
                    PompaInRegolazioneCheckBox.IsEnabled = false;
                    PompaInRegolazioneCheckBox.IsChecked = false;
                });
            }
            catch (Exception)
            {

            }
        }

        private void PompaRunningCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            startPompa();
        }

        private void PompaRunningCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            stopPompa();
        }

        private void PompaInRegolazioneCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            pompaInRegolazioneFlag = true;
        }

        private void PompaInRegolazioneCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            pompaInRegolazioneFlag = false;
        }

        private void LitriMinutoLetturaCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            litriMinutoLetturaTextBox.IsEnabled = true;
            litriMinutoLetturaValid = true;
        }

        private void LitriMinutoLetturaCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            litriMinutoLetturaTextBox.IsEnabled = false;
            litriMinutoLetturaValid = false;
        }

        private void TemperaturaEsternoCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            temperaturaEsternoTextBox.IsEnabled = true;
            temperaturaEsternoValid = true;
        }

        private void TemperaturaEsternoCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            temperaturaEsternoTextBox.IsEnabled = false;
            temperaturaEsternoValid = false;
        }

        private void PressioneContatoreCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            pressioneContatoreTextBox.IsEnabled = true;
            pressioneContatoreValid = true;
        }

        private void PressioneContatoreCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            pressioneContatoreTextBox.IsEnabled = false;
            pressioneContatoreValid = false;
        }

        private void BarometroCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            barometroTextBox.IsEnabled = true;
            barometroDigitaleValid = true;
        }

        private void BarometroCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            barometroTextBox.IsEnabled = false;
            barometroDigitaleValid = false;
        }

        private void TemperaturaContatoreCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            temperaturaContatoreTextBox.IsEnabled = true;
            TemperaturaContatoreValid = true;
        }

        private void TemperaturaContatoreCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            temperaturaContatoreTextBox.IsEnabled = false;
            TemperaturaContatoreValid = false;
        }

        private void temperaturaEsternoTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            temperaturaEsternoTextBox.Background = Brushes.Yellow;
        }

        private void barometroTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            barometroTextBox.Background = Brushes.Yellow;
        }

        private void pressioneContatoreTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            pressioneContatoreTextBox.Background = Brushes.Yellow;
        }

        private void litriMinutoLettura_TextChanged(object sender, TextChangedEventArgs e)
        {
            litriMinutoLetturaTextBox.Background = Brushes.Yellow;
        }

        private void temperaturaEsternoTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            Console.WriteLine(" " + e.Key + " " + e.KeyStates + " " + e.SystemKey);
        }

        private void temperaturaEsternoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                try
                {
                    temperaturaEsterno = (float)Math.Round(double.Parse(temperaturaEsternoTextBox.Text), 2);
                    if(temperaturaEsterno.ToString().Equals(temperaturaEsternoTextBox.Text) == false)
                    {
                        temperaturaEsternoTextBox.Text = temperaturaEsterno.ToString();
                        temperaturaEsternoTextBox.SelectionStart = temperaturaEsternoTextBox.Text.Length;
                        temperaturaEsternoTextBox.SelectionLength = 0;
                    }
                    temperaturaEsternoTextBox.Background = Brushes.White;
                }
                catch(Exception)
                {
                    temperaturaEsternoTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void temperaturaContatoreTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    temperaturaContatore = (float)Math.Round(double.Parse(temperaturaContatoreTextBox.Text), 2);
                    temperaturaContatoreTextBox.Background = Brushes.White;
                    if (temperaturaContatore.ToString().Equals(temperaturaContatoreTextBox.Text) == false)
                    {
                        temperaturaContatoreTextBox.Text = temperaturaEsterno.ToString();
                        temperaturaContatoreTextBox.SelectionStart = temperaturaContatoreTextBox.Text.Length;
                        temperaturaContatoreTextBox.SelectionLength = 0;
                    }
                    temperaturaContatoreTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    temperaturaContatoreTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void temperaturaContatoreTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            temperaturaContatoreTextBox.Background = Brushes.Yellow;
        }

        private void barometroTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    barometroDigitale = (float)Math.Round(double.Parse(barometroTextBox.Text), 1);
                    barometroTextBox.Background = Brushes.White;
                    if (barometroDigitale.ToString().Equals(barometroTextBox.Text) == false)
                    {
                        barometroTextBox.Text = barometroDigitale.ToString();
                        barometroTextBox.SelectionStart = barometroTextBox.Text.Length;
                        barometroTextBox.SelectionLength = 0;
                    }
                }
                catch (Exception)
                {
                    barometroTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void pressioneContatoreTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    pressioneContatore = (float)Math.Round(double.Parse(pressioneContatoreTextBox.Text), 1);
                    pressioneContatoreTextBox.Background = Brushes.White;
                    if (pressioneContatore.ToString().Equals(pressioneContatoreTextBox.Text) == false)
                    {
                        pressioneContatoreTextBox.Text = pressioneContatore.ToString();
                        pressioneContatoreTextBox.SelectionStart = pressioneContatoreTextBox.Text.Length;
                        pressioneContatoreTextBox.SelectionLength = 0;
                    }
                    pressioneContatoreTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    pressioneContatoreTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void delayReplyTimeMsTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    delayReplyTimeMs = uint.Parse(delayReplyTimeMsTextBox.Text);
                    delayReplyTimeMsTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    delayReplyTimeMsTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void litriMinutoLetturaTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    litriMinutoLettura = (float)Math.Round(double.Parse(litriMinutoLetturaTextBox.Text), 2);
                    litriMinutoLetturaTextBox.Background = Brushes.White;
                    if (litriMinutoLettura.ToString().Equals(litriMinutoLetturaTextBox.Text) == false)
                    {
                        litriMinutoLetturaTextBox.Text = litriMinutoLettura.ToString();
                        litriMinutoLetturaTextBox.SelectionStart = litriMinutoLetturaTextBox.Text.Length;
                        litriMinutoLetturaTextBox.SelectionLength = 0;
                    }
                }
                catch (Exception)
                {
                    litriMinutoLetturaTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void serialNumberTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    if(serialNumberTextBox.Text.Length == 7)
                    {
                        byte[] idString = System.Text.Encoding.UTF8.GetBytes(serialNumberTextBox.Text.ToCharArray());
                        eeprom.setDataFromId(EepromIds.EEPM_ID_t.EEPM_PRODUCT_SERIAL_NUMBER, idString);
                    }
                    else
                    {
                        serialNumberTextBox.Background = Brushes.OrangeRed;
                    }
                }
                catch (Exception)
                {
                    serialNumberTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void SendErrorChecksumCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            SendErrorChecksum = true;
        }

        private void SendErrorChecksumCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            SendErrorChecksum = false;
        }

        private void TxEnableCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            txEnable = true;
        }

        private void TxEnableCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            txEnable = false;
        }

        private void MontatoPressioneContatoreCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            pressioneContatoreMontato = true;
        }

        private void MontatoPressioneContatoreCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            pressioneContatoreMontato = false;
        }

        private void MontatoTemperaturaContatoreCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            temperaturaContatoreMontato = true;
        }

        private void MontatoTemperaturaContatoreCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            temperaturaContatoreMontato = false;
        }

        private void MontatoTemperaturaEsternoCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            temperaturaEsternoMontato = true;
        }

        private void MontatoTemperaturaEsternoCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            temperaturaEsternoMontato = false;
        }

        private void MontatoBarometroDigitaleCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            pressioneEsternoMontato = true;
        }

        private void MontatoBarometroDigitaleCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            pressioneEsternoMontato = false;
        }

        private void inverterOrPwmPumpComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(inverterOrPwmPumpComboBox.SelectedIndex == 0) 
            {
                inverterOrPwmPump = 0;
            }
            else
            {
                if(inverterOrPwmPumpComboBox.SelectedIndex == 1) /* Inverter */
                {
                    inverterOrPwmPump = 1;
                }
            }
        }

        private void connectDisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            if(_serialPort == null)
            {
                if (serialPortComboBox.SelectedIndex >= 0)
                {
                    _serialPort = new SerialPort(serialPortComboBox.Items[serialPortComboBox.SelectedIndex].ToString(), 9600, Parity.None, 8, StopBits.One);

                    readThread = new Thread(Read);

                    _serialPort.Open();

                    readThread.Start();

                    connectDisconnectButton.Content = "Disconnect";

                    using (StreamWriter outputFile = new StreamWriter(defaultComFileName))
                    {
                        outputFile.WriteLine(serialPortComboBox.Items[serialPortComboBox.SelectedIndex].ToString());
                    }
                }
                else
                {
                   
                    MessageBox.Show("No com selected");
                }
            }
            else
            {
                readThread.Abort();

                _serialPort.Close();

                readThread = null;

                _serialPort = null;

                connectDisconnectButton.Content = "Connect";
            }
           
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                readThread.Abort();

                readThread = null;
            }
            catch(Exception)
            {

            }

            try
            {
                _serialPort.Close();

                _serialPort = null;
            }
            catch (Exception)
            {

            }
        }

        private void powerSupplyPresentCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            powerSupplyPresent = true;
        }

        private void powerSupplyPresentCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            powerSupplyPresent = false;
        }

#region Error and Warning Code 

        private void errorSubCodeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            errorSubCodeTextBox.Background = Brushes.Yellow;
        }

        private void errorSubCodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    ushort value = ushort.Parse(errorSubCodeTextBox.Text);
                    if (value >= 0xFFFE)
                    {
                        errorSubCodeTextBox.Background = Brushes.OrangeRed;
                        errorSubCodeTextBox.Text = "";
                    }
                    else
                    {
                        tempErrorCode = (ushort)((tempErrorCode & 0xFF00U) + value);
                        errorSubCodeTextBox.Background = Brushes.White;
                    }
                }
                catch (Exception)
                {
                    errorSubCodeTextBox.Background = Brushes.OrangeRed;
                }
            }
        }


        private void errorCodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    ushort value = ushort.Parse(errorCodeTextBox.Text);
                    if (value >= 0xFFFE)
                    {
                        errorCodeTextBox.Background = Brushes.OrangeRed;
                        errorCodeTextBox.Text = "";
                    }
                    else
                    {
                        tempErrorCode = (ushort)((tempErrorCode & 0x00FFU) + (value << 8));
                        errorCodeTextBox.Background = Brushes.White;
                    }
                }
                catch (Exception)
                {
                    errorCodeTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void errorCodeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            errorCodeTextBox.Background = Brushes.Yellow;
        }

        private void warningCodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    ushort value = ushort.Parse(warningCodeTextBox.Text);
                    if (value >= 0xFE)
                    {
                        warningCodeTextBox.Background = Brushes.OrangeRed;
                        warningCodeTextBox.Text = "";
                    }
                    else
                    {
                        tempWarningCode = (ushort)((tempWarningCode & 0x00FFU) + (value << 8));
                        warningCodeTextBox.Background = Brushes.White;
                    }
                }
                catch (Exception)
                {
                    warningCodeTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void warningCodeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            warningCodeTextBox.Background = Brushes.Yellow;
        }

        private void warningSubCodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    ushort value = ushort.Parse(warningSubCodeTextBox.Text);
                    if (value >= 0xFE)
                    {
                        warningSubCodeTextBox.Background = Brushes.OrangeRed;
                        warningSubCodeTextBox.Text = "";
                    }
                    else
                    {
                        tempWarningCode = (ushort)((tempWarningCode & 0xFF00U) + value);
                        warningSubCodeTextBox.Background = Brushes.White;
                    }
                }
                catch (Exception)
                {
                    warningSubCodeTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void warningSubCodeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            warningSubCodeTextBox.Background = Brushes.Yellow;
        }

        private void warningCodeSetButton_Click(object sender, RoutedEventArgs e)
        {
            warningCode = tempWarningCode;
        }

        private void errorCodeSetButton_Click(object sender, RoutedEventArgs e)
        {
            errorCode = tempErrorCode;
        }
        #endregion

        private void eepromInitCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            eepromIsInit = true;
        }

        private void eepromInitCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            eepromIsInit = false;
        }

        private void sendCustomBytesTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (sendCustomBytesTextBox.Text.Length > 0)
                {
                    try
                    {
                        String str = sendCustomBytesTextBox.Text;
                        String strNoSpaces = "";
                        foreach (char c in str)
                        {
                            if (c.Equals(' ') != false)
                            {

                            }
                            else
                            {
                                strNoSpaces += c;
                            }
                        }
                        if ((strNoSpaces.Length % 2) == 0)
                        {
                            /* Ok */
                            List<byte> listByte = new List<byte>();
                            listByte.Clear();

                            var bytes = new byte[strNoSpaces.Length / 2];
                            for (var i = 0; i < bytes.Length; i++)
                            {
                                listByte.Add(Convert.ToByte(strNoSpaces.Substring(i * 2, 2), 16));
                            }

                            sendMessageFct(CalculateChecksumAndLengthXbeeProtocol(listByte.ToArray()));

                            sendCustomBytesTextBox.Background = Brushes.White;
                        }
                        else
                        {
                            /* Error do not send */
                            sendCustomBytesTextBox.Background = Brushes.Orange;
                        }
                    }
                    catch (Exception)
                    {
                        sendCustomBytesTextBox.Background = Brushes.Orange;
                    }
                }
            }
        }

        private void sendCustomBytesTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            sendCustomBytesTextBox.Background = Brushes.Yellow;
        }

        private void cpuLoadTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    double value = (double)Math.Round(double.Parse(cpuLoadTextBox.Text), 2);
                    if(value >= 0.0 && value <= 100.0)
                    {
                        cpuLoadTextBox.Background = Brushes.White;
                        if (value.ToString().Equals(cpuLoadTextBox.Text) == false)
                        {
                            cpuLoadTextBox.Text = value.ToString();
                            cpuLoadTextBox.SelectionStart = cpuLoadTextBox.Text.Length;
                            cpuLoadTextBox.SelectionLength = 0;
                            cpuLoad = (float)value;
                            cpuLoadRaw = (ushort)(cpuLoad * 100);
                        }
                    }
                    else
                    {
                        cpuLoadTextBox.Background = Brushes.OrangeRed;
                    }
                }
                catch (Exception)
                {
                    cpuLoadTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void cpuLoadTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            cpuLoadTextBox.Background = Brushes.Yellow;
        }

        private void testVoidLitersLostTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    double value = Math.Round(double.Parse(testVoidLitersLostTextBox.Text), 2);
                    if (value.ToString("F2").Equals(testVoidLitersLostTextBox.Text) == false)
                    {
                        testVoidLitersLostTextBox.Text = value.ToString("F2");
                        testVoidLitersLostTextBox.SelectionStart = testVoidLitersLostTextBox.Text.Length;
                        testVoidLitersLostTextBox.SelectionLength = 0;
                    }
                    testVoidLitersLostTextBox.Background = Brushes.White;
                    TestVoidGetLiterLostFromStart = value;
                }
                catch (Exception)
                {
                    testVoidLitersLostTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void testVoidLitersLostTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            testVoidLitersLostTextBox.Background = Brushes.Yellow;
        }

        private void lastCalibrationDateTimePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            byte[] dateTime = new byte[6];
            DateTime? today = lastCalibrationDateTimePicker.SelectedDate;
            
            dateTime[0] = (byte)((((today.Value.Year / 1000) % 10) << 4) + (((today.Value.Year / 100) % 10) << 0));
            dateTime[1] = (byte)((((today.Value.Year / 10) % 10) << 4) + (((today.Value.Year / 1) % 10) << 0));
            dateTime[2] = (byte)((((today.Value.Month / 10) % 10) << 4) + (((today.Value.Month / 1) % 10) << 0));
            dateTime[3] = (byte)((((today.Value.Day / 10) % 10) << 4) + (((today.Value.Day / 1) % 10) << 0));
            dateTime[4] = (byte)((((today.Value.Hour / 10) % 10) << 4) + (((today.Value.Hour / 1) % 10) << 0));
            dateTime[5] = (byte)((((today.Value.Minute / 10) % 10) << 4) + (((today.Value.Minute / 1) % 10) << 0));
            eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_LAST_CALIBRATION, dateTime);
        }

        private void secondLastCalibrationDateTimePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            byte[] dateTime = new byte[6];
            DateTime? today = secondLastCalibrationDateTimePicker.SelectedDate;
            dateTime[0] = (byte)((((today.Value.Year / 1000) % 10) << 4) + (((today.Value.Year / 100) % 10) << 0));
            dateTime[1] = (byte)((((today.Value.Year / 10) % 10) << 4) + (((today.Value.Year / 1) % 10) << 0));
            dateTime[2] = (byte)((((today.Value.Month / 10) % 10) << 4) + (((today.Value.Month / 1) % 10) << 0));
            dateTime[3] = (byte)((((today.Value.Day / 10) % 10) << 4) + (((today.Value.Day / 1) % 10) << 0));
            dateTime[4] = (byte)((((today.Value.Hour / 10) % 10) << 4) + (((today.Value.Hour / 1) % 10) << 0));
            dateTime[5] = (byte)((((today.Value.Minute / 10) % 10) << 4) + (((today.Value.Minute / 1) % 10) << 0));
            eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_SECOND_LAST_CALIBRATION, dateTime);
        }

        private void firstCalibrationDateTimePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            byte[] dateTime = new byte[6];
            DateTime? today = firstCalibrationDateTimePicker.SelectedDate;
            dateTime[0] = (byte)((((today.Value.Year / 1000) % 10) << 4) + (((today.Value.Year / 100) % 10) << 0));
            dateTime[1] = (byte)((((today.Value.Year / 10) % 10) << 4) + (((today.Value.Year / 1) % 10) << 0));
            dateTime[2] = (byte)((((today.Value.Month / 10) % 10) << 4) + (((today.Value.Month / 1) % 10) << 0));
            dateTime[3] = (byte)((((today.Value.Day / 10) % 10) << 4) + (((today.Value.Day / 1) % 10) << 0));
            dateTime[4] = (byte)((((today.Value.Hour / 10) % 10) << 4) + (((today.Value.Hour / 1) % 10) << 0));
            dateTime[5] = (byte)((((today.Value.Minute / 10) % 10) << 4) + (((today.Value.Minute / 1) % 10) << 0));
            eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_DATETIME_OF_FIRST_CALIBRATION, dateTime);
        }

        private void programEnvironmentalCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            programEnvEnable = true;
            byte[] dataToWrite = new byte[] {(byte)(programEnvEnable == false ? 0 : 1)};
            if(eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_ENVIRONMENTAL, dataToWrite);
            }
        }

        private void programEnvironmentalCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            programEnvEnable = false;
            byte[] dataToWrite = new byte[] { (byte)(programEnvEnable == false ? 0 : 1) };
            if (eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_ENVIRONMENTAL, dataToWrite);
            }
        }

        private void programPm10CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            programP10Enable = true;
            byte[] dataToWrite = new byte[] { (byte)(programP10Enable == false ? 0 : 1) };
            if (eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_PM10, dataToWrite);
            }
        }

        private void programPm10CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            programP10Enable = false;
            byte[] dataToWrite = new byte[] { (byte)(programP10Enable == false ? 0 : 1) };
            if (eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_PM10, dataToWrite);
            }
        }

        private void programDuctCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            programDucEnable = true;
            byte[] dataToWrite = new byte[] { (byte)(programDucEnable == false ? 0 : 1) };
            if (eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_DUCT, dataToWrite);
            }
        }

        private void programDuctCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            programDucEnable = false;
            byte[] dataToWrite = new byte[] { (byte)(programDucEnable == false ? 0 : 1) };
            if (eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_DUCT, dataToWrite);
            }
        }

        private void programTsbRemoteCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            programTsbEnable = true;
            byte[] dataToWrite = new byte[] { (byte)(programTsbEnable == false ? 0 : 1) };
            if (eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_TSB_REMOTE, dataToWrite);
            }
        }

        private void programTsbRemoteCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            programTsbEnable = false;
            byte[] dataToWrite = new byte[] { (byte)(programTsbEnable == false ? 0 : 1) };
            if (eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_TSB_REMOTE, dataToWrite);
            }
        }

        private void programSrbRemoteCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            programSrbEnable = true;
            byte[] dataToWrite = new byte[] { (byte)(programSrbEnable == false ? 0 : 1) };
            if (eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_SRB_REMOTE, dataToWrite);
            }
        }

        private void programSrbRemoteCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            programSrbEnable = false;
            byte[] dataToWrite = new byte[] { (byte)(programSrbEnable == false ? 0 : 1) };
            if (eeprom != null)
            {
                eeprom.setDataFromId(EepromIds.EEPM_ID_t.SYST_PROGRAM_SRB_REMOTE, dataToWrite);
            }
        }

        private void MontatoTemperaturaFiltroCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            temperaturaFiltroMontato = true;
        }

        private void MontatoTemperaturaFiltroCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            temperaturaFiltroMontato = false;
        }

        private void MontatoTemperaturaStorageCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            temperaturaStorageMontato = true;
        }

        private void MontatoTemperaturaStorageCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            temperaturaStorageMontato = false;
        }

        private void ValidoTemperaturaStorageCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            temperaturaStorageValido = true;
        }

        private void ValidoTemperaturaStorageCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            temperaturaStorageValido = false;
        }

        private void ValidoTemperaturaFiltroCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            temperaturaFiltroValido = true;
        }

        private void ValidoTemperaturaFiltroCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            temperaturaFiltroValido = false;
        }

        private void temperaturaFiltroTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    temperaturaFiltro = (float)Math.Round(double.Parse(temperaturaFiltroTextBox.Text), 2);
                    temperaturaFiltroTextBox.Background = Brushes.White;
                    if (temperaturaFiltro.ToString().Equals(temperaturaFiltroTextBox.Text) == false)
                    {
                        temperaturaFiltroTextBox.Text = temperaturaFiltro.ToString();
                        temperaturaFiltroTextBox.SelectionStart = temperaturaFiltroTextBox.Text.Length;
                        temperaturaFiltroTextBox.SelectionLength = 0;
                    }
                    temperaturaFiltroTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    temperaturaFiltroTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void temperaturaFiltroTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            temperaturaFiltroTextBox.Background = Brushes.Yellow;
        }

        private void temperaturaStorageTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    temperaturaStorage = (float)Math.Round(double.Parse(temperaturaStorageTextBox.Text), 2);
                    temperaturaStorageTextBox.Background = Brushes.White;
                    if (temperaturaStorage.ToString().Equals(temperaturaStorageTextBox.Text) == false)
                    {
                        temperaturaStorageTextBox.Text = temperaturaStorage.ToString();
                        temperaturaStorageTextBox.SelectionStart = temperaturaStorageTextBox.Text.Length;
                        temperaturaStorageTextBox.SelectionLength = 0;
                    }
                    temperaturaStorageTextBox.Background = Brushes.White;
                }
                catch (Exception)
                {
                    temperaturaStorageTextBox.Background = Brushes.OrangeRed;
                }
            }
        }

        private void temperaturaStorageTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            temperaturaStorageTextBox.Background = Brushes.Yellow;
        }
    }
}
