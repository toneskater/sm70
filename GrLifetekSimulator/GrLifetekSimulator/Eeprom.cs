﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace GrLifetekSimulator
{
    class Eeprom
    {
        private List<EepromEntry> eepromEntries;
        private String nameFile;

        private void createFileWithDefaultValue()
        {
            EepromIds.createEepromEntries(eepromEntries);

            saveToFile();
        }

        public Eeprom() : this("eepromDefault.eepm")
        {
            
        }

        public Eeprom(String nameFile)
        {
            this.nameFile = nameFile;
            this.eepromEntries = new List<EepromEntry>();
            if(File.Exists(this.nameFile))
            {
                loadFromFile();
            }
            else
            {
                createFileWithDefaultValue();
            }
        }

        private void saveToFile()
        {
            using (StreamWriter outputFile = new StreamWriter(this.nameFile))
            {
                foreach (EepromEntry eepromEntry in eepromEntries)
                {
                    String str = "";
                    byte[] data = eepromEntry.getData();
                    foreach (byte singleByte in data)
                    {
                        str += singleByte.ToString("X2");
                    }
                    outputFile.WriteLine(str);
                } 
            }
        }

        private void loadFromFile()
        {
            List<EepromEntry> tempEepromEntries = new List<EepromEntry>();
            EepromIds.createEepromEntries(tempEepromEntries);
            bool dataAreDifferent = false;
            int loadedValue = 0;

            using (StreamReader sr = new StreamReader(this.nameFile))
            {
                while(sr.EndOfStream == false)
                {
                    int byteCounter;
                    String str = sr.ReadLine();
                    byte[] arrayToSave = null;
                    int startIndex;
                    byteCounter = 0;
                    for (startIndex = 0; startIndex < str.Length; startIndex += 2)
                    {
                        byte[] valueByte = new byte[1];
                        String subString = str.Substring(startIndex, 2);
                        valueByte[0] = byte.Parse(subString, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                        if(arrayToSave == null)
                        {
                            arrayToSave = valueByte;
                        }
                        else
                        {
                            arrayToSave = arrayToSave.Concat(valueByte).ToArray();
                        }
                        byteCounter++;
                    }

                    int byteEntry = tempEepromEntries[loadedValue].getData().Length;
                    if (byteCounter != byteEntry)
                    {
                        dataAreDifferent = true;
                    }
                    eepromEntries.Add(new EepromEntry(arrayToSave));
                    loadedValue++;
                }
            }

            if((dataAreDifferent != false) || (eepromEntries.Count != (int)EepromIds.EEPM_ID_t.EEPM_ID_MAX_NUMBER))
            {
                eepromEntries.Clear();
                for(int i = 0; i < tempEepromEntries.Count; i++)
                {
                    eepromEntries.Add(tempEepromEntries[i]);
                }
            }
        }

        public void setDataFromId(EepromIds.EEPM_ID_t eepm_id, byte[] dataBuffer)
        {
            this.eepromEntries[(int)eepm_id].setData(dataBuffer);
            saveToFile();
        }

        public int getDataLengthFromId(EepromIds.EEPM_ID_t eepm_id)
        {
            return this.eepromEntries[(int)eepm_id].getLengthByte();
        }

        public byte[] getDataFromId(EepromIds.EEPM_ID_t eepm_id)
        {
            return this.eepromEntries[(int)eepm_id].getData();
        }

        internal uint getVersion()
        {
            return EepromIds.getVersion();
        }
    }
}
