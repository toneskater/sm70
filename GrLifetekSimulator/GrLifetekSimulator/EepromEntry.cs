﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrLifetekSimulator
{
    class EepromEntry
    {
        private byte[] data;
        private int length;

        public EepromEntry(byte byteValue)
        {
            this.data = new[] { byteValue };
            this.length = data.Length;
        }
        public EepromEntry(byte[] byteValue)
        {
            this.data = byteValue;
            this.length = data.Length;
        }

        public EepromEntry(uint uintValue)
        {
            this.data = BitConverter.GetBytes(uintValue);
            this.length = data.Length;
        }

        public EepromEntry(uint[] uintValue)
        {
            byte[] arrayToSave = null;
            for(int i = 0; i < uintValue.Length; i++)
            {
                arrayToSave = arrayToSave.Concat(BitConverter.GetBytes(uintValue[i])).ToArray();
            }
            this.data = arrayToSave;
            this.length = data.Length;
        }

        public EepromEntry(int intValue)
        {
            this.data = BitConverter.GetBytes(intValue);
            this.length = data.Length;
        }

        public EepromEntry(int[] intValue)
        {
            byte[] arrayToSave = null;
            for (int i = 0; i < intValue.Length; i++)
            {
                if (arrayToSave == null)
                {
                    arrayToSave = BitConverter.GetBytes(intValue[i]);
                }
                else
                {
                    arrayToSave = arrayToSave.Concat(BitConverter.GetBytes(intValue[i])).ToArray();
                }
            }
            this.data = arrayToSave;
            this.length = data.Length;
        }

        public EepromEntry(ushort ushortValue)
        {
            this.data = BitConverter.GetBytes(ushortValue);
            this.length = data.Length;
        }

        public EepromEntry(ushort[] ushortValue)
        {
            byte[] arrayToSave = null;
            for (int i = 0; i < ushortValue.Length; i++)
            {
                if(arrayToSave == null)
                {
                    arrayToSave = BitConverter.GetBytes(ushortValue[i]);
                }
                else
                {
                    arrayToSave = arrayToSave.Concat(BitConverter.GetBytes(ushortValue[i])).ToArray();
                }
            }
            this.data = arrayToSave;
            this.length = data.Length;
        }

        public EepromEntry(short shortValue)
        {
            this.data = BitConverter.GetBytes(shortValue);
            this.length = data.Length;
        }

        public EepromEntry(short[] shortValue)
        {
            byte[] arrayToSave = null;
            for (int i = 0; i < shortValue.Length; i++)
            {
                if (arrayToSave == null)
                {
                    arrayToSave = BitConverter.GetBytes(shortValue[i]);
                }
                else
                {
                    arrayToSave = arrayToSave.Concat(BitConverter.GetBytes(shortValue[i])).ToArray();
                }
            }
            this.data = arrayToSave;
            this.length = data.Length;
        }

        public int getLengthByte()
        {
            return this.length;
        }

        public byte[] getData()
        {
            return this.data;
        }

        public bool setData(byte[] newData)
        {
            bool returnValue = false;
            if(newData.Length == this.length)
            {
                this.data = newData;
                returnValue = true;
            }
            return returnValue;
        }
    }
}
