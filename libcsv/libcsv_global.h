#ifndef LIBCSV_GLOBAL_H
#define LIBCSV_GLOBAL_H

#ifdef _WIN32
    #define DECL_EXPORT     __declspec(dllexport)
    #define DECL_IMPORT     __declspec(dllimport)
    #define DECL_HIDDEN
#else
    #define DECL_EXPORT     __attribute__((visibility("default")))
    #define DECL_IMPORT     __attribute__((visibility("default")))
    #define DECL_HIDDEN     __attribute__((visibility("hidden")))
#endif

#ifdef CSV_STATIC
    #define CSV_EXPORT DECL_HIDDEN
#else
    #if defined(CSV_LIBRARY)
        #define CSV_EXPORT DECL_EXPORT
    #else
        #define CSV_EXPORT DECL_IMPORT
    #endif
#endif

#endif // LIBCSV_GLOBAL_H
